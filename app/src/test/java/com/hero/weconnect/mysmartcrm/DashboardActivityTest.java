package com.hero.weconnect.mysmartcrm;

import com.hero.weconnect.mysmartcrm.Retrofit.APIClientMain;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.activity.DashboardActivity;
import com.hero.weconnect.mysmartcrm.models.ApiController_Interface;
import com.hero.weconnect.mysmartcrm.models.TokenModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.assertTrue;

public class DashboardActivityTest {

    private static final String login = "test";
    private static final String pass = "test";
    HashMap<String, String> params = new HashMap<>();
    @Mock
    ApiController_Interface apiController_interface;
    String token;

   @Mock
    public DashboardActivity activity;

    @Before
    public void setUp() throws Exception {

        activity= new DashboardActivity();

        apiController_interface = new ApiController_Interface() {
            @Override
            public void getservicereminderdata(String token, HashMap<String, String> offset) {
                MockitoAnnotations.initMocks(this);

            }
        };

    }


    @Test(expected = Exception.class)
    public void ArgumentTest() {
        activity.get_Load_Infotech_Token("","");
        activity.get_Load_Infotech_Token(null,null);
    }

    @Test
    public void test_login(){
        APIInterface apiInterface = APIClientMain.getClienttoken().create(APIInterface.class);
        Call<TokenModel> call  =  apiInterface.getToken(login,pass,"password");
        try {
            Response<TokenModel> response =call.execute();
            TokenModel tokenModel  = response.body();
            System.out.println(tokenModel.getAccess_token());
            token = tokenModel.getAccess_token();
            //  Assert.assertThat(String.format("Email Validity Test failed for %s ", tokenModel.getAccess_token()),
            assertTrue(response.isSuccessful() && tokenModel.getToken_type().contains("bearer"));

        }catch (IOException e)
        {
            e.printStackTrace();
        }




    }
}
