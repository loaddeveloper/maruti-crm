package com.hero.weconnect.mysmartcrm;

import com.hero.weconnect.mysmartcrm.Retrofit.APIClientMain;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.activity.MissedCallActivity_SR;
import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.ApiController_Interface;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MissedCallActivity_SR_Test {
    HashMap<String, String> params = new HashMap<>();
    String token;
    private MissedCallActivity_SR activity;
    @Mock
    private ApiController_Interface mockApi;
    ApiController_Interface apiController_interface;
    @Captor
    private ArgumentCaptor<Callback<List<ServiceReminderDataModel>>> cb;


    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        activity= new MissedCallActivity_SR();
    }


    @Test
    public void getMissedData() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put("MissedTag", "SR");
        CountDownLatch latch = new CountDownLatch(1);

        Call<MissedDataModel_SR> call  =  apiInterface.getMissedData(token,params);
        call.enqueue(new Callback<MissedDataModel_SR>() {
            @Override
            public void onResponse(Call<MissedDataModel_SR> call, Response<MissedDataModel_SR> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<MissedDataModel_SR> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Add History Id Test Case
    @Test
    public void AddHistoryId()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterSRId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.BoundType,"OUTBOUND");
        CountDownLatch latch = new CountDownLatch(1);

        Call<AddCallHistoryUpdateModel> call  =  apiInterface.addCallHistoryUpdate(token,params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Update Customer Reply Test Case

    @Test
    public void updatecustomerreply()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reply, "Call Back");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecustomerreply(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //Update Booking Date Test Case

    @Test
    public void updatebookingdate()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.BookingDate, "2020-08-31 11:50:30");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatebookingdate(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Update Call End Time Test Case
    @Test
    public void updatecallendtime()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallType, "SR");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatecallendtime(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    //Update Remark Test Case
    @Test
    public void updateremark()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallType, "SR");
        params.put(ApiConstant.Comment, "Busy");

        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updateremark(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    //Update FollowUpDate Case
    @Test
    public void updatefollowupdate()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.FollowupDateTime, "2020-08-31 11:14:20");
        params.put(ApiConstant.CallType, "SR");

        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatefollowupdate(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update ContatctStatus Case
    @Test
    public void updatecontatctstatus()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Status, "Contacted");
        params.put(ApiConstant.CallType, "SR");


        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecontactstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    //Update Pick& Drop Case
    @Test
    public void updatepickanddrop()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Status, "YES");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatepickdrop(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update Called Status Case
    @Test
    public void updatecalledstatus()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallStatus, "CALL DONE");
        params.put(ApiConstant.CallType, "SR");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecalledstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update Not Coming Reason Test Case
    @Test
    public void updatenotcomingReason()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reason, "Out of Station");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecalledstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //Update Called Status Case
    @Test
    public void getSRHistoryData()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        CountDownLatch latch = new CountDownLatch(1);

        Call<SR_HistoryModel> call  =  apiInterface.getSRHistory(token,params);
        call.enqueue(new Callback<SR_HistoryModel>() {
            @Override
            public void onResponse(Call<SR_HistoryModel> call, Response<SR_HistoryModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<SR_HistoryModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    //Update Single Customer History Test Case
    @Test
    public void getSingleCustomerSRHistoryData()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        params.put(ApiConstant.Tag, "SR");
        CountDownLatch latch = new CountDownLatch(1);
        Call<SingleCustomerHistoryData> call  =  apiInterface.getsinglecustomerhistory(token,params);
        call.enqueue(new Callback<SingleCustomerHistoryData>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData> call, Response<SingleCustomerHistoryData> response) {

                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void getPositions()
    {
        activity.position=3;
        activity.lastposition=3;
        activity.call_current_position=3;



        Assert.assertEquals(activity.position,3);
        Assert.assertEquals(activity.lastposition,3);
        Assert.assertEquals(activity.call_current_position,3);

    }


    @Test
    public void getDateparse()
    {
        String date=activity.dateparse("2012-07-10 14:58:00.000000");




        Assert.assertEquals(date,"10-Jul-2012 14:58:00");

    }

    @Test
    public void getoffset()
    {

        activity.offset="6";

        Assert.assertEquals(activity.offset,"6");



    }



    @Test
    public void onCallingstatusTest()
    {
        activity.Callingstatus="CALL DONE";

        Assert.assertEquals(activity.Callingstatus,"CALL DONE");

    }


    @Test
    public void customernameTest()
    {
        activity.string_customername="RAJ KUMAR";

        Assert.assertEquals(activity.string_customername,"RAJ KUMAR");

    }


    @Test
    public void masterIdTest()
    {
        activity.string_masterId="kkkkk000011111";

        Assert.assertEquals(activity.string_masterId,"kkkkk000011111");

    }


    @Test
    public void listSizeTest()
    {
        activity.misseddatalist.add(new MissedDataModel_SR.DataBean());
        activity.misseddatalist.add(new MissedDataModel_SR.DataBean());
        activity.misseddatalist.add(new MissedDataModel_SR.DataBean());
        activity.misseddatalist.add(new MissedDataModel_SR.DataBean());
        activity.misseddatalist.add(new MissedDataModel_SR.DataBean());





        Assert.assertEquals(activity.misseddatalist.size(),5);

    }



    @Test
    public void historyListSizeTest()
    {
        activity.serviceReminderHistoryDataList.add(new SR_HistoryModel.DataBean());
        activity.serviceReminderHistoryDataList.add(new SR_HistoryModel.DataBean());
        activity.serviceReminderHistoryDataList.add(new SR_HistoryModel.DataBean());
        activity.serviceReminderHistoryDataList.add(new SR_HistoryModel.DataBean());


        Assert.assertEquals(activity.serviceReminderHistoryDataList.size(),4);

    }


    @Test
    public void disposableTest()
    {
        activity.disposables.add(activity.disposables);

        Assert.assertNotNull(activity.disposables);

    }



    @Test
    public void indexTest()
    {
        activity.index=7;

        Assert.assertEquals(activity.index,3+4);

    }

    @Test
    public void clickedTest()
    {

        activity.whatsappclicked=true;
        activity.textmesasageclicked=false;
        activity.historyID="";

        System.out.println(""+activity.whatsappclicked+"  "+activity.textmesasageclicked);


        Assert.assertNotNull(activity.historyID);
        Assert.assertTrue(activity.whatsappclicked);
        Assert.assertFalse(activity.textmesasageclicked);

    }



    @Test
    public void bookingdate_sendTest()
    {
        activity.bookingdate_send= "22/02/2020";

        Assert.assertEquals(activity.bookingdate_send,"22/02/2020");

    }


    @Test
    public void nextfollowupdate_sendTest()
    {
        activity.nextfollowupdate_send= "22/02/2020";

        Assert.assertEquals(activity.nextfollowupdate_send,"22/02/2020");

    }


}
