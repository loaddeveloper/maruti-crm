package com.hero.weconnect.mysmartcrm;

import android.content.Context;

import com.hero.weconnect.mysmartcrm.Retrofit.APIClientMain;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.ApiController_Interface;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.receiver.PhoneStateReceiverNew;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneStateReceiverNewTest {

    HashMap<String, String> params = new HashMap<>();
    String token;
    @Mock
    public Context activity;
    @Mock
    private ApiController_Interface mockApi;
    ApiController_Interface apiController_interface;
    @Captor
    private ArgumentCaptor<Callback<List<ServiceReminderDataModel>>> cb;


    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        activity=new PhoneStateReceiverNew().contextw;

    }


    @Test
    public void getservicereminderdata() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.offset,"0");
        CountDownLatch latch = new CountDownLatch(1);

        Call<ServiceReminderDataModel> call  =  apiInterface.getServiceReminderData(token,params);
        call.enqueue(new Callback<ServiceReminderDataModel>() {
            @Override
            public void onResponse(Call<ServiceReminderDataModel> call, Response<ServiceReminderDataModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<ServiceReminderDataModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
   /* // UpDate Remark TestCase in Android
    @Test
    public void updateRemark()
    {
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        params = new HashMap<>();
        params.put(ApiConstant.offset,"0");
        Call<CommonModel> call  =  apiInterface.updateremark(token,params);
        try {
            Response<CommonModel> response =call.execute();
            CommonModel commonModel  = response.body();
            assertTrue(response.isSuccessful() && commonModel.getMessage().toLowerCase().equals("updated"));
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }*/



    // Add History Id Test Case
    @Test
    public void AddHistoryId()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterSRId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.BoundType,"OUTBOUND");
        CountDownLatch latch = new CountDownLatch(1);

        Call<AddCallHistoryUpdateModel> call  =  apiInterface.addCallHistoryUpdate(token,params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Update Customer Reply Test Case

    @Test
    public void updatecustomerreply()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reply, "Call Back");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecustomerreply(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //Update Booking Date Test Case

    @Test
    public void updatebookingdate()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.BookingDate, "2020-08-31 11:50:30");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatebookingdate(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Update Call End Time Test Case
    @Test
    public void updatecallendtime()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallType, "SR");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatecallendtime(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    //Update Remark Test Case
    @Test
    public void updateremark()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallType, "SR");
        params.put(ApiConstant.Comment, "Busy");

        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updateremark(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    //Update FollowUpDate Case
    @Test
    public void updatefollowupdate()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.FollowupDateTime, "2020-08-31 11:14:20");
        params.put(ApiConstant.CallType, "SR");

        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatefollowupdate(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update ContatctStatus Case
    @Test
    public void updatecontatctstatus()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Status, "Contacted");
        params.put(ApiConstant.CallType, "SR");


        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecontactstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    //Update Pick& Drop Case
    @Test
    public void updatepickanddrop()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Status, "YES");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatepickdrop(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update Called Status Case
    @Test
    public void updatecalledstatus()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.CallStatus, "CALL DONE");
        params.put(ApiConstant.CallType, "SR");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecalledstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    //Update Not Coming Reason Test Case
    @Test
    public void updatenotcomingReason()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reason, "Out of Station");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatecalledstatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //Update Called Status Case
    @Test
    public void getSRHistoryData()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        CountDownLatch latch = new CountDownLatch(1);

        Call<SR_HistoryModel> call  =  apiInterface.getSRHistory(token,params);
        call.enqueue(new Callback<SR_HistoryModel>() {
            @Override
            public void onResponse(Call<SR_HistoryModel> call, Response<SR_HistoryModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<SR_HistoryModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    //Update Single Customer History Test Case
    @Test
    public void getSingleCustomerSRHistoryData()
    {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, "12dc60fa-4721-4e8a-a147-126c3251ce45");
        params.put(ApiConstant.Tag, "SR");
        CountDownLatch latch = new CountDownLatch(1);
        Call<SingleCustomerHistoryData> call  =  apiInterface.getsinglecustomerhistory(token,params);
        call.enqueue(new Callback<SingleCustomerHistoryData>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData> call, Response<SingleCustomerHistoryData> response) {

                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Get Enquiry Data Test Case
    @Test
    public void getEnquiryData() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put("MissedTag", "SR");
        CountDownLatch latch = new CountDownLatch(1);

        Call<SalesEnquiryDataModel> call  =  apiInterface.getSalesEnquiryData(token,params);
        call.enqueue(new Callback<SalesEnquiryDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryDataModel> call, Response<SalesEnquiryDataModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<SalesEnquiryDataModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }




    // Update Date of Purchase Test Case
    @Test
    public void UpdateDateofPurchase() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.ExpectedPurchaseDate, "2020-09-30 11:20:33");
        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatedateofpurchase(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //Update Closure Reason Test Case
    @Test
    public void UpdateClosureReason() {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reason, "");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updateclosurereason(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //Update Closure Sub Reason Test Case
    @Test
    public void UpdateClosureSubReason() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId,  "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Reason, "");
        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updateclosuresubreason(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
    //Update Make  Test Case
    @Test
    public void UpdateMake() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.MakeBought, "HONDA");

        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatemake(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    //Update Model Test Case
    @Test
    public void UpdateModel() {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.ModelBought, "HONDA");

        CountDownLatch latch = new CountDownLatch(1);
        Call<CommonModel> call  =  apiInterface.updatemodel(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                System.out.println("Success");
                latch.countDown();

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



    //Update Follow Up Done  Test Case
    @Test
    public void updateFolloupDoneStatus()
    {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, "01144D4F-266F-46E5-8035-4D01498CE371");
        params.put(ApiConstant.Status, "Contacted");

        CountDownLatch latch = new CountDownLatch(1);

        Call<CommonModel> call  =  apiInterface.updatesfollowupdonestatus(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                System.out.println("Success");
                latch.countDown();
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    //Update Not Coming Reason Test Case







}