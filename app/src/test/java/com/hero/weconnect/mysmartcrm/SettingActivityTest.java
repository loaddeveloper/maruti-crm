package com.hero.weconnect.mysmartcrm;

import android.content.Context;

import com.hero.weconnect.mysmartcrm.Retrofit.APIClientMain;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.activity.SettingActivity;
import com.hero.weconnect.mysmartcrm.models.ApiController_Interface;
import com.hero.weconnect.mysmartcrm.models.GetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SetSMSTemplateCommonModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivityTest {

    HashMap<String, String> params = new HashMap<>();
    String token;

    @Mock
    Context context;
    @Mock
    private SettingActivity activity;
    @Mock
    private ApiController_Interface mockApi;
    ApiController_Interface apiController_interface;
    @Captor
    private ArgumentCaptor<Callback<List<ServiceReminderDataModel>>> cb;



    @Before
    public void init() throws Exception {

        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void updatesmsstatus() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put("SmsStatus", "smsstatus");
        CountDownLatch latch = new CountDownLatch(1);

        Call<SetSMSTemplateCommonModel> call  =  apiInterface.updatesmsstatus(token,params);
        call.enqueue(new Callback<SetSMSTemplateCommonModel>() {
            @Override
            public void onResponse(Call<SetSMSTemplateCommonModel> call, Response<SetSMSTemplateCommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<SetSMSTemplateCommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void getsmsstatus() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        CountDownLatch latch = new CountDownLatch(1);

        Call<GetSMSTemplateCommonModel> call  =  apiInterface.getsmsstatus(token);
        call.enqueue(new Callback<GetSMSTemplateCommonModel>() {
            @Override
            public void onResponse(Call<GetSMSTemplateCommonModel> call, Response<GetSMSTemplateCommonModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<GetSMSTemplateCommonModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void getSmsTempleteList() {

        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);

        String token = "bearer sSsPm0sjj36qJd4YIk1hKJCxZWRVokplWiwy63oaVHiHNgHZnu1pPzwJq46Rz-jHTwoWe6MlhRLsJ8cp0ro8YK0cHcJtysDgFSm7cg0DK14A7cwRDqk4CnF28dqYWgMu6TAW66ixU4st5nH_DMULAyeQUszJt8z-jKGh3kkkxgCc-rVj_ig8kl23j1aZrlJmNt9qsuD5D8DhEqTi3D02OSpzl1NPtLWGVYZBfBxK4NvyYTOHxys5tWAWK9K_agHAGpopW0kA9Y9XsnUU8g7jzxjbMkMiHm3skrzkY4IusBg";
        params = new HashMap<>();
        params.put("TemplateType", "templatetype");
        CountDownLatch latch = new CountDownLatch(1);

        Call<GetTamplateListModel> call  =  apiInterface.getsmstemplate(token,params);
        call.enqueue(new Callback<GetTamplateListModel>() {
            @Override
            public void onResponse(Call<GetTamplateListModel> call, Response<GetTamplateListModel> response) {

                System.out.println("Success");



                latch.countDown();
            }

            @Override
            public void onFailure(Call<GetTamplateListModel> call, Throwable t) {

                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void checkSwitch()
    {
        boolean ischeckd=true;

        Assert.assertTrue(ischeckd);
        
    }




}