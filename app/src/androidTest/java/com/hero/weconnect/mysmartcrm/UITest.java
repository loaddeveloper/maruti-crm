package com.hero.weconnect.mysmartcrm;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.DashboardActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UITest {
//    @Test
//    public void useAppContext() {
//        // Context of the app under test.
//        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
//        assertEquals("com.hero.weconnect", appContext.getPackageName());
//    }

    public boolean isSales=false;

    @Rule
    public ActivityScenarioRule<DashboardActivity> activityRule = new ActivityScenarioRule<>(DashboardActivity.class);
//
//    @Rule
//    public ActivityScenarioRule<DashboardActivity> s_r_activityActivityScenarioRule = new ActivityScenarioRule<>(DashboardActivity.class);

  /*  @Test
    public void testPerformOnLogin() {
        activityRule.getScenario().recreate();
        final String username = "test";


     //   activityRule.getScenario().moveToState(Lifecycle.State.CREATED);

        final String pass = "test";
        onView(withId(R.id.username)).perform(typeText(username), closeSoftKeyboard());
        onView(withId(R.id.Password)).perform(typeText(pass), closeSoftKeyboard());
        onView(withId(R.id.username)).check(matches(withText("test")));
        onView(withId(R.id.Password)).check(matches(withText("test")));
        onView(withId(R.id.login)).perform(click());

      // activityRule.getScenario().moveToState(Lifecycle.State.STARTED);


        try {
            Thread.sleep(3000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // this view is in OTP Activity
        testSomethingAndSomethingElse1();
    }*/

  @Test
    public void testSomethingAndSomethingElse1() {
      //  new ExpressoTestOTP().testPerformOnOTP();
      //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);

        activityRule.getScenario().recreate();


;
       // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);

        if(CommonVariables.APP.equals("service"))
        {
            onView(withId(R.id.sr_cardview)).perform(click());

            isSales=false;

        }else
        {
            onView(withId(R.id.card_salesfollowup)).perform(click());
            isSales=true;
        }


        try {
            Thread.sleep(3000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);



        if(isSales)
        {
            testcallingending();
        }else
        {
            testcalling();

        }

    }


    public void testcalling() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        final String username = "test";
        final String pass = "test";
        ;
        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
            onView(withId(R.id.startcall)).perform(click());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);


        testSkipCall();
    }


    public void testcallingpending() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        final String username = "test";
        final String pass = "test";
        ;
        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
           onView(withId(R.id.followups_spinner)).perform(click());



        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);


        testSkipCall();
    }



    public void testSkipCall() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        final String username = "test";
        final String pass = "test";
        ;
        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
            onView(withId(R.id.pasusecall)).perform(click());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);


        testcallingend();
    }



    public void testcallingend() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        final String username = "test";
        final String pass = "test";
        ;
        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
            onView(withId(R.id.endcall)).perform(click());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);

    testcallingIndex();
    }




    public void testcallingIndex() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        final String index = "5";
        ;
        onView(withId(R.id.indexs)).perform(typeText(index), closeSoftKeyboard());
        onView(withId(R.id.indexs)).check(matches(withText("5")));

        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
            onView(withId(R.id.startcall)).perform(click());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);
testcallingending();

    }

    public void testcallingending() {
        //  new ExpressoTestOTP().testPerformOnOTP();
        //  activityRule.getScenario().moveToState(Lifecycle.State.INITIALIZED);


        ;
        // activityRule.getScenario().moveToState(Lifecycle.State.RESUMED);


        try {
            Thread.sleep(3000);
            onView(withId(R.id.endcall)).perform(click());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  activityRule.getScenario().moveToState(Lifecycle.State.DESTROYED);


    }







}