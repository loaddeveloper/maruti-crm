package com.hero.weconnect.mysmartcrm.customretrofit.Retrofit;

public class ApiConstant {
    /**
     * Base Url
     */

    //Production
    public static final String baseUrlApi = "https://weconnect.heromotocorp.biz/customcallingapi/";
    public static final String baseUrltokenApi = "https://loadcrm.com/customcallingapi/";
//Development
/*   public static final String baseUrlApi = "https://uat-weconnect.heromotocorp.biz/customcallingapi/";
    public static final String baseUrltokenApi = "https://loadcrm.com/customcallingapi/";*/


    /**
     * Param Name
     *
     *
     *
     *
     */
    public static final String uid = "uid";
    public static final String code = "code";

    /**
     * Bean Tag
     */
    public static final String TOKEN_TAG = "TOKEN_TAG";
    public static final String CUSTOMCALLING = "CUSTOMCALLING";
    public static final String CAMPAIGNLIST = "CAMPAIGNLIST";
    public static final String INPUTFIELDLIST = "INPUTFIELDLIST";
    public static final String INPUTDATAFIELDLIST = "INPUTDATAFIELDLIST";

    public static final String CALLING_SHARED = "CALLING_SHARED";
    public static final String CONTACT_STATUS = "CONTACT_STATUS";
    public static final String CALL_START = "CALL_START";
    public static final String SR = "SR";
    public static final String ENQUIRY = "ENQUIRY";
    public static final String DEALLOCATION = "DEALLOCATION";
    public static final String CALL_DISCONNECT = "CALL_DISCONNECT";
    public static final String CUSTOMER_REPLY = "CUSTOMER_REPLY";
    public static final String NOT_COMING_REASON = "NOT_COMING_REASON";
    public static final String CLOUSERREASON = "CLOUSERREASON";
    public static final String CLOUSERSUBREASON = "CLOUSERSUBREASON";
    public static final String MAKE = "MAKE";
    public static final String MODEL = "MODEL";
    public static final String CALLPROGRESS = "CALLPROGRESS";
    public static final String REPORTINGDATA = "REPORTINGDATA";
    public static final String ADDCALLHISTORYUPDATESR = "ADDCALLHISTORYUPDATESR";
    public static final String UPDATECONTACTNUMBERSR = "UPDATECONTACTNUMBERSR";
    public static final String UPDATECUSTOMERREPLYSR = "UPDATECUSTOMERREPLYSR";
    public static final String UPDATETAGGERSRSTATUSSR = "UPDATETAGGERSRSTATUSSR";
    public static final String UPDATEBOOKINGDATETIME = "UPDATEBOOKINGDATETIME";
    public static final String UPDATECALLENDTIME = "UPDATECALLENDTIME";
    public static final String UPDATESTATUS = "UPDATESTATUS";
    public static final String UPDATEREMARK = "UPDATEREMARK";
    public static final String CommonModel = "CommonModel";
    public static final String UPDATEFOLLOWUPDATE = "UPDATEFOLLOWUPDATE";
    public static final String UPDATECONTACTSTATUS = "UPDATECONTACTSTATUS";
    public static final String UPDATECALLEDSTATUS = "UPDATECALLEDSTATUS";
    public static final String UPDATENOTCOMINGREASON = "UPDATENOTCOMINGREASON";

    public static final String SALESENQUIRYDATA = "SALESENQUIRYDATA";
    public static final String UPDATESALESHISTORYID = "UPDATESALESHISTORYID";
    public static final String UPDATEDATEOFPURCHASE = "UPDATEDATEOFPURCHASE";
    public static final String UPDATECLOSUREREASON = "UPDATECLOSUREREASON";
    public static final String UPDATECLOSURESUBREASON = "UPDATECLOSURESUBREASON";
    public static final String UPDATEMAKE = "UPDATEMAKE";
    public static final String UPDATEMODEL = "UPDATEMODEL";
    public static final String UPDATEPICKDROP = "UPDATEPICKDROP";
    public static final String CREATEFOLLOWUP = "CREATEFOLLOWUP";
    public static final String SERVICEREQUEST = "SERVICEREQUEST";
    public static final String GETCALLINGHISTORY = "GETCALLINGHISTORY";
    public static final String GETSALESCALLINGHISTORY = "GETSALESCALLINGHISTORY";
    public static final String SETSMSSTATUS = "SETSMSSTATUS";
    public static final String SETSMSTEMPLATE = "SETSMSTEMPLATE";
    public static final String ADDSMSTEMPLATE = "ADDSMSTEMPLATE";
    public static final String GETSMSTEMPLATE = "GETSMSTEMPLATE";
    public static final String GETACTIVECALLING = "GETACTIVECALLING";
    public static final String GETSMSTATUS = "GETSMSTATUS";
    public static final String VIDEODATA = "VIDEODATA";
    public static final String UPLOADRECORDING = "UPLOADRECORDING";
    public static final String SRINCOMINGDATA = "SRINCOMINGDATA";
    public static final String ENQUIRYINCOMINGDATA = "ENQUIRYINCOMINGDATA";
    public static final String PACKAGEADD = "PACKAGEADD";
    public static final String MISSEDDATA = "MISSEDDATA";
    public static final String MISSEDDATASALES = "MISSEDDATASALES";
    public static final String SINGLEDATA = "SINGLEDATA";
    public static final String SINGLEDATASALES = "SINGLEDATASALES";
    public static final String ADDEXCEPTION = "ADDEXCEPTION";
    public static final String GETFOLLOWUPDONELIST = "GETFOLLOWUPDONELIST";
    public static final String GETFOLLOWUPSTATUSLIST = "GETFOLLOWUPSTATUSLIST";
    public static final String UPDATESALESFOLLOWUPSTATUS = "UPDATESALESFOLLOWUPSTATUS";
    public static final String UPDATESALESFOLLOWUPDONESTATUS = "UPDATESALESFOLLOWUPDONESTATUS";
    public static final String CANCELBOOKINGREQUEST = "CANCELBOOKINGREQUEST";

    /**** Constant API Params ****/
    public static final String COUNTRY_NAME = "India";
    public static final String Offset = "Offset";
    public static final String Campaign_Name = "Campaign_Name";
    public static final String Campaign_Id = "Campaign_Id";
    public static final String filter = "filter";
    public static final String Response_Type = "Response_Type";
    public static final String FollowupType = "FollowupType";
    public static final String MasterSEId = "MasterSEId";
    public static final String HistoryId = "History_Id";
    public static final String BoundType = "DialType";
    public static final String ExpectedPurchaseDate = "ExpectedPurchaseDate";
    public static final String Reason = "Reason";
    public static final String MakeBought = "MakeBought";
    public static final String ClosureReason = "ClosureReason";
    public static final String ModelBought = "ModelBought";
    public static final String MasterId = "Call_Id";
    public static final String FollowupDateTime = "FollowupDateTime";
    public static final String CallType = "CallType";
    public static final String Status = "Status";
    public static final String Comment = "Comment";
    public static final String CallStatus = "CallStatus";
    public static final String Tag = "Tag";
    public static final String MasterSRId = "Call_Id";
    public static final String NewContactNo = "NewContactNo";
    public static final String PriorityStatus = "PriorityStatus";
    public static final String Reply = "Reply";
    public static final String data = "data";
    public static final String Field_Type = "Field_Type";


    /*--------------------------------Common Calling SharePreferances----------------------------------------------*/
    public static final String COMMONCALLINGCOLORSHAREDPREANCES = "COMMONCALLINGCOLORSHAREDPREANCES";
    public static final String CALL_START_STOP_SHAREDPREANCES = "CALL_START_STOP_SHAREDPREANCES";
    public static final String COMMONCALLINGCOLORSHAREDPREANCESSHREE = "COMMONCALLINGCOLORSHAREDPREANCESSHREE";
    public static final String COMMONCALLINGCOLORPOSITION = "COMMONCALLINGCOLORPOSITION";
    public static final String SHAREDPREFERANCESPOSITION = "SHAREDPREFERANCESPOSITION";
    public static final String SHAREDPREFERANCECALLCURRENTSTATUS = "SHAREDPREFERANCECALLCURRENTSTATUS";
    public static final String SHAREDPREFERANCESCALLCURRENTMOBILENO = "SHAREDPREFERANCESCALLCURRENTMOBILENO";
    public static final String SHAREDPREFERANCESMASTETID = "SHAREDPREFERANCESMASTETID";


    public static String UNKNOWN_NUMBER = "UNKNOWN NUMBER";
}
