package com.hero.weconnect.mysmartcrm.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.role.RoleManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.hero.weconnect.mysmartcrm.CallUtils.CallRecorderService;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.Calling_Sales_EnquirySharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.ConnectionDetector;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.Utils.NetworkCheckerService;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_MissedCall_Enquiry;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Sales_Historydata;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_SalesModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_Sales;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.receiver.PhoneStateReceiverNew;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.analytics.Analytics;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;
import static java.lang.Thread.sleep;

public class MissedCallActivity_ENQUIRY extends AppCompatActivity implements View.OnClickListener, ApiResponseListener,
        Adapter_MissedCall_Enquiry.CustomButtonListener, AdapterTemplateShow.CustomButtonListener {

    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private static final int REQUEST_PERMISSION = 0;
    public static int calling_list_serial_no;
    public int dataposition;
    Intent recordService;

    public static boolean whatsupclick = false, callstuck = false, textmessageclick = false;
    Spinner followuptype, followuptype_spinner, clouserreason_spinner, clousersubreason_spinner, make_spinner, model_spinner, sp_enquiry_status;
    RecyclerView history_data_recyclerview, recyclerView_template;
    EditText et_nextfollowupdate, expected_nextfollowupdate, remarks;
    int counter = 0;
    Incoming_SalesModel.DataBean dataBeanSales;
    ArrayList<String>[] clouserreasonlist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] clousersubreasonlist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] makelist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] modellist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] followuplist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] followupdonelist = new ArrayList[]{new ArrayList<String>()};
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    public String string_called_status = "", string_customername, string_masterId, string_mobileno, whatsapp_number,oldhid="",getmodel="",getsubreason="";
    public String token = "", offset = "0", selectedPath = "", indexposition, historyID, Callingstatus = "BUSY", callstatus, followupdonestatus, followupstatus, closurereason, closuresubreason, make, string_model;
    ArrayAdapter<String> clouserreasonArrayAdapter, clousersubreasonArrayAdapter, makeArrayAdapter, modelArrayAdapter, followupArrayAdapter,followupDoneArrayAdapter;
    Calling_Sales_EnquirySharedPref callingSharedPref;
    HashMap<String, String> params = new HashMap<>();
    ImageView iv_nextfollowup, iv_expectednextfollowupdate, iv_clouserreson;
    public int call_current_position = 0, position = 0, state, lastposition;
    NetworkCheckerService networkCheckerService;
    public ArrayList<Sales_HistoryModel.DataBean> salesReminderHistoryDataList = new ArrayList<>();
    Adapter_Sales_Historydata adapter_sales_historydata;
    File audiofile = null;
    AudioManager am = null;
    MediaRecorder recorder;
    boolean stop = false;
    Date currentdail, discom;
    CustomDialog customWaitingDialog;
    ArrayList<String> arrayList_matserId = new ArrayList<String>();
    ArrayList<String> arrayList_customername = new ArrayList<String>();
    ArrayList<String> arrayList_callstatus = new ArrayList<String>();
    ArrayList<String> arrayList_mobileno = new ArrayList<String>();
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    SharedPreferences sharedPreferencescallUI;
    ImageView iv_booking, iv_nextfollowupdate;
    ArrayAdapter<String> contactArrayAdapter, customerArrayAdapter, notcomingreasonArrayAdapter;
    ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    boolean morebtncliked = false;
    AdapterTemplateShow adapterSettingTemplet;
    ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    Dialog Whatsapp_shippingDialog;
    public boolean whatsappclicked = false, textmesasageclicked = false;
    SingleCustomerHistoryData.DataBean singledatabean;
    Adapter_MissedCall_Enquiry adapter_missedCall;
    public ArrayList<MissedDataModel_Sales.DataBean> misseddatalist = new ArrayList<>();
    ArrayList<SingleCustomerHistoryData_Sales.DataBean> singledatalist = new ArrayList<>();
    public  String expecteddate_send, nextfollowupdate_send, NewDateFormat = null,FollowUpContatctStatus="";
    LinearLayout nodata_found_layout;
    Button Retry_Button;
    //  Class Variable Declartion
    private ImageButton statcall, pasusecall, endcall;
    private AppBarLayout appBarLayout;
    private LinearLayout ll_clouserreson, ll_clousersubreason, ll_make, ll_userInputs,ll_model;
    private EditText indexs, edit_salescuname, edit_sales_enquiry_number, edit_sales_enquiry_open_date, edit_sale_enquiry_status, edit_pickdrop;
    private ImageButton draggable_view;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private LinearLayoutManager linearLayoutManager;
    public CompositeDisposable disposables = new CompositeDisposable();
    private ConnectionDetector detector;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView misscall_recyclerview;
    public int index=0;
    CardView card_nextfollow,card_dop;
    Date date1;
    public boolean calldone=false;
    public boolean morebtnclicked=false;


    @SuppressLint("NewApi")
    public static void start(Context context, Call call) {
        context.startActivity(new Intent(context, S_R_Activity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle()));
    }

    public static String getFileSize(File file) {
        String modifiedFileSize = null;
        double fileSize = 0.0;
        if (file.isFile()) {
            fileSize = (double) file.length();//in Bytes
            if (fileSize < 1024) {
                modifiedFileSize = String.valueOf(fileSize).concat("B");
            } else if (fileSize > 1024 && fileSize < (1024 * 1024)) {
                modifiedFileSize = String.valueOf(Math.round((fileSize / 1024 * 100.0)) / 100.0).concat("KB");
            } else {
                modifiedFileSize = String.valueOf(Math.round((fileSize / (1024 * 1204) * 100.0)) / 100.0).concat("MB");
            }
        } else {
            modifiedFileSize = "Unknown";
        }

        return modifiedFileSize;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missed_call_list__sales__follow_up);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+ CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "MissedCallActivity_ENQUIRY");
        properties.put("Role", ""+CommonVariables.role);

        Analytics.trackEvent("MissedCallActivity_ENQUIRY", properties);

        networkCheckerService = new NetworkCheckerService(this, this);
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        customWaitingDialog = new CustomDialog(this);
        initView();



    }

    // Initialization of views
    private void initView() {

        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        apiController = new ApiController(this,this);
        callingSharedPref = new Calling_Sales_EnquirySharedPref(this);
        commonSharedPref = new CommonSharedPref(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        detector = new ConnectionDetector(MissedCallActivity_ENQUIRY.this);

        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Unauthorized access !!", Toast.LENGTH_SHORT).show();
            return;
        }
        token = "bearer " + commonSharedPref.getLoginData().getAccess_token();


        String folder_main = "Smart CRM";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        apiController.getClosureReason(token);
        apiController.getMake(token);
        apiController.getContactStatus(token);
        apiController.getFollowContatctStatusList(token);
        apiController.getFollowUpDoneList(token);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        // Id Genrattion
        /*--------------------------------------------------------------------------------------------*/
        statcall = findViewById(R.id.statcall);
        pasusecall = findViewById(R.id.pasusecall);
        endcall = findViewById(R.id.endcall);
        indexs = findViewById(R.id.indexs);
        nodata_found_layout = findViewById(R.id.nodatfound);
        Retry_Button = findViewById(R.id.refrshbutton);
        misscall_recyclerview = findViewById(R.id.missed_recyclerview);
        draggable_view = findViewById(R.id.draggable_view);



        /*--------------------------------------------------------------------------------------------*/


        if (common_calling_color_sharedPreferances.getPosition() == null && common_calling_color_sharedPreferances.getPosition().equals("")) {
            position = 0;
        } else {
            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
        }

        adapter_missedCall = new Adapter_MissedCall_Enquiry(this, MissedCallActivity_ENQUIRY.this, misseddatalist);
        linearLayoutManager = new LinearLayoutManager(MissedCallActivity_ENQUIRY.this);
        misscall_recyclerview.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(misscall_recyclerview.getContext(), linearLayoutManager.getOrientation());
        misscall_recyclerview.setAdapter(adapter_missedCall);
        adapter_missedCall.setCustomButtonListner(MissedCallActivity_ENQUIRY.this);
        adapter_missedCall.notifyDataSetChanged();

        getMissedData(token, ApiConstant.ENQUIRY,offset);


        new OngoingCall();
        Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
        disposables.add(disposable);
        new OngoingCall();
        Disposable disposable2 = OngoingCall.state
                .filter(state -> state == Call.STATE_DISCONNECTED)
                .delay(1, TimeUnit.SECONDS)
                .firstElement()
                .subscribe(this::finish);
        disposables.add(disposable2);


        indexs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {

                    int postionoflist = adapter_missedCall.getItemCount() - 1;


                    int asg = Integer.parseInt(indexs.getText().toString()) - 1;

                    index=asg;

                    if (asg < postionoflist) {
                        // ////Log.e("SizeofListMainAct","SizeofListMainAct"+postionoflist);

                    } else {
                        Toasty.warning(MissedCallActivity_ENQUIRY.this, " Index Not Avialabe in list Load More Data", Toast.LENGTH_LONG).show();
                    }

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
                    misscall_recyclerview.scrollToPosition(asg);


                } catch (Exception e) {

                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        statcall.setOnClickListener(this);
//        pasusecall.setOnClickListener(this);
//        endcall.setOnClickListener(this);
        draggable_view.setOnClickListener(this);


        statcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TelecomManager tm = (TelecomManager)getSystemService(Context.TELECOM_SERVICE);

                if (tm == null) {
                    // whether you want to handle this is up to you really
                    throw new NullPointerException("tm == null");
                }
                if(tm.isInCall()) {
                    try {
                        tm.endCall();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                indexs.setEnabled(false);
                try {

                    TelecomManager systemService = getSystemService(TelecomManager.class);
                    if (systemService != null && !systemService.getDefaultDialerPackage().equals(getPackageName())) {
                        startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.hero.weconnect"));
                        Toasty.warning(MissedCallActivity_ENQUIRY.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
                    } else {


                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

                            Toasty.warning(MissedCallActivity_ENQUIRY.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
                        } else {

                            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {


                                position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            } else {

                                position = Integer.parseInt(indexs.getText().toString()) - 1;

                            }
                            common_calling_color_sharedPreferances.setCall_start_stop_status("start");
                            looping();

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }
            }
        });
        endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                indexs.setEnabled(true);
                try {
                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");


                    OngoingCall.hangup();
                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }


                    if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {

                        OngoingCall.hangup();
                        CommonVariables.endcallclick = false;
                        CommonVariables.receivecall = false;
                        CommonVariables.incoming = false;
                        callstuck = true;
//                    Intent intent= new Intent(Sales_Followup_Activity.this,DashboardActivity.class);
//                    finish();
//                    startActivity(intent);
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
                try {
                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        CallService.discon1();
                    }
                } catch (Exception e) {
                    e.printStackTrace();


                }


                CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long l) {

                        if (calldone) {

                            if (call_current_position < arrayList_matserId.size())
                            {
                                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editss = chkfss.edit();
                                editss.putString("call", "CALL DONE");
                                editss.commit();

                            }
                        }

                        //  if(adapter_salesReminder != null)  adapter_salesReminder.notifyDataSetChanged();

                    }

                    @Override
                    public void onFinish() {


                    }
                };
                countDownTimer.start();
                misscall_recyclerview.scrollToPosition(call_current_position);
            }
        });
        pasusecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }
                try {
                    OngoingCall.hangup();


                } catch (Exception e) {

                }

                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {
                        clearCallUISharePreferances();
                        scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));

                        try {
                            int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));


                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        CallService.discon1();
                        if (OngoingCall.call4.size() > 0) OngoingCall.call4.get(0).disconnect();
                        looping();

   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
    startActivity(i);
    finish();*/


                    }

                } catch (Exception e) {

                }
            }
        });
        Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(MissedCallActivity_ENQUIRY.this, MissedCallActivity_ENQUIRY.class);
                startActivity(refresh);
                finish();
            }
        });
        // Call Functions APIS for getting LOVS from Server

        // Follow Up DropDown Selected Listener


    }

    // OnclickListener Implementation
    @Override
    public void onClick(View v) {
        int id = v.getId();
//        if (id == R.id.statcall) {
//
//            TelecomManager tm = (TelecomManager)getSystemService(Context.TELECOM_SERVICE);
//
//            if (tm == null) {
//                // whether you want to handle this is up to you really
//                throw new NullPointerException("tm == null");
//            }
//
//            tm.endCall();
//            indexs.setEnabled(false);
//            try {
//
//                TelecomManager systemService = getSystemService(TelecomManager.class);
//                if (systemService != null && !systemService.getDefaultDialerPackage().equals(MissedCallActivity_ENQUIRY.this.getPackageName())) {
//                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, MissedCallActivity_ENQUIRY.this.getPackageName()));
//                    Toasty.warning(MissedCallActivity_ENQUIRY.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
//                } else {
//
//
//                    if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
//
//                        Toasty.warning(MissedCallActivity_ENQUIRY.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
//
//
//                            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
//
//                        } else {
//
//                            position = Integer.parseInt(indexs.getText().toString()) - 1;
//
//                        }
//                        common_calling_color_sharedPreferances.setCall_start_stop_status("start");
//                        looping();
//
//                    }
//
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            if (DashboardActivity.callsk) {
//                DashboardActivity.callsk = false;
//            }
//        } else
//            if (id == R.id.pasusecall) {
//            if (DashboardActivity.callsk) {
//                DashboardActivity.callsk = false;
//            }
//            try {
//                OngoingCall.hangup();
//
//
//            } catch (Exception e) {
//
//            }
//
//            try {
//
//                if (sharedPreferencescallUI.contains("CALLUI")) {
//                    clearCallUISharePreferances();
//                    scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
//
//                    try {
//                        int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
//
//                        common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
//
//
//                    } catch (NumberFormatException e) {
//                        e.printStackTrace();
//                    }
//                    CallService.discon1();
//                    looping();
//
//   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
//    startActivity(i);
//    finish();*/
//
//
//                }
//
//            } catch (Exception e) {
//
//            }
//
//        } else
//            if (id == R.id.endcall) {
//
//            indexs.setEnabled(true);
//            try {
//                common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
//                common_calling_color_sharedPreferances.setcallcurrentstatus("d");
//
//
//                OngoingCall.hangup();
//                if (DashboardActivity.callsk) {
//                    DashboardActivity.callsk = false;
//                }
//
//
//                if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {
//
//                    OngoingCall.hangup();
//                    CommonVariables.endcallclick = false;
//                    CommonVariables.receivecall = false;
//                    CommonVariables.incoming = false;
//                    callstuck = true;
////                    Intent intent= new Intent(MissedCallActivity_ENQUIRY.this,DashboardActivity.class);
////                    finish();
////                    startActivity(intent);
//                }
//
//
//            } catch (Exception e) {
//
//                e.printStackTrace();
//            }
//            try {
//                if (sharedPreferencescallUI.contains("CALLUI")) {
//
//                    CallService.discon1();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//
//
//            }
//
//
//            CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
//                @Override
//                public void onTick(long l) {
//
//                    if (!stop) {
//
//                        if (call_current_position < arrayList_matserId.size()) {
//                            SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editss = chkfss.edit();
//                            editss.putString("call", "CALL DONE");
//                            editss.commit();
//                        }
//                    }
//
//                }
//
//                @Override
//                public void onFinish() {
//
//
//                }
//            };
//            countDownTimer.start();
//            misscall_recyclerview.scrollToPosition(call_current_position);
//        } else
            if (id == R.id.search) {
        } else if (id == R.id.draggable_view) {
        }
    }

    /*--------------------------------------------------------Calling Logic----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void looping() {
        Callingstatus = "BUSY";

       // Toast.makeText(this, ""+call_current_position, Toast.LENGTH_SHORT).show();

        if (common_calling_color_sharedPreferances != null && common_calling_color_sharedPreferances.getCall_start_stop_status() != null && common_calling_color_sharedPreferances.getCall_start_stop_status().equals("start")) {
            try {
                scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                call_current_position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());


                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                final String icca = chkfss.getString("call", "");
                string_called_status = arrayList_callstatus.get(call_current_position);


                if (string_called_status.equals("CALL DONE")) {

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else if (icca.equals("CALL DONE")) {

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else {
                    if (detector.isInternetAvailable()) {


                        common_calling_color_sharedPreferances.setcurrentcallmobileno(arrayList_mobileno.get(call_current_position));
                        common_calling_color_sharedPreferances.setcallcurrentstatus("s");
                        common_calling_color_sharedPreferances.setmasterid(arrayList_matserId.get(call_current_position));
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position));
                        misscall_recyclerview.setAdapter(adapter_missedCall);
                        adapter_missedCall.notifyDataSetChanged();


                        if (checkSelfPermission(CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {


                            if (sharedPreferencescallUI.contains("CALLUI")) {

                                clearCallUISharePreferances();
                            }

                            // if(indexs != null)indexs.setText(""+position);
                            Uri uri = Uri.parse("tel:" +"+91"+ ""+arrayList_mobileno.get(position));
                            CommonVariables.mobilenumber=arrayList_mobileno.get(position);

                           // Uri uri = Uri.parse("tel:" + "" + CommonVariables.SALES_Testingnumber);

                            startActivity(new Intent(Intent.ACTION_CALL, uri));
                            position = position + 1;

                            String uid = UUID.randomUUID().toString();


                            try {
                                string_masterId = arrayList_matserId.get(call_current_position);
                                string_mobileno = arrayList_mobileno.get(call_current_position);
                                string_customername = arrayList_customername.get(call_current_position);

                            } catch (IndexOutOfBoundsException e) {

                            }
                            SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editsas = prefsss.edit();
                            editsas.putString(string_masterId, uid);
                            editsas.commit();

                        } else {
                            // Request permission to call
                            ActivityCompat.requestPermissions(MissedCallActivity_ENQUIRY.this, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                        }
                        String uid = UUID.randomUUID().toString();


                        try {
                            string_masterId = arrayList_matserId.get(call_current_position);
                            string_mobileno = arrayList_mobileno.get(call_current_position);
                            string_customername = arrayList_customername.get(call_current_position);

                        } catch (IndexOutOfBoundsException e) {

                        }
                        SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editsas = prefsss.edit();
                        editsas.putString(string_masterId, uid);
                        editsas.commit();
                    } else {
                        Toasty.error(this, "Please connect Internet Connection", Toast.LENGTH_SHORT).show();

                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            } catch (IndexOutOfBoundsException e) {

                e.printStackTrace();
            }
        } else {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    public void updateUi(Integer state) {

        if (CommonVariables.incoming) {

            try {
                sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            CommonVariables.incoming = false;
            return;
        }

        // Set callInfo text by the state
        // a.setText(CallStateString.asString(state).toLowerCase() + "\n" + number);
        this.state = state;
        if (state == Call.STATE_DIALING) {
            stop = false;
            final Handler handler = new Handler();
            currentdail = Calendar.getInstance().getTime();
            final int delay = 4000;


            calldone=false;
            handler.postDelayed(new Runnable() {
                public void run() {

                    if (!stop) {

                        if (sharedPreferencescallUI.contains("CALLUI")) {
                        } else {
                            OngoingCall.call2.add(OngoingCall.call);
                            try {


                                recordService = new Intent(MissedCallActivity_ENQUIRY.this, CallRecorderService.class);
                                startService(recordService);
                             //   startRecording(historyID);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // //Log.e("FollowUpContatctStatus","Follow"+position+"  "+historyID);


                            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                                int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                                try {
                                    string_masterId = arrayList_matserId.get(ass);
                                    string_mobileno = arrayList_mobileno.get(ass);
                                    string_customername = arrayList_customername.get(ass);
                                } catch (IndexOutOfBoundsException e) {

                                    e.printStackTrace();
                                }

                            }
                            if(followuptype_spinner != null && position==dataposition) {
                                // //Log.e("FollowUpContatctStatus","Follow"+position+"  "+dataposition);

                                if(followuptype_spinner != null) followuptype_spinner.setSelection(1);
                                if(followuptype_spinner != null) followuptype_spinner.setEnabled(true);

                            }
                            followupstatus="not_contacted";
                            Callingstatus = "BUSY";
                            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            historyID = chkfss2.getString(string_masterId, "");
                       if(!arrayList_customername.get(position).isEmpty()) AddHistoryId(token, string_masterId, historyID);
                            try {
                                sleep(500);
                                updatecalledstatus(token, historyID, Callingstatus , CommonVariables.SALES_FOLLOW_UP);
                                updatecontatctstatus(token, historyID, "not_contacted", getString(R.string.ENQUIRYTAG));
                                updateFollowupDoneStatus(token,historyID,"Open");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            }, delay);
        }
        else if (state == Call.STATE_ACTIVE) {

            calldone=true;


            followupstatus="contacted";
            updatecontatctstatus(token, historyID, "contacted", getString(R.string.ENQUIRYTAG));
            updateFollowupDoneStatus(token,historyID,"Open");

            if(followuptype_spinner != null && position== dataposition) {
                followuptype_spinner.setSelection(0);
                followuptype_spinner.setEnabled(true);
                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

            }



            if (sharedPreferencescallUI.contains("CALLUI")) {
            } else {

                Callingstatus = "CALL DONE";
                SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                SharedPreferences.Editor editsfs = prefss.edit();
                editsfs.putString("call", Callingstatus);
                editsfs.commit();
                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");
                updatecalledstatus(token, historyID, Callingstatus , CommonVariables.SALES_FOLLOW_UP);


            }


        }
        else if (state == Call.STATE_DISCONNECTED) {


//            if(followuptype_spinner != null && position== dataposition) {
//             //   followuptype_spinner.setSelection(0);
//                followuptype_spinner.setEnabled(false);
//                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);
//
//            }

            if (CommonVariables.receivecall) adapter_missedCall.notifyDataSetChanged();
            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                try {
                    string_masterId = arrayList_matserId.get(ass);
                    string_mobileno = arrayList_mobileno.get(ass);
                    string_customername = arrayList_customername.get(ass);

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();

                }
            }
            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");

            updatecallendtime(token, historyID, ApiConstant.ENQUIRY);
             stopService(recordService);
            stopRecording(historyID);

            discom = Calendar.getInstance().getTime();
            try {
                long diffInMs = discom.getTime() - currentdail.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                if (diffInSec > 3) {
                    stop = false;
                } else {
                    stop = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (sharedPreferencescallUI.contains("CALLUI")) {

            } else {
                OngoingCall.call2.clear();


                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {

                    common_calling_color_sharedPreferances.clearposition();

                }


                common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                try {

                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                        /*asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition())));
                    } else {
                        /* asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition())+1 ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) + 1));
                    }
                    /* common_calling_color_sharedPreferances.setPosition(String.valueOf(asgkkk))*/

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                looping();

                SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                SharedPreferences.Editor editsfs = prefss.edit();
                editsfs.putString("call", Callingstatus);
                editsfs.commit();


            }


            adapter_missedCall.notifyDataSetChanged();


        }
        else if (state == Call.STATE_DISCONNECTING) {

            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");

            adapter_missedCall.notifyDataSetChanged();
            updatecallendtime(token, historyID, CommonVariables.SALES_FOLLOW_UP);

        }
        else if (!(state == Call.STATE_ACTIVE)) {
            if (Callingstatus.equals("CALL DONE")) {

            } else {
                Callingstatus = "BUSY";

            }
        }
        else if (!(state == Call.STATE_RINGING)) { }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void offerReplacingDefaultDialer() {
        if (Build.VERSION.SDK_INT <= 28) {
            TelecomManager systemService = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                systemService = this.getSystemService(TelecomManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
                }
            }
        } else {
            @SuppressLint("WrongConstant")
            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
            startActivityForResult(intent, 101);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void finish(Integer state) {


    }
    /*------------------------------------------------------------------------------------------------------------------------*/

    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStop() {
        super.onStop();
        customWaitingDialog.dismiss();
        disposables.clear();




    }

    /*-------------------------------------------------Activity Deafult Function-----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {

        super.onStart();
        offerReplacingDefaultDialer();


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (MissedCallActivity_ENQUIRY.whatsupclick == false && state == Call.STATE_ACTIVE && !CommonVariables.incoming && MissedCallActivity_ENQUIRY.textmessageclick == false) {

            /*OngoingCall.hangup();

            startActivity(new Intent(S_R_Activity.this,DashboardActivity.class));
            // pausestate=false;
            finish();*/

           // OngoingCall.hangup();

        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (MissedCallActivity_ENQUIRY.whatsupclick || MissedCallActivity_ENQUIRY.textmessageclick) {

            MissedCallActivity_ENQUIRY.whatsupclick = false;
            MissedCallActivity_ENQUIRY.textmessageclick = false;
            new OngoingCall();
            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            disposables.add(disposable);
        }

    }

    @Override
    public void onBackPressed() {

        final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.back_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        shippingDialog.setCanceledOnTouchOutside(false);
        TextView title = shippingDialog.findViewById(R.id.title);
        TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
        TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
        title.setText("Are you sure you want to exit \n Sales Follow-Up?");

        okbutton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (state == Call.STATE_ACTIVE) {
                    OngoingCall.hangup();

                }

                try {

                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    OngoingCall.hangup();
                    misscall_recyclerview.setAdapter(adapter_missedCall);

                    adapter_missedCall.notifyDataSetChanged();
                    String packagename = commonSharedPref.getPackagesList();
                    if (Build.VERSION.SDK_INT <= 28) {

                        TelecomManager systemService = MissedCallActivity_ENQUIRY.this.getSystemService(TelecomManager.class);
                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                        }
                        shippingDialog.dismiss();

                    } else {

                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                        startActivity(i);
                        shippingDialog.dismiss();


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                } catch (Exception e) {
                    e.printStackTrace();

                }
                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        clearCallUISharePreferances();
                        CallService.discon1();
                        misscall_recyclerview.setAdapter(adapter_missedCall);
                        adapter_missedCall.notifyDataSetChanged();


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();

                shippingDialog.dismiss();
                SharedPreferences prefssss = getSharedPreferences("call", Context.MODE_PRIVATE);
                SharedPreferences.Editor editss = prefssss.edit();
                editss.clear();
                editss.commit();
                editss.apply();
                clearCallUISharePreferances();
                common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                common_calling_color_sharedPreferances.clear__start_stop_status();
                clearcolorsharedeprefances();
            }
        });

        canncelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        shippingDialog.show();

    }
    /*------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------Other Function -----------------------------------------------------------------*/

    @Override
    protected void onDestroy() {

   //     Toast.makeText(this, "Destrory", Toast.LENGTH_SHORT).show();
        if (commonSharedPref.getLoginData() != null) {
            params = new HashMap<>();
            params.put("Type", ApiConstant.SR);
            params.put("UserName", CommonVariables.UserId);
            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
            super.onDestroy();
            //  networkCheckerService.UnregisterChecker();
            disposables.dispose();

        }
    }

    public void scrollmovepostion(Integer ab) {


        misscall_recyclerview.scrollToPosition(ab);

    }

    boolean isLastVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) misscall_recyclerview.getLayoutManager());
        lastposition = layoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = adapter_missedCall.getItemCount();
        int sizes = misseddatalist.size();
        return (lastposition >= numItems - 1);
    }

    public void notconn(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MissedCallActivity_ENQUIRY.this);
        builder.setTitle("No internet Connection");
        builder.setCancelable(false);
        builder.setMessage("Please turn on internet connection to continue. Last call data will not be saved");

        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (detector.isInternetAvailable()) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    notconn("");
                }


            }
        });
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();


                try {

                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");

                    OngoingCall.hangup();

                    misscall_recyclerview.setAdapter(adapter_missedCall);

                    adapter_missedCall.notifyDataSetChanged();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        clearCallUISharePreferances();
                        CallService.discon1();
                        //  recyclerView.setHasFixedSize(true);
                        //   recyclerView.setLayoutManager(linearLayoutManager);
                        //recyclerView.addItemDecoration(dividerItemDecoration);
                        misscall_recyclerview.setAdapter(adapter_missedCall);

                        adapter_missedCall.notifyDataSetChanged();

                      /*  Intent i = new Intent(getApplicationContext(), Dashboard.class);
                        startActivity(i);
                        finish();*/


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        AlertDialog alertDialog = builder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //
    public void clearCallUISharePreferances() {
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        SharedPreferences.Editor editss = sharedPreferencescallUI.edit();
        editss.clear();
        editss.commit();
        editss.apply();

    }
    /*------------------------------------------------------------------------------------------------------------------*/



    /*---------------------------------------------------API Call Methods------------------------------------------------------------*/


    //Get Missed Data
    public void getMissedData(String token, String missedtag,String offset) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MissedTag", missedtag);
        params.put("Offset",offset);
        apiController.getMissedDataSales(token, params);

    }


    //Get Single Customer Sales Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String tag, String masterId) {

        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, masterId);
        params.put(ApiConstant.Tag, tag);

        apiController.getSingleCustomerHistoryData_Sales(token, params);
    }

    // Add  Sales History Id Function
    public void AddHistoryId(String token, String mastersrid, String historyId) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.MasterSEId, mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BoundType, getString(R.string.OUTBOUND));
        apiController.UpdateHistorySales(token, params);

    }

    // Add Date of Purchase in this Function
    public void UpdateDateofPurchase(String token, String historyId, String expecteddateofpurchase,String masterid) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.ExpectedPurchaseDate, expecteddateofpurchase);
        apiController.Updatedateofpurchase(token, params);

        SharedPreferences prefs = getSharedPreferences(masterid, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status2", "NextFollowUp");
        edits.commit();

    }

    // Update Closure Reason in this Function
    public void UpdateClosureReason(String token, String historyId, String Reason,String masterid) {
       // customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, Reason);
        apiController.Updatedateofpurchase(token, params);

        SharedPreferences prefs = getSharedPreferences(masterid, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status3", "ClosureReason");
        edits.commit();

    }

    // Update Closure Sub Reason in this Function
    public void UpdateClosureSubReason(String token, String historyId, String Reason) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, Reason);
        apiController.UpdateClosureSubReason(token, params);

    }

    // Update Make in this Function
    public void UpdateMake(String token, String historyId, String make) {
       //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MakeBought, make);
        apiController.UpdateMake(token, params);

    }

    //Get Closure Reason List()
    public void getclosurereason(String token, String closurereson) {
        params = new HashMap<>();
        params.put(ApiConstant.ClosureReason, closurereson);
        apiController.getClosureSubReason(token, params);
    }

    // Update Model in this Function
    public void UpdateModel(String token, String historyId, String model) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.ModelBought, model);
        apiController.UpdateModel(token, params);

    }

    //  get Sales Reminder History Data
    public void getSalesHistoryData(String token, String Id) {

      //  customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, Id);

        apiController.getSalesHistory(token, params);
    }

    // Update Next Follow-up date in Database for this function API Code
    public void updatefollowupdate(String token, String historyId, String followdatetime, String calltype,String masterid) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.FollowupDateTime, followdatetime);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateNextFollowUpdate(token, params);

        SharedPreferences prefs = getSharedPreferences(masterid, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "booking");
        edits.commit();
    }

    // Update Contatct Status
    public void updatecontatctstatus(String token, String historyId, String contactstatys, String calltype) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatys);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateSaleFollowUpStatus(token, params);
    }

    // Update Followup Done Status
    public void updateFollowupDoneStatus(String token, String historyId, String contactstatus) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatus);

        apiController.UpdateSaleFollowUpDoneStatus(token, params);
    }

    // Update Remark
    public void updateremark(String token, String historyId, String remark, String calltype,String masterid) {
       // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Comment, remark);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateRemark(token, params);

        SharedPreferences prefs = getSharedPreferences(masterid, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status1", "rmkcolor");
        edits.commit();
    }

    // Update CalledStatus
    public void updatecalledstatus(String token, String historyId, String callstatus, String calltype) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallStatus, callstatus);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCalledStatus(token, params);
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String calltype) {
        // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCallEndTime(token, params);
    }




    // OnSuccess API Response Function
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.CLOUSERREASON)) {
            ClouserReasonModel clouserReasonModel = (ClouserReasonModel) superClassCastBean;
            if (clouserReasonModel.getData() != null && clouserReasonModel.getData().size() > 0) {
                clouserreasonlist[0].add("  ");
                for (ClouserReasonModel.DataBean dataBean : clouserReasonModel.getData()) {
                    clouserreasonlist[0].add(dataBean.getReason());

                }
                customWaitingDialog.dismiss();
            }
        }else if (beanTag.matches(ApiConstant.GETFOLLOWUPDONELIST)) {
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                //  followupdonelist[0].add("Open");
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followupdonelist[0].add(dataBean.getStatus());

                }
                      customWaitingDialog.dismiss();


            }

        }  else if (beanTag.matches(ApiConstant.CLOUSERSUBREASON)) {
            ClouserSubReasonModel clouserSubReasonModel = (ClouserSubReasonModel) superClassCastBean;
            if (clouserSubReasonModel.getData() != null && clouserSubReasonModel.getData().size() > 0) {
                for (ClouserSubReasonModel.DataBean dataBean : clouserSubReasonModel.getData()) {
                    clousersubreasonlist[0].add(dataBean.getStatus());


                }


            }else
            {
                clousersubreasonlist[0].add("  ");

            }

            clousersubreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, clousersubreasonlist[0]);
            clousersubreason_spinner.setAdapter(clousersubreasonArrayAdapter);
            customWaitingDialog.dismiss();
        } else if (beanTag.matches(ApiConstant.MAKE)) {
            MakeModel makeModel = (MakeModel) superClassCastBean;
            if (makeModel.getData() != null && makeModel.getData().size() > 0) {
                makelist[0].add("  ");
                for (MakeModel.DataBean dataBean : makeModel.getData()) {
                    makelist[0].add(dataBean.getMake());

                }
                customWaitingDialog.dismiss();

            }

        }  else if (beanTag.matches(ApiConstant.MODEL)) {
            modellist[0].clear();
            ModelModel modelModel = (ModelModel) superClassCastBean;
            if (modelModel.getData() != null && modelModel.getData().size() > 0) {
                for (ModelModel.DataBean dataBean : modelModel.getData()) {
                    modellist[0].add(dataBean.getModel());

                }

                modelArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, modellist[0]);
                model_spinner.setAdapter(modelArrayAdapter);
                for(int i=0;i<modellist[0].size();i++)
                {
                    if(getmodel.equals(modellist[0].get(i)))
                    {
                        model_spinner.setSelection(i);
                    }
                }

                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.UPDATESALESFOLLOWUPDONESTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().equals("Updated"))
            {


                }


                customWaitingDialog.dismiss();



        } else if (beanTag.matches(ApiConstant.GETFOLLOWUPSTATUSLIST)) {
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                //followuplist[0].add("Follow-UP");
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followuplist[0].add(dataBean.getStatus());

                }



                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.UPDATECALLEDSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.UPDATECALLENDTIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }

        }
        if (beanTag.matches(ApiConstant.MISSEDDATASALES)) {
            if (misseddatalist.size() == 0) offset = "0";
            if (morebtnclicked)
            {

                morebtnclicked = false;
            }
            else
            {
                misseddatalist.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();
            }

            MissedDataModel_Sales missedDataModel_sales = (MissedDataModel_Sales) superClassCastBean;

            if (missedDataModel_sales.getData().toString().contains("[]")) {
                customWaitingDialog.dismiss();
                nodata_found_layout.setVisibility(View.VISIBLE);

            } else {

                if (missedDataModel_sales.getData() != null) {
                    for (MissedDataModel_Sales.DataBean dataBean : missedDataModel_sales.getData()) {

                        offset = "" + dataBean.getSno();
                        misseddatalist.add(dataBean);
                        arrayList_mobileno.add(dataBean.getMobileno_missed());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_and_last_name());
                        arrayList_callstatus.add("BUSY");
                        nodata_found_layout.setVisibility(View.GONE);

                    }
                    misscall_recyclerview.setAdapter(adapter_missedCall);
                    customWaitingDialog.dismiss();
                    adapter_missedCall.notifyDataSetChanged();
                }
            }


        } else if (beanTag.matches(ApiConstant.UPDATESALESHISTORYID)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATEDATEOFPURCHASE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATECLOSUREREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATECLOSURESUBREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEMAKE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.UPDATEMODEL)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.GETSALESCALLINGHISTORY)) {
            Sales_HistoryModel sales_historyModel = (Sales_HistoryModel) superClassCastBean;

            if (sales_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(this, "No History Found..!", Toast.LENGTH_SHORT).show();
                customWaitingDialog.dismiss();

            }
            if (sales_historyModel.getData() != null) {

                for (Sales_HistoryModel.DataBean dataBean : sales_historyModel.getData()) {
                    salesReminderHistoryDataList.add(dataBean);

                }
                customWaitingDialog.dismiss();
                adapter_sales_historydata.notifyDataSetChanged();
            }


        } else if (beanTag.matches(ApiConstant.UPLOADRECORDING)) {
            UploadRecordingModel commonModel = (UploadRecordingModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
               // CommonVariables.audiofile.delete();
                CommonVariables.audiofile=null;
                customWaitingDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.ENQUIRYINCOMINGDATA)) {

            Incoming_SalesModel incoming_salesModel = (Incoming_SalesModel) superClassCastBean;

            if (incoming_salesModel.getData().size() > 0) {
                 dataBeanSales = incoming_salesModel.getData().get(0);

                edit_salescuname.setText("" + dataBeanSales.getCusotmer_first_and_last_name());
                edit_sales_enquiry_number.setText("" + dataBeanSales.getEnquirynumber());
                edit_sales_enquiry_open_date.setText("" + dateparse3(dataBeanSales.getEnquiry_open_date().substring(0,10)));
                edit_sale_enquiry_status.setText("" + dataBeanSales.getEnquiry_status().toUpperCase());
                // edit_pickdrop.setText(dataBeanSales.());
                if(dataBeanSales.getEnquiry_status().toUpperCase().equals("OPEN"))
                {
                    sp_enquiry_status.setSelection(1);
                    sp_enquiry_status.setEnabled(true);
                }else
                {
                    sp_enquiry_status.setSelection(0);
                    sp_enquiry_status.setEnabled(false);
                }


                ll_userInputs.setVisibility(View.VISIBLE);
                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");
                updatecalledstatus(token, historyID, "" + Callingstatus, getString(R.string.ENQUIRYTAG));
                try {
                    startRecording(historyID);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //  tv_customerName.setText(dataBeanSales.getCusotmer_first_and_last_name());

                // remarks.setText(""+dataBeanSales.getEnquiry_comments());

                AddHistoryId(token, dataBeanSales.getId(), historyID);
                getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), dataBeanSales.getId());


            } else {

            }

        }

        else if (beanTag.matches(ApiConstant.SINGLEDATASALES)) {


            customWaitingDialog.dismiss();
            singledatalist.clear();
            SingleCustomerHistoryData_Sales singleCustomerHistoryData = (SingleCustomerHistoryData_Sales) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {
                singledatalist.addAll(singleCustomerHistoryData.getData());

                FollowUpContatctStatus = singleCustomerHistoryData.getData().get(0).getFollowupDone();


                if (singleCustomerHistoryData.getData().get(0).getNextFollowupDate().contains("1900-01-01T00:00:00")) {

                    et_nextfollowupdate.setText("");
                } else {

                    dateparse(singleCustomerHistoryData.getData().get(0).getNextFollowupDate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", ""));
                    et_nextfollowupdate.setText(NewDateFormat);
                }
                if (singleCustomerHistoryData.getData().get(0).getExpectedDateOfPurchase().contains("1900-01-01T00:00:00")) {

                    expected_nextfollowupdate.setText("");

                } else {
                    dateparse(singleCustomerHistoryData.getData().get(0).getExpectedDateOfPurchase().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", ""));

                    expected_nextfollowupdate.setText(NewDateFormat);
                }

                try {
                    followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getFollowupStatus().trim()));
                    clouserreason_spinner.setSelection(((ArrayAdapter<String>) clouserreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getClosurereason()));

//                    getclosurereason(token, singleCustomerHistoryData.getData().get(0).getClosurereason());
//                    ll_clousersubreason.setVisibility(View.VISIBLE);

                    make_spinner.setSelection(((ArrayAdapter<String>) make_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getMake()));
                    params = new HashMap<>();
                    params.put("Make", singleCustomerHistoryData.getData().get(0).getMake());
                    apiController.getModel(token, params);
                    model_spinner.setSelection(((ArrayAdapter<String>) model_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getModel()));

                    sp_enquiry_status.setSelection(((ArrayAdapter<String>) sp_enquiry_status.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getFollowupStatus()));
                    remarks.setText("" + singleCustomerHistoryData.getData().get(0).getRemark());

                    if (singleCustomerHistoryData.getData().get(0).getClosurereason() != null && !singleCustomerHistoryData.getData().get(0).getClosureSubReason().equals("")) {
                        ll_clousersubreason.setVisibility(View.VISIBLE);
                        clousersubreason_spinner.setVisibility(View.VISIBLE);
                        clousersubreason_spinner.setSelection(((ArrayAdapter<String>) clousersubreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getClosureSubReason()));
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();

                }


            }else
            {
                sp_enquiry_status.setSelection(1);
            }
        }


    }

    @Override
    public void onFailure(String msg) {
        if (msg.matches(ApiConstant.MISSEDDATASALES)) {
            customWaitingDialog.dismiss();
            nodata_found_layout.setVisibility(View.VISIBLE);

        } else {
            customWaitingDialog.dismiss();
        }

    }

    @Override
    public void onError(String msg) {
        customWaitingDialog.dismiss();
    }


    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }

    /*------------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------Adapter Click Listeners----------------------------------------------------------------*/
//show customer Detailsdata popup
    @Override
    public void getCustomerDetails(int position, String matserid, String customername,
                                   String enuiryno, String model, String enquiryopendate, String enquirystatus, String mobileno) {

        dataposition= position+1;

        final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_customer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);
        /*--------------------------------ID Genration-----------------------------------------------*/
        clouserreason_spinner = shippingDialog.findViewById(R.id.closurereason);
        clousersubreason_spinner = shippingDialog.findViewById(R.id.closuresubreason);
        make_spinner = shippingDialog.findViewById(R.id.make);
        model_spinner = shippingDialog.findViewById(R.id.model);
        sp_enquiry_status = shippingDialog.findViewById(R.id.sp_enquiry_status);
        followuptype_spinner = shippingDialog.findViewById(R.id.followups_spinner);
        ll_make = shippingDialog.findViewById(R.id.makelayout);
        ll_model=shippingDialog.findViewById(R.id.modellayout);
        iv_clouserreson = shippingDialog.findViewById(R.id.add_updateDetails);
        iv_nextfollowup = shippingDialog.findViewById(R.id.iv_clear_nextfollowupdate);
        iv_expectednextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_expectedfollowupdate);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        expected_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupeditexpected);
        ll_clouserreson = shippingDialog.findViewById(R.id.ll_clouserreson);
        ll_clousersubreason = shippingDialog.findViewById(R.id.ll_reasonsub);
        card_nextfollow=shippingDialog.findViewById(R.id.nextfollowupdate);
        card_dop=shippingDialog.findViewById(R.id.dop);
        EditText sales_customername = shippingDialog.findViewById(R.id.salescuname);
        EditText sales_enquiry_number = shippingDialog.findViewById(R.id.sales_enquiry_number);
        EditText enquiry_open_date = shippingDialog.findViewById(R.id.sales_enquiry_open_date);
        EditText sales_enquiry_status = shippingDialog.findViewById(R.id.sale_enquiry_status);
        remarks = shippingDialog.findViewById(R.id.remarks);
        getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), matserid);



        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);
        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);

        /*-------------------------------------------------------------------------------*/


        /*-----------------------------------Edit Text to Set Values--------------------------------------------*/
        if (misseddatalist.get(position).getCusotmer_first_and_last_name() != null)
            sales_customername.setText("" + misseddatalist.get(position).getCusotmer_first_and_last_name().toUpperCase());
        if (misseddatalist.get(position).getEnquirynumber() != null)
            sales_enquiry_number.setText("" + misseddatalist.get(position).getEnquirynumber().toUpperCase());
        if (misseddatalist.get(position).getEnquiry_open_date() != null)
            enquiry_open_date.setText("" + misseddatalist.get(position).getEnquiry_open_date().toUpperCase().replaceAll(" 00:00:00.0000000", ""));
        if (misseddatalist.get(position).getEnquiry_status() != null)
            sales_enquiry_status.setText("" + misseddatalist.get(position).getEnquiry_status().toUpperCase());



        /*-------------------------------------------------------------------------------*/



        /*----------------------------------PopUp Click Listeners---------------------------------------------*/

        ib_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSalesHistoryData(token, misseddatalist.get(position).getId());

                final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.historydata_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);
                TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
                history_data_recyclerview.setHasFixedSize(true);
                history_data_recyclerview.setLayoutManager(new LinearLayoutManager(MissedCallActivity_ENQUIRY.this, RecyclerView.VERTICAL, false));
                adapter_sales_historydata = new Adapter_Sales_Historydata(MissedCallActivity_ENQUIRY.this, salesReminderHistoryDataList);
                history_data_recyclerview.setAdapter(adapter_sales_historydata);
                adapter_sales_historydata.notifyDataSetChanged();


                shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        customWaitingDialog.dismiss();
                    }
                });
                shippingDialog.show();

            }
        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });
        iv_clouserreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                if (counter % 2 != 0) {
                    iv_clouserreson.setImageResource(R.drawable.ic_baseline_remove_circle_outline_24);
                    ll_make.setVisibility(View.VISIBLE);
                    ll_clouserreson.setVisibility(View.VISIBLE);


                } else {
                    iv_clouserreson.setImageResource(R.drawable.icon_add);
                    ll_make.setVisibility(View.GONE);
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);

                }

            }
        });
        expected_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                if (dateTimeFragment == null) {
                    dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                            getString(R.string.label_datetime_dialog),
                            getString(android.R.string.ok),
                            getString(android.R.string.cancel),
                            getString(R.string.clean) // Optional
                    );



                }

                if(date1 != null)

                {
                    dateTimeFragment.setMinimumDateTime(date1);
                }

                // Optionally define a timezone
                dateTimeFragment.setTimeZone(TimeZone.getDefault());

                // Init format


                final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());

                // Assign unmodifiable values
                dateTimeFragment.set24HoursMode(true);
                dateTimeFragment.setHighlightAMPMSelection(false);


                // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                // Define new day and month format
                try {
                    dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                }

                // Set listener for date
                // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        if(et_nextfollowupdate.getText().length()>0)
                        {

                            if(date.getTime()>=date1.getTime())
                            {
                                expected_nextfollowupdate.setText(myDateFormat.format(date));
                                expecteddate_send = myDateFormat1.format(date);
                            }else
                            {
                                Toast.makeText(MissedCallActivity_ENQUIRY.this, "Expected date of purchase should be greater " +
                                        "than Nextfollowup date", Toast.LENGTH_SHORT).show();
                            }



                        }else {
                            expected_nextfollowupdate.setText(myDateFormat.format(date));
                            expecteddate_send = myDateFormat1.format(date);

                        }

//                        if (et_bookingdate.getText().length() != 0)
//                        {
//                            contactstatus.setSelection(((ArrayAdapter<String>)contactstatus.getAdapter()).getPosition("Contacted"));
//                        }

                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                        // Do nothing
                    }

                    @Override
                    public void onNeutralButtonClick(Date date) {
                        // Optional if neutral button does'nt exists
                        expected_nextfollowupdate.setText("");
                    }
                });
                dateTimeFragment.startAtCalendarView();

                if(date1 ==null) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    dateTimeFragment.setMinimumDateTime(cal.getTime());
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, 1);
                    dateTimeFragment.setMaximumDateTime(cal.getTime());
                    dateTimeFragment.setDefaultDateTime(new Date());
                }else
                {
                    dateTimeFragment.setDefaultDateTime(date1);

                }
                // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                if (dateTimeFragment.isAdded())
                {
                    return;
                }
                else
                {
                    dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                }



            }
        });
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                if (dateTimeFragment == null ) {
                    dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                            getString(R.string.label_datetime_dialog),
                            getString(android.R.string.ok),
                            getString(android.R.string.cancel),
                            getString(R.string.clean) // Optional
                    );
                }

                // Optionally define a timezone
                dateTimeFragment.setTimeZone(TimeZone.getDefault());

                // Init format


                final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                // Assign unmodifiable values
                dateTimeFragment.set24HoursMode(true);
                dateTimeFragment.setHighlightAMPMSelection(false);


                // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                // Define new day and month format
                try {
                    dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                }

                // Set listener for date
                // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        date1 = date;
                        et_nextfollowupdate.setText(myDateFormat.format(date));
                        nextfollowupdate_send = myDateFormat1.format(date);
                        if (et_nextfollowupdate.getText().length() != 0) {
                            followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition("Contacted"));
                        }
                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                        // Do nothing
                    }

                    @Override
                    public void onNeutralButtonClick(Date date) {
                        // Optional if neutral button does'nt exists
                        et_nextfollowupdate.setText("");
                    }
                });
                dateTimeFragment.startAtCalendarView();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                dateTimeFragment.setMinimumDateTime(cal.getTime());
                cal.setTime(new Date());
                cal.add(Calendar.MONTH, 1);
                dateTimeFragment.setMaximumDateTime(cal.getTime());
                dateTimeFragment.setDefaultDateTime(new Date());
                // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                if (dateTimeFragment.isAdded())
                {
                    return;
                }
                else
                {
                    dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                }



            }


        });
        iv_nextfollowup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_nextfollowupdate.setText("");
            }
        });
        iv_expectednextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expected_nextfollowupdate.setText("");
            }
        });

        /*----------------------------------------------------------------------------------------------------*/



        /*---------------------------------Spinner Selected Listener----------------------------------------------*/

        followuptype_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if(Callingstatus.equals("BUSY"))
                {
                    followuptype_spinner.setSelection(1);
                    followupstatus = followuptype_spinner.getSelectedItem().toString();

                }else if(Callingstatus.equals("CALL DONE"))
                {
                    followuptype_spinner.setSelection(0);
                    followupstatus = followuptype_spinner.getSelectedItem().toString();


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


                followupstatus = followuplist[0].get(0);
            }
        });

        followupDoneArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, followupdonelist[0]);
         sp_enquiry_status.setAdapter(followupDoneArrayAdapter);
        sp_enquiry_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                followupdonestatus = sp_enquiry_status.getSelectedItem().toString();


                /*-----------------------------------Visibility Gone Code--------------------------------------------*/
                if (followupdonestatus.equals("Open"))
                {
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_make.setVisibility(View.GONE);
                    ll_model.setVisibility(View.GONE);

                    card_nextfollow.setVisibility(View.VISIBLE);
                    card_dop.setVisibility(View.VISIBLE);


                }
                else if(followupdonestatus.equals("Closed")) {

                    card_nextfollow.setVisibility(View.GONE);
                    card_dop.setVisibility(View.GONE);

                    ll_clouserreson.setVisibility(View.VISIBLE);
                    ll_make.setVisibility(View.VISIBLE);
                    ll_model.setVisibility(View.VISIBLE);
                }



                /*-------------------------------------------------------------------------------*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                followupdonestatus = followupdonelist[0].get(0);
            }
        });

        clouserreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position==0)
                {
                    clousersubreason_spinner.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);
                }
                else {




                    clousersubreasonlist[0].clear();
                    closurereason = clouserreason_spinner.getSelectedItem().toString();
                    getclosurereason(token, clouserreason_spinner.getSelectedItem().toString());
                    clousersubreason_spinner.setVisibility(View.VISIBLE);
                    ll_clousersubreason.setVisibility(View.VISIBLE);



                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                clousersubreason_spinner.setVisibility(View.GONE);
                ll_clousersubreason.setVisibility(View.GONE);


            }
        });
        clousersubreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                closuresubreason = clousersubreason_spinner.getSelectedItem().toString();




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });
        make_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                } else {

                    make = make_spinner.getSelectedItem().toString();
                    params = new HashMap<>();
                    params.put("Make", make.toUpperCase());
                    apiController.getModel(token, params);
                    // modellist[0].add("Model");

                    modelArrayAdapter = new ArrayAdapter<String>(MissedCallActivity_ENQUIRY.this, R.layout.support_simple_spinner_dropdown_item, modellist[0]);
                    model_spinner.setAdapter(modelArrayAdapter);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                string_model = model_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        /*-------------------------------------------------------------------------------*/


       /* if(salesenquirydatalist.get(position).getNext_followup_date() != null && !salesenquirydatalist.get(position).getNext_followup_date().equals(""))
        {
            et_nextfollowupdate.setText(""+salesenquirydatalist.get(position).getNext_followup_date().replaceAll("T00:00:00",""));
        }

        if(salesenquirydatalist.get(position).getExpected_date_of_purchase() != null && !salesenquirydatalist.get(position).getExpected_date_of_purchase().equals(""))
        {
            et_nextfollowupdate.setText(""+salesenquirydatalist.get(position).getExpected_date_of_purchase().replaceAll("T00:00:00",""));
        }*/


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MissedCallActivity_ENQUIRY.this.position==dataposition) {

                    if (historyID != null && !historyID.equals("")) {

                        if (et_nextfollowupdate.getText().length() > 0) {
                            updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG),string_masterId);
                        }
                        if (expected_nextfollowupdate.getText().length() > 0) {
                            UpdateDateofPurchase(token, historyID, expecteddate_send,string_masterId);
                        }
                        if (followupstatus != null) {
                            updatecontatctstatus(token, historyID, followupstatus, getString(R.string.ENQUIRYTAG));
                        }
                        if (closurereason != null) {
                            UpdateClosureReason(token, historyID, closurereason,string_masterId);
                        }

                        if (closuresubreason != null) {
                            UpdateClosureSubReason(token, historyID, closuresubreason);
                        }

                        if (make != null) {
                            UpdateMake(token, historyID, make);
                        }
                        if (string_model != null) {
                            UpdateModel(token, historyID, string_model);
                        }

                        if (remarks.getText().length() > 0) {
                            updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG),string_masterId);
                        }
                    }

                    params = new HashMap<>();
                    params.put("HistoryId", historyID);

                    apiController.createFolloup(token, params);
                    try {
                        sleep(1000);
                        adapter_missedCall.notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    customWaitingDialog.dismiss();
                    shippingDialog.dismiss();
                    Toast.makeText(MissedCallActivity_ENQUIRY.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                }else
                {
                    if (oldhid != null && !oldhid.equals("")) {

                        if (et_nextfollowupdate.getText().length() > 0) {
                            updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                        }
                        if (expected_nextfollowupdate.getText().length() > 0) {
                            UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                        }
                        if (followupstatus != null) {
                            updatecontatctstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                        }
                        if (followupdonestatus != null) {
                            updateFollowupDoneStatus(token, oldhid, followupdonestatus);
                        }
                        if (closurereason != null) {
                            UpdateClosureReason(token, oldhid, closurereason, matserid);
                        }

                        if (closuresubreason != null) {
                            UpdateClosureSubReason(token, oldhid, closuresubreason);
                        }

                        if (make != null) {
                            UpdateMake(token, oldhid, make);
                        }
                        if (string_model != null) {
                            UpdateModel(token, oldhid, string_model);
                        }

                        if (remarks.getText().length() > 0) {
                            updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);

                        }
                    }

                    params = new HashMap<>();
                    params.put("HistoryId", oldhid);

                    apiController.createFolloup(token, params);

                    try {
                        sleep(1000);
                        adapter_missedCall.notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    customWaitingDialog.dismiss();
                    shippingDialog.dismiss();

                    Toast.makeText(MissedCallActivity_ENQUIRY.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                }

            }
        });
        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });
        /*-------------------------------------------------------------------------------*/

        clouserreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, clouserreasonlist[0]);
        clouserreason_spinner.setAdapter(clouserreasonArrayAdapter);

        makeArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, makelist[0]);
        make_spinner.setAdapter(makeArrayAdapter);

        followupArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, followuplist[0]);
        followuptype_spinner.setAdapter(followupArrayAdapter);






        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
    }

    //show customer Historydata popup
    @Override
    public void getHistoryDetails(int position) {

        getSalesHistoryData(token, misseddatalist.get(position).getId());

        final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter_sales_historydata = new Adapter_Sales_Historydata(this, salesReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sales_historydata);
        adapter_sales_historydata.notifyDataSetChanged();
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customWaitingDialog.dismiss();
            }
        });


        shippingDialog.show();

    }

    //show customer MoreDetailsData popup
    @Override
    public void getMoreDetails(int position) {
        final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_more_datapopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);

        // ID genration of More Deatils PopUp
        /*------------------------------------------------------------------------------------------*/
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        EditText sale_nextfollowupdate = shippingDialog.findViewById(R.id.sale_next_follow_date);
        EditText sale_email = shippingDialog.findViewById(R.id.sales_email_id);
        EditText sale_address = shippingDialog.findViewById(R.id.sale_address);
        EditText sale_age = shippingDialog.findViewById(R.id.sales_age);
        EditText sale_gender = shippingDialog.findViewById(R.id.sales_gender);
        EditText sale_model_intersted = shippingDialog.findViewById(R.id.sales_model_interested);
        EditText sale_exchange_required = shippingDialog.findViewById(R.id.sales_exchange_required);
        EditText finance_required = shippingDialog.findViewById(R.id.sales_fianance_required);
        EditText sale_DSE_name = shippingDialog.findViewById(R.id.sales_dse_name);
        EditText sales_employeeid = shippingDialog.findViewById(R.id.sales_DSE_Employee_ID);
        EditText sales_enquiryid = shippingDialog.findViewById(R.id.sales_Enquiry_ID);
        EditText sales_enquirysource = shippingDialog.findViewById(R.id.sales_Enquiry_source);
        EditText sales_testriderequired = shippingDialog.findViewById(R.id.sales_testriderequired);
        EditText sales_requiredtime = shippingDialog.findViewById(R.id.sales_Test_Required_Time);
        EditText sales_testridetaken = shippingDialog.findViewById(R.id.sales_testridetaken);
        EditText testridedate = shippingDialog.findViewById(R.id.sales_testridedate);
        EditText sales_leader = shippingDialog.findViewById(R.id.sales_leader);
        EditText sales_comments = shippingDialog.findViewById(R.id.sales_comments);
        EditText sales_existing_vehicle = shippingDialog.findViewById(R.id.sales_existingvehicles);
        EditText sales_awarness_resorce = shippingDialog.findViewById(R.id.sales_awerness_resorce);

        /*------------------------------------------------------------------------------------------*/


        // Vale Set  on Edit Texts
        /*------------------------------------------------------------------------------------------*/
        if (misseddatalist.get(position).getNext_followup_date() != null)
            sale_nextfollowupdate.setText("" + misseddatalist.get(position).getNext_followup_date().replaceAll("T00:00:00", "").toUpperCase());
        if (misseddatalist.get(position).getEmailid() != null)
            sale_email.setText("" + misseddatalist.get(position).getEmailid().toUpperCase());
        if (misseddatalist.get(position).getAddress() != null)
            sale_address.setText("" + misseddatalist.get(position).getAddress().toUpperCase());
        if (misseddatalist.get(position).getAge() != null)
            sale_age.setText("" + misseddatalist.get(position).getAge().toUpperCase());
        if (misseddatalist.get(position).getGender() != null)
            sale_gender.setText("" + misseddatalist.get(position).getGender().toUpperCase());
        if (misseddatalist.get(position).getModel_interested_in() != null)
            sale_model_intersted.setText("" + misseddatalist.get(position).getModel_interested_in().toUpperCase());
        if (misseddatalist.get(position).getExchange_required() != null)
            sale_exchange_required.setText("" + misseddatalist.get(position).getExchange_required().toUpperCase());
        if (misseddatalist.get(position).getFinance_required() != null)
            finance_required.setText("" + misseddatalist.get(position).getFinance_required().toUpperCase());
        if (misseddatalist.get(position).getDse_employee_id() != null)
            sales_employeeid.setText("" + misseddatalist.get(position).getDse_employee_id().toUpperCase());
        if (misseddatalist.get(position).getEnquiry_id() != null)
            sales_enquiryid.setText("" + misseddatalist.get(position).getEnquiry_id().toUpperCase());
        if (misseddatalist.get(position).getEnquiry_source() != null)
            sales_enquirysource.setText("" + misseddatalist.get(position).getEnquiry_source().toUpperCase());
        if (misseddatalist.get(position).getTest_ride_required() != null)
            sales_testriderequired.setText("" + misseddatalist.get(position).getTest_ride_required().toUpperCase());
        if (misseddatalist.get(position).getTest_ride_required_time() != null)
            sales_requiredtime.setText("" + misseddatalist.get(position).getTest_ride_required_time().toUpperCase());
        if (misseddatalist.get(position).getTest_ride_taken() != null)
            sales_testridetaken.setText("" + misseddatalist.get(position).getTest_ride_taken().toUpperCase());
        if (misseddatalist.get(position).getTest_ride_taken_time() != null)
            testridedate.setText("" + misseddatalist.get(position).getTest_ride_taken_time().toUpperCase());
        if (misseddatalist.get(position).getOpinion_leader() != null)
            sales_leader.setText("" + misseddatalist.get(position).getOpinion_leader().toUpperCase());
        if (misseddatalist.get(position).getEnquiry_comments() != null)
            sales_comments.setText("" + misseddatalist.get(position).getEnquiry_comments().toUpperCase());
        if (misseddatalist.get(position).getExisting_vehicle() != null)
            sales_existing_vehicle.setText("" + misseddatalist.get(position).getExisting_vehicle().toUpperCase());
        if (misseddatalist.get(position).getAwareness_source() != null)
            sales_awarness_resorce.setText("" + misseddatalist.get(position).getAwareness_source().toUpperCase());
        /*------------------------------------------------------------------------------------------*/


        // Click Listener
        /*------------------------------------------------------------------------------------------*/
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        /*------------------------------------------------------------------------------------------*/

        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customWaitingDialog.dismiss();
            }
        });


        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();


    }


    // Send WhatsAPP message to this function
    @Override
    public void sendWhatsappMessage(int position, String number) {

        whatsappclicked = true;
        whatsapp_number = number;
      //  Toast.makeText(this, number, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, MissedCallActivity_ENQUIRY.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(MissedCallActivity_ENQUIRY.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, CommonVariables.SALES_FOLLOW_UP);


    }

    // Send Text Message to this Function
    @Override
    public void sendtextmessage(int position, String contactnumber) {


        textmesasageclicked = true;
        whatsapp_number = contactnumber;
      //  Toast.makeText(this, contactnumber, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, MissedCallActivity_ENQUIRY.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(MissedCallActivity_ENQUIRY.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);

        getsmstemplate(token, getString(R.string.ENQUIRYTAG));


    }

    @Override
    public void getMissed(int position) {

        final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_missedcustomer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);
        /*--------------------------------ID Genration-----------------------------------------------*/
        clouserreason_spinner = shippingDialog.findViewById(R.id.closurereason);
        clousersubreason_spinner = shippingDialog.findViewById(R.id.closuresubreason);
        make_spinner = shippingDialog.findViewById(R.id.make);
        model_spinner = shippingDialog.findViewById(R.id.model);
        sp_enquiry_status = shippingDialog.findViewById(R.id.sp_enquiry_statuss);

        edit_salescuname = shippingDialog.findViewById(R.id.salescuname);
        edit_sales_enquiry_number = shippingDialog.findViewById(R.id.sales_enquiry_number);
        edit_sales_enquiry_open_date = shippingDialog.findViewById(R.id.sales_enquiry_open_date);
        edit_sale_enquiry_status = shippingDialog.findViewById(R.id.sale_enquiry_status);
        ll_userInputs = shippingDialog.findViewById(R.id.ll_userInputs);
        followuptype_spinner = shippingDialog.findViewById(R.id.followups_spinner);
        ll_make = shippingDialog.findViewById(R.id.ll_make);
        iv_clouserreson = shippingDialog.findViewById(R.id.add_updateDetails);
        iv_nextfollowup = shippingDialog.findViewById(R.id.iv_clear_nextfollowupdate);
        iv_expectednextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_expectedfollowupdate);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        expected_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupeditexpected);
        Spinner complaintcategory = shippingDialog.findViewById(R.id.closurereason);
        final LinearLayout complaintcategorysubll = shippingDialog.findViewById(R.id.ll_reasonsub);
        ll_clouserreson = shippingDialog.findViewById(R.id.ll_clouserreson);
        ll_clousersubreason = shippingDialog.findViewById(R.id.ll_reasonsub);
        EditText numbersearch = shippingDialog.findViewById(R.id.numbersearch);
        numbersearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 10) {
                    params = new HashMap<>();

                    params.put("MobileNo", "" + s.toString());
                    apiController.getEnquiryIncomingData(token, params);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        EditText sales_customername = shippingDialog.findViewById(R.id.salescuname);
        EditText sales_enquiry_number = shippingDialog.findViewById(R.id.sales_enquiry_number);
        EditText enquiry_open_date = shippingDialog.findViewById(R.id.sales_enquiry_open_date);
        EditText sales_enquiry_status = shippingDialog.findViewById(R.id.sale_enquiry_status);
        remarks = shippingDialog.findViewById(R.id.remarks);
        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);
        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);

        followupDoneArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, followupdonelist[0]);
        sp_enquiry_status.setAdapter(followupDoneArrayAdapter);
        sp_enquiry_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                followupdonestatus = sp_enquiry_status.getSelectedItem().toString();


                /*-----------------------------------Visibility Gone Code--------------------------------------------*/
                if (followupdonestatus.equals("Open"))
                {
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_make.setVisibility(View.GONE);

                  //  card_nextfollow.setVisibility(View.VISIBLE);
                   // card_dop.setVisibility(View.VISIBLE);


                }
                else if(followupdonestatus.equals("Closed")) {

                   // card_nextfollow.setVisibility(View.GONE);
                  //  card_dop.setVisibility(View.GONE);

                    ll_clouserreson.setVisibility(View.VISIBLE);
                    ll_make.setVisibility(View.VISIBLE);
                }



                /*-------------------------------------------------------------------------------*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                followupdonestatus = followupdonelist[0].get(0);
            }
        });
        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customWaitingDialog.dismiss();
            }
        });

        /*-------------------------------------------------------------------------------*/


        /*-----------------------------------Edit Text to Set Values--------------------------------------------*/
        if (misseddatalist.get(position).getCusotmer_first_and_last_name() != null)
            sales_customername.setText("" + misseddatalist.get(position).getCusotmer_first_and_last_name().toUpperCase());
        if (misseddatalist.get(position).getEnquirynumber() != null)
            sales_enquiry_number.setText("" + misseddatalist.get(position).getEnquirynumber().toUpperCase());
        if (misseddatalist.get(position).getEnquiry_open_date() != null)
            enquiry_open_date.setText("" + misseddatalist.get(position).getEnquiry_open_date().toUpperCase().replaceAll(" 00:00:00.0000000", ""));
        if (misseddatalist.get(position).getEnquiry_status() != null) {
            sales_enquiry_status.setText("" + misseddatalist.get(position).getEnquiry_status().toUpperCase());

        }
        /*-------------------------------------------------------------------------------*/


        /*----------------------------------PopUp Click Listeners---------------------------------------------*/

        ib_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog shippingDialog = new Dialog(MissedCallActivity_ENQUIRY.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.historydata_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);
                TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
                history_data_recyclerview.setHasFixedSize(true);
                history_data_recyclerview.setLayoutManager(new LinearLayoutManager(MissedCallActivity_ENQUIRY.this, RecyclerView.VERTICAL, false));
                adapter_sales_historydata = new Adapter_Sales_Historydata(MissedCallActivity_ENQUIRY.this, salesReminderHistoryDataList);
                history_data_recyclerview.setAdapter(adapter_sales_historydata);
                adapter_sales_historydata.notifyDataSetChanged();


                shippingDialog.show();

            }
        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });
        expected_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                if (dateTimeFragment == null) {
                    dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                            getString(R.string.label_datetime_dialog),
                            getString(android.R.string.ok),
                            getString(android.R.string.cancel),
                            getString(R.string.clean) // Optional
                    );
                }

                // Optionally define a timezone
                dateTimeFragment.setTimeZone(TimeZone.getDefault());

                // Init format


                final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", java.util.Locale.getDefault());
                final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());

                // Assign unmodifiable values
                dateTimeFragment.set24HoursMode(true);
                dateTimeFragment.setHighlightAMPMSelection(false);


                // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                // Define new day and month format
                try {
                    dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                }

                // Set listener for date
                // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        expected_nextfollowupdate.setText(myDateFormat.format(date));
                        expecteddate_send = myDateFormat1.format(date);

//                        if (et_bookingdate.getText().length() != 0)
//                        {
//                            contactstatus.setSelection(((ArrayAdapter<String>)contactstatus.getAdapter()).getPosition("Contacted"));
//                        }

                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                        // Do nothing
                    }

                    @Override
                    public void onNeutralButtonClick(Date date) {
                        // Optional if neutral button does'nt exists
                        expected_nextfollowupdate.setText("");
                    }
                });
                dateTimeFragment.startAtCalendarView();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                dateTimeFragment.setMinimumDateTime(cal.getTime());
                cal.setTime(new Date());
                cal.add(Calendar.MONTH, 1);
                dateTimeFragment.setMaximumDateTime(cal.getTime());
                dateTimeFragment.setDefaultDateTime(new Date());
                // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);


            }
        });
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                if (dateTimeFragment == null) {
                    dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                            getString(R.string.label_datetime_dialog),
                            getString(android.R.string.ok),
                            getString(android.R.string.cancel),
                            getString(R.string.clean) // Optional
                    );
                }

                // Optionally define a timezone
                dateTimeFragment.setTimeZone(TimeZone.getDefault());

                // Init format


                final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", java.util.Locale.getDefault());
                final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                // Assign unmodifiable values
                dateTimeFragment.set24HoursMode(true);
                dateTimeFragment.setHighlightAMPMSelection(false);


                // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                // Define new day and month format
                try {
                    dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                }

                // Set listener for date
                // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        et_nextfollowupdate.setText(myDateFormat.format(date));
                        nextfollowupdate_send = myDateFormat1.format(date);
                        if (et_nextfollowupdate.getText().length() != 0) {
                            followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition("Contacted"));
                        }
                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                        // Do nothing
                    }

                    @Override
                    public void onNeutralButtonClick(Date date) {
                        // Optional if neutral button does'nt exists
                        et_nextfollowupdate.setText("");
                    }
                });
                dateTimeFragment.startAtCalendarView();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                dateTimeFragment.setMinimumDateTime(cal.getTime());
                cal.setTime(new Date());
                cal.add(Calendar.MONTH, 1);
                dateTimeFragment.setMaximumDateTime(cal.getTime());
                dateTimeFragment.setDefaultDateTime(new Date());
                // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);


            }


        });
        iv_nextfollowup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_nextfollowupdate.setText("");
            }
        });
        iv_expectednextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expected_nextfollowupdate.setText("");
            }
        });

        /*----------------------------------------------------------------------------------------------------*/



        /*---------------------------------Spinner Selected Listener----------------------------------------------*/

        followuptype_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    followupstatus = followuptype_spinner.getSelectedItem().toString();

                } else {
                    followupstatus = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        sp_enquiry_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                if (position != 0) {
//                    followupdonestatus = followuptype_spinner.getSelectedItem().toString();
//
//                } else {
//                    followupdonestatus = null;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


        complaintcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    complaintcategorysubll.setVisibility(View.VISIBLE);
                } else {
                    complaintcategorysubll.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        clouserreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if(position>0)
                {
                    clousersubreasonlist[0].clear();
                    closurereason = clouserreason_spinner.getSelectedItem().toString();
                    getclosurereason(token, clouserreason_spinner.getSelectedItem().toString());
                    ll_clousersubreason.setVisibility(View.VISIBLE);
                }else
                {

                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        clousersubreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!clousersubreason_spinner.getSelectedItem().toString().equalsIgnoreCase("Clouser Sub Reason")) {

                    closuresubreason = clousersubreason_spinner.getSelectedItem().toString();


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        make_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position>0)
                {
                    make = make_spinner.getSelectedItem().toString();
                    params = new HashMap<>();
                    params.put("Make", make);
                    apiController.getModel(token, params);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                string_model = model_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*-------------------------------------------------------------------------------*/


       /* if(misseddatalist.get(position).getNext_followup_date() != null && !misseddatalist.get(position).getNext_followup_date().equals(""))
        {
            et_nextfollowupdate.setText(""+misseddatalist.get(position).getNext_followup_date().replaceAll("T00:00:00",""));
        }

        if(misseddatalist.get(position).getExpected_date_of_purchase() != null && !misseddatalist.get(position).getExpected_date_of_purchase().equals(""))
        {
            et_nextfollowupdate.setText(""+misseddatalist.get(position).getExpected_date_of_purchase().replaceAll("T00:00:00",""));
        }*/


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MissedCallActivity_ENQUIRY.this.position==dataposition) {


                    if (et_nextfollowupdate.getText().length() > 0) {
                        updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                    }
                    if (expected_nextfollowupdate.getText().length() > 0) {
                        UpdateDateofPurchase(token, historyID, expecteddate_send, string_masterId);
                    }
                    if (followuptype_spinner.getSelectedItem().toString() != null) {
                        updatecontatctstatus(token, historyID, followupstatus, getString(R.string.ENQUIRYTAG));
                    }
                    if (closurereason != null) {
                        UpdateClosureReason(token, historyID, closurereason, string_masterId);
                    }

                    if (closuresubreason != null) {
                        UpdateClosureSubReason(token, historyID, closuresubreason);
                    }

                    if (make != null) {
                        UpdateMake(token, historyID, make);
                    }
                    if (string_model != null) {
                        UpdateModel(token, historyID, string_model);
                    }

                    if (remarks.getText().length() > 0) {
                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                    }

                    customWaitingDialog.dismiss();
                    shippingDialog.dismiss();
                }else
                {
                    if(oldhid != null && !oldhid.equals(""))
                    if (et_nextfollowupdate.getText().length() > 0) {
                        updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                    }
                    if (expected_nextfollowupdate.getText().length() > 0) {
                        UpdateDateofPurchase(token, historyID, expecteddate_send, string_masterId);
                    }
                    if (followuptype_spinner.getSelectedItem().toString() != null) {
                        updatecontatctstatus(token, historyID, followupstatus, getString(R.string.ENQUIRYTAG));
                    }
                    if (closurereason != null) {
                        UpdateClosureReason(token, historyID, closurereason, string_masterId);
                    }

                    if (closuresubreason != null) {
                        UpdateClosureSubReason(token, historyID, closuresubreason);
                    }

                    if (make != null) {
                        UpdateMake(token, historyID, make);
                    }
                    if (string_model != null) {
                        UpdateModel(token, historyID, string_model);
                    }

                    if (remarks.getText().length() > 0) {
                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                    }

                    customWaitingDialog.dismiss();
                    shippingDialog.dismiss();
                }

            }
        });
        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });
        /*-------------------------------------------------------------------------------*/

        clouserreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, clouserreasonlist[0]);
        clouserreason_spinner.setAdapter(clouserreasonArrayAdapter);
        makeArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, makelist[0]);
        make_spinner.setAdapter(makeArrayAdapter);
        modelArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, modellist[0]);
        model_spinner.setAdapter(modelArrayAdapter);
        followupArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, followuplist[0]);
        followuptype_spinner.setAdapter(followupArrayAdapter);
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();

    }
    //Get SMS Template


    @Override
    public void holdCall(int position, String contatcnumber) {
        OngoingCall.call.hold();
    }

    @Override
    public void unHoldCall(int position, String contatcnumber) {
        OngoingCall.call.unhold();

    }


    public void getsmstemplate(String token, String templatetype) {
        templatelist.clear();
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    @Override
    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {


        //    Toast.makeText(this, "if " + templatebody, Toast.LENGTH_SHORT).show();


            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                    startActivity(sendMsg);
                    MissedCallActivity_ENQUIRY.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        } else if (textmesasageclicked == true) {
         //   Toast.makeText(this, "else " + templatebody, Toast.LENGTH_SHORT).show();


            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            startActivity(intent);
            MissedCallActivity_ENQUIRY.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }



    /*------------------------------------------------------------------------------------------------------------------------*/


    /*---------------------------------------------------Call Recording and Upload Code----------------------------------------------------------------*/
    //Recording Start Code
    public void startRecording(String number) throws IOException {
        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            audiofile = File.createTempFile(number, ".mp3", dir);
            recorder = new MediaRecorder();
            am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_IN_CALL);
            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(1411200);
            recorder.setAudioChannels(1);
            recorder.setAudioSamplingRate(88200);

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
                            am.setMode(AudioManager.MODE_IN_CALL);
//                            if (!am.isSpeakerphoneOn())
//                                am.setSpeakerphoneOn(true);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!am.isWiredHeadsetOn()) {
                thread.start();
            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (IOException e) {
                e.printStackTrace();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Stop  Recording Function
    public void stopRecording(String id) {

//        CommonVariables.recorder.stop();
//        CommonVariables.recorder.reset();
//        CommonVariables.recorder.release();
//        try {
//            if (am != null && recorder != null) {
//                am.setMode(AudioManager.MODE_NORMAL);
//                am.setStreamVolume(AudioManager.MODE_IN_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //after stopping the recorder, create the sound file and add it to media library.
        CommonVariables.wavRecorder.stopRecording();

        if (CommonVariables.audiofile != null) UploadCallRecording(token, CommonVariables.dealercode, historyID, getString(R.string.ENQUIRYTAG));

    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String dealercode, String historyid, String calltype) {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + CommonVariables.audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/wav");
        values.put(MediaStore.Audio.Media.DATA, CommonVariables.audiofile.getAbsolutePath());
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        if (newUri != null) {
            selectedPath = getPath(newUri);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
            File files = new File(selectedPath);
            RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
            RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
            RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/wav");
            RequestBody Recording = RequestBody.create(MediaType.parse("audio/wav"), files);
            RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
            MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording", files.getName(), Recording);
            apiController.UploadCallRecording(token, Dealer_Code, History_id, Content_type, Recording_file, Call_type);
        }


    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    // Get Recording File Path
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        NewDateFormat="";
        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return NewDateFormat;
    }

    public void clearcolorsharedeprefances() {
        if (arrayList_matserId.size() > 0) {
            for (int k = 0; k < arrayList_matserId.size(); k++) {
                SharedPreferences sharedPreferencesk = getSharedPreferences(arrayList_matserId.get(k), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesk.edit();
                editor.clear();
                editor.commit();


            }
        }
    }
    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }

    //load more Service reminder data
    public void moreData() {
        customWaitingDialog.show();
        if (misseddatalist.size() >= 10) getMissedData(token,"ENQUIRY",offset);
        morebtnclicked=true;
        CommonVariables.morecount=misseddatalist.size();


        adapter_missedCall.updateList(misseddatalist);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(CommonVariables.morecount <misseddatalist.size())
                {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }

            @Override
            public void onFinish() {
                if(CommonVariables.morecount <misseddatalist.size())
                {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }
        }.start();


    }
}