package com.hero.weconnect.mysmartcrm.customretrofit.Retrofit;






import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.AddHistoryModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CallProgressModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CallingStatusActiveModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CampaignModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CustomHistoryModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.DataFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.DeAllocationModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.FieldUpdateModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetSmsStatusModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetTemplateListModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.InputFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.ReportModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.SetSMSStatusModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.TokenModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.UpdateSMSTemplateModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.getCustomIncoingModel;
import com.hero.weconnect.mysmartcrm.models.VideoDataModel;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {

    // Token API  Method Interface
    @FormUrlEncoded
    @POST("token")
    Call<TokenModel> getToken(
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String granttype

    );

    // Get Custom Reminder Data API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_Calling_Data")
    Call<Custom_Model> getCustomCallingData(@Header("Authorization") String token, @Body HashMap<String, String> params);

    // Get Campaign List Data API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Campaign_List")
    Call<CampaignModel> getCampaignList(@Header("Authorization") String token, @Body HashMap<String, String> params);

    // Get Input Field List Data API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_Field_Names")
    Call<InputFieldModel> getInputList(@Header("Authorization") String token, @Body HashMap<String, String> params);



    // Get Input Field List Data API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_Field_Names")
    Call<DataFieldModel> getInputDataFieldsList(@Header("Authorization") String token, @Body HashMap<String, String> params);


    // Update SMS Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Update_SMS_Service_Status")
    Call<SetSMSStatusModel> updatesmsstatus(@Header("Authorization") String token,
                                            @Body HashMap<String, String> params);


    // Video API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/calling_api/TrainingVideoDesc")
    Call<VideoDataModel> getVideoData(@Body HashMap<String, String> params);

    // Get SMS Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_SMS_Service_Status")
    Call<GetSmsStatusModel> getsmsstatus(@Header("Authorization") String token);

    // Get SMS Template API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/SMS_Template_List")
    Call<GetTemplateListModel> getsmstemplate(@Header("Authorization") String token);



    // Add SMS Template API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Add_SMS_Template")
    Call<UpdateSMSTemplateModel> addsmstemplate(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);

    // Update SMS Template API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Edit_Template")
    Call<UpdateSMSTemplateModel> updatesmstemplate(@Header("Authorization") String token,
                                                   @Body HashMap<String, String> params);

    // Get Calling Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Login")
    Call<CallingStatusActiveModel> getActiveCalling(@Header("Authorization") String token);


    // Update Add History Add API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Add_Calling_History")
    Call<AddHistoryModel> addhistorymodel(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // Update Add History Add API Method Interface
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Update_History_Details")
    Call<FieldUpdateModel> updatefields(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_Single_Call_Data")
    Call<SingleCustomerHistoryData> getsinglecustomerhistory(@Header("Authorization") String token,
                                                             @Body HashMap<String, String> params);

    // Data Allocation API
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Deallocate_Data")
    Call<DeAllocationModel> setDeAllocation(@Header("Authorization") String token);


    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/ServiceRequest")
    Call<CreateFolloupModel> getServiceRequest(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);


    // Update Call Recording API Method
    @Multipart
    @POST("api/Calling_API/Upload_Call_Recording")
    Call<UploadRecordingModel> uploadRecording(@Header("Authorization") String token,
                                               @Part("History_Id") RequestBody historyid,
                                               @Part("ContentType") RequestBody contenttype,
                                               @Part MultipartBody.Part recording);

    //Get  SR Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/IncomingSearch")
    Call<getCustomIncoingModel> getCustomIncomingdata(@Header("Authorization") String token,
                                                      @Body HashMap<String, String> params);



    //Get  SR Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/Get_Call_History")
    Call<CustomHistoryModel> getHistory(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/GetUserCallingReport")
    Call<ReportModel> getReportingData(@Header("Authorization") String token,
                                       @Body HashMap<String, String> params);



    @Headers("Content-Type: application/json")
    @POST("api/Calling_API/GetCallProgressStatus")
    Call<CallProgressModel> getCallProgressModel(@Header("Authorization") String token,
                                                 @Body HashMap<String, String> params);













    /*


    // Contatct Status List  API Method Interface

    @Headers("Content-Type: application/json")
    @POST("list/contactstatus")
    Call<ContactStatusModel> getContactStatus(@Header("Authorization") String token

    );

    // Closure Sub  Reason List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/closuresubreason")
    Call<ClouserSubReasonModel> getClosureSubReason(@Header("Authorization") String token, @Body HashMap<String, String> params);


    // Closure  Reason List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/closurereason")
    Call<ClouserReasonModel> getClosureReason(@Header("Authorization") String token);


    // Not Coming Reason List API Method Interface

    @Headers("Content-Type: application/json")
    @POST("list/notcomingreason")
    Call<NotComingReasonModel> getNotComingReason(@Header("Authorization") String token);

    // Customer Reply List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/customerreply")
    Call<CustomerReplyModel> getCustomerReply(@Header("Authorization") String token);


    // Model List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/model")
    Call<ModelModel> getModel(@Header("Authorization") String token,
                              @Body HashMap<String, String> params);

    // Make List API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/make")
    Call<MakeModel> getMake(@Header("Authorization") String token);


    // Add Calling Histoy API Method InterFace

    @Headers("Content-Type: application/json")
    @POST("callhistory/addnew")
    Call<AddCallHistoryUpdateModel> addCallHistoryUpdate(@Header("Authorization") String token,
                                                         @Body HashMap<String, String> params);

    // Update Contact Number API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecontactchanged")
    Call<CommonModel> updatecontacts(@Header("Authorization") String token,
                                     @Body HashMap<String, String> params);

    // Update Customer Reply API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecustomerreply")
    Call<CommonModel> updatecustomerreply(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // Update Tagged SR Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatetaggedsrstatus")
    Call<CommonModel> updatetaggedsrstatus(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Booking date Time  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatebookingdate")
    Call<CommonModel> updatebookingdate(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);


    // Update Call End  Time  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecallendtime")
    Call<CommonModel> updatecallendtime(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);


    // Update Remark API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateremark")
    Call<CommonModel> updateremark(@Header("Authorization") String token,
                                   @Body HashMap<String, String> params);


    // Update Follow-up Date & Time API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupdate")
    Call<CommonModel> updatefollowupdate(@Header("Authorization") String token,
                                         @Body HashMap<String, String> params);


    // Update Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecontactstatus")
    Call<CommonModel> updatecontactstatus(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // Update Called Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecalledstatus")
    Call<CommonModel> updatecalledstatus(@Header("Authorization") String token,
                                         @Body HashMap<String, String> params);


    // Update Not Coming Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatencr")
    Call<CommonModel> updatenotconingreason(@Header("Authorization") String token,
                                            @Body HashMap<String, String> params);





    // Get Sales Enquiry Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("reminders/salesenquiryfollowup")
    Call<SalesEnquiryDataModel> getSalesEnquiryData(@Header("Authorization") String token,
                                                    @Body HashMap<String, String> params);


    // Get Service Reminder History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsrhistory")
    Call<SR_HistoryModel> getSRHistory(@Header("Authorization") String token,
                                       @Body HashMap<String, String> params);

    // Get Sales Calling History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsehistory")
    Call<Sales_HistoryModel> getSalesHistory(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);


    // Update History Id  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewsalesenquiry")
    Call<CommonModel> updatesaleshistoryId(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Date of Purchase  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatedateofpurchase")
    Call<CommonModel> updatedateofpurchase(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Closure Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosurereason")
    Call<CommonModel> updateclosurereason(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);

    // Update Closure Sub Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosuresubreason")
    Call<CommonModel> updateclosuresubreason(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);

    // Update Make API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemake")
    Call<CommonModel> updatemake(@Header("Authorization") String token,
                                 @Body HashMap<String, String> params);

    // Update Model API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemodel")
    Call<CommonModel> updatemodel(@Header("Authorization") String token,
                                  @Body HashMap<String, String> params);

    // Update Pick & Drop API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatepickndrop")
    Call<CommonModel> updatepickdrop(@Header("Authorization") String token,
                                     @Body HashMap<String, String> params);






    /





    //Get  SR Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("incoming/getsremcustomer")
    Call<Incoming_Service_ReminderModel> getSRIncomingdata(@Header("Authorization") String token,
                                                           @Body HashMap<String, String> params);

    //Get  Enquiry Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("incoming/getsenqcustomer")
    Call<Incoming_SalesModel> getEnquiryIncomingdata(@Header("Authorization") String token,
                                                     @Body HashMap<String, String> params);

    // Data Allocation API
    @Headers("Content-Type: application/json")
    @POST("reminders/deallocateserviceremidner")
    Call<CommonModel> setDeAllocation(@Header("Authorization") String token);


    // Get Missed Data Interface
    @Headers("Content-Type: application/json")
    @POST("missed/getmissedlist")
    Call<MissedDataModel_SR> getMissedData(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Get Missed Data Sales Interface
    @Headers("Content-Type: application/json")
    @POST("missed/getmissedlist")
    Call<MissedDataModel_Sales> getMissedDataSales(@Header("Authorization") String token,
                                                   @Body HashMap<String, String> params);


    // Add Missed Number Interface
    @Headers("Content-Type: application/json")
    @POST("missed/addmissedno")
    Call<CommonModel> addtMissedNumber(@Header("Authorization") String token,
                                       @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData> getsinglecustomerhistory(@Header("Authorization") String token,
                                                             @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData_Sales> getsinglecustomerhistorysales(@Header("Authorization") String token,
                                                                        @Body HashMap<String, String> params);

    // Update Add Exception  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("ex/addexception")
    Call<CommonModel> AddException(@Header("Authorization") String token,
                                   @Body HashMap<String, String> params);

    // Update Sales Follow Up Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupdone")
    Call<CommonModel> updatesalescontactstatus(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);


    // Update Sales Follow Up Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupstatus")                                                @Body HashMap<String, String> params);

    Call<CommonModel> updatesfollowupdonestatus(@Header("Authorization") String token,

    // Get Follow Up Contatct Status API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/followupdone")
    Call<FollowUpListModel> getSalesFollowupConatctList(@Header("Authorization") String token);

    // Get Follow Up Done List API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/followupstatus")
    Call<FollowUpListModel> getSalesFollowupDoneList(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("web/createfollowup")
    Call<CreateFolloupModel> setCreateFollowup(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("web/servicerequest")
    Call<CreateFolloupModel> getServiceRequest(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("web/cancelrequest")
    Call<CreateFolloupModel> CancelServiceRequest(@Header("Authorization") String token,
                                                  @Body HashMap<String, String> params);
*/

}
