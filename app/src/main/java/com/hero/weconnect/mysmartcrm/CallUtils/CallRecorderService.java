package com.hero.weconnect.mysmartcrm.CallUtils;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.github.squti.androidwaverecorder.WaveRecorder;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.microsoft.appcenter.Constants;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;

import static com.hero.weconnect.mysmartcrm.CallUtils.CallService.number;


public class CallRecorderService extends Service {

    static final String TAGS = "tntkhang";
    WaveRecorder waveRecorder;
    private boolean isStartRecordSuccess = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        CommonVariables.recorder = new MediaRecorder();
        CommonVariables.recorder.reset();

        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            CommonVariables.audiofile = File.createTempFile(CommonVariables.mobilenumber, ".wav", dir);

            waveRecorder =new WaveRecorder(CommonVariables.audiofile.getPath());
            waveRecorder.getWaveConfig().setSampleRate(44100);
          //  waveRecorder.setNoiseSuppressorActive(true);
            waveRecorder.getWaveConfig().setChannels(AudioFormat.CHANNEL_IN_MONO);
            waveRecorder.getWaveConfig().setAudioEncoding(AudioFormat.ENCODING_PCM_16BIT);



//
//
//            DataOutputStream output = null;
//            try {
//                output = new DataOutputStream(new FileOutputStream(audiofile));
//                // WAVE header
//                // see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
//                output.writeChars("RIFF"); // chunk id
//                output.writeInt((int) (36 + audiofile.length())); // chunk size
//                output.writeChars("WAVE"); // format
//                output.writeChars("fmt "); // subchunk 1 id
//                output.writeInt(16); // subchunk 1 size
//                output.writeShort((short) 1); // audio format (1 = PCM)
//                output.writeShort((short) 1); // number of channels
//                output.writeInt(44100); // sample rate
//                output.writeInt(44100 * 2); // byte rate
//                output.writeShort((short) 2); // block align
//                output.writeShort((short) 16); // bits per sample
//                output.writeChars((output) +"data"); // subchunk 2 id
//                output.writeInt((int) audiofile.length()); // subchunk 2 size
//                // Audio data (conversion big endian -> little endian)
//                short[] shorts = new short[(int) (audiofile.length() / 2)];
//                ByteBuffer.wrap(new byte[(int) audiofile.length()]).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
//                ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
//                for (short s : shorts) {
//                    bytes.putShort(s);
//                }
//                output.write(bytes.array());
//            }
//            finally {
//                if (output != null) {
//                    output.close();
//                }
//            }
//


            //  convertToByte(audiofile.getPath());

//            if (CommonVariables.audiofile == null) {
//                CommonVariables.audiofile = audiofile;
//                audiofile = null;
//            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }

//        String phoneNumber = CommonVariables.SALES_Testingnumber;
//        String outputPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/";
//       // Log.d(TAGS, "Phone number in service: " + phoneNumber);
//
//        CommonVariables.recorder.setAudioSamplingRate(320000);
//        CommonVariables.recorder.setAudioEncodingBitRate(256000);
//
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            CommonVariables.recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
//        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//            CommonVariables.recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        } else {
//            CommonVariables.recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
//        }
//
//
//        //       recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
////        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
////        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
////        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//
//        CommonVariables.recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_WB);
//        CommonVariables.recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//
////        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
////        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
////        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//
//
////        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
////        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
////        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//
//
////        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
////        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
////        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//
//        CommonVariables.recorder.setOutputFile(CommonVariables.audiofile.getPath());
//        Log.e("jayantfilesize", " " + CommonVariables.audiofile.length());
//        CommonVariables.recorder.prepare();
//        CommonVariables.recorder.start();


        try {
//            CommonVariables.recorder.prepare();
//            CommonVariables.recorder.start();
            CommonVariables.wavRecorder=waveRecorder;
            CommonVariables.wavRecorder.startRecording();

        } catch (Exception e) {
            isStartRecordSuccess = false;
            e.printStackTrace();
        }
        return START_NOT_STICKY;
    }


    public void onDestroy() {
        super.onDestroy();
        if (isStartRecordSuccess) {
            try {
                if (CommonVariables.recorder != null) {

//                    recorder.stop();
//                    recorder.reset();
//                    recorder.release();
//                    recorder = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Log.d(TAGS, "onDestroy: " + "Recording stopped");
        }


    }



    public byte[] convertToByte(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];

        for (int readNum; (readNum = fis.read(b)) != -1; ) {
            bos.write(b, 0, readNum);
        }

        byte[] bytes = bos.toByteArray();

        return bytes;
    }





}
