package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SR_Alloted_data_model extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"TotalCount":10,"username":"ASHU10375"}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * TotalCount : 10
         * username : ASHU10375
         */

        @SerializedName("TotalCount")
        private int TotalCount;
        @SerializedName("username")
        private String username;

        public int getTotalCount() {
            return TotalCount;
        }

        public void setTotalCount(int TotalCount) {
            this.TotalCount = TotalCount;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
