package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class DeAllocationModel extends SuperClassCastBean {


    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Column1":"Updated"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Column1 : Updated
         */

        @SerializedName("Column1")
        private String Column1;

        public String getColumn1() {
            return Column1;
        }

        public void setColumn1(String Column1) {
            this.Column1 = Column1;
        }
    }
}
