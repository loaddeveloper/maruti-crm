package com.hero.weconnect.mysmartcrm.models;

public class PSFAnswerModel {

    String question;
    String questionId;
    String questionType;

    public PSFAnswerModel(String question, String questionId, String questionType, String answer) {
        this.question = question;
        this.questionId = questionId;
        this.questionType = questionType;
        this.answer = answer;
    }

    String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
