package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;


import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;


public class TokenModel extends SuperClassCastBean {


    /**
     * access_token : -I6K1BzLYeThNtoDT5KIL54dQmWcSttySeeGPnZG5Rv4szHwl5nQGGxO0U7FvZKiS4rSMcQ-QTjYLM1F4U8xNAN-KXDRiO2VriWWv5n2Bfehj3kkILcB2jon2GHqr0QNF2A9rW3Dbq5tkj9GeBuVa526MBOheTfalotS_LCc6-EMLmyVWUzxCKSlaKZnO6QM-PQj-vP9EsdwWNpZi5g7YZk7Yc4OkTKbbV0W2kfG2iRGC1pP8mCVQ7PcGZKgN2N7Zrz3RIBoCnIdiFbZwLPyn6HQI2EYxIWl4GY2xNJ24HPy766DrMAbauUsn0AIVSudmnXloioNo4rqf6EW8O1CMdezD8cCW_MSCoP68nKTkC-KD2643LRwzevdUOVYbdgSJ_sWZv5Q8eIcLMU_CuDMoQ
     * token_type : bearer
     * expires_in : 86399
     */

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_in")
    private int expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }
}
