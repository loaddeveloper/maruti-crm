package com.hero.weconnect.mysmartcrm.activity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.role.RoleManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.hero.weconnect.mysmartcrm.CallUtils.CallRecorderService;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.Calling_Sales_EnquirySharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.ConnectionDetector;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.Utils.NetworkCheckerService;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SalesReminder;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Sales_Historydata;
import com.hero.weconnect.mysmartcrm.adapter.AllocationDataAdapter;
import com.hero.weconnect.mysmartcrm.adapter.AllocationDataAdapterSales;
import com.hero.weconnect.mysmartcrm.models.CallingSharedprefrencesModel;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.FilterDataSendModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.models.UserDetailsModel;
import com.hero.weconnect.mysmartcrm.receiver.PhoneStateReceiverNew;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.analytics.Analytics;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;
import static java.lang.Thread.sleep;

public class Sales_Followup_Activity extends AppCompatActivity implements ApiResponseListener,
        Adapter_SalesReminder.CustomButtonListener, AdapterTemplateShow.CustomButtonListener {
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    public static final int REQUEST_PERMISSION = 0;
    public static int calling_list_serial_no;
    public int dataposition;
    public boolean isDataInfo=false;

    MenuItem menuItem;
    public static boolean whatsupclick = false, callstuck = false, textmessageclick = false;
    public String searchfollouptype="",oldhid="",getmodel="",getsubreason="";
    Spinner followuptype,search_by, followuptype_spinner, clouserreason_spinner, clousersubreason_spinner, make_spinner,
            model_spinner, sp_enquiry_status;
    RecyclerView history_data_recyclerview, recyclerView_template;
    Adapter_SalesReminder adapter_salesReminder;
    CardView card_nextfollow, card_dop;
    EditText et_nextfollowupdate, expected_nextfollowupdate, remarks;
    public int counter = 0;
    public ArrayList<String>[] clouserreasonlist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] clousersubreasonlist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] makelist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] modellist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] followuplist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] followupdonelist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<SalesEnquiryDataModel.DataBean> salesenquirydatalist = new ArrayList<>();
    public ArrayList<SalesEnquiryDataModel.DataBean> salesenquirydatalist2 = new ArrayList<>();
    public ApiController apiController;
    public CommonSharedPref commonSharedPref;
    public String string_called_status = "", string_customername, string_masterId, string_mobileno, whatsapp_number;
    public String token = "",searchby="", offset = "0", selectedPath = "", indexposition, historyID, Callingstatus = "BUSY", callstatus, followupdonestatus, followupstatus, closurereason, closuresubreason, make, string_model,FollowUpContatctStatus="";
    public ArrayAdapter<String> clouserreasonArrayAdapter,userNameArrayAdapter, clousersubreasonArrayAdapter, makeArrayAdapter, modelArrayAdapter, followupArrayAdapter, followupDoneArrayAdapter;
    public Calling_Sales_EnquirySharedPref callingSharedPref;
    public HashMap<String, String> params = new HashMap<>();
    public ImageView iv_nextfollowup, iv_expectednextfollowupdate, iv_clouserreson;
    public int call_current_position = 0, position = 0, state, lastposition,oldposition=0;
    public NetworkCheckerService networkCheckerService;
    public ArrayList<Sales_HistoryModel.DataBean> salesReminderHistoryDataList = new ArrayList<>();
    public Adapter_Sales_Historydata adapter_sales_historydata;
    public File audiofile = null;
    public AudioManager am = null;
    public MediaRecorder recorder;
    public boolean stop = false;
    public Date currentdail, discom;
    public CustomDialog customWaitingDialog;
    public static CustomDialog SALEScustomWaitingDialog;
    public ArrayList<String> arrayList_matserId = new ArrayList<String>();
    public ArrayList<String> arrayList_customername = new ArrayList<String>();
    public ArrayList<String> arrayList_callstatus = new ArrayList<String>();
    public ArrayList<String> arrayList_mobileno = new ArrayList<String>();
    public Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    public SharedPreferences sharedPreferencescallUI;
    ImageView iv_booking, iv_nextfollowupdate;
    ArrayAdapter<String> contactArrayAdapter, customerArrayAdapter, notcomingreasonArrayAdapter;
    ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    public boolean morebtncliked = false;
    AdapterTemplateShow adapterSettingTemplet;
    public ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    Dialog Whatsapp_shippingDialog;
    public boolean whatsappclicked = false, textmesasageclicked = false,isLongPress=false;
    SingleCustomerHistoryData.DataBean singledatabean;
    public String expecteddate_send, nextfollowupdate_send, NewDateFormat = null;
    public ArrayList<SingleCustomerHistoryData_Sales.DataBean> singledatalist = new ArrayList<>();
    LinearLayout nodata_found_layout;
    Button Retry_Button;
    Intent recordService;
    //  Class Variable Declartion
    private ImageButton startcall, pausecall, endcall;
    private Toolbar toolbarsetup;
    private AppBarLayout appBarLayout;
    private LinearLayout ll_clouserreson,datalayout,ll_clousersubreason, ll_make, ll_model,ll_searchlayout,ll_index;
    private EditText indexs,indexs2,et_search;
    private RecyclerView sales_enquiry_recyclerview;
    private ImageButton draggable_view;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private LinearLayoutManager linearLayoutManager;
    public CompositeDisposable disposables = new CompositeDisposable();
    private ConnectionDetector detector;
    private DividerItemDecoration dividerItemDecoration;
    public int index = 0;
    Date date1;
    boolean indexclicked=false;
    String StringEnquiry,historyID2;
    public boolean calldone=false;
    public boolean morebtnclicked = false,start_call_button = false;
    CheckBox cb_calldone,cb_allView,cb_myView;
    public boolean isCalldoneChecked=false,isSubClouserReason=false;
    boolean PressStop_Button;
    NetworkInfo currentNetworkInfo;





    @SuppressLint("NewApi")
    public static void start(Context context, Call call) {
        context.startActivity(new Intent(context, S_R_Activity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle()));
    }

    public static String getFileSize(File file) {
        String modifiedFileSize = null;
        double fileSize = 0.0;
        if (file.isFile()) {
            fileSize = (double) file.length();//in Bytes
            if (fileSize < 1024) {
                modifiedFileSize = String.valueOf(fileSize).concat("B");
            } else if (fileSize > 1024 && fileSize < (1024 * 1024)) {
                modifiedFileSize = String.valueOf(Math.round((fileSize / 1024 * 100.0)) / 100.0).concat("KB");
            } else {
                modifiedFileSize = String.valueOf(Math.round((fileSize / (1024 * 1204) * 100.0)) / 100.0).concat("MB");
            }
        } else {
            modifiedFileSize = "Unknown";
        }

        return modifiedFileSize;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales__followup_);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        //   getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        networkCheckerService = new NetworkCheckerService(this, this);

        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        detector = new ConnectionDetector(Sales_Followup_Activity.this);
        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "Sales_Followup_Activity");
        properties.put("Role", ""+CommonVariables.role);
        Analytics.trackEvent("Sales_Followup_Activity", properties);
        apiController = new ApiController(this,this);

        commonSharedPref = new CommonSharedPref(this);


       /* if (commonSharedPref.getLoginData() != null) {

            params = new HashMap<>();
            params.put("Type", ApiConstant.SR);
            params.put("UserName", CommonVariables.UserId);
            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(), params);
            //  networkCheckerService.UnregisterChecker();

        }*/
        initView();
        DisableCallWaiting();


    }

    // Initialization of views
    private void initView() {

        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        customWaitingDialog = new CustomDialog(this);
        SALEScustomWaitingDialog = new CustomDialog(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);



        String folder_main = "Smart CRM";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        // Id Genrattion
        /*--------------------------------------------------------------------------------------------*/
        startcall = findViewById(R.id.startcall);
        pausecall = findViewById(R.id.pasusecall);
        endcall = findViewById(R.id.endcall);
        toolbarsetup = findViewById(R.id.toolbarsetup);
        datalayout = findViewById(R.id.datalayout);

        appBarLayout = findViewById(R.id.appBarLayout);
        indexs = findViewById(R.id.et_index);
        indexs2 = findViewById(R.id.et_index2);
        et_search = findViewById(R.id.et_searchbar);
        sales_enquiry_recyclerview = findViewById(R.id.sales_enquiry_recyclerview);
        draggable_view = findViewById(R.id.draggable_view);
        followuptype = findViewById(R.id.followuptype);
        search_by = findViewById(R.id.sp_searchby);
        nodata_found_layout = findViewById(R.id.nodatfound);
        Retry_Button = findViewById(R.id.refrshbutton);
        cb_calldone = findViewById(R.id.checkbox_calldone);
        cb_allView = findViewById(R.id.checkbox_allviewcb);
        cb_myView = findViewById(R.id.checkbox_myviewcb);
        ll_searchlayout = findViewById(R.id.ll_searchlayout);
        ll_index = findViewById(R.id.ll_index);

        cb_myView.setChecked(true);
        cb_myView.setEnabled(false);






        if(!CommonVariables.searchword.isEmpty())
        {
            et_search.setText(CommonVariables.searchword);
        }

        /*--------------------------------------------------------------------------------------------*/
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CommonVariables.searchword=s.toString();

                if(s.length()>0)
                {
                    indexclicked=true;
                    params = new HashMap<>();
                    params.put("Type", ApiConstant.ENQUIRY);
                    params.put("UserName", CommonVariables.userName);
                    apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                    followuptype.setEnabled(false);


                    getEnquirySearchData(""+s);
                }else
                {
                    followuptype.setEnabled(true);

                    // Toast.makeText(Sales_Followup_Activity.this, "hellow", Toast.LENGTH_SHORT).show();
//                         if(getIntent().getStringExtra("class")== null|| CommonVariables.filermodellist.size()>0 ||CommonVariables.filerenqsourcelist.size()>0)getEnquiryData(token, ""+CommonVariables.SALES_FOLLOW_UPTYPE, "0");
//

                    if(CommonVariables.filerenqsourcelist.size()>0 || CommonVariables.filermodellist.size()>0 || CommonVariables.filterexchnage.equals("Y")
                            || CommonVariables.filetfinancerequired.equals("Y")||CommonVariables.filterexchnage.equals("N")||CommonVariables.filetfinancerequired.equals("N"))
                    {
                        //Log.e("spinnerkkk22"," "+getIntent().getStringExtra("followup"));
                        params = new HashMap<>();
                        params.put("Type", ApiConstant.ENQUIRY);
                        params.put("UserName", CommonVariables.userName);
                        apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);


                        getEnquiryFilterData(token, ""+CommonVariables.SALES_FOLLOW_UPTYPE, "0",CommonVariables.userName);

                    }
                    else
                    {
                        if(indexclicked)
                        {
                            params = new HashMap<>();
                            params.put("Type", ApiConstant.ENQUIRY);
                            params.put("UserName", CommonVariables.userName);
                            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                            getEnquiryData(token,CommonVariables.SALES_FOLLOW_UPTYPE,"0",CommonVariables.userName);

                        }

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {



            }
        });
        if (common_calling_color_sharedPreferances.getPosition() == null && common_calling_color_sharedPreferances.getPosition().equals("")) {
            position = 0;
        } else {
            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
        }

        adapter_salesReminder = new Adapter_SalesReminder(this, Sales_Followup_Activity.this, salesenquirydatalist);
        adapter_salesReminder.setHasStableIds(true);
        linearLayoutManager = new LinearLayoutManager(Sales_Followup_Activity.this);
        sales_enquiry_recyclerview.setVisibility(View.VISIBLE);

        sales_enquiry_recyclerview.setLayoutManager(linearLayoutManager);
        sales_enquiry_recyclerview.setNestedScrollingEnabled(false);
        sales_enquiry_recyclerview.hasFixedSize();

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(sales_enquiry_recyclerview.getContext(), linearLayoutManager.getOrientation());
        sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
        adapter_salesReminder.setCustomButtonListner(Sales_Followup_Activity.this);



        Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
        disposables.add(disposable);
        Disposable disposable2 = OngoingCall.state
                .filter(state -> state == Call.STATE_DISCONNECTED)
                .delay(2, TimeUnit.SECONDS)
                .firstElement()
                .subscribe(this::finish);
        disposables.add(disposable2);
        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();                return;

        }

        token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
        CommonVariables.token=token;





        apiController.getClosureReason(token);
        apiController.getMake(token);
        apiController.getFollowContatctStatusList(token);
        apiController.getFollowUpDoneList(token);
        params= new HashMap<>();
        params.put("UserName",CommonVariables.UserId);
        apiController.getUserData(token,params);


        customWaitingDialog.show();
        //apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);

        if(getIntent() != null && getIntent().getStringExtra("followup") != null && getIntent().getStringExtra("followup").equals("Pending Follow-Up"))
        {
            followuptype.setSelection(1);
        }else
        {

            followuptype.setSelection(0);
        }


        cb_calldone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    // apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);

                    startActivity(new Intent(Sales_Followup_Activity.this,Sales_Followup_CallDoneActivity.class));
                    finish();

//                    if (commonSharedPref.getLoginData() != null) {
//
//                        params = new HashMap<>();
//                        params.put("Type", ApiConstant.ENQUIRY);
//                        params.put("UserName", CommonVariables.UserId);
//                        apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
//                        //  networkCheckerService.UnregisterChecker();
//                        try {
//                            sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    customWaitingDialog.show();
//                    isCalldoneChecked=true;
//                    CommonVariables.isCalldoneChecked=true;
//                    salesenquirydatalist.clear();
//                    adapter_salesReminder.notifyDataSetChanged();
//                    HashMap<String, String> params = new HashMap<>();
//                    params.put("Offset","0");
//                    params.put("FollowupType",CommonVariables.SALES_FOLLOW_UPTYPE);
//                    params.put("FilterType","All");
//                    apiController.getSalesEnquiryCallDoneData(token,params);
//                    ll_searchlayout.setVisibility(View.GONE);
//                    ll_index.setVisibility(View.GONE);
//                    endcall.setVisibility(View.GONE);
//                    pausecall.setVisibility(View.GONE);
//                    startcall.setVisibility(View.GONE);
//                    cb_myView.setVisibility(View.GONE);
//                    if(menuItem != null)menuItem.setVisible(false);
//                    toolbar.setTitle("Call Done Data");


                }
                else
                {

                    if (commonSharedPref.getLoginData() != null) {

                        params = new HashMap<>();
                        params.put("Type", ApiConstant.ENQUIRY);
                        params.put("UserName", CommonVariables.UserId);
                        apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                        //  networkCheckerService.UnregisterChecker();
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    isCalldoneChecked=false;
                    CommonVariables.isCalldoneChecked=false;

                    endcall.setVisibility(View.VISIBLE);
                    pausecall.setVisibility(View.VISIBLE);
                    startcall.setVisibility(View.VISIBLE);
                    ll_searchlayout.setVisibility(View.VISIBLE);
                    ll_index.setVisibility(View.VISIBLE);
                    cb_allView.setVisibility(View.GONE);
                    cb_myView.setVisibility(View.VISIBLE);
                    salesenquirydatalist.clear();
                    if(menuItem != null)menuItem.setVisible(true);
                    toolbar.setTitle("");

                    adapter_salesReminder.notifyDataSetChanged();
                    getEnquiryData(token,CommonVariables.SALES_FOLLOW_UPTYPE,"0",CommonVariables.userName);
                }
            }
        });
        search_by.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position)
                {
                    case 0:
                        searchby="Name";

                        et_search.setInputType(InputType.TYPE_CLASS_TEXT);
                        et_search.setFilters(new InputFilter[]{
                                new InputFilter() {
                                    public CharSequence filter(CharSequence src, int start,
                                                               int end, Spanned dst, int dstart, int dend) {
                                        if (src.equals("")) {
                                            return src;
                                        }
                                        if (src.toString().matches("[a-zA-Z ]+")) {
                                            return src;
                                        }
                                        return "";
                                    }
                                }
                        });
                        break;

                    case 1:
                        searchby="MobileNo";
                        et_search.setInputType(InputType.TYPE_CLASS_PHONE);
                        et_search.setFilters(new InputFilter[]{
                                new InputFilter() {
                                    public CharSequence filter(CharSequence src, int start,
                                                               int end, Spanned dst, int dstart, int dend) {
                                        if (src.equals("")) {
                                            return src;
                                        }
                                        if (src.toString().matches("[0-9 ]+")) {
                                            return src;
                                        }
                                        return "";
                                    }
                                }
                        });


                        break;

                    case 2:
                        searchby="Model";
                        et_search.setInputType(InputType.TYPE_CLASS_TEXT);
                        et_search.setFilters(new InputFilter[]{
                                new InputFilter() {
                                    public CharSequence filter(CharSequence src, int start,
                                                               int end, Spanned dst, int dstart, int dend) {
                                        if (src.equals("")) {
                                            return src;
                                        }
                                        if (src.toString().matches("[a-zA-Z ]+")) {
                                            return src;
                                        }
                                        return "";
                                    }
                                }
                        });

                        break;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                searchby="Name";

            }
        });
        followuptype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(isCalldoneChecked)
                {
                    String followup = followuptype.getSelectedItem().toString().trim();


                    if (followuptype.getSelectedItem().toString().equals("Today’s Follow-Up"))
                    {
                        common_calling_color_sharedPreferances.setPosition(""+0);
                        salesenquirydatalist.clear();
                        adapter_salesReminder.notifyDataSetChanged();

                        if (commonSharedPref.getLoginData() != null) {

                            params = new HashMap<>();
                            params.put("Type", ApiConstant.ENQUIRY);
                            params.put("UserName", CommonVariables.userName);
                            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                            //  networkCheckerService.UnregisterChecker();



                        }


                        if(getIntent() != null && getIntent().getStringExtra("class") != null &&  getIntent().getStringExtra("class").equals("sales"))
                        {

                            customWaitingDialog.show();

                            HashMap<String, String> params = new HashMap<>();
                            params.put("Offset","0");
                            params.put("FollowupType","Today");
                            params.put("FilterType","All");
                            apiController.getSalesEnquiryCallDoneData(token,params);
                            searchfollouptype="Today";
                            CommonVariables.SALES_FOLLOW_UPTYPE=searchfollouptype;



                        }
                        else
                        {

                            customWaitingDialog.show();

                            HashMap<String, String> params = new HashMap<>();
                            params.put("Offset","0");
                            params.put("FollowupType","Today");
                            params.put("FilterType","All");
                            apiController.getSalesEnquiryCallDoneData(token,params);
                            searchfollouptype="Today";
                            CommonVariables.SALES_FOLLOW_UPTYPE=searchfollouptype;

                            /*---------------------------------------------------------*/
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            clearCallUISharePreferances();
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            common_calling_color_sharedPreferances.clear__start_stop_status();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.clearSheer();
                            SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                            SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                            SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                            SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                            SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                            SharedPreferences.Editor editpostion = prefposition.edit();
                            SharedPreferences.Editor editceer = prefceer.edit();
                            SharedPreferences.Editor editcall = prefcall.edit();
                            editpostion.clear();
                            editcall.clear();
                            editceer.clear();
                            CallingColoreditor.clear();
                            CallingColoreditor.commit();
                            editcall.commit();
                            editceer.commit();
                            editpostion.commit();
                            clearcolorsharedeprefances();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.setPosition("0");
                            /*---------------------------------------------------------*/
                        }




                    }
                    else if (followuptype.getSelectedItem().toString().equals("Pending Follow-Up"))
                    {

                        salesenquirydatalist.clear();
                        adapter_salesReminder.notifyDataSetChanged();

                        if (commonSharedPref.getLoginData() != null) {

                            params = new HashMap<>();
                            params.put("Type", ApiConstant.ENQUIRY);
                            params.put("UserName", CommonVariables.userName);
                            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                            //  networkCheckerService.UnregisterChecker();

                        }
                        clearCallUISharePreferances();
                        clearcolorsharedeprefances();



                        if(getIntent() != null && getIntent().getStringExtra("class") != null && getIntent().getStringExtra("class").equals("sales"))
                        {


                            HashMap<String, String> params = new HashMap<>();
                            params.put("Offset","0");
                            params.put("FollowupType","Pending");
                            params.put("FilterType","All");
                            params.put("UserName",""+CommonVariables.userName);
                            apiController.getSalesEnquiryCallDoneData(token,params);
                            searchfollouptype = "Pending";
                            CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;


                        }else {
                            customWaitingDialog.show();

                            HashMap<String, String> params = new HashMap<>();
                            params.put("Offset","0");
                            params.put("FollowupType","Pending");
                            params.put("FilterType","All");
                            params.put("UserName",""+CommonVariables.userName);
                            apiController.getSalesEnquiryCallDoneData(token,params);
                            searchfollouptype = "Pending";
                            CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;

                            /*---------------------------------------------------------*/
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            clearCallUISharePreferances();
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            common_calling_color_sharedPreferances.clear__start_stop_status();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.clearSheer();
                            SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                            SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                            SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                            SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                            SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                            SharedPreferences.Editor editpostion = prefposition.edit();
                            SharedPreferences.Editor editceer = prefceer.edit();
                            SharedPreferences.Editor editcall = prefcall.edit();
                            editpostion.clear();
                            editcall.clear();
                            editceer.clear();
                            CallingColoreditor.clear();
                            CallingColoreditor.commit();
                            editcall.commit();
                            editceer.commit();
                            editpostion.commit();
                            clearcolorsharedeprefances();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.setPosition("0");
                            /*---------------------------------------------------------*/


                        }


                    }

                }
                else
                {
                    String followup = followuptype.getSelectedItem().toString().trim();
                    common_calling_color_sharedPreferances.setPosition(""+0);


                    if (followuptype.getSelectedItem().toString().equals("Today’s Follow-Up"))
                    {
                        searchfollouptype="Today";
                        CommonVariables.SALES_FOLLOW_UPTYPE=searchfollouptype;
                        salesenquirydatalist.clear();
                        adapter_salesReminder.notifyDataSetChanged();



                        if (commonSharedPref.getLoginData() != null) {

                            params = new HashMap<>();
                            params.put("Type", ApiConstant.ENQUIRY);
                            params.put("UserName", CommonVariables.UserId);
                            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                            //  networkCheckerService.UnregisterChecker();

                        }

                        if(et_search.getText().length()>0)
                        {
                            getEnquirySearchData(""+et_search.getText());
                            return;
                        }


                        if(getIntent() != null && getIntent().getStringExtra("class") != null &&  getIntent().getStringExtra("class").equals("sales"))
                        {

                            getEnquiryFilterData(token,"Today","0",CommonVariables.userName);


                        }
                        else {
                            if(et_search.getText().length()>0)
                            {
                                getEnquirySearchData(""+et_search.getText());
                                return;
                            }

                            getEnquiryData(token,CommonVariables.SALES_FOLLOW_UPTYPE,"0",CommonVariables.userName);


                            /*---------------------------------------------------------*/
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            clearCallUISharePreferances();
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            common_calling_color_sharedPreferances.clear__start_stop_status();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.clearSheer();
                            SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                            SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                            SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                            SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                            SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                            SharedPreferences.Editor editpostion = prefposition.edit();
                            SharedPreferences.Editor editceer = prefceer.edit();
                            SharedPreferences.Editor editcall = prefcall.edit();
                            editpostion.clear();
                            editcall.clear();
                            editceer.clear();
                            CallingColoreditor.clear();
                            CallingColoreditor.commit();
                            editcall.commit();
                            editceer.commit();
                            editpostion.commit();
                            clearcolorsharedeprefances();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.setPosition("0");
                            /*---------------------------------------------------------*/
                        }




                    }
                    else if (followuptype.getSelectedItem().toString().equals("Pending Follow-Up"))
                    {
                        searchfollouptype = "Pending";
                        CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;
                        salesenquirydatalist.clear();
                        adapter_salesReminder.notifyDataSetChanged();


                        if (commonSharedPref.getLoginData() != null) {

                            params = new HashMap<>();
                            params.put("Type", ApiConstant.ENQUIRY);
                            params.put("UserName", CommonVariables.UserId);
                            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                            //  networkCheckerService.UnregisterChecker();

                        }
                        if(et_search.getText().length()>0)
                        {
                            getEnquirySearchData(""+et_search.getText());
                            return;
                        }
                        clearCallUISharePreferances();
                        clearcolorsharedeprefances();



                        if(getIntent() != null && getIntent().getStringExtra("class") != null && getIntent().getStringExtra("class").equals("sales"))
                        {

                            getEnquiryFilterData(token,"Pending","0",CommonVariables.userName);


                        }else {

                            getEnquiryData(token,CommonVariables.SALES_FOLLOW_UPTYPE,"0",CommonVariables.userName);


                            /*---------------------------------------------------------*/
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            clearCallUISharePreferances();
                            common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                            common_calling_color_sharedPreferances.clear__start_stop_status();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.clearSheer();
                            SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                            SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                            SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                            SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                            SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                            SharedPreferences.Editor editpostion = prefposition.edit();
                            SharedPreferences.Editor editceer = prefceer.edit();
                            SharedPreferences.Editor editcall = prefcall.edit();
                            editpostion.clear();
                            editcall.clear();
                            editceer.clear();
                            CallingColoreditor.clear();
                            CallingColoreditor.commit();
                            editcall.commit();
                            editceer.commit();
                            editpostion.commit();
                            clearcolorsharedeprefances();
                            common_calling_color_sharedPreferances.clearposition();
                            common_calling_color_sharedPreferances.setPosition("0");
                            /*---------------------------------------------------------*/


                        }


                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                searchfollouptype="Today";
                CommonVariables.SALES_FOLLOW_UPTYPE="Today";
            }
        });
        indexs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {

                    int postionoflist = adapter_salesReminder.getItemCount() - 1;


                    int asg = Integer.parseInt(indexs.getText().toString()) - 1;

                    index = asg;



                    if (asg <= postionoflist) {
                        // ////Log.e("SizeofListMainAct","SizeofListMainAct"+postionoflist);

                    } else {
                        Toasty.warning(Sales_Followup_Activity.this, " Index Not Avialabe in list Load More Data", Toast.LENGTH_LONG).show();
                    }

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
                    sales_enquiry_recyclerview.scrollToPosition(asg);


                } catch (Exception e) {

                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        startcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detector.isInternetAvailable())
                {




                    if (salesenquirydatalist.size()==0)
                    {
                        Toast.makeText(Sales_Followup_Activity.this, "Calling Data Not Available....!!", Toast.LENGTH_SHORT).show();
                        return;

                    }
                    else
                    {

                        TelecomManager tm = (TelecomManager)getSystemService(Context.TELECOM_SERVICE);

                        if (tm == null) {
                            // whether you want to handle this is up to you really
                            throw new NullPointerException("tm == null");
                        }

                        try {
                            tm.endCall();
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                        indexs.setEnabled(false);
                        cb_allView.setEnabled(false);
                        cb_myView.setEnabled(false);
                        cb_calldone.setEnabled(false);

                        followuptype.setEnabled(false);
                        try {

                            TelecomManager systemService = getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(getPackageName())) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.hero.weconnect"));
                                Toasty.warning(Sales_Followup_Activity.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
                            } else {


                                if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

                                    Toasty.warning(Sales_Followup_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
                                } else {

                                    if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {


                                        position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                                    } else {

                                        position = Integer.parseInt(indexs.getText().toString()) - 1;

                                    }
                                    common_calling_color_sharedPreferances.setCall_start_stop_status("start");
                                    looping();

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (DashboardActivity.callsk) {
                            DashboardActivity.callsk = false;
                        }

                    }

                }
                else
                {
                    Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();

                }
            }
        });
        endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isInternetAvailable())
                {
                    PressStop_Button = true;
                    if (CommonVariables.Is_LongPress)
                    {
                        CommonVariables.Is_LongPress = false;

                        pausecall.performClick();
                        return;
                    }

                    if(indexs != null)indexs.setVisibility(View.VISIBLE);
                    if(indexs2 != null)indexs2.setVisibility(View.GONE);


                    indexs.setEnabled(true);
                    cb_allView.setEnabled(true);

                    cb_calldone.setEnabled(true);
                    followuptype.setEnabled(true);
                    if(isLongPress)
                    {
                        if(indexs != null)indexs.setText("1");
                        if(indexs2 != null)indexs2.setText("1");
                    }

                    try {


                        OngoingCall.hangup();
                        if (DashboardActivity.callsk) {
                            DashboardActivity.callsk = false;
                        }


                        if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {

                            OngoingCall.hangup();
                            CommonVariables.endcallclick = false;
                            CommonVariables.receivecall = false;
                            CommonVariables.incoming = false;
                            callstuck = true;
//                    Intent intent= new Intent(Sales_Followup_Activity.this,DashboardActivity.class);
//                    finish();
//                    startActivity(intent);
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    try {
                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            CallService.discon1();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }


                    CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long l) {



                            //  if(adapter_salesReminder != null)  adapter_salesReminder.notifyDataSetChanged();

                        }

                        @Override
                        public void onFinish() {

                            if (calldone) {

                                if (call_current_position < arrayList_matserId.size())
                                {
                                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editss = chkfss.edit();
                                    editss.putString("call", "CALL DONE");
                                    editss.commit();

                                }
                                calldone=false;

                            }
                        }
                    };
                    countDownTimer.start();
                    sales_enquiry_recyclerview.scrollToPosition(position);

                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                }
                else
                {
                    Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();
                }

            }
        });
        pausecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detector.isInternetAvailable())
                {

                    if(position >=salesenquirydatalist.size())
                    {
                        Toasty.warning(Sales_Followup_Activity.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                        return;

                    }
                    if(indexs != null)indexs.setVisibility(View.GONE);
                    if(indexs2 != null)indexs2.setVisibility(View.VISIBLE);
                    if(indexs2 != null)indexs2.setText(""+position);

                    CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long l) {




                        }

                        @Override
                        public void onFinish() {

                            if (calldone) {

                                if (call_current_position < arrayList_matserId.size()) {
                                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editss = chkfss.edit();
                                    editss.putString("call", "CALL DONE");
                                    editss.commit();
                                }

                                calldone=false;
                            }
                        }
                    };
                    countDownTimer.start();
                    sales_enquiry_recyclerview.scrollToPosition(position);

                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }
                    try {
                        OngoingCall.hangup();


                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {
                            clearCallUISharePreferances();
                            scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));

                            try {
                                int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                                common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));


                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            OngoingCall.call.disconnect();
                            if (OngoingCall.call4.size() > 0) OngoingCall.call4.get(0).disconnect();
                            looping();

   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
    startActivity(i);
    finish();*/


                        }else
                        {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                else {
                    Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();
                }


            }
        });
        Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(Sales_Followup_Activity.this, Sales_Followup_Activity.class);
                startActivity(refresh);
                finish();
            }
        });


    }    // OnclickListener Implementation

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);
        menuItem=menu.findItem(R.id.action_filter);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
        } else if (itemId == R.id.action_filter) {




            startActivity(new Intent(this,FilterActivity.class).putExtra("followup",followuptype.getSelectedItem().toString()));
            // Log.e("Spinkkkk"," "+followuptype.getSelectedItem().toString());
//            salesenquirydatalist.clear();
//            adapter_salesReminder.notifyDataSetChanged();
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    /*--------------------------------------------------------Calling Logic----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void looping() {
        if (detector.isInternetAvailable())
        {

            Callingstatus = "BUSY";
            if (common_calling_color_sharedPreferances != null && common_calling_color_sharedPreferances.getCall_start_stop_status() != null && common_calling_color_sharedPreferances.getCall_start_stop_status().equals("start")) {
                try {
                    scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                    call_current_position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());


                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                    final String icca = chkfss.getString("call", "");
                    string_called_status = arrayList_callstatus.get(call_current_position);


                    if (string_called_status.equals("CALL DONE")) {

                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                        looping();
                    } else if (icca.equals("CALL DONE")) {

                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                        looping();
                    } else {
                        if (detector.isInternetAvailable()) {


                            common_calling_color_sharedPreferances.setcurrentcallmobileno(arrayList_mobileno.get(call_current_position));
                            common_calling_color_sharedPreferances.setcallcurrentstatus("s");
                            common_calling_color_sharedPreferances.setmasterid(arrayList_matserId.get(call_current_position));
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position));
                            sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                            adapter_salesReminder.notifyDataSetChanged();


                            if (checkSelfPermission(CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {


                                if (sharedPreferencescallUI.contains("CALLUI")) {

                                    clearCallUISharePreferances();
                                }



                                String uid = "";

                                if(CommonVariables.calldelaytime>0)
                                {

                                    CountDownTimer countDownTimer = new CountDownTimer(CommonVariables.calldelaytime,1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            return;

                                        }

                                        @Override
                                        public void onFinish() {



                                            // Log.e("Mobilenumber"," "+arrayList_mobileno.get(position-1));

                                            //Uri uri = Uri.parse("tel:" + "" + CommonVariables.SALES_Testingnumber);
                                            CommonVariables.mobilenumber=arrayList_mobileno.get(position-1);

                                              Uri uri = Uri.parse("tel:" +"+91"+ ""+arrayList_mobileno.get(position-1));
                                            startActivity(new Intent(Intent.ACTION_CALL, uri));


                                        }
                                    };
                                    countDownTimer.start();


                                    position = position + 1;

                                    uid = UUID.randomUUID().toString();
                                }
                                else
                                {
                                    CountDownTimer countDownTimer = new CountDownTimer(CommonVariables.calldelaytime,1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            return;

                                        }

                                        @Override
                                        public void onFinish() {



                                            //  Log.e("Mobilenumber"," "+arrayList_mobileno.get(position));

                                           // Uri uri = Uri.parse("tel:" + "" + CommonVariables.SALES_Testingnumber);
                                            CommonVariables.mobilenumber=arrayList_mobileno.get(position);

                                             Uri uri = Uri.parse("tel:" +"+91"+ ""+arrayList_mobileno.get(position));
                                            startActivity(new Intent(Intent.ACTION_CALL, uri));


                                        }
                                    };
                                    countDownTimer.start();


                                    position = position + 1;

                                    uid = UUID.randomUUID().toString();

                                }




                                try {
                                    string_masterId = arrayList_matserId.get(call_current_position);
                                    string_mobileno = arrayList_mobileno.get(call_current_position);
                                    string_customername = arrayList_customername.get(call_current_position);

                                } catch (IndexOutOfBoundsException e) {

                                }
                                SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editsas = prefsss.edit();
                                editsas.putString(string_masterId, uid);
                                editsas.commit();

                            } else {
                                // Request permission to call
                                ActivityCompat.requestPermissions(Sales_Followup_Activity.this, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                            }
                            String uid = UUID.randomUUID().toString();


                            try {
                                string_masterId = arrayList_matserId.get(call_current_position);
                                string_mobileno = arrayList_mobileno.get(call_current_position);
                                string_customername = arrayList_customername.get(call_current_position);

                            } catch (IndexOutOfBoundsException e) {

                            }
                            SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editsas = prefsss.edit();
                            editsas.putString(string_masterId, uid);
                            editsas.commit();
                        } else {
                            Toasty.error(this, "Please connect Internet Connection", Toast.LENGTH_SHORT).show();

                            try {

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                } catch (IndexOutOfBoundsException e) {

                    e.printStackTrace();
                }
            } else {

            }
        }
        else {
            Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            endcall.performClick();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    public void updateUi(Integer state) {

        if (CommonVariables.incoming) {

            try {
                sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            CommonVariables.incoming = false;
            return;
        }

        // Set callInfo text by the state
        // a.setText(CallStateString.asString(state).toLowerCase() + "\n" + number);
        this.state = state;
        if (state == Call.STATE_DIALING) {
            calldone=false;

            if (detector.isInternetAvailable())
            {

                stop = false;
                final Handler handler = new Handler();
                currentdail = Calendar.getInstance().getTime();
                final int delay = 4000;
                if(indexs != null)indexs.setVisibility(View.GONE);
                if(indexs2 != null)indexs2.setVisibility(View.VISIBLE);
                if(indexs2 != null)indexs2.setText(""+position);


           /* handler.postDelayed(new Runnable() {
                public void run() {*/

                if (!stop) {

                    if (sharedPreferencescallUI.contains("CALLUI")) {
                    } else {
                        OngoingCall.call2.add(OngoingCall.call);
                        try {

                            recordService = new Intent(this, CallRecorderService.class);
                            startService(recordService);
//                           // startRecording(historyID);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // //Log.e("FollowUpContatctStatus","Follow"+position+"  "+historyID);


                        if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                            int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                            try {
                                string_masterId = arrayList_matserId.get(ass);
                                string_mobileno = arrayList_mobileno.get(ass);
                                string_customername = arrayList_customername.get(ass);
                            } catch (IndexOutOfBoundsException e) {

                                e.printStackTrace();
                            }

                        }
                        if(followuptype_spinner != null && position==dataposition) {

                            if(followuptype_spinner != null) followuptype_spinner.setSelection(0);
                            if(followuptype_spinner != null) followuptype_spinner.setEnabled(true);

                        }
                        followupstatus="not_contacted";
                        Callingstatus = "BUSY";
                        SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        historyID = chkfss2.getString(string_masterId, "");
                        AddHistoryId(token, string_masterId, historyID);



                        updatecalledstatus(token, historyID, Callingstatus , CommonVariables.SALES_FOLLOW_UP);
                        updatecontactstatus(token, historyID, "not_contacted", getString(R.string.ENQUIRYTAG));
                        updateFolloupDoneStatus(token,historyID,"Open");
                           /* try {
                                sleep(300);

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }*/
                    }
                }
            /*    }
            }, delay);*/
            }
            else
            {
                Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                endcall.performClick();

            }

        }
        else if (state == Call.STATE_ACTIVE)
        {

            calldone=true;

            Log.d("ACTIVE",""+calldone);

            if (detector.isInternetAvailable())
            {

                if(indexs != null)indexs.setVisibility(View.GONE);
                if(indexs2 != null)indexs2.setVisibility(View.VISIBLE);
                if(indexs2 != null)indexs2.setText(""+position);



                if(followuptype_spinner != null && position== dataposition) {
                    followuptype_spinner.setSelection(1);
                    followuptype_spinner.setEnabled(true);
                    // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

                }








                if (sharedPreferencescallUI.contains("CALLUI")) {


                } else {

                    Callingstatus = "CALL DONE";
                    SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editsfs = prefss.edit();
                    editsfs.putString("call", Callingstatus);
                    editsfs.commit();
                    SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    historyID = chkfss2.getString(string_masterId, "");

                    followupstatus="contacted";

                    updatecalledstatus(token, historyID, Callingstatus , CommonVariables.SALES_FOLLOW_UP);
                    updatecontactstatus(token, historyID, "contacted", getString(R.string.ENQUIRYTAG));
                    updateFolloupDoneStatus(token,historyID,"Open");

               /* try {
                    sleep(500);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
*/


                }

            }
            else {

                Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                endcall.performClick();
            }





        } else if (state == Call.STATE_DISCONNECTED && arrayList_mobileno.size()>0)
        {

            if (detector.isInternetAvailable())
            {
                if(indexs != null)indexs.setVisibility(View.VISIBLE);
                if(indexs2 != null)indexs2.setVisibility(View.GONE);



//            if(followuptype_spinner != null && position== dataposition) {
//             //   followuptype_spinner.setSelection(0);
//                followuptype_spinner.setEnabled(false);
//                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);
//
//            }

                if (CommonVariables.receivecall) adapter_salesReminder.notifyDataSetChanged();
                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                    int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                    try {
                        string_masterId = arrayList_matserId.get(ass);
                        string_mobileno = arrayList_mobileno.get(ass);
                        string_customername = arrayList_customername.get(ass);

                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();

                    }
                    SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    historyID = chkfss2.getString(string_masterId, "");

                    updatecallendtime(token, historyID, ApiConstant.ENQUIRY);
                    stopService(recordService);
                    stopRecording(historyID);
                }


                discom = Calendar.getInstance().getTime();
                try {
                    long diffInMs = discom.getTime() - currentdail.getTime();
                    long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                    if (diffInSec > 0) {
                        stop = false;
                    } else {
                        stop = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (sharedPreferencescallUI.contains("CALLUI")) {

                } else {
                    OngoingCall.call2.clear();


                    if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {

                        common_calling_color_sharedPreferances.clearposition();

                    }


                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                    try {

                        if (DashboardActivity.callsk) {
                            DashboardActivity.callsk = false;
                            /*asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) ;*/
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition())));
                        } else {
                            /* asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition())+1 ;*/
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) + 1));
                        }
                        /* common_calling_color_sharedPreferances.setPosition(String.valueOf(asgkkk))*/

                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }





                    if (PressStop_Button)
                    {

                        PressStop_Button =false;

                    }
                    else
                    {
                        SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editsfs = prefss.edit();
                        editsfs.putString("call", Callingstatus);
                        editsfs.commit();


                    }


                    looping();


                }


                adapter_salesReminder.notifyDataSetChanged();

                uiSetup(true,true,true,true,true);
            }
            else
            {
                Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                endcall.performClick();

            }

        } else if (state == Call.STATE_DISCONNECTING) {
            if (detector.isInternetAvailable())
            {
                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");

                adapter_salesReminder.notifyDataSetChanged();
                updatecallendtime(token, historyID, CommonVariables.SALES_FOLLOW_UP);
            }
            else {
                Sales_Followup_Activity.this.registerReceiver(Sales_Followup_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                endcall.performClick();
            }



        } else if (!(state == Call.STATE_ACTIVE)) {
            if (Callingstatus.equals("CALL DONE")) {

            } else {
                Callingstatus = "BUSY";

            }
        } else if (!(state == Call.STATE_RINGING)) {
        }

    }
    /*------------------------------------------------------------------------------------------------------------------------*/



    public void setIndex(int number,boolean longpress,String hid)
    {

        position=number;
        historyID= hid;
        isLongPress=longpress;

        if(indexs != null)indexs.setText(""+number);
        if(indexs2 != null)indexs2.setText(""+number);
    }






    @RequiresApi(api = Build.VERSION_CODES.M)
    private void offerReplacingDefaultDialer()
    {
//        if (Build.VERSION.SDK_INT <= 28) {
//            TelecomManager systemService = null;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                systemService = this.getSystemService(TelecomManager.class);
//            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
//                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
//                }
//            }
//        } else {
//            @SuppressLint("WrongConstant")
//            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
//            startActivityForResult(intent, 101);
//
//
//        }


        // TelecomManager systemService = null;
        if (Build.VERSION.SDK_INT <= 28) {
            TelecomManager systemService = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                systemService = this.getSystemService(TelecomManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
                }
            }
        } else {
            @SuppressLint("WrongConstant")
            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
            startActivityForResult(intent, 101);


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void finish(Integer state) {


    }

    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStop()
    {
        super.onStop();
        customWaitingDialog.dismiss();
        disposables.clear();


    }

    /*-------------------------------------------------Activity Deafult Function-----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {

        super.onStart();
        offerReplacingDefaultDialer();


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Sales_Followup_Activity.whatsupclick == false && state == Call.STATE_ACTIVE &&
                !CommonVariables.incoming && Sales_Followup_Activity.textmessageclick == false) {

            /*OngoingCall.hangup();

            startActivity(new Intent(S_R_Activity.this,DashboardActivity.class));
            // pausestate=false;
            finish();*/

            //  OngoingCall.hangup();

        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Toast.makeText(this, serviceReminderDataList.size()+"  onResume "+adapter.getItemCount(), Toast.LENGTH_SHORT).show();

        if (Sales_Followup_Activity.whatsupclick || Sales_Followup_Activity.textmessageclick) {

            Sales_Followup_Activity.whatsupclick = false;
            Sales_Followup_Activity.textmessageclick = false;
            new OngoingCall();
            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            disposables.add(disposable);
            //adapter.notifyDataSetChanged();
        }else
        {
            // new OngoingCall();
            // Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
//            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            //disposables.add(disposable);
        }

    }
    /*------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------Other Function -----------------------------------------------------------------*/


    @Override
    public void onBackPressed() {

        //  Log.e("CallStatus","   "+common_calling_color_sharedPreferances.getcallcurrentstatus());




        if(common_calling_color_sharedPreferances.getcallcurrentstatus() != null && !common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

            final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit \n Sales Follow-Up?");

            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {


                    if (state == Call.STATE_ACTIVE) {
                        OngoingCall.hangup();

                    }

                    try {


                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);

                        adapter_salesReminder.notifyDataSetChanged();
                        String packagename = commonSharedPref.getPackagesList();
                        salesenquirydatalist.clear();
                        if (Build.VERSION.SDK_INT <= 28) {

                            TelecomManager systemService = Sales_Followup_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            shippingDialog.dismiss();

                        } else {

                            TelecomManager systemService = Sales_Followup_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            //  shippingDialog.dismiss();


//                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
//                        startActivity(i);
                            shippingDialog.dismiss();

                      /*  RoleManager roleManager2 = (RoleManager) getSystemService(ROLE_SERVICE);
                        Intent intent = roleManager2.createRequestRoleIntent(RoleManager.ROLE_DIALER);

                        startActivityForResult(intent, 101);*/


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                            adapter_salesReminder.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });

            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();
        }else if(common_calling_color_sharedPreferances.getcallcurrentstatus()== null)
        {
            final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit \n Sales Follow-Up?");

            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {


                    if (state == Call.STATE_ACTIVE) {
                        OngoingCall.hangup();

                    }

                    try {


                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);

                        adapter_salesReminder.notifyDataSetChanged();
                        String packagename = commonSharedPref.getPackagesList();
                        salesenquirydatalist.clear();
                        if (Build.VERSION.SDK_INT <= 28) {

                            TelecomManager systemService = Sales_Followup_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            shippingDialog.dismiss();

                        } else {

                            TelecomManager systemService = Sales_Followup_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            //  shippingDialog.dismiss();


//                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
//                        startActivity(i);
                            shippingDialog.dismiss();

                      /*  RoleManager roleManager2 = (RoleManager) getSystemService(ROLE_SERVICE);
                        Intent intent = roleManager2.createRequestRoleIntent(RoleManager.ROLE_DIALER);

                        startActivityForResult(intent, 101);*/


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                            adapter_salesReminder.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });

            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();
        }else

        {
            Toasty.warning(Sales_Followup_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onDestroy() {


        if (commonSharedPref.getLoginData() != null && !isDataInfo) {

            params = new HashMap<>();
            params.put("Type", ApiConstant.ENQUIRY);
            params.put("UserName", CommonVariables.userName);
            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
            //  networkCheckerService.UnregisterChecker();
            disposables.dispose();

            InableCallWaiting();
            common_calling_color_sharedPreferances.setcallcurrentstatus("d");


        }
        super.onDestroy();

    }

    public void scrollmovepostion(Integer ab) {


        sales_enquiry_recyclerview.scrollToPosition(ab);

    }

    boolean isLastVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) sales_enquiry_recyclerview.getLayoutManager());
        lastposition = layoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = adapter_salesReminder.getItemCount();
        int sizes = salesenquirydatalist.size();
        return (lastposition >= numItems - 1);


    }
    /*------------------------------------------------------------------------------------------------------------------*/



    /*---------------------------------------------------API Call Methods------------------------------------------------------------*/

    public void notconn(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Sales_Followup_Activity.this);
        builder.setTitle("No internet Connection");
        builder.setCancelable(false);
        builder.setMessage("Please turn on internet connection to continue. Last call data will not be saved");

        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (detector.isInternetAvailable()) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    notconn("");
                }


            }
        });
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                try {
                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    OngoingCall.hangup();
                    sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                    adapter_salesReminder.notifyDataSetChanged();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        clearCallUISharePreferances();
                        CallService.discon1();
                        //  recyclerView.setHasFixedSize(true);
                        //   recyclerView.setLayoutManager(linearLayoutManager);
                        //recyclerView.addItemDecoration(dividerItemDecoration);
                        sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);

                        adapter_salesReminder.notifyDataSetChanged();

                      /*  Intent i = new Intent(getApplicationContext(), Dashboard.class);
                        startActivity(i);
                        finish();*/


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        AlertDialog alertDialog = builder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //
    public void clearCallUISharePreferances() {
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        SharedPreferences.Editor editss = sharedPreferencescallUI.edit();
        editss.clear();
        editss.commit();
        editss.apply();

    }

    // Get Sales Enquiry Data
    public void getEnquiryData(String token, String folllowuptype, String offset,String username) {

        //customWaitingDialog.show();
        SALEScustomWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.offset, offset);
        params.put(ApiConstant.FollowupType, folllowuptype);
        params.put("FilterType", "ALL");
        params.put("UserName", ""+username);
        apiController.getSalesEnquiryData(token, params);
    }


    // Get Sales Enquiry Filter Data
    public void getEnquiryFilterData(String token, String folllowuptype, String offset,String username) {

        //customWaitingDialog.show();
        SALEScustomWaitingDialog.show();
        FilterDataSendModel filterDataSendModel = new FilterDataSendModel();
        filterDataSendModel.setOffset(Integer.parseInt(offset));
        filterDataSendModel.setFilterType(CommonVariables.filtertype);
        filterDataSendModel.setFollowupType(folllowuptype);
        filterDataSendModel.setExchangeRequired(CommonVariables.filterexchnage);
        filterDataSendModel.setFinanceRequired(CommonVariables.filetfinancerequired);
        filterDataSendModel.setEnquirySources(CommonVariables.filerenqsourcelist);
        filterDataSendModel.setModels(CommonVariables.filermodellist);
        filterDataSendModel.setUserName(""+username);


        apiController.getFilteredData(token, filterDataSendModel);
    }





    // Get Sales Enquiry Data
    public void getEnquirySearchData( String text ) {

        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("SearchText", text);
        params.put("SearchType", searchby);
        params.put("ListType", searchfollouptype);
        apiController.getSearchSalesEnquiryData(token, params);
    }


    // Add  Sales History Id Function
    public void AddHistoryId(String token, String mastersrid, String historyId) {
        //customWaitingDialog.show();


        params = new HashMap<>();
        params.put(ApiConstant.MasterSEId, mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BoundType, getString(R.string.OUTBOUND));
        apiController.UpdateHistorySales(token, params);

    }

    // Add Date of Purchase in this Function
    public void UpdateDateofPurchase(String token, String historyId, String expecteddateofpurchase,String string_masterId ) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.ExpectedPurchaseDate, expecteddateofpurchase);
        apiController.Updatedateofpurchase(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status2", "NextFollowUp");
        edits.commit();

    }

    // Update Closure Reason in this Function
    public void UpdateClosureReason(String token, String historyId, String Reason,String string_masterId) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, Reason);
        apiController.UpdateClosureReason(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status3", "ClosureReason");
        edits.commit();

    }

    // Update Closure Sub Reason in this Function
    public void UpdateClosureSubReason(String token, String historyId, String Reason) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, Reason);
        apiController.UpdateClosureSubReason(token, params);

    }

    // Update Make in this Function
    public void UpdateMake(String token, String historyId, String make) {
        // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MakeBought, make);
        apiController.UpdateMake(token, params);

    }

    //Get Closure Reason List()
    public void getclosurereason(String token, String closurereson) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.ClosureReason, closurereson);
        apiController.getClosureSubReason(token, params);
    }

    // Update Model in this Function
    public void UpdateModel(String token, String historyId, String model) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.ModelBought, model);
        apiController.UpdateModel(token, params);

    }

    //  get Sales Reminder History Data
    public void getSalesHistoryData(String token, String number) {

        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MobileNo", number);
        params.put("Tag",getString(R.string.ENQUIRYTAG));
        apiController.getExpectedSalesHistory(token, params);
    }

    // Update Next Follow-up date in Database for this function API Code
    public void updatefollowupdate(String token, String historyId, String followdatetime, String calltype, String string_masterId ) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.FollowupDateTime, followdatetime);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateNextFollowUpdate(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "booking");
        edits.commit();
    }

    // Update Contatct Status
    public void updatecontactstatus(String token, String historyId, String contactstatus, String calltype) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatus);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateSaleFollowUpStatus(token, params);
    }

    // Update Contatct Status
    public void updateFolloupDoneStatus(String token, String historyId, String contactstatus) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatus);
        apiController.UpdateSaleFollowUpDoneStatus(token, params);
    }

    // Update Remark
    public void updateremark(String token, String historyId, String remark, String calltype,String string_masterId) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Comment, remark);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateRemark(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status1", "rmkcolor");
        edits.commit();
    }

    // Update CalledStatus
    public void updatecalledstatus(String token, String historyId, String callstatus, String calltype) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallStatus, callstatus);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCalledStatus(token, params);
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String calltype) {
        // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCallEndTime(token, params);
    }

    //Get Single Customer Sales Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String tag, String masterId) {

        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, masterId);
        params.put(ApiConstant.Tag, tag);

        apiController.getSingleCustomerHistoryData_Sales(token, params);
    }

    // OnSuccess API Response Function
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.CLOUSERREASON)) {
            ClouserReasonModel clouserReasonModel = (ClouserReasonModel) superClassCastBean;
            if (clouserReasonModel.getData() != null && clouserReasonModel.getData().size() > 0) {
                clouserreasonlist[0].add("  ");
                for (ClouserReasonModel.DataBean dataBean : clouserReasonModel.getData()) {
                    clouserreasonlist[0].add(dataBean.getReason());

                }
                customWaitingDialog.dismiss();
            }
        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customWaitingDialog.dismiss();
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
            CommonVariables.token=token;

            if (start_call_button)
            {
                start_call_button=false;

            }
            else
            {



            }
            ////Log.e("LLLLLLL  ",token);



        }
        else if (beanTag.matches(ApiConstant.CLOUSERSUBREASON)) {
            ClouserSubReasonModel clouserSubReasonModel = (ClouserSubReasonModel) superClassCastBean;
            if (clouserSubReasonModel.getData() != null && clouserSubReasonModel.getData().size() > 0) {
                isSubClouserReason=true;

                clousersubreasonlist[0].clear();
                clousersubreasonlist[0].add("  ");

                for (ClouserSubReasonModel.DataBean dataBean : clouserSubReasonModel.getData()) {
                    clousersubreasonlist[0].add(dataBean.getStatus());


                }

                clousersubreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, clousersubreasonlist[0]);
                clousersubreason_spinner.setAdapter(clousersubreasonArrayAdapter);
                for(int i=0;i<clousersubreasonlist[0].size();i++)
                {
                    if(getsubreason.equals(clousersubreasonlist[0].get(i)))
                    {
                        clousersubreason_spinner.setSelection(i);
                    }
                }
                customWaitingDialog.dismiss();

            }else
            {
                isSubClouserReason=false;
            }
        } else if (beanTag.matches(ApiConstant.MAKE)) {
            MakeModel makeModel = (MakeModel) superClassCastBean;
            if (makeModel.getData() != null && makeModel.getData().size() > 0) {
                makelist[0].add("  ");
                for (MakeModel.DataBean dataBean : makeModel.getData()) {
                    makelist[0].add(dataBean.getMake());

                }
                customWaitingDialog.dismiss();

            }

        }        else if (beanTag.matches(ApiConstant.USERDETAILSDATA)) {
            UserDetailsModel userDetailsModel = (UserDetailsModel) superClassCastBean;
            if (userDetailsModel.getData() != null) {
                for (UserDetailsModel.DataBean dataBean : userDetailsModel.getData()) {
                    if(dataBean.getCall_delay_time() != null && !dataBean.getCall_delay_time().isEmpty())
                    {
                        CommonVariables.callDelay=dataBean.getCall_delay_time().substring(dataBean.getCall_delay_time().length() -2,dataBean.getCall_delay_time().length());
                        CommonVariables.calldelaytime=1000*Integer.parseInt(CommonVariables.callDelay);

                    }else
                    {
                        CommonVariables.callDelay="0";
                    }

                }
            } else {
                customWaitingDialog.dismiss();
            }


            //      srtextviewtemplate.setText(commonModel.getData());
        }




        else if (beanTag.matches(ApiConstant.MODEL)) {
            modellist[0].clear();
            ModelModel modelModel = (ModelModel) superClassCastBean;
            if (modelModel.getData() != null && modelModel.getData().size() > 0) {
                modellist[0].add("  ");
                for (ModelModel.DataBean dataBean : modelModel.getData()) {
                    modellist[0].add(dataBean.getModel());

                }

                modelArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, modellist[0]);
                model_spinner.setAdapter(modelArrayAdapter);
                for(int i=0;i<modellist[0].size();i++)
                {
                    if(getmodel.equals(modellist[0].get(i)))
                    {
                        model_spinner.setSelection(i);
                    }
                }

                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.GETFOLLOWUPSTATUSLIST)) {
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                //followuplist[0].add("Follow-UP");
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followuplist[0].clear();

                    followuplist[0].add("not_contacted");
                    followuplist[0].add("contacted");

                }
                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.GETFOLLOWUPDONELIST)) {
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                //  followupdonelist[0].add("Open");
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followupdonelist[0].add(dataBean.getStatus());

                }
                customWaitingDialog.dismiss();


            }

        } else if (beanTag.matches(ApiConstant.UPDATECALLEDSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.UPDATESALESFOLLOWUPSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();


            }

        } else if (beanTag.matches(ApiConstant.UPDATECALLENDTIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }


        }
        else if (beanTag.matches(ApiConstant.SALESENQUIRYDATA))
        {
            sales_enquiry_recyclerview.setVisibility(View.VISIBLE);

            if(salesenquirydatalist.size()==0) offset="0";
            if(morebtnclicked)
            {
                morebtnclicked=false;
            }else
            {
                salesenquirydatalist.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();

            }
            calling_list_serial_no=0;

            SalesEnquiryDataModel salesEnquiryDataModel = (SalesEnquiryDataModel) superClassCastBean;

            ////Log.e("Response", "Response" + salesEnquiryDataModel.getData().toString());
//            if (salesEnquiryDataModel.getMessage().equals("Failed") && salesEnquiryDataModel.getData().toString().contains("No data to allocate.")) {
//                customWaitingDialog.dismiss();
//                if(salesenquirydatalist.size()==0) {
//                    salesenquirydatalist.clear();
//                    adapter_salesReminder.notifyDataSetChanged();
//                    nodata_found_layout.setVisibility(View.VISIBLE);
//
//                }else

            if(salesEnquiryDataModel.getMessage().equals("Failed") && salesEnquiryDataModel.getData().size()>0
                    && Integer.parseInt(salesEnquiryDataModel.getData().get(0).getDataCount())==0 )
            {
                customWaitingDialog.dismiss();
                SALEScustomWaitingDialog.dismiss();
                if(salesenquirydatalist.size()==0) {
                    salesenquirydatalist.clear();
                    adapter_salesReminder.notifyDataSetChanged();
                    nodata_found_layout.setVisibility(View.VISIBLE);
                    Retry_Button.setVisibility(View.GONE);

                }
            }
            else if(salesEnquiryDataModel.getMessage().equals("Failed") && salesEnquiryDataModel.getData().size()>0)
            {
                nodata_found_layout.setVisibility(View.VISIBLE);
                //datalayout.setVisibility(View.VISIBLE);
                Retry_Button.setVisibility(View.GONE);
                isDataInfo=true;


                SalesEnquiryDataModel salesEnquiryDataModel1=(SalesEnquiryDataModel) superClassCastBean;



                ArrayList<SalesEnquiryDataModel.DataBean> list= new ArrayList<>();
                for(int i=2;i<salesEnquiryDataModel1.getData().size();i++)
                {
                    list.add(salesEnquiryDataModel1.getData().get(i));
                }

                customWaitingDialog.dismiss();
                SALEScustomWaitingDialog.dismiss();

                showDataInfo(""+salesEnquiryDataModel1.getData().get(0).getDataCount(),
                        ""+salesEnquiryDataModel1.getData().get(1).getDataCount(),list);




            }

            else {
                if (salesEnquiryDataModel.getData() != null) {
                    for (SalesEnquiryDataModel.DataBean dataBean : salesEnquiryDataModel.getData()) {
                        offset = "" + dataBean.getSno();
                        salesenquirydatalist.add(dataBean);
                        calling_list_serial_no = dataBean.getSno();
                        arrayList_mobileno.add(dataBean.getMobilenumber());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_and_last_name());
                        arrayList_callstatus.add(dataBean.getCalledstatus());
                        nodata_found_layout.setVisibility(View.GONE);



                    }
                    sales_enquiry_recyclerview.setVisibility(View.VISIBLE);

                    sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                    customWaitingDialog.dismiss();
                    SALEScustomWaitingDialog.dismiss();
                    adapter_salesReminder.notifyDataSetChanged();


                }else

                {
                    customWaitingDialog.dismiss();
                    SALEScustomWaitingDialog.dismiss();

                    if(salesenquirydatalist.size()==0) {
                        nodata_found_layout.setVisibility(View.VISIBLE);
                    }else
                    {
                        nodata_found_layout.setVisibility(View.GONE);

                    }
                }

            }


        }
        else
        if (beanTag.matches(ApiConstant.SALESENQUIRYCALLDONEDATA))
        {

            if(salesenquirydatalist.size()==0) offset="0";
            if(morebtnclicked)
            {
                morebtnclicked=false;
            }else
            {
                salesenquirydatalist.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();

            }
            calling_list_serial_no=0;

            SalesEnquiryDataModel salesEnquiryDataModel = (SalesEnquiryDataModel) superClassCastBean;

            ////Log.e("Response", "Response" + salesEnquiryDataModel.getData().toString());
            if (salesEnquiryDataModel.getData().toString().contains("No data to allocate.")) {
                customWaitingDialog.dismiss();
                SALEScustomWaitingDialog.dismiss();
                if(salesenquirydatalist.size()==0) {
                    nodata_found_layout.setVisibility(View.VISIBLE);
                }else
                {
                    nodata_found_layout.setVisibility(View.GONE);

                }
            }
            else {
                if (salesEnquiryDataModel.getData() != null) {
                    for (SalesEnquiryDataModel.DataBean dataBean : salesEnquiryDataModel.getData()) {
                        offset = "" + dataBean.getSno();
                        salesenquirydatalist.add(dataBean);
                        calling_list_serial_no = dataBean.getSno();
                        arrayList_mobileno.add(dataBean.getMobilenumber());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_and_last_name());
                        arrayList_callstatus.add(dataBean.getCalledstatus());
                        nodata_found_layout.setVisibility(View.GONE);



                    }

                    ////Log.e("ArrayListIdSize", "ArrayList" + arrayList_matserId.size());
                    sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                    customWaitingDialog.dismiss();
                    SALEScustomWaitingDialog.dismiss();
                    adapter_salesReminder.notifyDataSetChanged();


                }else

                {
                    customWaitingDialog.dismiss();
                    SALEScustomWaitingDialog.dismiss();

                    if(salesenquirydatalist.size()==0) {
                        nodata_found_layout.setVisibility(View.VISIBLE);
                    }else
                    {
                        nodata_found_layout.setVisibility(View.GONE);

                    }
                }

            }


        }
        else if (beanTag.matches(ApiConstant.SALESENQUIRYDATASEARCH)) {
            sales_enquiry_recyclerview.setVisibility(View.VISIBLE);
            if(salesenquirydatalist.size()==0) offset="0";
            salesenquirydatalist.clear();
            calling_list_serial_no=0;
            arrayList_mobileno.clear();
            arrayList_callstatus.clear();
            arrayList_customername.clear();
            arrayList_matserId.clear();
            SalesEnquiryDataModel salesEnquiryDataModel = (SalesEnquiryDataModel) superClassCastBean;

            ////Log.e("Response", "Response" + salesEnquiryDataModel.getData().toString());
            if (salesEnquiryDataModel.getData().toString().contains("No data to allocate.")) {
                customWaitingDialog.dismiss();
                if(salesenquirydatalist.size()==0) {
                    nodata_found_layout.setVisibility(View.VISIBLE);
                }else
                {
                    nodata_found_layout.setVisibility(View.GONE);

                }

            }
            else if (salesEnquiryDataModel.getData().toString().equals("[]")) {
                customWaitingDialog.dismiss();
                if(salesenquirydatalist.size()==0) {
                    nodata_found_layout.setVisibility(View.VISIBLE);
                }else
                {
                    nodata_found_layout.setVisibility(View.GONE);

                }

            }
            else {
                if (salesEnquiryDataModel.getData() != null) {
                    nodata_found_layout.setVisibility(View.GONE);
                    for (SalesEnquiryDataModel.DataBean dataBean : salesEnquiryDataModel.getData()) {
                        offset = "" + dataBean.getSno();
                        salesenquirydatalist.add(dataBean);
                        calling_list_serial_no = dataBean.getSno();
                        arrayList_mobileno.add(dataBean.getMobilenumber());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_and_last_name());
                        arrayList_callstatus.add(dataBean.getCalledstatus());


                    }

                    ////Log.e("ArrayListIdSize", "ArrayList" + arrayList_matserId.size());
                    sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                    customWaitingDialog.dismiss();
                    adapter_salesReminder.notifyDataSetChanged();


                }


            }


        }



        else if (beanTag.matches(ApiConstant.SALESENQUIRYDATAFILTER))
        {
            if(morebtnclicked)
            {
                morebtnclicked=false;
            }
            else
            {
                salesenquirydatalist.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();

            }

            // Log.e("Response222", "Response" + salesenquirydatalist.size());

            calling_list_serial_no=0;

            SalesEnquiryDataModel salesEnquiryDataModel = (SalesEnquiryDataModel) superClassCastBean;

            // Log.e("Response", "Response" + salesEnquiryDataModel.getData().toString());
            if(salesEnquiryDataModel.getMessage().equals("Failed") && salesEnquiryDataModel.getData().size()>0
                    && Integer.parseInt(salesEnquiryDataModel.getData().get(0).getDataCount())==0 )
            {
                customWaitingDialog.dismiss();
                SALEScustomWaitingDialog.dismiss();
                if(salesenquirydatalist.size()==0) {
                    salesenquirydatalist.clear();
                    adapter_salesReminder.notifyDataSetChanged();
                    nodata_found_layout.setVisibility(View.VISIBLE);
                    Retry_Button.setVisibility(View.GONE);

                }
            }
            else if(salesEnquiryDataModel.getMessage().equals("Failed") && salesEnquiryDataModel.getData().size()>0)
            {
                nodata_found_layout.setVisibility(View.VISIBLE);
                //datalayout.setVisibility(View.VISIBLE);
                Retry_Button.setVisibility(View.GONE);
                isDataInfo=true;


                SalesEnquiryDataModel salesEnquiryDataModel1=(SalesEnquiryDataModel) superClassCastBean;



                ArrayList<SalesEnquiryDataModel.DataBean> list= new ArrayList<>();
                for(int i=2;i<salesEnquiryDataModel1.getData().size();i++)
                {
                    list.add(salesEnquiryDataModel1.getData().get(i));
                }

                customWaitingDialog.dismiss();
                SALEScustomWaitingDialog.dismiss();

                showDataInfo(""+salesEnquiryDataModel1.getData().get(0).getDataCount(),
                        ""+salesEnquiryDataModel1.getData().get(1).getDataCount(),list);




            }
            else {
                if (salesEnquiryDataModel.getData() != null) {
                    for (SalesEnquiryDataModel.DataBean dataBean : salesEnquiryDataModel.getData()) {
                        offset = "" + dataBean.getSno();
                        salesenquirydatalist.add(dataBean);
                        calling_list_serial_no = dataBean.getSno();
                        arrayList_mobileno.add(dataBean.getMobilenumber());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_and_last_name());
                        arrayList_callstatus.add(dataBean.getCalledstatus());


                    }

                    ////Log.e("ArrayListIdSize", "ArrayList" + arrayList_matserId.size());
                    sales_enquiry_recyclerview.setAdapter(adapter_salesReminder);
                    customWaitingDialog.dismiss();
                    SALEScustomWaitingDialog.dismiss();
                    adapter_salesReminder.notifyDataSetChanged();


                }

            }


        }



        else if (beanTag.matches(ApiConstant.UPDATESALESHISTORYID)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATEDATEOFPURCHASE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATECLOSUREREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }
        } else if (beanTag.matches(ApiConstant.UPDATECLOSURESUBREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEMAKE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.UPDATEMODEL)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.GETEXPECTEDSALESCALLINGHISTORY)) {
            Sales_HistoryModel sales_historyModel = (Sales_HistoryModel) superClassCastBean;

            if (sales_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(this, "No History Found..!", Toast.LENGTH_SHORT).show();
                customWaitingDialog.dismiss();

            }
            if (sales_historyModel.getData() != null) {

                for (Sales_HistoryModel.DataBean dataBean : sales_historyModel.getData()) {
                    salesReminderHistoryDataList.add(dataBean);

                }
                customWaitingDialog.dismiss();
                adapter_sales_historydata.notifyDataSetChanged();
            }


        }
        else if (beanTag.matches(ApiConstant.UPLOADRECORDING)) {
            UploadRecordingModel commonModel = (UploadRecordingModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
                //CommonVariables.audiofile.delete();
                CommonVariables.audiofile=null;
                customWaitingDialog.dismiss();
            }

        }
        else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
            }

        }
        else if (beanTag.matches(ApiConstant.SINGLEDATASALES)) {
            customWaitingDialog.dismiss();
            closurereason =null;
            closuresubreason = null;
            singledatalist.clear();
            SingleCustomerHistoryData_Sales singleCustomerHistoryData = (SingleCustomerHistoryData_Sales) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {
                singledatalist.addAll(singleCustomerHistoryData.getData());

                if ((singledatalist.get(0).getNextFollowupDate().length()==0)
                        && (singledatalist.get(0).getExpectedDateOfPurchase().length()==0)
                        && (singledatalist.get(0).getRemark().length()==0))
                {
                    et_nextfollowupdate.setText("");
                    expected_nextfollowupdate.setText("");
                    remarks.setText("");


                }

                FollowUpContatctStatus = singleCustomerHistoryData.getData().get(0).getFollowupDone();
                if(isCalldoneChecked)
                {
                    historyID2=singleCustomerHistoryData.getData().get(0).getId();

                }else
                {
                    oldhid=singleCustomerHistoryData.getData().get(0).getId();

                }

                remarks.setText("" + singleCustomerHistoryData.getData().get(0).getRemark());



                if(FollowUpContatctStatus != null && FollowUpContatctStatus.equals("contacted"))
                {
                    followuptype_spinner.setEnabled(false);
                    followuptype_spinner.setSelection(1);

                }else if(FollowUpContatctStatus != null && FollowUpContatctStatus.equals("not_contacted"))
                {
                    followuptype_spinner.setEnabled(false);
                    followuptype_spinner.setSelection(0);
                }
                else if(FollowUpContatctStatus == null || FollowUpContatctStatus.length()==0)
                {
                    //followuptype_spinner.setEnabled(false);
                    followuptype_spinner.setSelection(0);
                }
                if (singleCustomerHistoryData.getData().get(0).getFollowupStatus().trim().equals("Open")) {
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_make.setVisibility(View.GONE);
                    ll_model.setVisibility(View.GONE);
                    card_nextfollow.setVisibility(View.VISIBLE);
                    card_dop.setVisibility(View.VISIBLE);
                    sp_enquiry_status.setSelection(1);



                    // followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getFollowupdone()));

                    clouserreason_spinner.setSelection(((ArrayAdapter<String>) clouserreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getClosurereason()));




                } else if (singleCustomerHistoryData.getData().get(0).getFollowupStatus().trim().equals("Closed")) {

                    sp_enquiry_status.setEnabled(false);
                    card_nextfollow.setVisibility(View.GONE);
                    card_dop.setVisibility(View.GONE);
                    ll_clouserreson.setVisibility(View.VISIBLE);
                    ll_make.setVisibility(View.VISIBLE);
                    ll_model.setVisibility(View.VISIBLE);
                    sp_enquiry_status.setSelection(0);
                    followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getFollowupDone()));
                    clouserreason_spinner.setSelection(((ArrayAdapter<String>) clouserreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getClosurereason()));


                }

                if (singleCustomerHistoryData.getData().get(0).getNextFollowupDate().contains("1900-01-01T00:00:00") || singleCustomerHistoryData.getData().get(0).getNextFollowupDate().isEmpty())
                {

                    et_nextfollowupdate.setText("");
                }
                else {

                    et_nextfollowupdate.setText(dateparse2(singleCustomerHistoryData.getData().get(0).getNextFollowupDate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "").replaceAll(".0000000","")));
                }
                if (singleCustomerHistoryData.getData().get(0).getExpectedDateOfPurchase().contains("1900-01-01T00:00:00") || singleCustomerHistoryData.getData().get(0).getExpectedDateOfPurchase().isEmpty()) {

                    expected_nextfollowupdate.setText("");

                }
                else {


                    expected_nextfollowupdate.setText( dateparse2(singleCustomerHistoryData.getData().get(0).getExpectedDateOfPurchase().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "").replaceAll(".0000000","")));
                }

                if(singleCustomerHistoryData.getData().size()>0 &&  singleCustomerHistoryData.getData().get(0).getMake()!= null && singleCustomerHistoryData.getData().get(0).getMake().length()>0) {

//                    getclosurereason(token, singleCustomerHistoryData.getData().get(0).getClosurereason());
//                    ll_clousersubreason.setVisibility(View.VISIBLE);
                    getmodel=singleCustomerHistoryData.getData().get(0).getModel();
                    make_spinner.setSelection(((ArrayAdapter<String>) make_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getMake()));
                    params = new HashMap<>();
                    params.put("Make", singleCustomerHistoryData.getData().get(0).getMake());
                    apiController.getModel(token, params);

//                    //Log.e("MODEL","jay  "+singleCustomerHistoryData.getData().get(0).getModel()+" ");
//                    try {
//                        sleep(1000);
//
//                        model_spinner.setSelection(((ArrayAdapter<String>) model_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getModel()));
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }



                    if (singleCustomerHistoryData.getData().get(0).getClosurereason() != null && !singleCustomerHistoryData.getData().get(0).getClosurereason().equals("")) {
                        ll_clousersubreason.setVisibility(View.VISIBLE);
                        clousersubreason_spinner.setVisibility(View.VISIBLE);

                        getsubreason=singleCustomerHistoryData.getData().get(0).getClosureSubReason();
                        getclosurereason(token,singleCustomerHistoryData.getData().get(0).getClosurereason());

                        // clousersubreason_spinner.setSelection(((ArrayAdapter<String>) clousersubreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getClosureSubReason()));
                    }

                }

            }else
            {
                if(followuptype_spinner != null)
                {
                    // followuptype_spinner.setEnabled(false);
                    followuptype_spinner.setSelection(0);
                }

            }
        }


    }

    @Override
    public void onFailure(String msg) {
        if (msg.matches(ApiConstant.SALESENQUIRYDATA)) {
            customWaitingDialog.dismiss();
            salesenquirydatalist.clear();
            adapter_salesReminder.notifyDataSetChanged();
            if(salesenquirydatalist.size()==0) {
                nodata_found_layout.setVisibility(View.VISIBLE);
            }
            else
            {
                nodata_found_layout.setVisibility(View.GONE);

            }

        }

        else {
            customWaitingDialog.dismiss();
        }

    }

    @Override
    public void onError(String msg) {
        customWaitingDialog.dismiss();
        if(msg.equals(ApiConstant.SALESENQUIRYDATA))
        {
            salesenquirydatalist.clear();
            adapter_salesReminder.notifyDataSetChanged();
        }


        if(salesenquirydatalist.size()==0) {
            sales_enquiry_recyclerview.setVisibility(View.GONE);
            nodata_found_layout.setVisibility(View.VISIBLE);
        }else
        {
            sales_enquiry_recyclerview.setVisibility(View.VISIBLE);

            nodata_found_layout.setVisibility(View.GONE);

        }

    }

    //load more Service reminder data
    public void moreData() {
        customWaitingDialog.show();

        morebtnclicked=true;
        CommonVariables.morecount=salesenquirydatalist.size();
        if(isCalldoneChecked)
        {


            if (followuptype.getSelectedItem().toString().equals("Today’s Follow-Up")) {


                if (getIntent() != null && getIntent().getStringExtra("class") != null && getIntent().getStringExtra("class").equals("sales"))
                {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("Offset",""+offset);
                    params.put("FollowupType",""+CommonVariables.SALES_FOLLOW_UPTYPE);
                    params.put("FilterType","All");
                    apiController.getSalesEnquiryCallDoneData(token,params);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;

                } else {

                    HashMap<String, String> params = new HashMap<>();
                    params.put("Offset",""+offset);
                    params.put("FollowupType",""+CommonVariables.SALES_FOLLOW_UPTYPE);
                    params.put("FilterType","All");
                    apiController.getSalesEnquiryCallDoneData(token,params);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;
                }


            }
            else if (followuptype.getSelectedItem().toString().equals("Pending Follow-Up")) {


                if (getIntent() != null && getIntent().getStringExtra("class") != null &&
                        getIntent().getStringExtra("class").equals("sales")) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("Offset",""+offset);
                    params.put("FollowupType",""+CommonVariables.SALES_FOLLOW_UPTYPE);
                    params.put("FilterType","All");
                    apiController.getSalesEnquiryCallDoneData(token,params);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;


                } else {

                    HashMap<String, String> params = new HashMap<>();
                    params.put("Offset",""+offset);
                    params.put("FollowupType",""+CommonVariables.SALES_FOLLOW_UPTYPE);
                    params.put("FilterType","All");
                    apiController.getSalesEnquiryCallDoneData(token,params);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;


                }


            }

        }
        else {


            if (followuptype.getSelectedItem().toString().equals("Today’s Follow-Up")) {


                if (getIntent() != null && getIntent().getStringExtra("class") != null && getIntent().getStringExtra("class").equals("sales")) {
                    getEnquiryFilterData(token, "Today", offset, CommonVariables.userName);
                    searchfollouptype = "Today";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;

                } else {

                    getEnquiryData(token, CommonVariables.SALES_FOLLOW_UPTYPE, offset, CommonVariables.userName);
                    searchfollouptype = "Today";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;
                }


            }
            else if (followuptype.getSelectedItem().toString().equals("Pending Follow-Up")) {


                if (getIntent() != null && getIntent().getStringExtra("class") != null && getIntent().getStringExtra("class").equals("sales")) {
                    getEnquiryFilterData(token, "Pending", offset, CommonVariables.userName);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;


                } else {

                    getEnquiryData(token, "Pending", offset, CommonVariables.userName);
                    searchfollouptype = "Pending";
                    CommonVariables.SALES_FOLLOW_UPTYPE = searchfollouptype;


                }


            }
        }
        morebtncliked = true;
        adapter_salesReminder.updateList(salesenquirydatalist);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(CommonVariables.morecount <salesenquirydatalist.size())
                {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }

            @Override
            public void onFinish() {
                if(CommonVariables.morecount <salesenquirydatalist.size())
                {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }
        }.start();


    }

    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }

    /*------------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------Adapter Click Listeners----------------------------------------------------------------*/
//show customer Detailsdata popup
    @Override
    public void getCustomerDetails(int position, String matserid, String customername, String enuiryno, String model, String enquiryopendate, String enquirystatus, String mobileno,String calledStatus) {

        dataposition= position+1;


        final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_customer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);
        /*--------------------------------ID Genration-----------------------------------------------*/
        clouserreason_spinner = shippingDialog.findViewById(R.id.closurereason);
        clousersubreason_spinner = shippingDialog.findViewById(R.id.closuresubreason);
        make_spinner = shippingDialog.findViewById(R.id.make);
        model_spinner = shippingDialog.findViewById(R.id.model);
        sp_enquiry_status = shippingDialog.findViewById(R.id.sp_enquiry_status);
        followuptype_spinner = shippingDialog.findViewById(R.id.followups_spinner);
        followuptype_spinner.setEnabled(false);
        ll_make = shippingDialog.findViewById(R.id.makelayout);
        ll_model = shippingDialog.findViewById(R.id.modellayout);
        iv_clouserreson = shippingDialog.findViewById(R.id.add_updateDetails);
        iv_nextfollowup = shippingDialog.findViewById(R.id.iv_clear_nextfollowupdate);
        iv_expectednextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_expectedfollowupdate);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        expected_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupeditexpected);
        ll_clouserreson = shippingDialog.findViewById(R.id.ll_clouserreson);
        ll_clousersubreason = shippingDialog.findViewById(R.id.ll_reasonsub);
        card_nextfollow = shippingDialog.findViewById(R.id.nextfollowupdate);
        card_dop = shippingDialog.findViewById(R.id.dop);
        EditText sales_customername = shippingDialog.findViewById(R.id.salescuname);
        EditText sales_enquiry_number = shippingDialog.findViewById(R.id.sales_enquiry_number);
        EditText enquiry_open_date = shippingDialog.findViewById(R.id.sales_enquiry_open_date);
        EditText sales_enquiry_status = shippingDialog.findViewById(R.id.sale_enquiry_status);
        remarks = shippingDialog.findViewById(R.id.remarks);
        getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), matserid);

        followuptype_spinner.setEnabled(false);
        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);
        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);

        /*-------------------------------------------------------------------------------*/


        /*-----------------------------------Edit Text to Set Values--------------------------------------------*/
        if (salesenquirydatalist.get(position).getCusotmer_first_and_last_name() != null)
            sales_customername.setText("" + salesenquirydatalist.get(position).getCusotmer_first_and_last_name().toUpperCase());
        if (salesenquirydatalist.get(position).getEnquirynumber() != null)
            sales_enquiry_number.setText("" + salesenquirydatalist.get(position).getEnquirynumber().toUpperCase());
        if (salesenquirydatalist.get(position).getEnquiry_open_date() != null)

            // Log.e("dateparse","  "+dateparse3(salesenquirydatalist.get(position).getEnquiry_open_date()));
            enquiry_open_date.setText("" + dateparse3(salesenquirydatalist.get(position).getEnquiry_open_date()).substring(0,11));
        if (salesenquirydatalist.get(position).getEnquiry_status() != null)
            sales_enquiry_status.setText("" + salesenquirydatalist.get(position).getEnquiry_status().toUpperCase());



        /*-------------------------------------------------------------------------------*/




        /*----------------------------------PopUp Click Listeners---------------------------------------------*/

        ib_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salesReminderHistoryDataList.clear();
                getSalesHistoryData(token, salesenquirydatalist.get(position).getMobilenumber());

                final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.historydata_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);
                TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
                history_data_recyclerview.setHasFixedSize(true);
                history_data_recyclerview.setLayoutManager(new LinearLayoutManager(Sales_Followup_Activity.this, RecyclerView.VERTICAL, false));
                adapter_sales_historydata = new Adapter_Sales_Historydata(Sales_Followup_Activity.this, salesReminderHistoryDataList);
                history_data_recyclerview.setAdapter(adapter_sales_historydata);
                adapter_sales_historydata.notifyDataSetChanged();


                shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        customWaitingDialog.dismiss();
                    }
                });
                shippingDialog.show();

            }
        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });
        iv_clouserreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                if (counter % 2 != 0) {
                    iv_clouserreson.setImageResource(R.drawable.ic_baseline_remove_circle_outline_24);
                    ll_make.setVisibility(View.VISIBLE);
                    ll_clouserreson.setVisibility(View.VISIBLE);


                } else {
                    iv_clouserreson.setImageResource(R.drawable.icon_add);
                    ll_make.setVisibility(View.GONE);
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);

                }

            }
        });
        expected_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(singledatalist.size()==0)getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), matserid);

                if(singledatalist.size()==0)
                {
                    Toasty.warning(Sales_Followup_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                }
                else
                {

                    dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                    if (dateTimeFragment == null) {
                        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                getString(R.string.label_datetime_dialog),
                                getString(android.R.string.ok),
                                getString(android.R.string.cancel),
                                getString(R.string.clean) // Optional
                        );


                    }

                    if (date1 != null) {
                        dateTimeFragment.setMinimumDateTime(date1);
                    }

                    // Optionally define a timezone
                    dateTimeFragment.setTimeZone(TimeZone.getDefault());

                    // Init format


                    final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                    final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());

                    // Assign unmodifiable values
                    dateTimeFragment.set24HoursMode(true);
                    dateTimeFragment.setHighlightAMPMSelection(false);


                    // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                    // Define new day and month format
                    try {
                        dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                    } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                    }

                    // Set listener for date
                    // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                        @Override
                        public void onPositiveButtonClick(Date date) {
                            if (et_nextfollowupdate.getText().length() > 0) {

                                if (date.getTime() >= date1.getTime()) {
                                    expected_nextfollowupdate.setText(myDateFormat.format(date));
                                    expecteddate_send = myDateFormat1.format(date);
                                } else {
                                    Toast.makeText(Sales_Followup_Activity.this, "Expected date of purchase should be greater " +
                                            "than Nextfollowup date", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                expected_nextfollowupdate.setText(myDateFormat.format(date));
                                expecteddate_send = myDateFormat1.format(date);

                            }

                            if (expected_nextfollowupdate.getText().length() != 0)
                            {
                                //   followuptype_spinner.setSelection(((ArrayAdapter<String>)followuptype_spinner.getAdapter()).getPosition("contacted"));
                            }

                        }

                        @Override
                        public void onNegativeButtonClick(Date date) {
                            // Do nothing
                        }

                        @Override
                        public void onNeutralButtonClick(Date date) {
                            // Optional if neutral button does'nt exists
                            expected_nextfollowupdate.setText("");
                        }
                    });
                    dateTimeFragment.startAtCalendarView();

                    if (date1 == null) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                        dateTimeFragment.setMinimumDateTime(cal.getTime());
                        cal.setTime(new Date());
                        cal.add(Calendar.MONTH, 1);
                        dateTimeFragment.setMaximumDateTime(cal.getTime());
                        dateTimeFragment.setDefaultDateTime(new Date());
                    } else {
                        dateTimeFragment.setDefaultDateTime(date1);

                    }
                    // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                    if (dateTimeFragment.isAdded()) {
                        return;
                    } else {
                        dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                    }
                }




            }
        });
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(Sales_Followup_Activity.this, ""+calldone , Toast.LENGTH_SHORT).show();




                if(singledatalist.size()==0)getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), matserid);

                if(singledatalist.size()==0)
                {
                    Toasty.warning(Sales_Followup_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                    if (dateTimeFragment == null) {
                        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                getString(R.string.label_datetime_dialog),
                                getString(android.R.string.ok),
                                getString(android.R.string.cancel),
                                getString(R.string.clean) // Optional
                        );
                    }

                    // Optionally define a timezone
                    dateTimeFragment.setTimeZone(TimeZone.getDefault());

                    // Init format


                    final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                    final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());
                    // Assign unmodifiable values
                    dateTimeFragment.set24HoursMode(true);
                    dateTimeFragment.setHighlightAMPMSelection(false);


                    // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                    // Define new day and month format
                    try {
                        dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                    } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                    }

                    // Set listener for date
                    // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                        @Override
                        public void onPositiveButtonClick(Date date) {
                            date1 = date;
                            et_nextfollowupdate.setText(myDateFormat.format(date));
                            nextfollowupdate_send = myDateFormat1.format(date);

                            //  Log.e("newDate111 "," "+et_nextfollowupdate.getText()+"  "+nextfollowupdate_send);

                            if (et_nextfollowupdate.getText().length() != 0) {
                                //  followuptype_spinner.setSelection(((ArrayAdapter<String>) followuptype_spinner.getAdapter()).getPosition("contacted"));
                            }
                        }

                        @Override
                        public void onNegativeButtonClick(Date date) {
                            // Do nothing
                        }

                        @Override
                        public void onNeutralButtonClick(Date date) {
                            // Optional if neutral button does'nt exists
                            et_nextfollowupdate.setText("");
                        }
                    });
                    dateTimeFragment.startAtCalendarView();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    dateTimeFragment.setMinimumDateTime(cal.getTime());
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, 1);
                    dateTimeFragment.setMaximumDateTime(cal.getTime());
                    dateTimeFragment.setDefaultDateTime(new Date());
                    // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                    if (dateTimeFragment.isAdded()) {
                        return;
                    } else {
                        dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                    }


                }





            }


        });
        iv_nextfollowup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_nextfollowupdate.setText("");
            }
        });
        iv_expectednextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expected_nextfollowupdate.setText("");
            }
        });

        /*----------------------------------------------------------------------------------------------------*/



        /*---------------------------------Spinner Selected Listener----------------------------------------------*/

        followuptype_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                followupstatus = followuptype_spinner.getSelectedItem().toString();


//                if(Callingstatus.equals("BUSY"))
//                {
//                    followuptype_spinner.setSelection(1);
//                    followupstatus = followuptype_spinner.getSelectedItem().toString();
//
//                }else if(Callingstatus.equals("CALL DONE"))
//                {
//                    followuptype_spinner.setSelection(0);
//                    followupstatus = followuptype_spinner.getSelectedItem().toString();
//
//
//                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


                followupstatus = null;
            }
        });

        if(calledStatus != null && calledStatus.equals("CALL DONE"))
        {
            sp_enquiry_status.setSelection(0);
        }else
        {
            sp_enquiry_status.setSelection(1);

        }
        sp_enquiry_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                followupdonestatus = sp_enquiry_status.getSelectedItem().toString();


                /*-----------------------------------Visibility Gone Code--------------------------------------------*/


                if (followupdonestatus.equals("Open")) {
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_make.setVisibility(View.GONE);
                    ll_model.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);

                    card_nextfollow.setVisibility(View.VISIBLE);
                    card_dop.setVisibility(View.VISIBLE);


                } else if (followupdonestatus.equals("Closed") ) {

                    if(singledatalist.size()==0)getSingleCustomerSRHistoryData(token, getString(R.string.ENQUIRYTAG), matserid);

                    if(singledatalist.size()==0)
                    {
                        sp_enquiry_status.setSelection(1);
                        Toasty.warning(Sales_Followup_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {


                        card_nextfollow.setVisibility(View.GONE);
                        card_dop.setVisibility(View.GONE);
                        ll_clouserreson.setVisibility(View.VISIBLE);
                        ll_make.setVisibility(View.VISIBLE);
                        ll_model.setVisibility(View.VISIBLE);
                    }

                }
                else
                {




                    sp_enquiry_status.setSelection(1);
                    Toasty.warning(Sales_Followup_Activity.this, "Call was not contacted", Toast.LENGTH_SHORT).show();



                }

            }



            /*-------------------------------------------------------------------------------*/


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                followupdonestatus = followupdonelist[0].get(0);
            }
        });

        clouserreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                if (position == 0) {

                    clousersubreason_spinner.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);
                } else {


                    clousersubreasonlist[0].clear();
                    closurereason = clouserreason_spinner.getSelectedItem().toString();
                    getclosurereason(token, clouserreason_spinner.getSelectedItem().toString());
                    clousersubreason_spinner.setVisibility(View.VISIBLE);
                    ll_clousersubreason.setVisibility(View.VISIBLE);


                }






            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                clousersubreason_spinner.setVisibility(View.GONE);
                ll_clousersubreason.setVisibility(View.GONE);


            }
        });
        clousersubreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                //  //Log.e("Reason","Reason"+position+closurereason);

                if(position>0) {

                    try{
                        closuresubreason = clousersubreason_spinner.getSelectedItem().toString();

                    }catch (Exception e)
                    {

                    }
                }else
                {
                    closuresubreason=null;

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });
        make_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    make = make_spinner.getSelectedItem().toString();
                    params = new HashMap<>();
                    params.put("Make", make.toUpperCase());
                    // apiController.getModel(token, params);
                } else {


                    make = make_spinner.getSelectedItem().toString();
                    params = new HashMap<>();
                    params.put("Make", make.toUpperCase());
                    apiController.getModel(token, params);
                    // modellist[0].add("Model");

                    modelArrayAdapter = new ArrayAdapter<String>(Sales_Followup_Activity.this, R.layout.support_simple_spinner_dropdown_item, modellist[0]);
                    model_spinner.setAdapter(modelArrayAdapter);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if(position>0)
                {

                    string_model = model_spinner.getSelectedItem().toString();

                }else
                {
                    string_model= null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        /*-------------------------------------------------------------------------------*/


       /* if(salesenquirydatalist.get(position).getNext_followup_date() != null && !salesenquirydatalist.get(position).getNext_followup_date().equals(""))
        {
            et_nextfollowupdate.setText(""+salesenquirydatalist.get(position).getNext_followup_date().replaceAll("T00:00:00",""));
        }

        if(salesenquirydatalist.get(position).getExpected_date_of_purchase() != null && !salesenquirydatalist.get(position).getExpected_date_of_purchase().equals(""))
        {
            et_nextfollowupdate.setText(""+salesenquirydatalist.get(position).getExpected_date_of_purchase().replaceAll("T00:00:00",""));
        }*/


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.e("Historypos", "" + Sales_Followup_Activity.this.position + "  " + followupdonestatus + closurereason);

                if(isCalldoneChecked)
                {

                    if (historyID2 != null && !historyID2.isEmpty()) {

                        if (followupdonestatus.equals("Open")) {

                            if (et_nextfollowupdate.getText().length() > 0 || expected_nextfollowupdate.getText().length() > 0) {

                                if (et_nextfollowupdate.getText().length() > 0) {
                                    updatefollowupdate(token, historyID2, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);

                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status2", "NextFollowUp");
                                    edits.commit();
                                }
                                if (expected_nextfollowupdate.getText().length() > 0) {
                                    UpdateDateofPurchase(token, historyID2, expecteddate_send, string_masterId);

                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status", "booking");
                                    edits.commit();
                                }
                                if (followupstatus != null) {
                                    updatecontactstatus(token, historyID2, followupstatus, getString(R.string.ENQUIRYTAG));
                                }
                                if (followupdonestatus != null) {
                                    updateFolloupDoneStatus(token, historyID2, followupdonestatus);
                                }
                                if (closurereason != null) {
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String formattedDate = df.format(c.getTime());
                                    updatefollowupdate(token, historyID2, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                    UpdateClosureReason(token, historyID2, closurereason, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (closuresubreason != null) {
                                    UpdateClosureSubReason(token, historyID2, closuresubreason);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                    UpdateMake(token, historyID2, make);
                                }
                                if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                    UpdateModel(token, historyID2, string_model);
                                }

                                if (remarks.getText().length() > 0) {
                                    updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status1", "rmkcolor");
                                    edits.commit();
                                }


                                params = new HashMap<>();
                                params.put("HistoryId", historyID2);

                                apiController.createFolloup(token, params);
                                try {
                                    sleep(1000);
                                    adapter_salesReminder.notifyDataSetChanged();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                customWaitingDialog.dismiss();
                                shippingDialog.dismiss();
                                Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                            }
                            else if (et_nextfollowupdate.getText().length() > 0 || expected_nextfollowupdate.getText().length() > 0)
                            {
                                if (et_nextfollowupdate.getText().length() > 0) {
                                    updatefollowupdate(token, historyID2, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status2", "NextFollowUp");
                                    edits.commit();
                                }
                                if (expected_nextfollowupdate.getText().length() > 0) {
                                    UpdateDateofPurchase(token, historyID2, expecteddate_send, string_masterId);

                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status", "booking");
                                    edits.commit();
                                }
                                if (followupstatus != null) {
                                    updatecontactstatus(token, historyID2, followupstatus, getString(R.string.ENQUIRYTAG));
                                }
                                if (followupdonestatus != null) {
                                    updateFolloupDoneStatus(token, historyID2, followupdonestatus);
                                }
                                if (closurereason != null) {
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String formattedDate = df.format(c.getTime());
                                    updatefollowupdate(token, historyID2, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                    UpdateClosureReason(token, historyID2, closurereason, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (closuresubreason != null) {
                                    UpdateClosureSubReason(token, historyID2, closuresubreason);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                    UpdateMake(token, historyID2, make);
                                }
                                if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                    UpdateModel(token, historyID2, string_model);
                                }

                                if (remarks.getText().length() > 0) {
                                    updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status1", "rmkcolor");
                                    edits.commit();
                                }


                                params = new HashMap<>();
                                params.put("HistoryId", historyID2);

                                apiController.createFolloup(token, params);
                                try {
                                    sleep(1000);
                                    adapter_salesReminder.notifyDataSetChanged();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                customWaitingDialog.dismiss();
                                shippingDialog.dismiss();
                                Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                            }

                            else {
                                Toast.makeText(Sales_Followup_Activity.this, "NextFollowUp date or Expected date of Purchase is required", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if (followupdonestatus.equals("Closed")) {



                            if (closurereason != null && !closurereason.isEmpty() && closuresubreason != null && !closuresubreason.isEmpty())
                            {

                                if (et_nextfollowupdate.getText().length() > 0) {
                                    updatefollowupdate(token, historyID2, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status2", "NextFollowUp");
                                    edits.commit();
                                }
                                if (expected_nextfollowupdate.getText().length() > 0) {
                                    UpdateDateofPurchase(token, historyID2, expecteddate_send, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status", "booking");
                                    edits.commit();
                                }
                                if (followupstatus != null) {
                                    updatecontactstatus(token, historyID2, followupstatus, getString(R.string.ENQUIRYTAG));
                                }
                                if (followupdonestatus != null) {
                                    updateFolloupDoneStatus(token, historyID2, followupdonestatus);
                                }
                                if (closurereason != null) {
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String formattedDate = df.format(c.getTime());
                                    updatefollowupdate(token, historyID2, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                    UpdateClosureReason(token, historyID2, closurereason, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (closuresubreason != null) {
                                    UpdateClosureSubReason(token, historyID2, closuresubreason);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                    UpdateMake(token, historyID2, make);
                                }
                                if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                    UpdateModel(token, historyID2, string_model);
                                }

                                if (remarks.getText().length() > 0) {
                                    updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status1", "rmkcolor");
                                    edits.commit();
                                }


                                params = new HashMap<>();
                                params.put("HistoryId", historyID2);

                                apiController.createFolloup(token, params);
                                try {
                                    sleep(1000);
                                    adapter_salesReminder.notifyDataSetChanged();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                customWaitingDialog.dismiss();
                                shippingDialog.dismiss();
                                Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                            }
                            else if(closurereason != null && !closurereason.isEmpty() && closuresubreason !=null && !closuresubreason.isEmpty())
                            {

                                if (et_nextfollowupdate.getText().length() > 0) {
                                    updatefollowupdate(token, historyID2, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status2", "NextFollowUp");
                                    edits.commit();
                                }
                                if (expected_nextfollowupdate.getText().length() > 0) {
                                    UpdateDateofPurchase(token, historyID2, expecteddate_send, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status", "booking");
                                    edits.commit();
                                }
                                if (followupstatus != null) {
                                    updatecontactstatus(token, historyID2, followupstatus, getString(R.string.ENQUIRYTAG));
                                }
                                if (followupdonestatus != null) {
                                    updateFolloupDoneStatus(token, historyID2, followupdonestatus);
                                }
                                if (closurereason != null) {
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String formattedDate = df.format(c.getTime());
                                    updatefollowupdate(token, historyID2, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                    UpdateClosureReason(token, historyID2, closurereason, string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (closuresubreason == null) {
                                    UpdateClosureSubReason(token, historyID2, "");
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status3", "ClosureReason");
                                    edits.commit();
                                }

                                if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                    UpdateMake(token, historyID2, make);
                                }
                                if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                    UpdateModel(token, historyID2, string_model);
                                }

                                if (remarks.getText().length() > 0) {
                                    updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString("Status1", "rmkcolor");
                                    edits.commit();
                                }


                                params = new HashMap<>();
                                params.put("HistoryId", historyID2);

                                apiController.createFolloup(token, params);
                                try {
                                    sleep(1000);
                                    adapter_salesReminder.notifyDataSetChanged();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                customWaitingDialog.dismiss();
                                shippingDialog.dismiss();
                                Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                            }

                            else
                            {
                                if(closurereason != null && !isSubClouserReason)
                                {

                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, historyID2, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString("Status2", "NextFollowUp");
                                        edits.commit();
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, historyID2, expecteddate_send, string_masterId);
                                        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString("Status", "booking");
                                        edits.commit();
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, historyID2, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, historyID2, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, historyID2, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, historyID2, closurereason, string_masterId);
                                        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString("Status3", "ClosureReason");
                                        edits.commit();
                                    }

                                    if (closuresubreason == null) {
                                        UpdateClosureSubReason(token, historyID2, "");
                                        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString("Status3", "ClosureReason");
                                        edits.commit();
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, historyID2, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, historyID2, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString("Status1", "rmkcolor");
                                        edits.commit();
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", historyID2);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();



                                }else
                                {
                                    Toast.makeText(Sales_Followup_Activity.this, "Clouserreason and Clousersubreason required", Toast.LENGTH_SHORT).show();

                                }


                            }
                            if (remarks.getText().length() > 0){
                                updateremark(token, historyID2, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                Toast.makeText(Sales_Followup_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                                customWaitingDialog.dismiss();
                                SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                SharedPreferences.Editor edits = prefs.edit();
                                edits.putString("Status1", "rmkcolor");
                                edits.commit();
                                shippingDialog.dismiss();

                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(Sales_Followup_Activity.this, "First Start Call", Toast.LENGTH_SHORT).show();

                    }





                }





                else {
                    if (Sales_Followup_Activity.this.position == dataposition) {

                        if (historyID != null && !historyID.equals("") && !isLongPress)
                        {

                            if (followupdonestatus.equals("Open")) {

                                if (et_nextfollowupdate.getText().length() > 0 || expected_nextfollowupdate.getText().length() > 0)
                                {

                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, historyID, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, historyID, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, historyID, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, historyID, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, historyID, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, historyID, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, historyID, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, historyID, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", historyID);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                }
                                else if (et_nextfollowupdate.getText().length() == 0 || expected_nextfollowupdate.getText().length() == 0)
                                {
                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, historyID, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, historyID, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, historyID, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, historyID, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, historyID, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, historyID, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, historyID, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, historyID, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", historyID);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                }
                                else  if (remarks.getText().length() > 0)
                                {
                                    updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    Toast.makeText(Sales_Followup_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                }
                                else
                                {
                                    Toast.makeText(Sales_Followup_Activity.this, "NextFollowUp date or Expected date of Purchase is required", Toast.LENGTH_SHORT).show();

                                }

                            }
                            else if (followupdonestatus.equals("Closed"))
                            {


                                if (closurereason != null && closurereason.length() > 0 && closuresubreason != null && closuresubreason.length() > 0)
                                {

                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, historyID, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, historyID, followuptype.getSelectedItem().toString(), getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, historyID, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, historyID, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, historyID, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, historyID, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, historyID, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, historyID, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", historyID);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                                }

                                else {

                                    if (closurereason != null && !isSubClouserReason) {


                                        if (et_nextfollowupdate.getText().length() > 0) {
                                            updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                        }
                                        if (expected_nextfollowupdate.getText().length() > 0) {
                                            UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                        }
                                        if (followupstatus != null) {
                                            updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                        }
                                        if (followupdonestatus != null) {
                                            updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                        }
                                        if (closurereason != null) {
                                            UpdateClosureReason(token, oldhid, closurereason, matserid);
                                        }

                                        if (closuresubreason != null) {
                                            UpdateClosureSubReason(token, oldhid, closuresubreason);
                                        }

                                        if (make != null) {
                                            UpdateMake(token, oldhid, make);
                                        }
                                        if (string_model != null) {

                                            UpdateModel(token, oldhid, string_model);
                                        }

                                        if (remarks.getText().length() > 0) {
                                            updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                        }


                                        params = new HashMap<>();
                                        params.put("HistoryId", oldhid);

                                        apiController.createFolloup(token, params);

                                        try {
                                            sleep(1000);
                                            adapter_salesReminder.notifyDataSetChanged();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        customWaitingDialog.dismiss();
                                        shippingDialog.dismiss();

                                        Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                    }
                                    else {
                                        Toast.makeText(Sales_Followup_Activity.this, "Clouserreason and Clousersubreason required", Toast.LENGTH_SHORT).show();

                                    }


                                }
                            }
                        }
                        else if(isLongPress && !oldhid.isEmpty())
                        {

                            if (followupdonestatus.equals("Open")) {

                                if (et_nextfollowupdate.getText().length() > 0 || expected_nextfollowupdate.getText().length() > 0) {

                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, oldhid, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, oldhid, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                }
                                else if (et_nextfollowupdate.getText().length() == 0 || expected_nextfollowupdate.getText().length() == 0) {
                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, oldhid, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, oldhid, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                }
                                else  if (remarks.getText().length() > 0){
                                    updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    Toast.makeText(Sales_Followup_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                }
                                else
                                {
                                    Toast.makeText(Sales_Followup_Activity.this, "NextFollowUp date or Expected date of Purchase is required", Toast.LENGTH_SHORT).show();

                                }

                            }
                            else if (followupdonestatus.equals("Closed"))
                            {


                                if (closurereason != null && closurereason.length() > 0 && closuresubreason != null && closuresubreason.length() > 0)
                                {

                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), string_masterId);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, string_masterId);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followuptype.getSelectedItem().toString(), getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        String formattedDate = df.format(c.getTime());
                                        updatefollowupdate(token, oldhid, formattedDate, getString(R.string.ENQUIRYTAG), string_masterId);
                                        UpdateClosureReason(token, oldhid, closurereason, string_masterId);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null && make_spinner.getSelectedItemPosition() > 0) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null && model_spinner.getSelectedItemPosition() > 0) {
                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);
                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();
                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                                }

                                else {

                                    if (closurereason != null && !isSubClouserReason) {


                                        if (et_nextfollowupdate.getText().length() > 0) {
                                            updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                        }
                                        if (expected_nextfollowupdate.getText().length() > 0) {
                                            UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                        }
                                        if (followupstatus != null) {
                                            updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                        }
                                        if (followupdonestatus != null) {
                                            updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                        }
                                        if (closurereason != null) {
                                            UpdateClosureReason(token, oldhid, closurereason, matserid);
                                        }

                                        if (closuresubreason != null) {
                                            UpdateClosureSubReason(token, oldhid, closuresubreason);
                                        }

                                        if (make != null) {
                                            UpdateMake(token, oldhid, make);
                                        }
                                        if (string_model != null) {

                                            UpdateModel(token, oldhid, string_model);
                                        }

                                        if (remarks.getText().length() > 0) {
                                            updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                        }


                                        params = new HashMap<>();
                                        params.put("HistoryId", oldhid);

                                        apiController.createFolloup(token, params);

                                        try {
                                            sleep(1000);
                                            adapter_salesReminder.notifyDataSetChanged();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        customWaitingDialog.dismiss();
                                        shippingDialog.dismiss();

                                        Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                    }
                                    else {
                                        Toast.makeText(Sales_Followup_Activity.this, "Clouserreason and Clousersubreason required", Toast.LENGTH_SHORT).show();

                                    }


                                }
                            }

                        }
                        else
                        {
                            Toast.makeText(Sales_Followup_Activity.this, "First Start Call", Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {

                        if (oldhid != null && !oldhid.equals(""))
                        {


                            if (followupdonestatus.equals("Open"))
                            {

                                if (et_nextfollowupdate.getText().length() > 0 || expected_nextfollowupdate.getText().length() > 0)
                                {


                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        UpdateClosureReason(token, oldhid, closurereason, matserid);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null) {

                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);

                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                                }
                                else if (et_nextfollowupdate.getText().length() > 0 && expected_nextfollowupdate.getText().length() > 0) {
                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        UpdateClosureReason(token, oldhid, closurereason, matserid);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null) {

                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);

                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                                }
                                else  if (remarks.getText().length() > 0){
                                    updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), string_masterId);
                                    Toast.makeText(Sales_Followup_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                }
                                else {
                                    Toast.makeText(Sales_Followup_Activity.this, "NextFollowUp date or Expected date of Purchase is required", Toast.LENGTH_SHORT).show();

                                }
                            }
                            else if (followupdonestatus.equals("Closed"))
                            {
                                if (closurereason != null && closurereason.length() > 0 && closuresubreason != null && closuresubreason.length() > 0)
                                {


                                    if (et_nextfollowupdate.getText().length() > 0) {
                                        updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                    }
                                    if (expected_nextfollowupdate.getText().length() > 0) {
                                        UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                    }
                                    if (followupstatus != null) {
                                        updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                    }
                                    if (followupdonestatus != null) {
                                        updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                    }
                                    if (closurereason != null) {
                                        UpdateClosureReason(token, oldhid, closurereason, matserid);
                                    }

                                    if (closuresubreason != null) {
                                        UpdateClosureSubReason(token, oldhid, closuresubreason);
                                    }

                                    if (make != null) {
                                        UpdateMake(token, oldhid, make);
                                    }
                                    if (string_model != null) {

                                        UpdateModel(token, oldhid, string_model);
                                    }

                                    if (remarks.getText().length() > 0) {
                                        updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                    }


                                    params = new HashMap<>();
                                    params.put("HistoryId", oldhid);

                                    apiController.createFolloup(token, params);

                                    try {
                                        sleep(1000);
                                        adapter_salesReminder.notifyDataSetChanged();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    customWaitingDialog.dismiss();
                                    shippingDialog.dismiss();

                                    Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                }

                                else {
                                    if (closurereason != null && !isSubClouserReason) {



                                        if (et_nextfollowupdate.getText().length() > 0) {
                                            updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.ENQUIRYTAG), matserid);
                                        }
                                        if (expected_nextfollowupdate.getText().length() > 0) {
                                            UpdateDateofPurchase(token, oldhid, expecteddate_send, matserid);
                                        }
                                        if (followupstatus != null) {
                                            updatecontactstatus(token, oldhid, followupstatus, getString(R.string.ENQUIRYTAG));
                                        }
                                        if (followupdonestatus != null) {
                                            updateFolloupDoneStatus(token, oldhid, followupdonestatus);
                                        }
                                        if (closurereason != null) {
                                            UpdateClosureReason(token, oldhid, closurereason, matserid);
                                        }

                                        if (closuresubreason != null) {
                                            UpdateClosureSubReason(token, oldhid, closuresubreason);
                                        }

                                        if (make != null) {
                                            UpdateMake(token, oldhid, make);
                                        }
                                        if (string_model != null) {

                                            UpdateModel(token, oldhid, string_model);
                                        }

                                        if (remarks.getText().length() > 0) {
                                            updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.ENQUIRYTAG), matserid);
                                        }


                                        params = new HashMap<>();
                                        params.put("HistoryId", oldhid);

                                        apiController.createFolloup(token, params);

                                        try {
                                            sleep(1000);
                                            adapter_salesReminder.notifyDataSetChanged();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        customWaitingDialog.dismiss();
                                        shippingDialog.dismiss();

                                        Toast.makeText(Sales_Followup_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                                    }
                                    else {
                                        Toast.makeText(Sales_Followup_Activity.this, "Clouserreason and Clousersubreason required", Toast.LENGTH_SHORT).show();

                                    }

                                }

                            }

                            isLongPress= false;
                        }else
                        {

                            Toast.makeText(Sales_Followup_Activity.this, "First Start Call", Toast.LENGTH_SHORT).show();

                        }
                    }



                }


            }
        });
        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });
        /*-------------------------------------------------------------------------------*/

        clouserreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, clouserreasonlist[0]);
        clouserreason_spinner.setAdapter(clouserreasonArrayAdapter);
        makeArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, makelist[0]);
        make_spinner.setAdapter(makeArrayAdapter);

        followupArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, followuplist[0]);
        followuptype_spinner.setAdapter(followupArrayAdapter);

        followupDoneArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, followupdonelist[0]);
        sp_enquiry_status.setAdapter(followupDoneArrayAdapter);
        if (sales_enquiry_status.getText().toString().trim().equals("OPEN")) {
            int spinnerPosition = followupDoneArrayAdapter.getPosition("Open");
            sp_enquiry_status.setSelection(spinnerPosition);

        }


        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);


        if(shippingDialog != null) {
            shippingDialog.dismiss();
        }


        shippingDialog.show();

    }

    //show customer Historydata popup
    @Override
    public void getHistoryDetails(int position) {

        salesReminderHistoryDataList.clear();
        getSalesHistoryData(token, salesenquirydatalist.get(position).getMobilenumber());
        final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter_sales_historydata = new Adapter_Sales_Historydata(this, salesReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sales_historydata);
        adapter_sales_historydata.notifyDataSetChanged();
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });

        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customWaitingDialog.dismiss();
            }
        });

        shippingDialog.show();

    }

    //show customer MoreDetailsData popup
    @Override
    public void getMoreDetails(int position) {
        final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_more_datapopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);

        // ID genration of More Deatils PopUp
        /*------------------------------------------------------------------------------------------*/
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        EditText sale_nextfollowupdate = shippingDialog.findViewById(R.id.sale_next_follow_date);
        EditText sale_email = shippingDialog.findViewById(R.id.sales_email_id);
        EditText sale_address = shippingDialog.findViewById(R.id.sale_address);
        EditText sale_age = shippingDialog.findViewById(R.id.sales_age);
        EditText sale_gender = shippingDialog.findViewById(R.id.sales_gender);
        EditText sale_model_intersted = shippingDialog.findViewById(R.id.sales_model_interested);
        EditText sale_exchange_required = shippingDialog.findViewById(R.id.sales_exchange_required);
        EditText finance_required = shippingDialog.findViewById(R.id.sales_fianance_required);
        EditText sale_DSE_name = shippingDialog.findViewById(R.id.sales_dse_name);
        EditText sales_employeeid = shippingDialog.findViewById(R.id.sales_DSE_Employee_ID);
        EditText sales_enquiryid = shippingDialog.findViewById(R.id.sales_Enquiry_ID);
        EditText sales_enquirysource = shippingDialog.findViewById(R.id.sales_Enquiry_source);
        EditText sales_testriderequired = shippingDialog.findViewById(R.id.sales_testriderequired);
        EditText sales_requiredtime = shippingDialog.findViewById(R.id.sales_Test_Required_Time);
        EditText sales_testridetaken = shippingDialog.findViewById(R.id.sales_testridetaken);
        EditText testridedate = shippingDialog.findViewById(R.id.sales_testridedate);
        EditText sales_leader = shippingDialog.findViewById(R.id.sales_leader);
        EditText sales_comments = shippingDialog.findViewById(R.id.sales_comments);
        EditText sales_existing_vehicle = shippingDialog.findViewById(R.id.sales_existingvehicles);
        EditText sales_awarness_resorce = shippingDialog.findViewById(R.id.sales_awerness_resorce);


        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customWaitingDialog.dismiss();
            }
        });

        /*------------------------------------------------------------------------------------------*/


        // Vale Set  on Edit Texts
        /*------------------------------------------------------------------------------------------*/
        if (salesenquirydatalist.get(position).getNext_followup_date() != null)
            sale_nextfollowupdate.setText("" + dateparse3(salesenquirydatalist.get(position).getNext_followup_date().replaceAll("T", " ")).substring(0,11));
        if (salesenquirydatalist.get(position).getEmailid() != null)
            sale_email.setText("" + salesenquirydatalist.get(position).getEmailid().toUpperCase());
        if (salesenquirydatalist.get(position).getAddress() != null)
            sale_address.setText("" + salesenquirydatalist.get(position).getAddress().toUpperCase());
        if (salesenquirydatalist.get(position).getAge() != null)
            sale_age.setText("" + salesenquirydatalist.get(position).getAge().toUpperCase());
        if (salesenquirydatalist.get(position).getGender() != null)
            sale_gender.setText("" + salesenquirydatalist.get(position).getGender().toUpperCase());
        if (salesenquirydatalist.get(position).getModel_interested_in() != null)
            sale_model_intersted.setText("" + salesenquirydatalist.get(position).getModel_interested_in().toUpperCase());
        if (salesenquirydatalist.get(position).getExchange_required() != null)
            sale_exchange_required.setText("" + salesenquirydatalist.get(position).getExchange_required().toUpperCase());
        if (salesenquirydatalist.get(position).getFinance_required() != null)
            finance_required.setText("" + salesenquirydatalist.get(position).getFinance_required().toUpperCase());
        if (salesenquirydatalist.get(position).getDse_employee_id() != null)
            sales_employeeid.setText("" + salesenquirydatalist.get(position).getDse_employee_id().toUpperCase());
        if (salesenquirydatalist.get(position).getEnquiry_id() != null)
            sales_enquiryid.setText("" + salesenquirydatalist.get(position).getEnquiry_id().toUpperCase());
        if (salesenquirydatalist.get(position).getEnquiry_source() != null)
            sales_enquirysource.setText("" + salesenquirydatalist.get(position).getEnquiry_source().toUpperCase());
        if (salesenquirydatalist.get(position).getTest_ride_required() != null)
            sales_testriderequired.setText("" + salesenquirydatalist.get(position).getTest_ride_required().toUpperCase());
        if (salesenquirydatalist.get(position).getTest_ride_required_time() != null)
            sales_requiredtime.setText("" + salesenquirydatalist.get(position).getTest_ride_required_time().toUpperCase());
        if (salesenquirydatalist.get(position).getTest_ride_taken() != null)
            sales_testridetaken.setText("" + salesenquirydatalist.get(position).getTest_ride_taken().toUpperCase());
        if (salesenquirydatalist.get(position).getTest_ride_taken_time() != null)
            testridedate.setText("" + salesenquirydatalist.get(position).getTest_ride_taken_time().toUpperCase());
        if (salesenquirydatalist.get(position).getOpinion_leader() != null)
            sales_leader.setText("" + salesenquirydatalist.get(position).getOpinion_leader().toUpperCase());
        if (salesenquirydatalist.get(position).getEnquiry_comments() != null)
            sales_comments.setText("" + salesenquirydatalist.get(position).getEnquiry_comments().toUpperCase());
        if (salesenquirydatalist.get(position).getExisting_vehicle() != null)
            sales_existing_vehicle.setText("" + salesenquirydatalist.get(position).getExisting_vehicle().toUpperCase());
        if (salesenquirydatalist.get(position).getAwareness_source() != null)
            sales_awarness_resorce.setText("" + salesenquirydatalist.get(position).getAwareness_source().toUpperCase());
        /*------------------------------------------------------------------------------------------*/


        // Click Listener
        /*------------------------------------------------------------------------------------------*/
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        /*------------------------------------------------------------------------------------------*/


        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();


    }

    // Send WhatsAPP message to this function
    @Override
    public void sendWhatsappMessage(int position, String number) {

        whatsappclicked = true;
        whatsapp_number = number;
        // Toast.makeText(this, number, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(Sales_Followup_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, Sales_Followup_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(Sales_Followup_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, CommonVariables.SALES_FOLLOW_UP);


    }

    // Send Text Message to this Function
    @Override
    public void sendtextmessage(int position, String contactnumber) {


        customWaitingDialog.show();
        textmesasageclicked = true;
        whatsapp_number = contactnumber;
        //  Toast.makeText(this, contactnumber, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(Sales_Followup_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, Sales_Followup_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(Sales_Followup_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);

        getsmstemplate(token, getString(R.string.ENQUIRYTAG));

        customWaitingDialog.dismiss();


    }

    @Override
    public void holdCall(int position, String contatcnumber) {
        OngoingCall.call.hold();
    }

    @Override
    public void unHoldCall(int position, String contatcnumber) {
        OngoingCall.call.unhold();

    }



    /*------------------------------------------------------------------------------------------------------------------------*/

    //Get SMS Template
    public void getsmstemplate(String token, String templatetype) {
        templatelist.clear();
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    @Override
    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {


            //    Toast.makeText(this, "if " + templatebody, Toast.LENGTH_SHORT).show();


            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                    startActivity(sendMsg);
                    Sales_Followup_Activity.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        }
        else if (textmesasageclicked == true) {
            //   Toast.makeText(this, "else " + templatebody, Toast.LENGTH_SHORT).show();


            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            startActivity(intent);
            Sales_Followup_Activity.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }

    /*---------------------------------------------------Call Recording and Upload Code----------------------------------------------------------------*/
    //Recording Start Code
    public void startRecording(String number) throws IOException {
        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            audiofile = File.createTempFile(number, ".mp3", dir);
            recorder = new MediaRecorder();
            am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_IN_CALL);
            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(1411200);
            recorder.setAudioChannels(1);
            recorder.setAudioSamplingRate(88200);

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
                            am.setMode(AudioManager.MODE_IN_CALL);
//                            if (!am.isSpeakerphoneOn())
//                                am.setSpeakerphoneOn(true);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!am.isWiredHeadsetOn()) {
                thread.start();
            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (IOException e) {
                e.printStackTrace();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Stop  Recording Function
    public void stopRecording(String id) {
//
//        CommonVariables.recorder.stop();
//        CommonVariables.recorder.reset();
//        CommonVariables.recorder.release();
//        try {
//            if (am != null && recorder != null) {
//                am.setMode(AudioManager.MODE_NORMAL);
//                am.setStreamVolume(AudioManager.MODE_IN_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        CommonVariables.wavRecorder.stopRecording();


        //after stopping the recorder, create the sound file and add it to media library.

        if (CommonVariables.audiofile != null) UploadCallRecording(token, CommonVariables.dealercode, historyID, getString(R.string.ENQUIRYTAG));

    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String dealercode, String historyid, String calltype) {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + CommonVariables.audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/wav");
        values.put(MediaStore.Audio.Media.DATA, CommonVariables.audiofile.getAbsolutePath());
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        if (newUri != null) {
            selectedPath = getPath(newUri);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
            File files = new File(selectedPath);
            RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
            RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
            RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/wav");
            RequestBody Recording = RequestBody.create(MediaType.parse("audio/wav"), files);
            RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
            MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording", files.getName(), Recording);
            apiController.UploadCallRecording(token, Dealer_Code, History_id, Content_type, Recording_file, Call_type);
        }


    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    // Get Recording File Path
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /*------------------------------------------------------------------------------------------------------------------------*/
    public void clearcolorsharedeprefances() {
        if (arrayList_matserId.size() > 0) {
            for (int k = 0; k < arrayList_matserId.size(); k++) {
                SharedPreferences sharedPreferencesk = getSharedPreferences(arrayList_matserId.get(k), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesk.edit();
                editor.clear();
                editor.commit();


            }
        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return inputdate;
    }

    public String dateparse1(String inputdate) {

        // Oct 31 2020  2:16PM
        String inputPattern = "MMM dd yyyy HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return inputdate;
    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }


    public String dateparse2(String inputdate) {
        NewDateFormat="";
        if(!inputdate.isEmpty()) {

            // Oct 31 2020  2:16PM


            //2020-12-14 12:51:00.0000000
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd-MMM-yyyy HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            NewDateFormat= null;
            try {
                date = inputFormat.parse(inputdate);
                NewDateFormat = outputFormat.format(date);
                // Log.e("adte","date"+NewDateFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return NewDateFormat;
        }else
        {
            return NewDateFormat;
        }
    }
    public void DisableCallWaiting()
    {

        if(CommonVariables.isCallWaiting)
        {
            return;
        }

        String ussd =  Uri.encode("#")+ "43" + Uri.encode("#");
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        CommonVariables.isCallWaiting=true;

    }

    public void showDataInfo(String dataCont,String totaleDataCount,ArrayList<SalesEnquiryDataModel.DataBean> list)
    {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_data_popup_layout);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        TextView tv_total_count = dialog.findViewById(R.id.tv_total_count);
        RecyclerView userdataallocation_rc = dialog.findViewById(R.id.userdataallocation_rc);
        TextView tv_data_count = dialog.findViewById(R.id.tv_data_count);
        TextView closetv =dialog.findViewById(R.id.popupclose);
        tv_total_count.setText(""+dataCont);
        tv_data_count.setText(""+totaleDataCount);
        userdataallocation_rc.setLayoutManager(new LinearLayoutManager(this));
        userdataallocation_rc.hasFixedSize();

        AllocationDataAdapterSales allocationDataAdapterSales = new AllocationDataAdapterSales(this,list);
        userdataallocation_rc.setAdapter(allocationDataAdapterSales);

        allocationDataAdapterSales.notifyDataSetChanged();
        closetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();

    }

    public void uiSetup(boolean searchby,boolean searchbar,boolean followups,boolean index,boolean cbcalldone)

    {
        search_by.setEnabled(searchby);
        et_search.setEnabled(searchbar);
        followuptype.setEnabled(followups);
        indexs.setEnabled(index);
        cb_calldone.setEnabled(cbcalldone);
    }


    public void InableCallWaiting()
    {
        if(!CommonVariables.isCallWaiting)
        {
            return;
        }

//        String ussd =  "*"+ "43" + Uri.encode("#");
//        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        CommonVariables.isCallWaiting= false;

    }


    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent)
        {


            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);
            currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            if (currentNetworkInfo.isConnected()) {

                // finish();
                // startActivity(getIntent());
                Toast.makeText(getApplicationContext(), "Connected",Toast.LENGTH_LONG).show();

            }
            else {
                final Dialog shippingDialog = new Dialog(Sales_Followup_Activity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.layout_no_internet);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(false);
                ImageView iv_btn_close  = shippingDialog.findViewById(R.id.iv_btn_close);
                iv_btn_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shippingDialog.dismiss();
                    }
                });

                if(shippingDialog!=null)
                {
                    shippingDialog.show();

                }


            }
        }
    };


}