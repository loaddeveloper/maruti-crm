package com.hero.weconnect.mysmartcrm.Retrofit;

public class ApiConstant {
    /**
     * Base Url
     */

                      /*Development URL*/
/* public static final String baseUrlApi = "http://20.40.45.20/weconnect/api/";
 public static final String baseUrltokenApi = "http://20.40.45.20/weconnect/";*/


    public static final String baseUrlApi = "https://uat-weconnect.heromotocorp.biz/weconnect/api/";
    public static final String baseUrltokenApi = "https://uat-weconnect.heromotocorp.biz/weconnect/";
                                   /*Production URL*/
/*
   public static final String baseUrlApi = "https://weconnect.heromotocorp.biz/weconnect/api/";
   public static final String baseUrltokenApi = "https://weconnect.heromotocorp.biz/weconnect/";
*/
                                    /*Testing URL*/
//        public static final String baseUrlApi = "https://weconnect.heromotocorp.biz/weconnecttestp/api/";
//        public static final String baseUrltokenApi = "https://weconnect.heromotocorp.biz/weconnecttestp/";
//        public static final String baseUrlApi = "https://loadcrm.com/weconnect/api/";
//        public static final String baseUrltokenApi = "https://loadcrm.com/weconnect/";


    /**
     * Param Name
     */
    public static final String uid = "uid";
    public static final String code = "code";

    /**
     * Bean Tag
     */
    public static final String TOKEN_TAG = "TOKEN_TAG";
    public static final String CALLING_SHARED = "CALLING_SHARED";
    public static final String CONTACT_STATUS = "CONTACT_STATUS";
    public static final String CALL_START = "CALL_START";
    public static final String SR = "SR";
    public static final String ENQUIRY = "ENQUIRY";
    public static final String PURCHASEDATE = "PURCHASEDATE";
    public static final String DEALLOCATION = "DEALLOCATION";
    public static final String CALL_DISCONNECT = "CALL_DISCONNECT";
    public static final String CUSTOMER_REPLY = "CUSTOMER_REPLY";
    public static final String NOT_COMING_REASON = "NOT_COMING_REASON";
    public static final String CLOUSERREASON = "CLOUSERREASON";
    public static final String CLOUSERSUBREASON = "CLOUSERSUBREASON";
    public static final String MAKE = "MAKE";
    public static final String MODEL = "MODEL";
    public static final String ADDCALLHISTORYUPDATESR = "ADDCALLHISTORYUPDATESR";
    public static final String UPDATECONTACTNUMBERSR = "UPDATECONTACTNUMBERSR";
    public static final String UPDATECUSTOMERREPLYSR = "UPDATECUSTOMERREPLYSR";
    public static final String UPDATETAGGERSRSTATUSSR = "UPDATETAGGERSRSTATUSSR";
    public static final String UPDATEBOOKINGDATETIME = "UPDATEBOOKINGDATETIME";
    public static final String UPDATECALLENDTIME = "UPDATECALLENDTIME";
    public static final String UPDATEREMARK = "UPDATEREMARK";
    public static final String CommonModel = "CommonModel";
    public static final String UPDATEFOLLOWUPDATE = "UPDATEFOLLOWUPDATE";
    public static final String UPDATECONTACTSTATUS = "UPDATECONTACTSTATUS";
    public static final String UPDATECALLEDSTATUS = "UPDATECALLEDSTATUS";
    public static final String UPDATENOTCOMINGREASON = "UPDATENOTCOMINGREASON";
    public static final String SERVICEREMINDERDATA = "SERVICEREMINDERDATA";
    public static final String PSFHISTORYDATA = "PSFHISTORYDATA";
    public static final String NONTURNUPREMINDERDATA = "NONTURNUPREMINDERDATA";
    public static final String BOOKINGREMINDERDATA = "BOOKINGREMINDERDATA";
    public static final String BOOKINGREMINDERHISTORYDATA = "BOOKINGREMINDERHISTORYDATA";
    public static final String SALESENQUIRYDATA = "SALESENQUIRYDATA";
    public static final String BOOKINGSALESENQUIRYDATA = "BOOKINGSALESENQUIRYDATA";
    public static final String SALESENQUIRYCALLDONEDATA = "SALESENQUIRYCALLDONEDATA";
    public static final String SALESEXPECTEDENQUIRYCALLDONEDATA = "SALESEXPECTEDENQUIRYCALLDONEDATA";
    public static final String SRCALLDONEDATA = "SRCALLDONEDATA";
    public static final String NONTURNUPCALLDONEDATA = "NONTURNUPCALLDONEDATA";
    public static final String BRCALLDONEDATA = "BRCALLDONEDATA";
    public static final String PSFCALLDONEDATA = "PSFCALLDONEDATA";
    public static final String EXPECTEDREPORTINGDATA = "EXPECTEDREPORTINGDATA";
    public static final String REPORTINGDATA = "REPORTINGDATA";
    public static final String REPORTINGDATA_SR = "REPORTINGDATA_SR";
    public static final String REPORTINGDATA_BR = "REPORTINGDATA_BR";
    public static final String GETVERSION = "GETVERSION";
    public static final String USERDETAILSDATA = "USERDETAILSDATA";
    public static final String USERNAMELIST = "USERNAMELIST";
    public static final String ALLOTEDDATA = "ALLOTEDDATA";
    public static final String SALESEXPECTEDENQUIRYDATASEARCH = "SALESEXPECTEDENQUIRYDATASEARCH";
    public static final String SALESENQUIRYDATASEARCH = "SALESENQUIRYDATASEARCH";
    public static final String SALESENQUIRYDATAFILTER = "SALESENQUIRYDATAFILTER";
    public static final String SALESEXPECTEDENQUIRYDATAFILTER = "SALESEXPECTEDENQUIRYDATAFILTER";
    public static final String UPDATESALESHISTORYID = "UPDATESALESHISTORYID";
    public static final String UPDATESALESEXPECTEDHISTORYID = "UPDATESALESEXPECTEDHISTORYID";
    public static final String UPDATEDATEOFPURCHASE = "UPDATEDATEOFPURCHASE";
    public static final String UPDATEEXPECTEDDATEOFPURCHASE = "UPDATEEXPECTEDDATEOFPURCHASE";
    public static final String UPDATECLOSUREREASON = "UPDATECLOSUREREASON";
    public static final String UPDATEEXPECTEDCLOSUREREASON = "UPDATEEXPECTEDCLOSUREREASON";
    public static final String UPDATECLOSURESUBREASON = "UPDATECLOSURESUBREASON";
    public static final String UPDATEEXPECTEDCLOSURESUBREASON = "UPDATEEXPECTEDCLOSURESUBREASON";
    public static final String UPDATEMAKE = "UPDATEMAKE";
    public static final String UPDATEEXPECTEDMAKE = "UPDATEEXPECTEDMAKE";
    public static final String UPDATEMODEL = "UPDATEMODEL";
    public static final String UPDATEEXPECTEDMODEL = "UPDATEEXPECTEDMODEL";
    public static final String UPDATEPICKDROP = "UPDATEPICKDROP";
    public static final String CREATEFOLLOWUP = "CREATEFOLLOWUP";
    public static final String SERVICEREQUEST = "SERVICEREQUEST";
    public static final String GETCALLINGHISTORY = "GETCALLINGHISTORY";
    public static final String GETPSFQUESTIONLIST = "GETPSFQUESTIONLIST";
    public static final String GETPSFDISATISFACTIONREASONLIST = "GETPSFDISATISFACTIONREASONLIST";
    public static final String UPDATEPSFDISATISFACTIONREASONL = "UPDATEPSFDISATISFACTIONREASONL";
    public static final String UPDATEPSFRATINGANSWER = "UPDATEPSFRATINGANSWER";
    public static final String GETEXPECTEDSALESCALLINGHISTORY = "GETEXPECTEDSALESCALLINGHISTORY";
    public static final String GETSALESCALLINGHISTORY = "GETSALESCALLINGHISTORY";
    public static final String SETSMSSTATUS = "SETSMSSTATUS";
    public static final String SETSMSTEMPLATE = "SETSMSTEMPLATE";
    public static final String GETSMSTEMPLATE = "GETSMSTEMPLATE";
    public static final String GETSMSTATUS = "GETSMSTATUS";
    public static final String UPLOADRECORDING = "UPLOADRECORDING";
    public static final String SRINCOMINGDATA = "SRINCOMINGDATA";
    public static final String ENQUIRYINCOMINGDATA = "ENQUIRYINCOMINGDATA";
    public static final String PACKAGEADD = "PACKAGEADD";
    public static final String MISSEDDATA = "MISSEDDATA";
    public static final String MISSEDDATASALES = "MISSEDDATASALES";
    public static final String SINGLEDATA = "SINGLEDATA";
    public static final String SINGLEDATABR = "SINGLEDATABR";
    public static final String SINGLEDATAPSF = "SINGLEDATAPSF";
    public static final String SINGLEDATANONTURNUP = "SINGLEDATANONTURNUP";
    public static final String SINGLEDATASALES = "SINGLEDATASALES";
    public static final String ADDEXCEPTION = "ADDEXCEPTION";
    public static final String GETFOLLOWUPDONELIST = "GETFOLLOWUPDONELIST";
    public static final String GETFOLLOWUPSTATUSLIST = "GETFOLLOWUPSTATUSLIST";
    public static final String UPDATESALESFOLLOWUPSTATUS = "UPDATESALESFOLLOWUPSTATUS";
    public static final String UPDATESALESEXPECTEDFOLLOWUPSTATUS = "UPDATESALESEXPECTEDFOLLOWUPSTATUS";
    public static final String UPDATESALESFOLLOWUPDONESTATUS = "UPDATESALESFOLLOWUPDONESTATUS";
    public static final String UPDATESALESEXPECTEDFOLLOWUPDONESTATUS = "UPDATESALESEXPECTEDFOLLOWUPDONESTATUS";
    public static final String CANCELBOOKINGREQUEST = "CANCELBOOKINGREQUEST";
   public static final String ALLOTEDDSE = "ALLOTEDDSE";
   public static final String ALLOTED_SR = "ALLOTED_SR";

    /**** Constant API Params ****/
    public static final String COUNTRY_NAME = "India";
    public static final String offset = "offset";
    public static final String FollowupType = "FollowupType";
    public static final String MasterSEId = "MasterSEId";
    public static final String HistoryId = "HistoryId";
    public static final String BoundType = "BoundType";
    public static final String ExpectedPurchaseDate = "ExpectedPurchaseDate";
    public static final String Reason = "Reason";
    public static final String MakeBought = "MakeBought";
    public static final String ClosureReason = "ClosureReason";
    public static final String ModelBought = "ModelBought";
    public static final String MasterId = "MasterId";
    public static final String FollowupDateTime = "FollowupDateTime";
    public static final String CallType = "CallType";
    public static final String Status = "Status";
    public static final String Comment = "Comment";
    public static final String CallStatus = "CallStatus";
    public static final String Tag = "Tag";
    public static final String MasterSRId = "MasterSRId";
    public static final String NewContactNo = "NewContactNo";
    public static final String PriorityStatus = "PriorityStatus";
    public static final String Reply = "Reply";
    public static final String BookingDate = "BookingDate";


    /*--------------------------------Common Calling SharePreferances----------------------------------------------*/
    public static final String COMMONCALLINGCOLORSHAREDPREANCES = "COMMONCALLINGCOLORSHAREDPREANCES";
    public static final String CALL_START_STOP_SHAREDPREANCES = "CALL_START_STOP_SHAREDPREANCES";
    public static final String COMMONCALLINGCOLORSHAREDPREANCESSHREE = "COMMONCALLINGCOLORSHAREDPREANCESSHREE";
    public static final String COMMONCALLINGCOLORPOSITION = "COMMONCALLINGCOLORPOSITION";
    public static final String SHAREDPREFERANCESPOSITION = "SHAREDPREFERANCESPOSITION";
    public static final String SHAREDPREFERANCECALLCURRENTSTATUS = "SHAREDPREFERANCECALLCURRENTSTATUS";
    public static final String SHAREDPREFERANCESCALLCURRENTMOBILENO = "SHAREDPREFERANCESCALLCURRENTMOBILENO";
    public static final String SHAREDPREFERANCESMASTETID = "SHAREDPREFERANCESMASTETID";
    public static String UNKNOWN_NUMBER = "UNKNOWN NUMBER";
}
