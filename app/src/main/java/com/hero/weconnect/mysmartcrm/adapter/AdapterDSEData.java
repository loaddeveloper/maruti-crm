package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.activity.SettingActivity;
import com.hero.weconnect.mysmartcrm.customfonts.TextView_Roboto_Medium;
import com.hero.weconnect.mysmartcrm.models.DataAllotedModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;

import java.util.ArrayList;


public class AdapterDSEData extends RecyclerView.Adapter<AdapterDSEData.ViewHolder> {
    ArrayList<DataAllotedModel.DataDTO> templatelist = new ArrayList<>();

    private Context context;
    AdapterDSEData.CustomButtonListener customListner;

    public AdapterDSEData(Context context, AdapterDSEData.CustomButtonListener customButtonListener, ArrayList<DataAllotedModel.DataDTO> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.templatelist = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.data_allocation_card, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        DataAllotedModel.DataDTO dataBean = templatelist.get(position);
        holder.dseid.setText("" + dataBean.getDSE());
        holder.totalfollowps.setText("" + dataBean.getTotalFollowups());
        holder.totalcalls.setText(""+dataBean.getCalled());
        holder.alloteddatal.setText(""+dataBean.getAlloted());
        holder.AllotedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customListner.getAllotedCustomer(position,""+dataBean.getDSE());

            }
        });






    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return templatelist.size();
    }
    public void setCustomButtonListner(AdapterDSEData.CustomButtonListener listener) {
        this.customListner = listener;
    }

    public interface CustomButtonListener {

        void getAllotedCustomer(int position, String DSEID);


    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView dseid, totalfollowps,totalcalls,alloteddatal;
        public LinearLayout AllotedData;


        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            dseid = rootView.findViewById(R.id.dealerCodenew);
            this.totalfollowps = rootView.findViewById(R.id.totalfollowps);
            this.totalcalls = rootView.findViewById(R.id.totalcalls);
            this.alloteddatal = rootView.findViewById(R.id.alloteddata);
            this.AllotedData = rootView.findViewById(R.id.alloteddatalayout);
        }
    }


}