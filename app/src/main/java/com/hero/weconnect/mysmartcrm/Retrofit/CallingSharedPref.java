package com.hero.weconnect.mysmartcrm.Retrofit;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;

import java.lang.reflect.Type;

public class CallingSharedPref {

    private Context mContext;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;

    public CallingSharedPref(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("SRCallingdata", Context.MODE_PRIVATE);

    }

    public ServiceReminderDataModel getSRCallingData() {

        String json = sharedPreferences.getString(ApiConstant.SERVICEREMINDERDATA, null);
        Gson gson = new Gson();
        Type type = new TypeToken<ServiceReminderDataModel>() {
        }.getType();

        ServiceReminderDataModel beans = null;
        beans = gson.fromJson(json, type);

        return beans;

    }

    public void setSRCallingData(ServiceReminderDataModel beans) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString(ApiConstant.SERVICEREMINDERDATA, json);
        editor.commit();
    }

    public String getSRCallingStatus() {
        String status = sharedPreferences.getString(ApiConstant.SERVICEREMINDERDATA + "status", null);

        return status;

    }

    public void setSRCallingStatus(String status) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.SERVICEREMINDERDATA + "status", status);
        editor.commit();
    }

    public String getSRCallingPosition() {
        String position = sharedPreferences.getString(ApiConstant.SERVICEREMINDERDATA + "position", null);

        return position;

    }

    public void setSRCallingPosition(String position) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.SERVICEREMINDERDATA + "position", position);
        editor.commit();
    }

    public String getSRCallStatus() {
        String callstatus = sharedPreferences.getString(ApiConstant.SERVICEREMINDERDATA + "callstatus", null);

        return callstatus;

    }

    public void setSRCallStatus(String callstatus) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.SERVICEREMINDERDATA + "callstatus", callstatus);
        editor.commit();
    }

    public void setAllClearSRCallingData() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }
}
