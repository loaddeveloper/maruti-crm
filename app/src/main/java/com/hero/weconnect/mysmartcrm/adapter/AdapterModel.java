package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.ModelModel;

import java.util.ArrayList;


public class AdapterModel extends RecyclerView.Adapter<AdapterModel.ViewHolder> {
    ArrayList<String> modelList = new ArrayList<>();
    ModelClickListener modelClickListener;
    private Context context;
    int counter=1;
    int pos;


    public AdapterModel(Context context,ArrayList<String> modelList ,ModelClickListener modelClickListener) {

        this.context = context;

       // this.customListner = customButtonListener;
        this.modelList = modelList;
        this.modelClickListener= modelClickListener;
    
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.selective_rc_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.checkBox.setText(modelList.get(position));


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

//                    if(CommonVariables.filermodellist.size()>0)
//                    {
//                        for(String modelName:CommonVariables.filermodellist)
//                        {
//                            if(modelName.equals(modelList.get(position)))
//                            {
//
//
//                            }else
//                            {
//                                modelClickListener.addmodel(position,modelList.get(position));
//                                return;
//                            }
//
//                        }
//                    }else
//                    {
                        modelClickListener.addmodel(position,modelList.get(position));

                   // }

                }else
                {
                    modelClickListener.removeModel(position,modelList.get(position));

//                    if(CommonVariables.filermodellist.size()>0)
//                    {
//                        for(String modelName:CommonVariables.filermodellist)
//                        {
//                            if(modelName.equals(modelList.get(position)))
//                            {
//                                modelClickListener.removeModel(position,modelList.get(position));
//                                return;
//
//                            }
//                        }
//                    }

                }
            }
        });

        if(CommonVariables.filermodellist.size()>0)
        {
            for(String modelName:CommonVariables.filermodellist)
            {
                if(modelName.equals(modelList.get(position)))
                {
                    holder.checkBox.setChecked(true);
                }
            }
        }

        holder.tv_option.setVisibility(View.GONE);
        holder.tv_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(counter==1)
                {
                    counter=counter+1;
                    holder.checkBox.setChecked(true);

                }else
                {
                    counter=counter-1;
                    holder.checkBox.setChecked(false);

                }

            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return modelList.size();
    }



    public interface ModelClickListener {
        void addmodel(int position, String model);
        void removeModel(int position, String model);


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView tv_option;
        public CheckBox checkBox;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.tv_option = rootView.findViewById(R.id.tv_option);
            this.checkBox = rootView.findViewById(R.id.cb);
        }
    }

}