package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.activity.VideoActivity;
import com.hero.weconnect.mysmartcrm.models.VideoDataModel;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    Context cxt;
    ArrayList<VideoDataModel.DataDTO> list;

    public VideoAdapter(Context context, ArrayList<VideoDataModel.DataDTO> list) {

        this.cxt=context;
        this.list=list;


    }





    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(cxt).inflate(R.layout.video_rc_layout,parent,false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, int position)
    {
        VideoDataModel.DataDTO dataDTO=list.get(position);

        holder.tv_des.setText(dataDTO.getDescription());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataDTO.getLink()));
                cxt.startActivity(browserIntent);
            }
        });





    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VideoViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_des;
        ConstraintLayout container;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_des = itemView.findViewById(R.id.videoDes);
            container = itemView.findViewById(R.id.container);
        }
    }
}
