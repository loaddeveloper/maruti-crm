package com.hero.weconnect.mysmartcrm.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.activity.DashboardActivity;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.microsoft.appcenter.analytics.Analytics;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class ExceptionHandler implements Thread.UncaughtExceptionHandler, ApiResponseListener {
    private final Activity myContext;
    private final String LINE_SEPARATOR = "\n";
    StringWriter stackTrace = new StringWriter();
    SharedPreferences chkfs;
    String tok = "", usr = "", tokenbearer = "";
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    SharedPreferences appname;

    public ExceptionHandler(Activity context) {
        myContext = context;
        commonSharedPref = new CommonSharedPref(context);
        appname= context.getSharedPreferences("appname",MODE_PRIVATE);

        if(commonSharedPref.getLoginData() != null && !commonSharedPref.getLoginData().getAccess_token().isEmpty()) {

           tok = commonSharedPref.getLoginData().getAccess_token();
           usr = commonSharedPref.getLoginData().getDealercode();
           tokenbearer = "bearer " + tok;
           apiController = new ApiController(this,myContext);
       }

       // Log.e("Tokennnss", " " + tok);
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());

        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);
        bookingdatesr(tokenbearer,exception.toString());
       // Log.e("errorreport", "" + errorReport.toString());
        Intent intent = new Intent(myContext, DashboardActivity.class);
        intent.putExtra("error", errorReport.toString());

        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+ CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "ExceptionHandler");
        properties.put("Role", ""+CommonVariables.role);
        properties.put("Error", ""+errorReport.toString());

        Analytics.trackEvent("ExceptionHandler", properties);
        if(appname.getString("appname","") != null)
        {
            if(appname.getString("appname","").equals("HJC"))
            {
                PackageManager pm = myContext.getPackageManager();
                Intent intent1 = pm.getLaunchIntentForPackage("com.hero.JCapp");
                if (intent1 != null) {
                    myContext.startActivity(intent1);
                    //android.os.Process.killProcess(android.os.Process.myPid());
                }
            }else
            {

                PackageManager pm = myContext.getPackageManager();
                Intent intent2 = pm.getLaunchIntentForPackage("com.herocorp");
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                List<ApplicationInfo> packages;

                pm = myContext.getPackageManager();
                //get a list of installed apps.
                packages = pm.getInstalledApplications(0);

               // Log.e("Packages","package"+packages);
                ActivityManager mActivityManager = (ActivityManager)myContext.getSystemService(Context.ACTIVITY_SERVICE);


                for (ActivityManager.RunningAppProcessInfo pid : mActivityManager.getRunningAppProcesses()) {
                    mActivityManager.killBackgroundProcesses(pid.processName);
                }

                myContext.finish();
                myContext.finishAndRemoveTask();
                myContext.startActivity(intent2);


                if (intent2 != null) {




                    //android.os.Process.killProcess(android.os.Process.myPid());


                }
            }
        }
        intent.putExtra("token", tokenbearer);
       // myContext.startActivity(intent);

//        try {
//            sleep(1000);
////            android.os.Process.killProcess(android.os.Process.myPid());
////            System.exit(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    public void bookingdatesr(String token, String exception) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("ExceptionLog", "" + exception);
        params.put("Brand", "" + Build.BRAND);
        params.put("Model", "" + Build.MODEL);


        ////Log.e("savebooking", params.toString() + token);
        ////Log.e("booking", params.toString());

        apiController.addException(tokenbearer, params);



  /*      StringRequest str2 = new StringRequest(Request.Method.POST, AppCommonUrl.commonURL+"error/androidError/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ////Log.e("notcoming", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ////Log.e("Error.Response", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ErrorLog",""+exception);
                params.put("Line",""+exception);
                params.put("Brand",""+Build.BRAND);
                params.put("Model",""+Build.MODEL);
                params.put("UserName",""+usr);
                params.put("DealerCode",""+usr);
                ////Log.e("savebooking", params.toString()+token);

                return params;
            }

            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "bearer "+token);
                return headers;
            }
        };
        MainController.getInstance().addToRequestQueue(str2);
        str2.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });*/

    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.ADDEXCEPTION)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().toLowerCase().equals("sucess")) {
                Toast.makeText(myContext, "Sucess", Toast.LENGTH_SHORT).show();

            }
        }

    }

    @Override
    public void onFailure(String msg) {

    }

    @Override
    public void onError(String msg) {

    }

}