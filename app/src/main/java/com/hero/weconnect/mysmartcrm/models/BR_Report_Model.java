package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class BR_Report_Model extends SuperClassCastBean {


    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("TotalCustomer")
        private int totalCustomer;
        @SerializedName("TotalCallsMade")
        private int totalCallsMade;
        @SerializedName("CallDone")
        private int callDone;
        @SerializedName("CallNotDone")
        private int callNotDone;
        @SerializedName("Followups")
        private int followups;
        @SerializedName("Bookings")
        private int bookings;
        @SerializedName("BookingConfirmation")
        private int bookingConfirmation;
        @SerializedName("NoResponse")
        private int noResponse;

        public int getTotalCustomer() {
            return totalCustomer;
        }

        public void setTotalCustomer(int totalCustomer) {
            this.totalCustomer = totalCustomer;
        }

        public int getTotalCallsMade() {
            return totalCallsMade;
        }

        public void setTotalCallsMade(int totalCallsMade) {
            this.totalCallsMade = totalCallsMade;
        }

        public int getCallDone() {
            return callDone;
        }

        public void setCallDone(int callDone) {
            this.callDone = callDone;
        }

        public int getCallNotDone() {
            return callNotDone;
        }

        public void setCallNotDone(int callNotDone) {
            this.callNotDone = callNotDone;
        }

        public int getFollowups() {
            return followups;
        }

        public void setFollowups(int followups) {
            this.followups = followups;
        }

        public int getBookings() {
            return bookings;
        }

        public void setBookings(int bookings) {
            this.bookings = bookings;
        }

        public int getBookingConfirmation() {
            return bookingConfirmation;
        }

        public void setBookingConfirmation(int bookingConfirmation) {
            this.bookingConfirmation = bookingConfirmation;
        }

        public int getNoResponse() {
            return noResponse;
        }

        public void setNoResponse(int noResponse) {
            this.noResponse = noResponse;
        }
    }
}
