package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class NonTurnUpSingleDataModel extends SuperClassCastBean {

    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("masternonturnupbookingreminderid")
        private String masternonturnupbookingreminderid;
        @SerializedName("contactstatus")
        private String contactstatus;
        @SerializedName("newservicebooking")
        private String newservicebooking;
        @SerializedName("servicerequest")
        private String servicerequest;
        @SerializedName("reminderfollowupdate")
        private String reminderfollowupdate;
        @SerializedName("comments")
        private String comments;
        @SerializedName("calledstatus")
        private String calledstatus;
        @SerializedName("hcresfaultmessage")
        private String hcresfaultmessage;
        @SerializedName("id")
        private String id;

        public String getMasternonturnupbookingreminderid() {
            return masternonturnupbookingreminderid;
        }

        public void setMasternonturnupbookingreminderid(String masternonturnupbookingreminderid) {
            this.masternonturnupbookingreminderid = masternonturnupbookingreminderid;
        }

        public String getContactstatus() {
            return contactstatus;
        }

        public void setContactstatus(String contactstatus) {
            this.contactstatus = contactstatus;
        }

        public String getNewservicebooking() {
            return newservicebooking;
        }

        public void setNewservicebooking(String newservicebooking) {
            this.newservicebooking = newservicebooking;
        }

        public String getServicerequest() {
            return servicerequest;
        }

        public void setServicerequest(String servicerequest) {
            this.servicerequest = servicerequest;
        }

        public String getReminderfollowupdate() {
            return reminderfollowupdate;
        }

        public void setReminderfollowupdate(String reminderfollowupdate) {
            this.reminderfollowupdate = reminderfollowupdate;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getHcresfaultmessage() {
            return hcresfaultmessage;
        }

        public void setHcresfaultmessage(String hcresfaultmessage) {
            this.hcresfaultmessage = hcresfaultmessage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
