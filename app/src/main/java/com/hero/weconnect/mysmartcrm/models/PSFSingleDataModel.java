package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class PSFSingleDataModel extends SuperClassCastBean {

    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("mobileno")
        private String mobileno;
        @SerializedName("masterpsfid")
        private String masterpsfid;
        @SerializedName("calledstatus")
        private String calledstatus;
        @SerializedName("bookingdate")
        private String bookingdate;
        @SerializedName("bookingtime")
        private String bookingtime;
        @SerializedName("dealercode")
        private String dealercode;
        @SerializedName("remark")
        private String remark;
        @SerializedName("followupdate")
        private String followupdate;

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getMasterpsfid() {
            return masterpsfid;
        }

        public void setMasterpsfid(String masterpsfid) {
            this.masterpsfid = masterpsfid;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getBookingdate() {
            return bookingdate;
        }

        public void setBookingdate(String bookingdate) {
            this.bookingdate = bookingdate;
        }

        public String getBookingtime() {
            return bookingtime;
        }

        public void setBookingtime(String bookingtime) {
            this.bookingtime = bookingtime;
        }

        public String getDealercode() {
            return dealercode;
        }

        public void setDealercode(String dealercode) {
            this.dealercode = dealercode;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getFollowupdate() {
            return followupdate;
        }

        public void setFollowupdate(String followupdate) {
            this.followupdate = followupdate;
        }
    }
}
