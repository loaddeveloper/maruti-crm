package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class CustomHistoryModel extends SuperClassCastBean {


    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Serial_No":1,"Id":"f667e397-8233-4692-9dc6-000402be2bfd","Calling_Data_Id":"f667e397-8233-4692-9dc6-000402be2bfb","Called_Status":"","Call_Start_Time":"2020-12-03T12:53:07.16","Call_End_Time":"1900-01-01T00:00:00","Booking_Date":"1900-01-01T00:00:00","Follow_Up_Date":"1900-01-01T00:00:00","Status":"","Remark":"","Recording_Path":"~/Content/Recordings/f667e397-8233-4692-9dc6-000402be2bfd.mp3","Content_type":"audio/mp3","Data1":"","Data2":""},{"Serial_No":2,"Id":"f667e397-8233-4692-9dc6-000402be2bfc","Calling_Data_Id":"f667e397-8233-4692-9dc6-000402be2bfb","Called_Status":"","Call_Start_Time":"2020-12-02T12:18:11.007","Call_End_Time":"1900-01-01T00:00:00","Booking_Date":"2020-11-23T00:00:00","Follow_Up_Date":"1900-01-01T00:00:00","Status":"","Remark":"","Recording_Path":"","Content_type":"","Data1":"","Data2":""}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Serial_No : 1
         * Id : f667e397-8233-4692-9dc6-000402be2bfd
         * Calling_Data_Id : f667e397-8233-4692-9dc6-000402be2bfb
         * Called_Status :
         * Call_Start_Time : 2020-12-03T12:53:07.16
         * Call_End_Time : 1900-01-01T00:00:00
         * Booking_Date : 1900-01-01T00:00:00
         * Follow_Up_Date : 1900-01-01T00:00:00
         * Status :
         * Remark :
         * Recording_Path : ~/Content/Recordings/f667e397-8233-4692-9dc6-000402be2bfd.mp3
         * Content_type : audio/mp3
         * Data1 :
         * Data2 :
         */

        @SerializedName("Serial_No")
        private int SerialNo;
        @SerializedName("Id")
        private String Id;
        @SerializedName("Calling_Data_Id")
        private String CallingDataId;
        @SerializedName("Called_Status")
        private String CalledStatus;
        @SerializedName("Call_Start_Time")
        private String CallStartTime;
        @SerializedName("Call_End_Time")
        private String CallEndTime;
        @SerializedName("Booking_Date")
        private String BookingDate;
        @SerializedName("Follow_Up_Date")
        private String FollowUpDate;
        @SerializedName("Status")
        private String Status;
        @SerializedName("Remark")
        private String Remark;
        @SerializedName("Recording_Path")
        private String RecordingPath;
        @SerializedName("Content_type")
        private String ContentType;
        @SerializedName("Data1")
        private String Data1;
        @SerializedName("Data2")
        private String Data2;
        @SerializedName("Column1")
        private String Column;

        public int getSerialNo() {
            return SerialNo;
        }

        public void setSerialNo(int SerialNo) {
            this.SerialNo = SerialNo;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public String getCallingDataId() {
            return CallingDataId;
        }

        public void setCallingDataId(String CallingDataId) {
            this.CallingDataId = CallingDataId;
        }

        public String getCalledStatus() {
            return CalledStatus;
        }

        public void setCalledStatus(String CalledStatus) {
            this.CalledStatus = CalledStatus;
        }

        public String getCallStartTime() {
            return CallStartTime;
        }

        public void setCallStartTime(String CallStartTime) {
            this.CallStartTime = CallStartTime;
        }

        public String getCallEndTime() {
            return CallEndTime;
        }

        public void setCallEndTime(String CallEndTime) {
            this.CallEndTime = CallEndTime;
        }

        public String getBookingDate() {
            return BookingDate;
        }

        public void setBookingDate(String BookingDate) {
            this.BookingDate = BookingDate;
        }

        public String getFollowUpDate() {
            return FollowUpDate;
        }

        public void setFollowUpDate(String FollowUpDate) {
            this.FollowUpDate = FollowUpDate;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public String getRecordingPath() {
            return RecordingPath;
        }

        public void setRecordingPath(String RecordingPath) {
            this.RecordingPath = RecordingPath;
        }

        public String getContentType() {
            return ContentType;
        }

        public void setContentType(String ContentType) {
            this.ContentType = ContentType;
        }

        public String getData1() {
            return Data1;
        }

        public void setData1(String Data1) {
            this.Data1 = Data1;
        }

        public String getData2() {
            return Data2;
        }

        public void setData2(String Data2) {
            this.Data2 = Data2;
        }

        public String getColumn() {
            return Column;
        }

        public void setColumn(String column) {
            Column = column;
        }
    }
}
