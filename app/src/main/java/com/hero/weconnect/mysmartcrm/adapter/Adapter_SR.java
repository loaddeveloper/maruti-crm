package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;
import com.hero.weconnect.mysmartcrm.activity.Sales_Followup_Activity;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Logger;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_SR extends RecyclerView.Adapter<Adapter_SR.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    CustomButtonListener customListner;
    int size = 0;
    String holdtag = "";
    ArrayList<ServiceReminderDataModel.DataBean> servicerreminderdata;
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    private Context context;
    CountDownTimer countDownTimer;
    final long defaultClickTimeGap=2000;
    long lastClickTime= SystemClock.elapsedRealtime();

    public Adapter_SR(Context context, CustomButtonListener customButtonListener, ArrayList<ServiceReminderDataModel.DataBean> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.servicerreminderdata = list;
        setHasStableIds(true);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.sr_data_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       // holder.setIsRecyclable(true);




        ////Log.e("SRADAPTER",""+position);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);
        if (servicerreminderdata.size() > 0) {

            if (position == servicerreminderdata.size()) {
                holder.morebtn1.setVisibility(View.VISIBLE);
                holder.main_ll.setVisibility(View.GONE);

            } else {
                ServiceReminderDataModel.DataBean dataBean = servicerreminderdata.get(position);

               if(holder.morebtn1 != null) holder.morebtn1.setVisibility(View.GONE);
                if(holder.main_ll != null)holder.main_ll.setVisibility(View.VISIBLE);
                final String idd = String.valueOf(dataBean.getId());
                holder.srno.setText("" + dataBean.getSno() + ".");
                holder.customer_Name.setText("ABCD");
                holder.registration_number.setText("RJ37SP4636");
                holder.customer_contact.setText("1111111111");
                holder.model.setText("ACTIVA 3G");
                holder.servicetype.setText("FREE");
             if (dataBean != null && dataBean.getNext_service_due_date() != null && !dataBean.getNext_service_due_date().isEmpty())holder.serviceduedate.setText("" +dateparse3( dataBean.getNext_service_due_date()).substring(0,11));
                holder.calledstatus.setText("" + dataBean.getCalledstatus().toUpperCase());

                /*  SharedPreferences chkfs  = context.getSharedPreferences("color", Context.MODE_PRIVATE);*/
                /*final String  idssss= chkfs.getString("id","");*/
                /*final String  call= chkfs.getString("call","");*/

                countDownTimer =new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        final String idssss = common_calling_color_sharedPreferances.getmatserid();
                        final String call = common_calling_color_sharedPreferances.getcallcurrentstatus();
                        SharedPreferences chkfsss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String status = chkfsss.getString("Status", "");
                        final String status1 = chkfsss.getString("Status1", "");
                        SharedPreferences chkfss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String icca = chkfss.getString("call", "");



                        if (dataBean.getId().equals(idssss) && call.equals("s")) {
                            holder.calledstatus.setText("Ringing");
                            pos = position;
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            holder.tv_hold.setVisibility(View.VISIBLE);
                        }
                        else if (dataBean.getId().equals(idssss) && dataBean.getCalledstatus().equals("CALL DONE") && call.equals("s")) {
                            holder.calledstatus.setText("Ringing");
                            pos = position;
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            holder.tv_hold.setVisibility(View.VISIBLE);

                        }
                        else if (( icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);


                        }
                        else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);


                            if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.tv_hold.setVisibility(View.GONE);


                            } else {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_hold.setVisibility(View.GONE);

                            }
                        }
                        else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        }
                        else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {

                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        }
                        else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        }
                        else {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                            holder.tv_hold.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onFinish() {

                        countDownTimer.cancel();

                    }
                };

                countDownTimer.start();



                holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.sendWhatsappMessage(position, "" + dataBean.getCusotmercontact());
                    }
                });
                holder.textmessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.sendtextmessage(position, "" + dataBean.getCusotmercontact());
                    }
                });
                holder.main_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(SystemClock.elapsedRealtime()-lastClickTime < defaultClickTimeGap)
                        {

                            return;
                        }

                        lastClickTime=SystemClock.elapsedRealtime();
                        /*int position,String mastertableid,String customername,String model,String serviceduefrom, String pickdropbalance, String contactnumber);*/
                        String customername = "" + dataBean.getCusotmer_first_name() + " " + dataBean.getCustomer_last_name().toUpperCase();
                        customListner.getCustomerDetails(position, dataBean.getId(), customername,
                                dataBean.getServiceType().toUpperCase(), dataBean.getModel().toUpperCase(),
                                dateparse3(dataBean.getNext_service_due_date().toUpperCase().replace("T", " ")),
                                dataBean.getLs_balance().toUpperCase(),
                                dataBean.getCusotmercontact(),holder.calledstatus.getText().toString());
                    }
                });
                holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public boolean onLongClick(View v) {

                        CommonVariables.Is_LongPress = true;
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(position));

                        //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                        String uid = UUID.randomUUID().toString();

                        SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                        SharedPreferences.Editor edits = prefs.edit();
                        edits.putString(dataBean.getId(), uid);
                        edits.putString("longpress", uid);
                        edits.commit();

                        //Log.e("MasterIdAdapter","Adapter"+dataBean.getId());

                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                            Toasty.warning(context, "Call is Currently Ongoing", Toast.LENGTH_SHORT).show();
                        } else {
                            if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                // Create the Uri from phoneNumberInput
                                //   Uri uri = Uri.parse("tel:" + "+91" + ""+ dataBean.getCusotmercontact());

                                CommonVariables.mobilenumber=dataBean.getCusotmercontact();
                              Uri uri = Uri.parse("tel:" + "+91"+ CommonVariables.SR_Testingnumber);

                                // Start call to the number in input
                                context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                holder.calledstatus.setText("Ringing");
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                ((S_R_Activity)context).setIndex(position+1,true);


                                common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                            } else {
                                // Request permission to call
                                ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                            }
                        }
                        return false;
                    }
                });
                holder.historyybtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getHistoryDetails(position);
                    }
                });
                holder.moredetailsbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getMoreDetails(position);
                    }
                });


                holder.tv_hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.VISIBLE);
                        customListner.holdCall(position, dataBean.getCusotmercontact());
                    }

                });

                holder.tv_unhold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.VISIBLE);
                        holder.tv_unhold.setVisibility(View.GONE);
                        customListner.unHoldCall(position, dataBean.getCusotmercontact());
                    }

                });


            }
        }


        holder.morebtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (context instanceof S_R_Activity) {

                    ((S_R_Activity) context).moreData();

                }

            }
        });





    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        if (servicerreminderdata.size() >= 10) {
            if ((servicerreminderdata.size() % 10) == 0) {
                size = servicerreminderdata.size() + 1;

            } else {
                size = servicerreminderdata.size();

            }
        } else {
            size = servicerreminderdata.size();
        }


        return size;
    }

    public void setCustomButtonListner(Adapter_SR.CustomButtonListener listener) {
        this.customListner = listener;
    }

    public void updateList(ArrayList<ServiceReminderDataModel.DataBean> list11) {
        servicerreminderdata = list11;
        notifyDataSetChanged();
    }

    public interface CustomButtonListener {
        void getCustomerDetails(int position, String mastertableid, String customername, String servicetype, String model,
                                String serviceduefrom, String pickdropbalance, String contactnumber,String calledstatus);

        void getHistoryDetails(int position);

        void getMoreDetails(int position);

        void sendWhatsappMessage(int position, String contactnumber);

        void sendtextmessage(int position, String contatcnumber);

        void holdCall(int position, String contatcnumber);

        void unHoldCall(int position, String contatcnumber);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Variable Declare
        public TextView srno, idddata, tv_hold, tv_unhold, customer_Name, registration_number, customer_contact, model, servicetype, serviceduedate, calledstatus, morebtn1;
        public ImageView moredetailsbtn, historyybtn, whatsapp, textmessage;
        LinearLayout main_ll;

        public ViewHolder(View rootView) {
            super(rootView);
            this.srno = rootView.findViewById(R.id.srno);
            this.idddata = rootView.findViewById(R.id.idddata);
            this.customer_Name = rootView.findViewById(R.id.customername);
            this.registration_number = rootView.findViewById(R.id.regno);
            this.customer_contact = rootView.findViewById(R.id.mobileno);
            this.model = rootView.findViewById(R.id.model);
            this.servicetype = rootView.findViewById(R.id.servicetype);
            this.serviceduedate = rootView.findViewById(R.id.serviceduedate);
            this.calledstatus = rootView.findViewById(R.id.cstatus);
            this.morebtn1 = rootView.findViewById(R.id.morebtn1);
            this.tv_hold = rootView.findViewById(R.id.tv_hold);
            this.tv_unhold = rootView.findViewById(R.id.tv_unhold);
            this.moredetailsbtn = rootView.findViewById(R.id.moredetailsbtn);
            this.historyybtn = rootView.findViewById(R.id.historyybtn);
            this.whatsapp = rootView.findViewById(R.id.whatsapp);
            this.textmessage = rootView.findViewById(R.id.textmessage);
            this.main_ll = rootView.findViewById(R.id.main_ll);

        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
       String NewDateFormat=null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }

}




