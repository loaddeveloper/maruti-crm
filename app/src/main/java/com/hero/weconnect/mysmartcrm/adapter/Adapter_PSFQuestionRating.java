package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.B_R_Activity;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;
import com.hero.weconnect.mysmartcrm.models.PSFSOPRatingQuestionsModel;
import com.hero.weconnect.mysmartcrm.models.booking_reminder_model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_PSFQuestionRating extends RecyclerView.Adapter<Adapter_PSFQuestionRating.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    CustomClickListener customListner;

    ArrayList<PSFSOPRatingQuestionsModel.DataDTO> list;

    private Context context;



    public Adapter_PSFQuestionRating(Context context, CustomClickListener customClickListener, ArrayList<PSFSOPRatingQuestionsModel.DataDTO> list) {

        this.context = context;
        this.customListner = customClickListener;
        this.list = list;
        setHasStableIds(true);



    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.rating_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       // holder.setIsRecyclable(true);

        PSFSOPRatingQuestionsModel.DataDTO dataDTO=list.get(position);
        if(dataDTO.getQuestionType().equals("RATING"))
        {
            holder.mTvQuestion.setText((position +1)+". "+dataDTO.getPsfSopQuestion());
            holder.mRatingRbg.setVisibility(View.VISIBLE);
            holder.mSopRbg.setVisibility(View.GONE);

            holder.mRatingRbg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(i>0) {
                        customListner.getQuestionAnswerClick(dataDTO.getPsfSopQuestion(), dataDTO.getId(), dataDTO.getQuestionType(), "" + holder.mRatingRbg.getSelectedItem());
                    }else
                    {
                        customListner.getQuestionAnswerClickRemove(dataDTO.getPsfSopQuestion(), dataDTO.getId(), dataDTO.getQuestionType(), "" +holder.mSopRbg.getSelectedItem());
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });



        }else
        {
            holder.mTvQuestion.setText((position +1)+". "+dataDTO.getPsfSopQuestion());
            holder.mRatingRbg.setVisibility(View.GONE);
            holder.mSopRbg.setVisibility(View.VISIBLE);

            holder.mSopRbg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(i>0) {
                        customListner.getQuestionAnswerClick(dataDTO.getPsfSopQuestion(), dataDTO.getId(), dataDTO.getQuestionType(), "" +holder.mSopRbg.getSelectedItem());
                    }else
                    {
                        customListner.getQuestionAnswerClickRemove(dataDTO.getPsfSopQuestion(), dataDTO.getId(), dataDTO.getQuestionType(), "" +holder.mSopRbg.getSelectedItem());

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });




        }






    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return list.size();
    }




    public interface CustomClickListener {


        void getQuestionAnswerClick(String Question,String  QuestionId,String Type,String Answer);
        void getQuestionAnswerClickRemove(String Question,String  QuestionId,String Type,String Answer);


    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Variable Declare
        private TextView mTvQuestion;
        private Spinner mRatingRbg;

        private Spinner mSopRbg;


        public ViewHolder(View rootView) {
            super(rootView);
            mTvQuestion = rootView.findViewById(R.id.tv_question);
            mRatingRbg = rootView.findViewById(R.id.rating_spinner);

            mSopRbg = rootView.findViewById(R.id.sop_spinner);

        }
    }


}




