package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.adapter.AdapterDSEData;
import com.hero.weconnect.mysmartcrm.adapter.AdapterSettingTemplet;
import com.hero.weconnect.mysmartcrm.models.AllotedDSEModel;
import com.hero.weconnect.mysmartcrm.models.DataAllotedModel;
import com.hero.weconnect.mysmartcrm.models.GetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.ReportingModel;
import com.hero.weconnect.mysmartcrm.models.SetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.TokenModel;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import es.dmoral.toasty.Toasty;

public class DsetodayReport extends AppCompatActivity implements ApiResponseListener,AdapterDSEData.CustomButtonListener {
    RecyclerView recyclerView;
    AdapterDSEData adapterDSEData;
    ArrayList<DataAllotedModel.DataDTO> dataDTOArrayList = new ArrayList<>();
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    CustomDialog customWaitingDialog;
    HashMap<String, String> params = new HashMap<>();
    String token,String_DSE;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dsetoday_report);

        initView();
    }

    private void initView() {
        recyclerView = findViewById(R.id.temp_recyclerview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("DSE Today Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        toolbar.inflateMenu(R.menu.dse_menu);



        adapterDSEData  = new AdapterDSEData(this,DsetodayReport.this, dataDTOArrayList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapterDSEData);

        apiController = new ApiController(this,this);
        customWaitingDialog = new CustomDialog(this);
        commonSharedPref = new CommonSharedPref(this);
        customWaitingDialog.show();
        apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);

      /*  token = "bearer " + commonSharedPref.getLoginData().getAccess_token();

        params.put("DealerCode",CommonVariables.dealercode);
        apiController.getAllotedDataList(token,params);
*/

    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

       if (beanTag.matches(ApiConstant.ALLOTEDDATA)) {
            DataAllotedModel commonModel = (DataAllotedModel) superClassCastBean;
         //  Log.e("getdata","Getdata"+commonModel.getData());
           if (commonModel.getData().size()>0)
           {
               for (DataAllotedModel.DataDTO dataBean : commonModel.getData()) {

                   dataDTOArrayList.add(dataBean);
               }
               recyclerView.setAdapter(adapterDSEData);
               customWaitingDialog.dismiss();
               adapterDSEData.notifyDataSetChanged();

           }

            else {
                customWaitingDialog.dismiss();
                Toasty.error(DsetodayReport.this, "Data Not Found....!", Toast.LENGTH_SHORT).show();
            }

        }
       else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
           TokenModel tokenModel = (TokenModel) superClassCastBean;
           commonSharedPref.setAllClear();
           commonSharedPref.setLoginData(tokenModel);
           customWaitingDialog.dismiss();

           token = "bearer " + commonSharedPref.getLoginData().getAccess_token();

           params.put("DealerCode",CommonVariables.dealercode);
           apiController.getAllotedDataList(token,params);



       }

        if (beanTag.matches(ApiConstant.ALLOTEDDSE)) {


            AllotedDSEModel reportingModel=(AllotedDSEModel)superClassCastBean;
            if(reportingModel.getData().size()>0)
            {

                for(AllotedDSEModel.DataDTO dataBean:reportingModel.getData()) {


                    String_DSE = dataBean.getDSE().toString();


                    customWaitingDialog.dismiss();

                }
            }
            else {
                customWaitingDialog.dismiss();
                Toasty.error(DsetodayReport.this, "No Data to Allocate....!", Toast.LENGTH_SHORT).show();
            }









            //      srtextviewtemplate.setText(commonModel.getData());
        }

    }

    @Override
    public void onFailure(String msg) {

    }

    @Override
    public void onError(String msg) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dse_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        } else if (itemId == R.id.action_filter) {



        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getAllotedCustomer(int position, String DSEID) {

        customWaitingDialog.show();
        params.put("Dse",DSEID);
        params.put("DealerCode",CommonVariables.dealercode);
        apiController.getAllotedDSENew(token,params);

        if (String_DSE !=null)
        {
            final Dialog shippingDialog = new Dialog(DsetodayReport.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(true);
            shippingDialog.show();
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            okbutton.setVisibility(View.GONE);
            title.setText("Data Allocated to "+String_DSE+"  DSE");
            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shippingDialog.dismiss();
                }
            });

        }
    }


}