package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class DataAllotedModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"DSE":"10375G01","TotalFollowups":3,"Called":0,"Alloted":0},{"DSE":"10375MADHU","TotalFollowups":2,"Called":0,"Alloted":0},{"DSE":"10375S20","TotalFollowups":4,"Called":0,"Alloted":0}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * DSE : 10375G01
         * TotalFollowups : 3
         * Called : 0
         * Alloted : 0
         */

        @SerializedName("DSE")
        private String DSE;
        @SerializedName("TotalFollowups")
        private int TotalFollowups;
        @SerializedName("Called")
        private int Called;
        @SerializedName("Alloted")
        private int Alloted;

        public String getDSE() {
            return DSE;
        }

        public void setDSE(String DSE) {
            this.DSE = DSE;
        }

        public int getTotalFollowups() {
            return TotalFollowups;
        }

        public void setTotalFollowups(int TotalFollowups) {
            this.TotalFollowups = TotalFollowups;
        }

        public int getCalled() {
            return Called;
        }

        public void setCalled(int Called) {
            this.Called = Called;
        }

        public int getAlloted() {
            return Alloted;
        }

        public void setAlloted(int Alloted) {
            this.Alloted = Alloted;
        }
    }
}
