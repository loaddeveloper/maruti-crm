package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class GetNonTurnUpHistoryModel extends SuperClassCastBean {

    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("sno")
        private Integer sno;
        @SerializedName("contact_status")
        private String contactStatus;
        @SerializedName("new_service_booking")
        private String newServiceBooking;
        @SerializedName("service_request")
        private String serviceRequest;
        @SerializedName("reminder_followup_date")
        private String reminderFollowupDate;
        @SerializedName("Column1")
        private String column1;
        @SerializedName("call_start_datetime")
        private String callStartDatetime;
        @SerializedName("call_end_datetime")
        private String callEndDatetime;
        @SerializedName("calledstatus")
        private String calledstatus;
        @SerializedName("call_bound_type")
        private String callBoundType;
        @SerializedName("User")
        private String user;
        @SerializedName("calledstatus1")
        private String calledstatus1;

        public Integer getSno() {
            return sno;
        }

        public void setSno(Integer sno) {
            this.sno = sno;
        }

        public String getContactStatus() {
            return contactStatus;
        }

        public void setContactStatus(String contactStatus) {
            this.contactStatus = contactStatus;
        }

        public String getNewServiceBooking() {
            return newServiceBooking;
        }

        public void setNewServiceBooking(String newServiceBooking) {
            this.newServiceBooking = newServiceBooking;
        }

        public String getServiceRequest() {
            return serviceRequest;
        }

        public void setServiceRequest(String serviceRequest) {
            this.serviceRequest = serviceRequest;
        }

        public String getReminderFollowupDate() {
            return reminderFollowupDate;
        }

        public void setReminderFollowupDate(String reminderFollowupDate) {
            this.reminderFollowupDate = reminderFollowupDate;
        }

        public String getColumn1() {
            return column1;
        }

        public void setColumn1(String column1) {
            this.column1 = column1;
        }

        public String getCallStartDatetime() {
            return callStartDatetime;
        }

        public void setCallStartDatetime(String callStartDatetime) {
            this.callStartDatetime = callStartDatetime;
        }

        public String getCallEndDatetime() {
            return callEndDatetime;
        }

        public void setCallEndDatetime(String callEndDatetime) {
            this.callEndDatetime = callEndDatetime;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getCallBoundType() {
            return callBoundType;
        }

        public void setCallBoundType(String callBoundType) {
            this.callBoundType = callBoundType;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getCalledstatus1() {
            return calledstatus1;
        }

        public void setCalledstatus1(String calledstatus1) {
            this.calledstatus1 = calledstatus1;
        }
    }
}
