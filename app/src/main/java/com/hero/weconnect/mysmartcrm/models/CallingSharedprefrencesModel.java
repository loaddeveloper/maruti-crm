package com.hero.weconnect.mysmartcrm.models;

public class CallingSharedprefrencesModel {

    String position;
    String color;
    String call;
    String Mobileno;
    String id;
    String startcalling;

    public String getStartcalling() {
        return startcalling;
    }

    public void setStartcalling(String startcalling) {
        this.startcalling = startcalling;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getMobileno() {
        return Mobileno;
    }

    public void setMobileno(String mobileno) {
        Mobileno = mobileno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
