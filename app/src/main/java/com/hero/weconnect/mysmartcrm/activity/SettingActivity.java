package com.hero.weconnect.mysmartcrm.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.adapter.AdapterSettingTemplet;
import com.hero.weconnect.mysmartcrm.models.GetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.SetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.microsoft.appcenter.analytics.Analytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;


public class SettingActivity extends AppCompatActivity implements View.OnClickListener, ApiResponseListener, Switch.OnCheckedChangeListener {

    // Class Veriable Declartion
    LinearLayout srtemplate, salestemplate;
    TextView srtextviewtemplate, salestextviewtemplate, textView_smsservice, header_template;
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    CustomDialog customWaitingDialog;
    HashMap<String, String> params = new HashMap<>();
    String token;
    Switch smsstatus_switch;
    Toolbar toolbar;

    RecyclerView recyclerView;
    AdapterSettingTemplet adapterSettingTemplet;
    ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_activitiy);


        initView();
        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "SettingActivity");
        properties.put("Role", ""+CommonVariables.role);

        Analytics.trackEvent("SettingActivity", properties);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

    }

    // View Intilization Function of Activity Class
    private void initView() {
        smsstatus_switch = (findViewById(R.id.smsService));
        textView_smsservice = (findViewById(R.id.smsOnOff));
        recyclerView = findViewById(R.id.temp_recyclerview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Setting");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        toolbar.inflateMenu(R.menu.setting_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {


                final Dialog shippingDialog = new Dialog(SettingActivity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.add_new_template);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);
                shippingDialog.show();
                ImageView close_btn = shippingDialog.findViewById(R.id.iv_btn_close);
                EditText template_name = shippingDialog.findViewById(R.id.templatename);
                EditText template_body = shippingDialog.findViewById(R.id.body);
                Button done = shippingDialog.findViewById(R.id.setupnewDone);
                Button cancel = shippingDialog.findViewById(R.id.setupnewCancle);


                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (template_name.getText().toString().isEmpty())
                        {
                            template_name.setError("Please input template name");
                        }
                        else if (template_body.getText().toString().isEmpty())
                        {
                            template_body.setError("Please input template Body");
                        }
                        else
                        {
                            updatesmstemplate(token, template_name.getText().toString(), template_body.getText().toString(), CommonVariables.WECONNECT_APP_ROLE, "");
                            shippingDialog.dismiss();

                        }


                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                close_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });

                return false;
            }
        });

        adapterSettingTemplet = new AdapterSettingTemplet(this, templatelist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapterSettingTemplet);

        smsstatus_switch.setOnCheckedChangeListener(this);
        apiController = new ApiController(this,this);
        customWaitingDialog = new CustomDialog(this);
        commonSharedPref = new CommonSharedPref(this);



        customWaitingDialog.show();
        apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);

        // Role Decicde



    }



    // SR POPUP FUNCTION
    public void srlayouttemplate(String templetname, String templateBody, String Id) {

        final Dialog shippingDialog = new Dialog(SettingActivity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.add_new_template);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        ImageView close_btn = shippingDialog.findViewById(R.id.iv_btn_close);

        TextView title = shippingDialog.findViewById(R.id.setupnewtitle);
        title.setText("Update Template ?");
        EditText template_name = shippingDialog.findViewById(R.id.templatename);
        EditText templatebody = shippingDialog.findViewById(R.id.body);
        Button done = shippingDialog.findViewById(R.id.setupnewDone);
        Button cancel = shippingDialog.findViewById(R.id.setupnewCancle);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        template_name.setText(templetname);
        templatebody.setText(templateBody);

        shippingDialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shippingDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                updatesmstemplate(token, template_name.getText().toString(), templatebody.getText().toString(), CommonVariables.WECONNECT_APP_ROLE, Id);
                shippingDialog.dismiss();

            }
        });
    }


    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.GETSMSTATUS)) {
            GetSMSTemplateCommonModel commonModel = (GetSMSTemplateCommonModel) superClassCastBean;
            if (commonModel.getData().matches("YES")) {

                smsstatus_switch.setChecked(true);
                textView_smsservice.setText("ON");
                customWaitingDialog.dismiss();
            } else {
                smsstatus_switch.setChecked(false);
                textView_smsservice.setText("OFF");
                customWaitingDialog.dismiss();
            }
        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customWaitingDialog.dismiss();
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();

            if (CommonVariables.WECONNECT_APP_ROLE.equals("ENQUIRY")) {
                //  header_template.setText("Sales Follow-Up Template");
                getsmstemplate(token, CommonVariables.WECONNECT_APP_ROLE);

            } else if (CommonVariables.WECONNECT_APP_ROLE.equals("SR")) {
                // header_template.setText("Service Reminder Template");
                getsmstemplate(token, CommonVariables.WECONNECT_APP_ROLE);

            }
            getsmsstatus(token);




        }
        else if (beanTag.matches(ApiConstant.SETSMSSTATUS)) {
            SetSMSTemplateCommonModel commonModel = (SetSMSTemplateCommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Success")) {
                customWaitingDialog.dismiss();

            }

        } else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                recyclerView.setAdapter(adapterSettingTemplet);
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
                Toasty.error(SettingActivity.this, "Data Not Found....!", Toast.LENGTH_SHORT).show();
            }


            //      srtextviewtemplate.setText(commonModel.getData());
        } else if (beanTag.matches(ApiConstant.SETSMSTEMPLATE)) {
            SetSMSTemplateCommonModel commonModel = (SetSMSTemplateCommonModel) superClassCastBean;

            if (commonModel.getMessage().matches("Success")) {
                customWaitingDialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        }

    }

    @Override
    public void onFailure(String msg) {

    }

    @Override
    public void onError(String msg) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        } else if (itemId == R.id.action_filter) {



        }
        return super.onOptionsItemSelected(item);
    }


    // Update SMS Status
    public void updatesmsstatus(String token, String smsstatus) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("SmsStatus", smsstatus);
        apiController.UpdateSMSStatus(token, params);
    }

    // Get SMS Status
    public void getsmsstatus(String token) {
        customWaitingDialog.show();
        apiController.GetSMSStatus(token);
    }


    //Get SMS Template
    public void getsmstemplate(String token, String templatetype) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    // Update SMS Template Status
    public void updatesmstemplate(String token, String templatename, String templatebody, String templatetag, String Id) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateName", templatename);
        params.put("TemplateBody", templatebody);
        params.put("TempalteTag", templatetag);
        params.put("ID", Id);
        apiController.UpdateSMSTemplate(token, params);
    }

    //Checked Changed Listener
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            updatesmsstatus(token, "YES");
            textView_smsservice.setText("ON");

        } else {
            updatesmsstatus(token, "NO");
            textView_smsservice.setText("OFF");

        }


    }

    @Override
    public void onClick(View v) {

    }
}