package com.hero.weconnect.mysmartcrm.CallUtils;

import android.os.Build;
import android.telecom.Call;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;


@RequiresApi(api = Build.VERSION_CODES.M)
public class OngoingCall {

    public static final BehaviorSubject<Integer> state;
    private static final Call.Callback callback;
    public static Call call;
    public static ArrayList<Call> call2 = new ArrayList<Call>();
    public static ArrayList<Call> call4 = new ArrayList<Call>();
    private static Call calleess;
    private static List<Call> call3;

    static {
        // Create a BehaviorSubject to subscribe
        state = BehaviorSubject.create();
        callback = new Call.Callback() {
            public void onStateChanged(Call call, int newState) {

                // Change call state
                try {
                    OngoingCall.state.onNext(newState);
                } catch (Exception e) {

                }

                //call2.add(call);
            }
        };
    }

    // Anwser the call
    public static void answer() {
        try {
            Call ssss = call4.get(0);
            ssss.answer(0);
        } catch (Exception e) {
           // ////Log.e("receiving ", e.getMessage());

        }
        try {

            call.answer(0);
        } catch (Exception e) {
           // ////Log.e("receiving 2", e.getMessage());

        }


    }

    public static void unhold() {
        try {
            Call css = call2.get(0);
            css.unhold();
        } catch (Exception e) {

        }
    }


    // Hangup the call
    public static void hangup() {
        try {
            call.disconnect();
        } catch (Exception e) {

        }
        try {
            Call css = call2.get(0);
            css.disconnect();
            call2.clear();
        } catch (Exception e) {

        }


    }

    public static void cendsprensCalls() {


        if (call4.size() > 0)
        {
            try {
                Call ccc = call4.get(0);
               // ////Log.e("Disconnect", String.valueOf(ccc));
                ccc.disconnect();
                call4.clear();
            } catch (Exception e) {

            }
            try {
                call.disconnect();
            } catch (Exception e) {

            }
        }else
        {
            try {
                Call ccc = call2.get(0);
               // ////Log.e("Disconnect", String.valueOf(ccc));
                ccc.disconnect();
                call2.clear();
            } catch (Exception e) {

            }
            try {
                call.disconnect();
            } catch (Exception e) {

            }
        }
    }

    public final BehaviorSubject getState() {
        return state;
    }

    public final void setCall(Call value) {
        if (call != null) {
            call.unregisterCallback(callback);
        }

        if (value != null) {
            value.registerCallback(callback);
            state.onNext(value.getState());
        }

        call = value;

    }



}




