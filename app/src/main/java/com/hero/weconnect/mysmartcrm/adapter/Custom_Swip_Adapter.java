package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.hero.weconnect.mysmartcrm.R;

public class Custom_Swip_Adapter extends PagerAdapter {

    public int[] image_resources={
            R.drawable.xl6,
            R.drawable.celerio,
            R.drawable.baleno,
            R.drawable.swift_dezire,

    };

    private Context ctx;
    private LayoutInflater layoutInflater;
    public Custom_Swip_Adapter(Context ctx)
    {
        this.ctx=ctx;
    }

    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view== object);
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position)

    {
        layoutInflater =(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.swipe_page,container,false);
        ImageView img= view.findViewById(R.id.image_viewpager);
       // img.setImageResource(image_resources[position]);

        Glide.with(ctx)
                .load(image_resources[position])
                .into(img);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
