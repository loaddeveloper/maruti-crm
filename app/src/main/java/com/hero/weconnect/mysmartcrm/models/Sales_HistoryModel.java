package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class Sales_HistoryModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-29T16:28:55.06","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":2,"followup_status":"","next_followup_date":"2020-10-07T16:17:58","expected_date_of_purchase":"2020-10-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"Model","remark":"Call back Later","call_start_datetime":"2020-09-29T16:17:47.693","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":3,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-29T16:06:58.893","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":4,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-24T10:41:09.113","call_end_datetime":"1900-01-01T00:00:00","User":"TM10800","calledstatus":"CALL DONE"},{"sno":5,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-24T10:13:04.51","call_end_datetime":"2020-09-24T10:13:14.217","User":"TM10800","calledstatus":"CALL DONE"},{"sno":6,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-24T10:12:04.187","call_end_datetime":"2020-09-24T10:12:59.527","User":"TM10800","calledstatus":"BUSY"},{"sno":7,"followup_status":"Open","next_followup_date":"2020-09-24T18:39:52","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"KM not covered","call_start_datetime":"2020-09-23T18:39:35.18","call_end_datetime":"2020-09-23T18:40:16.453","User":"TM10800","calledstatus":"CALL DONE"},{"sno":8,"followup_status":"Open","next_followup_date":"2020-09-24T18:28:45","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"Need to change auto suggest remark","call_start_datetime":"2020-09-23T18:27:52.67","call_end_datetime":"2020-09-23T18:29:49.03","User":"TM10800","calledstatus":"CALL DONE"},{"sno":9,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-23T18:24:22.697","call_end_datetime":"2020-09-23T18:26:18.44","User":"TM10800","calledstatus":"BUSY"},{"sno":10,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-23T16:21:54.18","call_end_datetime":"2020-09-23T16:21:59.74","User":"TM10800","calledstatus":"BUSY"},{"sno":11,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T17:25:38.503","call_end_datetime":"2020-09-22T18:45:49.82","User":"test","calledstatus":"BUSY"},{"sno":12,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:43:26.297","call_end_datetime":"2020-09-22T16:48:03.74","User":"test","calledstatus":"BUSY"},{"sno":13,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:29:24.443","call_end_datetime":"2020-09-22T16:31:07.51","User":"test","calledstatus":"BUSY"},{"sno":14,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:18:22.243","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":15,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:15:20.84","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":16,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:11:40.133","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":17,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T16:08:33.103","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":18,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T15:55:06.033","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":19,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T15:49:49.43","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":20,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T15:48:21.69","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":21,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T15:32:03.507","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":22,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T15:24:22.517","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":23,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-22T11:35:04.253","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":24,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T16:32:09.403","call_end_datetime":"2020-09-19T16:32:20","User":"test","calledstatus":"BUSY"},{"sno":25,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T16:22:27.277","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":26,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T16:22:11.967","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":27,"followup_status":"","next_followup_date":"2020-09-10T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"busy","call_start_datetime":"2020-09-19T16:19:38.68","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":28,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T16:06:48.797","call_end_datetime":"2020-09-19T16:18:01.89","User":"test","calledstatus":"CALL DONE"},{"sno":29,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T15:59:50.27","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE"},{"sno":30,"followup_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T15:59:13.77","call_end_datetime":"2020-09-19T16:00:09.867","User":"test","calledstatus":"CALL DONE"},{"sno":31,"followup_status":"","next_followup_date":"2020-09-10T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"Proximity","make":"ENFIELD","model":"GLAMOUR FI","remark":"Busy","call_start_datetime":"2020-09-19T15:12:02.68","call_end_datetime":"2020-09-19T15:12:45.123","User":"test","calledstatus":"CALL DONE"},{"sno":32,"followup_status":"","next_followup_date":"2020-09-10T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"Proximity","make":"PIAGGIO","model":"GLAMOUR FI","remark":"Call back Later","call_start_datetime":"2020-09-19T15:00:37.72","call_end_datetime":"2020-09-19T15:01:58.363","User":"test","calledstatus":"CALL DONE"},{"sno":33,"followup_status":"Open","next_followup_date":"2020-09-20T14:42:28.617","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-19T14:15:18.55","call_end_datetime":"2020-09-19T14:16:20.62","User":"test","calledstatus":"CALL DONE"},{"sno":34,"followup_status":"Open","next_followup_date":"2020-09-27T03:25:20","expected_date_of_purchase":"2020-09-25T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"Switch OFF","call_start_datetime":"2020-09-18T12:25:14.88","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":35,"followup_status":"Open","next_followup_date":"2020-09-26T12:24:04","expected_date_of_purchase":"2020-09-26T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"Busy","call_start_datetime":"2020-09-18T12:23:45.423","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"BUSY"},{"sno":36,"followup_status":"Open","next_followup_date":"2020-09-20T14:42:28.617","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-17T18:40:38.213","call_end_datetime":"2020-09-17T18:44:09.78","User":"test","calledstatus":"CALL DONE"},{"sno":37,"followup_status":"Open","next_followup_date":"2020-09-20T14:42:28.617","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","call_start_datetime":"2020-09-17T18:40:15.663","call_end_datetime":"2020-09-17T18:44:09.403","User":"test","calledstatus":"CALL DONE"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * followup_status :
         * next_followup_date : 1900-01-01T00:00:00
         * expected_date_of_purchase : 1900-01-01T00:00:00
         * closurereason :
         * closure_sub_reason :
         * make :
         * model :
         * remark :
         * call_start_datetime : 2020-09-29T16:28:55.06
         * call_end_datetime : 1900-01-01T00:00:00
         * User : test
         * calledstatus : CALL DONE
         */

        private int sno;
        private String followup_status;
        private String next_followup_date;
        private String expected_date_of_purchase;
        private String closurereason;
        private String closure_sub_reason;
        private String make;
        private String model;
        private String remark;
        private String call_start_datetime;
        private String call_end_datetime;
        private String User;
        private String calledstatus;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getFollowup_status() {
            return followup_status;
        }

        public void setFollowup_status(String followup_status) {
            this.followup_status = followup_status;
        }

        public String getNext_followup_date() {
            return next_followup_date;
        }

        public void setNext_followup_date(String next_followup_date) {
            this.next_followup_date = next_followup_date;
        }

        public String getExpected_date_of_purchase() {
            return expected_date_of_purchase;
        }

        public void setExpected_date_of_purchase(String expected_date_of_purchase) {
            this.expected_date_of_purchase = expected_date_of_purchase;
        }

        public String getClosurereason() {
            return closurereason;
        }

        public void setClosurereason(String closurereason) {
            this.closurereason = closurereason;
        }

        public String getClosure_sub_reason() {
            return closure_sub_reason;
        }

        public void setClosure_sub_reason(String closure_sub_reason) {
            this.closure_sub_reason = closure_sub_reason;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCall_start_datetime() {
            return call_start_datetime;
        }

        public void setCall_start_datetime(String call_start_datetime) {
            this.call_start_datetime = call_start_datetime;
        }

        public String getCall_end_datetime() {
            return call_end_datetime;
        }

        public void setCall_end_datetime(String call_end_datetime) {
            this.call_end_datetime = call_end_datetime;
        }

        public String getUser() {
            return User;
        }

        public void setUser(String User) {
            this.User = User;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
