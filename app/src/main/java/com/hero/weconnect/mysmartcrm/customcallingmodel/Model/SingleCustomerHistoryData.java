package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class SingleCustomerHistoryData extends SuperClassCastBean {


    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Id":"f667e397-8233-4692-9dc6-000402be2bfb","Data1":"","Data2":"","Data3":"","Data4":"","Data5":"","Data6":"","Data7":"","Remark":"","NextFollowupdate":"1900-01-01T00:00:00","BookingDate":"2020-11-23T00:00:00","CallingStatus":"","Status":"","HistoryId":"f667e397-8233-4692-9dc6-000402be2bfc"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Id : f667e397-8233-4692-9dc6-000402be2bfb
         * Data1 :
         * Data2 :
         * Data3 :
         * Data4 :
         * Data5 :
         * Data6 :
         * Data7 :
         * Remark :
         * NextFollowupdate : 1900-01-01T00:00:00
         * BookingDate : 2020-11-23T00:00:00
         * CallingStatus :
         * Status :
         * HistoryId : f667e397-8233-4692-9dc6-000402be2bfc
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("Data1")
        private String Data1;
        @SerializedName("Data2")
        private String Data2;
        @SerializedName("Data3")
        private String Data3;
        @SerializedName("Data4")
        private String Data4;
        @SerializedName("Data5")
        private String Data5;
        @SerializedName("Data6")
        private String Data6;
        @SerializedName("Data7")
        private String Data7;
        @SerializedName("Remark")
        private String Remark;
        @SerializedName("NextFollowupdate")
        private String NextFollowupdate;
        @SerializedName("BookingDate")
        private String BookingDate;
        @SerializedName("CallingStatus")
        private String CallingStatus;
        @SerializedName("Status")
        private String Status;
        @SerializedName("HistoryId")
        private String HistoryId;


        @SerializedName("DateValue")
        private String DateValue;

        @SerializedName("DateValueend")
        private String DateValueend;

        public String getDateValueend() {
            return DateValueend;
        }

        public void setDateValueend(String dateValueend) {
            DateValueend = dateValueend;
        }

        public String getDateValue() {
            return DateValue;
        }

        public void setDateValue(String dateValue) {
            DateValue = dateValue;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public String getData1() {
            return Data1;
        }

        public void setData1(String Data1) {
            this.Data1 = Data1;
        }

        public String getData2() {
            return Data2;
        }

        public void setData2(String Data2) {
            this.Data2 = Data2;
        }

        public String getData3() {
            return Data3;
        }

        public void setData3(String Data3) {
            this.Data3 = Data3;
        }

        public String getData4() {
            return Data4;
        }

        public void setData4(String Data4) {
            this.Data4 = Data4;
        }

        public String getData5() {
            return Data5;
        }

        public void setData5(String Data5) {
            this.Data5 = Data5;
        }

        public String getData6() {
            return Data6;
        }

        public void setData6(String Data6) {
            this.Data6 = Data6;
        }

        public String getData7() {
            return Data7;
        }

        public void setData7(String Data7) {
            this.Data7 = Data7;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public String getNextFollowupdate() {
            return NextFollowupdate;
        }

        public void setNextFollowupdate(String NextFollowupdate) {
            this.NextFollowupdate = NextFollowupdate;
        }

        public String getBookingDate() {
            return BookingDate;
        }

        public void setBookingDate(String BookingDate) {
            this.BookingDate = BookingDate;
        }

        public String getCallingStatus() {
            return CallingStatus;
        }

        public void setCallingStatus(String CallingStatus) {
            this.CallingStatus = CallingStatus;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getHistoryId() {
            return HistoryId;
        }

        public void setHistoryId(String HistoryId) {
            this.HistoryId = HistoryId;
        }
    }
}
