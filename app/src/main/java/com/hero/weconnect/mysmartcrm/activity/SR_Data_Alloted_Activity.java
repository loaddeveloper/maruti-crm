package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.adapter.AdapterDSEData;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Data;
import com.hero.weconnect.mysmartcrm.models.DataAllotedModel;
import com.hero.weconnect.mysmartcrm.models.SR_Alloted_data_model;
import com.hero.weconnect.mysmartcrm.models.TokenModel;

import java.util.ArrayList;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;

public class SR_Data_Alloted_Activity extends AppCompatActivity implements ApiResponseListener,Adapter_SR_Data.CustomButtonListener {

    RecyclerView recyclerView;
    Adapter_SR_Data adapterDSEData;
    ArrayList<SR_Alloted_data_model.DataDTO> dataDTOArrayList = new ArrayList<>();
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    CustomDialog customWaitingDialog;
    HashMap<String, String> params = new HashMap<>();
    String token,String_DSE;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_r__data__alloted_);
        initView();
    }

    private void initView() {

        recyclerView = findViewById(R.id.temp_recyclerview);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Alloted Data Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.inflateMenu(R.menu.dse_menu);
        adapterDSEData  = new Adapter_SR_Data(this,SR_Data_Alloted_Activity.this, dataDTOArrayList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapterDSEData);
        apiController = new ApiController(this,this);
        customWaitingDialog = new CustomDialog(this);
        commonSharedPref = new CommonSharedPref(this);

        customWaitingDialog.show();
        apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);




    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.ALLOTED_SR)) {
            SR_Alloted_data_model commonModel = (SR_Alloted_data_model) superClassCastBean;
            //  Log.e("getdata","Getdata"+commonModel.getData());
            if (commonModel.getData().size()>0)
            {
                for (SR_Alloted_data_model.DataDTO dataBean : commonModel.getData()) {

                    dataDTOArrayList.add(dataBean);
                }
                recyclerView.setAdapter(adapterDSEData);
                customWaitingDialog.dismiss();
                adapterDSEData.notifyDataSetChanged();

            }

            else {
                customWaitingDialog.dismiss();
                Toasty.error(SR_Data_Alloted_Activity.this, "Data Not Found....!", Toast.LENGTH_SHORT).show();
            }

        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customWaitingDialog.dismiss();
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
            params.put("DealerCode", CommonVariables.dealercode);
            apiController.getAllotedDataSR(token,params);






        }
    }

    @Override
    public void onFailure(String msg) {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void getAllotedCustomer(int position, String DSEID) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dse_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
        } else if (itemId == R.id.action_filter) {



        }
        return super.onOptionsItemSelected(item);
    }
}