package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CustomHistoryModel;

import java.util.ArrayList;


public class Adapter_SR_Historydatacustom extends RecyclerView.Adapter<Adapter_SR_Historydatacustom.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    public static String srtemplate;
    static Typeface tf;
    ArrayList<CustomHistoryModel.DataDTO> srhistorydatalist = new ArrayList<>();
    Adapter_SR_Historydata adapter;
    int ye, day, mon;
    int hh = 0;
    int size = 0;
    private Context context;


    public Adapter_SR_Historydatacustom(Context context, ArrayList<CustomHistoryModel.DataDTO> datalist) {

        this.context = context;
        this.srhistorydatalist = datalist;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.history_sr_popup_cardview, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_SR_Historydatacustom.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        CustomHistoryModel.DataDTO dataBean = srhistorydatalist.get(position);
        holder.tv_srnumber.setText(""+dataBean.getSerialNo());
        holder.customer_name.setText("" + dataBean.getData1());
        holder.calledstatus.setText("" + dataBean.getCalledStatus().toUpperCase());
        holder.reminder_followup_date.setText("" + dataBean.getFollowUpDate().replaceAll("T00:00:00", "").toUpperCase());
        holder.service_booking.setText("" + dataBean.getBookingDate().replaceAll("T00:00:00", "").toUpperCase());
        holder.remarks.setText("" + dataBean.getRemark().toUpperCase());
        holder.contact_status.setText("" + dataBean.getData2().toUpperCase());
        holder.service_request.setText("" + dataBean.getCallEndTime().toUpperCase());

        holder.mainsview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog shippingDialog1 = new Dialog(context);
                shippingDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog1.setContentView(R.layout.history_moredetails_cardview_sr);
                shippingDialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog1.setCancelable(true);
                shippingDialog1.setCanceledOnTouchOutside(false);

                TextView closebtn;
                EditText customer_name,tv_srnumber,contact_status,created_at,service_booking,service_request,tageged_sr_status,contact_changed,reminder_followup_date,
                        remarks,not_comming_reason,pickup_drop,call_start_datetime,call_end_datetime,calledstatus;

                customer_name = shippingDialog1.findViewById(R.id.customer_name);
                contact_status = shippingDialog1.findViewById(R.id.contact_status);
                created_at = shippingDialog1.findViewById(R.id.created_at);
                service_booking = shippingDialog1.findViewById(R.id.service_booking);
                service_request = shippingDialog1.findViewById(R.id.service_request);
                tageged_sr_status = shippingDialog1.findViewById(R.id.tageged_sr_status);
                contact_changed = shippingDialog1.findViewById(R.id.contact_changed);
                reminder_followup_date = shippingDialog1.findViewById(R.id.reminder_followup_date);
                remarks = shippingDialog1.findViewById(R.id.remarks);
                not_comming_reason = shippingDialog1.findViewById(R.id.not_comming_reason);
                pickup_drop = shippingDialog1.findViewById(R.id.pickup_drop);
                call_start_datetime = shippingDialog1.findViewById(R.id.call_start_datetime);
                call_end_datetime = shippingDialog1.findViewById(R.id.call_end_datetime);
                calledstatus = shippingDialog1.findViewById(R.id.calledstatus);
                closebtn=shippingDialog1.findViewById(R.id.txtclose);
                closebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog1.dismiss();
                    }
                });



                customer_name.setText("" );
                calledstatus.setText("" + dataBean.getCalledStatus());
                call_start_datetime.setText("" + dataBean.getCallStartTime().replaceAll("T00:00:00", ""));
                call_end_datetime.setText("" + dataBean.getCallEndTime().replaceAll("T00:00:00", ""));
                reminder_followup_date.setText("" + dataBean.getFollowUpDate().replaceAll("T00:00:00", ""));
                service_booking.setText("" + dataBean.getBookingDate().replaceAll("T00:00:00", ""));
                remarks.setText("" + dataBean.getRemark());
                contact_status.setText("" + dataBean.getCalledStatus());
                created_at.setText("" );
                not_comming_reason.setText("" );
                tageged_sr_status.setText("" );
                contact_changed.setText("" );
                pickup_drop.setText("" );
                service_request.setText("" );


                shippingDialog1.show();

            }
        });


    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return srhistorydatalist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView customer_name;
        public TextView tv_srnumber;
        public TextView contact_status;
        public TextView created_at;
        public TextView service_booking;
        public TextView service_request;
        public TextView tageged_sr_status;
        public TextView contact_changed;
        public TextView reminder_followup_date;
        public TextView remarks;
        public TextView not_comming_reason;
        public TextView pickup_drop;
        public TextView call_start_datetime;
        public TextView call_end_datetime;
        public TextView calledstatus;
        public LinearLayout mainsview;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.customer_name = rootView.findViewById(R.id.customer_name);
            this.tv_srnumber = rootView.findViewById(R.id.tv_srnumber);
            this.contact_status = rootView.findViewById(R.id.contact_status);
            this.service_booking = rootView.findViewById(R.id.service_booking);
            this.service_request = rootView.findViewById(R.id.service_request);
            this.remarks = rootView.findViewById(R.id.remarks);
            this.reminder_followup_date = rootView.findViewById(R.id.reminder_followup_date);
            this.calledstatus = rootView.findViewById(R.id.calledstatus);
            this.mainsview = rootView.findViewById(R.id.mainsview);
        }
    }


}