package com.hero.weconnect.mysmartcrm.Utils;

import android.media.MediaRecorder;

import com.github.squti.androidwaverecorder.WaveRecorder;
import com.hero.weconnect.mysmartcrm.CallUtils.WavRecorder;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.models.AllocationDataModel;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class CommonVariables {


    public static String LOC2 = "";
    public static String WeConnect_User_Name = "";
    public static String WeConnect_Password = "";
    public static MediaRecorder recorder=new MediaRecorder() ;
    public static WaveRecorder wavRecorder ;

    public static String token = "";
    public static String New_token_for_template = "";
    public static String customtoken = "";
    public static String customtokenbearer = "bearer "+customtoken;



    public static float RAM = 0;
    public static float STORAGE =0;
    public static String AppCenter_URL ="";


    public static String username = "";
    public static String role = "";
    public static String dealercode = "";
    public static String pagename = "";
    public static String password = "";
    public static String UserId = "";
    public static String searchword = "";
    public static String filtertype = "All";
    public static String SALES_FOLLOW_UP = "ENQUIRY";
    public static String callDelay = "0";
    public static int morecount = 0;
    public  static long calldelaytime=0;
    public static String SALES_FOLLOW_UPTYPE = "Today";
    public static String APP = "";
    public static String LOC = "";
    public static String userName = "";
    public static File audiofile=null;
    public static File audiofilewave=null;
    public static AllocationDataModel superClassCastBean=null;
    public static WaveRecorder waveRecorder=null;

    public static String filetfinancerequired = "";
    public static String filterexchnage = "";
    public static boolean Is_LongPress = false;

    public static CopyOnWriteArrayList<String> filermodellist= new CopyOnWriteArrayList<>();
    public static CopyOnWriteArrayList<String> filerenqsourcelist= new CopyOnWriteArrayList<>();
    public static boolean receivecall = false, endcallclick = false, incoming=false,dashboard=false,isCalldoneChecked=false,isCallWaiting=false,incomingCrash=false;

//sales

//  public static String SR_Testingnumber = "9891917275";
   /* public static String SALES_Testingnumber = "9891917275";
    public static String SR_Testingnumber = "9663621310";*/

//
//
//    public static String SR_Testingnumber = "9772661128";
//
//    public static String SALES_Testingnumber = "9772661128";



    /*  public static String SR_Testingnumber = "7976085985";
      public static String SALES_Testingnumber = "7976085985";*/
    public static String App_IdentifyService = "HJC";
     //public static String App_IdentifyService = "HJCPT";
       public static String mobilenumber = "";


        public static String SR_Testingnumber = "7300001608";
        public static String SALES_Testingnumber = "7300001608";


/*
    public static String SR_Testingnumber = "8209388849";
    public static String SALES_Testingnumber = "8209388849";
*/



//    public static String SR_Testingnumber = "8432177399";
//    public static String SALES_Testingnumber = "8432177399";


//    public static String SR_Testingnumber = "9115831958";
//    public static String SALES_Testingnumber = "9115831958";

//sr
//     public static String SALES_Testingnumber = "9663621310";




    // WE Connect APP Role
    public static String WECONNECT_APP_ROLE = "";
    // Service APP Roles
    public static String SR_Role1 = "FLS";
    public static String SR_Role2 = "CCE";

    // Sales APP Roles
    public static String Sales_Role1 = "RSE";
    public static String Sales_Role2 = "DSE";
    public static String Sales_Role3 = "DSM";
    public static String Sales_Role4 = "CCE";

    // APP Roles
    public static String APP_Role_1 = "service";
    public static String APP_Role_2 = "sales";
    public static String incomingNumber = "";

}



