package com.hero.weconnect.mysmartcrm.customretrofit.Retrofit;

import android.content.Context;
import android.content.SharedPreferences;

public class Common_Calling_Color_SharedPreferances {

    private Context mContext;
    private SharedPreferences.Editor editor, editorceer, editorpostion, call_start_stop_editor;
    private SharedPreferences sharedPreferences, sharedPreferencesceer, sharedPreferencesposition, callstart_stop_sharedPreferences;

    public Common_Calling_Color_SharedPreferances(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
        sharedPreferencesceer = mContext.getSharedPreferences("ceer", Context.MODE_PRIVATE);
        sharedPreferencesposition = mContext.getSharedPreferences("Position", Context.MODE_PRIVATE);
        callstart_stop_sharedPreferences = mContext.getSharedPreferences("start", Context.MODE_PRIVATE);

    }

    public String getPosition() {

        String position = sharedPreferences.getString(ApiConstant.SHAREDPREFERANCESPOSITION, null);
        return position;

    }

    public void setPosition(String position) {
        editor = sharedPreferences.edit();
        editor.putString(ApiConstant.SHAREDPREFERANCESPOSITION, position);
        editor.commit();
    }

    public String getcallcurrentstatus() {

        String calledstatus = sharedPreferences.getString(ApiConstant.SHAREDPREFERANCECALLCURRENTSTATUS, null);
        return calledstatus;
    }

    public void setcallcurrentstatus(String calledstatus) {
        editor = sharedPreferences.edit();
        editor.putString(ApiConstant.SHAREDPREFERANCECALLCURRENTSTATUS, calledstatus);
        editor.commit();

    }

    public String getcurrentcallmobileno() {
        String current_call_mobileno = sharedPreferences.getString(ApiConstant.SHAREDPREFERANCESCALLCURRENTMOBILENO, null);
        return current_call_mobileno;

    }

    public void setcurrentcallmobileno(String current_call_mobileno) {
        editor = sharedPreferences.edit();
        editor.putString(ApiConstant.SHAREDPREFERANCESCALLCURRENTMOBILENO, current_call_mobileno);
        editor.commit();

    }

    public String getmatserid() {
        String current_call_masterId = sharedPreferences.getString(ApiConstant.SHAREDPREFERANCESMASTETID, null);
        return current_call_masterId;

    }

    public void setmasterid(String current_call_masterId) {
        editor = sharedPreferences.edit();
        editor.putString(ApiConstant.SHAREDPREFERANCESMASTETID, current_call_masterId);
        editor.commit();

    }

    public String getSharePreferancesContains() {
        String contains = sharedPreferencesposition.getString(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES, null);
        return contains;

    }

    public void setContains(String contains) {
        editorpostion = sharedPreferencesposition.edit();
        editorpostion.putString(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES, contains);
        editorpostion.commit();

    }

    public void clearposition() {
        if (editorpostion != null) {
            editorpostion = sharedPreferencesposition.edit();
            editorpostion.clear();
            editorpostion.commit();
            editorpostion.apply();
        }

    }

    public String getSharePreferancesContainsSheer() {
        String contains = sharedPreferencesceer.getString(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE, null);
        return contains;

    }

    public void setContainsSheer(String contains) {
        editorceer = sharedPreferencesceer.edit();
        editorceer.putString(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE, contains);
        editorceer.commit();

    }

    public void clearSheer() {
        if (editorceer != null) {
            editorceer = sharedPreferencesceer.edit();
            editorceer.clear();
            editorceer.commit();
            editorceer.apply();
        }

    }

    public String getCall_start_stop_status() {
        String call_start_stop = callstart_stop_sharedPreferences.getString(ApiConstant.CALL_START_STOP_SHAREDPREANCES, null);
        return call_start_stop;

    }

    public void setCall_start_stop_status(String call_start_stop) {
        call_start_stop_editor = callstart_stop_sharedPreferences.edit();
        call_start_stop_editor.putString(ApiConstant.CALL_START_STOP_SHAREDPREANCES, call_start_stop);
        call_start_stop_editor.commit();

    }

    public void clear__start_stop_status() {
        if (call_start_stop_editor != null) {
            call_start_stop_editor = callstart_stop_sharedPreferences.edit();
            call_start_stop_editor.clear();
            call_start_stop_editor.commit();
            call_start_stop_editor.apply();
        }

    }


    public void clearcommoncallingsharedpreferances() {
        if (editor != null) {
            editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
            editor.apply();
        }

    }


}
