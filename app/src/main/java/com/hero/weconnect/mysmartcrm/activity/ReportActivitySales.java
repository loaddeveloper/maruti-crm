package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.models.ReportingModel;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.analytics.Analytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


public class ReportActivitySales extends AppCompatActivity implements ApiResponseListener,View.OnClickListener {
    CardView todate,fromdate;
    EditText et_todate,et_fromdate;
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    TextView totalavilablecalls,totalcalls,calldone,callnotdone,folowup,enqdoneonhc,followupclosed;
    String dateto,datefrom,token,callType;
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    Date date1;
    public ArrayAdapter<String> userArrayAdapter;
    ArrayList<String>[] userArrayList;

    private SwitchDateTimeDialogFragment dateTimeFragment;
    Spinner sp_user_report,enq_type_report;
    CustomDialog customDialog;
    Calendar calendar;
    String myFormat = "MM-dd-yyyy"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    Calendar myCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_sales);

        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+ CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "ReportActivitySales");
        properties.put("Role", ""+CommonVariables.role);

        Analytics.trackEvent("ReportActivitySales", properties);
        init();

    }


    void init()
    {

        apiController = new ApiController(this,this);
        fromdate = (CardView) findViewById(R.id.fromdate);
        todate = (CardView) findViewById(R.id.todate);
        sp_user_report = (Spinner) findViewById(R.id.sp_user_report);
        enq_type_report = (Spinner) findViewById(R.id.enq_type_report);
        et_fromdate = (EditText) findViewById(R.id.fromtxtdate);
        et_todate = (EditText) findViewById(R.id.totxtdate);
        totalavilablecalls = (TextView) findViewById(R.id.totalavilablecalls);
        totalcalls = (TextView) findViewById(R.id.totalcalls);
        calldone = (TextView) findViewById(R.id.calldone);
        callnotdone = (TextView) findViewById(R.id.callnotdone);
        folowup = (TextView) findViewById(R.id.folowup);
        enqdoneonhc = (TextView) findViewById(R.id.enqdoneonhc);
        followupclosed = (TextView) findViewById(R.id.followupclosed);
        commonSharedPref = new CommonSharedPref(this);
        customDialog = new CustomDialog(this);
        userArrayList= new ArrayList[]{new ArrayList<String>()};
        calendar = Calendar.getInstance();
        et_fromdate.setText(sdf.format(myCalendar.getTime()));
        et_todate.setText(sdf.format(myCalendar.getTime()));
        datefrom=sdf.format(myCalendar.getTime());
        dateto=sdf.format(myCalendar.getTime());
        userArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,userArrayList[0]);
        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Unauthorized access !!", Toast.LENGTH_SHORT).show();            return;
        }

        token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
        CommonVariables.token=token;
        Toolbar toolbar = findViewById(R.id.dashtoolbar);
        toolbar.setTitle("Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        enq_type_report.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch(position)
                {
                    case 0:
                        callType="All";
                        getReportingdata(token,datefrom,dateto,""+callType);
                        break;

                    case 1:
                        callType="Today";

                        getReportingdata(token,datefrom,dateto,""+callType);
                        break;

                    case 2:
                        callType="Pending";

                        getReportingdata(token,datefrom,dateto,""+callType);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                callType="All";



            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        et_fromdate.setOnClickListener(ReportActivitySales.this);
        et_todate.setOnClickListener(ReportActivitySales.this);
        fromdate.setOnClickListener(ReportActivitySales.this);
        todate.setOnClickListener(ReportActivitySales.this);

        arrayAdapterSetUp();



    }


    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {
        if(beanTag.matches(ApiConstant.REPORTINGDATA))
        {
            ReportingModel reportingModel=(ReportingModel)superClassCastBean;
            if(reportingModel.getData().size()>0)
            {

                for(ReportingModel.DataBean dataBean:reportingModel.getData()) {


                    totalavilablecalls.setText(""+dataBean.getTotalCustomer());
                    totalcalls.setText(""+dataBean.getTotalCalls());
                    folowup.setText(""+dataBean.getFollowups());
                    enqdoneonhc.setText(""+dataBean.getEntryDoneOnHc());
                    calldone.setText(""+dataBean.getCallDone());
                    callnotdone.setText(""+dataBean.getCallNotDone());
                    followupclosed.setText(""+dataBean.getClosedFollowups());


                }
            }

            customDialog.dismiss();

        }

    }

    @Override
    public void onFailure(String msg) {
        customDialog.dismiss();

    }

    @Override
    public void onError(String msg) {
        customDialog.dismiss();

    }


    public  void getReportingdata(String token,String startdate,String enddate,String followuptype)
    {
        customDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("StartDate",startdate);
        params.put("EndDate",enddate);
        params.put("FollowupType",followuptype);

        apiController.getReportingData(token,params);


    }

    public void arrayAdapterSetUp()
    {

        userArrayList= new ArrayList[]{new ArrayList<String>()};
        userArrayList[0].add(""+CommonVariables.role);
        userArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,userArrayList[0]);
        sp_user_report.setAdapter(userArrayAdapter);
        sp_user_report.setEnabled(false);


    }
    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id==R.id.fromtxtdate || id==R.id.fromdate)
        {

            final Calendar c = Calendar.getInstance();
            final int mYear = c.get(Calendar.YEAR);
            final int mMonth = c.get(Calendar.MONTH);
            final int mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(ReportActivitySales.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            int  ye = year;
                            int  day = dayOfMonth;
                            int ss = month + 1;
                            int mon = ss;


                            et_fromdate.setText(ss + "-" + day + "-" + ye);
                            datefrom=ye+"-"+mon+"-"+day;
                            getReportingdata(token,datefrom,dateto,"All");
                            // totalCustomerData();

                        }


                    }, mYear, mMonth, mDay);
            dpd.getDatePicker().setMaxDate(new Date().getTime());

            dpd.show();
        }
        else if (id==R.id.totxtdate||id==R.id.todate)
        {

            final Calendar c = Calendar.getInstance();
            final int mYear = c.get(Calendar.YEAR);
            final int mMonth = c.get(Calendar.MONTH);
            final int mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(ReportActivitySales.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            int  ye = year;
                            int  day = dayOfMonth;
                            int ss = month + 1;
                            int mon = ss;


                            et_todate.setText(ss + "-" + day + "-" + ye);
                            dateto=ye+"-"+mon+"-"+day;
                            getReportingdata(token,datefrom,dateto,"All");
                            // totalCustomerData();

                        }


                    }, mYear, mMonth, mDay);

            dpd.getDatePicker().setMaxDate(new Date().getTime());
            dpd.show();
        }
    }
}