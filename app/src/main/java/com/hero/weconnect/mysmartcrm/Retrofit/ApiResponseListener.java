package com.hero.weconnect.mysmartcrm.Retrofit;


public interface ApiResponseListener {
    void onSuccess(String beanTag, SuperClassCastBean superClassCastBean);

    void onFailure(String msg);

    void onError(String msg);


}
