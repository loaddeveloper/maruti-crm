package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class CallingStatusActiveModel extends SuperClassCastBean {

    /**
     * status : true
     * message : Data Fetched Successfully
     * Data : [{"CustomCallingService":"YES","ExcelCallingService":"NO"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * CustomCallingService : YES
         * ExcelCallingService : NO
         */

        @SerializedName("CustomCallingService")
        private String CustomCallingService;
        @SerializedName("ExcelCallingService")
        private String ExcelCallingService;

        public String getCustomCallingService() {
            return CustomCallingService;
        }

        public void setCustomCallingService(String CustomCallingService) {
            this.CustomCallingService = CustomCallingService;
        }

        public String getExcelCallingService() {
            return ExcelCallingService;
        }

        public void setExcelCallingService(String ExcelCallingService) {
            this.ExcelCallingService = ExcelCallingService;
        }
    }
}
