package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

public class TokenModel extends SuperClassCastBean {

    /**
     * access_token : 0kv0gOfCcbPL66Y2sOA-8jo3Efi2fcBEOhdrs9nPxldDtyOvSgsknUdyYONlwb9qjWYmaaEfNRYFc6T0sRab4lXGh6YMDQYhvhhBUCubSq82-8ILopOnPrrd0CTTa7DaxZc63dgthgfbs1x9YS2nwlvWc_Qk7KqusZ5136l70fN5xycRK_swKW7CkAmn8UHj5K78bIpL43umyUBrwdX7vVgmtett90MgYT6Zf4bOxkYjivoLRYxDOkqPJQ7ymT_XWuY10qVhWyNLbX0bh5vXyUqJg2nf8T49fke6ciSR7aQ
     * token_type : bearer
     * expires_in : 86399
     */

    private String access_token;
    private String token_type;
    private String role = "SR";
    private String dealercode = "";
    private String pagename = "";
    private int expires_in;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDealercode() {
        return dealercode;
    }

    public void setDealercode(String dealercode) {
        this.dealercode = dealercode;
    }

    public String getPagename() {
        return pagename;
    }

    public void setPagename(String pagename) {
        this.pagename = pagename;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }
}
