package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;

import java.util.ArrayList;


public class AdapterEnqSource extends RecyclerView.Adapter<AdapterEnqSource.ViewHolder> {
    ArrayList<String> enqSourcelist = new ArrayList<>();
    AdapterEnqSource.EnqSourceClickListener enqSourceClickListener;
    private Context context;


    public AdapterEnqSource(Context context,ArrayList<String> enqSourcelist,EnqSourceClickListener enqSourceClickListener) {

        this.context = context;
        this.enqSourceClickListener = enqSourceClickListener;
        this.enqSourcelist = enqSourcelist;
    
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.selective_rc_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.checkBox.setText(enqSourcelist.get(position));
        holder.tv_option.setVisibility(View.GONE);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    enqSourceClickListener.addEnqSource(position,enqSourcelist.get(position));

                }else
                {
                    enqSourceClickListener.removeEnqSource(position,enqSourcelist.get(position));

                }
            }
        });
        if(CommonVariables.filerenqsourcelist.size()>0)
        {
            for(String modelName:CommonVariables.filerenqsourcelist)
            {
                if(modelName.equals(enqSourcelist.get(position)))
                {
                    holder.checkBox.setChecked(true);
                }
            }
        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return enqSourcelist.size();
    }



    public interface EnqSourceClickListener {
        void addEnqSource(int position, String enqSource);
        void removeEnqSource(int position, String enqSource);

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView tv_option;
        public CheckBox checkBox;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.tv_option = rootView.findViewById(R.id.tv_option);
            this.checkBox = rootView.findViewById(R.id.cb);
        }
    }

}