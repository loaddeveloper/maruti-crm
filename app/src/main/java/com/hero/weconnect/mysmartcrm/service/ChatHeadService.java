package com.hero.weconnect.mysmartcrm.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.hero.weconnect.mysmartcrm.Utils.FloatingViewListener;


/**
 * ChatHead Service
 */


public class ChatHeadService extends Service implements FloatingViewListener {

    public static final String EXTRA_CUTOUT_SAFE_AREA = "cutout_safe_area";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onFinishFloatingView() {

    }

    @Override
    public void onTouchFinished(boolean isFinishing, int x, int y) {

    }

    /**
     /* * デバッグログ用のタグ
     *//*
    private static final String TAG = "ChatHeadService";

    *//**
     * Intent key (Cutout safe area)
     *//*
    public static final String EXTRA_CUTOUT_SAFE_AREA = "cutout_safe_area";

    *//**
     * 通知ID
     *//*
    private static final int NOTIFICATION_ID = 9083150;

    *//**
     * FloatingViewManager
     *//*
    private FloatingViewManager mFloatingViewManager;

    *//**
     * {@inheritDoc}
     *//*
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // 既にManagerが存在していたら何もしない
        if (mFloatingViewManager != null) {
            return START_STICKY;
        }

        final DisplayMetrics metrics = new DisplayMetrics();
        final WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        final LayoutInflater inflater = LayoutInflater.from(this);
        final ImageView iconView = (ImageView) inflater.inflate(R.layout.widget_chathead, null, false);
        iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ////Log.d(TAG, getString(R.string.chathead_click_message));
            }
        });

        mFloatingViewManager = new FloatingViewManager(this, this);
        mFloatingViewManager.setFixedTrashIconImage(R.drawable.ic_trash_fixed);
        mFloatingViewManager.setActionTrashIconImage(R.drawable.ic_trash_action);
        mFloatingViewManager.setSafeInsetRect((Rect) intent.getParcelableExtra(EXTRA_CUTOUT_SAFE_AREA));
        final FloatingViewManager.Options options = new FloatingViewManager.Options();
        options.overMargin = (int) (16 * metrics.density);
        mFloatingViewManager.addViewToWindow(iconView, options);

        // 常駐起動
        startForeground(NOTIFICATION_ID, createNotification(this));

        return START_REDELIVER_INTENT;
    }

    *//**
     * {@inheritDoc}
     *//*
    @Override
    public void onDestroy() {
        destroy();
        super.onDestroy();
    }

    *//**
     * {@inheritDoc}
     *//*
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    *//**
     * {@inheritDoc}
     *//*
    @Override
    public void onFinishFloatingView() {
        stopSelf();
        ////Log.d(TAG, getString(R.string.finish_deleted));
    }

    *//**
     * {@inheritDoc}
     *//*
    @Override
    public void onTouchFinished(boolean isFinishing, int x, int y) {
        if (isFinishing) {
            ////Log.d(TAG, getString(R.string.deleted_soon));
        } else {
            ////Log.d(TAG, getString(R.string.touch_finished_position, x, y));
        }
    }

    *//**
     * Viewを破棄します。
     *//*
    private void destroy() {
        if (mFloatingViewManager != null) {
            mFloatingViewManager.removeAllViewToWindow();
            mFloatingViewManager = null;
        }
    }

    *//**
     * 通知を表示します。
     * クリック時のアクションはありません。
     *//*
    private static Notification createNotification(Context context) {
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.default_floatingview_channel_id));
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(context.getString(R.string.chathead_content_title));
        builder.setContentText(context.getString(R.string.content_text));
        builder.setOngoing(true);
        builder.setPriority(NotificationCompat.PRIORITY_MIN);
        builder.setCategory(NotificationCompat.CATEGORY_SERVICE);

        return builder.build();
    }*/
}
