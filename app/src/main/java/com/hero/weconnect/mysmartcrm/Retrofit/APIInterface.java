package com.hero.weconnect.mysmartcrm.Retrofit;


import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.AllotedDSEModel;
import com.hero.weconnect.mysmartcrm.models.BRCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.BR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.BR_Report_Model;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.CreateFolloupModel;
import com.hero.weconnect.mysmartcrm.models.CustomerReplyModel;
import com.hero.weconnect.mysmartcrm.models.DataAllotedModel;
import com.hero.weconnect.mysmartcrm.models.FilterDataSendModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetNonTurnUpHistoryModel;
import com.hero.weconnect.mysmartcrm.models.GetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.GetVersionModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_SalesModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_Service_ReminderModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_Sales;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.NonTurnUpDataModel;
import com.hero.weconnect.mysmartcrm.models.NotComingReasonModel;
import com.hero.weconnect.mysmartcrm.models.PSFDataModel;
import com.hero.weconnect.mysmartcrm.models.PSFDissatisfactionReasonModel;
import com.hero.weconnect.mysmartcrm.models.PSFSOPRatingQuestionsModel;
import com.hero.weconnect.mysmartcrm.models.PSFSingleDataModel;
import com.hero.weconnect.mysmartcrm.models.ReportingModel;
import com.hero.weconnect.mysmartcrm.models.SRCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.SR_Alloted_data_model;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SR_Report_Model;
import com.hero.weconnect.mysmartcrm.models.SalesBookingDataModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryDataBR;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_NonTurnUp;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.models.UserDetailsModel;
import com.hero.weconnect.mysmartcrm.models.UserNameListModel;
import com.hero.weconnect.mysmartcrm.models.booking_reminder_model;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {


    // Token API  Method Interface
    @FormUrlEncoded
    @POST("token")
    Call<TokenModel> getToken(
            @Field("Username") String username,
            @Field("Password") String password,
            @Field("grant_type") String granttype
    );


    // Contatct Status List  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/contactstatus")
    Call<ContactStatusModel> getContactStatus(@Header("Authorization") String token
    );

    // Closure Sub  Reason List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/closuresubreason")
    Call<ClouserSubReasonModel> getClosureSubReason(@Header("Authorization") String token, @Body HashMap<String, String> params);


    // Closure  Reason List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/closurereason")
    Call<ClouserReasonModel> getClosureReason(@Header("Authorization") String token);


    // Not Coming Reason List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/notcomingreason")
    Call<NotComingReasonModel> getNotComingReason(@Header("Authorization") String token);

    // Customer Reply List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/customerreply")
    Call<CustomerReplyModel> getCustomerReply(@Header("Authorization") String token);


    // Model List API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/model")
    Call<ModelModel> getModel(@Header("Authorization") String token,
                              @Body HashMap<String, String> params);

    // Make List API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/make")
    Call<MakeModel> getMake(@Header("Authorization") String token);


    // Add Calling Histoy API Method InterFace

    @Headers("Content-Type: application/json")
    @POST("callhistory/addnew")
    Call<AddCallHistoryUpdateModel> addCallHistoryUpdate(@Header("Authorization") String token,
                                                         @Body HashMap<String, String> params);

    // Update Contact Number API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecontactchanged")
    Call<CommonModel> updatecontacts(@Header("Authorization") String token,
                                     @Body HashMap<String, String> params);

    // Update Customer Reply API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecustomerreply")
    Call<CommonModel> updatecustomerreply(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // Update Tagged SR Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatetaggedsrstatus")
    Call<CommonModel> updatetaggedsrstatus(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Booking date Time  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatebookingdate")
    Call<CommonModel> updatebookingdate(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);


    // Update Call End  Time  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecallendtime")
    Call<CommonModel> updatecallendtime(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);


    // Update Remark API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateremark")
    Call<CommonModel> updateremark(@Header("Authorization") String token,
                                   @Body HashMap<String, String> params);


    // Update Follow-up Date & Time API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupdate")
    Call<CommonModel> updatefollowupdate(@Header("Authorization") String token,
                                         @Body HashMap<String, String> params);


    // Update Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecontactstatus")
    Call<CommonModel> updatecontactstatus(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // Update Called Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatecalledstatus")
    Call<CommonModel> updatecalledstatus(@Header("Authorization") String token,
                                         @Body HashMap<String, String> params);


    // Update Not Coming Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatencr")
    Call<CommonModel> updatenotconingreason(@Header("Authorization") String token,
                                            @Body HashMap<String, String> params);


    // Get Service Reminder Data API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("reminders/servicereminder")
    Call<ServiceReminderDataModel> getServiceReminderData(@Header("Authorization") String token,@Body HashMap<String, String> params);


    // Get Sales Enquiry Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("reminders/salesenquiryfollowup")
    Call<SalesEnquiryDataModel> getSalesEnquiryData(@Header("Authorization") String token,
                                                    @Body HashMap<String, String> params);


    // Get Service Reminder History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsrhistory")
    Call<SR_HistoryModel> getSRHistory(@Header("Authorization") String token,
                                       @Body HashMap<String, String> params);

    // Get Sales Calling History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsehistory")
    Call<Sales_HistoryModel> getSalesHistory(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);

    // Get Sales Calling History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getepdhistory")
    Call<Sales_HistoryModel> getExpectedSalesHistory(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);


    // Get Sales Calling History Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getepdhistory")
    Call<SR_HistoryModel> getExpectedSRHistory(@Header("Authorization") String token,
                                                     @Body HashMap<String, String> params);



    // Update History Id  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewsalesenquiry")
    Call<CommonModel> updatesaleshistoryId(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Date of Purchase  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatedateofpurchase")
    Call<CommonModel> updatedateofpurchase(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Closure Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosurereason")
    Call<CommonModel> updateclosurereason(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);

    // Update Closure Sub Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosuresubreason")
    Call<CommonModel> updateclosuresubreason(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);

    // Update Make API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemake")
    Call<CommonModel> updatemake(@Header("Authorization") String token,
                                 @Body HashMap<String, String> params);

    // Update Model API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemodel")
    Call<CommonModel> updatemodel(@Header("Authorization") String token,
                                  @Body HashMap<String, String> params);

    // Update Pick & Drop API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatepickndrop")
    Call<CommonModel> updatepickdrop(@Header("Authorization") String token,
                                     @Body HashMap<String, String> params);


    // Update SMS Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("dealer/setStatus")
    Call<SetSMSTemplateCommonModel> updatesmsstatus(@Header("Authorization") String token,
                                                    @Body HashMap<String, String> params);

    // Update SMS Template API Method Interface
    @Headers("Content-Type: application/json")
    @POST("dealer/setTempalte")
    Call<SetSMSTemplateCommonModel> updatesmstemplate(@Header("Authorization") String token,
                                                      @Body HashMap<String, String> params);

    // Get SMS Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("dealer/getstatus")
    Call<GetSMSTemplateCommonModel> getsmsstatus(@Header("Authorization") String token);

    // Get SMS Template API Method Interface
    @Headers("Content-Type: application/json")
    @POST("dealer/gettemplatelistusingtag")
    Call<GetTamplateListModel> getsmstemplate(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);


    // Update Call Recording API Method
    @Multipart
    @POST("upload/recording")
    Call<UploadRecordingModel> uploadRecording(@Header("Authorization") String token,
                                               @Part("DealerCode") RequestBody dealercode,
                                               @Part("HistoryId") RequestBody historyid,
                                               @Part("ContentType") RequestBody contenttype,
                                               @Part MultipartBody.Part recording,
                                               @Part("CallType") RequestBody calltype);


    //Get  SR Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("incoming/getsremcustomer")
    Call<Incoming_Service_ReminderModel> getSRIncomingdata(@Header("Authorization") String token,
                                                           @Body HashMap<String, String> params);

    //Get  Enquiry Incoming Number  Data  fetch API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("incoming/getsenqcustomer")
    Call<Incoming_SalesModel> getEnquiryIncomingdata(@Header("Authorization") String token,
                                                     @Body HashMap<String, String> params);

    // Data Allocation API
    @Headers("Content-Type: application/json")
    @POST("reminders/deallocateserviceremidner")
    Call<CommonModel> setDeAllocation(@Header("Authorization") String token,
                                      @Body HashMap<String, String> params);


    // Get Missed Data Interface
    @Headers("Content-Type: application/json")
    @POST("missed/getmissedlist")
    Call<MissedDataModel_SR> getMissedData(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Get Missed Data Sales Interface
    @Headers("Content-Type: application/json")
    @POST("missed/getmissedlist")
    Call<MissedDataModel_Sales> getMissedDataSales(@Header("Authorization") String token,
                                                   @Body HashMap<String, String> params);


    // Add Missed Number Interface
    @Headers("Content-Type: application/json")
    @POST("missed/addmissedno")
    Call<CommonModel> addtMissedNumber(@Header("Authorization") String token,
                                       @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData> getsinglecustomerhistory(@Header("Authorization") String token,
                                                             @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData_NonTurnUp> getsinglecustomernonturnuphistory(@Header("Authorization") String token,
                                                                                @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryDataBR> getsinglecustomerhistoryBR(@Header("Authorization") String token,
                                                                 @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<PSFSingleDataModel> getsinglecustomerhistoryPSF(@Header("Authorization") String token,
                                                         @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData_Sales> getsinglecustomerhistorysales(@Header("Authorization") String token,
                                                                        @Body HashMap<String, String> params);



    // Update Add Exception  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("ex/addexception")
    Call<CommonModel> AddException(@Header("Authorization") String token,
                                   @Body HashMap<String, String> params);

    // Update Sales Follow Up Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupdone")
    Call<CommonModel> updatesalescontactstatus(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);


    // Update Sales Follow Up Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupstatus")
    Call<CommonModel> updatesfollowupdonestatus(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);

    // Get Follow Up Contatct Status API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/followupdone")
    Call<FollowUpListModel> getSalesFollowupConatctList(@Header("Authorization") String token);

    // Get Follow Up Done List API Method InterFace
    @Headers("Content-Type: application/json")
    @POST("list/followupstatus")
    Call<FollowUpListModel> getSalesFollowupDoneList(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("web/createfollowup")
    Call<CreateFolloupModel> setCreateFollowup(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("web/servicerequest")
    Call<CreateFolloupModel> getServiceRequest(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("web/cancelrequest")
    Call<CreateFolloupModel> CancelServiceRequest(@Header("Authorization") String token,
                                                  @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("search/purchasedate")
    Call<SalesBookingDataModel> getExpectedSearchingData(@Header("Authorization") String token,
                                                  @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("search/sales")
    Call<SalesEnquiryDataModel> getSearchingData(@Header("Authorization") String token,
                                                 @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("reminders/salesenquiryfollowup")
    Call<SalesEnquiryDataModel> getFilteredData(@Header("Authorization") String token,
                                                @Body FilterDataSendModel filterDataSendModel);


    @Headers("Content-Type: application/json")
    @POST("reminders/salesexpecteddateofpurchase")
    Call<SalesBookingDataModel> getExptectedFilteredData(@Header("Authorization") String token,
                                                @Body FilterDataSendModel filterDataSendModel);




    @Headers("Content-Type: application/json")
    @POST("report/callingstatistics")
    Call<ReportingModel> getReportingData(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("report/callingstatisticsepd")
    Call<ReportingModel> getExpectedReportingData(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);




    @Headers("Content-Type: application/json")
    @POST("report/callingstatisticssr")
    Call<SR_Report_Model> getReportingData_SR(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("report/callingstatisticsbr")
    Call<BR_Report_Model> getReportingData_BR(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("version/getversion")
    Call<GetVersionModel> getVersion(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("reminders/salesenquirycalldone")
    Call<SalesEnquiryCallDoneDataModel> getCalldoneData(@Header("Authorization") String token,
                                                        @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("reminders/expecteddateofpurchasecalldone")
    Call<SalesEnquiryCallDoneDataModel> getExpectedCalldoneData(@Header("Authorization") String token,
                                                        @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("reminders/nonturnupbookingremindercalldone")
    Call<NonTurnUpDataModel> getNONTURNUPBOOKINGCalldoneData(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("followup/psfcalldone")
    Call<PSFDataModel> getPSFCalldoneData(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("reminders/bookingremindercalldone")
    Call<BRCallDoneDataModel> getBRCalldoneData(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("reminders/serviceremindercalldone")
    Call<SRCallDoneDataModel> getSRCalldoneData(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("user/getuserdetails")
    Call<UserDetailsModel> getUserDetails(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("list/usernames")
    Call<UserNameListModel> getUserNameListData(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("report/dsedatatodaylist")
    Call<DataAllotedModel> getAllotedDatalist(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("report/allocateduserlistofdse")
    Call<AllotedDSEModel> getAllotedDSE(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);


    @Headers("Content-Type: application/json")
    @POST("reminders/salesexpecteddateofpurchase")
    Call<SalesBookingDataModel> getSalesExpectedDateOfPurchase(@Header("Authorization") String token,
                                        @Body HashMap<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("report/allocateduserlistofsr")
    Call<SR_Alloted_data_model> getAllotedDATA_SR(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);


    // Update Sales ExpectedDateOfPurchase API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupdoneinexphistory")
    Call<CommonModel> updateExpectedDateOfPurchaseFollowUpDone(@Header("Authorization") String token,
                                               @Body HashMap<String, String> params);


    // Update Sales Follow Up Contatct Status API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatefollowupstatusinexphistory")
    Call<CommonModel> updatesExpectedfollowupdonestatus(@Header("Authorization") String token,
                                                @Body HashMap<String, String> params);


    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData> getsingleExpectedcustomerhistory(@Header("Authorization") String token,
                                                             @Body HashMap<String, String> params);

    // Get Single Customer History Data
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsinglecustomerhistory")
    Call<SingleCustomerHistoryData_Sales> getsinglecustomerhistorysalesExpected(@Header("Authorization") String token,
                                                                        @Body HashMap<String, String> params);


    // Update History Id  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewsalesexppurchasedate")
    Call<CommonModel> updatesalesExpectedhistoryId(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Date of Purchase  API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatedateofpurchaseinexphistory")
    Call<CommonModel> updateExpecteddateofpurchase(@Header("Authorization") String token,
                                           @Body HashMap<String, String> params);

    // Update Closure Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosurereasoninexphistory")
    Call<CommonModel> updateExpectedclosurereason(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);

    // Update Closure Sub Reason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updateclosuresubreasoninexphistory")
    Call<CommonModel> updateExpectedclosuresubreason(@Header("Authorization") String token,
                                             @Body HashMap<String, String> params);

    // Update Make API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemakeinexphistory")
    Call<CommonModel> updateExpectedmake(@Header("Authorization") String token,
                                 @Body HashMap<String, String> params);

    // Update Model API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatemodelinexphistory")
    Call<CommonModel> updateExpectedmodel(@Header("Authorization") String token,
                                  @Body HashMap<String, String> params);



    // BookingReminders API Method Interface
    @Headers("Content-Type: application/json")
    @POST("reminders/bookingreminder")
    Call<booking_reminder_model> getBookingRemindersData(@Header("Authorization") String token,
                                                         @Body HashMap<String, String> params);


    // addBookingReminderHistory API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewbookingreminderhistory")
    Call<AddCallHistoryUpdateModel> addBookingReminderHistory(@Header("Authorization") String token,
                                          @Body HashMap<String, String> params);


    // NON TURNUP BookingReminders API Method Interface
    @Headers("Content-Type: application/json")
    @POST("reminders/nonturnupbookingreminder")
    Call<NonTurnUpDataModel> getNonTurnUpData(@Header("Authorization") String token,
                                              @Body HashMap<String, String> params);


    // addNonTurnUpBookingHistory API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewnonturnupbookinghistory")
    Call<AddCallHistoryUpdateModel> addNonTurnUpHistory(@Header("Authorization") String token,
                                                              @Body HashMap<String, String> params);


    // PSF Data API Method Interface
    @Headers("Content-Type: application/json")
    @POST("followup/psf")
    Call<PSFDataModel> getPSFData(@Header("Authorization") String token,
                                  @Body HashMap<String, String> params);


    // addPSFHistory API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/addnewpsfhistory")
    Call<AddCallHistoryUpdateModel> addPSFHistory(@Header("Authorization") String token,
                                                        @Body HashMap<String, String> params);

//    // addNonTurnUpHistory API Method Interface
//    @Headers("Content-Type: application/json")
//    @POST("callhistory/addnewnonturnupbookinghistory")
//    Call<AddCallHistoryUpdateModel> addNonTurnUpHistory(@Header("Authorization") String token,
//                                                  @Body HashMap<String, String> params);



    // getBookingReminderHistory API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getbookingreminderhistory")
    Call<BR_HistoryModel> getBookingReminderHistory(@Header("Authorization") String token,
                                                    @Body HashMap<String, String> params);


    // getBookingReminderHistory API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getsrhistory")
    Call<SR_HistoryModel> getPSFHistory(@Header("Authorization") String token,
                                                              @Body HashMap<String, String> params);



    // getNonTurnUpBookingReminder API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/getnonturnupbookingreminder")
    Call<GetNonTurnUpHistoryModel> getNonTurnUpBookingReminder(@Header("Authorization") String token,
                                                               @Body HashMap<String, String> params);


    // getPSFSOPRatingQuestions API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/psfsopratingquestions")
    Call<PSFSOPRatingQuestionsModel> getPSFSOPRatingQuestions(@Header("Authorization") String token);

    // getPSFDissatisfactionreason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("list/psfdissatisfactionreason")
    Call<PSFDissatisfactionReasonModel> getPSFDissatisfactionreason(@Header("Authorization") String token);


        // InsertOrUpdatePSFAnswer API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/insertorupdatepsfanswer")
    Call<JSONObject> insertOrUpdatePSFAnswer(@Header("Authorization") String token,
                                                              @Body HashMap<String, String> params);


    // updatePSFdissatisfactionreason API Method Interface
    @Headers("Content-Type: application/json")
    @POST("callhistory/updatepsfdissatisfactionreason")
    Call<JSONObject> updatePSFdissatisfactionreason(@Header("Authorization") String token,
                                                              @Body HashMap<String, String> params);







//    callhistory/getbookingreminderhistory
//    callhistory/getnonturnupbookingreminder
//    callhistory/addnewsalesexppurchasedate
//    callhistory/updatedateofpurchaseinexphistory
//    callhistory/updateclosurereasoninexphistory
//    callhistory/updateclosuresubreasoninexphistory
//    callhistory/updatemakeinexphistory
//    callhistory/updatemodelinexphistory


//    api/followup/psfcalldone
//    api/reminders/bookingremindercalldone
//    api/reminders/nonturnupbookingremindercalldone
}
