package com.hero.weconnect.mysmartcrm.customretrofit.Retrofit;


import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.AddHistoryModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CallProgressModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CallingStatusActiveModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CampaignModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CustomHistoryModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.DataFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.DeAllocationModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.FieldUpdateModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetSmsStatusModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetTemplateListModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.InputFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.ReportModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.SetSMSStatusModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.TokenModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.UpdateSMSTemplateModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.getCustomIncoingModel;
import com.hero.weconnect.mysmartcrm.models.CreateFolloupModel;
import com.hero.weconnect.mysmartcrm.models.VideoDataModel;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiController  {
    private APIInterface apiInterface;
    private ApiResponseListener apiResponseListener;
    Context mcontext;


    public ApiController(ApiResponseListener apiResponseListener) {
        this.apiResponseListener = apiResponseListener;

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }

    }


    public void getToken(String username, String password,String granttype) {
        APIInterface apiInterface = APIClientMain.getClient().create(APIInterface.class);
        Call<TokenModel> call = apiInterface.getToken(username, password,granttype);
        call.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if (response.body() != null) {
                    TokenModel tokenModel = response.body();
                    SuperClassCastBean superClassCastBean = tokenModel;
                    apiResponseListener.onSuccess(ApiConstant.TOKEN_TAG, superClassCastBean);

                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Custom Callling Data in this Function  API Code
    public void getCustomCallingData(String token, HashMap<String, String> params) {
        Call<Custom_Model> call = apiInterface.getCustomCallingData(token, params);
        call.enqueue(new Callback<Custom_Model>() {
            @Override
            public void onResponse(Call<Custom_Model> call, Response<Custom_Model> response) {

                if (response.body() != null)
                {
                    Custom_Model serviceReminderDataModel = response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;
                    apiResponseListener.onSuccess(ApiConstant.CUSTOMCALLING, superClassCastBean);
                }
                else if (response.code()==401)
                {
                    apiResponseListener.onError(String.valueOf(response.code()));
                }
                else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Custom_Model> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.CUSTOMCALLING);
            }
        });


    }


    // Get Custom Campaign Data in this Function  API Code
    public void getCampaignData(String token, HashMap<String, String> params) {
        Call<CampaignModel> call = apiInterface.getCampaignList(token, params);
        call.enqueue(new Callback<CampaignModel>() {
            @Override
            public void onResponse(Call<CampaignModel> call, Response<CampaignModel> response) {

                if (response.body() != null)
                {
                    CampaignModel campaignModel = response.body();
                    SuperClassCastBean superClassCastBean = campaignModel;
                    apiResponseListener.onSuccess(ApiConstant.CAMPAIGNLIST, superClassCastBean);
                }
                else if (response.code()==401)
                {
                    apiResponseListener.onError(String.valueOf(response.code()));
                }
                else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CampaignModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.CAMPAIGNLIST);
            }
        });


    }


    // Get Input Field Data in this Function  API Code
    public void getInputFieldData(String token, HashMap<String, String> params) {
        Call<InputFieldModel> call = apiInterface.getInputList(token, params);
        call.enqueue(new Callback<InputFieldModel>() {
            @Override
            public void onResponse(Call<InputFieldModel> call, Response<InputFieldModel> response) {

                if (response.body() != null)
                {
                    InputFieldModel inputFieldModel = response.body();
                    SuperClassCastBean superClassCastBean = inputFieldModel;
                    apiResponseListener.onSuccess(ApiConstant.INPUTFIELDLIST, superClassCastBean);
                }
                else if (response.code()==401)
                {
                    apiResponseListener.onError(String.valueOf(response.code()));
                }
                else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<InputFieldModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.INPUTFIELDLIST);
            }
        });


    }

    // Get Input Field Data in this Function  API Code
    public void getInputDataFieldData(String token, HashMap<String, String> params) {
        Call<DataFieldModel> call = apiInterface.getInputDataFieldsList(token, params);
        call.enqueue(new Callback<DataFieldModel>() {
            @Override
            public void onResponse(Call<DataFieldModel> call, Response<DataFieldModel> response) {

                if (response.body() != null)
                {
                    DataFieldModel inputFieldModel = response.body();
                    SuperClassCastBean superClassCastBean = inputFieldModel;
                    apiResponseListener.onSuccess(ApiConstant.INPUTDATAFIELDLIST, superClassCastBean);
                }
                else if (response.code()==401)
                {
                    apiResponseListener.onError(String.valueOf(response.code()));
                }
                else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<DataFieldModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.INPUTDATAFIELDLIST);
            }
        });


    }


    // Update SMS Status in Database for this function API Code
    public void UpdateSMSStatus(String token, HashMap<String, String> params) {
        Call<SetSMSStatusModel> call = apiInterface.updatesmsstatus(token, params);
        call.enqueue(new Callback<SetSMSStatusModel>() {
            @Override
            public void onResponse(Call<SetSMSStatusModel> call, Response<SetSMSStatusModel> response) {
                if (response.body() != null) {
                    SetSMSStatusModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.SETSMSSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");

                }

            }

            @Override
            public void onFailure(Call<SetSMSStatusModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get Video Data for this function API Code
    public void getVideoData(HashMap<String,String> params) {

        Call<VideoDataModel> call = apiInterface.getVideoData(params);
        call.enqueue(new Callback<VideoDataModel>() {
            @Override
            public void onResponse(Call<VideoDataModel> call, Response<VideoDataModel> response) {
                if (response.body() != null) {
                    VideoDataModel videoDataModel = response.body();
                    SuperClassCastBean superClassCastBean = videoDataModel;

                    apiResponseListener.onSuccess(ApiConstant.VIDEODATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<VideoDataModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get SMS Status in Database for this function API Code
    public void GetSMSStatus(String token) {

        Call<GetSmsStatusModel> call = apiInterface.getsmsstatus(token);
        call.enqueue(new Callback<GetSmsStatusModel>() {
            @Override
            public void onResponse(Call<GetSmsStatusModel> call, Response<GetSmsStatusModel> response) {
                if (response.body() != null) {
                    GetSmsStatusModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSMSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetSmsStatusModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get SMS Template  in Database for this function API Code
    public void GetSMSTemplate(String token) {
        Call<GetTemplateListModel> call = apiInterface.getsmstemplate(token);
        call.enqueue(new Callback<GetTemplateListModel>() {
            @Override
            public void onResponse(Call<GetTemplateListModel> call, Response<GetTemplateListModel> response) {
                if (response.body() != null) {
                    GetTemplateListModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSMSTEMPLATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetTemplateListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Add SMS Template  in Database for this function API Code
    public void AddSMSTemplate(String token, HashMap<String, String> params) {
        Call<UpdateSMSTemplateModel> call = apiInterface.addsmstemplate(token, params);
        call.enqueue(new Callback<UpdateSMSTemplateModel>() {
            @Override
            public void onResponse(Call<UpdateSMSTemplateModel> call, Response<UpdateSMSTemplateModel> response) {
                if (response.body() != null) {
                    UpdateSMSTemplateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDSMSTEMPLATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UpdateSMSTemplateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update SMS Template  in Database for this function API Code
    public void UpdateSMSTemplate(String token, HashMap<String, String> params) {
        Call<UpdateSMSTemplateModel> call = apiInterface.updatesmstemplate(token, params);
        call.enqueue(new Callback<UpdateSMSTemplateModel>() {
            @Override
            public void onResponse(Call<UpdateSMSTemplateModel> call, Response<UpdateSMSTemplateModel> response) {
                if (response.body() != null) {
                    UpdateSMSTemplateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.SETSMSTEMPLATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UpdateSMSTemplateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get SMS Template  in Database for this function API Code
    public void GetActiveCalling(String token) {
        Call<CallingStatusActiveModel> call = apiInterface.getActiveCalling(token);
        call.enqueue(new Callback<CallingStatusActiveModel>() {
            @Override
            public void onResponse(Call<CallingStatusActiveModel> call, Response<CallingStatusActiveModel> response) {
                if (response.body() != null) {
                    CallingStatusActiveModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.GETACTIVECALLING, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CallingStatusActiveModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update History in Database for this function API Code
    public void AddHistory(String token, HashMap<String, String> params) {
        Call<AddHistoryModel> call = apiInterface.addhistorymodel(token, params);
        call.enqueue(new Callback<AddHistoryModel>() {
            @Override
            public void onResponse(Call<AddHistoryModel> call, Response<AddHistoryModel> response) {
                if (response.body() != null) {
                    AddHistoryModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddHistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update BookingDate in Database for this function API Code
    public void UpdateBookingDate(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEBOOKINGDATETIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update FollowupDate in Database for this function API Code
    public void UpdateFollowupDate(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEFOLLOWUPDATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Remark in Database for this function API Code
    public void UpdateRemark(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEREMARK, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Call Status in Database for this function API Code
    public void UpdateCallStatus(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLEDSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update CallEnd Time in Database for this function API Code
    public void UpdateCallEndTime(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLENDTIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Status in Database for this function API Code
    public void UpdateStatus(String token, HashMap<String, String> params) {
        Call<FieldUpdateModel> call = apiInterface.updatefields(token, params);
        call.enqueue(new Callback<FieldUpdateModel>() {
            @Override
            public void onResponse(Call<FieldUpdateModel> call, Response<FieldUpdateModel> response) {
                if (response.body() != null) {
                    FieldUpdateModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FieldUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get Single Customer history Data in this Function  API Code
    public void getSingleCustomerHistoryData(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData> call = apiInterface.getsinglecustomerhistory(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData> call, Response<SingleCustomerHistoryData> response) {
                if (response.body() != null) {
                    SingleCustomerHistoryData singleCustomerHistoryData = response.body();
                    SuperClassCastBean superClassCastBean = singleCustomerHistoryData;

                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void deAllocation(String token) {
        Call<DeAllocationModel> call = apiInterface.setDeAllocation(token);
        call.enqueue(new Callback<DeAllocationModel>() {
            @Override
            public void onResponse(Call<DeAllocationModel> call, Response<DeAllocationModel> response) {
                if (response.body() != null) {
                    DeAllocationModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.DEALLOCATION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<DeAllocationModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Upload Call Recording Code
    public void UploadCallRecording(String token, RequestBody historyid, RequestBody contenttype, MultipartBody.Part recording) {

        Call<UploadRecordingModel> call = apiInterface.uploadRecording(token, historyid, contenttype, recording);
        call.enqueue(new Callback<UploadRecordingModel>() {
            @Override
            public void onResponse(Call<UploadRecordingModel> call, Response<UploadRecordingModel> response) {
                if (response.body() != null) {
                    UploadRecordingModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPLOADRECORDING, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UploadRecordingModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get Service Reminder Incoming  Data Fetch in this Function  API Code
    public void getCustomIncomingData(String token, HashMap<String, String> params) {
        Call<getCustomIncoingModel> call = apiInterface.getCustomIncomingdata(token, params);
        call.enqueue(new Callback<getCustomIncoingModel>() {
            @Override
            public void onResponse(Call<getCustomIncoingModel> call, Response<getCustomIncoingModel> response) {
                if (response.body() != null) {
                    getCustomIncoingModel incoming_service_reminderModel = response.body();
                    SuperClassCastBean superClassCastBean = incoming_service_reminderModel;

                    apiResponseListener.onSuccess(ApiConstant.SRINCOMINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<getCustomIncoingModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Service Reminder History Data in this Function  API Code
    public void getCustomHistory(String token, HashMap<String, String> params) {
        Call<CustomHistoryModel> call = apiInterface.getHistory(token, params);
        call.enqueue(new Callback<CustomHistoryModel>() {
            @Override
            public void onResponse(Call<CustomHistoryModel> call, Response<CustomHistoryModel> response) {
                if (response.body() != null) {
                    CustomHistoryModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CustomHistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getReportingData(String token, HashMap<String, String> params) {
        Call<ReportModel> call = apiInterface.getReportingData(token, params);
        call.enqueue(new Callback<ReportModel>() {
            @Override
            public void onResponse(Call<ReportModel> call, Response<ReportModel> response) {
                if (response.body() != null) {
                    ReportModel reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.REPORTINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ReportModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.REPORTINGDATA);
            }
        });


    }


    // Get Service Reminder History Data in this Function  API Code
    public void getCallProgressStatus(String token, HashMap<String, String> params) {
        Call<CallProgressModel> call = apiInterface.getCallProgressModel(token, params);
        call.enqueue(new Callback<CallProgressModel>() {
            @Override
            public void onResponse(Call<CallProgressModel> call, Response<CallProgressModel> response) {
                if (response.body() != null) {
                    CallProgressModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;
                    // TODO: 05-12-2020 error
                    apiResponseListener.onSuccess(ApiConstant.CALLPROGRESS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CallProgressModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void createServiceRequest(String token, HashMap<String, String> params) {
        apiInterface = APIClientMain.getClient().create(APIInterface.class);

        Call<com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel> call = apiInterface.getServiceRequest(token, params);
        call.enqueue(new Callback<com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel>() {
            @Override
            public void onResponse(Call<com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel> call, Response<com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel> response) {
                if(response.code()==401)
                {
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant.SERVICEREQUEST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    private class DownloadWebPageTask extends AsyncTask<Void, Void, Void> {
        String userName = "";
        String password = "";

        public DownloadWebPageTask(String username, String password) {
            this.userName = username;
            this.password = password;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "");
            String encoding = null;
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    encoding = Base64.getEncoder().encodeToString((userName + ":" + password).getBytes("UTF-8"));
                } else {
                    encoding =android.util.Base64.encodeToString((userName + ":" + password).getBytes("UTF-8"),android.util.Base64.NO_WRAP);
                }
               // Log.e("TAG", "encoded value :  " + encoding);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Request request = new Request.Builder()
                    .url("https://loadcrm.com/customcallingapi/api/Calling_API/Get_token")
                    .method("POST", body)
                    .addHeader("Authorization", "Basic " + encoding)

                    .build();
            try {
                okhttp3.Response response = client.newCall(request).execute();
                try {
                    if (response.isSuccessful()) {
//                        TokenModel entity = new Gson().fromJson(response.body().string(), TokenModel.class);
                        TokenModel entity = new Gson().fromJson(response.peekBody(2048).string(), TokenModel.class);
                        apiResponseListener.onSuccess(ApiConstant.TOKEN_TAG, entity);
                    } else {
                        apiResponseListener.onError("error in response :  " + response.code());
                    }
                } catch (ClassCastException classCastException) {
                    apiResponseListener.onError(classCastException.getMessage());
                } catch (IllegalStateException exception) {
                    // do nothing
                } catch (Exception exception) {
                    apiResponseListener.onError(exception.getLocalizedMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void getToken2(String username, String password) {
        DownloadWebPageTask downloadWebPageTask = new DownloadWebPageTask(username, password);
        downloadWebPageTask.execute();
    }



//    // Session Expired Popup
//    public void sessionExpired_Popup()
//    {
//
//
//        if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {
//
//            final Dialog shippingDialog = new Dialog();
//            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            shippingDialog.setContentView(R.layout.session_expired);
//            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            shippingDialog.setCancelable(true);
//            shippingDialog.setCanceledOnTouchOutside(false);
//            shippingDialog.show();
//            shippingDialog.setCanceledOnTouchOutside(false);
//            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
//            okbutton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.hero.JCapp");
//                    if (intent != null) {
//                        ComponentName componentName = intent.getComponent();
//                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
//                        context.startActivity(mainIntent);
//                        ((Activity)context).finish();
//                        ((Activity)context).finishAndRemoveTask();
//                        ((Activity)context).finishAffinity();
//                    }
//                }
//            });
//
//
//
//        } else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {
//
//
//            final Dialog shippingDialog = new Dialog(context);
//            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            shippingDialog.setContentView(R.layout.session_expired);
//            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            shippingDialog.setCancelable(true);
//            shippingDialog.setCanceledOnTouchOutside(false);
//            shippingDialog.show();
//            shippingDialog.setCanceledOnTouchOutside(false);
//            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
//            okbutton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.herocorp");
//                    if (intent != null) {
//                        ComponentName componentName = intent.getComponent();
//                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
//                        context.startActivity(mainIntent);
//                        ((Activity)context).finish();
//                        ((Activity)context).finishAndRemoveTask();
//                        ((Activity)context).finishAffinity();
//                    }
//                }
//            });
//
//        }
//
//    }

  /*

    public void getContactStatus(String token) {
        Call<ContactStatusModel> call = apiInterface.getContactStatus(token);
        call.enqueue(new Callback<ContactStatusModel>() {
            @Override
            public void onResponse(Call<ContactStatusModel> call, Response<ContactStatusModel> response) {
                if (response.body() != null) {
                    ContactStatusModel contactStatusModel = response.body();
                    SuperClassCastBean superClassCastBean = contactStatusModel;

                    apiResponseListener.onSuccess(ApiConstant.CONTACT_STATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ContactStatusModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getMake(String token) {
        Call<MakeModel> call = apiInterface.getMake(token);
        call.enqueue(new Callback<MakeModel>() {
            @Override
            public void onResponse(Call<MakeModel> call, Response<MakeModel> response) {
                if (response.body() != null) {
                    MakeModel makeModel = response.body();
                    SuperClassCastBean superClassCastBean = makeModel;

                    apiResponseListener.onSuccess(ApiConstant.MAKE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MakeModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getModel(String token, HashMap<String, String> params) {
        Call<ModelModel> call = apiInterface.getModel(token, params);
        call.enqueue(new Callback<ModelModel>() {
            @Override
            public void onResponse(Call<ModelModel> call, Response<ModelModel> response) {
                if (response.body() != null) {
                    ModelModel modelModel = response.body();
                    SuperClassCastBean superClassCastBean = modelModel;

                    apiResponseListener.onSuccess(ApiConstant.MODEL, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ModelModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getCustomerReply(String token) {
        Call<CustomerReplyModel> call = apiInterface.getCustomerReply(token);
        call.enqueue(new Callback<CustomerReplyModel>() {
            @Override
            public void onResponse(Call<CustomerReplyModel> call, Response<CustomerReplyModel> response) {
                if (response.body() != null) {
                    CustomerReplyModel customerReplyModel = response.body();
                    SuperClassCastBean superClassCastBean = customerReplyModel;

                    apiResponseListener.onSuccess(ApiConstant.CUSTOMER_REPLY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CustomerReplyModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getNotComingReason(String token) {
        Call<NotComingReasonModel> call = apiInterface.getNotComingReason(token);
        call.enqueue(new Callback<NotComingReasonModel>() {
            @Override
            public void onResponse(Call<NotComingReasonModel> call, Response<NotComingReasonModel> response) {
                if (response.body() != null) {
                    NotComingReasonModel notComingReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = notComingReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.NOT_COMING_REASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<NotComingReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getClosureReason(String token) {
        Call<ClouserReasonModel> call = apiInterface.getClosureReason(token);
        call.enqueue(new Callback<ClouserReasonModel>() {
            @Override
            public void onResponse(Call<ClouserReasonModel> call, Response<ClouserReasonModel> response) {
                if (response.body() != null) {
                    ClouserReasonModel clouserReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = clouserReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.CLOUSERREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ClouserReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getClosureSubReason(String token, HashMap<String, String> params) {
        Call<ClouserSubReasonModel> call = apiInterface.getClosureSubReason(token, params);
        call.enqueue(new Callback<ClouserSubReasonModel>() {
            @Override
            public void onResponse(Call<ClouserSubReasonModel> call, Response<ClouserSubReasonModel> response) {
                if (response.body() != null) {
                    ClouserSubReasonModel clouserSubReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = clouserSubReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.CLOUSERSUBREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ClouserSubReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Add History in Database API Code
    public void addCallHistoryData(String token, HashMap<String, String> params) {
        Call<AddCallHistoryUpdateModel> call = apiInterface.addCallHistoryUpdate(token, params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {
                if (response.body() != null) {
                    AddCallHistoryUpdateModel addCallHistoryUpdateModel = response.body();
                    SuperClassCastBean superClassCastBean = addCallHistoryUpdateModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Contact Number  in Database for this function API Code
    public void UpdateContactNumber(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecontacts(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECONTACTNUMBERSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Customer Reply in Database for this function API Code
    public void UpdateCustomerReply(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecustomerreply(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECUSTOMERREPLYSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Taggged SR Status in Database for this function API Code
    public void UpdateTaggedSRStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatetaggedsrstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATETAGGERSRSTATUSSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Booking Date and Time in Database for this function API Code
    public void UpdateBookingate(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatebookingdate(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEBOOKINGDATETIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Call End Time  in Database for this function API Code
    public void UpdateCallEndTime(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecallendtime(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLENDTIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Remark in Database for this function API Code
    public void UpdateRemark(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateremark(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEREMARK, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Next Follow-up date in Database for this function API Code
    public void UpdateNextFollowUpdate(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatefollowupdate(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEFOLLOWUPDATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Contatct  Status in Database for this function API Code
    public void UpdateContactStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecontactstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECONTACTSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Called  Status in Database for this function API Code
    public void UpdateCalledStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecalledstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLEDSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    } // Update Not Coming Reasson in Database for this function API Code

    public void UpdateNotComingReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatenotconingreason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATENOTCOMINGREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }



    // Get Sales Enquiry Data in this Function  API Code
    public void getSalesEnquiryData(String token, HashMap<String, String> params) {
        Call<SalesEnquiryDataModel> call = apiInterface.getSalesEnquiryData(token, params);
        call.enqueue(new Callback<SalesEnquiryDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryDataModel> call, Response<SalesEnquiryDataModel> response) {
                if (response.body() != null) {
                    SalesEnquiryDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESENQUIRYDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESENQUIRYDATA);
            }
        });


    }



    // Get Sales History Data in this Function  API Code
    public void getSalesHistory(String token, HashMap<String, String> params) {
        Call<Sales_HistoryModel> call = apiInterface.getSalesHistory(token, params);
        call.enqueue(new Callback<Sales_HistoryModel>() {
            @Override
            public void onResponse(Call<Sales_HistoryModel> call, Response<Sales_HistoryModel> response) {
                if (response.body() != null) {
                    Sales_HistoryModel sales_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sales_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSALESCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Sales_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Update History Id in  Database for this function API Code
    public void UpdateHistorySales(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesaleshistoryId(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESHISTORYID, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Date of Purchase in Database for this function API Code
    public void Updatedateofpurchase(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatedateofpurchase(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEDATEOFPURCHASE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Reason in Database for this function API Code
    public void UpdateClosureReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateclosurereason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECLOSUREREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Sub Reason in Database for this function API Code
    public void UpdateClosureSubReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateclosuresubreason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECLOSURESUBREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Model in Database for this function API Code
    public void UpdateMake(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatemake(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEMAKE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Model in Database for this function API Code
    public void UpdateModel(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatemodel(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEMODEL, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Pick&Drop in Database for this function API Code
    public void UpdatePickdrop(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatepickdrop(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEPICKDROP, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }















    // Get Enquiry Incoming  Data Fetch in this Function  API Code
    public void getEnquiryIncomingData(String token, HashMap<String, String> params) {
        Call<Incoming_SalesModel> call = apiInterface.getEnquiryIncomingdata(token, params);
        call.enqueue(new Callback<Incoming_SalesModel>() {
            @Override
            public void onResponse(Call<Incoming_SalesModel> call, Response<Incoming_SalesModel> response) {
                if (response.body() != null) {
                    Incoming_SalesModel incoming_salesModel = response.body();
                    SuperClassCastBean superClassCastBean = incoming_salesModel;

                    apiResponseListener.onSuccess(ApiConstant.ENQUIRYINCOMINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Incoming_SalesModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    public void deAllocation(String token) {
        Call<CommonModel> call = apiInterface.setDeAllocation(token);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.DEALLOCATION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Get Missed Data in this Function  API Code
    public void getMissedData(String token, HashMap<String, String> params) {
        Call<MissedDataModel_SR> call = apiInterface.getMissedData(token, params);
        call.enqueue(new Callback<MissedDataModel_SR>() {
            @Override
            public void onResponse(Call<MissedDataModel_SR> call, Response<MissedDataModel_SR> response) {
                if (response.body() != null) {
                    MissedDataModel_SR missedDataModelSR = response.body();
                    SuperClassCastBean superClassCastBean = missedDataModelSR;

                    apiResponseListener.onSuccess(ApiConstant.MISSEDDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MissedDataModel_SR> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.MISSEDDATA);
            }
        });


    }

    // Get Missed Data Sales in this Function  API Code
    public void getMissedDataSales(String token, HashMap<String, String> params) {
        Call<MissedDataModel_Sales> call = apiInterface.getMissedDataSales(token, params);
        call.enqueue(new Callback<MissedDataModel_Sales>() {
            @Override
            public void onResponse(Call<MissedDataModel_Sales> call, Response<MissedDataModel_Sales> response) {
                if (response.body() != null) {
                    MissedDataModel_Sales missedDataModel_sales = response.body();
                    SuperClassCastBean superClassCastBean = missedDataModel_sales;

                    apiResponseListener.onSuccess(ApiConstant.MISSEDDATASALES, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MissedDataModel_Sales> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.MISSEDDATASALES);
            }
        });


    }

    // Get Single Customer history Data in this Function  API Code
    public void getSingleCustomerHistoryData(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData> call = apiInterface.getsinglecustomerhistory(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData> call, Response<SingleCustomerHistoryData> response) {
                if (response.body() != null) {
                    SingleCustomerHistoryData singleCustomerHistoryData = response.body();
                    SuperClassCastBean superClassCastBean = singleCustomerHistoryData;

                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Single Customer Sales history Data in this Function  API Code
    public void getSingleCustomerHistoryData_Sales(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData_Sales> call = apiInterface.getsinglecustomerhistorysales(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData_Sales>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData_Sales> call, Response<SingleCustomerHistoryData_Sales> response) {
                if (response.body() != null) {
                    SingleCustomerHistoryData_Sales singleCustomerHistoryData = response.body();
                    SuperClassCastBean superClassCastBean = singleCustomerHistoryData;

                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATASALES, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData_Sales> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void addMissedNumber(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.addtMissedNumber(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.CommonModel, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Add Exception  in Database for this function API Code
    public void addException(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.AddException(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDEXCEPTION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get Follow Up Done List
    public void getFollowUpDoneList(String token) {
        Call<FollowUpListModel> call = apiInterface.getSalesFollowupDoneList(token);
        call.enqueue(new Callback<FollowUpListModel>() {
            @Override
            public void onResponse(Call<FollowUpListModel> call, Response<FollowUpListModel> response) {
                if (response.body() != null) {
                    FollowUpListModel followUpListModel = response.body();
                    SuperClassCastBean superClassCastBean = followUpListModel;

                    apiResponseListener.onSuccess(ApiConstant.GETFOLLOWUPDONELIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FollowUpListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Follow Up Conatct Status List
    public void getFollowContatctStatusList(String token) {
        Call<FollowUpListModel> call = apiInterface.getSalesFollowupConatctList(token);
        call.enqueue(new Callback<FollowUpListModel>() {
            @Override
            public void onResponse(Call<FollowUpListModel> call, Response<FollowUpListModel> response) {
                if (response.body() != null) {
                    FollowUpListModel followUpListModel = response.body();
                    SuperClassCastBean superClassCastBean = followUpListModel;

                    apiResponseListener.onSuccess(ApiConstant.GETFOLLOWUPSTATUSLIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FollowUpListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleFollowUpStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesalescontactstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleFollowUpDoneStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesfollowupdonestatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPDONESTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    public void createFolloup(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.setCreateFollowup(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.CREATEFOLLOWUP, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    public void createServiceRequest(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.getServiceRequest(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.SERVICEREQUEST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    public void CancelServiceRequest(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.CancelServiceRequest(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.CANCELBOOKINGREQUEST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }
*/

}
