package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class ReportingModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"TotalCustomer":155,"TotalCalls":8,"CallDone":6,"CallNotDone":2,"Followups":1,"ClosedFollowups":0,"EntryDoneOnHc":1}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * TotalCustomer : 155
         * TotalCalls : 8
         * CallDone : 6
         * CallNotDone : 2
         * Followups : 1
         * ClosedFollowups : 0
         * EntryDoneOnHc : 1
         */

        @SerializedName("TotalCustomer")
        private int TotalCustomer;
        @SerializedName("TotalCalls")
        private int TotalCalls;
        @SerializedName("CallDone")
        private int CallDone;
        @SerializedName("CallNotDone")
        private int CallNotDone;
        @SerializedName("Followups")
        private int Followups;
        @SerializedName("ClosedFollowups")
        private int ClosedFollowups;
        @SerializedName("EntryDoneOnHc")
        private int EntryDoneOnHc;

        public int getTotalCustomer() {
            return TotalCustomer;
        }

        public void setTotalCustomer(int TotalCustomer) {
            this.TotalCustomer = TotalCustomer;
        }

        public int getTotalCalls() {
            return TotalCalls;
        }

        public void setTotalCalls(int TotalCalls) {
            this.TotalCalls = TotalCalls;
        }

        public int getCallDone() {
            return CallDone;
        }

        public void setCallDone(int CallDone) {
            this.CallDone = CallDone;
        }

        public int getCallNotDone() {
            return CallNotDone;
        }

        public void setCallNotDone(int CallNotDone) {
            this.CallNotDone = CallNotDone;
        }

        public int getFollowups() {
            return Followups;
        }

        public void setFollowups(int Followups) {
            this.Followups = Followups;
        }

        public int getClosedFollowups() {
            return ClosedFollowups;
        }

        public void setClosedFollowups(int ClosedFollowups) {
            this.ClosedFollowups = ClosedFollowups;
        }

        public int getEntryDoneOnHc() {
            return EntryDoneOnHc;
        }

        public void setEntryDoneOnHc(int EntryDoneOnHc) {
            this.EntryDoneOnHc = EntryDoneOnHc;
        }
    }
}
