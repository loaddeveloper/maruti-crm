package com.hero.weconnect.mysmartcrm.CallUtils;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Service;
import android.icu.text.IDNA;
import android.media.AudioManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import static android.media.AudioManager.ADJUST_RAISE;

public class MyAccessibilityService extends AccessibilityService {

    Service mService = null;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
    }

    @Override
    public void onInterrupt() {

    }


    @Override
    protected void onServiceConnected() {
    }

    @Override
    public void onCreate() {
        this.mService = this;
    }
}
