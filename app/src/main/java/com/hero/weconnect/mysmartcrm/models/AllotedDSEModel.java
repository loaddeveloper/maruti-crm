package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class AllotedDSEModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"alloted_master_login_id":"60550d83-21d8-4c8d-a2b0-85e94e176931","username":"HEENA1037501","DSE":"HEENA1037501"}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * alloted_master_login_id : 60550d83-21d8-4c8d-a2b0-85e94e176931
         * username : HEENA1037501
         * DSE : HEENA1037501
         */

        @SerializedName("alloted_master_login_id")
        private String allotedMasterLoginId;
        @SerializedName("username")
        private String username;
        @SerializedName("DSE")
        private String DSE;

        public String getAllotedMasterLoginId() {
            return allotedMasterLoginId;
        }

        public void setAllotedMasterLoginId(String allotedMasterLoginId) {
            this.allotedMasterLoginId = allotedMasterLoginId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getDSE() {
            return DSE;
        }

        public void setDSE(String DSE) {
            this.DSE = DSE;
        }
    }
}
