package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.adapter.VideoAdapter;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.models.VideoDataModel;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoActivity extends AppCompatActivity implements ApiResponseListener {

    private TextView mTitle;
    private TextView mType;
    private RecyclerView mVideoRc;
    VideoAdapter videoAdapter;
    ArrayList<VideoDataModel.DataDTO> list;

    ApiController apiController;
    CustomDialog waitting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        list = new ArrayList<>();
        apiController = new ApiController(this);
        waitting = new CustomDialog(this);

         HashMap<String,String> params = new HashMap<>();
         params.put("type",CommonVariables.WECONNECT_APP_ROLE);

         waitting.show();
        apiController.getVideoData(params);



        initView();
    }

    public void initView() {

        mTitle = findViewById(R.id.title);
        mType = findViewById(R.id.type);
        mVideoRc = findViewById(R.id.videoRc);
        Toolbar toolbar = findViewById(R.id.dashtoolbar);
        toolbar.setTitle("Training Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        if(CommonVariables.WECONNECT_APP_ROLE.equals("SR"))
        {
            mType.setText("Service");
        }else
        {
            mType.setText("Sales");

        }

        recyclerviewSetup(list);
    }


    public void recyclerviewSetup(ArrayList<VideoDataModel.DataDTO> list)
    {
        mVideoRc.hasFixedSize();
        mVideoRc.setLayoutManager(new LinearLayoutManager(this));
        videoAdapter = new VideoAdapter(this,list);
        mVideoRc.setAdapter(videoAdapter);
        videoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if(beanTag.matches(ApiConstant.VIDEODATA))
        {
            VideoDataModel videoDataModel =(VideoDataModel) superClassCastBean;
             list.clear();
            if(videoDataModel.getData().size()>0)
            {
                waitting.dismiss();

                list.addAll(videoDataModel.getData());

                recyclerviewSetup(list);
            }else
            {
                waitting.dismiss();
            }

        }

    }

    @Override
    public void onFailure(String msg) {
        if(msg.equals("Failure"))
        {
            waitting.dismiss();

        }

    }

    @Override
    public void onError(String msg) {
        if(msg.equals("No Data"))
        {
            waitting.dismiss();

        }
    }
}