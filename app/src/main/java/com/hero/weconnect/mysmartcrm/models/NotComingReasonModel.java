package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class NotComingReasonModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"Reason":"Any Other"},{"Reason":"Channel Partner Related"},{"Reason":"Hero GoodLife Related"},{"Reason":"Legal Complaint"},{"Reason":"Product Complaint"},{"Reason":"Sales Related"},{"Reason":"Service Quality"},{"Reason":"Spare Parts Related"},{"Reason":"Staff Behaviour"}]
     * ErrorMessage : null
     */

    private String Message;
    private Object ErrorMessage;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Object getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Reason : Any Other
         */

        private String Reason;

        public String getReason() {
            return Reason;
        }

        public void setReason(String Reason) {
            this.Reason = Reason;
        }
    }
}
