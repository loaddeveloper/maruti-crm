package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;


import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

public class CreateFolloupModel extends SuperClassCastBean {


    /**
     * message : Followup entries are not available right now.
     * data : null
     */

    private String message;
    private Object data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
