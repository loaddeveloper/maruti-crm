package com.hero.weconnect.mysmartcrm.Utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.hero.weconnect.mysmartcrm.R;

public class CustomDialog extends ProgressDialog {


    public CustomDialog(Context context) {
        super(context);
        setTitle(context.getResources().getString(R.string.loading));
        setCancelable(false);
    }
}
