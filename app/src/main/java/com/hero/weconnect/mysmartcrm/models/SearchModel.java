package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SearchModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"id":"246696b3-8b82-4242-9567-75a36e9bcd0e","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":2,"id":"2d6e28c5-5605-4292-a74e-95afc24f3131","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"8209388849","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":3,"id":"4614b6a7-8029-43a6-8fad-97db395ca790","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":4,"id":"e3b568df-1a87-403b-8d43-9f838cc4d3fe","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":5,"id":"173b98ee-7f83-4c2f-b452-aab103072a9b","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":6,"id":"76561ac2-4bb9-4ebf-b48b-b2ecaebad221","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":7,"id":"ae2a7162-9e96-442b-8d45-dfb9d6e3b807","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":8,"id":"b08c3300-7adf-4b0f-9d45-f42b22d599d1","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":9,"id":"09f21faf-6d4f-42c3-b141-f79f683e6586","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","model":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * id : 246696b3-8b82-4242-9567-75a36e9bcd0e
         * enquirynumber : 11935-01-SENQ-0119-3394
         * enquiry_open_date : 2019-01-02 00:00:00.0000000
         * enquiry_status : Open
         * next_followup_date : 2020-09-03T05:30:00
         * expected_date_purchase : 2022-12-11 05:30:00.0000000
         * cusotmer_first_and_last_name : SAMIM KHAN
         * mobilenumber : 9969996232
         * emailid :
         * address : VARANASI,VARANASI,VARANASI,(UP)
         * age : 28
         * gender : M
         * model_interested_in : PASSION PRO
         * model : PASSION PRO
         * exchange_required : N
         * finance_required : Y
         * dse_name : AWASTHI HIMANSHU DHAR
         * position_of_executive : DSE
         * enquiry_id : 1-30TCJ6MT
         * dealer_name : VISHAL AUTO AGENCIES
         * last_follow_up_date_if_any :
         * enquiry_comments : HIMANSHU11935~
         * dse_employee_id : 11935-02-EMP-7
         * existing_vehicle : First Time Buyer
         * test_ride_required :
         * test_ride_required_time :
         * test_ride_taken : N
         * test_ride_taken_time :
         * enquiry_source :
         * awareness_source :
         * opinion_leader :
         * financier :
         * calledstatus :
         */

        @SerializedName("sno")
        private int sno;
        @SerializedName("id")
        private String id;
        @SerializedName("enquirynumber")
        private String enquirynumber;
        @SerializedName("enquiry_open_date")
        private String enquiryOpenDate;
        @SerializedName("enquiry_status")
        private String enquiryStatus;
        @SerializedName("next_followup_date")
        private String nextFollowupDate;
        @SerializedName("expected_date_purchase")
        private String expectedDatePurchase;
        @SerializedName("cusotmer_first_and_last_name")
        private String cusotmerFirstAndLastName;
        @SerializedName("mobilenumber")
        private String mobilenumber;
        @SerializedName("emailid")
        private String emailid;
        @SerializedName("address")
        private String address;
        @SerializedName("age")
        private String age;
        @SerializedName("gender")
        private String gender;
        @SerializedName("model_interested_in")
        private String modelInterestedIn;
        @SerializedName("model")
        private String model;
        @SerializedName("exchange_required")
        private String exchangeRequired;
        @SerializedName("finance_required")
        private String financeRequired;
        @SerializedName("dse_name")
        private String dseName;
        @SerializedName("position_of_executive")
        private String positionOfExecutive;
        @SerializedName("enquiry_id")
        private String enquiryId;
        @SerializedName("dealer_name")
        private String dealerName;
        @SerializedName("last_follow_up_date_if_any")
        private String lastFollowUpDateIfAny;
        @SerializedName("enquiry_comments")
        private String enquiryComments;
        @SerializedName("dse_employee_id")
        private String dseEmployeeId;
        @SerializedName("existing_vehicle")
        private String existingVehicle;
        @SerializedName("test_ride_required")
        private String testRideRequired;
        @SerializedName("test_ride_required_time")
        private String testRideRequiredTime;
        @SerializedName("test_ride_taken")
        private String testRideTaken;
        @SerializedName("test_ride_taken_time")
        private String testRideTakenTime;
        @SerializedName("enquiry_source")
        private String enquirySource;
        @SerializedName("awareness_source")
        private String awarenessSource;
        @SerializedName("opinion_leader")
        private String opinionLeader;
        @SerializedName("financier")
        private String financier;
        @SerializedName("calledstatus")
        private String calledstatus;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEnquirynumber() {
            return enquirynumber;
        }

        public void setEnquirynumber(String enquirynumber) {
            this.enquirynumber = enquirynumber;
        }

        public String getEnquiryOpenDate() {
            return enquiryOpenDate;
        }

        public void setEnquiryOpenDate(String enquiryOpenDate) {
            this.enquiryOpenDate = enquiryOpenDate;
        }

        public String getEnquiryStatus() {
            return enquiryStatus;
        }

        public void setEnquiryStatus(String enquiryStatus) {
            this.enquiryStatus = enquiryStatus;
        }

        public String getNextFollowupDate() {
            return nextFollowupDate;
        }

        public void setNextFollowupDate(String nextFollowupDate) {
            this.nextFollowupDate = nextFollowupDate;
        }

        public String getExpectedDatePurchase() {
            return expectedDatePurchase;
        }

        public void setExpectedDatePurchase(String expectedDatePurchase) {
            this.expectedDatePurchase = expectedDatePurchase;
        }

        public String getCusotmerFirstAndLastName() {
            return cusotmerFirstAndLastName;
        }

        public void setCusotmerFirstAndLastName(String cusotmerFirstAndLastName) {
            this.cusotmerFirstAndLastName = cusotmerFirstAndLastName;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getModelInterestedIn() {
            return modelInterestedIn;
        }

        public void setModelInterestedIn(String modelInterestedIn) {
            this.modelInterestedIn = modelInterestedIn;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getExchangeRequired() {
            return exchangeRequired;
        }

        public void setExchangeRequired(String exchangeRequired) {
            this.exchangeRequired = exchangeRequired;
        }

        public String getFinanceRequired() {
            return financeRequired;
        }

        public void setFinanceRequired(String financeRequired) {
            this.financeRequired = financeRequired;
        }

        public String getDseName() {
            return dseName;
        }

        public void setDseName(String dseName) {
            this.dseName = dseName;
        }

        public String getPositionOfExecutive() {
            return positionOfExecutive;
        }

        public void setPositionOfExecutive(String positionOfExecutive) {
            this.positionOfExecutive = positionOfExecutive;
        }

        public String getEnquiryId() {
            return enquiryId;
        }

        public void setEnquiryId(String enquiryId) {
            this.enquiryId = enquiryId;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public String getLastFollowUpDateIfAny() {
            return lastFollowUpDateIfAny;
        }

        public void setLastFollowUpDateIfAny(String lastFollowUpDateIfAny) {
            this.lastFollowUpDateIfAny = lastFollowUpDateIfAny;
        }

        public String getEnquiryComments() {
            return enquiryComments;
        }

        public void setEnquiryComments(String enquiryComments) {
            this.enquiryComments = enquiryComments;
        }

        public String getDseEmployeeId() {
            return dseEmployeeId;
        }

        public void setDseEmployeeId(String dseEmployeeId) {
            this.dseEmployeeId = dseEmployeeId;
        }

        public String getExistingVehicle() {
            return existingVehicle;
        }

        public void setExistingVehicle(String existingVehicle) {
            this.existingVehicle = existingVehicle;
        }

        public String getTestRideRequired() {
            return testRideRequired;
        }

        public void setTestRideRequired(String testRideRequired) {
            this.testRideRequired = testRideRequired;
        }

        public String getTestRideRequiredTime() {
            return testRideRequiredTime;
        }

        public void setTestRideRequiredTime(String testRideRequiredTime) {
            this.testRideRequiredTime = testRideRequiredTime;
        }

        public String getTestRideTaken() {
            return testRideTaken;
        }

        public void setTestRideTaken(String testRideTaken) {
            this.testRideTaken = testRideTaken;
        }

        public String getTestRideTakenTime() {
            return testRideTakenTime;
        }

        public void setTestRideTakenTime(String testRideTakenTime) {
            this.testRideTakenTime = testRideTakenTime;
        }

        public String getEnquirySource() {
            return enquirySource;
        }

        public void setEnquirySource(String enquirySource) {
            this.enquirySource = enquirySource;
        }

        public String getAwarenessSource() {
            return awarenessSource;
        }

        public void setAwarenessSource(String awarenessSource) {
            this.awarenessSource = awarenessSource;
        }

        public String getOpinionLeader() {
            return opinionLeader;
        }

        public void setOpinionLeader(String opinionLeader) {
            this.opinionLeader = opinionLeader;
        }

        public String getFinancier() {
            return financier;
        }

        public void setFinancier(String financier) {
            this.financier = financier;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
