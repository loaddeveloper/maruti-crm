package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class PSFSOPRatingQuestionsModel extends SuperClassCastBean {

    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("id")
        private String id;
        @SerializedName("psf_sop_question")
        private String psfSopQuestion;
        @SerializedName("question_type")
        private String questionType;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPsfSopQuestion() {
            return psfSopQuestion;
        }

        public void setPsfSopQuestion(String psfSopQuestion) {
            this.psfSopQuestion = psfSopQuestion;
        }

        public String getQuestionType() {
            return questionType;
        }

        public void setQuestionType(String questionType) {
            this.questionType = questionType;
        }
    }
}
