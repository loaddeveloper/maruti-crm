package com.hero.weconnect.mysmartcrm.models;

import java.util.List;

public class FilterDataSendModel {


    /**
     * EnquirySources : ["Telephonic"]
     * ExchangeRequired : Y
     * FilterType : Filter
     * FinanceRequired : Y
     * FollowupType : Pending
     * Models : ["E-SPRINT","CRUZ"]
     * Offset : 0
     * UserName : All
     */

    private String ExchangeRequired;
    private String FilterType;
    private String FinanceRequired;
    private String FollowupType;
    private int Offset;
    private String UserName;
    private List<String> EnquirySources;
    private List<String> Models;

    public String getExchangeRequired() {
        return ExchangeRequired;
    }

    public void setExchangeRequired(String ExchangeRequired) {
        this.ExchangeRequired = ExchangeRequired;
    }

    public String getFilterType() {
        return FilterType;
    }

    public void setFilterType(String FilterType) {
        this.FilterType = FilterType;
    }

    public String getFinanceRequired() {
        return FinanceRequired;
    }

    public void setFinanceRequired(String FinanceRequired) {
        this.FinanceRequired = FinanceRequired;
    }

    public String getFollowupType() {
        return FollowupType;
    }

    public void setFollowupType(String FollowupType) {
        this.FollowupType = FollowupType;
    }

    public int getOffset() {
        return Offset;
    }

    public void setOffset(int Offset) {
        this.Offset = Offset;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public List<String> getEnquirySources() {
        return EnquirySources;
    }

    public void setEnquirySources(List<String> EnquirySources) {
        this.EnquirySources = EnquirySources;
    }

    public List<String> getModels() {
        return Models;
    }

    public void setModels(List<String> Models) {
        this.Models = Models;
    }
}
