package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SalesEnquiryCallDoneDataModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"id":"3616af14-95a7-4d94-88ad-1eeff0dfb658","enquirynumber":"11036-01-SENQ-0620-40","enquiry_open_date":"2020-06-11 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-09T12:43:57","expected_date_of_purchase":"1900-01-01T00:00:00","cusotmer_first_and_last_name":"BIKRAM ROUT","mobilenumber":"1111111111","emailid":"","address":"JAJAPUR,JAJAPUR,JAJAPUR,(ODISHA)","age":"38","gender":"M","model":"XTREME 200S","model_interested_in":"XTREME 200S","exchange_required":"N","finance_required":"N","dse_name":"KHAN ISMAIL","position_of_executive":"DSE","enquiry_id":"1-5GXZPMS","dealer_name":"SOUMIK AUTOMOBILES","last_follow_up_date_if_any":"2020-07-15 19:34:54.0000000","enquiry_comments":"ISMAIL11036~","dse_employee_id":"11036-02-EMP-31","existing_vehicle":"Two Wheeler","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"Y","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"CALL DONE","followup_status":"Open","followup_done":"contacted","closurereason":"","closure_sub_reason":"","make":"","model1":"","remark":"Booked"},{"sno":2,"id":"9d2fd8b6-cc52-47bd-b60f-177f91f70134","enquirynumber":"10196-01-SENQ-0820-594","enquiry_open_date":"2020-08-12 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"2021-01-22T12:44:31","cusotmer_first_and_last_name":"GOUTAM DUTTA","mobilenumber":"1111111111","emailid":"","address":"CHAS,BOKARO,BOKARO,(JHARKHAND)","age":"29","gender":"M","model":"DESTINI 125","model_interested_in":"DESTINI 125","exchange_required":"N","finance_required":"Y","dse_name":"ANSARI NAJIMUDDIN","position_of_executive":"DSE","enquiry_id":"2-6M0XFFS","dealer_name":"HINDUSTAN AUTO AGENCY","last_follow_up_date_if_any":"","enquiry_comments":"NAJI10196~","dse_employee_id":"10196-01-EMP-108","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"CALL DONE","followup_status":"Open","followup_done":"contacted","closurereason":"","closure_sub_reason":"","make":"","model1":"","remark":"No Response"},{"sno":3,"id":"1bd15442-18d2-4e4d-952f-076f130a552e","enquirynumber":"64154-01-SENQ-0220-1319","enquiry_open_date":"2020-02-15 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-02T12:44:31","expected_date_of_purchase":"1900-01-01T00:00:00","cusotmer_first_and_last_name":"MANJUR ALAM","mobilenumber":"1111111111","emailid":"","address":"KARANDIGHI,KARANDIGHI,UTTAR DINAJPUR,(W.BENGAL)","age":"25","gender":"M","model":"SPLENDOR +","model_interested_in":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":"PAUL RAMDEB","position_of_executive":"DSE","enquiry_id":"1-3XZEHIO","dealer_name":"Tarique Auto Centre","last_follow_up_date_if_any":"","enquiry_comments":"64154RAM~","dse_employee_id":"64154-02-EMP-2","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"Existing customer","opinion_leader":"","financier":"","calledstatus":"CALL DONE","followup_status":"Open","followup_done":"contacted","closurereason":"","closure_sub_reason":"","make":"","model1":"","remark":"Booked"}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * id : 3616af14-95a7-4d94-88ad-1eeff0dfb658
         * enquirynumber : 11036-01-SENQ-0620-40
         * enquiry_open_date : 2020-06-11 00:00:00.0000000
         * enquiry_status : Open
         * next_followup_date : 2021-01-09T12:43:57
         * expected_date_of_purchase : 1900-01-01T00:00:00
         * cusotmer_first_and_last_name : BIKRAM ROUT
         * mobilenumber : 1111111111
         * emailid :
         * address : JAJAPUR,JAJAPUR,JAJAPUR,(ODISHA)
         * age : 38
         * gender : M
         * model : XTREME 200S
         * model_interested_in : XTREME 200S
         * exchange_required : N
         * finance_required : N
         * dse_name : KHAN ISMAIL
         * position_of_executive : DSE
         * enquiry_id : 1-5GXZPMS
         * dealer_name : SOUMIK AUTOMOBILES
         * last_follow_up_date_if_any : 2020-07-15 19:34:54.0000000
         * enquiry_comments : ISMAIL11036~
         * dse_employee_id : 11036-02-EMP-31
         * existing_vehicle : Two Wheeler
         * test_ride_required :
         * test_ride_required_time :
         * test_ride_taken : Y
         * test_ride_taken_time :
         * enquiry_source :
         * awareness_source :
         * opinion_leader :
         * financier :
         * calledstatus : CALL DONE
         * followup_status : Open
         * followup_done : contacted
         * closurereason :
         * closure_sub_reason :
         * make :
         * model1 :
         * remark : Booked
         */

        @SerializedName("sno")
        private int sno;
        @SerializedName("id")
        private String id;
        @SerializedName("enquirynumber")
        private String enquirynumber;
        @SerializedName("enquiry_open_date")
        private String enquiryOpenDate;
        @SerializedName("enquiry_status")
        private String enquiryStatus;
        @SerializedName("next_followup_date")
        private String nextFollowupDate;
        @SerializedName("expected_date_of_purchase")
        private String expectedDateOfPurchase;
        @SerializedName("cusotmer_first_and_last_name")
        private String cusotmerFirstAndLastName;
        @SerializedName("mobilenumber")
        private String mobilenumber;
        @SerializedName("emailid")
        private String emailid;
        @SerializedName("address")
        private String address;
        @SerializedName("age")
        private String age;
        @SerializedName("gender")
        private String gender;
        @SerializedName("model")
        private String model;
        @SerializedName("model_interested_in")
        private String modelInterestedIn;
        @SerializedName("exchange_required")
        private String exchangeRequired;
        @SerializedName("finance_required")
        private String financeRequired;
        @SerializedName("dse_name")
        private String dseName;
        @SerializedName("position_of_executive")
        private String positionOfExecutive;
        @SerializedName("enquiry_id")
        private String enquiryId;
        @SerializedName("dealer_name")
        private String dealerName;
        @SerializedName("last_follow_up_date_if_any")
        private String lastFollowUpDateIfAny;
        @SerializedName("enquiry_comments")
        private String enquiryComments;
        @SerializedName("dse_employee_id")
        private String dseEmployeeId;
        @SerializedName("existing_vehicle")
        private String existingVehicle;
        @SerializedName("test_ride_required")
        private String testRideRequired;
        @SerializedName("test_ride_required_time")
        private String testRideRequiredTime;
        @SerializedName("test_ride_taken")
        private String testRideTaken;
        @SerializedName("test_ride_taken_time")
        private String testRideTakenTime;
        @SerializedName("enquiry_source")
        private String enquirySource;
        @SerializedName("awareness_source")
        private String awarenessSource;
        @SerializedName("opinion_leader")
        private String opinionLeader;
        @SerializedName("financier")
        private String financier;
        @SerializedName("calledstatus")
        private String calledstatus;
        @SerializedName("followup_status")
        private String followupStatus;
        @SerializedName("followup_done")
        private String followupDone;
        @SerializedName("closurereason")
        private String closurereason;
        @SerializedName("closure_sub_reason")
        private String closureSubReason;
        @SerializedName("make")
        private String make;
        @SerializedName("model1")
        private String model1;
        @SerializedName("remark")
        private String remark;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEnquirynumber() {
            return enquirynumber;
        }

        public void setEnquirynumber(String enquirynumber) {
            this.enquirynumber = enquirynumber;
        }

        public String getEnquiryOpenDate() {
            return enquiryOpenDate;
        }

        public void setEnquiryOpenDate(String enquiryOpenDate) {
            this.enquiryOpenDate = enquiryOpenDate;
        }

        public String getEnquiryStatus() {
            return enquiryStatus;
        }

        public void setEnquiryStatus(String enquiryStatus) {
            this.enquiryStatus = enquiryStatus;
        }

        public String getNextFollowupDate() {
            return nextFollowupDate;
        }

        public void setNextFollowupDate(String nextFollowupDate) {
            this.nextFollowupDate = nextFollowupDate;
        }

        public String getExpectedDateOfPurchase() {
            return expectedDateOfPurchase;
        }

        public void setExpectedDateOfPurchase(String expectedDateOfPurchase) {
            this.expectedDateOfPurchase = expectedDateOfPurchase;
        }

        public String getCusotmerFirstAndLastName() {
            return cusotmerFirstAndLastName;
        }

        public void setCusotmerFirstAndLastName(String cusotmerFirstAndLastName) {
            this.cusotmerFirstAndLastName = cusotmerFirstAndLastName;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getModelInterestedIn() {
            return modelInterestedIn;
        }

        public void setModelInterestedIn(String modelInterestedIn) {
            this.modelInterestedIn = modelInterestedIn;
        }

        public String getExchangeRequired() {
            return exchangeRequired;
        }

        public void setExchangeRequired(String exchangeRequired) {
            this.exchangeRequired = exchangeRequired;
        }

        public String getFinanceRequired() {
            return financeRequired;
        }

        public void setFinanceRequired(String financeRequired) {
            this.financeRequired = financeRequired;
        }

        public String getDseName() {
            return dseName;
        }

        public void setDseName(String dseName) {
            this.dseName = dseName;
        }

        public String getPositionOfExecutive() {
            return positionOfExecutive;
        }

        public void setPositionOfExecutive(String positionOfExecutive) {
            this.positionOfExecutive = positionOfExecutive;
        }

        public String getEnquiryId() {
            return enquiryId;
        }

        public void setEnquiryId(String enquiryId) {
            this.enquiryId = enquiryId;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public String getLastFollowUpDateIfAny() {
            return lastFollowUpDateIfAny;
        }

        public void setLastFollowUpDateIfAny(String lastFollowUpDateIfAny) {
            this.lastFollowUpDateIfAny = lastFollowUpDateIfAny;
        }

        public String getEnquiryComments() {
            return enquiryComments;
        }

        public void setEnquiryComments(String enquiryComments) {
            this.enquiryComments = enquiryComments;
        }

        public String getDseEmployeeId() {
            return dseEmployeeId;
        }

        public void setDseEmployeeId(String dseEmployeeId) {
            this.dseEmployeeId = dseEmployeeId;
        }

        public String getExistingVehicle() {
            return existingVehicle;
        }

        public void setExistingVehicle(String existingVehicle) {
            this.existingVehicle = existingVehicle;
        }

        public String getTestRideRequired() {
            return testRideRequired;
        }

        public void setTestRideRequired(String testRideRequired) {
            this.testRideRequired = testRideRequired;
        }

        public String getTestRideRequiredTime() {
            return testRideRequiredTime;
        }

        public void setTestRideRequiredTime(String testRideRequiredTime) {
            this.testRideRequiredTime = testRideRequiredTime;
        }

        public String getTestRideTaken() {
            return testRideTaken;
        }

        public void setTestRideTaken(String testRideTaken) {
            this.testRideTaken = testRideTaken;
        }

        public String getTestRideTakenTime() {
            return testRideTakenTime;
        }

        public void setTestRideTakenTime(String testRideTakenTime) {
            this.testRideTakenTime = testRideTakenTime;
        }

        public String getEnquirySource() {
            return enquirySource;
        }

        public void setEnquirySource(String enquirySource) {
            this.enquirySource = enquirySource;
        }

        public String getAwarenessSource() {
            return awarenessSource;
        }

        public void setAwarenessSource(String awarenessSource) {
            this.awarenessSource = awarenessSource;
        }

        public String getOpinionLeader() {
            return opinionLeader;
        }

        public void setOpinionLeader(String opinionLeader) {
            this.opinionLeader = opinionLeader;
        }

        public String getFinancier() {
            return financier;
        }

        public void setFinancier(String financier) {
            this.financier = financier;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getFollowupStatus() {
            return followupStatus;
        }

        public void setFollowupStatus(String followupStatus) {
            this.followupStatus = followupStatus;
        }

        public String getFollowupDone() {
            return followupDone;
        }

        public void setFollowupDone(String followupDone) {
            this.followupDone = followupDone;
        }

        public String getClosurereason() {
            return closurereason;
        }

        public void setClosurereason(String closurereason) {
            this.closurereason = closurereason;
        }

        public String getClosureSubReason() {
            return closureSubReason;
        }

        public void setClosureSubReason(String closureSubReason) {
            this.closureSubReason = closureSubReason;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel1() {
            return model1;
        }

        public void setModel1(String model1) {
            this.model1 = model1;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
