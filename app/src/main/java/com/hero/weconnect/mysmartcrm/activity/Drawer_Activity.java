package com.hero.weconnect.mysmartcrm.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.role.RoleManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.hero.weconnect.mysmartcrm.CallUtils.CallRecorderService;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPrefCustom;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.ConnectionDetector;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.Utils.NetworkCheckerService;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShowCustom;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Custom;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Historydata;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Historydatacustom;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CallProgressModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CampaignModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CustomHistoryModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.DataFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.FieldUpdateModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetTemplateListModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.InputFieldModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.TokenModel;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;
import static java.lang.Thread.sleep;

public class Drawer_Activity extends AppCompatActivity implements Adapter_Custom.CustomButtonListener,
        AdapterTemplateShow.CustomButtonListener, ApiResponseListener, com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener {

    // private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawerLayout;
    NavigationView nav_view;
    AppBarLayout Appbar;
    private TextView nav_header_user_name;
    NavigationView arcNavigationView;
    String datevalue="0";
    String datevalueend="0";
    Intent recordService;

    /*------------------------------------Activity Variable Declare---------------------------------------------------*/
    public static final int REQUEST_PERMISSION = 0;
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    public static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;
    public static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    public static int calling_list_serial_no;
    public int dataposition;
    CountDownTimer countDownTimer;
    public boolean calldone = false;
    public static boolean whatsupclick = false, callstuck = false, textmessageclick = false;
    public static RecyclerView recyclerView;
    public ArrayList<Custom_Model.DataBean> serviceReminderDataList = new ArrayList<>();
    public ArrayList<CustomHistoryModel.DataDTO> serviceReminderHistoryDataList = new ArrayList<>();
    public Adapter_Custom adapter;
    public int call_current_position = 0, position = 0, state, lastposition;
    public String string_called_status = "", token = "", string_customername, string_masterId, string_mobileno, selectedPath = "", oldhid = "";
    public String Callingstatus = "BUSY", offset = "0";
    public ImageButton startcall, endcall, pausecall;
    public boolean stop = false;
    public Date currentdail, discom;
    public CustomDialog customWaitingDialog;
    public HashMap<String, String> params = new HashMap<>();
    public ArrayList<String> arrayList_matserId = new ArrayList<String>();
    public ArrayList<String> arrayList_datevalue = new ArrayList<String>();
    public ArrayList<String> arrayList_datevalueend= new ArrayList<String>();
    public ArrayList<String> arrayList_customername = new ArrayList<String>();
    public ArrayList<String> arrayList_callstatus = new ArrayList<String>();
    public ArrayList<String> arrayList_mobileno = new ArrayList<String>();
    public ArrayList<DataFieldModel.DataBean> dataFieldModelArrayList = new ArrayList<>();
    public ApiController apiController;
    public com.hero.weconnect.mysmartcrm.Retrofit.ApiController apiControllerNew;
    public CommonSharedPrefCustom commonSharedPref;
    public Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    public SharedPreferences sharedPreferencescallUI;
    public Spinner contactstatus, status;
    public ImageView iv_booking, iv_nextfollowupdate, iv_show_calling_history, iv_whatsapp_message;
    public String historyID, callstatus, pickanddrop, contacted_status, customer_reply, whatsapp_number, String_status;
    public ArrayAdapter<String> CampaignArrayAdapter, CallArrayAdapter;
    public ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    public Adapter_SR_Historydatacustom adapter_sr_historydata;
    public boolean morebtncliked = false;
    public File audiofile = null;
   // public AudioManager am = null;
    public MediaRecorder recorder;
    public AdapterTemplateShowCustom adapterSettingTemplet;
    public ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    public Dialog Whatsapp_shippingDialog;
    public boolean whatsappclicked = false, textmesasageclicked = false;
    //public ArrayList<SingleCustomerHistoryData.DataBean> singledatalist = new ArrayList<>();
    public String bookingdatenew = "New";
    public String bookingdate_send, nextfollowupdate_send, NewDateFormat = null;
    public LinearLayout nodata_found_layout;
    public Button Retry_Button;
    public NetworkCheckerService networkCheckerService;
    public LinearLayoutManager linearLayoutManager;
    public CompositeDisposable disposables = new CompositeDisposable();
    public RecyclerView history_data_recyclerview, recyclerView_template;
    public ConnectionDetector detector;
    public DividerItemDecoration dividerItemDecoration;
    public LinearLayout servicelinear, reminderlinear, complaintcategorysubll, complaintstatusll, ll_update;
    public EditText indexs, indexs2;
    public SwitchDateTimeDialogFragment dateTimeFragment;
    public Intent intentFilter;
    public APIInterface apiInterface;
    public int index = 0;
    Button submit_btn;
    Spinner spinner_campaignreminders, spinner_campain_list, spinner_calledStatus;
    FrameLayout fl_clickable_view;
    public ArrayList<String>[] campaign_arraylist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] callprogressstatus = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String> campaign_arraylist_ID = new ArrayList();
    public ArrayList<String>[] inputfiled_arraylist = new ArrayList[]{new ArrayList<String>()};
    EditText editText_bookingdate, editText_nextfollowpdate, editText_remark;
    public ArrayList<SingleCustomerHistoryData.DataDTO> singledatalist = new ArrayList<>();
    String spinner_selected_item;
    Integer spinner_name_position;
    CardView cardView_bookingdate, cardView_nextfollowupdate;
    LinearLayout card_remark, card_callstatus, card_status;
    int check = 0;
    int Current_Index_Value;
    boolean PressStop_Button,isLongPress = false;
    public boolean morebtnclicked = false,start_call_button = false;
    int Call_Current_Status_Live = 1;

    @SuppressLint("NewApi")
    public static void start(Context context, Call call) {
        context.startActivity(new Intent(context, Drawer_Activity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle()));
    }

    /*----------------------------------------------------------------------------------------------*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_);
        customWaitingDialog = new CustomDialog(Drawer_Activity.this);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        token =CommonVariables.customtokenbearer;
        networkCheckerService = new NetworkCheckerService(this, this);
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        setToolBar();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void setToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Custom ");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        customWaitingDialog = new CustomDialog(Drawer_Activity.this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        apiController = new ApiController(this);
        apiControllerNew=new com.hero.weconnect.mysmartcrm.Retrofit.ApiController(this,this);

        detector = new ConnectionDetector(Drawer_Activity.this);
        apiController.getToken(""+CommonVariables.userName,""+CommonVariables.userName,"password");

        String folder_main = "Smart CRM";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }


        recyclerView = findViewById(R.id.service_reminder_recyclerview);
        startcall = findViewById(R.id.startcall);
        pausecall = findViewById(R.id.pasusecall);
        endcall = findViewById(R.id.endcall);
        indexs = findViewById(R.id.indexs);
        indexs2 = findViewById(R.id.indexs2);
        nodata_found_layout = findViewById(R.id.nodatfound);
        Retry_Button = findViewById(R.id.refrshbutton);
        spinner_campaignreminders = (Spinner) findViewById(R.id.spinner_reminders);
        spinner_campain_list = (Spinner) findViewById(R.id.spinner_campaingn);
        fl_clickable_view = findViewById(R.id.fl_clickable_view);
        fl_clickable_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing here
            }
        });

        if (common_calling_color_sharedPreferances.getPosition() == null || common_calling_color_sharedPreferances.getPosition().equals("")) {
            position = 0;
        } else {
            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
        }

        if (position != Current_Index_Value) {
            position = Current_Index_Value;
        }
        adapter = new Adapter_Custom(this, Drawer_Activity.this, serviceReminderDataList);
        adapter.setHasStableIds(true);
        linearLayoutManager = new LinearLayoutManager(Drawer_Activity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.hasFixedSize();

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.setAdapter(adapter);
        adapter.setCustomButtonListner(Drawer_Activity.this);


        //  Toast.makeText(this, "jayant llll", Toast.LENGTH_SHORT).show();
        Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
        disposables.add(disposable);
        Disposable disposable2 = OngoingCall.state
                .filter(state -> state == Call.STATE_DISCONNECTED)
                .delay(2, TimeUnit.SECONDS)
                .firstElement()
                .subscribe(this::finish);
        disposables.add(disposable2);

        commonSharedPref = new CommonSharedPrefCustom(this);


//        if (commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getData() == null) {
//            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        token = "Basic " + commonSharedPref.getLoginData().getData();







//        apiController.getNotComingReason(token);
//        apiController.getContactStatus(token);
//        apiController.getCustomerReply(token);



        isLastVisible();





        /*  -----------------------------------Calling Button Click Listeners----------------------------------------------------*/
        indexs.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {

                    int postionoflist = adapter.getItemCount() - 1;


                    int asg = Integer.parseInt(indexs.getText().toString()) - 1;
                    index = asg;


                    if (asg < postionoflist) {


                    } else {
                        Toasty.warning(Drawer_Activity.this, " Index Not Avialabe in list Load More Data", Toast.LENGTH_LONG).show();
                    }

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
                    recyclerView.scrollToPosition(asg);


                } catch (Exception e) {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        startcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                indexs2.setEnabled(false);
                indexs.setEnabled(false);

                //Toast.makeText(Drawer_Activity.this, "STARTCALL", Toast.LENGTH_SHORT).show();
                TelecomManager tm = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                if (tm == null) {
                    // whether you want to handle this is up to you really
                    throw new NullPointerException("tm == null");
                }

                if (ContextCompat.checkSelfPermission(Drawer_Activity.this, Manifest.permission.MODIFY_PHONE_STATE) ==
                        PackageManager.PERMISSION_GRANTED) {
                    if (tm.isInCall()) {
                        tm.endCall();
                    }
                }
                if (position >= serviceReminderDataList.size()) {
                    Toasty.warning(Drawer_Activity.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                    return;

                }
                indexs.setEnabled(false);
                spinner_campaignreminders.setEnabled(false);
                spinner_campain_list.setEnabled(false);
                try {
                    TelecomManager systemService = getSystemService(TelecomManager.class);
                    if (systemService != null && !systemService.getDefaultDialerPackage().equals(getPackageName())) {
                        startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.hero.weconnect"));
                        Toasty.warning(Drawer_Activity.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
                    } else {
                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

                            Toasty.warning(Drawer_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
                        }
                        else {

                            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {


                                position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            }
                            else {

                                position = Integer.parseInt(indexs.getText().toString()) - 1;
                            }
                            common_calling_color_sharedPreferances.setCall_start_stop_status("start");
                            looping();

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }

            }
        });
        endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexs2.setEnabled(true);
                indexs.setEnabled(true);
                PressStop_Button = true;

                if (CommonVariables.Is_LongPress) {

                    pausecall.performClick();
                    CommonVariables.Is_LongPress = false;
                    return;
                }

                if (indexs != null) indexs.setVisibility(View.VISIBLE);
                if (indexs2 != null) indexs2.setVisibility(View.GONE);


                indexs.setEnabled(true);
                spinner_campaignreminders.setEnabled(true);
                spinner_campain_list.setEnabled(true);
                CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long l) {

                        if (calldone) {

                            if (call_current_position < arrayList_matserId.size()) {
                                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editss = chkfss.edit();
                                editss.putString("call", "CALL DONE");
                                editss.commit();
                            }
                        }


                    }

                    @Override
                    public void onFinish() {


                    }
                };
                countDownTimer.start();
                recyclerView.scrollToPosition(call_current_position);

                if (isLongPress) {
                    if (indexs != null) indexs.setText("1");
                    if (indexs2 != null) indexs2.setText("1");
                    isLongPress = false;
                }

                try {
                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");


                    OngoingCall.hangup();
                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }


                    if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {

                        OngoingCall.hangup();
                        CommonVariables.endcallclick = false;
                        CommonVariables.receivecall = false;
                        CommonVariables.incoming = false;
                        callstuck = true;
                       /* Intent intent= new Intent(S_R_Activity.this,DashboardActivity.class);
                        finish();
                        startActivity(intent);*/
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
                try {
                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        CallService.discon1();
                    }
                } catch (Exception e) {
                    e.printStackTrace();


                }




            }
        });
        pausecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (position >= serviceReminderDataList.size()) {
                    Toasty.warning(Drawer_Activity.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                    return;

                }

                if (indexs != null) indexs.setVisibility(View.GONE);
                if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
                if (indexs2 != null) indexs2.setText("" + position);
                Current_Index_Value = position;

                CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long l) {

                        if (calldone) {

                            if (call_current_position < arrayList_matserId.size()) {
                                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(position - 1), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editss = chkfss.edit();
                                editss.putString("call", "CALL DONE");
                                editss.commit();
                            }
                        }


                    }

                    @Override
                    public void onFinish() {


                    }
                };
                countDownTimer.start();
                recyclerView.scrollToPosition(call_current_position);
                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }
                try {

                    OngoingCall.hangup();


                } catch (Exception e) {

                }

                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {
                        clearCallUISharePreferances();
                        scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                        try {

                            int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));


                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        OngoingCall.call.disconnect();
                        if (OngoingCall.call4.size() > 0) OngoingCall.call4.get(0).disconnect();

                        looping();

   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
    startActivity(i);
    finish();*/


                    }


                } catch (Exception e) {

                }


            }
        });
        Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(Drawer_Activity.this, Drawer_Activity.class);
                startActivity(refresh);
                finish();
            }
        });

        spinner_campaignreminders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //customWaitingDialog.show();



                campaign_arraylist[0].clear();
                campaign_arraylist_ID.clear();
                spinner_campain_list.setAdapter(new ArrayAdapter<String>(Drawer_Activity.this, android.R.layout.simple_dropdown_item_1line, campaign_arraylist[0]));
                //  CampaignArrayAdapter.notifyDataSetChanged();

               /*
                */

                getcampaignlist(CommonVariables.customtokenbearer);
                common_calling_color_sharedPreferances.setPosition(""+0);





            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_campain_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (++check>0)
                {
                    spinner_selected_item = spinner_campain_list.getSelectedItem().toString();
                    spinner_name_position = position;

                    if (CallArrayAdapter != null) {
                        CallArrayAdapter.clear();
                    }
                    datevalue=arrayList_datevalue.get(position);
                    datevalueend=arrayList_datevalueend.get(position);
                    Log.d("arrayList_datevalueend",datevalue+"\n "+datevalueend);

                    common_calling_color_sharedPreferances.setPosition(""+0);
                    apiController.deAllocation(CommonVariables.customtokenbearer);
                    getcustomcallingdata(token, "0");
                }else
                {
                    datevalue=arrayList_datevalue.get(0);
                    datevalueend=arrayList_datevalueend.get(0);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*---------------------------------------------------------------------------------------------------*/


    }

    /*--------------------------------------------------------Calling Logic----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void looping() {
        Callingstatus = "BUSY";
        if (common_calling_color_sharedPreferances != null && common_calling_color_sharedPreferances.getCall_start_stop_status() != null && common_calling_color_sharedPreferances.getCall_start_stop_status().equals("start")) {
            try {
                scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                call_current_position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                final String icca = chkfss.getString("call", "");
                string_called_status = arrayList_callstatus.get(call_current_position);

                if (string_called_status.equals("CALL DONE")) {
                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else if (icca.equals("CALL DONE")) {
                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else {
                    if (detector.isInternetAvailable()) {
                        common_calling_color_sharedPreferances.setcurrentcallmobileno(arrayList_mobileno.get(call_current_position));
                        common_calling_color_sharedPreferances.setcallcurrentstatus("s");
                        common_calling_color_sharedPreferances.setmasterid(arrayList_matserId.get(call_current_position));
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position));
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        if (checkSelfPermission(CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            if (sharedPreferencescallUI.contains("CALLUI")) {
                                clearCallUISharePreferances();
                            }
                             Uri uri = Uri.parse("tel:" + ""+CommonVariables.SR_Testingnumber);
                            //Uri uri = Uri.parse("tel:" + "+91" +arrayList_mobileno.get(position));
                           // Log.e("TAG", "changed mobile no " + arrayList_mobileno.get(position) + " to 4951");
                        //   Uri uri = Uri.parse("tel:" +"+91"+ CommonVariables.SR_Testingnumber);
                         //  Uri uri = Uri.parse("tel:" +"+91"+ CommonVariables.SR_Testingnumber);

                            CommonVariables.mobilenumber=arrayList_mobileno.get(position);

                            startActivity(new Intent(Intent.ACTION_CALL, uri));
                            position = position + 1;
                            String uid = UUID.randomUUID().toString();

                            try {
                                string_masterId = arrayList_matserId.get(call_current_position);
                                string_mobileno = arrayList_mobileno.get(call_current_position);
                                string_customername = arrayList_customername.get(call_current_position);
                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                            SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editsas = prefsss.edit();
                            editsas.putString(string_masterId, uid);
                            editsas.commit();

                        } else {
                            // Request permission to call
                            ActivityCompat.requestPermissions(Drawer_Activity.this, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                        }
                        String uid = UUID.randomUUID().toString();

                        try {
                            string_masterId = arrayList_matserId.get(call_current_position);
                            string_mobileno = arrayList_mobileno.get(call_current_position);
                            string_customername = arrayList_customername.get(call_current_position);
                        } catch (IndexOutOfBoundsException e) {

                        }

                        SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editsas = prefsss.edit();
                        editsas.putString(string_masterId, uid);
                        editsas.commit();
                    } else {
                        Toasty.error(this, "Please connect Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } else {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    public void updateUi(Integer state) {
        ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);
        if (CommonVariables.incoming) {
            //  adapter.notifyDataSetChanged();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);
            CommonVariables.incoming = false;
            return;
        }


        // Set callInfo text by the state
        // a.setText(CallStateString.asString(state).toLowerCase() + "\n" + number);
        this.state = state;
        if (state == Call.STATE_DIALING) {
            //changeSpinnersAbility(false);
            stop = false;
            final Handler handler = new Handler();
            currentdail = Calendar.getInstance().getTime();
            final int delay = 4000;
            calldone = false;
            if (indexs != null) indexs.setVisibility(View.GONE);
            if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
            if (indexs2 != null) indexs2.setText("" + position);
            Current_Index_Value = position;

            if (!stop) {
                if (sharedPreferencescallUI.contains("CALLUI")) {
                } else {
                    OngoingCall.call2.add(OngoingCall.call);
                    try {
                         recordService = new Intent(this, CallRecorderService.class);
                        startService(recordService);

                        //startRecording(historyID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                        int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                        try {
                            string_masterId = arrayList_matserId.get(ass);
                            string_mobileno = arrayList_mobileno.get(ass);
                            string_customername = arrayList_customername.get(ass);
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                    }
                    if (contactstatus != null && position == dataposition) {
                               /* contactstatus.setSelection(1);
                                contactstatus.setEnabled(true);*/
                        if (contactstatus != null) contactstatus.setSelection(1);
                        if (contactstatus != null) contactstatus.setEnabled(false);
                        // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);
                    }
                    Callingstatus = "BUSY";
                    SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    historyID = chkfss2.getString(string_masterId, "");

                    AddHistoryId(token, string_masterId, historyID, Callingstatus);
                    updatecontatctstatus(token, historyID, "BUSY", string_masterId);

                }
            }
        } else if (state == Call.STATE_ACTIVE) {
            calldone = true;
            if (indexs != null) indexs.setVisibility(View.GONE);
            if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
            if (indexs2 != null) indexs2.setText("" + position);
            Current_Index_Value = position;
            if (contactstatus != null && position == dataposition) {
                contactstatus.setSelection(0);
                contactstatus.setEnabled(false);
                Call_Current_Status_Live = 0;
                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

            }
            updatecontatctstatus(token, historyID, "CALL DONE", string_masterId);

            // Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
            if (sharedPreferencescallUI.contains("CALLUI")) {

            } else {

                Callingstatus = "CALL DONE";
                SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                SharedPreferences.Editor editsfs = prefss.edit();
                editsfs.putString("call", Callingstatus);
                editsfs.commit();

                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");
                //  updatecalledstatus(token, historyID, "" + Callingstatus, getString(R.string.SRTAG));

            }

        }
        else if (state == Call.STATE_DISCONNECTED && !CommonVariables.incoming) {
            //changeSpinnersAbility(true);
            Call_Current_Status_Live = 1;
            if (indexs != null) indexs.setVisibility(View.VISIBLE);
            if (indexs != null) indexs.setEnabled(true);
            if (indexs2 != null) indexs2.setVisibility(View.GONE);
            ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());
            ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());
            //     Toast.makeText(this, "disconnected1111 "+Calendar.getInstance().getTimeInMillis(), Toast.LENGTH_SHORT).show();
            if (CommonVariables.receivecall) adapter.notifyDataSetChanged();
            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                try {
                    string_masterId = arrayList_matserId.get(ass);
                    string_mobileno = arrayList_mobileno.get(ass);
                    string_customername = arrayList_customername.get(ass);

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");


            try {
                sleep(2000);
                updatecallendtime(CommonVariables.customtokenbearer, historyID, string_masterId);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            stopService(recordService);

            stopRecording(historyID);

            discom = Calendar.getInstance().getTime();
            try {
                long diffInMs = discom.getTime() - currentdail.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                if (diffInSec > 3) {
                    stop = false;
                } else {
                    stop = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (sharedPreferencescallUI.contains("CALLUI")) {

            } else {
                OngoingCall.call2.clear();


                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {

                    common_calling_color_sharedPreferances.clearposition();

                }


                common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                try {
                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                        /*asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition())));
                    } else {
                        /* asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition())+1 ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) + 1));
                    }
                    /* common_calling_color_sharedPreferances.setPosition(String.valueOf(asgkkk))*/

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (PressStop_Button) {

                    PressStop_Button = false;

                } else {
                    SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editsfs = prefss.edit();
                    editsfs.putString("call", Callingstatus);
                    editsfs.commit();


                }

                looping();


            }


            adapter.notifyDataSetChanged();


        }
        else if (state == Call.STATE_DISCONNECTING) {
            Call_Current_Status_Live = 1;
            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");

            adapter.notifyDataSetChanged();
            updatecallendtime(CommonVariables.customtokenbearer, historyID, string_masterId);

        } else if (!(state == Call.STATE_ACTIVE)) {
            if (Callingstatus.equals("CALL DONE")) {

            } else {
                Callingstatus = "BUSY";

            }
        } else if (!(state == Call.STATE_RINGING)) {
        }

    }

    // this will make spinner non clickable while user is on call
    private void changeSpinnersAbility(boolean isSpinnersActive) {
        for (int i = 0; i < spinner_campaignreminders.getChildCount(); i++) {
            View child = spinner_campaignreminders.getChildAt(i);
            child.setClickable(isSpinnersActive);
        }
        for (int i = 0; i < spinner_campain_list.getChildCount(); i++) {
            View child = spinner_campain_list.getChildAt(i);
            child.setClickable(isSpinnersActive);
        }
        fl_clickable_view.setVisibility(isSpinnersActive ? View.GONE : View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void offerReplacingDefaultDialer() {


       /* TelecomManager systemService = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            systemService = this.getSystemService(TelecomManager.class);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
            }
        }*/
        if (Build.VERSION.SDK_INT <= 28) {
            TelecomManager systemService = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                systemService = this.getSystemService(TelecomManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
                }
            }
        } else {
            @SuppressLint("WrongConstant")
            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
            startActivityForResult(intent, 101);


        }
    }
    /*------------------------------------------------------------------------------------------------------------------------*/


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void finish(Integer state) {

        ////Log.e("newStatekkkk",""+state);


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStop() {
        super.onStop();
        customWaitingDialog.dismiss();

        disposables.clear();


    }

    /*-------------------------------------------------Activity Deafult Function-----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {

        super.onStart();
       offerReplacingDefaultDialer();
//        campaign_arraylist[0].clear();
//        campaign_arraylist_ID.clear();


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {
        customWaitingDialog.dismiss();

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Drawer_Activity.whatsupclick == false && state == Call.STATE_ACTIVE && !CommonVariables.incoming
                && Drawer_Activity.textmessageclick == false) {


            /*OngoingCall.hangup();

            startActivity(new Intent(S_R_Activity.this,DashboardActivity.class));
            // pausestate=false;
            finish();*/

            // OngoingCall.hangup();
            //  adapter.notifyDataSetChanged();


        }


        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        // Toast.makeText(this, serviceReminderDataList.size()+"  onResume "+adapter.getItemCount(), Toast.LENGTH_SHORT).show();

        if (Drawer_Activity.whatsupclick || Drawer_Activity.textmessageclick) {

            Drawer_Activity.whatsupclick = false;
            Drawer_Activity.textmessageclick = false;
            new OngoingCall();
            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            disposables.add(disposable);
            //adapter.notifyDataSetChanged();
        } else {
//            new OngoingCall();
//            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
//            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
//            disposables.add(disposable);
        }
    }
    /*------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------Other Function -----------------------------------------------------------------*/

    @Override
    public void onBackPressed() {
        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null
                && !common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
            final Dialog shippingDialog = new Dialog(Drawer_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit ?");
            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {


                    if (state == Call.STATE_ACTIVE) {

                        OngoingCall.hangup();

                    }


                    //  adapter.notifyDataSetChanged();
                    String packagename = commonSharedPref.getPackagesList();
                    if (Build.VERSION.SDK_INT <= 28) {
                        //  Toast.makeText(S_R_Activity.this, "back ", Toast.LENGTH_SHORT).show();

                        TelecomManager systemService = Drawer_Activity.this.getSystemService(TelecomManager.class);
                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                        }
                        shippingDialog.dismiss();

                    }
                    else {
                        //Toast.makeText(S_R_Activity.this, "back 2", Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                        startActivity(i);
                        shippingDialog.dismiss();

                    }

                    try {

                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        recyclerView.setAdapter(adapter);


//                    else {
////                  /*      TelecomManager systemService = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
////
////                      //  Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();
////                       Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER);
////                       intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
////                               packagename);
////                                        startActivity(intent);*/
////
////                  /*      @SuppressLint("WrongConstant")
////                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
////                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
////                        startActivityForResult(intent, 101);*/
////
////
////
////                /*        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
////                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.android.contacts"));
////                        }
////*/
////
////
////                   /*     Intent showSettings = new Intent();
////                        showSettings.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
////                        Uri uriAppSettings = Uri.fromParts("package", "com.android.phone", null);
////                        showSettings.setData(uriAppSettings);
////                        startActivity(showSettings);*/
//                       /* TelecomManager systemService = S_R_Activity.this.getSystemService(TelecomManager.class);
//                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
//                        }*/
//
//                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
//                         startActivity(i);
//                        shippingDialog.dismiss();
//
//
//                 /*       try{
//                            Runtime.getRuntime().exec("adb shell pm reset-permissions");
//                            finish();
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//*/
//                      //  clearApplicationData(S_R_Activity.this);
//
//
////
////                        @SuppressLint("WrongConstant")
////                          String ACTION_REQUEST_ROLE = "android.app.role.action.REQUEST_ROLE";
////                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
////                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
////                        startActivityForResult(intent, 10);
//                        shippingDialog.dismiss();
//                    }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });
            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();
        }
        else if (common_calling_color_sharedPreferances.getcallcurrentstatus() == null) {
            final Dialog shippingDialog = new Dialog(Drawer_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit ?");
            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    if (state == Call.STATE_ACTIVE) {
                        OngoingCall.hangup();
                    }
                    try {
                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        recyclerView.setAdapter(adapter);

                        //  adapter.notifyDataSetChanged();
                        String packagename = commonSharedPref.getPackagesList();
                        if (Build.VERSION.SDK_INT <= 28) {
                            //Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();

                            TelecomManager systemService = Drawer_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            shippingDialog.dismiss();

                        } else if (Build.MODEL.equals("Redmi 7A")) {
                            Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                            startActivity(i);

                        } else {
//                  /*      TelecomManager systemService = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
//
//                      //  Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();
//                       Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER);
//                       intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
//                               packagename);
//                                        startActivity(intent);*/
//
//                  /*      @SuppressLint("WrongConstant")
//                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
//                        startActivityForResult(intent, 101);*/
//
//
//
//                /*        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.android.contacts"));
//                        }
//*/
//
//
//                   /*     Intent showSettings = new Intent();
//                        showSettings.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        Uri uriAppSettings = Uri.fromParts("package", "com.android.phone", null);
//                        showSettings.setData(uriAppSettings);
//                        startActivity(showSettings);*/
//                            TelecomManager systemService = S_R_Activity.this.getSystemService(TelecomManager.class);
//                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
//                            }
                            shippingDialog.dismiss();
                            Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                            startActivity(i);

                 /*       try{
                            Runtime.getRuntime().exec("adb shell pm reset-permissions");
                            finish();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
*/
                            //  clearApplicationData(S_R_Activity.this);


//
//                        @SuppressLint("WrongConstant")
//                          String ACTION_REQUEST_ROLE = "android.app.role.action.REQUEST_ROLE";
//                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
//                        startActivityForResult(intent, 10);
                            shippingDialog.dismiss();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });
            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();
        } else {
            Toasty.warning(Drawer_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onDestroy() {

        if (commonSharedPref.getLoginData() != null) {


           apiController.deAllocation(CommonVariables.customtokenbearer);
            super.onDestroy();
            //  networkCheckerService.UnregisterChecker();
            disposables.dispose();

            OngoingCall.hangup();


        }
        try {
            Runtime.getRuntime().gc();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void scrollmovepostion(Integer ab) {


        recyclerView.scrollToPosition(ab);

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    boolean isLastVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
        lastposition = layoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = adapter.getItemCount();
        int sizes = serviceReminderDataList.size();
        return (lastposition >= numItems - 1);
    }

    /*------------------------------------------------------------------------------------------------------------------*/

    // Clear Call Shared Preferances
    public void clearCallUISharePreferances() {
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        SharedPreferences.Editor editss = sharedPreferencescallUI.edit();
        editss.clear();
        editss.commit();
        editss.apply();

    }

    /*---------------------------------------------------API Call Methods------------------------------------------------------------*/
    // Get Custom Calling Data
    public void getcustomcallingdata(String token, String offset) {
        customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.Campaign_Name, spinner_selected_item);
        params.put(ApiConstant.Campaign_Id, campaign_arraylist_ID.get(spinner_name_position));
        params.put(ApiConstant.Offset, offset);
      //  Log.e("getcustomcallingdata  ", new Gson().toJson(params));
        apiController.getCustomCallingData(CommonVariables.customtokenbearer, params);
    }

    // Get Campaign List Data
    public void getCallProgressstatus(String token, String campaingname) {
       // customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.Campaign_Name, campaingname);
        params.put("Response_Type", "callprogressstatus");

        //Log.e("getCallProgressstatus ", new Gson().toJson(params));

        apiController.getCallProgressStatus(CommonVariables.customtokenbearer, params);
    }

    // Get Campaign List Data
    public void getcampaignlist(String token) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.filter, "-250");
        apiController.getCampaignData(CommonVariables.customtokenbearer, params);

        //Log.e("getcampaignlist ", new Gson().toJson(params));

    }

    // Get Input Field List Data
    public void getInputFieldlist(String token) {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.Campaign_Name, spinner_campain_list.getSelectedItem().toString());
        params.put(ApiConstant.Response_Type, "InputField");

       // Log.e("getInputFieldlist ", new Gson().toJson(params));

        apiController.getInputFieldData(CommonVariables.customtokenbearer, params);
    }


    // Get Input Data Field List Data
    public void getInputDataFieldlist() {
        //customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.Campaign_Name, spinner_campain_list.getSelectedItem().toString());
        params.put(ApiConstant.Response_Type, "DataField");

        Log.e("getInputFieldlist ", new Gson().toJson(params));

        apiController.getInputDataFieldData(CommonVariables.customtokenbearer, params);
    }

    // Add History Id Function
    public void AddHistoryId(String token, String mastersrid, String historyId, String callingstatus) {
        // customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BoundType, getString(R.string.OUTBOUND));
        params.put("MobileNo", serviceReminderDataList.get(call_current_position).getData2());
        params.put("VIN", serviceReminderDataList.get(call_current_position).getData3());
        params.put("CustomerName", serviceReminderDataList.get(call_current_position).getData1());
        params.put("CampaignName", spinner_campain_list.getSelectedItem().toString());
        params.put("CampaignId", campaign_arraylist_ID.get(spinner_campain_list.getSelectedItemPosition()));

      //  Log.e("history id ", "History");
        apiController.AddHistory(CommonVariables.customtokenbearer, params);
        SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor editsfs = prefss.edit();
        editsfs.putString("call", callingstatus);
        editsfs.commit();

    }

    // Update Booking Date & Time
    public void updatebookingdate(String token, String historyId, String bookingdate, String string_masterId) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, bookingdate);
        params.put(ApiConstant.Field_Type, "bookingdate");
        apiController.UpdateBookingDate(CommonVariables.customtokenbearer, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "booking");
        edits.commit();
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String string_masterId) {
        // customWaitingDialog.show();

        params = new HashMap<>();

        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, "2020-10-12");
        params.put(ApiConstant.Field_Type, "callendtime");

        apiController.UpdateCallEndTime(CommonVariables.customtokenbearer, params);
    }


    // Update Remark
    public void updateremark(String token, String historyId, String remark, String string_masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, remark);
        params.put(ApiConstant.Field_Type, "remark");
        apiController.UpdateRemark(CommonVariables.customtokenbearer, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status1", "rmkcolor");
        edits.commit();

    }


    // Update Follow-Up Date
    public void updatefollowupdate(String token, String historyId, String followdatetime, String string_masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, followdatetime);
        params.put(ApiConstant.Field_Type, "followupdate");
        apiController.UpdateFollowupDate(CommonVariables.customtokenbearer, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "NextFollowUp");
        edits.commit();
    }

    // Update Contatct Status
    public void updatecontatctstatus(String token, String historyId, String contactstatys, String string_masterId) {
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, contactstatys);
        params.put(ApiConstant.Field_Type, "callingstatus");

        apiController.UpdateCallStatus(CommonVariables.customtokenbearer, params);
    }


    // Update Contatct Status
    public void updatestatus(String token, String historyId, String status, String string_masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.MasterId, string_masterId);
        params.put(ApiConstant.data, status);
        params.put(ApiConstant.Field_Type, "status");

        apiController.UpdateStatus(CommonVariables.customtokenbearer, params);
    }

    //Get Single Customer Service Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String masterId) {

        customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, masterId);
        Log.e("getSingleSRData ", new Gson().toJson(params));

        apiController.getSingleCustomerHistoryData(CommonVariables.customtokenbearer, params);
    }

    // Update get Service Reminder History Data
    public void getSRHistoryData(String token, String Id) {

        customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.MasterId, Id);
        apiController.getCustomHistory(CommonVariables.customtokenbearer, params);

    }



    @Override
    public void onSuccess(String beanTag, com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean superClassCastBean) {
        if (customWaitingDialog.isShowing()) {
            customWaitingDialog.dismiss();
        }
        if (beanTag.matches(ApiConstant.CUSTOMCALLING)) {


            if (serviceReminderDataList.size() == 0) offset = "0";
            if (morebtnclicked) {
                morebtnclicked = false;
            } else {
                serviceReminderDataList.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();

            }
            Custom_Model serviceReminderDataModel = (Custom_Model) superClassCastBean;
            //Log.e("Data", "Data" + serviceReminderDataModel.getData());

            getInputDataFieldlist();

            if (serviceReminderDataModel.getData().contains("[]") || serviceReminderDataModel.getData().size()==0) {


               if(offset.equals("0") && serviceReminderDataModel.getData().size()==0)
               {
                   nodata_found_layout.setVisibility(View.VISIBLE);
                   customWaitingDialog.dismiss();
                   serviceReminderDataModel.getData().size();
                   adapter.notifyDataSetChanged();

               }


            } else {
                if (serviceReminderDataModel.getData().size() >0) {
//                     serviceReminderDataList.clear();

                    for (Custom_Model.DataBean dataBean : serviceReminderDataModel.getData()) {

                        offset = "" + dataBean.getSerialNo();
                        serviceReminderDataList.add(dataBean);
                        calling_list_serial_no = dataBean.getSerialNo();
                        // TODO: 10-12-2020 change value
                        arrayList_mobileno.add(dataBean.getData2());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getData1());
                        arrayList_callstatus.add(dataBean.getCalledstatus());
                        nodata_found_layout.setVisibility(View.GONE);


                    }

                    Log.e("SRdatalistsize ","  "+serviceReminderDataList.size());

                    recyclerView.setAdapter(adapter);
                    customWaitingDialog.dismiss();
                    adapter.notifyDataSetChanged();


                } else {
                    nodata_found_layout.setVisibility(View.VISIBLE);
                    customWaitingDialog.dismiss();

                }

            }


       }
//        else if (beanTag.matches(com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant.UPLOADRECORDING)) {
//            UploadRecordingModel commonModel = (UploadRecordingModel) superClassCastBean;
//            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
//
//                CommonVariables.audiofile.delete();
//                CommonVariables.audiofile=null;
//                customWaitingDialog.dismiss();
//            }
//
//        }
        else if (beanTag.matches(ApiConstant.CAMPAIGNLIST)) {
            CampaignModel campaignModel = (CampaignModel) superClassCastBean;
            if (campaignModel.getData() != null && campaignModel.getData().size() > 0) {
                campaign_arraylist[0].clear();
                campaign_arraylist_ID.clear();
                arrayList_datevalue.clear();
                arrayList_datevalueend.clear();
                for (CampaignModel.DataDTO dataBean : campaignModel.getData()) {
                    campaign_arraylist[0].add(dataBean.getCampaignName());
                    campaign_arraylist_ID.add(dataBean.getId());
                    arrayList_datevalue.add(dataBean.getBookingStartDate());
                    arrayList_datevalueend.add(dataBean.getBookingEndDate());
                }
                CampaignArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, campaign_arraylist[0]);
                spinner_campain_list.setAdapter(CampaignArrayAdapter);


            }
            customWaitingDialog.dismiss();
        }
        else if (beanTag.matches(ApiConstant.CALLPROGRESS)) {
            CallProgressModel campaignModel = (CallProgressModel) superClassCastBean;

            callprogressstatus[0].clear();

            if (campaignModel.getData() != null && campaignModel.getData().size() > 0) {
                callprogressstatus[0].add("  ");

                card_status.setVisibility(View.VISIBLE);
                for (CallProgressModel.DataDTO dataBean : campaignModel.getData()) {
                    callprogressstatus[0].add(dataBean.getValue());
                }
                CallArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, callprogressstatus[0]);
                status.setAdapter(CallArrayAdapter);

            }else
            {
                card_status.setVisibility(View.GONE);

            }
        }


        else if (beanTag.matches(ApiConstant.INPUTFIELDLIST))
        {
            InputFieldModel inputFieldModel = (InputFieldModel) superClassCastBean;
            if (inputFieldModel.getData() != null) {

                if (inputFieldModel.getData().getBookingDate().equals("NO")) {
                    cardView_bookingdate.setVisibility(View.GONE);
                } else if (inputFieldModel.getData().getNextFollowUpDate().equals("NO")) {
                    cardView_nextfollowupdate.setVisibility(View.GONE);
                } else if (inputFieldModel.getData().getRemark().equals("NO")) {
                    card_remark.setVisibility(View.GONE);
                } else if (inputFieldModel.getData().getCallProgressStatus().equals("NO")) {
                    card_status.setVisibility(View.GONE);
                }
            }
        }

        else if (beanTag.matches(ApiConstant.INPUTDATAFIELDLIST))
        {
            dataFieldModelArrayList.clear();
            DataFieldModel inputFieldModel = (DataFieldModel) superClassCastBean;
            if (inputFieldModel.getData() != null) {

                for(DataFieldModel.DataBean dataBean:inputFieldModel.getData())
                {
                    dataFieldModelArrayList.add(dataBean);

                }

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATEBOOKINGDATETIME)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {
            } else {
            }

        }
        else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {


            } else {

            }

        }
        else if (beanTag.matches(ApiConstant.UPDATEREMARK)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {


            } else {

            }

        }
        else if (beanTag.matches(ApiConstant.UPDATECALLEDSTATUS)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {


            } else {

            }

        }
        else if (beanTag.matches(ApiConstant.UPDATECALLENDTIME)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {


            } else {

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATESTATUS)) {
            FieldUpdateModel commonModel = (FieldUpdateModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {


            } else {

            }
        }
        else if (beanTag.matches(ApiConstant.SINGLEDATA)) {

            singledatalist.clear();
            SingleCustomerHistoryData singleCustomerHistoryData = (SingleCustomerHistoryData) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {

                singledatalist.addAll(singleCustomerHistoryData.getData());
                oldhid = singleCustomerHistoryData.getData().get(0).getHistoryId();
                calldone=true;

                String ContatctStatus = singleCustomerHistoryData.getData().get(0).getCallingStatus();



//                if(!singleCustomerHistoryData.getData().get(0).getDateValue().isEmpty())
//                {
//
//                    Log.d("arrayList_datevalueend",singleCustomerHistoryData.getData().get(0).getDateValue()+"\n "+singleCustomerHistoryData.getData().get(0).getDateValueend());
//
//                    // datevalue=singleCustomerHistoryData.getData().get(0).getDateValue();
//                   // datevalueend=singleCustomerHistoryData.getData().get(0).getDateValueend();
//                }

                if ((singledatalist.get(0).getBookingDate().length() == 0)
                        && (singledatalist.get(0).getNextFollowupdate().length() == 0)
                        && (singledatalist.get(0).getStatus().length() == 0)
                        && (singledatalist.get(0).getRemark().length() == 0)) {


                } else {
                    submit_btn.setText("Resubmit");
                }
                if (ContatctStatus != null && ContatctStatus.equals("CALL DONE")) {
                    contactstatus.setSelection(0);
                    contactstatus.setEnabled(false);

                } else if (ContatctStatus != null && ContatctStatus.equals("BUSY")) {
                    contactstatus.setSelection(1);
                    contactstatus.setEnabled(false);

                } else if (ContatctStatus == null || ContatctStatus.length() == 0) {
                    //followuptype_spinner.setEnabled(false);
                    contactstatus.setSelection(1);
                }


                try {
                    editText_remark.setText("" + singleCustomerHistoryData.getData().get(0).getRemark());
                    if (singleCustomerHistoryData.getData().get(0).getNextFollowupdate().contains("1900-01-01T00:00:00")) {

                        editText_nextfollowpdate.setText("");
                    } else {

                        editText_nextfollowpdate.setText(dateparse1(singleCustomerHistoryData.getData().get(0).getNextFollowupdate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "")));
                    }
                    if (singleCustomerHistoryData.getData().get(0).getBookingDate().contains("1900-01-01T00:00:00")) {

                        //edit_Bookkingno.setText("");

                    } else if (singleCustomerHistoryData.getData().get(0).getBookingDate().length() > 0) {


                        editText_bookingdate.setText(dateparse1(singleCustomerHistoryData.getData().get(0).getBookingDate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "")));
                        //  et_bookingdate.setEnabled(false);


                    } else {

                        editText_bookingdate.setText("");
                        //  et_bookingdate.setEnabled(true);

                    }


                    status.setSelection(((ArrayAdapter<String>) status.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getStatus().trim()));


                } catch (Exception e) {

                    e.printStackTrace();

                }


            }
            else
            {
                calldone=false;
            }

            customWaitingDialog.dismiss();

        } else if (beanTag.matches(ApiConstant.GETCALLINGHISTORY)) {
            serviceReminderHistoryDataList.clear();
            CustomHistoryModel sr_historyModel = (CustomHistoryModel) superClassCastBean;


            if (sr_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(this, "No History Created Yet..!!", Toast.LENGTH_SHORT).show();
                adapter_sr_historydata.notifyDataSetChanged();

                customWaitingDialog.dismiss();
            } else {
                if (sr_historyModel.getData() != null)
                {
                    for (CustomHistoryModel.DataDTO dataBean : sr_historyModel.getData()) {
                        serviceReminderHistoryDataList.add(dataBean);

                    }
                    customWaitingDialog.dismiss();
                    adapter_sr_historydata.notifyDataSetChanged();

                }
            }
            customWaitingDialog.dismiss();

        }

        else if(beanTag.matches(ApiConstant.TOKEN_TAG))
        {
            TokenModel tokenModel=(TokenModel)superClassCastBean;


            CommonVariables.customtokenbearer="bearer "+tokenModel.getAccessToken();


            getcampaignlist(CommonVariables.customtokenbearer);

            Toast.makeText(this,"Successfully",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (customWaitingDialog.isShowing()) {
            customWaitingDialog.dismiss();
        }
        if (beanTag.matches(ApiConstant.GETSMSTEMPLATE))
        {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
            }

            customWaitingDialog.dismiss();

            //      srtextviewtemplate.setText(commonModel.getData());
        }
    }

    @Override
    public void onFailure(String msg) {

        customWaitingDialog.dismiss();

        //Toast.makeText(this, "Failure" + msg.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        customWaitingDialog.dismiss();

        //Toast.makeText(this, "Error" + msg.toString(), Toast.LENGTH_SHORT).show();
        if (msg.equals("401")) {
           // alertsessiondialog();
        }
    }


    //load more Service reminder data
    public void moreData() {

        ////Log.e("MoreClick",offset);
          customWaitingDialog.show();
        if (serviceReminderDataList.size() >= 10) getcustomcallingdata(token, offset);
        morebtnclicked = true;
        CommonVariables.morecount = serviceReminderDataList.size();
        morebtncliked = true;
        adapter.updateList(serviceReminderDataList);

        new CountDownTimer(5000, 5000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (CommonVariables.morecount < serviceReminderDataList.size()) {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }

            @Override
            public void onFinish() {
                if (CommonVariables.morecount < serviceReminderDataList.size()) {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }
        }.start();
    }

    /*---------------------------------------------------------------------------------------------------------------*/


    /*---------------------------------------------------Adapter Click Listeners----------------------------------------------------------------*/
    // Customer Details Show Fuction( This Function is Show Input Popup and Get Feedback from Customer in this Popup)
    public void getCustomerDetails(int position, String master_ID, String Name, String MobileNo) {
        final Dialog shippingDialog = new Dialog(Drawer_Activity.this);
        dataposition = position + 1;

        String Master_ID_Updated = master_ID;
        //Toast.makeText(this, ""+Master_ID_Updated, Toast.LENGTH_SHORT).show();


            getSingleCustomerSRHistoryData(token, master_ID);

        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.custom_customer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Customer Details Popup Id Genrate Code
        /*-------------------------------------------------*/
        FloatingActionButton popupclose = (FloatingActionButton) shippingDialog.findViewById(R.id.txtclose);

        iv_booking = shippingDialog.findViewById(R.id.iv_clear_bookingdate);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        iv_show_calling_history = shippingDialog.findViewById(R.id.iv_show_calling_history);// karan sir ne judvaye he
        iv_whatsapp_message = shippingDialog.findViewById(R.id.iv_whatsapp_message);

        EditText edit_name = shippingDialog.findViewById(R.id.data1);
        EditText edit_mobileno = shippingDialog.findViewById(R.id.data2);

        editText_bookingdate = shippingDialog.findViewById(R.id.bboking);
        editText_nextfollowpdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        editText_remark = shippingDialog.findViewById(R.id.remarks);
        contactstatus = shippingDialog.findViewById(R.id.contactstatus);
        contactstatus.setEnabled(false);
        status = shippingDialog.findViewById(R.id.status);
        cardView_bookingdate = shippingDialog.findViewById(R.id.bookingdate);
        cardView_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupdate);
        card_remark = shippingDialog.findViewById(R.id.card_remark);
        card_callstatus = shippingDialog.findViewById(R.id.card_call_status);
        card_status = shippingDialog.findViewById(R.id.card_status);

        contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("BUSY"));

        getInputFieldlist(CommonVariables.customtokenbearer);
        if (campaign_arraylist[0].size() > 0) {

            getCallProgressstatus(token, spinner_selected_item);
        }


        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);


        // getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);
        submit_btn = shippingDialog.findViewById(R.id.submit);



        iv_show_calling_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                shippingDialog.dismiss(); karan sir ne dismiss krne ko mana kiya he
                adapter.getCustomListner().getHistoryDetails(position);
            }
        });
        iv_whatsapp_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                shippingDialog.dismiss(); karan sir ne dismiss krne ko mana kiya he
                adapter.getCustomListner().sendWhatsappMessage(position, MobileNo);
               // adapter.getCustomListner().sendWhatsappMessage(position, ""+MobileNo);
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Drawer_Activity.this.position == dataposition-1) {
                    SharedPreferences chkfss = getSharedPreferences(master_ID, Context.MODE_PRIVATE);
                    historyID = chkfss.getString(master_ID, "");


                    if (editText_bookingdate.getText().length() > 0) {
                        updatebookingdate(CommonVariables.customtokenbearer, historyID, bookingdate_send, string_masterId);
                    }
                    if (editText_nextfollowpdate.getText().length() > 0) {
                        updatefollowupdate(CommonVariables.customtokenbearer, historyID, nextfollowupdate_send, string_masterId);
                    }
                    if (String_status != null) {

                        updatestatus(CommonVariables.customtokenbearer, historyID, String_status, string_masterId);
                    }


                    if (contacted_status != null) {

                        updatecontatctstatus(CommonVariables.customtokenbearer, historyID, contacted_status, string_masterId);

                    }


                    if (editText_remark.getText().length() > 0) {
                        updateremark(CommonVariables.customtokenbearer, historyID, editText_remark.getText().toString(), string_masterId);
                    }



                    if(editText_bookingdate.getText().length()>0)
                    {
                        params = new HashMap<>();
                        params.put("HistoryId", historyID);
                        params.put("Loc", CommonVariables.LOC);
                        params.put("DealerCode", CommonVariables.dealercode);
                        params.put("BookingDate", editText_bookingdate.getText().toString());
                        params.put("CampaignId", campaign_arraylist_ID.get(spinner_campain_list.getSelectedItemPosition()));
                        params.put("VIN", serviceReminderDataList.get(position).getData3());


                      //  Log.e("request11111 ",new Gson().toJson(params));
                        apiController.createServiceRequest(CommonVariables.customtokenbearer, params);
                    }




                    try {
                        sleep(1000);
                        adapter.notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    customWaitingDialog.dismiss();
                    shippingDialog.dismiss();
                    Toast.makeText(Drawer_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();


                }
                else {

                    if (oldhid != null && !oldhid.isEmpty()) {
                        if (editText_bookingdate.getText().length() > 0) {
                            updatebookingdate(CommonVariables.customtokenbearer, oldhid, bookingdate_send, Master_ID_Updated);
                        }
                        if (editText_nextfollowpdate.getText().length() > 0) {
                            updatefollowupdate(CommonVariables.customtokenbearer, oldhid, nextfollowupdate_send, Master_ID_Updated);
                        }
                        if (String_status != null) {
                            updatestatus(CommonVariables.customtokenbearer, oldhid, String_status, Master_ID_Updated);
                        }


                        if (contacted_status != null) {
                            updatecontatctstatus(CommonVariables.customtokenbearer, oldhid, contacted_status, Master_ID_Updated);
                        }


                        if (editText_remark.getText().length() > 0) {
                            updateremark(CommonVariables.customtokenbearer, oldhid, editText_remark.getText().toString(), Master_ID_Updated);
                        }


                        if(editText_bookingdate.getText().length()>0)
                        {
                            params = new HashMap<>();
                            params.put("HistoryId", historyID);
                            params.put("Loc", CommonVariables.LOC);
                            params.put("DealerCode", CommonVariables.dealercode);
                            params.put("BookingDate", editText_bookingdate.getText().toString());
                            params.put("CampaignId", campaign_arraylist_ID.get(spinner_campain_list.getSelectedItemPosition()));

                            //Log.e("request11111 ",new Gson().toJson(params));

                            apiController.createServiceRequest(CommonVariables.customtokenbearer, params);
                        }

                        try {
                            sleep(1000);
                            adapter.notifyDataSetChanged();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        customWaitingDialog.dismiss();
                        shippingDialog.dismiss();
                        Toast.makeText(Drawer_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();

                    }


                }


            }
        });




        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(editText_remark, sugesstion7);
            }
        });


        // Popup to Set Text the Values
        edit_name.setText(Name);
        edit_mobileno.setText(MobileNo);


        if (Call_Current_Status_Live==0)
        {
            contactstatus.setSelection(0);
        }
        else if (Call_Current_Status_Live==1){
            contactstatus.setSelection(1);
        }

        contactstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                contacted_status = contactstatus.getSelectedItem().toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

      /*  notcomingreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, notcomingreasonlist[0]);
        sp_notcomingreason.setAdapter(notcomingreasonArrayAdapter);*/

        if (CallArrayAdapter != null) {
            status.setAdapter(CallArrayAdapter);
            status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        if (position > 0) {
                            String_status = status.getSelectedItem().toString();
                        } else {
                            String_status = null;
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                    String_status = null;
                }
            });
        }


        /*-------------------------------------------------*/


        iv_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (singledatalist.size()>0 && singledatalist.get(0).getBookingDate().length() > 0) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Drawer_Activity.this);
                    builder.setMessage("Do you want cancel booking ?");
                    builder.setNeutralButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            updatebookingdate(token, singledatalist.get(0).getId(), "", master_ID);

                            editText_bookingdate.setText("");

                            editText_bookingdate.setEnabled(true);
                            shippingDialog.dismiss();


                        }
                    });
                    builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });

                    builder.show();


                } else {
                    editText_bookingdate.setText("");


                }


            }
        });
        iv_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText_nextfollowpdate.setText("");
            }
        });
        editText_bookingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!calldone) {
                    Toasty.warning(Drawer_Activity.this, "First start call", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (singledatalist.size() == 0) getSingleCustomerSRHistoryData(token, master_ID);

                if (singledatalist.size() > 0 && !singledatalist.get(0).getBookingDate().replaceAll("1900-01-01T00:00:00", "").equals("")) {
                    Toasty.warning(Drawer_Activity.this, "Booking Already Done Cannot Be Changed..!!", Toast.LENGTH_SHORT).show();
                } else if(editText_nextfollowpdate.getText().length()>0) {

                    Toasty.warning(Drawer_Activity.this, "First clear Next FollowUp Date", Toast.LENGTH_SHORT).show();

                }else
                    {


                    if (singledatalist.size() > 0) {

                        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                        if (dateTimeFragment == null) {
                            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                    getString(R.string.label_datetime_dialog),
                                    getString(android.R.string.ok),
                                    getString(android.R.string.cancel),
                                    getString(R.string.clean) // Optional
                            );
                        }

                        // Optionally define a timezone
                        dateTimeFragment.setTimeZone(TimeZone.getDefault());

                        // Init format


                        final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", Locale.getDefault());
                        final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());


                        // Assign unmodifiable values
                        dateTimeFragment.set24HoursMode(true);
                        dateTimeFragment.setHighlightAMPMSelection(false);


                        // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                        // Define new day and month format
                        try {
                            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                            e.printStackTrace();
                        }

                        // Set listener for date
                        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                            @Override
                            public void onPositiveButtonClick(Date date) {
                                editText_bookingdate.setText(myDateFormat.format(date));

                                bookingdate_send = myDateFormat1.format(date);


                                if (editText_bookingdate.getText().length() != 0) {
                                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("CALL DONE"));
                                }

                            }

                            @Override
                            public void onNegativeButtonClick(Date date) {
                                // Do nothing
                            }

                            @Override
                            public void onNeutralButtonClick(Date date) {
                                // Optional if neutral button does'nt exists
                                editText_bookingdate.setText("");
                            }
                        });

                        if(Integer.parseInt(datevalue)>0) {
                            dateTimeFragment.startAtCalendarView();
                            Calendar calmin = Calendar.getInstance();
                            calmin.add(Calendar.YEAR, 0);
                            calmin.add(Calendar.MONTH, 0);
                            calmin.add(Calendar.DAY_OF_MONTH, Integer.parseInt(datevalue));


                            Calendar calmax = Calendar.getInstance();
                            calmax.add(Calendar.YEAR, 0);
                            calmax.add(Calendar.MONTH, 0);
                            calmax.add(Calendar.DAY_OF_MONTH, Integer.parseInt(datevalueend));


                            //  calmin.setTime(new Date());
                            // calmin.set(calmin.get(Calendar.YEAR), calmin.get(Calendar.MONTH), calmin.get(Calendar.DATE), calmin.get(Calendar.HOUR), calmin.get(Calendar.MINUTE), calmin.get(Calendar.SECOND));
                            dateTimeFragment.setMinimumDateTime(calmin.getTime());
                            //   calmin.setTime(new Date());
                            // calmin.add(Calendar.MONTH, 1);
                            dateTimeFragment.setMaximumDateTime(calmax.getTime());

//                        Log.e("heapSize ","  "+Runtime.getRuntime().freeMemory());
//                        Log.e("heapSize2 ","  "+Runtime.getRuntime().totalMemory());
                            dateTimeFragment.setDefaultDateTime(calmin.getTime());
                            // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                        }else
                        {
                            if(Integer.parseInt(datevalueend) < 0)
                            {
                                Toast.makeText(Drawer_Activity.this, "Campaign booking date expired", Toast.LENGTH_SHORT).show();
                                return;
                            }


                            dateTimeFragment.startAtCalendarView();
                            Calendar calmin = Calendar.getInstance();
                            calmin.add(Calendar.YEAR, 0);
                            calmin.add(Calendar.MONTH, 0);
                            calmin.add(Calendar.DAY_OF_MONTH, 0);


                            Calendar calmax = Calendar.getInstance();
                            calmax.add(Calendar.YEAR, 0);
                            calmax.add(Calendar.MONTH, 0);
                            calmax.add(Calendar.DAY_OF_MONTH,  Integer.parseInt(datevalueend));


                            //  calmin.setTime(new Date());
                            // calmin.set(calmin.get(Calendar.YEAR), calmin.get(Calendar.MONTH), calmin.get(Calendar.DATE), calmin.get(Calendar.HOUR), calmin.get(Calendar.MINUTE), calmin.get(Calendar.SECOND));
                            dateTimeFragment.setMinimumDateTime(calmin.getTime());
                            //   calmin.setTime(new Date());
                            // calmin.add(Calendar.MONTH, 1);
                            dateTimeFragment.setMaximumDateTime(calmax.getTime());

//                        Log.e("heapSize ","  "+Runtime.getRuntime().freeMemory());
//                        Log.e("heapSize2 ","  "+Runtime.getRuntime().totalMemory());
                            dateTimeFragment.setDefaultDateTime(calmin.getTime());
                            // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());



                        }
                        if (dateTimeFragment.isAdded()) {
                            return;
                        } else {
                            dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                        }
                    } else if (!editText_bookingdate.getText().toString().isEmpty()) {
                        Toasty.warning(Drawer_Activity.this, "Booking already done", Toast.LENGTH_SHORT).show();

                    } else {

                        Toasty.warning(Drawer_Activity.this, "First start call", Toast.LENGTH_SHORT).show();

                    }


                }


            }


        });
        editText_nextfollowpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!calldone) {
                    Toasty.warning(Drawer_Activity.this, "First start call", Toast.LENGTH_SHORT).show();
                    return;


                }

                 if(editText_bookingdate.getText().length()>0) {

                    Toasty.warning(Drawer_Activity.this, "First clear Booking Date", Toast.LENGTH_SHORT).show();

                    return;
                }

                dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                if (dateTimeFragment == null) {
                    dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                            getString(R.string.label_datetime_dialog),
                            getString(android.R.string.ok),
                            getString(android.R.string.cancel),
                            getString(R.string.clean) // Optional
                    );
                }

                // Optionally define a timezone
                dateTimeFragment.setTimeZone(TimeZone.getDefault());

                // Init format


                final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", Locale.getDefault());
                final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());


                // Assign unmodifiable values
                dateTimeFragment.set24HoursMode(true);
                dateTimeFragment.setHighlightAMPMSelection(false);


                // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                // Define new day and month format
                try {
                    dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                    e.printStackTrace();
                }

                // Set listener for date
                // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        editText_nextfollowpdate.setText(myDateFormat.format(date));
                        nextfollowupdate_send = myDateFormat1.format(date);
                        if (editText_nextfollowpdate.getText().length() != 0) {
                            contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("CALL DONE"));
                        }
                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                        // Do nothing
                    }

                    @Override
                    public void onNeutralButtonClick(Date date) {
                        // Optional if neutral button does'nt exists
                        editText_nextfollowpdate.setText("");
                    }
                });
                dateTimeFragment.startAtCalendarView();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                dateTimeFragment.setMinimumDateTime(cal.getTime());
                cal.setTime(new Date());
                cal.add(Calendar.MONTH, 1);
                dateTimeFragment.setMaximumDateTime(cal.getTime());
                dateTimeFragment.setDefaultDateTime(new Date());
                // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                if (dateTimeFragment.isAdded()) {
                    return;
                } else {
                    dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                }


            }


        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });

        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);

        shippingDialog.show();


    }


    // History  Details Show Fuction( This Function is Used on click listener of history icon)
    public void getHistoryDetails(int position) {

       // Log.e("CallId", "CallId" + serviceReminderDataList.get(position).getId());

        getSRHistoryData(CommonVariables.customtokenbearer, serviceReminderDataList.get(position).getId());
        final Dialog shippingDialog = new Dialog(Drawer_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter_sr_historydata = new Adapter_SR_Historydatacustom(this, serviceReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sr_historydata);
        adapter_sr_historydata.notifyDataSetChanged();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });


        shippingDialog.show();
    }

    // More Details Fuction( This Function is Used on click listener of more details icon)
    public void getMoreDetails(int position) {
        final Dialog shippingDialog = new Dialog(Drawer_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sr_more_datapopupcustom);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });

        TextInputLayout til1=(TextInputLayout) shippingDialog.findViewById(R.id.til1);
        TextInputLayout til2=(TextInputLayout) shippingDialog.findViewById(R.id.til2);
        TextInputLayout til3=(TextInputLayout) shippingDialog.findViewById(R.id.til3);
        TextInputLayout til4=(TextInputLayout) shippingDialog.findViewById(R.id.til4);
        TextInputLayout til5=(TextInputLayout) shippingDialog.findViewById(R.id.til5);
        TextInputLayout til6=(TextInputLayout) shippingDialog.findViewById(R.id.til6);
        TextInputLayout til7=(TextInputLayout) shippingDialog.findViewById(R.id.til7);
        TextInputLayout til8=(TextInputLayout) shippingDialog.findViewById(R.id.til8);
        TextInputLayout til9=(TextInputLayout) shippingDialog.findViewById(R.id.til9);
        TextInputLayout til10=(TextInputLayout) shippingDialog.findViewById(R.id.til10);
        TextInputLayout til11=(TextInputLayout) shippingDialog.findViewById(R.id.til11);
        TextInputLayout til12=(TextInputLayout) shippingDialog.findViewById(R.id.til12);
        TextInputLayout til13=(TextInputLayout) shippingDialog.findViewById(R.id.til13);
        TextInputLayout til14=(TextInputLayout) shippingDialog.findViewById(R.id.til14);
        TextInputLayout til15=(TextInputLayout) shippingDialog.findViewById(R.id.til15);
        TextInputLayout til16=(TextInputLayout) shippingDialog.findViewById(R.id.til16);
        TextInputLayout til17=(TextInputLayout) shippingDialog.findViewById(R.id.til17);
        TextInputLayout til18=(TextInputLayout) shippingDialog.findViewById(R.id.til18);




        EditText data1 =(EditText) shippingDialog.findViewById(R.id.data1);
        EditText data2 = (EditText) shippingDialog.findViewById(R.id.data2);
        EditText data3 = (EditText) shippingDialog.findViewById(R.id.data3);
        EditText data4 = (EditText) shippingDialog.findViewById(R.id.data4);
        EditText data5 = (EditText) shippingDialog.findViewById(R.id.data5);
        EditText data6 = (EditText) shippingDialog.findViewById(R.id.data6);
        EditText data7 = (EditText) shippingDialog.findViewById(R.id.data7);

        EditText data8 =(EditText) shippingDialog.findViewById(R.id.data8);
        EditText data9 = (EditText) shippingDialog.findViewById(R.id.data9);
        EditText data10 = (EditText) shippingDialog.findViewById(R.id.data10);
        EditText data11 = (EditText) shippingDialog.findViewById(R.id.data11);
        EditText data12 = (EditText) shippingDialog.findViewById(R.id.data12);
        EditText data13 = (EditText) shippingDialog.findViewById(R.id.data13);
        EditText data14 = (EditText) shippingDialog.findViewById(R.id.data14);

        EditText data15 =(EditText) shippingDialog.findViewById(R.id.data15);
        EditText data16 = (EditText) shippingDialog.findViewById(R.id.data16);
        EditText data17 = (EditText) shippingDialog.findViewById(R.id.data17);
        EditText data18 = (EditText) shippingDialog.findViewById(R.id.data18);

        if(dataFieldModelArrayList.size()>0)til1.setHint(dataFieldModelArrayList.get(0).getFieldName());
        if(dataFieldModelArrayList.size()>5)til2.setHint(dataFieldModelArrayList.get(5).getFieldName());
        if(dataFieldModelArrayList.size()>2)til3.setHint(dataFieldModelArrayList.get(2).getFieldName());
        if(dataFieldModelArrayList.size()>3)til4.setHint(dataFieldModelArrayList.get(3).getFieldName());
        if(dataFieldModelArrayList.size()>4)til5.setHint(dataFieldModelArrayList.get(4).getFieldName());
        if(dataFieldModelArrayList.size()>1)til6.setHint(dataFieldModelArrayList.get(1).getFieldName());
        if(dataFieldModelArrayList.size()>6)til7.setHint(dataFieldModelArrayList.get(6).getFieldName());
        if(dataFieldModelArrayList.size()>7)til8.setHint(dataFieldModelArrayList.get(7).getFieldName());
        if(dataFieldModelArrayList.size()>8)til9.setHint(dataFieldModelArrayList.get(8).getFieldName());
        if(dataFieldModelArrayList.size()>9)til10.setHint(dataFieldModelArrayList.get(9).getFieldName());
        if(dataFieldModelArrayList.size()>10)til11.setHint(dataFieldModelArrayList.get(10).getFieldName());
        if(dataFieldModelArrayList.size()>11)til12.setHint(dataFieldModelArrayList.get(11).getFieldName());
        if(dataFieldModelArrayList.size()>12)til13.setHint(dataFieldModelArrayList.get(12).getFieldName());
        if(dataFieldModelArrayList.size()>13)til14.setHint(dataFieldModelArrayList.get(13).getFieldName());
        if(dataFieldModelArrayList.size()>14)til15.setHint(dataFieldModelArrayList.get(14).getFieldName());
        if(dataFieldModelArrayList.size()>15)til16.setHint(dataFieldModelArrayList.get(15).getFieldName());
        if(dataFieldModelArrayList.size()>16)til17.setHint(dataFieldModelArrayList.get(16).getFieldName());
        if(dataFieldModelArrayList.size()>17)til18.setHint(dataFieldModelArrayList.get(17).getFieldName());



        if (serviceReminderDataList.get(position).getData1() != null && !serviceReminderDataList.get(position).getData1().toString().isEmpty())
            data1.setVisibility(View.VISIBLE);
            data1.setText("" + serviceReminderDataList.get(position).getData1().toString());
        if (serviceReminderDataList.get(position).getData6() != null && !serviceReminderDataList.get(position).getData6().toString().isEmpty()) {
            data2.setVisibility(View.VISIBLE);

            data2.setText("" + serviceReminderDataList.get(position).getData6().toString());
        }
        if (serviceReminderDataList.get(position).getData3() != null && !serviceReminderDataList.get(position).getData3().toString().isEmpty()) {
            data3.setVisibility(View.VISIBLE);

            data3.setText("" + serviceReminderDataList.get(position).getData3().toString());
        } else {
          //  ((LinearLayout) data3.getParent().getParent().getParent()).setVisibility(View.GONE);
        }
        if (serviceReminderDataList.get(position).getData4() != null && !serviceReminderDataList.get(position).getData4().toString().isEmpty()) {
            data4.setVisibility(View.VISIBLE);

            data4.setText("" + serviceReminderDataList.get(position).getData4().toString());
        } else {
            //((LinearLayout) data4.getParent().getParent().getParent()).setVisibility(View.GONE);
        }
        if (serviceReminderDataList.get(position).getData5() != null && !serviceReminderDataList.get(position).getData5().toString().isEmpty())
            data5.setVisibility(View.VISIBLE);

        data5.setText("" + serviceReminderDataList.get(position).getData5().toString());
        if (serviceReminderDataList.get(position).getData2() != null && !serviceReminderDataList.get(position).getData2().toString().isEmpty())
            data6.setVisibility(View.VISIBLE);

        data6.setText("" + serviceReminderDataList.get(position).getData2().toString());
        if (serviceReminderDataList.get(position).getData7() != null && !serviceReminderDataList.get(position).getData7().toString().isEmpty())
            data7.setVisibility(View.VISIBLE);

        data7.setText("" + serviceReminderDataList.get(position).getData7());

        if (serviceReminderDataList.get(position).getData8() != null && !serviceReminderDataList.get(position).getData8().toString().isEmpty())
            data8.setVisibility(View.VISIBLE);

        data8.setText("" + serviceReminderDataList.get(position).getData8().toString());
        if (serviceReminderDataList.get(position).getData9() != null && !serviceReminderDataList.get(position).getData9().toString().isEmpty()) {
            data9.setVisibility(View.VISIBLE);
            data9.setText("" + serviceReminderDataList.get(position).getData9());

            //data9.setText("" + serviceReminderDataList.get(position).getData9().toString());
        }
        if (serviceReminderDataList.get(position).getData10() != null && !serviceReminderDataList.get(position).getData10().toString().isEmpty()) {
            data10.setVisibility(View.VISIBLE);
            data10.setText(""+ serviceReminderDataList.get(position).getData10());

           // data10.setText("" + serviceReminderDataList.get(position).getData10().toString());
        } else {
          //  ((LinearLayout) data10.getParent().getParent().getParent()).setVisibility(View.GONE);
        }
        if (serviceReminderDataList.get(position).getData11() != null && !serviceReminderDataList.get(position).getData11().toString().isEmpty()) {
            data11.setVisibility(View.VISIBLE);
            data11.setText("" +serviceReminderDataList.get(position).getData11());

           // data11.setText("" + serviceReminderDataList.get(position).getData11().toString());
        } else {
           // ((LinearLayout) data11.getParent().getParent().getParent()).setVisibility(View.GONE);
        }
        if (serviceReminderDataList.get(position).getData12() != null && !serviceReminderDataList.get(position).getData12().toString().isEmpty())
            data12.setVisibility(View.VISIBLE);
        data12.setText("" +serviceReminderDataList.get(position).getData12());

      //  data12.setText("" + serviceReminderDataList.get(position).getData12().toString());
        if (serviceReminderDataList.get(position).getData13() != null && !serviceReminderDataList.get(position).getData13().toString().isEmpty())
            data13.setVisibility(View.VISIBLE);
        data13.setText("" + serviceReminderDataList.get(position).getData13());

        //data13.setText("" + serviceReminderDataList.get(position).getData13().toString());
        if (serviceReminderDataList.get(position).getData14() != null && !serviceReminderDataList.get(position).getData14().toString().isEmpty())
            data14.setVisibility(View.VISIBLE);

        data14.setText("" + serviceReminderDataList.get(position).getData14().toString());

        if (serviceReminderDataList.get(position).getData15() != null && !serviceReminderDataList.get(position).getData15().toString().isEmpty()) {
            data15.setVisibility(View.VISIBLE);
            data15.setText("" +serviceReminderDataList.get(position).getData15());

           // data15.setText("" + serviceReminderDataList.get(position).getData15().toString());
        } else {
          //  ((LinearLayout) data15.getParent().getParent().getParent()).setVisibility(View.GONE);
        }
        if (serviceReminderDataList.get(position).getData16() != null && !serviceReminderDataList.get(position).getData16().toString().isEmpty())
            data16.setVisibility(View.VISIBLE);

        data16.setText("" + serviceReminderDataList.get(position).getData16().toString());
        if (serviceReminderDataList.get(position).getData17() != null && !serviceReminderDataList.get(position).getData17().toString().isEmpty())
            data17.setVisibility(View.VISIBLE);

        data17.setText("" + serviceReminderDataList.get(position).getData17().toString());
        if (serviceReminderDataList.get(position).getData18() != null && !serviceReminderDataList.get(position).getData18().toString().isEmpty())
            data18.setVisibility(View.VISIBLE);

        data18.setText("" + serviceReminderDataList.get(position).getData18().toString());


    }

    @Override
    public void holdCall(int position, String contatcnumber) {


        OngoingCall.call.hold();

    }

    @Override
    public void unHoldCall(int position, String contatcnumber) {

        OngoingCall.call.unhold();


    }

    // Send WhatsAPP message to this function
    public void sendWhatsappMessage(int position, String number) {

        whatsappclicked = true;
        whatsapp_number = number;
        //  Toast.makeText(this, number, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(Drawer_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShowCustom(this, Drawer_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(Drawer_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(CommonVariables.New_token_for_template);


    }

    // Send Text Message to this Function
    public void sendtextmessage(int position, String contactnumber) {


        textmesasageclicked = true;
        whatsapp_number = contactnumber;
        //   Toast.makeText(this, contactnumber, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(Drawer_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShowCustom(this, Drawer_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(Drawer_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(CommonVariables.New_token_for_template);


    }

    //Get SMS Template
    public void getsmstemplate(String token) {
        templatelist.clear();
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", getString(R.string.SRTAG));
        apiControllerNew.GetSMSTemplate(token, params);



    }

    @Override
    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {

            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                    startActivity(sendMsg);
                    Drawer_Activity.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        } else if (textmesasageclicked == true) {

            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            startActivity(intent);
            Drawer_Activity.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }

    /*------------------------------------------------------------------------------------------------------------------------*/


    /*------------------------------------------------Call Recording and Upload Code---------------------------------------------------------*/
    //Recording Start Code
    public void startRecording(String number) throws IOException {
        if (number == null) {
            return;
        }
        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            audiofile = File.createTempFile(number, ".amr", dir);
            recorder = new MediaRecorder();
            recorder.reset();
//            am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
//            am.setMode(AudioManager.MODE_IN_CALL);
//            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSamplingRate(44100);
            recorder.setAudioEncodingBitRate(96000);
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
            } else if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            } else {
                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
            }

            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);


            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
//                            if (!am.isSpeakerphoneOn())
//                                am.setSpeakerphoneOn(true);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

//            if (!am.isWiredHeadsetOn()) {
//                thread.start();
//            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (IOException e) {
                e.printStackTrace();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Stop  Recording Function
    public void stopRecording(String id) {

        CommonVariables.wavRecorder.stopRecording();


//        CommonVariables.recorder.stop();
//        CommonVariables.recorder.reset();
//        CommonVariables.recorder.release();
//        try {
//            if ( recorder != null) {
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//                recorder=null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //after stopping the recorder, create the sound file and add it to media library.

       if (CommonVariables.audiofile != null) UploadCallRecording(CommonVariables.customtokenbearer, historyID);

    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String historyid) {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + CommonVariables.audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/mp3");
        values.put(MediaStore.Audio.Media.DATA, CommonVariables.audiofile.getAbsolutePath());
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        if (newUri != null) {
            selectedPath = getPath(newUri);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
            File files = new File(selectedPath);
            // RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
            RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
            RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/mp3");
            RequestBody Recording = RequestBody.create(MediaType.parse("audio/*"), files);
            //  RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
            MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording_File", files.getName(), Recording);
            apiController.UploadCallRecording(token, History_id, Content_type, Recording_file);
            //CommonVariables.audiofile = null;
        }
    }

    /*------------------------------------------------------------------------------------------------------------------------*/



    /*------------------------------------------------------------------------------------------------------------------------*/
    // Get Recording File Path

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void clearcolorsharedeprefances() {
        if (arrayList_matserId.size() > 0) {
            for (int k = 0; k < arrayList_matserId.size(); k++) {
                SharedPreferences sharedPreferencesk = getSharedPreferences(arrayList_matserId.get(k), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesk.edit();
                editor.clear();
                editor.commit();
            }
        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        NewDateFormat = null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            //  Toast.makeText(this, ""+new Gson().toJson(data), Toast.LENGTH_SHORT).show();
        }

    }


    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public String dateparse1(String inputdate) {

        if (!inputdate.isEmpty()) {

            // Oct 31 2020  2:16PM

            String inputPattern = "MMM dd yyyy HH:mm:ss";
            String outputPattern = "dd-MMM-yyyy HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            try {
                date = inputFormat.parse(inputdate);
                NewDateFormat = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return inputdate;
        }
        else {
            return inputdate;
        }
    }

    public String dateparse3(String inputdate) {

        if (!inputdate.isEmpty()) {


            // Oct 31 2020  2:16PM
            String inputPattern = "yyyy-MM-dd";
            String outputPattern = "dd-MMM-yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            String newdate = "";
            try {
                date = inputFormat.parse(inputdate);
                newdate = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
           // Log.e("NewDate", "NewData" + newdate);
           // Log.e("InoutDate", "InoutDate" + inputdate);
            return newdate;
        } else {
            return inputdate;
        }
    }

    /*----------------------------------------------------------------------------------------------------------------------------------------------*/




    /*-------------------------------------------------Session Expired Dialog-----------------------------------------------------------------------*/

    public void alertsessiondialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Drawer_Activity.this);
        builder.setTitle("Alert");
        builder.setMessage("Session Expired !!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(layoutParams);


    }


    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }

    /*------------------------------------------------------------------------------------------------------------------------*/


    public void setIndex(int number, boolean longpress) {

        position = number;
        isLongPress = longpress;

        if (indexs != null) indexs.setText("" + number);
        if (indexs2 != null) indexs2.setText("" + number);


    }




}







