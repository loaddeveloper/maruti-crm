package com.hero.weconnect.mysmartcrm.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.role.RoleManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.CallUtils.CallRecorderService;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.ConnectionDetector;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_MissedCall_SR;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Historydata;
import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.CustomerReplyModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_Service_ReminderModel;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.NotComingReasonModel;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.receiver.PhoneStateReceiverNew;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.analytics.Analytics;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;

public class MissedCallActivity_SR extends AppCompatActivity implements ApiResponseListener,
        Adapter_MissedCall_SR.CustomButtonListener, AdapterTemplateShow.CustomButtonListener {
    public   static final int REQUEST_PERMISSION = 0;
    public   static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    public   static int calling_list_serial_no;
    public   static boolean whatsupclick = false, callstuck = false, textmessageclick = false,isLongPress=false;

    public  ArrayList<SR_HistoryModel.DataBean> serviceReminderHistoryDataList = new ArrayList<>();
    public  int call_current_position = 0, position = 0, state, lastposition;
    public  String string_called_status = "", token="", string_customername, string_masterId, string_mobileno, selectedPath = "";
    public  String Callingstatus = "BUSY", offset = "0";
    public  ImageButton startcall, endcall, pausecall;
    public  boolean stop = false;
    public  Date currentdail, discom;
    Intent recordService;

    public  CustomDialog customWaitingDialog;
    public  String bookingdate_send, nextfollowupdate_send, NewDateFormat = null;
    public  HashMap<String, String> params = new HashMap<>();
    public  ArrayList<String> arrayList_matserId = new ArrayList<String>();
    public  ArrayList<String> arrayList_customername = new ArrayList<String>();
    public  ArrayList<String> arrayList_callstatus = new ArrayList<String>();
    public  ArrayList<String> arrayList_mobileno = new ArrayList<String>();
    public  ApiController apiController;
    public  CommonSharedPref commonSharedPref;
    public  Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    public  SharedPreferences sharedPreferencescallUI;
    public  Spinner sp_notcomingreason, contactstatus, customerreply, sp_pickanddrop;
    public  ImageView iv_booking, iv_nextfollowupdate;
    public  String historyID, callstatus, notcomingreason, pickanddrop, contacted_status, customer_reply, whatsapp_number;
    public  ArrayAdapter<String> contactArrayAdapter, customerArrayAdapter, notcomingreasonArrayAdapter;
    public  ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    public  ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    public  ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    public  Adapter_SR_Historydata adapter_sr_historydata;
    public  boolean morebtncliked = false;
    public  File audiofile = null;
    public  AudioManager am = null;
    public  MediaRecorder recorder;
    public  AdapterTemplateShow adapterSettingTemplet;
    public  ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    public  Dialog Whatsapp_shippingDialog;
    public  boolean whatsappclicked = false, textmesasageclicked = false;
    public  Adapter_MissedCall_SR adapter_missedCall;
    public  ArrayList<MissedDataModel_SR.DataBean> misseddatalist = new ArrayList<>();
    public  ArrayList<SingleCustomerHistoryData.DataBean> singledatalist = new ArrayList<>();
    public  SingleCustomerHistoryData.DataBean singledatabean;
    public  Incoming_Service_ReminderModel.DataBean dataBeanSR;
    public  LinearLayout nodata_found_layout;
    public  Button Retry_Button;
    public  LinearLayoutManager linearLayoutManager;
    public  CompositeDisposable disposables = new CompositeDisposable();
    private RecyclerView history_data_recyclerview, recyclerView_template;
    private ConnectionDetector detector;
    private DividerItemDecoration dividerItemDecoration;
    private LinearLayout servicelinear, reminderlinear, complaintcategorysubll, complaintstatusll, ll_update, updatelayout;
    private EditText indexs,indexs2, et_bookingdate, et_nextfollowupdate, remarks, edit_Bookkingno, edit_srname, edit_srservicetype, edit_srmodel, edit_serviceduedate, edit_pickdrop;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private RecyclerView misscall_recyclerview;
    public int index=0,dataposition;
    public boolean calldone=false;
    public boolean morebtnclicked = false;



    @SuppressLint("NewApi")
    public static void start(Context context, Call call) {
        context.startActivity(new Intent(context, MissedCallActivity_SR.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle()));
    }

    public static String getFileSize(File file) {
        String modifiedFileSize = null;
        double fileSize = 0.0;
        if (file.isFile()) {
            fileSize = (double) file.length();//in Bytes
            if (fileSize < 1024) {
                modifiedFileSize = String.valueOf(fileSize).concat("B");
            } else if (fileSize > 1024 && fileSize < (1024 * 1024)) {
                modifiedFileSize = String.valueOf(Math.round((fileSize / 1024 * 100.0)) / 100.0).concat("KB");
            } else {
                modifiedFileSize = String.valueOf(Math.round((fileSize / (1024 * 1204) * 100.0)) / 100.0).concat("MB");
            }
        } else {
            modifiedFileSize = "Unknown";
        }

        return modifiedFileSize;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_missed_call);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+ CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "MissedCallActivity_SR");
        properties.put("Role", ""+CommonVariables.role);

        Analytics.trackEvent("MissedCallActivity_SR", properties);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        initView();

    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        toolbar.setTitle("SR ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
      //  toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        customWaitingDialog = new CustomDialog(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        apiController = new ApiController(this,this);

        detector = new ConnectionDetector(MissedCallActivity_SR.this);

        String folder_main = "Smart CRM";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        customWaitingDialog.show();
        apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        misscall_recyclerview = findViewById(R.id.missed_recyclerview);
        startcall = findViewById(R.id.statcall);
        pausecall = findViewById(R.id.pasusecall);
        endcall = findViewById(R.id.endcall);
        indexs = findViewById(R.id.indexs);
        indexs2 = findViewById(R.id.indexs2);
        nodata_found_layout = findViewById(R.id.nodatfound);
        Retry_Button = findViewById(R.id.refrshbutton);
        if (common_calling_color_sharedPreferances.getPosition().equals("")) {
            position = 0;
        } else {
            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
        }


        adapter_missedCall = new Adapter_MissedCall_SR(this, MissedCallActivity_SR.this, misseddatalist);
        adapter_missedCall.setHasStableIds(true);
        linearLayoutManager = new LinearLayoutManager(MissedCallActivity_SR.this);
        misscall_recyclerview.setLayoutManager(linearLayoutManager);
        dividerItemDecoration = new DividerItemDecoration(misscall_recyclerview.getContext(), linearLayoutManager.getOrientation());
        misscall_recyclerview.setAdapter(adapter_missedCall);
        adapter_missedCall.setCustomButtonListner(MissedCallActivity_SR.this);
        adapter_missedCall.notifyDataSetChanged();
        new OngoingCall();
        Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
        disposables.add(disposable);
        new OngoingCall();
        Disposable disposable2 = OngoingCall.state
                .filter(state -> state == Call.STATE_DISCONNECTED)
                .delay(1, TimeUnit.SECONDS)
                .firstElement()
                .subscribe(this::finish);
        disposables.add(disposable2);
        commonSharedPref = new CommonSharedPref(this);
        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Unauthorized access !!", Toast.LENGTH_SHORT).show();            return;
        }
      /*  token = "bearer " + commonSharedPref.getLoginData().getAccess_token();


        apiController.getContactStatus(token);
        apiController.getCustomerReply(token);
        apiController.getNotComingReason(token);


        if(misseddatalist.size()==0)getMissedData(token, getString(R.string.SRTAG),offset);*/




        /*-----------------------------------Calling Button Click Listeners----------------------------------------------------*/
        indexs.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {

                    int postionoflist = misseddatalist.size();


                    int asg = Integer.parseInt(indexs.getText().toString()) - 1;

                    index=asg;
                    if (asg < postionoflist) {
                        // ////Log.e("SizeofListMainAct","SizeofListMainAct"+postionoflist);

                    } else {
                        Toasty.warning(MissedCallActivity_SR.this, " Index Not Avialabe in list Load More Data", Toast.LENGTH_LONG).show();
                    }

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
                    misscall_recyclerview.scrollToPosition(asg);


                } catch (Exception e) {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        startcall.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {

                // if(indexs != null)indexs.setText("1");


                TelecomManager tm = (TelecomManager)getSystemService(Context.TELECOM_SERVICE);

                if (tm == null) {
                    // whether you want to handle this is up to you really
                    throw new NullPointerException("tm == null");
                }

                tm.endCall();
                if(position >=misseddatalist.size())
                {
                    Toasty.warning(MissedCallActivity_SR.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                    return;

                }

                indexs.setEnabled(false);
                try {
                    TelecomManager systemService = getSystemService(TelecomManager.class);
                    if (systemService != null && !systemService.getDefaultDialerPackage().equals(getPackageName()))
                    {
                        startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.hero.weconnect"));
                        Toasty.warning(MissedCallActivity_SR.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

                            Toasty.warning(MissedCallActivity_SR.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
                        } else {

                            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {


                                position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            } else {

                                position = Integer.parseInt(indexs.getText().toString()) - 1;

                            }
                            common_calling_color_sharedPreferances.setCall_start_stop_status("start");
                            looping();

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }

            }
        });
        endcall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {


                indexs.setEnabled(true);

                try {
                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");


                    OngoingCall.hangup();
                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }


                    if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {

                        OngoingCall.hangup();
                        CommonVariables.endcallclick = false;
                        CommonVariables.receivecall = false;
                        CommonVariables.incoming = false;
                        callstuck = true;
                       /* Intent intent= new Intent(S_R_Activity.this,DashboardActivity.class);
                        finish();
                        startActivity(intent);*/
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
                try {
                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        CallService.discon1();
                    }
                } catch (Exception e) {
                    e.printStackTrace();


                }


                CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long l) {

                        if (calldone) {

                            if (call_current_position < arrayList_matserId.size()) {
                                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editss = chkfss.edit();
                                editss.putString("call", "CALL DONE");
                                editss.commit();
                            }
                        }


                    }

                    @Override
                    public void onFinish() {


                    }
                };
                countDownTimer.start();
                misscall_recyclerview.scrollToPosition(call_current_position);

            }
        });
        pausecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(position >=misseddatalist.size())
                {
                    Toasty.warning(MissedCallActivity_SR.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                    return;

                }


                if (DashboardActivity.callsk) {
                    DashboardActivity.callsk = false;
                }
                try {

                    OngoingCall.hangup();


                } catch (Exception e) {

                }

                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {
                        clearCallUISharePreferances();
                        scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                        try {

                            int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                            common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));


                        } catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                        OngoingCall.call.disconnect();
                        if (OngoingCall.call4.size()>0)OngoingCall.call4.get(0).disconnect();

                        looping();

   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
    startActivity(i);
    finish();*/


                    }


                } catch (Exception e) {

                }


            }
        });
        Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(MissedCallActivity_SR.this, MissedCallActivity_SR.class);
                startActivity(refresh);
                finish();
            }
        });


        /*---------------------------------------------------------------------------------------*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /*--------------------------------------------------------Calling Logic----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void looping() {
        Callingstatus = "BUSY";

        if (common_calling_color_sharedPreferances != null && common_calling_color_sharedPreferances.getCall_start_stop_status() != null && common_calling_color_sharedPreferances.getCall_start_stop_status().equals("start")) {
            try {
                scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                call_current_position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());



                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                final String icca = chkfss.getString("call", "");
                string_called_status = arrayList_callstatus.get(call_current_position);


                if (string_called_status.equals("CALL DONE")) {

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else if (icca.equals("CALL DONE")) {

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                    looping();
                } else {
                    if (detector.isInternetAvailable()) {


                        common_calling_color_sharedPreferances.setcurrentcallmobileno(arrayList_mobileno.get(call_current_position));
                        common_calling_color_sharedPreferances.setcallcurrentstatus("s");
                        common_calling_color_sharedPreferances.setmasterid(arrayList_matserId.get(call_current_position));
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position));
                        misscall_recyclerview.setAdapter(adapter_missedCall);
                        adapter_missedCall.notifyDataSetChanged();


                        if (checkSelfPermission(CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {


                            if (sharedPreferencescallUI.contains("CALLUI")) {

                                clearCallUISharePreferances();
                            }


                          // Uri uri = Uri.parse("tel:" + ""+CommonVariables.SR_Testingnumber);
                            Uri uri = Uri.parse("tel:" +"+91"+ "" +arrayList_mobileno.get(position));
                            //Uri uri = Uri.parse("tel:" + "9024367470");

                            startActivity(new Intent(Intent.ACTION_CALL, uri));
                            position = position + 1;
                            String uid = UUID.randomUUID().toString();


                            try {
                                string_masterId = arrayList_matserId.get(call_current_position);
                                string_mobileno = arrayList_mobileno.get(call_current_position);
                                string_customername = arrayList_customername.get(call_current_position);

                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                            SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editsas = prefsss.edit();
                            editsas.putString(string_masterId, uid);
                            editsas.commit();

                        } else {
                            // Request permission to call
                            ActivityCompat.requestPermissions(MissedCallActivity_SR.this, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                        }
                        String uid = UUID.randomUUID().toString();


                        try {
                            string_masterId = arrayList_matserId.get(call_current_position);
                            string_mobileno = arrayList_mobileno.get(call_current_position);
                            string_customername = arrayList_customername.get(call_current_position);
                        }
                        catch (IndexOutOfBoundsException e) {

                        }
                        SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editsas = prefsss.edit();
                        editsas.putString(string_masterId, uid);
                        editsas.commit();
                    } else {
                        Toasty.error(this, "Please connect Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (IndexOutOfBoundsException e) {

                e.printStackTrace();
            }
        } else {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    public void updateUi(Integer state) {

        ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);

        if(CommonVariables.incoming)
        {
            //  adapter.notifyDataSetChanged();

            try {
                Thread.sleep(3000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);

            CommonVariables.incoming=false;
            return;
        }



        // Set callInfo text by the state
        // a.setText(CallStateString.asString(state).toLowerCase() + "\n" + number);
        this.state = state;
        if (state == Call.STATE_DIALING) {
            stop = false;
            final Handler handler = new Handler();
            currentdail = Calendar.getInstance().getTime();
            final int delay = 4000;
            calldone=false;


            handler.postDelayed(new Runnable() {
                public void run() {

                    if (!stop) {

                        if (sharedPreferencescallUI.contains("CALLUI")) {
                        } else {
                            OngoingCall.call2.add(OngoingCall.call);
                            try {

                                recordService = new Intent(MissedCallActivity_SR.this, CallRecorderService.class);
                                startService(recordService);
                               // startRecording(historyID);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                                int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                                try {
                                    string_masterId = arrayList_matserId.get(ass);
                                    string_mobileno = arrayList_mobileno.get(ass);
                                    string_customername = arrayList_customername.get(ass);
                                } catch (IndexOutOfBoundsException e) {

                                    e.printStackTrace();
                                }

                            }

                            if(contactstatus != null && position== dataposition) {
                               /* contactstatus.setSelection(1);
                                contactstatus.setEnabled(true);*/

                                if(contactstatus != null) contactstatus.setSelection(1);
                                if(contactstatus != null) contactstatus.setEnabled(false);
                                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

                            }


                            Callingstatus = "BUSY";
                            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            historyID = chkfss2.getString(string_masterId, "");

                            if(CommonVariables.Is_LongPress)
                            {
                                CommonVariables.Is_LongPress=false;
                                if(!arrayList_customername.get(position-1).isEmpty()) AddHistoryId(token, string_masterId, historyID, Callingstatus);

                            }else
                            {
                                if(!arrayList_customername.get(position).isEmpty()) AddHistoryId(token, string_masterId, historyID, Callingstatus);

                            }

                            updatecontatctstatus(token, historyID, "Not Contacted", getString(R.string.SRTAG));


                        }


                    }
                }
            }, delay);
        }
        else if (state == Call.STATE_ACTIVE) {
            calldone=true;

            if(contactstatus != null && position == dataposition) {
                contactstatus.setSelection(0);
                contactstatus.setEnabled(false);
                // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

            }
            updatecontatctstatus(token, historyID, "Contacted", getString(R.string.SRTAG));

            // Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
            if (sharedPreferencescallUI.contains("CALLUI"))
            {

            }
            else {

                Callingstatus = "CALL DONE";
                SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                SharedPreferences.Editor editsfs = prefss.edit();
                editsfs.putString("call", Callingstatus);
                editsfs.commit();

                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");
                updatecalledstatus(token, historyID, "" + Callingstatus, getString(R.string.SRTAG));

            }

        }
        else if (state == Call.STATE_DISCONNECTED && !CommonVariables.incoming) {


            ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());


            ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());

            //     Toast.makeText(this, "disconnected1111 "+Calendar.getInstance().getTimeInMillis(), Toast.LENGTH_SHORT).show();

            if (CommonVariables.receivecall) adapter_missedCall.notifyDataSetChanged();


            if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES) && arrayList_mobileno.size()>0) {
                int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                try {
                    string_masterId = arrayList_matserId.get(ass);
                    string_mobileno = arrayList_mobileno.get(ass);
                    string_customername = arrayList_customername.get(ass);

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");

            updatecallendtime(token, historyID, getString(R.string.SRTAG));

            stopService(recordService);
            stopRecording(historyID);

            discom = Calendar.getInstance().getTime();
            try {
                long diffInMs = discom.getTime() - currentdail.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                if (diffInSec > 3) {
                    stop = false;
                } else {
                    stop = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (sharedPreferencescallUI.contains("CALLUI")) {

            } else {
                OngoingCall.call2.clear();


                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {

                    common_calling_color_sharedPreferances.clearposition();

                }


                common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                try {

                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                        /*asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition())));
                    } else {
                        /* asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition())+1 ;*/
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) + 1));
                    }
                    /* common_calling_color_sharedPreferances.setPosition(String.valueOf(asgkkk))*/

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }


                looping();
                SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                SharedPreferences.Editor editsfs = prefss.edit();
                editsfs.putString("call", Callingstatus);
                editsfs.commit();


            }




            adapter_missedCall.notifyDataSetChanged();


        }
        else if (state == Call.STATE_DISCONNECTING) {

            SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
            historyID = chkfss2.getString(string_masterId, "");

            adapter_missedCall.notifyDataSetChanged();
            updatecallendtime(token, historyID, getString(R.string.SRTAG));

        }
        else if (!(state == Call.STATE_ACTIVE)) {
            if (Callingstatus.equals("CALL DONE")) {

            } else {
                Callingstatus = "BUSY";

            }
        }
        else if (!(state == Call.STATE_RINGING)) {
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void offerReplacingDefaultDialer() {
        if (Build.VERSION.SDK_INT <= 28) {
            TelecomManager systemService = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                systemService = this.getSystemService(TelecomManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
                }
            }
        } else {
            @SuppressLint("WrongConstant")
            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
            startActivityForResult(intent, 101);


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void finish(Integer state)
    {

    }
    /*------------------------------------------------------------------------------------------------------------------------*/

    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStop() {
        super.onStop();
        customWaitingDialog.dismiss();
        disposables.clear();


    }

    /*-------------------------------------------------Activity Deafult Function-----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {

        super.onStart();
        offerReplacingDefaultDialer();


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (MissedCallActivity_SR.whatsupclick == false && state == Call.STATE_ACTIVE && !CommonVariables.incoming && MissedCallActivity_SR.textmessageclick == false) {

            /*OngoingCall.hangup();

            startActivity(new Intent(S_R_Activity.this,DashboardActivity.class));
            // pausestate=false;
            finish();*/

            // OngoingCall.hangup();

        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (MissedCallActivity_SR.whatsupclick || MissedCallActivity_SR.textmessageclick) {

            MissedCallActivity_ENQUIRY.whatsupclick = false;
            MissedCallActivity_ENQUIRY.textmessageclick = false;
            new OngoingCall();
            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            disposables.add(disposable);
            adapter_missedCall.notifyDataSetChanged();
        }

    }

    @Override
    public void onBackPressed() {

        final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.back_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        shippingDialog.setCanceledOnTouchOutside(false);
        TextView title = shippingDialog.findViewById(R.id.title);
        TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
        TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
        title.setText("Are you sure you want to exit \n Missed List ?");

        okbutton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (state == Call.STATE_ACTIVE) {
                    OngoingCall.hangup();

                }

                try {

                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                    OngoingCall.hangup();
                    misscall_recyclerview.setAdapter(adapter_missedCall);

                    adapter_missedCall.notifyDataSetChanged();
                    String packagename = commonSharedPref.getPackagesList();
                    if (Build.VERSION.SDK_INT <= 28) {

                        TelecomManager systemService = MissedCallActivity_SR.this.getSystemService(TelecomManager.class);
                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                        }
                        shippingDialog.dismiss();

                    } else {

                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                        startActivity(i);
                        shippingDialog.dismiss();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                } catch (Exception e) {
                    e.printStackTrace();

                }
                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        clearCallUISharePreferances();
                        CallService.discon1();
                        misscall_recyclerview.setAdapter(adapter_missedCall);
                        adapter_missedCall.notifyDataSetChanged();


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();

                shippingDialog.dismiss();
                clearCallUISharePreferances();
                common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                common_calling_color_sharedPreferances.clear__start_stop_status();
                common_calling_color_sharedPreferances.clearposition();
                common_calling_color_sharedPreferances.clearSheer();
                SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                SharedPreferences.Editor editpostion = prefposition.edit();
                SharedPreferences.Editor editceer = prefceer.edit();
                SharedPreferences.Editor editcall = prefcall.edit();
                editpostion.clear();
                editcall.clear();
                editceer.clear();
                CallingColoreditor.clear();
                CallingColoreditor.commit();
                editcall.commit();
                editceer.commit();
                editpostion.commit();
                clearcolorsharedeprefances();
            }
        });

        canncelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        shippingDialog.show();

    }
    /*------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------Other Function -----------------------------------------------------------------*/

    @Override
    protected void onDestroy() {

        if (commonSharedPref.getLoginData() != null) {
            params = new HashMap<>();
            params.put("Type", ApiConstant.SR);
            params.put("UserName", CommonVariables.UserId);
            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
            super.onDestroy();
            //  networkCheckerService.UnregisterChecker();
            disposables.dispose();

        }
    }

    public void scrollmovepostion(Integer ab) {


        misscall_recyclerview.scrollToPosition(ab);

    }

    boolean isLastVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) misscall_recyclerview.getLayoutManager());
        lastposition = layoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = adapter_missedCall.getItemCount();
        int sizes = misseddatalist.size();
        return (lastposition >= numItems - 1);
    }

    public void notconn(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MissedCallActivity_SR.this);
        builder.setTitle("No internet Connection");
        builder.setCancelable(false);

        builder.setMessage("Please turn on internet connection to continue. Last call data will not be saved");

        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (detector.isInternetAvailable()) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    notconn("");
                }


            }
        });
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();


                try {

                    common_calling_color_sharedPreferances.setCall_start_stop_status("stop");

                    OngoingCall.hangup();

                    misscall_recyclerview.setAdapter(adapter_missedCall);

                    adapter_missedCall.notifyDataSetChanged();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {

                    if (sharedPreferencescallUI.contains("CALLUI")) {

                        clearCallUISharePreferances();
                        CallService.discon1();
                        //  recyclerView.setHasFixedSize(true);
                        //   recyclerView.setLayoutManager(linearLayoutManager);
                        //recyclerView.addItemDecoration(dividerItemDecoration);
                        misscall_recyclerview.setAdapter(adapter_missedCall);

                        adapter_missedCall.notifyDataSetChanged();

                      /*  Intent i = new Intent(getApplicationContext(), Dashboard.class);
                        startActivity(i);
                        finish();*/


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        AlertDialog alertDialog = builder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void clearCallUISharePreferances() {
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        SharedPreferences.Editor editss = sharedPreferencescallUI.edit();
        editss.clear();
        editss.commit();
        editss.apply();

    }

    /*------------------------------------------------------------------------------------------------------------------*/



    /*---------------------------------------------------API Call Methods------------------------------------------------------------*/

    //Get Missed Data
    public void getMissedData(String token, String missedtag,String offset) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MissedTag", missedtag);
        params.put("Offset",offset);
        apiController.getMissedData(token, params);

    }



    // Add History Id Function
    public void AddHistoryId(String token, String mastersrid, String historyId, String callingstatus) {
        // customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.MasterSRId, mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BoundType, getString(R.string.OUTBOUND));
        apiController.addCallHistoryData(token, params);
        SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor editsfs = prefss.edit();
        editsfs.putString("call", callingstatus);
        editsfs.commit();
    }

    // Update Contatct Number Function
    public void UpdateContatctNumber(String token, String mastersrid, String historyId, String newcontactno, String priorotyStatus) {
        params = new HashMap<>();
        params.put(ApiConstant.MasterSRId, mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.NewContactNo, newcontactno);
        params.put(ApiConstant.PriorityStatus, priorotyStatus);
        apiController.UpdateContactNumber(token, params);

    }

    // Update Customer Reply Function
    public void updatecustomerreply(String token, String historyId, String Reply) {
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reply, Reply);
        apiController.UpdateCustomerReply(token, params);
    }

    // Update Tagged SR Status
    public void updatetaggedsrstatus(String token, String historyId, String Status) {
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, Status);
        apiController.UpdateTaggedSRStatus(token, params);
    }

    // Update Booking Date & Time
    public void updatebookingdate(String token, String historyId, String bookingdate, String string_masterId) {
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BookingDate, bookingdate);
        apiController.UpdateBookingate(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "booking");
        edits.commit();
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String calltype) {
        // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCallEndTime(token, params);
    }

    // Update Remark
    public void updateremark(String token, String historyId, String remark, String calltype, String string_masterId) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Comment, remark);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateRemark(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status1", "rmkcolor");
        edits.commit();

    }

    // Update Follow-Up Date
    public void updatefollowupdate(String token, String historyId, String followdatetime, String calltype, String masterId) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.FollowupDateTime, followdatetime);
        params.put(ApiConstant.CallType, calltype);
        apiController.UpdateNextFollowUpdate(token, params);
        SharedPreferences prefs = getSharedPreferences(masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "NextFollowUp");
        edits.commit();
    }

    // Update Contatct Status
    public void updatecontatctstatus(String token, String historyId, String contactstatys, String calltype) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatys);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateContactStatus(token, params);
    }

    // Update Contatct Status
    public void updatepickanddrop(String token, String historyId, String status) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, status);

        apiController.UpdatePickdrop(token, params);
    }

    // Update CalledStatus
    public void updatecalledstatus(String token, String historyId, String callstatus, String calltype) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallStatus, callstatus);
        params.put(ApiConstant.CallType, calltype);

        apiController.UpdateCalledStatus(token, params);
    }

    // Update NotComingReason
    public void updatenotcomingReason(String token, String historyId, String reason, String masterId) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, reason);

        apiController.UpdateNotComingReason(token, params);
        SharedPreferences prefs = getSharedPreferences(masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "NCR");
        edits.commit();
    }

    // Update get Service Reminder History Data
    public void getSRHistoryData(String token, String Id) {

        customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, Id);

        apiController.getSRHistory(token, params);
    }

    //Get Single Customer Service Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String tag, String masterId) {

        customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, masterId);
        params.put(ApiConstant.Tag, tag);

        apiController.getSingleCustomerHistoryData(token, params);
    }

    // API Respose of Contact Status List(In this Function Get API Responses of Spinner Lists)
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.MISSEDDATA)) {

            if (misseddatalist.size() == 0) offset = "0";
            if (morebtnclicked)
            {

                morebtnclicked = false;
            }
            else
            {
                misseddatalist.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();
            }
            MissedDataModel_SR missedDataModelSR = (MissedDataModel_SR) superClassCastBean;
            if (missedDataModelSR.getData().toString().contains("No data to allocate.") || missedDataModelSR.getData().size()==0) {
                customWaitingDialog.dismiss();
                nodata_found_layout.setVisibility(View.VISIBLE);

            } else {
                if (missedDataModelSR.getData() != null) {
                    for (MissedDataModel_SR.DataBean dataBean : missedDataModelSR.getData()) {

                        offset = "" + dataBean.getSno();
                        misseddatalist.add(dataBean);
                        arrayList_mobileno.add(dataBean.getCusotmercontact());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCusotmer_first_name());
                        arrayList_callstatus.add("BUSY");
                        nodata_found_layout.setVisibility(View.GONE);

                    }
                    misscall_recyclerview.setAdapter(adapter_missedCall);
                    customWaitingDialog.dismiss();
                    adapter_missedCall.notifyDataSetChanged();
                }

            }

        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customWaitingDialog.dismiss();

            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();


            apiController.getContactStatus(token);
            apiController.getCustomerReply(token);
            apiController.getNotComingReason(token);


            if(misseddatalist.size()==0)getMissedData(token, getString(R.string.SRTAG),offset);

            ////Log.e("LLLLLLL  ",token);



        }
        else if (beanTag.matches(ApiConstant.SRINCOMINGDATA))
        {

            Incoming_Service_ReminderModel incoming_service_reminderModel = (Incoming_Service_ReminderModel) superClassCastBean;

            if (incoming_service_reminderModel.getData().size() > 0) {
                dataBeanSR = incoming_service_reminderModel.getData().get(0);

                edit_srname.setText(dataBeanSR.getCusotmer_first_name() + dataBeanSR.getCustomer_last_name());
                edit_srservicetype.setText(dataBeanSR.getServiceupdate());
                edit_srmodel.setText(dataBeanSR.getModel());
                if(!dataBeanSR.getNext_service_due_date().isEmpty())edit_serviceduedate.setText(dateparse3(dataBeanSR.getNext_service_due_date().substring(0,10)));
                edit_pickdrop.setText(dataBeanSR.getLs_balance());
                updatelayout.setVisibility(View.VISIBLE);
                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");
                AddHistoryId(token, dataBeanSR.getId(), historyID, Callingstatus);
                updatecalledstatus(token, historyID, "" + Callingstatus, getString(R.string.SRTAG));
                getSingleCustomerSRHistoryData(token,getString(R.string.SRTAG),dataBeanSR.getId());
                try {
                    startRecording(historyID);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }


        } else if (beanTag.matches(ApiConstant.CONTACT_STATUS)) {
            ContactStatusModel contactStatusModel = (ContactStatusModel) superClassCastBean;
            if (contactStatusModel.getData() != null && contactStatusModel.getData().size() > 0) {
                for (ContactStatusModel.DataBean dataBean : contactStatusModel.getData()) {
                    contactStatuslist[0].add(dataBean.getStatus());

                }
            }
        } else if (beanTag.matches(ApiConstant.CUSTOMER_REPLY)) {
            CustomerReplyModel customerReplyModel = (CustomerReplyModel) superClassCastBean;
            if (customerReplyModel.getData() != null && customerReplyModel.getData().size() > 0) {
                customerreplylist[0].add("  ");
                for (CustomerReplyModel.DataBean dataBean : customerReplyModel.getData()) {
                    customerreplylist[0].add(dataBean.getReply());

                }
            }
        } else if (beanTag.matches(ApiConstant.NOT_COMING_REASON)) {
            NotComingReasonModel notComingReasonModel = (NotComingReasonModel) superClassCastBean;
            if (notComingReasonModel.getData() != null && notComingReasonModel.getData().size() > 0) {
                notcomingreasonlist[0].add("  ");
                for (NotComingReasonModel.DataBean dataBean : notComingReasonModel.getData()) {
                    notcomingreasonlist[0].add(dataBean.getReason());

                }
            }
        } else if (beanTag.matches(ApiConstant.ADDCALLHISTORYUPDATESR)) {
            AddCallHistoryUpdateModel addCallHistoryUpdateModel = (AddCallHistoryUpdateModel) superClassCastBean;
            if (addCallHistoryUpdateModel.getMessage().matches("Updated")) {

            }
        } else if (beanTag.matches(ApiConstant.UPDATECALLENDTIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {

            }
        } else if (beanTag.matches(ApiConstant.UPDATECALLEDSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {

            }
        } else if (beanTag.matches(ApiConstant.GETCALLINGHISTORY)) {
            SR_HistoryModel sr_historyModel = (SR_HistoryModel) superClassCastBean;
            if (sr_historyModel.getData() != null) {
                for (SR_HistoryModel.DataBean dataBean : sr_historyModel.getData()) {
                    serviceReminderHistoryDataList.add(dataBean);

                }
                customWaitingDialog.dismiss();
                adapter_sr_historydata.notifyDataSetChanged();
            }


            customWaitingDialog.dismiss();


        } else if (beanTag.matches(ApiConstant.UPLOADRECORDING)) {

            UploadRecordingModel uploadRecordingModel = (UploadRecordingModel) superClassCastBean;
            if (uploadRecordingModel.getData().matches("Updated"))
            {
              //  CommonVariables.audiofile.delete();
                CommonVariables.audiofile=null;
            }
                customWaitingDialog.dismiss();


        } else if (beanTag.matches(ApiConstant.UPDATEBOOKINGDATETIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {


                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEREMARK)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
            }


            //      srtextviewtemplate.setText(commonModel.getData());
        } else if (beanTag.matches(ApiConstant.SINGLEDATA)) {


            singledatalist.clear();
            SingleCustomerHistoryData singleCustomerHistoryData = (SingleCustomerHistoryData) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {
                singledatalist.addAll(singleCustomerHistoryData.getData());


                try {
                    remarks.setText("" + singleCustomerHistoryData.getData().get(0).getComments());
                    if (singleCustomerHistoryData.getData().get(0).getFollowup_date().contains("1900-01-01T00:00:00")) {

                        et_nextfollowupdate.setText("");
                    } else {

                        dateparse(singleCustomerHistoryData.getData().get(0).getFollowup_date().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", ""));
                        et_nextfollowupdate.setText(NewDateFormat);
                    }
                    if (singleCustomerHistoryData.getData().get(0).getServicebooking().contains("1900-01-01T00:00:00")) {

                        edit_Bookkingno.setText("");

                    } else {
                        dateparse(singleCustomerHistoryData.getData().get(0).getServicebooking().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", ""));

                        et_bookingdate.setText(NewDateFormat);
                    }

                    edit_Bookkingno.setText(singleCustomerHistoryData.getData().get(0).getServicerequest());
                    sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getNot_coming_reason().trim()));
                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getCustomerreply()));
                    customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getCustomerreply()));

                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        }
        customWaitingDialog.dismiss();

    }

    @Override
    public void onFailure(String msg) {
        if (msg.matches(ApiConstant.MISSEDDATA)) {
            customWaitingDialog.dismiss();
            nodata_found_layout.setVisibility(View.VISIBLE);

        } else {
            customWaitingDialog.dismiss();
        }

    }

    @Override
    public void onError(String msg) {
        customWaitingDialog.dismiss();
    }

    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }
    /*---------------------------------------------------------------------------------------------------------------*/

    /*---------------------------------------------------Adapter Click Listeners----------------------------------------------------------------*/
    // Customer Details Show Fuction( This Function is Show Input Popup and Get Feedback from Customer in this Popup)
    public void getCustomerDetails(int position, String master_ID, String customername, String servicetype, String model, String serviceduefrom, String pickdropbalance, String contactnumber) {
        final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.missed_sr_data_layout);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Customer Details Popup Id Genrate Code
        /*-------------------------------------------------*/


        ll_update = shippingDialog.findViewById(R.id.ll_update);
        customerreply = shippingDialog.findViewById(R.id.customerreplay);
        contactstatus = shippingDialog.findViewById(R.id.contactstatus);
        sp_notcomingreason = shippingDialog.findViewById(R.id.notcomingreason);
        sp_pickanddrop = shippingDialog.findViewById(R.id.pickanddrop);
        et_bookingdate = shippingDialog.findViewById(R.id.bboking);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        ib_history.setVisibility(View.GONE);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        iv_booking = shippingDialog.findViewById(R.id.iv_clear_bookingdate);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);

        edit_srname = shippingDialog.findViewById(R.id.srcuname);
        edit_srservicetype = shippingDialog.findViewById(R.id.serivcetype);
        edit_srmodel = shippingDialog.findViewById(R.id.srmodel);
        edit_serviceduedate = shippingDialog.findViewById(R.id.srserduefrom);
        edit_Bookkingno = shippingDialog.findViewById(R.id.bookingno);
        edit_pickdrop = shippingDialog.findViewById(R.id.pickdropbalance);
        remarks = shippingDialog.findViewById(R.id.remarks);

        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);


        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);


        if (singledatalist != null && singledatalist.size() > 0) {
//            if(singledatalist.get(0).getServicebooking() != null && !singledatalist.get(0).getServicebooking().equals(""))
//            {
//
//                et_bookingdate.setText(""+singledatalist.get(0).getServicebooking().toString().replaceAll("T00:00:00",""));
//            }
//
//            if(singledatalist.get(0).getReminder_followup_date() != null && !singledatalist.get(0).getReminder_followup_date().equals(""))
//            {
//                et_nextfollowupdate.setText(""+singledatalist.get(0).getReminder_followup_date().toString().replaceAll("T00:00:00",""));
//            }
//
//            if(singledatalist.get(0).getComments() != null && !singledatalist.get(0).getComments().equals(""))
//            {
//                remarks.setText(""+singledatalist.get(0).getComments());
//            }
        } else {


        }

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Toast.makeText(MissedCallActivity_SR.this, "masterID " + master_ID, Toast.LENGTH_SHORT).show();

                SharedPreferences chkfss = getSharedPreferences(master_ID, Context.MODE_PRIVATE);
                historyID = chkfss.getString(master_ID, "");

                if (et_bookingdate.getText().length() > 0) {
                    updatebookingdate(token, historyID, et_bookingdate.getText().toString(), string_masterId);
                }
                if (et_nextfollowupdate.getText().length() > 0) {
                    updatefollowupdate(token, historyID, et_nextfollowupdate.getText().toString(), getString(R.string.SRTAG), string_masterId);
                }
                if (notcomingreason != null) {
                    updatenotcomingReason(token, historyID, notcomingreason, string_masterId);
                }
                if (customer_reply != null) {
                    updatecustomerreply(token, historyID, customer_reply);
                }

                if (contacted_status != null) {
                    updatecontatctstatus(token, historyID, contacted_status, getString(R.string.SRTAG));
                }

                if (pickanddrop != null) {
                    updatepickanddrop(token, historyID, pickanddrop);
                }

                if (remarks.getText().length() > 0) {
                    updateremark(token, historyID, remarks.getText().toString(), getString(R.string.SRTAG), string_masterId);
                }

                params = new HashMap<>();
                params.put("HistoryId", historyID);
                apiController.createServiceRequest(token, params);

                shippingDialog.dismiss();

            }
        });
        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });

        // Popup to Set Text the Values
        edit_srname.setText(customername);
        edit_srservicetype.setText(servicetype);
        edit_srmodel.setText(model);
        edit_serviceduedate.setText(serviceduefrom);
        edit_pickdrop.setText(pickdropbalance);

        contactArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, contactStatuslist[0]);
        contactstatus.setAdapter(contactArrayAdapter);
        contactstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    contacted_status = contactstatus.getSelectedItem().toString();

                } else {
                    callstatus = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        customerArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, customerreplylist[0]);
        customerreply.setAdapter(customerArrayAdapter);
        customerreply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    customer_reply = customerreply.getSelectedItem().toString();

                } else {
                    customer_reply = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        notcomingreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, notcomingreasonlist[0]);
        sp_notcomingreason.setAdapter(notcomingreasonArrayAdapter);
        sp_notcomingreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (et_nextfollowupdate.getText().toString().equals("") && et_bookingdate.getText().toString().equals("")) {

                    if (position != 0) {
                        notcomingreason = sp_notcomingreason.getSelectedItem().toString();

                    } else {
                        notcomingreason = null;
                    }
                } else if (!et_nextfollowupdate.getText().toString().equals("")) {

                    try {
                        sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition("NotComing Reason"));

                    } catch (NullPointerException e) {

                    }
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Next Followup Date", Toast.LENGTH_SHORT).show();
                } else if (!et_bookingdate.getText().toString().equals("")) {
                    try {
                        sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition("NotComing Reason"));

                    } catch (NullPointerException e) {

                    }
                    Toasty.warning(MissedCallActivity_SR.this, "First clear Booking Date", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_pickanddrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    pickanddrop = sp_pickanddrop.getSelectedItem().toString();

                } else {
                    pickanddrop = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*-------------------------------------------------*/


        iv_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_Bookkingno.getText().length() > 0) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(MissedCallActivity_SR.this);
                    builder.setMessage("Do you want cancel booking ?");
                    builder.setNeutralButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            params = new HashMap<>();
                            params.put("HistoryId", singledatalist.get(0).getId());
                            apiController.CancelServiceRequest(token, params);
                            et_bookingdate.setText("");
                            edit_Bookkingno.setText("");


                        }
                    });
                    builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });

                    builder.show();


                } else {
                    et_bookingdate.setText("");

                }
            }
        });
        iv_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowupdate.setText("");
            }
        });
        ib_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getSRHistoryData(token, misseddatalist.get(position).getId());

                final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.historydata_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);

                TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
                history_data_recyclerview.setHasFixedSize(true);
                history_data_recyclerview.setLayoutManager(new LinearLayoutManager(MissedCallActivity_SR.this, RecyclerView.VERTICAL, false));
                adapter_sr_historydata = new Adapter_SR_Historydata(MissedCallActivity_SR.this, serviceReminderHistoryDataList);
                history_data_recyclerview.setAdapter(adapter_sr_historydata);
                adapter_sr_historydata.notifyDataSetChanged();


                shippingDialog.show();

            }
        });
        et_bookingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(singledatalist.size()==0)getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);

                if (sp_notcomingreason.getSelectedItemPosition() == 0 && et_nextfollowupdate.getText().toString().length() == 0) {
                    if( singledatalist.size()>0 && singledatalist.get(0).getServicerequest().isEmpty()) {

                        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                        if (dateTimeFragment == null) {
                            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                    getString(R.string.label_datetime_dialog),
                                    getString(android.R.string.ok),
                                    getString(android.R.string.cancel),
                                    getString(R.string.clean) // Optional
                            );
                        }

                        // Optionally define a timezone
                        dateTimeFragment.setTimeZone(TimeZone.getDefault());

                        // Init format


                        final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                        final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());


                        // Assign unmodifiable values
                        dateTimeFragment.set24HoursMode(true);
                        dateTimeFragment.setHighlightAMPMSelection(false);


                        // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                        // Define new day and month format
                        try {
                            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                            e.printStackTrace();
                        }

                        // Set listener for date
                        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                            @Override
                            public void onPositiveButtonClick(Date date) {
                                et_bookingdate.setText(myDateFormat.format(date));
                                sp_pickanddrop.setEnabled(true);
                                bookingdate_send = myDateFormat1.format(date);


                                if (et_bookingdate.getText().length() != 0) {
                                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("Contacted"));
                                }

                            }

                            @Override
                            public void onNegativeButtonClick(Date date) {
                                // Do nothing
                            }

                            @Override
                            public void onNeutralButtonClick(Date date) {
                                // Optional if neutral button does'nt exists
                                et_bookingdate.setText("");
                            }
                        });
                        dateTimeFragment.startAtCalendarView();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                        dateTimeFragment.setMinimumDateTime(cal.getTime());
                        cal.setTime(new Date());
                        cal.add(Calendar.MONTH, 1);
                        dateTimeFragment.setMaximumDateTime(cal.getTime());
                        dateTimeFragment.setDefaultDateTime(new Date());
                        // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());

                        if (dateTimeFragment.isAdded()) {
                            return;
                        } else {
                            dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                        }
                    }else if(!et_bookingdate.getText().toString().isEmpty())
                    {
                        Toasty.warning(MissedCallActivity_SR.this, "Booking already done", Toast.LENGTH_SHORT).show();

                    }else
                    {
                        Toasty.warning(MissedCallActivity_SR.this, "First start call", Toast.LENGTH_SHORT).show();

                    }

                } else if (et_nextfollowupdate.getText().toString().length() > 0) {

                    Toasty.warning(MissedCallActivity_SR.this, "First Clear NextFollowup Date", Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                }

            }


        });
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bookingdate.getText().length() == 0 && sp_notcomingreason.getSelectedItemPosition()==0) {
                    dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                    if (dateTimeFragment == null) {
                        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                getString(R.string.label_datetime_dialog),
                                getString(android.R.string.ok),
                                getString(android.R.string.cancel),
                                getString(R.string.clean) // Optional
                        );
                    }

                    // Optionally define a timezone
                    dateTimeFragment.setTimeZone(TimeZone.getDefault());

                    // Init format


                    final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", java.util.Locale.getDefault());
                    final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());

                    // Assign unmodifiable values
                    dateTimeFragment.set24HoursMode(true);
                    dateTimeFragment.setHighlightAMPMSelection(false);


                    // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                    // Define new day and month format
                    try {
                        dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                    } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                        e.printStackTrace();
                    }

                    // Set listener for date
                    // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                        @Override
                        public void onPositiveButtonClick(Date date) {
                            et_nextfollowupdate.setText(myDateFormat.format(date));
                            nextfollowupdate_send = myDateFormat1.format(date);
                            if (et_nextfollowupdate.getText().length() != 0) {
                                customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition("Call Back"));
                            }
                        }

                        @Override
                        public void onNegativeButtonClick(Date date) {
                            // Do nothing
                        }

                        @Override
                        public void onNeutralButtonClick(Date date) {
                            // Optional if neutral button does'nt exists
                            et_nextfollowupdate.setText("");
                        }
                    });
                    dateTimeFragment.startAtCalendarView();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    dateTimeFragment.setMinimumDateTime(cal.getTime());
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, 1);
                    dateTimeFragment.setMaximumDateTime(cal.getTime());
                    dateTimeFragment.setDefaultDateTime(new Date());
                    // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                    if (dateTimeFragment.isAdded())
                    {
                        return;
                    }
                    else
                    {
                        dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                    }

                } else if(et_bookingdate.getText().toString().length()>0){

                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Booking Date", Toast.LENGTH_SHORT).show();
                }else
                {
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                }

            }


        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });

        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
    }

    // History  Details Show Fuction( This Function is Used on click listener of history icon)
    public void getHistoryDetails(int position) {

        getSRHistoryData(token, misseddatalist.get(position).getId());

        final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter_sr_historydata = new Adapter_SR_Historydata(this, serviceReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sr_historydata);
        adapter_sr_historydata.notifyDataSetChanged();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });


        shippingDialog.show();

    }

    // More Details Fuction( This Function is Used on click listener of more details icon)
    public void getMoreDetails(int position) {
        final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sr_more_datapopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        EditText edit_number = shippingDialog.findViewById(R.id.mobileno);
        EditText last_service_date = shippingDialog.findViewById(R.id.last_service_date);
        EditText last_service_type = shippingDialog.findViewById(R.id.last_service_type);
        EditText last_jc_km = shippingDialog.findViewById(R.id.last_jc_km);
        EditText last_invoice_value = shippingDialog.findViewById(R.id.last_invoice_value);
        EditText open_complaint_number = shippingDialog.findViewById(R.id.open_complaint_number);
        EditText open_sr_number = shippingDialog.findViewById(R.id.open_sr_number);
        EditText fsc1_date = shippingDialog.findViewById(R.id.fsc1_date);
        EditText fsc2_date = shippingDialog.findViewById(R.id.fsc2_date);
        EditText fsc3_date = shippingDialog.findViewById(R.id.fsc3_date);
        EditText fsc4_date = shippingDialog.findViewById(R.id.fsc4_date);
        EditText fsc5_date = shippingDialog.findViewById(R.id.fsc5_date);
        EditText paid1_date = shippingDialog.findViewById(R.id.paid1_date);
        EditText creatat = shippingDialog.findViewById(R.id.createdat);
        EditText vin = shippingDialog.findViewById(R.id.vin);
        EditText color = shippingDialog.findViewById(R.id.color);
        EditText hsrp_stage = shippingDialog.findViewById(R.id.hsrp_stage);
        EditText warranty_start_date = shippingDialog.findViewById(R.id.warranty_start_date);
        EditText service_update = shippingDialog.findViewById(R.id.service_update);
        EditText mandatory_update = shippingDialog.findViewById(R.id.mandatory_update);
        EditText herosure = shippingDialog.findViewById(R.id.herosure);
        EditText avg_km_run = shippingDialog.findViewById(R.id.avg_km_run);
        EditText digital_customer = shippingDialog.findViewById(R.id.digitalCustomer);
        EditText organization = shippingDialog.findViewById(R.id.organization);
        EditText customer_rating = shippingDialog.findViewById(R.id.customer_rating);
        EditText address = shippingDialog.findViewById(R.id.address);
        EditText multiple_nameandmobile = shippingDialog.findViewById(R.id.name_and_mobileno_multiple);
        EditText next_service_due_date = shippingDialog.findViewById(R.id.next_service_due_date);
        EditText reminder_followup_date = shippingDialog.findViewById(R.id.reminder_follow_update);
        EditText insurance_expiry_date = shippingDialog.findViewById(R.id.insurance_expiry_date);
        EditText gl_expiry = shippingDialog.findViewById(R.id.gl_expiry);
        EditText gl_points = shippingDialog.findViewById(R.id.gl_points);
        EditText ls_expiry = shippingDialog.findViewById(R.id.ls_expiry);
        EditText ls_balance = shippingDialog.findViewById(R.id.ls_balance);
        EditText joyride_expiry = shippingDialog.findViewById(R.id.joyride_expiry);
        EditText joyride_balance = shippingDialog.findViewById(R.id.joyride_balance);

        if (misseddatalist.get(position).getCreatedat() != null)
            creatat.setText("" + misseddatalist.get(position).getCreatedat().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getVin() != null)
            vin.setText("" + misseddatalist.get(position).getVin());
        if (misseddatalist.get(position).getColor() != null)
            color.setText("" + misseddatalist.get(position).getColor().toUpperCase());
        if (misseddatalist.get(position).getHsrp_stage() != null)
            hsrp_stage.setText("" + misseddatalist.get(position).getHsrp_stage().toUpperCase());
        if (misseddatalist.get(position).getWarranty_start_date() != null)
            warranty_start_date.setText("" + misseddatalist.get(position).getWarranty_start_date().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getServiceupdate() != null)
            service_update.setText("" + misseddatalist.get(position).getServiceupdate().toUpperCase());
        if (misseddatalist.get(position).getMandatoryupdate() != null)
            mandatory_update.setText("" + misseddatalist.get(position).getMandatoryupdate().toUpperCase());
        if (misseddatalist.get(position).getHerosure() != null)
            herosure.setText("" + misseddatalist.get(position).getHerosure().toUpperCase());
        if (misseddatalist.get(position).getAvg_km_run() != null)
            avg_km_run.setText("" + misseddatalist.get(position).getAvg_km_run());
        if (misseddatalist.get(position).getDigitalcustomer() != null)
            digital_customer.setText("" + misseddatalist.get(position).getDigitalcustomer().toUpperCase());
        if (misseddatalist.get(position).getCustomerrating() != null)
            customer_rating.setText("" + misseddatalist.get(position).getCustomerrating());
        if (misseddatalist.get(position).getAddress() != null)
            address.setText("" + misseddatalist.get(position).getAddress().toUpperCase());
        if (misseddatalist.get(position).getName_and_mobileno_multiple() != null)
            multiple_nameandmobile.setText("" + misseddatalist.get(position).getName_and_mobileno_multiple().toUpperCase());
        if (misseddatalist.get(position).getNext_service_due_date() != null)
            next_service_due_date.setText("" + misseddatalist.get(position).getNext_service_due_date().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getReminder_followup_date() != null)
            reminder_followup_date.setText("" + misseddatalist.get(position).getReminder_followup_date().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getInsurance_expiry_date() != null)
            insurance_expiry_date.setText("" + misseddatalist.get(position).getInsurance_expiry_date().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getGl_expiry() != null)
            gl_expiry.setText("" + misseddatalist.get(position).getGl_expiry().toUpperCase().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getGl_points() != null)
            gl_points.setText("" + misseddatalist.get(position).getGl_points().toUpperCase());
        if (misseddatalist.get(position).getLs_expiry() != null)
            ls_expiry.setText("" + misseddatalist.get(position).getLs_expiry().toUpperCase().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getLs_balance() != null)
            ls_balance.setText("" + misseddatalist.get(position).getLs_balance().toUpperCase());
        if (misseddatalist.get(position).getJoyride_expiry() != null)
            joyride_expiry.setText("" + misseddatalist.get(position).getJoyride_expiry().toUpperCase().replaceAll("T00:00:00", ""));
        if (misseddatalist.get(position).getJoyride_balance() != null)
            joyride_balance.setText("" + misseddatalist.get(position).getJoyride_balance().toUpperCase());
        last_service_date.setText(misseddatalist.get(position).getLast_service_date().toUpperCase().replaceAll("T00:00:00", ""));
        last_service_type.setText(misseddatalist.get(position).getLast_service_type().toUpperCase().replaceAll("T00:00:00", ""));
        last_jc_km.setText(misseddatalist.get(position).getLast_jc_km().toUpperCase());
        last_invoice_value.setText(misseddatalist.get(position).getLast_invoice_value().toUpperCase());
        open_complaint_number.setText(misseddatalist.get(position).getOpen_complaint_number().toUpperCase());
        open_sr_number.setText(misseddatalist.get(position).getOpen_sr_number().toUpperCase());
        fsc1_date.setText(misseddatalist.get(position).getFsc1_date().toUpperCase().replaceAll("T00:00:00", ""));
        fsc2_date.setText(misseddatalist.get(position).getFsc2_date().toUpperCase().replaceAll("T00:00:00", ""));
        fsc3_date.setText(misseddatalist.get(position).getFsc3_date().toUpperCase().replaceAll("T00:00:00", ""));
        fsc4_date.setText(misseddatalist.get(position).getFsc4_date().toUpperCase().replaceAll("T00:00:00", ""));
        fsc5_date.setText(misseddatalist.get(position).getFsc5_date().toUpperCase().replaceAll("T00:00:00", ""));
        paid1_date.setText(misseddatalist.get(position).getPaid1_date().toUpperCase().replaceAll("T00:00:00", ""));
        organization.setText(misseddatalist.get(position).getOrganization().toUpperCase());

        edit_number.setText(misseddatalist.get(position).getCusotmercontact().toUpperCase());
//        edit_number.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
//                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                shippingDialog.setContentView(R.layout.mobile_number_change_popup);
//                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                shippingDialog.setCancelable(true);
//                shippingDialog.setCanceledOnTouchOutside(false);
//                EditText et_newnumber =(EditText) shippingDialog.findViewById(R.id.setupnewEdit);
//                Button done_btn =(Button)shippingDialog.findViewById(R.id.setupnewDone);
//                Button cancel_btn =(Button)shippingDialog.findViewById(R.id.setupnewCancle);
//                done_btn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if(et_newnumber.getText().length()==10 )
//                        {
//                            if(!misseddatalist.get(position).getCusotmercontact().equals(et_newnumber.getText().toString()))
//                            {
//                                UpdateContatctNumber(token,misseddatalist.get(position).getId1(),historyID,et_newnumber.getText().toString(),
//                                        "Alternate");
//
//                            }else
//                            {
//                                Toast.makeText(MissedCallActivity_SR.this, " number already exist", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }else
//                        {
//                            Toast.makeText(MissedCallActivity_SR.this, "Please check number", Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                });
//
//
//
//                shippingDialog.show();
//                TextView title = (TextView) shippingDialog.findViewById(R.id.setupnewtitle);
//                title.setText("Do You Want to Change Customer Mobile Number ?");
//
//
//
//
//                return false;
//            }
//        });


    }

    // Send WhatsAPP message to this function
    public void sendWhatsappMessage(int position, String number) {

        whatsappclicked = true;
        whatsapp_number = number;
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(MissedCallActivity_SR.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, MissedCallActivity_SR.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(MissedCallActivity_SR.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, getString(R.string.SRTAG));


    }

    // Send Text Message to this Function
    public void sendtextmessage(int position, String contactnumber) {


        textmesasageclicked = true;
        whatsapp_number = contactnumber;
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(MissedCallActivity_SR.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, MissedCallActivity_SR.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(MissedCallActivity_SR.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, getString(R.string.SRTAG));


    }

    @Override
    public void getMissedData(int position) {


        final Dialog shippingDialog = new Dialog(MissedCallActivity_SR.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.missed_sr_data_layout);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.show();
        // Customer Details Popup Id Genrate Code
        /*-------------------------------------------------*/


        ll_update = shippingDialog.findViewById(R.id.ll_update);
        customerreply = shippingDialog.findViewById(R.id.customerreplay);
        contactstatus = shippingDialog.findViewById(R.id.contactstatus);
        sp_notcomingreason = shippingDialog.findViewById(R.id.notcomingreason);
        sp_pickanddrop = shippingDialog.findViewById(R.id.pickanddrop);
        et_bookingdate = shippingDialog.findViewById(R.id.bboking);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        ib_history.setVisibility(View.GONE);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        iv_booking = shippingDialog.findViewById(R.id.iv_clear_bookingdate);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        edit_srname = shippingDialog.findViewById(R.id.srcuname);
        edit_srservicetype = shippingDialog.findViewById(R.id.serivcetype);
        edit_srmodel = shippingDialog.findViewById(R.id.srmodel);
        edit_serviceduedate = shippingDialog.findViewById(R.id.srserduefrom);
        edit_Bookkingno = shippingDialog.findViewById(R.id.bookingno);
        edit_pickdrop = shippingDialog.findViewById(R.id.pickdropbalance);
        EditText search = shippingDialog.findViewById(R.id.step2search);
        updatelayout = shippingDialog.findViewById(R.id.updatedetailslayout);
        remarks = shippingDialog.findViewById(R.id.remarks);
        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);
        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(MissedCallActivity_SR.this, "masterID " + arrayList_matserId.get(position), Toast.LENGTH_SHORT).show();

                SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(position), Context.MODE_PRIVATE);
                historyID = chkfss.getString(arrayList_matserId.get(position), "");

                if (et_bookingdate.getText().length() > 0) {
                    updatebookingdate(token, historyID, bookingdate_send, string_masterId);
                }
                if (et_nextfollowupdate.getText().length() > 0) {
                    updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.SRTAG), string_masterId);
                }
                if (notcomingreason != null) {
                    updatenotcomingReason(token, historyID, notcomingreason, string_masterId);
                }
                if (customer_reply != null) {
                    updatecustomerreply(token, historyID, customer_reply);
                }

                if (contacted_status != null) {
                    updatecontatctstatus(token, historyID, contacted_status, getString(R.string.SRTAG));
                }

                if (pickanddrop != null) {
                    updatepickanddrop(token, historyID, pickanddrop);
                }

                if (remarks.getText().length() > 0) {
                    updateremark(token, historyID, remarks.getText().toString(), getString(R.string.SRTAG), string_masterId);
                }

                shippingDialog.dismiss();

            }
        });

        et_bookingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sp_notcomingreason.getSelectedItemPosition() == 0 && et_nextfollowupdate.getText().toString().length() == 0) {

                    dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                    if (dateTimeFragment == null) {
                        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                getString(R.string.label_datetime_dialog),
                                getString(android.R.string.ok),
                                getString(android.R.string.cancel),
                                getString(R.string.clean) // Optional
                        );
                    }

                    // Optionally define a timezone
                    dateTimeFragment.setTimeZone(TimeZone.getDefault());

                    // Init format


                    final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", java.util.Locale.getDefault());
                    final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());


                    // Assign unmodifiable values
                    dateTimeFragment.set24HoursMode(true);
                    dateTimeFragment.setHighlightAMPMSelection(false);


                    // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                    // Define new day and month format
                    try {
                        dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                    } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                        e.printStackTrace();
                    }

                    // Set listener for date
                    // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                        @Override
                        public void onPositiveButtonClick(Date date) {
                            et_bookingdate.setText(myDateFormat.format(date));
                            bookingdate_send = myDateFormat1.format(date);


                            if (et_bookingdate.getText().length() != 0) {
                                contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("Contacted"));
                            }

                        }

                        @Override
                        public void onNegativeButtonClick(Date date) {
                            // Do nothing
                        }

                        @Override
                        public void onNeutralButtonClick(Date date) {
                            // Optional if neutral button does'nt exists
                            et_bookingdate.setText("");
                        }
                    });
                    dateTimeFragment.startAtCalendarView();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    dateTimeFragment.setMinimumDateTime(cal.getTime());
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, 1);
                    dateTimeFragment.setMaximumDateTime(cal.getTime());
                    dateTimeFragment.setDefaultDateTime(new Date());
                    // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                    dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);


                } else if (et_nextfollowupdate.getText().toString().length() > 0) {

                    Toasty.warning(MissedCallActivity_SR.this, "First Clear NextFollowup Date", Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                }
            }


        });
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bookingdate.getText().length() == 0 && sp_notcomingreason.getSelectedItemPosition()==0) {
                    dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                    if (dateTimeFragment == null) {
                        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                getString(R.string.label_datetime_dialog),
                                getString(android.R.string.ok),
                                getString(android.R.string.cancel),
                                getString(R.string.clean) // Optional
                        );
                    }

                    // Optionally define a timezone
                    dateTimeFragment.setTimeZone(TimeZone.getDefault());

                    // Init format


                    final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss", java.util.Locale.getDefault());
                    final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.getDefault());

                    // Assign unmodifiable values
                    dateTimeFragment.set24HoursMode(true);
                    dateTimeFragment.setHighlightAMPMSelection(false);


                    // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                    // Define new day and month format
                    try {
                        dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                    } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                        e.printStackTrace();
                    }

                    // Set listener for date
                    // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                        @Override
                        public void onPositiveButtonClick(Date date) {
                            et_nextfollowupdate.setText(myDateFormat.format(date));
                            nextfollowupdate_send = myDateFormat1.format(date);
                            if (et_nextfollowupdate.getText().length() != 0) {
                                customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition("Call Back"));
                            }
                        }

                        @Override
                        public void onNegativeButtonClick(Date date) {
                            // Do nothing
                        }

                        @Override
                        public void onNeutralButtonClick(Date date) {
                            // Optional if neutral button does'nt exists
                            et_nextfollowupdate.setText("");
                        }
                    });
                    dateTimeFragment.startAtCalendarView();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    dateTimeFragment.setMinimumDateTime(cal.getTime());
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, 1);
                    dateTimeFragment.setMaximumDateTime(cal.getTime());
                    dateTimeFragment.setDefaultDateTime(new Date());
                    // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                    dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

                } else if(et_bookingdate.getText().toString().length()>0){

                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Booking Date", Toast.LENGTH_SHORT).show();
                }else
                {
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                }

            }


        });

        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        updatelayout.setVisibility(View.GONE);


        search.setVisibility(View.VISIBLE);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {

                    searchdata(s.toString());
                    // Popup to Set Text the Values


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

     /*   submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences chkfss  = getSharedPreferences(master_ID, Context.MODE_PRIVATE);
                historyID= chkfss.getString(master_ID,"");

                if(et_bookingdate.getText().length() >0) {
                    updatebookingdate(token, historyID, et_bookingdate.getText().toString(),string_masterId);
                }
                if(et_nextfollowupdate.getText().length() >0) {
                    updatefollowupdate(token, historyID, et_nextfollowupdate.getText().toString(),getString(R.string.SRTAG),string_masterId);
                }
                if(notcomingreason != null)
                {
                    updatenotcomingReason(token,historyID,notcomingreason,string_masterId);
                }
                if(customer_reply != null)
                {
                    updatecustomerreply(token,historyID,customer_reply);
                }

                if(contacted_status != null)
                {
                    updatecontatctstatus(token,historyID,contacted_status,getString(R.string.SRTAG));
                }

                if(pickanddrop != null)
                {
                    updatepickanddrop(token,historyID,pickanddrop);
                }

                if(remarks.getText().length()>0)
                {
                    updateremark(token,historyID,remarks.getText().toString(),getString(R.string.SRTAG),string_masterId);
                }

                shippingDialog.dismiss();

            }
        });*/
        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });
        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });
        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });
        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });
        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });
        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });
        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });


        contactArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, contactStatuslist[0]);
        contactstatus.setAdapter(contactArrayAdapter);
        contactstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    contacted_status = contactstatus.getSelectedItem().toString();




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        customerArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, customerreplylist[0]);
        customerreply.setAdapter(customerArrayAdapter);
        customerreply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    customer_reply = customerreply.getSelectedItem().toString();

                } else {
                    customer_reply = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        notcomingreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, notcomingreasonlist[0]);
        sp_notcomingreason.setAdapter(notcomingreasonArrayAdapter);
        sp_notcomingreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (et_nextfollowupdate.getText().toString().equals("") && et_bookingdate.getText().toString().equals("")) {

                    if (position != 0) {
                        notcomingreason = sp_notcomingreason.getSelectedItem().toString();

                    } else {
                        notcomingreason = null;
                    }
                } else if (!et_nextfollowupdate.getText().toString().equals("")) {

                    try {
                        sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition("NotComing Reason"));

                    } catch (NullPointerException e) {

                    }
                    sp_notcomingreason.setSelection(0,true);
                    Toasty.warning(MissedCallActivity_SR.this, "First Clear Next Followup Date", Toast.LENGTH_SHORT).show();
                } else if (!et_bookingdate.getText().toString().equals("")) {
                    try {
                        sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition("NotComing Reason"));

                    } catch (NullPointerException e) {

                    }
                    sp_notcomingreason.setSelection(0,true);

                    Toasty.warning(MissedCallActivity_SR.this, "First clear Booking Date", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_pickanddrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    pickanddrop = sp_pickanddrop.getSelectedItem().toString();

                } else {
                    pickanddrop = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    //Get SMS Template
    public void getsmstemplate(String token, String templatetype) {
        templatelist.clear();
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    @Override
    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {

            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                    startActivity(sendMsg);
                    MissedCallActivity_SR.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        } else if (textmesasageclicked == true) {

            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            startActivity(intent);
            MissedCallActivity_SR.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }

    @Override
    public void holdCall(int position, String contatcnumber) {
        OngoingCall.call.hold();
    }

    @Override
    public void unHoldCall(int position, String contatcnumber) {
        OngoingCall.call.unhold();

    }



    /*------------------------------------------------------------------------------------------------------------------------*/


    /*---------------------------------------------------Call Recording and Upload Code----------------------------------------------------------------*/
    //Recording Start Code
    public void startRecording(String number) throws IOException {
        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            audiofile = File.createTempFile(number, ".mp3", dir);
            recorder = new MediaRecorder();
            am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_IN_CALL);
            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(1411200);
            recorder.setAudioChannels(1);
            recorder.setAudioSamplingRate(88200);

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
                            am.setMode(AudioManager.MODE_IN_CALL);
//                            if (!am.isSpeakerphoneOn())
//                                am.setSpeakerphoneOn(true);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!am.isWiredHeadsetOn()) {
                thread.start();
            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (IOException e) {
                e.printStackTrace();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Stop  Recording Function
    public void stopRecording(String id) {

//        CommonVariables.recorder.stop();
//        CommonVariables.recorder.reset();
//        CommonVariables.recorder.release();
//        try {
//            if (am != null) {
//                am.setMode(AudioManager.MODE_NORMAL);
//                am.setStreamVolume(AudioManager.MODE_IN_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
//                recorder.stop();
//                recorder.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //after stopping the recorder, create the sound file and add it to media library.
        CommonVariables.wavRecorder.stopRecording();

       if (CommonVariables.audiofile != null) UploadCallRecording(token, CommonVariables.dealercode, historyID, getString(R.string.SRTAG));

    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String dealercode, String historyid, String calltype) {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + CommonVariables.audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/wav");
        values.put(MediaStore.Audio.Media.DATA, CommonVariables.audiofile.getAbsolutePath());
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        selectedPath = getPath(newUri);
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
        File files = new File(selectedPath);
        RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
        RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
        RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/wav");
        RequestBody Recording = RequestBody.create(MediaType.parse("audio/wav"), files);
        RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
        MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording", files.getName(), Recording);
        apiController.UploadCallRecording(token, Dealer_Code, History_id, Content_type, Recording_file, Call_type);
    }

    // Get Recording File Path
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /*---------------------------------------------------------------------------------------------------------------*/


    public void clearcolorsharedeprefances() {
        if (arrayList_matserId.size() > 0) {
            for (int k = 0; k < arrayList_matserId.size(); k++) {
                SharedPreferences sharedPreferencesk = getSharedPreferences(arrayList_matserId.get(k), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesk.edit();
                editor.clear();
                editor.commit();


            }
        }
    }


    /*------------------------------------------------------------------------------------------------------------------------*/


    public void searchdata(String contactnumber) {
        params= new HashMap<>();
        params.put("MobileNo", "" + contactnumber);
        apiController.getSRIncomingData(token, params);

    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        NewDateFormat="";


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String newdate=null;

        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newdate;

    }


    public void setIndex(int number,boolean longpress)
    {

        position=number;
        isLongPress=longpress;

        if(indexs != null)indexs.setText(""+number);
        if(indexs2 != null)indexs2.setText(""+number);
    }


    //load more Service reminder data
    public void moreData() {

        ////Log.e("MoreClick",offset);
        customWaitingDialog.show();
        if (misseddatalist.size() >= 10) getMissedData(token,"SR",offset);
        morebtnclicked = true;
        CommonVariables.morecount = misseddatalist.size();
        adapter_missedCall.updateList(misseddatalist);
        scrollmovepostion(Integer.parseInt(offset) + 1);

        morebtncliked = true;
        adapter_missedCall.updateList(misseddatalist);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (CommonVariables.morecount < misseddatalist.size()) {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }

            @Override
            public void onFinish() {
                if (CommonVariables.morecount < misseddatalist.size()) {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }
        }.start();
    }
}