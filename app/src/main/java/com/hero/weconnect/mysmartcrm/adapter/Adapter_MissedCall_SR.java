package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.MissedCallActivity_SR;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_MissedCall_SR extends RecyclerView.Adapter<Adapter_MissedCall_SR.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    CustomButtonListener customListner;
    Adapter_MissedCall_SR adapter;
    ArrayList<MissedDataModel_SR.DataBean> misseddatalist = new ArrayList<>();
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    private Context context;
    CountDownTimer countDownTimer;
    int size=0;



    public Adapter_MissedCall_SR(Context context, CustomButtonListener customButtonListener, ArrayList<MissedDataModel_SR.DataBean> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.misseddatalist = list;

    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.sr_data_layout, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       // holder.setIsRecyclable(true);



        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);
        if (misseddatalist.size() > 0) {
            if (position == misseddatalist.size()) {
                holder.morebtn1.setVisibility(View.VISIBLE );
                holder.main_ll.setVisibility(View.GONE);

            }
            else
                {


                MissedDataModel_SR.DataBean dataBean = misseddatalist.get(position);
                    holder.morebtn1.setVisibility(View.GONE);
                    holder.main_ll.setVisibility(View.VISIBLE);
                    int sno = position + 1;
                final String idd = String.valueOf(dataBean.getId());

                holder.srno.setText("" + sno + ".");
                holder.customer_Name.setText("" + dataBean.getCusotmer_first_name() + " " + dataBean.getCustomer_last_name().toUpperCase());
                holder.registration_number.setText("" + dataBean.getRegistration_number().toUpperCase());
                holder.customer_contact.setText("" + dataBean.getMobileno_missed().toUpperCase());
                holder.model.setText("" + dataBean.getModel().toUpperCase());
                holder.servicetype.setText("" + dataBean.getServiceupdate().toUpperCase());
                holder.serviceduedate.setText("" + dataBean.getNext_service_due_date().toUpperCase().replaceAll("T00:00:00", ""));
                holder.calledstatus.setText("");

                countDownTimer =new CountDownTimer(1000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {


                        final String idssss = common_calling_color_sharedPreferances.getmatserid();
                        final String call = common_calling_color_sharedPreferances.getcallcurrentstatus();
                        SharedPreferences chkfsss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String status = chkfsss.getString("Status", "");
                        final String status1 = chkfsss.getString("Status1", "");
                        SharedPreferences chkfss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String icca = chkfss.getString("call", "");

                        if (dataBean.getCusotmer_first_name() != null && !dataBean.getCusotmer_first_name().equals("")) {

                            //Toast.makeText(context, "221 "+String.valueOf(position), Toast.LENGTH_SHORT).show();


                            if (dataBean.getId().equals(idssss) && call.equals("s")) {
                                holder.calledstatus.setText("Ringing");
                                pos = position;
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                holder.tv_hold.setVisibility(View.VISIBLE);
                            } else if (dataBean.getId().equals(idssss) && icca.equals("CALL DONE") && call.equals("s")) {
                                holder.calledstatus.setText("Ringing");
                                pos = position;
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                holder.tv_hold.setVisibility(View.VISIBLE);

                            } else if (dataBean.getId().equals(idssss) && call.equals("d") && icca.equals("BUSY")) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                                holder.tv_hold.setVisibility(View.GONE);
                                holder.tv_unhold.setVisibility(View.GONE);


                            } else if ((icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_hold.setVisibility(View.GONE);
                                holder.tv_unhold.setVisibility(View.GONE);


                            } else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_hold.setVisibility(View.GONE);
                                holder.tv_unhold.setVisibility(View.GONE);


                                if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                    holder.tv_hold.setVisibility(View.GONE);
                                    holder.tv_unhold.setVisibility(View.GONE);

                                } else {
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                    holder.calledstatus.setText("CALL DONE");
                                    holder.tv_hold.setVisibility(View.GONE);
                                    holder.tv_unhold.setVisibility(View.GONE);


                                }
                            } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_hold.setVisibility(View.GONE);
                                holder.tv_unhold.setVisibility(View.GONE);


                            } else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_unhold.setVisibility(View.GONE);
                                holder.tv_hold.setVisibility(View.GONE);


                            } else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_unhold.setVisibility(View.GONE);
                                holder.tv_hold.setVisibility(View.GONE);


                            } else {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                                holder.tv_hold.setVisibility(View.GONE);
                                holder.tv_unhold.setVisibility(View.GONE);


                            }
                            holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendWhatsappMessage(position, "" + dataBean.getMobileno_missed());
                                }
                            });
                            holder.textmessage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendtextmessage(position, "" + dataBean.getMobileno_missed());
                                }
                            });
                            holder.main_ll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    String customername = "" + dataBean.getCusotmer_first_name() + " " + dataBean.getCustomer_last_name().toUpperCase();
                                    customListner.getCustomerDetails(position, dataBean.getId(), customername, dataBean.getServiceupdate().toUpperCase(),
                                            dataBean.getModel().toUpperCase(), dataBean.getNext_service_due_date().toUpperCase().replaceAll("T00:00:00", ""), dataBean.getLs_balance().toUpperCase(), dataBean.getMobileno_missed());
                                }
                            });
//                            holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
//                                @RequiresApi(api = Build.VERSION_CODES.M)
//                                @Override
//                                public boolean onLongClick(View v) {
//                                    common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
//
//
//                                    String uid = UUID.randomUUID().toString();
//
//                                    SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
//                                    SharedPreferences.Editor edits = prefs.edit();
//                                    edits.putString(dataBean.getId(), uid);
//                                    edits.commit();
//
//                                    if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
//                                        Toasty.warning(context, "Call is Currently Onging", Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {
//
//                                            // Create the Uri from phoneNumberInput
//                                            Uri uri = Uri.parse("tel:" + CommonVariables.SR_Testingnumber);
//
//                                            // Start call to the number in input
//                                            context.startActivity(new Intent(Intent.ACTION_CALL, uri));
//                                            holder.calledstatus.setText("Ringing");
//                                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
//                                            common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
//
//
//                                            common_calling_color_sharedPreferances.setcallcurrentstatus("s");
//
//
//                                        } else {
//                                            // Request permission to call
//                                            ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
//                                        }
//                                    }
//                                    return false;
//                                }
//                            });


                            holder.main_ll.setOnLongClickListener(new View.OnLongClickListener()
                            {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public boolean onLongClick(View v) {

                                    CommonVariables.Is_LongPress = true;
                                    common_calling_color_sharedPreferances.setPosition(String.valueOf(position));

                                    //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                                    String uid = UUID.randomUUID().toString();

                                    SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString(dataBean.getId(), uid);
                                    edits.putString("longpress", uid);
                                    edits.commit();

                                    //Log.e("MasterIdAdapter","Adapter"+dataBean.getId());

                                    if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                                        Toasty.warning(context, "Call is Currently Ongoing", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                            // Create the Uri from phoneNumberInput
                                            Uri uri = Uri.parse("tel:" + dataBean.getCusotmercontact());
                                            //Uri uri = Uri.parse("tel:" + CommonVariables.SR_Testingnumber);

                                            // Start call to the number in input
                                            context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                            holder.calledstatus.setText("Ringing");
                                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                            common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                            ((S_R_Activity)context).setIndex(position+1,true);


                                            common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                                        } else {
                                            // Request permission to call
                                            ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                                        }
                                    }
                                    return false;
                                }
                            });

                            holder.historyybtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.getHistoryDetails(position);
                                }
                            });


                        }
                        else
                            {

                            holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendWhatsappMessage(position, "" + dataBean.getMobileno_missed());
                                }
                            });
                            holder.textmessage.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendtextmessage(position, "" + dataBean.getMobileno_missed());
                                }
                            });

                            holder.main_ll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    customListner.getMissedData(position);
                                }
                            });

                                holder.main_ll.setOnLongClickListener(new View.OnLongClickListener()
                                {
                                    @RequiresApi(api = Build.VERSION_CODES.M)
                                    @Override
                                    public boolean onLongClick(View v) {

                                        CommonVariables.Is_LongPress = true;
                                        common_calling_color_sharedPreferances.setPosition(String.valueOf(position));

                                        //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                                        String uid = UUID.randomUUID().toString();

                                        SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                                        SharedPreferences.Editor edits = prefs.edit();
                                        edits.putString(dataBean.getId(), uid);
                                        edits.putString("longpress", uid);
                                        edits.commit();

                                        //Log.e("MasterIdAdapter","Adapter"+dataBean.getId());

                                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                                            Toasty.warning(context, "Call is Currently Ongoing", Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                                // Create the Uri from phoneNumberInput
                                                Uri uri = Uri.parse("tel:" + dataBean.getCusotmercontact());
                                             //  Uri uri = Uri.parse("tel:" + CommonVariables.SR_Testingnumber);

                                                // Start call to the number in input
                                                context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                                holder.calledstatus.setText("Ringing");
                                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                                common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                               ((MissedCallActivity_SR)context).setIndex(position+1,true);


                                                common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                                            } else {
                                                // Request permission to call
                                                ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                                            }
                                        }
                                        return false;
                                    }
                                });

                            if (dataBean.getId().equals(idssss) && call.equals("s")) {
                                holder.calledstatus.setText("Ringing");
                                pos = position;
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            } else if (dataBean.getId().equals(idssss) && icca.equals("CALL DONE") && call.equals("s")) {
                                holder.calledstatus.setText("Ringing");
                                pos = position;
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            } else if (dataBean.getId().equals(idssss) && call.equals("d") && icca.equals("BUSY")) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                            } else if ((icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");

                            } else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");


                                if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));

                                } else {
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                    holder.calledstatus.setText("CALL DONE");
                                }
                            } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                            } else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {

                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                            } else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.calledstatus.setText("CALL DONE");
                            } else {
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                            }


                        }



                    }

                    @Override
                    public void onFinish() {

                        countDownTimer.cancel();

                    }
                };

                countDownTimer.start();



                holder.tv_hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.VISIBLE);
                        customListner.holdCall(position, dataBean.getCusotmercontact());
                    }

                });

                holder.tv_unhold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.VISIBLE);
                        holder.tv_unhold.setVisibility(View.GONE);
                        customListner.unHoldCall(position, dataBean.getCusotmercontact());
                    }

                });




            }

        }

        holder.morebtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (context instanceof MissedCallActivity_SR) {

                    ((MissedCallActivity_SR) context).moreData();

                }

            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        if (misseddatalist.size() >= 10) {
            if ((misseddatalist.size() % 10) == 0) {
                size = misseddatalist.size() + 1;

            } else {
                size = misseddatalist.size();

            }
        } else {
            size = misseddatalist.size();
        }


        return size;
    }

    public void setCustomButtonListner(CustomButtonListener listener) {
        this.customListner = listener;
    }

    public void updateList(ArrayList<MissedDataModel_SR.DataBean> list11) {
        misseddatalist = list11;
        notifyDataSetChanged();
    }

    public interface CustomButtonListener {
        void getCustomerDetails(int position, String mastertableid, String customername, String servicetype, String model, String serviceduefrom, String pickdropbalance, String contactnumber);

        void getHistoryDetails(int position);

        void getMoreDetails(int position);

        void sendWhatsappMessage(int position, String contactnumber);

        void sendtextmessage(int position, String contatcnumber);

        void getMissedData(int position);

        void holdCall(int position, String contatcnumber);

        void unHoldCall(int position, String contatcnumber);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout main_ll;
        // Variable Declare
        public TextView srno, idddata, tv_hold, tv_unhold, customer_Name, registration_number, customer_contact, model, servicetype, serviceduedate, calledstatus, morebtn1;
        public ImageView moredetailsbtn, historyybtn, whatsapp, textmessage;

        public ViewHolder(View rootView) {
            super(rootView);
            this.srno = rootView.findViewById(R.id.srno);
            this.idddata = rootView.findViewById(R.id.idddata);
            this.customer_Name = rootView.findViewById(R.id.customername);
            this.registration_number = rootView.findViewById(R.id.regno);
            this.customer_contact = rootView.findViewById(R.id.mobileno);
            this.model = rootView.findViewById(R.id.model);
            this.servicetype = rootView.findViewById(R.id.servicetype);
            this.serviceduedate = rootView.findViewById(R.id.serviceduedate);
            this.calledstatus = rootView.findViewById(R.id.cstatus);
            this.morebtn1 = rootView.findViewById(R.id.morebtn1);
            this.tv_hold = rootView.findViewById(R.id.tv_hold);
            this.tv_unhold = rootView.findViewById(R.id.tv_unhold);
            this.moredetailsbtn = rootView.findViewById(R.id.moredetailsbtn);
            this.historyybtn = rootView.findViewById(R.id.historyybtn);
            this.whatsapp = rootView.findViewById(R.id.whatsapp);
            this.textmessage = rootView.findViewById(R.id.textmessage);
            main_ll = rootView.findViewById(R.id.main_ll);


        }
    }
}