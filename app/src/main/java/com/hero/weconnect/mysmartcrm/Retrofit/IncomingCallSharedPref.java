package com.hero.weconnect.mysmartcrm.Retrofit;


import android.content.Context;
import android.content.SharedPreferences;

public class IncomingCallSharedPref {
    private Context mContext;
    private SharedPreferences.Editor editor, editorpackages;
    private SharedPreferences sharedPreferences, sharedPrefereceposition;

    public IncomingCallSharedPref(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("incoming", Context.MODE_PRIVATE);
        sharedPreferences = mContext.getSharedPreferences("position", Context.MODE_PRIVATE);

    }

    public SharedPreferences getIncomingCallSharedPrefData() {
        return sharedPreferences;

    }


    public void setPositionLongPress(String position) {
        editor = sharedPrefereceposition.edit();
        editor.putString("position", position);
        editor.commit();
    }

    public String getPositionLongPressed() {
        String position;

        if (sharedPrefereceposition.getString("position", "") != null) {
            position = sharedPrefereceposition.getString("position", "");
        } else {
            position = "";
        }

        return position;
    }


    public void setpositionAllClear() {
        editor = sharedPrefereceposition.edit();
        editor.clear();
        editor.commit();

    }


    public void setAllClear() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }


}
