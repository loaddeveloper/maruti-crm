package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class GetTamplateListModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"id":"810be3c6-b7c7-42e2-859d-55e21c073fa1","template_name":"SAaaaa","template_body":"aaaa "},{"id":"e01d25e7-5ebd-4274-8037-fb38f84fa77e","template_name":"SALES-FOLLOW-UP","template_body":"Hello Sales Walo Sales Down Ja Rahi Sales Lao "}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * id : 810be3c6-b7c7-42e2-859d-55e21c073fa1
         * template_name : SAaaaa
         * template_body : aaaa
         */

        private String id;
        private String template_name;
        private String template_body;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTemplate_name() {
            return template_name;
        }

        public void setTemplate_name(String template_name) {
            this.template_name = template_name;
        }

        public String getTemplate_body() {
            return template_body;
        }

        public void setTemplate_body(String template_body) {
            this.template_body = template_body;
        }
    }
}
