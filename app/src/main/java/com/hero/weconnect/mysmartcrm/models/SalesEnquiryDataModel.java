package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SalesEnquiryDataModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"id":"8dd05100-259a-4a56-8eeb-00f87dbeb5d8","enquirynumber":"10351-01-SENQ-0820-1062","enquiry_open_date":"2020-08-31 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-01T00:00:00","expected_date_purchase":"2020-09-10 00:00:00.0000000","cusotmer_first_and_last_name":"RAHUL KUMAR DANGI","mobilenumber":"7301145496","emailid":"","address":"GIDHAUR,BARTA,CHATRA,(JHARKHAND)","age":"26","gender":"M","model_interested_in":"GLAMOUR FI","exchange_required":"N","finance_required":"Y","dse_name":"SINGH REWAT","position_of_executive":"DSE","enquiry_id":"1-3B69D7Z9","dealer_name":"STAR AUTOMOBILES","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-752496684","existing_vehicle":"Two Wheeler","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"News Paper","opinion_leader":"","financier":"ALLAHABAD BANK, ANUPPUR  BRANCH","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":2,"id":"aef82f36-8fd8-4abb-bb34-157433f8c7b4","enquirynumber":"10877-01-SENQ-0920-1050","enquiry_open_date":"2020-09-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T00:00:00","expected_date_purchase":"2020-09-12 00:00:00.0000000","cusotmer_first_and_last_name":"KUMAR DURAI","mobilenumber":"9879879877","emailid":"","address":"PALAKKAD,PALAKKAD,PALAKKAD,(KERALA)","age":"31","gender":"M","model_interested_in":"DESTINI 125","exchange_required":"N","finance_required":"N","dse_name":"Singla Madan","position_of_executive":"DSE","enquiry_id":"2-3B69DQ43","dealer_name":"SINGLA AUTOMOBILES","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-2485351","existing_vehicle":"Two Wheeler","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"News Paper","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":3,"id":"8a52a684-ede6-42ca-9d1a-20a0a947150b","enquirynumber":"10375-01-SENQ-0820-4339","enquiry_open_date":"2020-08-25 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-08-28T00:00:00","expected_date_purchase":"2020-08-28 05:30:00.0000000","cusotmer_first_and_last_name":"TEST_2508 TEST_2508","mobilenumber":"9090908980","emailid":"","address":"","age":"","gender":"","model_interested_in":"MAESTRO EDGE","exchange_required":"N","finance_required":"N","dse_name":"GOEL ANIL","position_of_executive":"DSE","enquiry_id":"1-3B3GIW1K","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-65430585","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Email","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":4,"id":"7a0bcbfd-bda7-4c5e-bf9d-2f4aded9a6c8","enquirynumber":"10000-02-SENQ-0820-203","enquiry_open_date":"2020-08-19 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-01T00:00:00","expected_date_purchase":"2020-08-31 05:30:00.0000000","cusotmer_first_and_last_name":"VCV DFFDSFDS","mobilenumber":"9832323232","emailid":"","address":"","age":"","gender":"","model_interested_in":"PASSION PRO","exchange_required":"N","finance_required":"N","dse_name":"PURI ROHIT","position_of_executive":"DSE","enquiry_id":"1-3B3GHHJF","dealer_name":"JASWANT MOTORS","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-889305657","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":5,"id":"8b0df510-92a9-4577-9fff-4818894af703","enquirynumber":"10000-02-SENQ-0820-200","enquiry_open_date":"2020-08-14 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-01T00:00:00","expected_date_purchase":"2020-09-01 05:30:00.0000000","cusotmer_first_and_last_name":". NISHTHA D","mobilenumber":"7823546623","emailid":"","address":"","age":"","gender":"","model_interested_in":"XTREME 160R","exchange_required":"N","finance_required":"N","dse_name":". .","position_of_executive":"DSE","enquiry_id":"1-3B3GF8ZV","dealer_name":"JASWANT MOTORS","last_follow_up_date_if_any":"","enquiry_comments":" Customer has asked about the Sales Promotion /campaigns","dse_employee_id":"1-259234196701","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"sbiyono","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":6,"id":"c5405438-2d58-48b8-a230-5d487c573edb","enquirynumber":"22957-03-SENQ-0818-610","enquiry_open_date":"2018-08-14 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-08-29T00:00:00","expected_date_purchase":"2021-07-24 00:00:00.0000000","cusotmer_first_and_last_name":"SADA SUNA","mobilenumber":"8018700254","emailid":"","address":"UMARKOTE,UMARKOTE,NABARANGAPUR,(ODISHA)","age":"30","gender":"M","model_interested_in":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"MAJUMDAR SUJIT","position_of_executive":"DSE","enquiry_id":"1-2P2Y9TNJ","dealer_name":"BHARAT SALES","last_follow_up_date_if_any":"2019-03-28 18:26:42.0000000","enquiry_comments":"","dse_employee_id":"1-176694334412","existing_vehicle":"Two Wheeler","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"Hoarding","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":7,"id":"15487684-6e59-4d95-9177-81fb35051f4c","enquirynumber":"10375-01-SENQ-0820-4341","enquiry_open_date":"2020-08-25 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-08-28T00:00:00","expected_date_purchase":"2020-08-28 05:30:00.0000000","cusotmer_first_and_last_name":"TEST_25AUG1 TEST_25AUG1","mobilenumber":"9876987654","emailid":"","address":"","age":"","gender":"","model_interested_in":"KARIZMA ZMR","exchange_required":"N","finance_required":"N","dse_name":"GOEL ANIL","position_of_executive":"DSE","enquiry_id":"1-3B3PI3BB","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-65430585","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Financer","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":8,"id":"54ace289-4afc-44f4-b759-89c09b62198a","enquirynumber":"10351-01-SENQ-0820-1057","enquiry_open_date":"2020-08-31 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-01T00:00:00","expected_date_purchase":"2020-09-10 00:00:00.0000000","cusotmer_first_and_last_name":"RAJESH KUMAR","mobilenumber":"9897223399","emailid":"na","address":"DADRI,DADRI,GAUTAM BUDDHA NAGAR,(UP)","age":"22","gender":"M","model_interested_in":"PLEASURE+","exchange_required":"N","finance_required":"Y","dse_name":"SINGH REWAT","position_of_executive":"DSE","enquiry_id":"1-3B69D1VJ","dealer_name":"STAR AUTOMOBILES","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"1-752496684","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"News Paper","opinion_leader":"","financier":"ALLAHABAD BANK, BRANCH- BELA (SATNA)","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":9,"id":"e432e330-1000-4819-ac59-8a945fdccd09","enquirynumber":"11935-01-SENQ-0219-3941","enquiry_open_date":"2019-02-27 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-08-28T05:30:00","expected_date_purchase":"2022-02-08 05:30:00.0000000","cusotmer_first_and_last_name":"KUSUM RAI ..","mobilenumber":"9664994653","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-355AL8QR","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""},{"sno":10,"id":"2d6e28c5-5605-4292-a74e-95afc24f3131","enquirynumber":"11935-01-SENQ-0119-3394","enquiry_open_date":"2019-01-02 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2020-09-03T05:30:00","expected_date_purchase":"2022-12-11 05:30:00.0000000","cusotmer_first_and_last_name":"SAMIM KHAN","mobilenumber":"9969996232","emailid":"","address":"VARANASI,VARANASI,VARANASI,(UP)","age":"28","gender":"M","model_interested_in":"PASSION PRO","exchange_required":"N","finance_required":"Y","dse_name":"AWASTHI HIMANSHU DHAR","position_of_executive":"DSE","enquiry_id":"1-30TCJ6MT","dealer_name":"VISHAL AUTO AGENCIES","last_follow_up_date_if_any":"","enquiry_comments":"HIMANSHU11935~","dse_employee_id":"11935-02-EMP-7","existing_vehicle":"First Time Buyer","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":"","followup_status":"","current_next_followup_date":"1900-01-01T00:00:00","expected_date_of_purchase":"1900-01-01T00:00:00","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":""}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * id : 8dd05100-259a-4a56-8eeb-00f87dbeb5d8
         * enquirynumber : 10351-01-SENQ-0820-1062
         * enquiry_open_date : 2020-08-31 00:00:00.0000000
         * enquiry_status : Open
         * next_followup_date : 2020-09-01T00:00:00
         * expected_date_purchase : 2020-09-10 00:00:00.0000000
         * cusotmer_first_and_last_name : RAHUL KUMAR DANGI
         * mobilenumber : 7301145496
         * emailid :
         * address : GIDHAUR,BARTA,CHATRA,(JHARKHAND)
         * age : 26
         * gender : M
         * model_interested_in : GLAMOUR FI
         * exchange_required : N
         * finance_required : Y
         * dse_name : SINGH REWAT
         * position_of_executive : DSE
         * enquiry_id : 1-3B69D7Z9
         * dealer_name : STAR AUTOMOBILES
         * last_follow_up_date_if_any :
         * enquiry_comments :
         * dse_employee_id : 1-752496684
         * existing_vehicle : Two Wheeler
         * test_ride_required :
         * test_ride_required_time :
         * test_ride_taken :
         * test_ride_taken_time :
         * enquiry_source : Walk-In
         * awareness_source : News Paper
         * opinion_leader :
         * financier : ALLAHABAD BANK, ANUPPUR  BRANCH
         * calledstatus :
         * followup_status :
         * current_next_followup_date : 1900-01-01T00:00:00
         * expected_date_of_purchase : 1900-01-01T00:00:00
         * closurereason :
         * closure_sub_reason :
         * make :
         * model :
         * remark :
         * DataCount :
         * Title :
         */

        private int sno;
        private String id;
        private String enquirynumber;
        private String enquiry_open_date;
        private String enquiry_status;
        private String next_followup_date;
        private String expected_date_purchase;
        private String cusotmer_first_and_last_name;
        private String mobilenumber;
        private String emailid;
        private String address;
        private String age;
        private String gender;
        private String model_interested_in;
        private String exchange_required;
        private String finance_required;
        private String dse_name;
        private String position_of_executive;
        private String enquiry_id;
        private String dealer_name;
        private String last_follow_up_date_if_any;
        private String enquiry_comments;
        private String dse_employee_id;
        private String existing_vehicle;
        private String test_ride_required;
        private String test_ride_required_time;
        private String test_ride_taken;
        private String test_ride_taken_time;
        private String enquiry_source;
        private String awareness_source;
        private String opinion_leader;
        private String financier;
        private String calledstatus;
        private String followup_status;
        private String current_next_followup_date;
        private String expected_date_of_purchase;
        private String closurereason;
        private String closure_sub_reason;
        private String make;
        private String model;
        private String remark;

        public String getDataCount() {
            return DataCount;
        }

        public void setDataCount(String dataCount) {
            DataCount = dataCount;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        private String DataCount;
        private String Title;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEnquirynumber() {
            return enquirynumber;
        }

        public void setEnquirynumber(String enquirynumber) {
            this.enquirynumber = enquirynumber;
        }

        public String getEnquiry_open_date() {
            return enquiry_open_date;
        }

        public void setEnquiry_open_date(String enquiry_open_date) {
            this.enquiry_open_date = enquiry_open_date;
        }

        public String getEnquiry_status() {
            return enquiry_status;
        }

        public void setEnquiry_status(String enquiry_status) {
            this.enquiry_status = enquiry_status;
        }

        public String getNext_followup_date() {
            return next_followup_date;
        }

        public void setNext_followup_date(String next_followup_date) {
            this.next_followup_date = next_followup_date;
        }

        public String getExpected_date_purchase() {
            return expected_date_purchase;
        }

        public void setExpected_date_purchase(String expected_date_purchase) {
            this.expected_date_purchase = expected_date_purchase;
        }

        public String getCusotmer_first_and_last_name() {
            return cusotmer_first_and_last_name;
        }

        public void setCusotmer_first_and_last_name(String cusotmer_first_and_last_name) {
            this.cusotmer_first_and_last_name = cusotmer_first_and_last_name;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getModel_interested_in() {
            return model_interested_in;
        }

        public void setModel_interested_in(String model_interested_in) {
            this.model_interested_in = model_interested_in;
        }

        public String getExchange_required() {
            return exchange_required;
        }

        public void setExchange_required(String exchange_required) {
            this.exchange_required = exchange_required;
        }

        public String getFinance_required() {
            return finance_required;
        }

        public void setFinance_required(String finance_required) {
            this.finance_required = finance_required;
        }

        public String getDse_name() {
            return dse_name;
        }

        public void setDse_name(String dse_name) {
            this.dse_name = dse_name;
        }

        public String getPosition_of_executive() {
            return position_of_executive;
        }

        public void setPosition_of_executive(String position_of_executive) {
            this.position_of_executive = position_of_executive;
        }

        public String getEnquiry_id() {
            return enquiry_id;
        }

        public void setEnquiry_id(String enquiry_id) {
            this.enquiry_id = enquiry_id;
        }

        public String getDealer_name() {
            return dealer_name;
        }

        public void setDealer_name(String dealer_name) {
            this.dealer_name = dealer_name;
        }

        public String getLast_follow_up_date_if_any() {
            return last_follow_up_date_if_any;
        }

        public void setLast_follow_up_date_if_any(String last_follow_up_date_if_any) {
            this.last_follow_up_date_if_any = last_follow_up_date_if_any;
        }

        public String getEnquiry_comments() {
            return enquiry_comments;
        }

        public void setEnquiry_comments(String enquiry_comments) {
            this.enquiry_comments = enquiry_comments;
        }

        public String getDse_employee_id() {
            return dse_employee_id;
        }

        public void setDse_employee_id(String dse_employee_id) {
            this.dse_employee_id = dse_employee_id;
        }

        public String getExisting_vehicle() {
            return existing_vehicle;
        }

        public void setExisting_vehicle(String existing_vehicle) {
            this.existing_vehicle = existing_vehicle;
        }

        public String getTest_ride_required() {
            return test_ride_required;
        }

        public void setTest_ride_required(String test_ride_required) {
            this.test_ride_required = test_ride_required;
        }

        public String getTest_ride_required_time() {
            return test_ride_required_time;
        }

        public void setTest_ride_required_time(String test_ride_required_time) {
            this.test_ride_required_time = test_ride_required_time;
        }

        public String getTest_ride_taken() {
            return test_ride_taken;
        }

        public void setTest_ride_taken(String test_ride_taken) {
            this.test_ride_taken = test_ride_taken;
        }

        public String getTest_ride_taken_time() {
            return test_ride_taken_time;
        }

        public void setTest_ride_taken_time(String test_ride_taken_time) {
            this.test_ride_taken_time = test_ride_taken_time;
        }

        public String getEnquiry_source() {
            return enquiry_source;
        }

        public void setEnquiry_source(String enquiry_source) {
            this.enquiry_source = enquiry_source;
        }

        public String getAwareness_source() {
            return awareness_source;
        }

        public void setAwareness_source(String awareness_source) {
            this.awareness_source = awareness_source;
        }

        public String getOpinion_leader() {
            return opinion_leader;
        }

        public void setOpinion_leader(String opinion_leader) {
            this.opinion_leader = opinion_leader;
        }

        public String getFinancier() {
            return financier;
        }

        public void setFinancier(String financier) {
            this.financier = financier;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getFollowup_status() {
            return followup_status;
        }

        public void setFollowup_status(String followup_status) {
            this.followup_status = followup_status;
        }

        public String getCurrent_next_followup_date() {
            return current_next_followup_date;
        }

        public void setCurrent_next_followup_date(String current_next_followup_date) {
            this.current_next_followup_date = current_next_followup_date;
        }

        public String getExpected_date_of_purchase() {
            return expected_date_of_purchase;
        }

        public void setExpected_date_of_purchase(String expected_date_of_purchase) {
            this.expected_date_of_purchase = expected_date_of_purchase;
        }

        public String getClosurereason() {
            return closurereason;
        }

        public void setClosurereason(String closurereason) {
            this.closurereason = closurereason;
        }

        public String getClosure_sub_reason() {
            return closure_sub_reason;
        }

        public void setClosure_sub_reason(String closure_sub_reason) {
            this.closure_sub_reason = closure_sub_reason;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
