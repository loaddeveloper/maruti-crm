package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class GetTemplateListModel extends SuperClassCastBean {

    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Id":"ee28d8da-0bb2-4e3a-951a-4eca3c18ffdd","Master_Account_Id":"5fa71cb4-43bf-4f47-9fc3-0156888e039c","SmsTemplate":"Happy Diwali","TemplateTag":"Diwali"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Id : ee28d8da-0bb2-4e3a-951a-4eca3c18ffdd
         * Master_Account_Id : 5fa71cb4-43bf-4f47-9fc3-0156888e039c
         * SmsTemplate : Happy Diwali
         * TemplateTag : Diwali
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("Master_Account_Id")
        private String MasterAccountId;
        @SerializedName("SmsTemplate")
        private String SmsTemplate;
        @SerializedName("TemplateTag")
        private String TemplateTag;

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public String getMasterAccountId() {
            return MasterAccountId;
        }

        public void setMasterAccountId(String MasterAccountId) {
            this.MasterAccountId = MasterAccountId;
        }

        public String getSmsTemplate() {
            return SmsTemplate;
        }

        public void setSmsTemplate(String SmsTemplate) {
            this.SmsTemplate = SmsTemplate;
        }

        public String getTemplateTag() {
            return TemplateTag;
        }

        public void setTemplateTag(String TemplateTag) {
            this.TemplateTag = TemplateTag;
        }
    }
}
