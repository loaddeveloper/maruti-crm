package com.hero.weconnect.mysmartcrm.CallUtils;

import android.annotation.TargetApi;
import android.os.Build;
import android.telecom.Call;
import android.telecom.InCallService;
import android.util.Log;

import com.hero.weconnect.mysmartcrm.receiver.PhoneStateReceiverNew;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;


@TargetApi(Build.VERSION_CODES.M)
public class CallService extends InCallService {
    private static ArrayList<Call> call2 = new ArrayList<Call>();
    private static Call call1;
   static String number;
   PhoneStateReceiverNew phoneStateReceiverNew;

    public static void discon1() {
        try {
            if (call2.size() > 0) {
                call1 = call2.get(0);
                call1.disconnect();
                call2.clear();
               // ////Log.e("Discon1", "Discon1" + call1);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        call2.clear();
    }

    public static void discon2() {
       // ////Log.e("call4", "diss" + OngoingCall.call4.size());
        try {
            if(OngoingCall.call4.size()>0)
            {
                OngoingCall.call4.get(0).disconnect();
            }



        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        OngoingCall.call4.clear();


    }


    public static void receiveCall() {
        // ////Log.e("call4", "diss" + OngoingCall.call4.size());
        try {
            if(OngoingCall.call4.size()>0)
            {
                if(OngoingCall.call4 != null)OngoingCall.call4.get(0).answer(0);
            }



        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        OngoingCall.call4.clear();
    }


    public static String getIncommingNumber()
    {
        return number;
    }

    public static void discon3() {
       // ////Log.e("hold", "hold" + call2.size());

        try {
            call1.hold();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        call2.clear();
    }

    @Override
    public void onCallAdded(Call call) {
        new OngoingCall().setCall(call);

        if (call.getDetails().getHandle() != null && call.getDetails().getHandle().toString().length() > 10) {
            number = call.getDetails().getHandle().toString().substring(call.getDetails().getHandle().toString().length() - 10);
            PhoneStateReceiverNew.start(number);

        } else {
            if (call.getDetails().getHandle() != null)  number = call.getDetails().getHandle().toString();
        }

        if (number != null) PhoneStateReceiverNew.start(number);


        //////Log.e("Discon1jjjjjj", "Discon1jay " + number);


        // Dashboard.start(getApplicationContext(),call);


        try {
            OngoingCall.call4.add(call);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // ////Log.e("Calling List", "calling" + OngoingCall.call4.size());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCallRemoved(Call call) {
        OngoingCall.call4.remove(call);
        OngoingCall.call4.clear();

       // PhoneStateReceiverNew.dailogdismmiss2();
    }
}




