package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SingleCustomerHistoryData extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"service_reminder_id":"638a1724-9939-473c-b11d-2a57e984be7d","contact_status":"Contacted","calledstatus":"CALL DONE","customerreply":"","servicebooking":"2020-10-02T12:28:50","servicerequest":"","tagged_sr_status":"","contact_changed":"","followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","id":"2b82bb7d-19a2-499d-93e3-410a52105af2","hc_res_fault_message":"An Open SR # 10375-03-RSRB-0920-8034 already exists for this Vehicle.(SBL-BPR-00131)"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * service_reminder_id : 638a1724-9939-473c-b11d-2a57e984be7d
         * contact_status : Contacted
         * calledstatus : CALL DONE
         * customerreply :
         * servicebooking : 2020-10-02T12:28:50
         * servicerequest :
         * tagged_sr_status :
         * contact_changed :
         * followup_date : 1900-01-01T00:00:00
         * not_coming_reason :
         * comments :
         * pickup_drop :
         * id : 2b82bb7d-19a2-499d-93e3-410a52105af2
         * hc_res_fault_message : An Open SR # 10375-03-RSRB-0920-8034 already exists for this Vehicle.(SBL-BPR-00131)
         */

        private String service_reminder_id;
        private String contact_status;
        private String calledstatus;
        private String customerreply;
        private String servicebooking;
        private String servicerequest;
        private String tagged_sr_status;
        private String contact_changed;
        private String followup_date;
        private String not_coming_reason;
        private String comments;
        private String pickup_drop;
        private String id;
        private String hc_res_fault_message;

        public String getService_reminder_id() {
            return service_reminder_id;
        }

        public void setService_reminder_id(String service_reminder_id) {
            this.service_reminder_id = service_reminder_id;
        }

        public String getContact_status() {
            return contact_status;
        }
        public void setContact_status(String contact_status) {
            this.contact_status = contact_status;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getCustomerreply() {
            return customerreply;
        }

        public void setCustomerreply(String customerreply) {
            this.customerreply = customerreply;
        }

        public String getServicebooking() {
            return servicebooking;
        }

        public void setServicebooking(String servicebooking) {
            this.servicebooking = servicebooking;
        }

        public String getServicerequest() {
            return servicerequest;
        }

        public void setServicerequest(String servicerequest) {
            this.servicerequest = servicerequest;
        }

        public String getTagged_sr_status() {
            return tagged_sr_status;
        }

        public void setTagged_sr_status(String tagged_sr_status) {
            this.tagged_sr_status = tagged_sr_status;
        }

        public String getContact_changed() {
            return contact_changed;
        }

        public void setContact_changed(String contact_changed) {
            this.contact_changed = contact_changed;
        }

        public String getFollowup_date() {
            return followup_date;
        }

        public void setFollowup_date(String followup_date) {
            this.followup_date = followup_date;
        }

        public String getNot_coming_reason() {
            return not_coming_reason;
        }

        public void setNot_coming_reason(String not_coming_reason) {
            this.not_coming_reason = not_coming_reason;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getPickup_drop() {
            return pickup_drop;
        }

        public void setPickup_drop(String pickup_drop) {
            this.pickup_drop = pickup_drop;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHc_res_fault_message() {
            return hc_res_fault_message;
        }

        public void setHc_res_fault_message(String hc_res_fault_message) {
            this.hc_res_fault_message = hc_res_fault_message;
        }
    }
}
