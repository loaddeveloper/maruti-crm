package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class CampaignModel extends SuperClassCastBean {


    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("Id")
        private String id;
        @SerializedName("Campaign_Name")
        private String campaignName;
        @SerializedName("BookingStartDate")
        private String bookingStartDate;
        @SerializedName("BookingEndDate")
        private String bookingEndDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCampaignName() {
            return campaignName;
        }

        public void setCampaignName(String campaignName) {
            this.campaignName = campaignName;
        }

        public String getBookingStartDate() {
            return bookingStartDate;
        }

        public void setBookingStartDate(String bookingStartDate) {
            this.bookingStartDate = bookingStartDate;
        }

        public String getBookingEndDate() {
            return bookingEndDate;
        }

        public void setBookingEndDate(String bookingEndDate) {
            this.bookingEndDate = bookingEndDate;
        }
    }
}

