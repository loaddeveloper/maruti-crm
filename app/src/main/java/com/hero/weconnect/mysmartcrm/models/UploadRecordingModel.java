package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

public class UploadRecordingModel extends SuperClassCastBean {

    /**
     * message : Updated
     * data : C:\inetpub\wwwroot\weconnect\DealerRecordings\SR-TRIAL-7-9-2020\A20848D1-B166-4F5C-A32A-0E9193B0FB62.m4a
     */

    private String message;
    private String data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
