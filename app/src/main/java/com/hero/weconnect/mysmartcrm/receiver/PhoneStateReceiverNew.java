package com.hero.weconnect.mysmartcrm.receiver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.MyApplication;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_HistoryIncomingdata;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Historydata;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Sales_BR_HistoryIncomingdata;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Sales_HistoryIncomingdata;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_Sales_Historydata;
import com.hero.weconnect.mysmartcrm.customfonts.TextView_Roboto_Medium;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.CustomerReplyModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_SalesModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_Service_ReminderModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.NotComingReasonModel;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.service.CustomFloatingViewService;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Context.TELECOM_SERVICE;


public class PhoneStateReceiverNew extends BroadcastReceiver implements ApiResponseListener, AdapterTemplateShow.CustomButtonListener {

    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    public  Dialog shippingDialog;
    public static String hid = "";
    public static boolean IncomingCalling_Started = false;
    boolean isShown = false;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    SharedPreferences prefssss;
    Handler handler;
    public Context contextw;
    public static Context contexts;
    boolean callpic = false;
    String callstarl,newState;
    SharedPreferences.Editor editss;
    String uid;
    AudioManager am;
    EditText incomingnumber;
  public static   boolean dialogshown = false;
    int sata = 0, counter = 0;
    ArrayList<Sales_HistoryModel.DataBean> salesReminderHistoryDataList = new ArrayList<>();
    Adapter_Sales_HistoryIncomingdata adapter_sales_historydata;
    boolean whatsappclicked = false, textmesasageclicked = false;
    ImageButton callpick, endcall;


    AdapterTemplateShow adapterSettingTemplet;
    ArrayList<SR_HistoryModel.DataBean> serviceReminderHistoryDataList = new ArrayList<>();
    Adapter_SR_HistoryIncomingdata adapter_sr_historydata;
    Adapter_Sales_BR_HistoryIncomingdata adapter_expected_sales_historydata;
    ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    Dialog Whatsapp_shippingDialog;
    TextView popupclose;
    File audiofile = null;
    String selectedPath = "", token = "", masterID = "";
    MediaRecorder recorder;
    Intent intentkk;
    Typeface tf;
    Button sr_btn, sales_btn;
    int Seconds, Minutes, MilliSeconds;
    String IncomingCallState;
    BroadcastReceiver incomingBroadCastReceiver;
    public Runnable runnable = new Runnable() {

        @SuppressLint("SetTextI18n")
        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            TIMERS.setText("" + Minutes + ":" + String.format("%02d", Seconds));

            handler.postDelayed(this, 0);
        }

    };
    TextView id, cuss_name, TIMERS;
    EditText remarks,edit_Bookkingno;
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    ApiController apiController;
    CustomDialog customDialog;
    HashMap<String, String> params;
    public String string_called_status = "", string_customername, string_masterId, string_mobileno;
    EditText et_bookingdate, et_nextfollowuptime, et_bookingtime, et_nextfollowupdate, et_numberSearch, expected_nextfollowupdate, expected_nextfollowuptime;
    CommonSharedPref commonSharedPref;
    Spinner followuptype, clouserreason_spinner, clousersubreason_spinner, make_spinner, model_spinner, sp_enquiry_status;
    ImageView iv_nextfollowup, iv_nextfollowuptime, iv_expectednextfollowupdate, iv_expectednextfollowuptime, iv_clouserreson;
    ArrayList<String>[] clouserreasonlist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] clousersubreasonlist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] makelist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] modellist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] followuplist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] followupdonelist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<SalesEnquiryDataModel.DataBean> salesenquirydatalist = new ArrayList<>();
    public ArrayList<ServiceReminderDataModel.DataBean> serviceReminderDataListsr = new ArrayList<>();
    ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    Incoming_Service_ReminderModel.DataBean dataBeanSR;
    Incoming_SalesModel.DataBean dataBeanSales;
    SingleCustomerHistoryData_Sales.DataBean SingledataBeanSales;
    TextView tv_customerName;
    public String offset = "0", indexposition, historyID = "", callstatus, followupstatus, closurereason, closuresubreason, make, string_model;
    ArrayAdapter<String> clouserreasonArrayAdapter, clousersubreasonArrayAdapter, makeArrayAdapter, modelArrayAdapter, followupArrayAdapter, followupdoneArrayAdapter;
    ImageView iv_booking, iv_bookingtime, iv_nextfollowupdate, iv_more, iv_history, iv_textmessage, iv_whatsapp;
    Spinner sp_notcomingreason, contactstatus, customerreply, sp_pickanddrop;
    public String notcomingreason = "NotComing Reason", pickanddrop = "", contacted_status = "", customer_reply = "", whatsapp_number = "",NewDateFormat = null;
    ArrayAdapter<String> contactArrayAdapter, customerArrayAdapter, notcomingreasonArrayAdapter;
    public CompositeDisposable disposables = new CompositeDisposable();
    private LinearLayout ll_clouserreson, ll_clousersubreason, ll_make,ll_model;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private LinearLayout servicelinear, reminderlinear, complaintcategorysubll, complaintstatusll, ll_update;
    private RecyclerView history_data_recyclerview, recyclerView_template;
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    CardView cv_nextfollowdate,cv_expecteddate;
    ArrayList<SingleCustomerHistoryData.DataBean> singledatalist = new ArrayList<>();
    ArrayList<SingleCustomerHistoryData_Sales.DataBean> singledatalist_sales = new ArrayList<>();


    public void  dailogdismmiss()
    {
        shippingDialog.dismiss();
    }



    public static String start(String number) {
        CommonVariables.incomingNumber = "";
        CommonVariables.incomingNumber = number;

        //if()


        return number;

    }

    public  void incomingdiaogshow() {

        if(!CommonVariables.dashboard)
        {

            if( dialogshown==true)
            {
                shippingDialog.dismiss();
                dialogshown=false;
            }
            else
            {
                dialogshown=true;
                shippingDialog.show();


            }






        }else
        {
            shippingDialog.dismiss();


            TelecomManager tm = (TelecomManager) contexts
                    .getSystemService(Context.TELECOM_SERVICE);

            if (tm == null) {
                // whether you want to handle this is up to you really
                throw new NullPointerException("tm == null");
            }else
            {
                tm.endCall();

            }



            CallService.discon2();
        }

    }




    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        intentkk = intent;
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);
        TelecomManager systemService = context.getSystemService(TelecomManager.class);
        contextw = context;
        contexts = context;

//
//        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
//                | PowerManager.ACQUIRE_CAUSES_WAKEUP
//                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
//        wakeLock.acquire();



        customDialog = new CustomDialog(contexts);
        commonSharedPref = new CommonSharedPref(contexts);
        apiController = new ApiController(this,contexts);


        if (systemService.getDefaultDialerPackage().equals(context.getPackageName())) {

            tf = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
            am = (AudioManager) contextw.getSystemService(Context.AUDIO_SERVICE);
            recorder = new MediaRecorder();

          /*  TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            telephonyManager.listen(new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String phoneNumber) {


                    super.onCallStateChanged(state, phoneNumber);
                }
            }, PhoneStateListener.LISTEN_CALL_STATE);*/

            if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    final String channelId = "default_floatingview_channel";
                    final String channelName = "Default Channel";
                    final NotificationChannel defaultChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_MIN);
                    final NotificationManager manager = (NotificationManager) contextw.getSystemService(NOTIFICATION_SERVICE);
                    if (manager != null) {
                        manager.createNotificationChannel(defaultChannel);
                    }
                }


                contextw.startService(new Intent(contextw, CustomFloatingViewService.class));

                if (commonSharedPref.getLoginData().getAccess_token()!=null)
                {
                    token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
                }




                apiController.getNotComingReason(token);
                apiController.getContactStatus(token);
                apiController.getCustomerReply(token);
                // Call Functions APIS for getting LOVS from Server
                apiController.getClosureReason(token);
                apiController.getFollowContatctStatusList(token);
                apiController.getFollowUpDoneList(token);

                CommonVariables.receivecall=false;
                callpic= false;
                CommonVariables.endcallclick= false;

                apiController.getMake(token);
                apiController.getContactStatus(token);
                shippingDialog = new Dialog(MyApplication.context);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.incoming_popup);
                CommonVariables.incoming=true;
                CommonVariables.incomingCrash=true;
               // ////Log.e("incomekkkk", "disconnected1111 "+CommonVariables.incoming);

                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            /*    shippingDialog.setCancelable(false);
                shippingDialog.setCanceledOnTouchOutside(false);*/
                tv_customerName = shippingDialog.findViewById(R.id.customerName);
                et_numberSearch = shippingDialog.findViewById(R.id.step2search);

                et_numberSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 10) {
                            if (CommonVariables.WECONNECT_APP_ROLE.matches(ApiConstant.SR)) {

                                params = new HashMap<>();

                                params.put("MobileNo", "" + s.toString());
                                apiController.getSRIncomingData(token, params);
                            } else {
                                params = new HashMap<>();

                                params.put("MobileNo", "" + s.toString());
                                apiController.getEnquiryIncomingData(token, params);
                            }

                        } else {
                            tv_customerName.setText(ApiConstant.UNKNOWN_NUMBER);
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                sr_btn = shippingDialog.findViewById(R.id.sr_button);
                sales_btn = shippingDialog.findViewById(R.id.sales_btn);




                sr_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSRPOPUP(dataBeanSR);
                    }
                });

                sales_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSalesPOPUP(dataBeanSales);
                    }
                });

                hid = uid;
                shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {


                        if(CommonVariables.endcallclick)
                        {
                            contextw.stopService(new Intent(contextw,CustomFloatingViewService.class));
                            // OngoingCall.hangup();


                        }


//
//                        //OngoingCall.hangup();
//                        OngoingCall.call4.clear();
//                        OngoingCall.unhold();
//                        SharedPreferences chkfs = contextw.getSharedPreferences("LOGINss", Context.MODE_PRIVATE);
//                        final String tok = chkfs.getString("token", "");
//                        final String usr = chkfs.getString("Username", "");


                    }
                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {
                    shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                }


                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }


                callpick = shippingDialog.findViewById(R.id.pickcall);
                endcall = shippingDialog.findViewById(R.id.endcall);
                popupclose = shippingDialog.findViewById(R.id.moreclose);

             //   Log.e("MODEL"," "+Build.MODEL+"  "+Build.VERSION.SDK_INT);


                id = shippingDialog.findViewById(R.id.idddata);
                TextView closeds = shippingDialog.findViewById(R.id.moreclose);
                 incomingnumber = shippingDialog.findViewById(R.id.incomingnumber);
                closeds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();

                    }
                });


                try {

                    System.out.println("Receiver start");
                    new OngoingCall();
                    Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
                    disposables.add(disposable);

                    new OngoingCall();

                    Disposable disposable2 = OngoingCall.state
                            .filter(state -> state == Call.STATE_DISCONNECTED)
                            .delay(2, TimeUnit.SECONDS)
                            .firstElement()
                            .subscribe(this::finish);


                    disposables.add(disposable2);
                    String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                    newState=state;
                    String incomingNumbers = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    //////Log.e("Incmoing Number", "Inc" + incomingNumbers);

                    new CountDownTimer(2000,500)
                    {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            incomingnumber.setText(CallService.getIncommingNumber());

                        }

                        @Override
                        public void onFinish() {

                            CommonVariables.incomingNumber= CallService.getIncommingNumber();
                            incomingnumber.setText(CallService.getIncommingNumber());
                            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
                            addMissedNumber(token, CommonVariables.WECONNECT_APP_ROLE, CallService.getIncommingNumber());
                            searchdata(CommonVariables.WECONNECT_APP_ROLE, "" + incomingnumber.getText().toString());

                        }
                    }.start();

                    incomingnumber.setText(CallService.getIncommingNumber());

                    historyID = UUID.randomUUID().toString();


                    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

                        //////Log.e("Details ",new Gson().toJson(OngoingCall.call.getDetails()));


                        OngoingCall.call4.add(OngoingCall.call);

                        incomingnumber.setText(CallService.getIncommingNumber());



                        if (CommonVariables.incomingNumber != null && CommonVariables.incomingNumber.length() == 10) {


                            if (CommonVariables.WECONNECT_APP_ROLE.equals(ApiConstant.SR)) {
                                sr_btn.setVisibility(View.VISIBLE);
                                sales_btn.setVisibility(View.GONE);
                                params = new HashMap<>();
                                params.put("MobileNo", "" + incomingnumber.getText().toString());
                                apiController.getSRIncomingData(token, params);

                            } else if (CommonVariables.WECONNECT_APP_ROLE.equals(ApiConstant.ENQUIRY)) {

                                sales_btn.setVisibility(View.VISIBLE);
                                sr_btn.setVisibility(View.GONE);
                                params = new HashMap<>();

                                params.put("MobileNo", "" +incomingnumber.getText().toString());
                                apiController.getEnquiryIncomingData(token, params);


                            }


                        }



                        callpick.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {

//                                if (Build.MODEL.equals("Redmi 8A Dual")) {
//
//                                    TelecomManager tm = (TelecomManager) contextw
//                                            .getSystemService(Context.TELECOM_SERVICE);
//
//                                    if (tm == null) {
//                                        // whether you want to handle this is up to you really
//                                        throw new NullPointerException("tm == null");
//                                    }
//
//                                        if(OngoingCall.call2.size()>0)
//                                        {
//                                            OngoingCall.hangup();
//                                        }
//                                    tm.acceptRingingCall(0);
//                                    CommonVariables.receivecall = true;
//                                    callpick.setVisibility(View.GONE);
//
//
//                                    try {
//                                       if (OngoingCall.call2.size() > 0) OngoingCall.call2.get(OngoingCall.call2.size() - 2).disconnect();
//////                                        OngoingCall.call2.get(OngoingCall.call2.size() - 1).answer(0);
//////                                        OngoingCall.answer();
//////                                        if (OngoingCall.call4.size() > 0) OngoingCall.call4.get(0).answer(0);
//////                                        if (OngoingCall.call2.size() > 0) OngoingCall.call2.get(OngoingCall.call2.size() - 1).answer(0);
//////
//                                        OngoingCall.call2.get(0).answer(0);
////
////                                        OngoingCall.answer();
//
//                                        callpic = true;
//                                        CommonVariables.receivecall = true;
//                                        callpick.setVisibility(View.GONE);
//                                    }catch (Exception e)
//                                    {
//                                        e.printStackTrace();
//                                    }
//
//
//                                    if (incomingnumber.getText().toString() != null || !incomingnumber.getText().toString().equals("")) {
//                                        try {
//                                            if (am != null && recorder != null)
//                                                startRecording(historyID);
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                    }
//
//
//
//                                }
                                if (Build.MODEL.equals("Redmi 8A Dual")) {
                                    if (OngoingCall.call2.size() > 0) {
                                        OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
                                    }


                                    if(OngoingCall.call4.size() > 0) {
                                        OngoingCall.call4.get(OngoingCall.call4.size() - 1).answer(0);
                                    }

//                                    TelecomManager tm = (TelecomManager) contextw
//                                            .getSystemService(Context.TELECOM_SERVICE);
//
//                                    if (tm == null) {
//                                        // whether you want to handle this is up to you really
//                                        throw new NullPointerException("tm == null");
//                                    }
//
//                                    if(tm.isInCall())
//                                    {
//                                        tm.endCall();
//                                    }else
//                                    {
//
//                                    }
//
//
//                                    tm.acceptRingingCall(0);

                                     CallService.receiveCall();

                                    try {

                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }


                                        callpic = true;
                                        CommonVariables.receivecall = true;
                                        callpick.setVisibility(View.GONE);



                                    if (incomingnumber.getText().toString() != null || !incomingnumber.getText().toString().equals("")) {
                                        try {
                                            if (am != null && recorder != null)
                                                startRecording(historyID);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }



                                }
                                else if (Build.MODEL.equals("Redmi 7A"))
                                {
                                    if (OngoingCall.call2.size() > 0) {
                                        OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
                                    }


                                    if(OngoingCall.call4.size() > 0) {
                                        OngoingCall.call4.get(OngoingCall.call4.size() - 1).answer(0);
                                    }

//                                    TelecomManager tm = (TelecomManager) contextw
//                                            .getSystemService(Context.TELECOM_SERVICE);
//
//                                    if (tm == null) {
//                                        // whether you want to handle this is up to you really
//                                        throw new NullPointerException("tm == null");
//                                    }
//
//                                    if(tm.isInCall())
//                                    {
//                                        tm.endCall();
//                                    }else
//                                    {
//
//                                    }
//
//
//                                    tm.acceptRingingCall(0);

                                    CallService.receiveCall();

                                    try {

                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }


                                    callpic = true;
                                    CommonVariables.receivecall = true;
                                    callpick.setVisibility(View.GONE);



                                    if (incomingnumber.getText().toString() != null || !incomingnumber.getText().toString().equals("")) {
                                        try {
                                            if (am != null && recorder != null)
                                                startRecording(historyID);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }



                                }
                                else {
                                    if (OngoingCall.call2.size() > 0) {
                                        OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
                                    }


                                    if(OngoingCall.call4 != null && OngoingCall.call4.size() > 0) {
                                        OngoingCall.call4.get(OngoingCall.call4.size() - 1).answer(0);
                                    }

//                                    TelecomManager tm = (TelecomManager) contextw
//                                            .getSystemService(Context.TELECOM_SERVICE);
//
//                                    if (tm == null) {
//                                        // whether you want to handle this is up to you really
//                                        throw new NullPointerException("tm == null");
//                                    }
//
//                                    if(tm.isInCall())
//                                    {
//                                        tm.endCall();
//                                    }else
//                                    {
//
//                                    }
//
//
//                                    tm.acceptRingingCall(0);

                                    CallService.receiveCall();

                                    try {

                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }


                                    callpic = true;
                                    CommonVariables.receivecall = true;
                                    callpick.setVisibility(View.GONE);



                                    if (incomingnumber.getText().toString() != null || !incomingnumber.getText().toString().equals("")) {
                                        try {
                                            if (am != null && recorder != null)
                                                startRecording(historyID);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }



                                }


                            }
                        });


                        endcall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (Build.MODEL.equals("Redmi 8A Dual")|| Build.MODEL.equals("Redmi 7A") ||Build.MODEL.equals("Redmi 6 Pro"))
                                {

                                    if (dialogshown==true)
                                    {
                                        shippingDialog.dismiss();
                                        dailogdismmiss();

                                    }

                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
                                    editss = prefssss.edit();
                                    editss.putString("CALLUI", "stop");
                                    editss.commit();
                                    editss.apply();
                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                        if(CommonVariables.receivecall) {


                                            TelecomManager tm = (TelecomManager) contextw
                                                    .getSystemService(Context.TELECOM_SERVICE);

                                            if (tm == null) {
                                                // whether you want to handle this is up to you really
                                                throw new NullPointerException("tm == null");
                                            }

                                            tm.endCall();

                                            // rejectCall();
                                            shippingDialog.dismiss();
                                            dailogdismmiss();
                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));

                                            try {

                                                shippingDialog.dismiss();
                                                dailogdismmiss();



                                                //  OngoingCall.call.disconnect();
//                                                OngoingCall.call4.get(0).disconnect();
//                                                OngoingCall.call4.clear();
//                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();

                                                if (callpic) stopRecording(historyID);
                                                CommonVariables.endcallclick = true;
                                                shippingDialog.dismiss();
//                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                                    if (telecomManager.isInCall() ) {
//                                                        telecomManager.endCall();
//                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }

//                                               // S_R_Activity s_r_activity = new S_R_Activity();
//                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        }
                                        else
                                        {





                                            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {


                                                TelecomManager tm = (TelecomManager) contextw
                                                        .getSystemService(Context.TELECOM_SERVICE);

                                                if (tm == null) {
                                                    // whether you want to handle this is up to you really
                                                    throw new NullPointerException("tm == null");
                                                }

                                                tm.endCall();

                                                // rejectCall();
                                                dailogdismmiss();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));


                                            }



                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);

                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


                                                if (telecomManager.isInCall() ) {
                                                    telecomManager.endCall();
                                                    Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
                                                }
                                            }


                                            //Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
                                            // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();





                                        }

                                        shippingDialog.dismiss();
                                        dailogdismmiss();
                                    }
                                    else {

                                        if(CommonVariables.receivecall) {


                                            TelecomManager tm = (TelecomManager) contextw
                                                    .getSystemService(Context.TELECOM_SERVICE);

                                            if (tm == null) {
                                                // whether you want to handle this is up to you really
                                                throw new NullPointerException("tm == null");
                                            }

                                            tm.endCall();

                                            // rejectCall();
                                            dailogdismmiss();
                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));

                                            try {


                                              //  Toast.makeText(contextw, "onreceive", Toast.LENGTH_SHORT).show();

                                                //  OngoingCall.call.disconnect();
//                                                OngoingCall.call4.get(0).disconnect();
//                                                OngoingCall.call4.clear();
//                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();

                                                if (callpic) stopRecording(historyID);
                                                CommonVariables.endcallclick = true;
                                                shippingDialog.dismiss();
//                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                                    if (telecomManager.isInCall() ) {
//                                                        telecomManager.endCall();
//                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }

//                                               // S_R_Activity s_r_activity = new S_R_Activity();
//                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        }
                                        else
                                        {





                                            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {


                                                TelecomManager tm = (TelecomManager) contextw
                                                        .getSystemService(Context.TELECOM_SERVICE);

                                                if (tm == null) {
                                                    // whether you want to handle this is up to you really
                                                    throw new NullPointerException("tm == null");
                                                }

                                                tm.endCall();

                                                // rejectCall();
                                                dailogdismmiss();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));


                                            }



                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);

                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


                                                if (telecomManager.isInCall() ) {
                                                    telecomManager.endCall();
                                                    //Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
                                                }
                                            }


                                            //Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
                                            // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();





                                        }


                                    }
                                }
                                else {

                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
                                    editss = prefssss.edit();
                                    editss.putString("CALLUI", "stop");
                                    editss.commit();
                                    editss.apply();
                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                        if(CommonVariables.receivecall) {

                                            if(OngoingCall.call4.size() > 0) {
                                                OngoingCall.call4.get(OngoingCall.call4.size() - 1).disconnect();
                                            }

                                            try{
                                                shippingDialog.dismiss();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));

                                            }catch (Exception e)
                                            {
                                                e.printStackTrace();
                                            }
                                            TelecomManager tm = (TelecomManager) contextw
                                                    .getSystemService(Context.TELECOM_SERVICE);

                                            if (tm == null) {
                                                // whether you want to handle this is up to you really
                                                throw new NullPointerException("tm == null");
                                            }

                                            tm.endCall();

                                            // rejectCall();
                                            dailogdismmiss();
                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));

                                            try {


                                              //  Toast.makeText(contextw, "onreceive", Toast.LENGTH_SHORT).show();

                                                //  OngoingCall.call.disconnect();
//                                                OngoingCall.call4.get(0).disconnect();
//                                                OngoingCall.call4.clear();
//                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();

                                                if (callpic) stopRecording(historyID);
                                                CommonVariables.endcallclick = true;
                                                shippingDialog.dismiss();
//                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                                    if (telecomManager.isInCall() ) {
//                                                        telecomManager.endCall();
//                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }

//                                               // S_R_Activity s_r_activity = new S_R_Activity();
//                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        }
                                        else
                                        {



                                            if(OngoingCall.call4.size() > 0) {
                                                OngoingCall.call4.get(OngoingCall.call4.size() - 1).disconnect();
                                            }





                                            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {


                                                TelecomManager tm = (TelecomManager) contextw
                                                        .getSystemService(Context.TELECOM_SERVICE);

                                                if (tm == null) {
                                                    // whether you want to handle this is up to you really
                                                    throw new NullPointerException("tm == null");
                                                }

                                                tm.endCall();

                                                // rejectCall();
                                                dailogdismmiss();
                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));


                                            }



                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);

                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {


                                                if (telecomManager.isInCall() ) {
                                                    telecomManager.endCall();
                                                    //Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
                                                }
                                            }


                                            //Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
                                            // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();





                                        }

                                    }else
                                    {

                                         CallService.discon1();
                                        if(OngoingCall.call4.size() > 0) {
                                            OngoingCall.call4.get(OngoingCall.call4.size() - 1).disconnect();
                                        }

                                        try{
                                            shippingDialog.dismiss();
                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));

                                        }catch (Exception e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                            }
                        });


//                        endcall.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                               // Toast.makeText(contextw, "endcall", Toast.LENGTH_SHORT).show();
//
//                               // //////Log.e("Model12345 Redmi 6 Pro",Build.MODEL);
//
//                                if (Build.MODEL.equals("Redmi 8A Dual"))
//                                {
//
//                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
//                                    editss = prefssss.edit();
//                                    editss.putString("CALLUI", "stop");
//                                    editss.commit();
//                                    editss.apply();
//                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
//                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                                        if(CommonVariables.receivecall)
//                                        {
//
//                                            dailogdismmiss();
//                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                            TelecomManager tm = (TelecomManager) contextw
//                                                    .getSystemService(Context.TELECOM_SERVICE);
//
//                                            if (tm == null) {
//                                                // whether you want to handle this is up to you really
//                                                throw new NullPointerException("tm == null");
//                                            }
//
//                                            tm.endCall();
//
//                                            // rejectCall();
//
//
//                                            try {
//
//
//                                                Toast.makeText(contextw, "onreceive", Toast.LENGTH_SHORT).show();
//
//                                                //  OngoingCall.call.disconnect();
////                                                OngoingCall.call4.get(0).disconnect();
////                                                OngoingCall.call4.clear();
////                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
//
//                                                if (callpic) stopRecording(historyID);
//                                                CommonVariables.endcallclick = true;
//                                                shippingDialog.dismiss();
////                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
////
////                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////
////
////                                                    if (telecomManager.isInCall() ) {
////                                                        telecomManager.endCall();
////                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
////                                                    }
////                                                }
//
////                                               // S_R_Activity s_r_activity = new S_R_Activity();
////                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
//                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//
//
//                                        }else
//                                        {
//
//
//
//
//
//                                            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING) && CommonVariables.incoming) {
//
//
//                                                TelecomManager tm = (TelecomManager) contextw
//                                                        .getSystemService(Context.TELECOM_SERVICE);
//
//                                                if (tm == null) {
//                                                    // whether you want to handle this is up to you really
//                                                    throw new NullPointerException("tm == null");
//                                                }
//
//                                                tm.endCall();
//
//                                                // rejectCall();
//                                                dailogdismmiss();
//                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//
//
//                                            }else
//                                            {
//                                                dailogdismmiss();
//                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//
//                                            }
//
//
//
////                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
////
////                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////
////
////                                                if (telecomManager.isInCall() ) {
////                                                    telecomManager.endCall();
////                                                    Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
////                                                }
////                                            }
////
////
////                                            //////Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
////                                            // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();
////
//
//
//
//
//                                        }
//
//                                    }
//                                }
//                                else  if (Build.MODEL.equals("Redmi 6 Pro"))
//                                {
//
//                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
//                                    editss = prefssss.edit();
//                                    editss.putString("CALLUI", "stop");
//                                    editss.commit();
//                                    editss.apply();
//                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
//
//                                        if(CommonVariables.receivecall) {
//
//                                            dailogdismmiss();
//                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                            TelecomManager tm = (TelecomManager) contextw
//                                                    .getSystemService(Context.TELECOM_SERVICE);
//
//                                            if (tm == null) {
//                                                // whether you want to handle this is up to you really
//                                                throw new NullPointerException("tm == null");
//                                            }
//
//                                            tm.endCall();
//
//                                            // rejectCall();
//
//
//                                            try {
//
//
//                                                Toast.makeText(contextw, "onreceive", Toast.LENGTH_SHORT).show();
//
//                                                //  OngoingCall.call.disconnect();
////                                                OngoingCall.call4.get(0).disconnect();
////                                                OngoingCall.call4.clear();
////                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
//
//                                                if (callpic) stopRecording(historyID);
//                                                CommonVariables.endcallclick = true;
//                                                shippingDialog.dismiss();
////                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
////
////                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////
////
////                                                    if (telecomManager.isInCall() ) {
////                                                        telecomManager.endCall();
////                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
////                                                    }
////                                                }
//
////                                               // S_R_Activity s_r_activity = new S_R_Activity();
////                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
//                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//
//
//                                        }else
//                                        {
//
//
//
//
//
//                                            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//
//
//                                                TelecomManager tm = (TelecomManager) contextw
//                                                        .getSystemService(Context.TELECOM_SERVICE);
//
//                                                if (tm == null) {
//                                                    // whether you want to handle this is up to you really
//                                                    throw new NullPointerException("tm == null");
//                                                }
//
//                                                tm.endCall();
//
//                                                // rejectCall();
//                                                dailogdismmiss();
//                                                contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//
//
//                                            }
//
//
//
//                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                                if (telecomManager.isInCall() ) {
//                                                    telecomManager.endCall();
//                                                    Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                                }
//                                            }
//
//
//                                            //////Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
//                                            // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();
//
//
//
//
//
//
//
//                                    }
//                                }
//                                else if (Build.MODEL.equals("SM-A307FN"))
//                                {
//                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
//                                    editss = prefssss.edit();
//                                    editss.putString("CALLUI", "stop");
//                                    editss.commit();
//                                    editss.apply();
//                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
//                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//
//
//                                        try {
//                                            OngoingCall.call4.get(0).disconnect();
//                                            OngoingCall.call4.clear();
//                                            OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
//                                            if (callpic) stopRecording(historyID);
//                                            CommonVariables.endcallclick = true;
//                                            shippingDialog.dismiss();
//                                            TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                            if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                                if (telecomManager.isInCall()) {
//                                                    telecomManager.endCall();
//                                                    Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                                }
//                                            }
//
//                                            S_R_Activity s_r_activity = new S_R_Activity();
//                                            s_r_activity.looping();
//                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                    }
//                                }
//                                else
//                                {
//
//                                    CallService.discon2();
//
//                                    prefssss = contextw.getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
//                                    editss = prefssss.edit();
//                                    editss.putString("CALLUI", "stop");
//                                    editss.commit();
//                                    editss.apply();
//                                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");
//
//                                    if(CommonVariables.receivecall) {
//
//                                        dailogdismmiss();
//                                        contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                        TelecomManager tm = (TelecomManager) contextw
//                                                .getSystemService(Context.TELECOM_SERVICE);
//
//                                        if (tm == null) {
//                                            // whether you want to handle this is up to you really
//                                            throw new NullPointerException("tm == null");
//                                        }
//
//                                        tm.endCall();
//
//                                        // rejectCall();
//
//
//                                        try {
//
//
//                                            Toast.makeText(contextw, "onreceive", Toast.LENGTH_SHORT).show();
//
//                                            //  OngoingCall.call.disconnect();
////                                                OngoingCall.call4.get(0).disconnect();
////                                                OngoingCall.call4.clear();
////                                                OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
//
//                                            if (callpic) stopRecording(historyID);
//                                            CommonVariables.endcallclick = true;
//                                            shippingDialog.dismiss();
////                                                TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
////
////                                                if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////
////
////                                                    if (telecomManager.isInCall() ) {
////                                                        telecomManager.endCall();
////                                                        Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
////                                                    }
////                                                }
//
////                                               // S_R_Activity s_r_activity = new S_R_Activity();
////                                              if(!PhoneStateReceiverNew.incoming)  s_r_activity.looping();
//                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//
//                                    }else
//                                    {
//
//
//
//
//
//                                        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//
//
//                                            TelecomManager tm = (TelecomManager) contextw
//                                                    .getSystemService(Context.TELECOM_SERVICE);
//
//                                            if (tm == null) {
//                                                // whether you want to handle this is up to you really
//                                                throw new NullPointerException("tm == null");
//                                            }
//
//                                            tm.endCall();
//
//                                            // rejectCall();
//                                            dailogdismmiss();
//                                            contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//
//
//                                        }
//
//
//
//                                        TelecomManager telecomManager = (TelecomManager) contextw.getSystemService(TELECOM_SERVICE);
//
//                                        if (ActivityCompat.checkSelfPermission(contextw, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//
//                                            if (telecomManager.isInCall() ) {
//                                                telecomManager.endCall();
//                                                Toast.makeText(contextw, "End Call", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//
//
//                                        //////Log.e("Call Details  ",new Gson().toJson(OngoingCall.call4.get(0)));
//                                        // if(OngoingCall.call4.size()>0)  OngoingCall.call4.get(OngoingCall.call4.size()-1).disconnect();
//
//
//
//
//
//
//
//                                    }
//                                }
//
//                            }
//                        });

                        popupclose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                             shippingDialog.dismiss();
                                dailogdismmiss();

                                // contextw.stopService(new Intent(contextw, CustomFloatingViewService.class));
//                                try {
//                                    endcallclick=true;
//
//
//                                    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//                                    {
//                                        if(sata==Call.STATE_DISCONNECTED)
//                                        {
//
//
//
//                                            try{
//                                                if(am != null && recorder !=null)stopRecording(uid);
//                                            }catch (IllegalStateException e)
//                                            {
//                                                e.printStackTrace();
//                                            }
//
//                                        }
//                                        else
//                                        {
//                                            if(!Build.BRAND.equals("samsung"))
//                                            {
//
//                                                if(callpic==true) {
//                                                    OngoingCall.call2.get(OngoingCall.call2.size() - 1).disconnect();
//                                                    OngoingCall.call2.get(OngoingCall.call2.size() - 2).unhold();
//
//                                                }
//
//                                                try {
//
//                                                    OngoingCall.cendsprensCalls();
//                                                   shippingDialog.dismiss();
//                                                    try{
//                                                        if(am != null && recorder !=null)stopRecording(uid);
//                                                    }catch (IllegalStateException e)
//                                                    {
//                                                        e.printStackTrace();
//                                                    }
//                                                   shippingDialog.dismiss();
//                                                } catch (Exception e) {
//                                                    OngoingCall.cendsprensCalls();
//
//                                                    e.printStackTrace();
//
//
//                                                }
//
//                                                OngoingCall.cendsprensCalls();
//
//                                                shippingDialog.dismiss();
//                                               // stopRecording(uid);
//
//                                            }else
//                                            {
//                                                OngoingCall.cendsprensCalls();
//                                                shippingDialog.dismiss();
//                                                try{
//                                                    if(am != null && recorder !=null)stopRecording(uid);
//                                                }catch (IllegalStateException e)
//                                                {
//                                                    e.printStackTrace();
//                                                }
//
//                                            }
//
//
//
//                                        }
//
//                                    }
//                                    else
//                                    {
//
//
//                                        OngoingCall.cendsprensCalls();
//
//
//                                    }
//
//
//
//
//                                    try{
//                                        if(am != null && recorder !=null)stopRecording(uid);
//                                    }catch (IllegalStateException e)
//                                    {
//                                        e.printStackTrace();
//                                    }
//                                } catch (Exception e) {
//                                    OngoingCall.cendsprensCalls();
//                                    e.printStackTrace();

                                //            }
                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (String.valueOf(sata).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    OngoingCall.hangup();
                    shippingDialog.dismiss();
                    dailogdismmiss();
                } else {

                    incomingdiaogshow();
                }
            } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)) {

            } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)) {

            } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {

                IncomingCallState = TelephonyManager.EXTRA_STATE_OFFHOOK;
                // Toast.makeText(contextw, "OFFHOOK", Toast.LENGTH_SHORT).show();


                isShown = true;


            } else {
            }


        }




    }

    private void finish(Integer state) {


    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void updateUi(Integer state)
    {

        this.sata = state;

        Log.e("income1", "");
        // Set callInfo text by the state


        if (state == Call.STATE_DIALING) {
           // Toast.makeText(contextw, "dailing ", Toast.LENGTH_SHORT).show();

            //////Log.e("income2", "");


        } else if (state == Call.STATE_DISCONNECTED && CommonVariables.incoming) {
            //////Log.e("income", "disconnected1111 "+CommonVariables.incoming);

            CommonVariables.incoming=false;

            endcall.performClick();
            shippingDialog.dismiss();

           // Toast.makeText(contextw, "disconnected111122 "+Calendar.getInstance().getTimeInMillis(), Toast.LENGTH_SHORT).show();


            updatecallendtime(token, historyID, CommonVariables.WECONNECT_APP_ROLE);

//            if(receivecall==true)
//            {
            dailogdismmiss();
            OngoingCall.call4.clear();
            // OngoingCall.unhold();
            contextw.stopService(new Intent(contextw,CustomFloatingViewService.class));


            try {
                if (am != null && recorder != null) stopRecording(uid);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            // }


        } else if (state == Call.STATE_ACTIVE) {

            if(followuptype != null)followuptype.setSelection(0);

            //////Log.e("income4", "");
           // Toast.makeText(contextw, "active 2 ", Toast.LENGTH_SHORT).show();


            callstarl = "CALL DONE";

            updatecalledstatus(token, historyID, callstarl, CommonVariables.WECONNECT_APP_ROLE);


        } else if (state == Call.STATE_DISCONNECTING) {
            //////Log.e("income5", "");
            shippingDialog.dismiss();
            dailogdismmiss();

          //  Toast.makeText(contextw, "disconnecting ", Toast.LENGTH_SHORT).show();

//            if(receivecall==true)
//            {
            shippingDialog.dismiss();
            OngoingCall.call4.clear();
            // OngoingCall.unhold();

            // }
        } else if (!(state == Call.STATE_ACTIVE)) {
            //Toast.makeText(contextw, "active ", Toast.LENGTH_SHORT).show();
            if(CommonVariables.incoming)
            {
//                PhoneStateReceiverNew phoneStateReceiverNew= new PhoneStateReceiverNew();
//
//                phoneStateReceiverNew.shippingDialog.dismiss();
////                contextw.stopService(new Intent(contextw,CustomFloatingViewService.class));


            }

            callstarl = "BUSY";
        } else if (!(state == Call.STATE_RINGING)) {
            //////Log.e("income6", "");

          //  Toast.makeText(contextw, "ringing ", Toast.LENGTH_SHORT).show();

        }


    }

    // Recording Code and Implement
    /*-------------------------------------------------------------------------------------*/
    public void startRecording(String number) throws IOException
    {


        try {


            File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));

            audiofile = File.createTempFile(number, ".mp3", dir);

            am = (AudioManager) contextw.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_IN_CALL);
            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
                            am.setMode(AudioManager.MODE_IN_CALL);
                           /* if (!am.isSpeakerphoneOn())
                                am.setSpeakerphoneOn(true);*/
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!am.isWiredHeadsetOn()) {
                thread.start();
            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (Exception e) {
                //////Log.e("Voice Recorder", "prepare() failed " + e.getMessage());
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        //////Log.e("startrecording", "number" + number);

        //Creating MediaRecorder and specifying audio source, output format, encoder & output format

    }

    public void stopRecording(String id)
    {

        //stopping recorder
        try {
            if (am != null && recorder != null) {
                am.setMode(AudioManager.MODE_IN_CALL);
                try
                {
                    recorder.stop();

                }
                catch (RuntimeException e)
                {
                    e.printStackTrace();
                }
                recorder.reset();
                recorder.release();
                //addRecordingToMediaLibrary(id);
                //////Log.e("startrecording", "stop calling");
                UploadCallRecording(token, "test", historyID, CommonVariables.WECONNECT_APP_ROLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            OngoingCall.hangup();
            shippingDialog.dismiss();
        }
        //after stopping the recorder, create the sound file and add it to media library.


    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String dealercode, String historyid, String calltype)
    {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/mp3");
        values.put(MediaStore.Audio.Media.DATA, audiofile.getAbsolutePath());
        ContentResolver contentResolver = contextw.getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        selectedPath = getPath(contextw, newUri);
        contextw.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
        File files = new File(selectedPath);
        RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
        RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
        RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/m4a");
        RequestBody Recording = RequestBody.create(MediaType.parse("audio/*"), files);
        RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
        MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording", files.getName(), Recording);
        apiController.UploadCallRecording(token, Dealer_Code, History_id, Content_type, Recording_file, Call_type);
    }

    // Get Recording File Path
    public String getPath(Context context, Uri uri)
    {


        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            //////Log.e("ABC", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean)
    {
        if (beanTag.matches(ApiConstant.SRINCOMINGDATA)) {
            sr_btn.setVisibility(View.GONE);

            //  Toast.makeText(contextw, "success", Toast.LENGTH_SHORT).show();

            //////Log.e("SRINCOMINGDATA", new Gson().toJson(superClassCastBean));
            Incoming_Service_ReminderModel incoming_service_reminderModel = (Incoming_Service_ReminderModel) superClassCastBean;

            if (incoming_service_reminderModel.getData().size() > 0) {
                dataBeanSR = incoming_service_reminderModel.getData().get(0);
                tv_customerName.setText(dataBeanSR.getCusotmer_first_name() + " " + dataBeanSR.getCustomer_last_name());
                masterID = dataBeanSR.getId();

                addHistoryId(token, dataBeanSR.getId(), historyID);

                sr_btn.setVisibility(View.VISIBLE);


                //////Log.e("HistoryIDkkkk", dataBeanSR.getId() + " " + historyID);
            } else {
                sr_btn.setVisibility(View.GONE);

            }


        } else if (beanTag.matches(ApiConstant.GETSALESCALLINGHISTORY)) {
            Sales_HistoryModel sales_historyModel = (Sales_HistoryModel) superClassCastBean;

            if (sales_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(contextw, "No History Found..!", Toast.LENGTH_SHORT).show();

            }
            if (sales_historyModel.getData() != null) {

                for (Sales_HistoryModel.DataBean dataBean : sales_historyModel.getData()) {
                    salesReminderHistoryDataList.add(dataBean);

                }
                adapter_sales_historydata.notifyDataSetChanged();
            }


        }
        else if (beanTag.matches(ApiConstant.ENQUIRYINCOMINGDATA)) {
            sales_btn.setVisibility(View.VISIBLE);

            // Toast.makeText(contextw, "success", Toast.LENGTH_SHORT).show();
            //////Log.e("ENQUIRYINCOMINGDATA", new Gson().toJson(superClassCastBean));

            Incoming_SalesModel incoming_salesModel = (Incoming_SalesModel) superClassCastBean;

            if (incoming_salesModel.getData().size() > 0) {
                dataBeanSales = incoming_salesModel.getData().get(0);

                masterID = dataBeanSales.getId();

                tv_customerName.setText(dataBeanSales.getCusotmer_first_and_last_name());

                // remarks.setText(""+dataBeanSales.getEnquiry_comments());

                addHistoryIdSales(token, dataBeanSales.getId(), historyID);
                //////Log.e("HistoryIDkkkk", dataBeanSales.getId() + " " + historyID);

            } else {
                sales_btn.setVisibility(View.GONE);
            }

        }
        else if (beanTag.matches(ApiConstant.CLOUSERSUBREASON)) {
            ClouserSubReasonModel clouserSubReasonModel = (ClouserSubReasonModel) superClassCastBean;
            if (clouserSubReasonModel.getData() != null && clouserSubReasonModel.getData().size() > 0) {
                clousersubreasonlist[0].add("  ");
                for (ClouserSubReasonModel.DataBean dataBean : clouserSubReasonModel.getData()) {
                    clousersubreasonlist[0].add(dataBean.getStatus());
                    clousersubreasonArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, clousersubreasonlist[0]);
                    clousersubreason_spinner.setAdapter(clousersubreasonArrayAdapter);

                }

            }
            customDialog.dismiss();
        }
        else if (beanTag.matches(ApiConstant.UPDATESALESHISTORYID)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();

            }
        }
        else if (beanTag.matches(ApiConstant.GETFOLLOWUPDONELIST)) {
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followupdonelist[0].add(dataBean.getStatus());

                }

                followupdoneArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, followupdonelist[0]);
                if(sp_enquiry_status != null && followupdoneArrayAdapter != null)sp_enquiry_status.setAdapter(followupdoneArrayAdapter);
                customDialog.dismiss();


            }

        }
        else if (beanTag.matches(ApiConstant.GETCALLINGHISTORY)) {
            serviceReminderHistoryDataList.clear();
            SR_HistoryModel sr_historyModel = (SR_HistoryModel) superClassCastBean;


            if (sr_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(contextw, "No History Created Yet..!!", Toast.LENGTH_SHORT).show();
                customDialog.dismiss();
            } else {
                if (sr_historyModel.getData() != null) {
                    for (SR_HistoryModel.DataBean dataBean : sr_historyModel.getData()) {
                        serviceReminderHistoryDataList.add(dataBean);

                    }
                    customDialog.dismiss();
                    adapter_sr_historydata.notifyDataSetChanged();
                }
            }
            customDialog.dismiss();

        }
        else if (beanTag.matches(ApiConstant.UPDATEDATEOFPURCHASE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATESALESFOLLOWUPSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(contextw.getString(R.string.CommonAPIResponse))) {

                customDialog.dismiss();


            }

        }
        else if (beanTag.matches(ApiConstant.CONTACT_STATUS)) {

            contactStatuslist[0].clear();
            ContactStatusModel contactStatusModel = (ContactStatusModel) superClassCastBean;
            if (contactStatusModel.getData() != null && contactStatusModel.getData().size() > 0) {
                contactStatuslist[0].add("  ");
                for (ContactStatusModel.DataBean dataBean : contactStatusModel.getData()) {
                    contactStatuslist[0].add(dataBean.getStatus());


                }
            }
        }
        else if (beanTag.matches(ApiConstant.GETFOLLOWUPSTATUSLIST)) {

            followuplist[0].clear();
            FollowUpListModel followUpListModel = (FollowUpListModel) superClassCastBean;
            if (followUpListModel.getData() != null && followUpListModel.getData().size() > 0) {
                for (FollowUpListModel.DataBean dataBean : followUpListModel.getData()) {
                    followuplist[0].add(dataBean.getStatus());


                }
            }
        }
        else if (beanTag.matches(ApiConstant.CUSTOMER_REPLY)) {
            CustomerReplyModel customerReplyModel = (CustomerReplyModel) superClassCastBean;
            if (customerReplyModel.getData() != null && customerReplyModel.getData().size() > 0) {
                customerreplylist[0].add("  ");
                for (CustomerReplyModel.DataBean dataBean : customerReplyModel.getData()) {
                    customerreplylist[0].add(dataBean.getReply());

                }
            }
        }
        else if (beanTag.matches(ApiConstant.NOT_COMING_REASON)) {
            NotComingReasonModel notComingReasonModel = (NotComingReasonModel) superClassCastBean;
            if (notComingReasonModel.getData() != null && notComingReasonModel.getData().size() > 0) {
                notcomingreasonlist[0].add("  ");
                for (NotComingReasonModel.DataBean dataBean : notComingReasonModel.getData()) {
                    notcomingreasonlist[0].add(dataBean.getReason());

                }
            }
        }
        else if (beanTag.matches(ApiConstant.UPDATECLOSUREREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATECLOSURESUBREASON)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATEMAKE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();

            }
        }
        else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();

            }

        }
        else if (beanTag.matches(ApiConstant.UPDATEMODEL)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches("Updated")) {
                customDialog.dismiss();


            }
        }
        else if (beanTag.matches(ApiConstant.CLOUSERREASON)) {
            ClouserReasonModel clouserReasonModel = (ClouserReasonModel) superClassCastBean;
            if (clouserReasonModel.getData() != null && clouserReasonModel.getData().size() > 0) {
                clouserreasonlist[0].add("  ");
                for (ClouserReasonModel.DataBean dataBean : clouserReasonModel.getData()) {
                    clouserreasonlist[0].add(dataBean.getReason());

                }

                clouserreasonArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, clouserreasonlist[0]);
           if(clouserreason_spinner != null)     clouserreason_spinner.setAdapter(clouserreasonArrayAdapter);


            }
        }
        else if (beanTag.matches(ApiConstant.CLOUSERSUBREASON)) {
            ClouserSubReasonModel clouserSubReasonModel = (ClouserSubReasonModel) superClassCastBean;
            if (clouserSubReasonModel.getData() != null && clouserSubReasonModel.getData().size() > 0) {
                clousersubreasonlist[0].add("");
                for (ClouserSubReasonModel.DataBean dataBean : clouserSubReasonModel.getData()) {
                    clousersubreasonlist[0].add(dataBean.getStatus());

                }
                clousersubreasonArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, clousersubreasonlist[0]);
                if(clousersubreason_spinner != null)   clousersubreason_spinner.setAdapter(clousersubreasonArrayAdapter);


            }
            customDialog.dismiss();
        }
        else if (beanTag.matches(ApiConstant.MAKE)) {
            MakeModel makeModel = (MakeModel) superClassCastBean;
            if (makeModel.getData() != null && makeModel.getData().size() > 0) {
                makelist[0].add("  ");
                for (MakeModel.DataBean dataBean : makeModel.getData()) {
                    makelist[0].add(dataBean.getMake());

                }

                makeArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, makelist[0]);
                if(make_spinner != null)   make_spinner.setAdapter(makeArrayAdapter);

            }
        }
        else if (beanTag.matches(ApiConstant.MODEL)) {
            modellist[0].clear();
            ModelModel modelModel = (ModelModel) superClassCastBean;
            if (modelModel.getData() != null && modelModel.getData().size() > 0) {
                modellist[0].add("  ");
                for (ModelModel.DataBean dataBean : modelModel.getData()) {
                    modellist[0].add(dataBean.getModel());

                }
                modelArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, modellist[0]);
                if(model_spinner != null)    model_spinner.setAdapter(modelArrayAdapter);
            }
        }
        else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customDialog.dismiss();
            }

        }
        else if (beanTag.matches(ApiConstant.SINGLEDATA)) {


            singledatalist.clear();
            SingleCustomerHistoryData singleCustomerHistoryData = (SingleCustomerHistoryData) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {
                singledatalist.addAll(singleCustomerHistoryData.getData());
              //  Log.e("SINGLEDATA","  "+new Gson().toJson(singleCustomerHistoryData.getData()));


                try {
                    remarks.setText("" + singleCustomerHistoryData.getData().get(0).getComments());


                    String[] data = singleCustomerHistoryData.getData().get(0).getFollowup_date().split("T",2);
                    try
                    {
                        et_nextfollowupdate.setText("" + data[0]);
                      if(data.length>1)  et_nextfollowuptime.setText("" + data[1]);

                    }catch (ArrayIndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }

                    String[] data1 =singleCustomerHistoryData.getData().get(0).getServicebooking().split("T",2);
                    try
                    {
                        et_bookingdate.setText("" + data1[0]);
                        if(data.length>1)  et_bookingtime.setText("" + data1[1]);

                    }catch (ArrayIndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }

                    edit_Bookkingno.setText(singleCustomerHistoryData.getData().get(0).getServicerequest());
                    sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getNot_coming_reason().trim()));
                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getCustomerreply()));
                    customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getCustomerreply()));

                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        }

        else if (beanTag.matches(ApiConstant.SINGLEDATASALES)) {


            singledatalist_sales.clear();
            SingleCustomerHistoryData_Sales singleCustomerHistoryData1 = (SingleCustomerHistoryData_Sales) superClassCastBean;
            if (singleCustomerHistoryData1.getData() != null && singleCustomerHistoryData1.getData().size() > 0) {
                singledatalist_sales.addAll(singleCustomerHistoryData1.getData());
               // Log.e("SINGLEDATASALES11","  "+new Gson().toJson(singleCustomerHistoryData1.getData()));


                String[] data = singleCustomerHistoryData1.getData().get(0).getNextFollowupDate().split("T",2);
                try
                {
                    et_nextfollowupdate.setText("" + data[0]);
                    et_nextfollowuptime.setText("" + data[1]);

                }catch (ArrayIndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }


                String[] data1 = singleCustomerHistoryData1.getData().get(0).getExpectedDateOfPurchase().split("T",2);
                try
                {
                    expected_nextfollowupdate.setText("" + data[0]);
                    expected_nextfollowuptime.setText("" + data[1]);

                }catch (ArrayIndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }
              //  Log.e("SINGLEDATASALES1122","  "+new Gson().toJson(singleCustomerHistoryData1.getData().get(0).getRemark()));


                if(singleCustomerHistoryData1.getData().get(0).getRemark() != null )remarks.setText("" + singleCustomerHistoryData1.getData().get(0).getRemark());


                try {
                    followuptype.setSelection(((ArrayAdapter<String>) followuptype.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getFollowupStatus().trim()));
                    clouserreason_spinner.setSelection(((ArrayAdapter<String>) clouserreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getClosurereason()));
                    make_spinner.setSelection(((ArrayAdapter<String>) make_spinner.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getMake()));
                    model_spinner.setSelection(((ArrayAdapter<String>) model_spinner.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getModel()));
                    sp_enquiry_status.setSelection(((ArrayAdapter<String>) sp_enquiry_status.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getFollowupStatus()));
                    remarks.setText("" + singleCustomerHistoryData1.getData().get(0).getRemark());
                    if (singleCustomerHistoryData1.getData().get(0).getClosurereason() != null && !singleCustomerHistoryData1.getData().get(0).getClosurereason().isEmpty()) {
                        ll_clousersubreason.setVisibility(View.VISIBLE);
                        clousersubreason_spinner.setSelection(((ArrayAdapter<String>) clousersubreason_spinner.getAdapter()).getPosition(singleCustomerHistoryData1.getData().get(0).getClosureSubReason()));
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();

                }


            }
        }


    }/**/

    @Override
    public void onFailure(String msg)
    {
        // Toast.makeText(contextw, "fail", Toast.LENGTH_SHORT).show();

    }
    /*-------------------------------------------------------------------------------------*/

    @Override
    public void onError(String msg)
    {
        //  Toast.makeText(contextw, "Error", Toast.LENGTH_SHORT).show();


    }

    void showSRPOPUP(Incoming_Service_ReminderModel.DataBean databean) {
        final Dialog shippingDialog = new Dialog(contexts);
        getSingleCustomerSRHistoryData(token, CommonVariables.WECONNECT_APP_ROLE, masterID);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sr_incomingcustomer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // Customer Details Popup Id Genrate Code
        /*-------------------------------------------------*/

        ll_update = shippingDialog.findViewById(R.id.ll_update);
        customerreply = shippingDialog.findViewById(R.id.customerreplay);
        contactstatus = shippingDialog.findViewById(R.id.contactstatus);
        sp_notcomingreason = shippingDialog.findViewById(R.id.notcomingreason);
        sp_pickanddrop = shippingDialog.findViewById(R.id.pickanddrop);
        et_bookingdate = shippingDialog.findViewById(R.id.bboking);
        et_bookingtime = shippingDialog.findViewById(R.id.bbokingtime);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        iv_booking = shippingDialog.findViewById(R.id.iv_clear_bookingdate);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        iv_more = shippingDialog.findViewById(R.id.moredetailsbtn);
        iv_history = shippingDialog.findViewById(R.id.historyybtn);
        iv_textmessage = shippingDialog.findViewById(R.id.textmessage);
        iv_whatsapp = shippingDialog.findViewById(R.id.whatsapp);
        EditText edit_srname = shippingDialog.findViewById(R.id.srcuname);
        EditText edit_srservicetype = shippingDialog.findViewById(R.id.serivcetype);
        EditText edit_srmodel = shippingDialog.findViewById(R.id.srmodel);
        EditText edit_serviceduedate = shippingDialog.findViewById(R.id.srserduefrom);
        edit_Bookkingno = shippingDialog.findViewById(R.id.bookingno);
        EditText edit_pickdrop = shippingDialog.findViewById(R.id.pickdropbalance);

        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);
        TextView sugesstionRNR = shippingDialog.findViewById(R.id.sugesstionRNR);


        iv_bookingtime = shippingDialog.findViewById(R.id.iv_clear_bookingTime);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        iv_nextfollowuptime = shippingDialog.findViewById(R.id.iv_clear_followupdatetime);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        et_nextfollowuptime = shippingDialog.findViewById(R.id.nextfollowupedittime);

        et_bookingtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_nextfollowuptime.getText().length() == 0 && et_nextfollowuptime.getText().length() == 0 && sp_notcomingreason.getSelectedItemPosition() == 0) {
                    timePicker(et_bookingtime);
                } else if (sp_notcomingreason.getSelectedItemPosition() > 0) {
                    Toast.makeText(contextw, "First Select (Select NotComing Reason)", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(contextw, "First clear nextfollowupdate", Toast.LENGTH_SHORT).show();

                }
            }
        });

        et_nextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bookingdate.getText().length() == 0 && et_bookingtime.getText().length() == 0 && sp_notcomingreason.getSelectedItemPosition() == 0) {
                    timePicker(et_nextfollowuptime);
                } else if (sp_notcomingreason.getSelectedItemPosition() > 0) {
                    Toast.makeText(contextw, "First Select (Select NotComing Reason)", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(contextw, "First clear bookingdate", Toast.LENGTH_SHORT).show();

                }
            }
        });


        iv_nextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowuptime.setText("");

            }
        });

        iv_bookingtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_bookingtime.setText("");
            }
        });

        remarks = shippingDialog.findViewById(R.id.remarks);
        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);

        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMoreDetails();
            }
        });

        iv_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHistoryDetails();
            }
        });
        iv_textmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendtextmessage(dataBeanSR.getCusotmercontact());
            }
        });
        iv_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendWhatsappMessage(dataBeanSR.getCusotmercontact());
            }
        });

        if (databean != null && databean.getServiceupdate() != null && !databean.getServiceupdate().equals("")) {
           /* String[] data = databean.getServiceupdate().split("T", 2);
            try
            {
                et_bookingdate.setText("" + data[0]);
                et_bookingtime.setText("" + data[1]);

            }catch (ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }*/

        }

        if (databean != null && databean.getReminder_followup_date() != null && !databean.getReminder_followup_date().equals("")) {
            String[] data = databean.getReminder_followup_date().split("T", 2);

           /* try {

                et_nextfollowupdate.setText("" + data[0]);
                et_nextfollowuptime.setText("" + data[1]);
            }catch (ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }*/
        }

        if (dataBeanSR != null && dataBeanSR.getServiceupdate() != null && !dataBeanSR.getServiceupdate().equals(""))
            //  et_bookingdate.setText("" + dataBeanSR.getServiceupdate());
            if (dataBeanSR != null && dataBeanSR.getNext_service_due_date() != null && !dataBeanSR.getNext_service_due_date().equals(""))
                //  et_nextfollowupdate.setText("" + dataBeanSR.getNext_service_due_date());
                // if(dataBeanSR.r() != null) remarks.setText(""+dataBeanSR.getNext_service_due_date());
//
                submit_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (et_bookingdate.getText().length() > 0) {
                            updatebookingdate(token, historyID, et_bookingdate.getText().toString());
                        }
                        if (et_nextfollowupdate.getText().length() > 0) {
                            updatefollowupdate(token, historyID, et_nextfollowupdate.getText().toString(), ApiConstant.SR);
                        }
                        if (notcomingreason != null) {
                            updatenotcomingReason(token, historyID, notcomingreason);
                        }
                        if (customer_reply != null) {
                            updatecustomerreply(token, historyID, customer_reply);
                        }

                        if (contacted_status != null) {
                            updatecontatctstatusSR(token, historyID, contacted_status, ApiConstant.SR);
                        }

                        if (pickanddrop != null) {
                            updatepickanddrop(token, historyID, pickanddrop);
                        }

                        if (remarks.getText().length() > 0) {
                            updateremark(token, historyID, remarks.getText().toString(), ApiConstant.SR);
                        }
                        params = new HashMap<>();
                        params.put("HistoryId", historyID);
                        apiController.createServiceRequest(token, params);

                        shippingDialog.dismiss();
                    }
                });

        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });

        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });


        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });


        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });


        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });


        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });


        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });

        sugesstionRNR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstionRNR);
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (et_bookingdate.getText().length() > 0) {
                    updatebookingdate(token, historyID, et_bookingdate.getText().toString());
                }
                if (et_nextfollowupdate.getText().length() > 0) {
                    updatefollowupdate(token, historyID, et_nextfollowupdate.getText().toString(), ApiConstant.SR);
                }
                if (notcomingreason != null) {
                    updatenotcomingReason(token, historyID, notcomingreason);
                }
                if (customer_reply != null) {
                    updatecustomerreply(token, historyID, customer_reply);
                }

                if (contacted_status != null) {
                    updatecontatctstatusSR(token, historyID, contacted_status, ApiConstant.SR);
                }

                if (pickanddrop != null) {
                    updatepickanddrop(token, historyID, pickanddrop);
                }

                if (remarks.getText().length() > 0) {
                    updateremark(token, historyID, remarks.getText().toString(), ApiConstant.SR);
                }
                params = new HashMap<>();
                params.put("HistoryId", historyID);
                apiController.createServiceRequest(token, params);

                shippingDialog.dismiss();
            }
        });


        // Popup to Set Text the Values

        if (databean != null) {
            edit_srname.setText(tv_customerName.getText().toString());
            if (databean.getLast_service_type() != null)
                edit_srservicetype.setText("" + databean.getLast_service_type());
            if (databean.getModel() != null) edit_srmodel.setText("" + databean.getModel());
            if (databean.getLast_service_date() != null && !databean.getLast_service_date().isEmpty()) {
                edit_serviceduedate.setText("" +dateparse3(databean.getLast_service_date().substring(0,10)));
            }else
            {
                edit_serviceduedate.setText("" + databean.getLast_service_date());

            }
            edit_pickdrop.setText("");

        }


        contactArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, contactStatuslist[0]);
        contactstatus.setAdapter(contactArrayAdapter);


        contactstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    contacted_status = contactstatus.getSelectedItem().toString();

                } else {
                    callstatus = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // remarks.setText("");

        customerArrayAdapter = new ArrayAdapter<String>(contextw.getApplicationContext(), android.R.layout.simple_dropdown_item_1line, customerreplylist[0]);
        customerreply.setAdapter(customerArrayAdapter);

        customerreply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    customer_reply = customerreply.getSelectedItem().toString();

                } else {
                    customer_reply = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        notcomingreasonArrayAdapter = new ArrayAdapter<String>(contextw.getApplicationContext(), android.R.layout.simple_dropdown_item_1line, notcomingreasonlist[0]);
        sp_notcomingreason.setAdapter(notcomingreasonArrayAdapter);

        sp_notcomingreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (et_bookingdate.getText().length() == 0 && et_nextfollowupdate.getText().length() == 0) {

                    if (position != 0) {
                        notcomingreason = sp_notcomingreason.getSelectedItem().toString();

                    } else {
                        notcomingreason = null;
                    }
                } else {
                    sp_notcomingreason.setSelection(0, true);

                    if (et_nextfollowupdate.getText().length() > 0) {
                        Toast.makeText(contextw, "First Clear nextfollowupdate Date", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(contextw, "First Clear Booking Date", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_pickanddrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    pickanddrop = sp_pickanddrop.getSelectedItem().toString();

                } else {
                    pickanddrop = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*-------------------------------------------------*/


        iv_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_Bookkingno.getText().length() > 0) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(contextw);
                    builder.setMessage("Do you want cancel booking ?");
                    builder.setNeutralButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            params = new HashMap<>();
                            params.put("HistoryId", historyID);
                            apiController.CancelServiceRequest(token, params);
                            et_bookingdate.setText("");
                            et_bookingtime.setText("");
                            edit_Bookkingno.setText("");


                        }
                    });
                    builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });

                    builder.show();


                } else {
                    et_bookingdate.setText("");

                }
            }
        });
        iv_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowupdate.setText("");
            }
        });


        et_bookingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // if(singledatalist.size()==0)getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);

                if (et_nextfollowupdate.getText().toString().equals("") && sp_notcomingreason != null &&
                        sp_notcomingreason.getSelectedItemPosition() == 0)
                {
                    final Calendar c = Calendar.getInstance();
                    final int mYear = c.get(Calendar.YEAR);
                    final int mMonth = c.get(Calendar.MONTH);
                    final int mDay = c.get(Calendar.DAY_OF_MONTH);

                    // Launch Date Picker Dialog
                    DatePickerDialog dpd = new DatePickerDialog(contexts.getApplicationContext(),R.style.datepickerCustom,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    int ye = year;
                                    day = dayOfMonth;
                                    int ss = month + 1;
                                    int mon = ss;

                                    et_bookingdate.setText(dateparse3(ye + "-" + mon + "-" + day));

                                }
                            }, mYear, mMonth, mDay);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                    } else {
                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                    }
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    }
                    dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                    dpd.show();

                } else {
                    if (et_nextfollowupdate.getText().length() > 0) {
                        Toast.makeText(contextw, "First Clear nextfollowupdate Date", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(contextw, "First Select (NotComing Reason)", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////Log.e("BookingDate", "Booking" + et_bookingdate.getText().toString());
                //////Log.e("NotComingReason", "NCR" + notcomingreason);

                if (et_bookingdate.getText().toString().equals("") &&  sp_notcomingreason != null &&
                        sp_notcomingreason.getSelectedItemPosition() == 0) {
                    // Process to get Current Date
                    final Calendar c = Calendar.getInstance();
                    final int mYear = c.get(Calendar.YEAR);
                    final int mMonth = c.get(Calendar.MONTH);
                    final int mDay = c.get(Calendar.DAY_OF_MONTH);

                    // Launch Date Picker Dialog
                    DatePickerDialog dpd = new DatePickerDialog(contexts.getApplicationContext(),R.style.datepickerCustom,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    int ye = year;
                                    day = dayOfMonth;
                                    int ss = month + 1;
                                    int mon = ss;

                                    et_nextfollowupdate.setText(dateparse3(ye + "-" + mon + "-" + day));

                                }
                            }, mYear, mMonth, mDay);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                    } else {
                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                    }
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    }
                    dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                    dpd.show();

                } else {

                    if (et_bookingdate.getText().length() > 0) {
                        Toast.makeText(contextw, "First Clear Booking Date", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(contextw, "First Select (NotComing Reason)", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();

    }

    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }

    void showSalesPOPUP(Incoming_SalesModel.DataBean dataBean) {
        final Dialog shippingDialog = new Dialog(contexts);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_incoming_customer_details_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customDialog.dismiss();
            }
        });


        /*--------------------------------ID Genration-----------------------------------------------*/
        clouserreason_spinner = shippingDialog.findViewById(R.id.closurereason);
        clousersubreason_spinner = shippingDialog.findViewById(R.id.closuresubreason);
        make_spinner = shippingDialog.findViewById(R.id.make);
        model_spinner = shippingDialog.findViewById(R.id.model);
        followuptype = shippingDialog.findViewById(R.id.followups_spinner);
        sp_enquiry_status = shippingDialog.findViewById(R.id.sp_enquiry_status);
        ll_make = shippingDialog.findViewById(R.id.ll_make);
        ll_model = shippingDialog.findViewById(R.id.ll_model);
       // iv_clouserreson = shippingDialog.findViewById(R.id.add_updateDetails);
        iv_nextfollowup = shippingDialog.findViewById(R.id.iv_clear_nextfollowupdate);
        iv_nextfollowuptime = shippingDialog.findViewById(R.id.iv_clear_nextfollowuptime);
        iv_expectednextfollowuptime = shippingDialog.findViewById(R.id.iv_clear_expectedfollowuptime);
        iv_expectednextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_expectedfollowupdate);
       CardView cv_nextfollowdate1 = shippingDialog.findViewById(R.id.nextfollowupdate);
       CardView cv_expecteddate1 = shippingDialog.findViewById(R.id.dop);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        et_nextfollowuptime = shippingDialog.findViewById(R.id.nextfollowupedittime);
        expected_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupeditexpected);
        expected_nextfollowuptime = shippingDialog.findViewById(R.id.nextfollowupeditexpectedtime);
        Spinner complaintcategory = shippingDialog.findViewById(R.id.closurereason);
        iv_more = shippingDialog.findViewById(R.id.moredetailsbtn);
        iv_history = shippingDialog.findViewById(R.id.historyybtn);
        iv_textmessage = shippingDialog.findViewById(R.id.textmessage);
        iv_whatsapp = shippingDialog.findViewById(R.id.whatsapp);
        final LinearLayout complaintcategorysubll = shippingDialog.findViewById(R.id.ll_reasonsub);
        ll_clouserreson = shippingDialog.findViewById(R.id.ll_clouserreson);
        ll_clousersubreason = shippingDialog.findViewById(R.id.ll_reasonsub);
        EditText sales_customername = shippingDialog.findViewById(R.id.salescuname);
        EditText sales_enquiry_number = shippingDialog.findViewById(R.id.sales_enquiry_number);
        EditText enquiry_open_date = shippingDialog.findViewById(R.id.sales_enquiry_open_date);
        EditText sales_enquiry_status = shippingDialog.findViewById(R.id.sale_enquiry_status);
         remarks = shippingDialog.findViewById(R.id.remarks);
        Button submit_btn = shippingDialog.findViewById(R.id.submitstatus);
        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);

        getSingleCustomerSRHistoryData_Sales(token, "ENQUIRY", masterID);

        expected_nextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expected_nextfollowupdate.getText().length() != 0) {
                    timePicker(expected_nextfollowuptime);
                } else {
                    Toasty.warning(contextw, "First select date of purchase ", Toast.LENGTH_SHORT).show();

                }
            }
        });

        et_nextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_nextfollowupdate.getText().length() != 0) {
                    timePicker(et_nextfollowuptime);
                } else {
                    Toast.makeText(contextw, "First select Nextfollowup date", Toast.LENGTH_SHORT).show();

                }
            }
        });


        iv_nextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowuptime.setText("");

            }
        });

        iv_expectednextfollowuptime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expected_nextfollowuptime.setText("");
            }
        });

        /*-------------------------------------------------------------------------------*/


        /*-----------------------------------Edit Text to Set Values--------------------------------------------*/

     //   Log.e("SINGLESALES"," "+new Gson().toJson(dataBean));
        if (dataBean.getCusotmer_first_and_last_name() != null)
            sales_customername.setText("" + dataBean.getCusotmer_first_and_last_name().toUpperCase());
        if (dataBean.getEnquirynumber() != null)
            sales_enquiry_number.setText("" + dataBean.getEnquirynumber().toUpperCase());
        if (dataBean.getEnquiry_open_date() != null && !dataBean.getEnquiry_open_date().isEmpty())
            enquiry_open_date.setText("" + dateparse3(dataBean.getEnquiry_open_date().substring(0,10)));
        if (dataBean.getEnquiry_status() != null)
            sales_enquiry_status.setText("" + dataBean.getEnquiry_status().toUpperCase());

        String enqstatus=dataBean.getEnquiry_status().toUpperCase();

       // if (dataBean.getEnquiry_comments() != null) remarks.setText(""+dataBean.getEnquiry_comments());

       // if(sp_enquiry_status != null && sales_enquiry_status.getText().equals("OPEN"))sp_enquiry_status.setSelection(1);
        /*-------------------------------------------------------------------------------*/


        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMoreDetailsSales(dataBeanSales);
            }
        });

        iv_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHistoryDetailsSales(masterID);
            }
        });
        iv_textmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendtextmessage(dataBeanSales.getMobilenumber());
            }
        });
        iv_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendWhatsappMessage(dataBeanSales.getMobilenumber());
            }
        });
        /*----------------------------------PopUp Click Listeners---------------------------------------------*/
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(contextw,R.style.datepickerCustom,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                int ye = year;
                                day = dayOfMonth;
                                int ss = month + 1;
                                int mon = ss;

                                et_nextfollowupdate.setText(dateparse3(ye + "-" + mon + "-" + day));

                            }
                        }, mYear, mMonth, mDay);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {
                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                dpd.show();


            }
        });


        expected_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Process to get Current Date
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(contextw,R.style.datepickerCustom,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                int ye = year;
                                day = dayOfMonth;
                                int ss = month + 1;
                                int mon = ss;

                                expected_nextfollowupdate.setText(dateparse3(ye + "-" + mon + "-" + day));

                            }
                        }, mYear, mMonth, mDay);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {
                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    dpd.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                dpd.show();


            }
        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                // CallService.discon1();
            }
        });
//        iv_clouserreson.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                counter++;
//                if (counter % 2 != 0) {
//                    iv_clouserreson.setImageResource(R.drawable.ic_baseline_remove_circle_outline_24);
//                    ll_make.setVisibility(View.VISIBLE);
//                    ll_clouserreson.setVisibility(View.VISIBLE);
//
//
//                } else {
//                    iv_clouserreson.setImageResource(R.drawable.icon_add);
//                    ll_make.setVisibility(View.GONE);
//                    ll_clouserreson.setVisibility(View.GONE);
//                    ll_clousersubreason.setVisibility(View.GONE);
//
//                }
//
//            }
//        });
        iv_expectednextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expected_nextfollowupdate.setText("");

            }
        });
        iv_nextfollowup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowupdate.setText("");


            }


        });

        /*----------------------------------------------------------------------------------------------------*/



        /*---------------------------------Spinner Selected Listener----------------------------------------------*/
        complaintcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    complaintcategorysubll.setVisibility(View.VISIBLE);
                } else {
                    complaintcategorysubll.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        followupdoneArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, followupdonelist[0]);
        if(sp_enquiry_status != null && followupdoneArrayAdapter != null)sp_enquiry_status.setAdapter(followupdoneArrayAdapter);
        customDialog.dismiss();
        ll_clouserreson.setVisibility(View.GONE);
        ll_make.setVisibility(View.GONE);
        ll_model.setVisibility(View.GONE);

       // Log.e(""+dataBean.getEnquiry_status().toUpperCase(),"  "+dataBean.getEnquiry_status().toUpperCase());
        if(dataBean.getEnquiry_status().toUpperCase().equals("OPEN"))sp_enquiry_status.setSelection(1);
        sp_enquiry_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                followupstatus = sp_enquiry_status.getSelectedItem().toString();


                /*-----------------------------------Visibility Gone Code--------------------------------------------*/
                if (followupstatus.equals("Open")) {
                    ll_clouserreson.setVisibility(View.GONE);
                    ll_make.setVisibility(View.GONE);
                    ll_model.setVisibility(View.GONE);
                    ll_clousersubreason.setVisibility(View.GONE);

                    if(cv_nextfollowdate1 != null)cv_nextfollowdate1.setVisibility(View.VISIBLE);
                    if(cv_expecteddate1 != null)cv_expecteddate1.setVisibility(View.VISIBLE);


                } else if (followupstatus.equals("Closed")) {

                    if(cv_nextfollowdate1 != null)cv_nextfollowdate1.setVisibility(View.GONE);
                    if(cv_expecteddate1 != null)cv_expecteddate1.setVisibility(View.GONE);

                    ll_clouserreson.setVisibility(View.VISIBLE);
                    ll_make.setVisibility(View.VISIBLE);
                    ll_model.setVisibility(View.VISIBLE);
                }



                /*-------------------------------------------------------------------------------*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                followupstatus = "Open";
            }
        });

        clouserreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (clouserreason_spinner.getSelectedItemPosition() != 0) {
                    clousersubreasonlist[0].clear();
                    closurereason = clouserreason_spinner.getSelectedItem().toString();
                    getclosurereason(token, clouserreason_spinner.getSelectedItem().toString());
                  if(ll_clouserreson.getVisibility() == View.VISIBLE)  ll_clousersubreason.setVisibility(View.VISIBLE);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                closurereason= null;

            }
        });
        clousersubreason_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (clousersubreason_spinner.getSelectedItemPosition() != 0) {

                    closuresubreason = clousersubreason_spinner.getSelectedItem().toString();


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                closuresubreason= null;

            }
        });
        make_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    make = make_spinner.getSelectedItem().toString();
                    params = new HashMap<>();
                    params.put("Make", make.toUpperCase());
                    apiController.getModel(token, params);

                } else {
                    modellist[0].clear();
                    modellist[0].add("Model");

                    modelArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, modellist[0]);
                    model_spinner.setAdapter(modelArrayAdapter);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                make=null;

            }
        });
        model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                string_model = model_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                string_model= null;

            }
        });

        /*-------------------------------------------------------------------------------*/


        if (dataBean.getNext_followup_date() != null && !dataBean.getNext_followup_date().equals("")) {
            String[] data = dataBean.getNext_followup_date().split("T", 2);

          /*  try
            {
                et_nextfollowupdate.setText("" + data[0]);
                et_nextfollowuptime.setText("" + data[1]);

            }catch (ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }
*/
        }

        if (dataBean.getExpected_date_purchase() != null && !dataBean.getExpected_date_purchase().equals("")) {
            String[] data = dataBean.getExpected_date_purchase().split("T", 2);

          /*  try
            {
                expected_nextfollowupdate.setText("" + data[0]);
                expected_nextfollowuptime.setText("" + data[1]);

            }catch (ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }*/

        }


        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (et_nextfollowupdate.getText().length() > 0) {
                    updatefollowupdate(token, historyID, et_nextfollowupdate.getText().toString(), "ENQUIRY");
                }
                if (expected_nextfollowupdate.getText().length() > 0) {
                    UpdateDateofPurchase(token, historyID, expected_nextfollowupdate.getText().toString());
                }
                if (followupstatus != null) {
                    updatecontatctstatusSales(token, historyID, followupstatus, "ENQUIRY");
                }
                if (closurereason != null) {
                    UpdateClosureReason(token, historyID, closurereason);
                }

                if (closuresubreason != null) {
                    UpdateClosureSubReason(token, historyID, closuresubreason);
                }

                if (make != null) {
                    UpdateMake(token, historyID, make);
                }
                if (string_model != null) {
                    UpdateModel(token, historyID, dataBean.getModel_interested_in());
                }

                if (remarks.getText().length() > 0) {
                    updateremark(token, historyID, remarks.getText().toString(), "ENQUIRY");
                }

                params = new HashMap<>();
                params.put("HistoryId", historyID);

                apiController.createFolloup(token, params);


                shippingDialog.dismiss();

            }
        });

        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });

        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });


        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });


        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });


        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });


        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });


        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });
        /*-------------------------------------------------------------------------------*/


        clouserreasonArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, clouserreasonlist[0]);
        clouserreason_spinner.setAdapter(clouserreasonArrayAdapter);


        makeArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, makelist[0]);
        make_spinner.setAdapter(makeArrayAdapter);

        modelArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, modellist[0]);
        model_spinner.setAdapter(modelArrayAdapter);

        followupArrayAdapter = new ArrayAdapter<String>(contextw, android.R.layout.simple_dropdown_item_1line, followuplist[0]);
        followuptype.setAdapter(followupArrayAdapter);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();

    }
    // Get Service Reminder Data
    public void getservicereminderdata(String token, String offset) {
       // customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.offset, offset);
        apiController.getServiceReminderData(token, params);
    }
    public void addHistoryId(String token, String mastersrid, String historyId) {
        params = new HashMap<>();
        params.put("MasterSRId", mastersrid);
        params.put("HistoryId", historyId);
        params.put("BoundType", "INBOUND");
        apiController.addCallHistoryData(token, params);

    }

    // Update Contatct Number Function
    public void UpdateContatctNumber(String token, String mastersrid, String historyId,
                                     String newcontactno, String priorotyStatus) {
        params = new HashMap<>();
        params.put("MasterSRId", mastersrid);
        params.put("HistoryId", historyId);
        params.put("NewContactNo", newcontactno);
        params.put("PriorityStatus", priorotyStatus);
        apiController.UpdateContactNumber(token, params);
    }

    // Update Customer Reply Function
    public void updatecustomerreply(String token, String historyId, String Reply) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Reply", Reply);
        apiController.UpdateCustomerReply(token, params);
    }

    // Update Tagged SR Status
    public void updatetaggedsrstatus(String token, String historyId, String Status) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Status", Status);
        apiController.UpdateTaggedSRStatus(token, params);
    }

    // Update Booking Date & Time
    public void updatebookingdate(String token, String historyId, String bookingdate) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("BookingDate", bookingdate);
        //////Log.e("Updatebooking ", new Gson().toJson(params));
        apiController.UpdateBookingate(token, params);
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String calltype) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("CallType", calltype);
        apiController.UpdateCallEndTime(token, params);
    }

    // Update Remark
    public void updateremark(String token, String historyId, String remark, String calltype) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Comment", remark);
        params.put("CallType", calltype);
        apiController.UpdateRemark(token, params);
    }

    // Update Follow-Up Date
    public void updatefollowupdate(String token, String historyId, String followdatetime, String calltype) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("FollowupDateTime", followdatetime);
        params.put("CallType", calltype);

        apiController.UpdateNextFollowUpdate(token, params);
    }

    // Update Contatct Status ServiceReminder
    public void updatecontatctstatusSR(String token, String historyId, String contactstatys, String calltype) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Status", contactstatys);
        params.put("CallType", calltype);

        apiController.UpdateContactStatus(token, params);
    }

    // Update Contatct Status ServiceReminder
    public void updatecontatctstatusSales(String token, String historyId, String contactstatys, String calltype) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Status", contactstatys);
        params.put("CallType", calltype);

        apiController.UpdateSaleFollowUpStatus(token, params);
    }

    // Update Followup done Status
    public void updateFollowupDoneStatus(String token, String historyId, String contactstatus) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Status", contactstatus);

        apiController.UpdateSaleFollowUpStatus(token, params);
    }

    //Get Single Customer Service Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String tag, String masterId) {


        params = new HashMap<>();
        params.put("MasterId", masterId);
        params.put("Tag", tag);

        apiController.getSingleCustomerHistoryData(token, params);
    }

    public void getSingleCustomerSRHistoryData_Sales(String token, String tag, String masterId) {


        params = new HashMap<>();
        params.put("MasterId", masterId);
        params.put("Tag", tag);

        apiController.getSingleCustomerHistoryData_Sales(token, params);
    }

    // Update Contatct Status
    public void updatepickanddrop(String token, String historyId, String status) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Status", status);

        apiController.UpdatePickdrop(token, params);
    }

    // Add  Sales History Id Function
    public void addHistoryIdSales(String token, String mastersrid, String historyId) {
        params = new HashMap<>();
        params.put("MasterSEId", mastersrid);
        params.put("HistoryId", historyId);
        params.put("BoundType", "INBOUND");
        apiController.UpdateHistorySales(token, params);

    }

    // Update CalledStatus
    public void updatecalledstatus(String token, String historyId, String callstatus, String calltype) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("CallStatus", callstatus);
        params.put("CallType", calltype);

        apiController.UpdateCalledStatus(token, params);
    }

    // Update NotComingReason
    public void updatenotcomingReason(String token, String historyId, String reason) {

        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("CallType", reason);

        apiController.UpdateNotComingReason(token, params);
    }

    // Update get Service Reminder History Data
    public void getSRHistoryData(String token, String Id) {


        params = new HashMap<>();
        params.put("MasterId", Id);

        apiController.getSRHistory(token, params);
    }

    // Get Sales Enquiry Data
    public void getEnquiryData(String token, String folllowuptype, String offset) {

        params = new HashMap<>();
        params.put("offset", offset);
        params.put("FollowupType", folllowuptype);
        params.put("FilterType", "ALL");
        apiController.getSalesEnquiryData(token, params);
    }

    // Add missed Number
    public void addMissedNumber(String token, String tag, String number) {
        params = new HashMap<>();
        params.put("MissedNo", number);
        params.put("MissedTag", tag);
        apiController.addMissedNumber("" + token, params);
    }

    // Add Date of Purchase in this Function
    public void UpdateDateofPurchase(String token, String historyId, String expecteddateofpurchase) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("ExpectedPurchaseDate", expecteddateofpurchase);
        apiController.Updatedateofpurchase(token, params);

    }

    // Update Closure Reason in this Function
    public void UpdateClosureReason(String token, String historyId, String Reason) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Reason", Reason);
        apiController.Updatedateofpurchase(token, params);

    }

    // Update Closure Sub Reason in this Function
    public void UpdateClosureSubReason(String token, String historyId, String Reason) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("Reason", Reason);
        apiController.UpdateClosureSubReason(token, params);

    }

    // Update Make in this Function
    public void UpdateMake(String token, String historyId, String make) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("MakeBought", make);
        apiController.UpdateMake(token, params);

    }

    //Get Closure Reason List()
    public void getclosurereason(String token, String closurereson) {
        params = new HashMap<>();
        params.put("ClosureReason", closurereson);
        apiController.getClosureSubReason(token, params);
    }

    // Update Model in this Function
    public void UpdateModel(String token, String historyId, String model) {
        params = new HashMap<>();
        params.put("HistoryId", historyId);
        params.put("ModelBought", model);
        apiController.UpdateModel(token, params);

    }

    //  get Sales Reminder History Data
    public void getSalesHistoryData(String token, String Id) {

        params = new HashMap<>();
        params.put("MasterId", Id);
     //   Log.e("Historydata"," "+new Gson().toJson(params));

        apiController.getSalesHistory(token, params);
    }

    public void searchdata(String tag, String contactnumber) {

        if (tag.equals(ApiConstant.SR)) {


            getSingleCustomerSRHistoryData(token, tag, string_masterId);


        } else if (tag.matches(ApiConstant.ENQUIRY))


            getSingleCustomerSRHistoryData(token, tag, string_masterId);

    }

    public void timePicker(TextView textView) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(contextw,R.style.datepickerCustom,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        textView.setText("" + hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            timePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            timePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            timePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            timePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }
        timePickerDialog.show();
    }

    public void getHistoryDetails() {


        getSRHistoryData(token, dataBeanSR.getId());
        final Dialog shippingDialog = new Dialog(contextw);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(contextw, RecyclerView.VERTICAL, false));
        adapter_sr_historydata = new Adapter_SR_HistoryIncomingdata(contextw, serviceReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sr_historydata);
        adapter_sr_historydata.notifyDataSetChanged();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }


        shippingDialog.show();

    }

    // More Details Fuction( This Function is Used on click listener of more details icon)
    public void getMoreDetails() {
        final Dialog shippingDialog = new Dialog(contextw);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sr_incomming_morepopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        shippingDialog.show();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        EditText edit_number = shippingDialog.findViewById(R.id.mobileno);
        EditText last_service_date = shippingDialog.findViewById(R.id.last_service_date);
        EditText last_service_type = shippingDialog.findViewById(R.id.last_service_type);
        EditText last_jc_km = shippingDialog.findViewById(R.id.last_jc_km);
        EditText last_invoice_value = shippingDialog.findViewById(R.id.last_invoice_value);
        EditText open_complaint_number = shippingDialog.findViewById(R.id.open_complaint_number);
        EditText open_sr_number = shippingDialog.findViewById(R.id.open_sr_number);
        EditText fsc1_date = shippingDialog.findViewById(R.id.fsc1_date);
        EditText fsc2_date = shippingDialog.findViewById(R.id.fsc2_date);
        EditText fsc3_date = shippingDialog.findViewById(R.id.fsc3_date);
        EditText fsc4_date = shippingDialog.findViewById(R.id.fsc4_date);
        EditText fsc5_date = shippingDialog.findViewById(R.id.fsc5_date);
        EditText paid1_date = shippingDialog.findViewById(R.id.paid1_date);
        EditText creatat = shippingDialog.findViewById(R.id.createdat);
        EditText vin = shippingDialog.findViewById(R.id.vin);
        EditText color = shippingDialog.findViewById(R.id.color);
        EditText hsrp_stage = shippingDialog.findViewById(R.id.hsrp_stage);
        EditText warranty_start_date = shippingDialog.findViewById(R.id.warranty_start_date);
        EditText service_update = shippingDialog.findViewById(R.id.service_update);
        EditText mandatory_update = shippingDialog.findViewById(R.id.mandatory_update);
        EditText herosure = shippingDialog.findViewById(R.id.herosure);
        EditText avg_km_run = shippingDialog.findViewById(R.id.avg_km_run);
        EditText digital_customer = shippingDialog.findViewById(R.id.digitalCustomer);
        EditText organization = shippingDialog.findViewById(R.id.organization);
        EditText customer_rating = shippingDialog.findViewById(R.id.customer_rating);
        EditText address = shippingDialog.findViewById(R.id.address);
        EditText multiple_nameandmobile = shippingDialog.findViewById(R.id.name_and_mobileno_multiple);
        EditText next_service_due_date = shippingDialog.findViewById(R.id.next_service_due_date);
        EditText reminder_followup_date = shippingDialog.findViewById(R.id.reminder_follow_update);
        EditText insurance_expiry_date = shippingDialog.findViewById(R.id.insurance_expiry_date);
        EditText gl_expiry = shippingDialog.findViewById(R.id.gl_expiry);
        EditText gl_points = shippingDialog.findViewById(R.id.gl_points);
        EditText ls_expiry = shippingDialog.findViewById(R.id.ls_expiry);
        EditText ls_balance = shippingDialog.findViewById(R.id.ls_balance);
        EditText joyride_expiry = shippingDialog.findViewById(R.id.joyride_expiry);
        EditText joyride_balance = shippingDialog.findViewById(R.id.joyride_balance);

        // if(dataBeanSR.getCreatedat() != null) creatat.setText(""+serviceReminderDataList.get(position).getCreatedat().toString().replaceAll("T00:00:00",""));

        if (dataBeanSR != null) {
            if (dataBeanSR.getVin() != null && vin != null) vin.setText("" + dataBeanSR.getVin());
            if (dataBeanSR.getColor() != null)
                color.setText("" + dataBeanSR.getColor().toUpperCase());
            if (dataBeanSR.getHsrp_stage() != null)
                hsrp_stage.setText("" + dataBeanSR.getHsrp_stage().toUpperCase());
            if (dataBeanSR.getWarranty_start_date() != null && !dataBeanSR.getWarranty_start_date().isEmpty())
                warranty_start_date.setText("" + dateparse3(dataBeanSR.getWarranty_start_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getServiceupdate() != null)
                service_update.setText("" + dataBeanSR.getServiceupdate().toUpperCase());
            if (dataBeanSR.getMandatoryupdate() != null)
                mandatory_update.setText("" + dataBeanSR.getMandatoryupdate().toUpperCase());
            if (dataBeanSR.getHerosure() != null)
                herosure.setText("" + dataBeanSR.getHerosure().toUpperCase());
            if (dataBeanSR.getAvg_km_run() != null)
                avg_km_run.setText("" + dataBeanSR.getAvg_km_run());
            if (dataBeanSR.getDigitalcustomer() != null)
                digital_customer.setText("" + dataBeanSR.getDigitalcustomer().toUpperCase());
            if (dataBeanSR.getCustomerrating() != null)
                customer_rating.setText("" + dataBeanSR.getCustomerrating());
            if (dataBeanSR.getAddress() != null)
                address.setText("" + dataBeanSR.getAddress().toUpperCase());
            if (dataBeanSR.getName_and_mobileno_multiple() != null)
                multiple_nameandmobile.setText("" + dataBeanSR.getName_and_mobileno_multiple().toUpperCase());
            if (dataBeanSR.getNext_service_due_date() != null && !dataBeanSR.getNext_service_due_date().isEmpty())
                next_service_due_date.setText("" + dateparse3(dataBeanSR.getNext_service_due_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getReminder_followup_date() != null && !dataBeanSR.getReminder_followup_date().isEmpty())
                reminder_followup_date.setText("" + dateparse3(dataBeanSR.getReminder_followup_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getInsurance_expiry_date() != null && !dataBeanSR.getInsurance_expiry_date().isEmpty())
                insurance_expiry_date.setText("" + dateparse3(dataBeanSR.getInsurance_expiry_date().replace("T", " ")));
            if (dataBeanSR.getGl_expiry() != null && !dataBeanSR.getGl_expiry().isEmpty())
                gl_expiry.setText("" + dateparse3(dataBeanSR.getGl_expiry().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getGl_points() != null)
                gl_points.setText("" + dataBeanSR.getGl_points().toUpperCase());
            if (dataBeanSR.getLs_expiry() != null && !dataBeanSR.getLs_expiry().isEmpty())
                ls_expiry.setText("" + dateparse3(dataBeanSR.getLs_expiry().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getLs_balance() != null)
                ls_balance.setText("" + dataBeanSR.getLs_balance().toUpperCase());
            if (dataBeanSR.getJoyride_expiry() != null && !dataBeanSR.getJoyride_expiry().isEmpty())
                joyride_expiry.setText("" + dateparse3(dataBeanSR.getJoyride_expiry().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getJoyride_balance() != null)
                joyride_balance.setText("" + dataBeanSR.getJoyride_balance().toUpperCase());


            if (dataBeanSR.getLast_service_date() != null && !dataBeanSR.getLast_service_date().isEmpty())

            last_service_date.setText(dateparse3(dataBeanSR.getLast_service_date().replace("T", " ").substring(0,10)));

            last_service_type.setText(dataBeanSR.getLast_service_type());

            last_jc_km.setText(dataBeanSR.getLast_jc_km().toUpperCase());
            last_invoice_value.setText(dataBeanSR.getLast_invoice_value().toUpperCase());
            open_complaint_number.setText(dataBeanSR.getOpen_complaint_number().toUpperCase());
            open_sr_number.setText(dataBeanSR.getOpen_sr_number().toUpperCase());
            if (dataBeanSR.getFsc1_date() != null && !dataBeanSR.getFsc1_date().isEmpty())

                fsc1_date.setText(dateparse3(dataBeanSR.getFsc1_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getFsc2_date() != null && !dataBeanSR.getFsc2_date().isEmpty())

                fsc2_date.setText(dateparse3(dataBeanSR.getFsc2_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getFsc3_date() != null && !dataBeanSR.getFsc3_date().isEmpty())

                fsc3_date.setText(dateparse3(dataBeanSR.getFsc3_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getFsc4_date() != null && !dataBeanSR.getFsc4_date().isEmpty())

                fsc4_date.setText(dateparse3(dataBeanSR.getFsc4_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getFsc5_date() != null && !dataBeanSR.getFsc5_date().isEmpty())

                fsc5_date.setText(dateparse3(dataBeanSR.getFsc5_date().replace("T", " ").substring(0,10)));
            if (dataBeanSR.getPaid1_date() != null && !dataBeanSR.getPaid1_date().isEmpty())

                paid1_date.setText(dateparse3(dataBeanSR.getPaid1_date().replace("T", " ").substring(0,10)));

            organization.setText(dataBeanSR.getOrganization().toUpperCase());

            edit_number.setText(dataBeanSR.getCusotmercontact().toUpperCase());

//            edit_number.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//
//                    final Dialog shippingDialog = new Dialog(contextw);
//                    shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    shippingDialog.setContentView(R.layout.mobile_number_change_popup);
//                    shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//                    shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    shippingDialog.setCancelable(true);
//                    shippingDialog.setCanceledOnTouchOutside(false);
//                    EditText et_newnumber = (EditText) shippingDialog.findViewById(R.id.setupnewEdit);
//                    et_newnumber.setText("" + dataBeanSR.getCusotmercontact());
//                    Button done_btn = (Button) shippingDialog.findViewById(R.id.setupnewDone);
//                    Button cancel_btn = (Button) shippingDialog.findViewById(R.id.setupnewCancle);
//                    done_btn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (et_newnumber.getText().length() == 10) {
//                                if (!dataBeanSR.getCusotmercontact().equals(et_newnumber.getText().toString())) {
//                                    UpdateContatctNumber(token, dataBeanSR.getId(), historyID, et_newnumber.getText().toString(),
//                                            "Alternate");
//
//                                } else {
//                                    Toast.makeText(contextw, " number already exist", Toast.LENGTH_SHORT).show();
//                                }
//
//                            } else {
//                                Toast.makeText(contextw, "Please check number", Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//                    });
//
//
//                    shippingDialog.show();
//                    TextView title = (TextView) shippingDialog.findViewById(R.id.setupnewtitle);
//                    title.setText("Do You Want to Change Customer Mobile Number ?");
//
//
//                    return false;
//                }
//            });
        }


    }

    public void holdCall(String contatcnumber) {
        OngoingCall.call.hold();
    }

    public void unHoldCall(String contatcnumber) {
        OngoingCall.call.unhold();

    }

    // Send WhatsAPP message to this function
    public void sendWhatsappMessage(String number) {

        whatsappclicked = true;
        whatsapp_number = number;
        //   Toast.makeText(contextw, number, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(contextw);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(contextw, this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(contextw, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, contextw.getString(R.string.SRTAG));


    }

    // Send Text Message to this Function
    public void sendtextmessage(String contactnumber) {


        textmesasageclicked = true;
        whatsapp_number = contactnumber;
        // Toast.makeText(contextw, contactnumber, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(contextw);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Whatsapp_shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(contextw, this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(contextw, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, contextw.getString(R.string.SRTAG));


    }

    //Get SMS Template
    public void getsmstemplate(String token, String templatetype) {
        templatelist.clear();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {

            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(contextw.getPackageManager()) != null) {
                    sendMsg.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    contextw.startActivity(sendMsg);
                    S_R_Activity.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(contextw, " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        } else if (textmesasageclicked == true) {

            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            contextw.startActivity(intent);
            S_R_Activity.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }

    //show customer Historydata popup
    public void getHistoryDetailsSales(String masterID) {

        getSalesHistoryData(token, masterID);

        final Dialog shippingDialog = new Dialog(contextw);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(contextw, RecyclerView.VERTICAL, false));
        adapter_sales_historydata = new Adapter_Sales_HistoryIncomingdata(contextw, salesReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sales_historydata);
        adapter_sales_historydata.notifyDataSetChanged();
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });

        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customDialog.dismiss();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        if (salesReminderHistoryDataList.size() > 0) {

          //  Log.e("Historydata"," "+salesReminderHistoryDataList.size());
          //  Toast.makeText(contextw, "history "+salesReminderHistoryDataList.size(), Toast.LENGTH_SHORT).show();
            shippingDialog.show();
        } else {

            Toasty.warning(contextw, "History data not found", Toast.LENGTH_SHORT).show();

        }
        //Toast.makeText(contextw, "history 2"+salesReminderHistoryDataList.size(), Toast.LENGTH_SHORT).show();
      //  Log.e("Historydata2"," "+salesReminderHistoryDataList.size());


    }

    //show customer MoreDetailsData popup
    public void getMoreDetailsSales(Incoming_SalesModel.DataBean dataBeanSales) {
        final Dialog shippingDialog = new Dialog(contextw);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.sales_incomming_more_datapopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);

        // ID genration of More Deatils PopUp
        /*------------------------------------------------------------------------------------------*/
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        EditText sale_nextfollowupdate = shippingDialog.findViewById(R.id.sale_next_follow_date);
        EditText sale_email = shippingDialog.findViewById(R.id.sales_email_id);
        EditText sale_address = shippingDialog.findViewById(R.id.sale_address);
        EditText sale_age = shippingDialog.findViewById(R.id.sales_age);
        EditText sale_gender = shippingDialog.findViewById(R.id.sales_gender);
        EditText sale_model_intersted = shippingDialog.findViewById(R.id.sales_model_interested);
        EditText sale_exchange_required = shippingDialog.findViewById(R.id.sales_exchange_required);
        EditText finance_required = shippingDialog.findViewById(R.id.sales_fianance_required);
        EditText sale_DSE_name = shippingDialog.findViewById(R.id.sales_dse_name);
        EditText sales_employeeid = shippingDialog.findViewById(R.id.sales_DSE_Employee_ID);
        EditText sales_enquiryid = shippingDialog.findViewById(R.id.sales_Enquiry_ID);
        EditText sales_enquirysource = shippingDialog.findViewById(R.id.sales_Enquiry_source);
        EditText sales_testriderequired = shippingDialog.findViewById(R.id.sales_testriderequired);
        EditText sales_requiredtime = shippingDialog.findViewById(R.id.sales_Test_Required_Time);
        EditText sales_testridetaken = shippingDialog.findViewById(R.id.sales_testridetaken);
        EditText testridedate = shippingDialog.findViewById(R.id.sales_testridedate);
        EditText sales_leader = shippingDialog.findViewById(R.id.sales_leader);
        EditText sales_comments = shippingDialog.findViewById(R.id.sales_comments);
        EditText sales_existing_vehicle = shippingDialog.findViewById(R.id.sales_existingvehicles);
        EditText sales_awarness_resorce = shippingDialog.findViewById(R.id.sales_awerness_resorce);


        shippingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customDialog.dismiss();
            }
        });

        /*------------------------------------------------------------------------------------------*/


        // Vale Set  on Edit Texts
        /*------------------------------------------------------------------------------------------*/
        if (dataBeanSales.getNext_followup_date() != null)
            sale_nextfollowupdate.setText("" +dateparse3(dataBeanSales.getNext_followup_date().replaceAll("T", " ").substring(0,10)));
        if (dataBeanSales.getEmailid() != null)
            sale_email.setText("" + dataBeanSales.getEmailid().toUpperCase());
        if (dataBeanSales.getAddress() != null)
            sale_address.setText("" + dataBeanSales.getAddress().toUpperCase());
        if (dataBeanSales.getAge() != null)
            sale_age.setText("" + dataBeanSales.getAge().toUpperCase());
        if (dataBeanSales.getGender() != null)
            sale_gender.setText("" + dataBeanSales.getGender().toUpperCase());
        if (dataBeanSales.getModel_interested_in() != null)
            sale_model_intersted.setText("" + dataBeanSales.getModel_interested_in().toUpperCase());
        if (dataBeanSales.getExchange_required() != null)
            sale_exchange_required.setText("" + dataBeanSales.getExchange_required().toUpperCase());
        if (dataBeanSales.getFinance_required() != null)
            finance_required.setText("" + dataBeanSales.getFinance_required().toUpperCase());
        if (dataBeanSales.getDse_employee_id() != null)
            sales_employeeid.setText("" + dataBeanSales.getDse_employee_id().toUpperCase());
        if (dataBeanSales.getEnquiry_id() != null)
            sales_enquiryid.setText("" + dataBeanSales.getEnquiry_id().toUpperCase());
        if (dataBeanSales.getEnquiry_source() != null)
            sales_enquirysource.setText("" + dataBeanSales.getEnquiry_source().toUpperCase());
        if (dataBeanSales.getTest_ride_required() != null)
            sales_testriderequired.setText("" + dataBeanSales.getTest_ride_required().toUpperCase());
        if (dataBeanSales.getTest_ride_required_time() != null)
            sales_requiredtime.setText("" + dataBeanSales.getTest_ride_required_time().toUpperCase());
        if (dataBeanSales.getTest_ride_taken() != null)
            sales_testridetaken.setText("" + dataBeanSales.getTest_ride_taken().toUpperCase());
        if (dataBeanSales.getTest_ride_taken_time() != null)
            testridedate.setText("" + dataBeanSales.getTest_ride_taken_time().toUpperCase());
        if (dataBeanSales.getOpinion_leader() != null)
            sales_leader.setText("" + dataBeanSales.getOpinion_leader().toUpperCase());
        if (dataBeanSales.getEnquiry_comments() != null)
            sales_comments.setText("" + dataBeanSales.getEnquiry_comments().toUpperCase());
        if (dataBeanSales.getExisting_vehicle() != null)
            sales_existing_vehicle.setText("" + dataBeanSales.getExisting_vehicle().toUpperCase());
        if (dataBeanSales.getAwareness_source() != null)
            sales_awarness_resorce.setText("" + dataBeanSales.getAwareness_source().toUpperCase());
        /*------------------------------------------------------------------------------------------*/


        // Click Listener
        /*------------------------------------------------------------------------------------------*/
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        /*------------------------------------------------------------------------------------------*/


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            shippingDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();

      //  contextw.startService(new Intent(contextw,ReceiveCallService.class));


    }


    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
       String newDateFormat="";

        try {
            date = inputFormat.parse(inputdate);
            newDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDateFormat;

    }


    private void declinePhone(Context context) throws Exception {

        try {

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();
            //////Log.d("unable", "msg cant dissconect call....");

        }
    }


    private void rejectCall(){


        try {
            TelephonyManager telephonyManager;

            telephonyManager= (TelephonyManager) contextw.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class<?> classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method method = classTelephony.getDeclaredMethod("getITelephony");
            // Disable access check
            method.setAccessible(true);
            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = method.invoke(telephonyManager);
            // Get the endCall method from ITelephony
            Class<?> telephonyInterfaceClass =Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public void disconnectCall() {
        try {

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();

        }
    }






}

