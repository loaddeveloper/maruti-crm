package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class FollowUpListModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"Status":"contacted"},{"Status":"not_contacted"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Status : contacted
         */

        private String Status;

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }
    }
}
