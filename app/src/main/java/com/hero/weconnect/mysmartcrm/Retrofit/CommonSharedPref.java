package com.hero.weconnect.mysmartcrm.Retrofit;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hero.weconnect.mysmartcrm.models.TokenModel;

import java.lang.reflect.Type;

public class CommonSharedPref {
    private Context mContext;
    private SharedPreferences.Editor editor, editorpackages;
    private SharedPreferences sharedPreferences, sharedPreferencespackages;

    public CommonSharedPref(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("logindata", Context.MODE_PRIVATE);
        sharedPreferencespackages = mContext.getSharedPreferences("PackagesList", Context.MODE_PRIVATE);

    }

    public TokenModel getLoginData() {

        String json = sharedPreferences.getString(ApiConstant.TOKEN_TAG, null);
        Gson gson = new Gson();
        Type type = new TypeToken<TokenModel>() {
        }.getType();

        TokenModel beans = null;
        beans = gson.fromJson(json, type);

        return beans;

    }

    public void setLoginData(TokenModel beans) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString(ApiConstant.TOKEN_TAG, json);
        editor.commit();
    }


    public String getPackagesList() {

        String packagename = sharedPreferencespackages.getString(ApiConstant.PACKAGEADD, null);


        return packagename;

    }

    public void setPackages(String packages) {
        editorpackages = sharedPreferencespackages.edit();
        editorpackages.putString(ApiConstant.PACKAGEADD, packages);
        editorpackages.commit();
    }

    public void clearpackages() {
        editorpackages = sharedPreferencespackages.edit();
        editorpackages.clear();
        editorpackages.commit();
    }


    public void setAllClear() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }


}
