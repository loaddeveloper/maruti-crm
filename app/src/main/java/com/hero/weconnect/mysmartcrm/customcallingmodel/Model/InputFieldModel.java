package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

public class InputFieldModel extends SuperClassCastBean {


    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : {"Booking_Date":"YES","Call_Progress_Status":"NO","Is_Closed":"NO","Next_Follow_Up_Date":"NO","Remark":"NO"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private DataDTO Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataDTO getData() {
        return Data;
    }

    public void setData(DataDTO Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Booking_Date : YES
         * Call_Progress_Status : NO
         * Is_Closed : NO
         * Next_Follow_Up_Date : NO
         * Remark : NO
         */

        @SerializedName("Booking_Date")
        private String BookingDate;
        @SerializedName("Call_Progress_Status")
        private String CallProgressStatus;
        @SerializedName("Is_Closed")
        private String IsClosed;
        @SerializedName("Next_Follow_Up_Date")
        private String NextFollowUpDate;
        @SerializedName("Remark")
        private String Remark;

        public String getBookingDate() {
            return BookingDate;
        }

        public void setBookingDate(String BookingDate) {
            this.BookingDate = BookingDate;
        }

        public String getCallProgressStatus() {
            return CallProgressStatus;
        }

        public void setCallProgressStatus(String CallProgressStatus) {
            this.CallProgressStatus = CallProgressStatus;
        }

        public String getIsClosed() {
            return IsClosed;
        }

        public void setIsClosed(String IsClosed) {
            this.IsClosed = IsClosed;
        }

        public String getNextFollowUpDate() {
            return NextFollowUpDate;
        }

        public void setNextFollowUpDate(String NextFollowUpDate) {
            this.NextFollowUpDate = NextFollowUpDate;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }
    }
}
