package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class ServiceReminderDataModel extends SuperClassCastBean {



    /**
     * Message : Success
     * Data : [{"sno":1,"id":"0b2434bc-8375-47ae-a0bd-000ce014502c","createdat":null,"vin":"MBLHAR204J4M00476","registration_number":"","model":"HF DELUXE","color":"PBK","hsrp_stage":"","warranty_start_date":"2019-02-19 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"48","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"PRABHULAL","customer_last_name":"CHARPOTA","cusotmercontact":"1111111111","address":"VI- MAJIYA ,PO- NAHALI GARHI","name_and_mobileno_multiple":"PRABHULAL CHARPOTA 1111111111","last_service_date":"2019-04-24 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"3065","organization":"SURBHI AUTOMOBILES","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-03-18 12:42:03.0000000","fsc2_date":"2019-04-24 10:16:55.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-06-26 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":2,"id":"bd366771-30e2-41ff-bde1-000d9f7435d2","createdat":null,"vin":"MBLHAR074KHA84607","registration_number":"TN42AC7086","model":"SPLENDOR +","color":"VBK","hsrp_stage":"","warranty_start_date":"2019-02-07 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"50","digitalcustomer":"Y","customerrating":"","cusotmer_first_name":"KARTHIKEYAN","customer_last_name":"R","cusotmercontact":"1111111111","address":"D NO 4/407 T ANDIPALAYAM THONGUTTIPALAYAMTIRUPPUR","name_and_mobileno_multiple":"KARTHIKEYAN R 1111111111","last_service_date":"2019-04-15 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"3335","organization":"T.K.T MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-02-25 13:19:16.0000000","fsc2_date":"2019-04-15 18:02:52.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-06-14 00:00:00.0000000","reminder_followup_date":"2019-04-10 00:00:00.0000000","insurance_expiry_date":"","gl_expiry":"21/02/2022","gl_points":"1595","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"2019-04-10 00:00:00.0000000","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":3,"id":"b9b6257b-4be8-4916-a943-000f42b86a0d","createdat":null,"vin":"MBLJAR027KGA06933","registration_number":"","model":"GLAMOUR","color":"TBK","hsrp_stage":"","warranty_start_date":"2019-02-11 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"27","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"KEDHASI","customer_last_name":"AJAY KUMAR","cusotmercontact":"1111111111","address":"# 7TH WARD SC COLONY BODDU CHINTHALAPALLI GEESUGONDA","name_and_mobileno_multiple":"KEDHASI AJAY KUMAR 1111111111","last_service_date":"2019-03-11 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"750","organization":"SRI SRI SRI AUTOMOBILES","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-03-11 11:14:42.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-06-19 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":4,"id":"0a95b881-10cd-4863-acda-0010a9b6cc4b","createdat":null,"vin":"MBLHAR182J5L01303","registration_number":"","model":"PASSION PRO","color":"BHG","hsrp_stage":"","warranty_start_date":"2019-02-19 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"17","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"SHREYAS JAISWAL","customer_last_name":"S/O SATISH JAISWAL","cusotmercontact":"1111111111","address":"FL NO.E-1001. 10FLR GOODWILL PARADISE PL NO.24 SEC-15 NR DMART PANVEL","name_and_mobileno_multiple":"SHREYAS JAISWAL S/O SATISH JAISWAL 1111111111","last_service_date":"2019-04-03 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"750","organization":"H.M.MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-04-03 10:27:33.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-07-12 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":5,"id":"b1b06608-3ebf-4392-9c45-00121c491c55","createdat":null,"vin":"MBLHAR186JHL09354","registration_number":"","model":"PASSION PRO","color":"BHG","hsrp_stage":"","warranty_start_date":"2019-02-11 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"40","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"BABU","customer_last_name":"L","cusotmercontact":"1111111111","address":"NO:1/27,SAVEUJAR LKOVIL KEELA STREET VALLAMTHANJAVUR","name_and_mobileno_multiple":"BABU L 1111111111","last_service_date":"2019-04-30 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"3142","organization":"ABI & ABI MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-03-12 10:27:03.0000000","fsc2_date":"2019-04-30 09:52:55.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-07-13 00:00:00.0000000","reminder_followup_date":"2019-04-30 00:00:00.0000000","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"2019-04-30 00:00:00.0000000","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":6,"id":"17b70fb5-9dd1-40ec-aa4b-00121f0e1018","createdat":null,"vin":"MBLJAR031J9J03344","registration_number":"","model":"SUPER SPLENDOR","color":"GBK","hsrp_stage":"","warranty_start_date":"2019-02-11 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"39","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"BIRBAL","customer_last_name":"RAY","cusotmercontact":"1111111111","address":"AT-NIKHRA,WARD NO-13 P.O-PARBHELI,PS-KADWAKAMRAILI","name_and_mobileno_multiple":"BIRBAL RAY 1111111111","last_service_date":"2019-04-20 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"2650","organization":"SHIVAM MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-02-28 14:49:32.0000000","fsc2_date":"2019-04-20 11:06:37.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-07-06 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"12/02/2022","gl_points":"175","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":7,"id":"49476832-d2f8-41bb-aa8c-001269d33bac","createdat":null,"vin":"MBLHAW07XK9A31323","registration_number":"","model":"HF DELUXE","color":"BLK","hsrp_stage":"","warranty_start_date":"2019-02-07 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"33","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"MUKESH KUMAR","customer_last_name":"BHARDWAJ","cusotmercontact":"1111111111","address":"KAITHA,BILAIGARH,BALODA BAZAR,BHATAPARA C/O C.G.AUTO CARE,BHATAGAON,RING ROAD NO.01RAIPUR","name_and_mobileno_multiple":"MUKESH KUMAR BHARDWAJ 1111111111","last_service_date":"2019-02-25 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"590","organization":"kiran auto","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-02-25 17:32:14.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-05-31 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"20/02/2022","gl_points":"175","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":8,"id":"fd91c950-be98-4617-9b21-0012dee3d9bf","createdat":null,"vin":"MBLJAS029J9J05423","registration_number":"","model":"GLAMOUR","color":"TBK","hsrp_stage":"","warranty_start_date":"2019-02-19 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"20","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"SUNUMON S","customer_last_name":".","cusotmercontact":"1111111111","address":"ALINTE THEKKETHIL(H),PASUPPARA P.O,LAKSHAM VEEDU IDUKKI","name_and_mobileno_multiple":"SUNUMON S . 1111111111","last_service_date":"2019-03-22 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"620","organization":"CHENNATTUMATTAM MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-03-22 15:25:44.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-06-30 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":9,"id":"49d8e518-b37f-4422-aaa2-0014497256c0","createdat":null,"vin":"MBLJAR032J9D05760","registration_number":"","model":"SUPER SPLENDOR","color":"VBK","hsrp_stage":"","warranty_start_date":"2019-02-07 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"9","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"SALIKARAM","customer_last_name":".","cusotmercontact":"1111111111","address":"GANGAPUR PANDEY PO- AMODHARA CHHAWANI TEH- HARRAIYA BASTI - 272127 C/O- ANOOP SINGH AACHARY NAGAR JANAURAFAIZABAD","name_and_mobileno_multiple":"SALIKARAM . 1111111111","last_service_date":"2019-03-19 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"346","organization":"VANSHADI MOTORS","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-03-19 12:50:36.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-06-27 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"08/02/2022","gl_points":"575","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"},{"sno":10,"id":"3f01bd66-6855-4b31-ab7c-0015ebf9bca6","createdat":null,"vin":"MBLJFN029JGH17560","registration_number":"","model":"DUET","color":"MVG","hsrp_stage":"","warranty_start_date":"2019-02-07 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"39","digitalcustomer":"N","customerrating":"","cusotmer_first_name":"VISHALBHAI ARVINDBHAI","customer_last_name":"GAMIT","cusotmercontact":"1111111111","address":" BAGALPUR","name_and_mobileno_multiple":"VISHALBHAI ARVINDBHAI GAMIT 1111111111","last_service_date":"2019-03-08 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"1121","organization":"DESAI AUTOMOBILES","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-02-14 11:38:45.0000000","fsc2_date":"2019-03-08 10:26:13.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2019-05-25 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","contact_status":"","calledstatus":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","followup_date":"","not_coming_reason":"","comments":"","ServiceType":"Srvce_Skpd_FSC5"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * id : 0b2434bc-8375-47ae-a0bd-000ce014502c
         * createdat : null
         * vin : MBLHAR204J4M00476
         * registration_number :
         * model : HF DELUXE
         * color : PBK
         * hsrp_stage :
         * warranty_start_date : 2019-02-19 00:00:00.0000000
         * serviceupdate : N
         * mandatoryupdate : N
         * herosure : N
         * herosure_date :
         * avg_km_run : 48
         * digitalcustomer : N
         * customerrating :
         * cusotmer_first_name : PRABHULAL
         * customer_last_name : CHARPOTA
         * cusotmercontact : 1111111111
         * address : VI- MAJIYA ,PO- NAHALI GARHI
         * name_and_mobileno_multiple : PRABHULAL CHARPOTA 1111111111
         * last_service_date : 2019-04-24 00:00:00.0000000
         * last_service_type : FSC
         * last_jc_km : 3065
         * organization : SURBHI AUTOMOBILES
         * last_invoice_value :
         * open_complaint_number :
         * open_sr_number :
         * fsc1_date : 2019-03-18 12:42:03.0000000
         * fsc2_date : 2019-04-24 10:16:55.0000000
         * fsc3_date :
         * fsc4_date :
         * fsc5_date :
         * paid1_date :
         * next_service_due_date : 2019-06-26 00:00:00.0000000
         * reminder_followup_date :
         * insurance_expiry_date :
         * gl_expiry :
         * gl_points :
         * ls_expiry :
         * ls_balance :
         * joyride_expiry :
         * joyride_balance :
         * contact_status :
         * calledstatus :
         * customerreply :
         * servicebooking : 1900-01-01T00:00:00
         * followup_date :
         * not_coming_reason :
         * comments :
         * DataCount :
         * Title :
         * ServiceType : Srvce_Skpd_FSC5
         */

        private int sno;
        private String id;
        private Object createdat;
        private String vin;
        private String registration_number;
        private String model;
        private String color;
        private String hsrp_stage;
        private String warranty_start_date;
        private String serviceupdate;
        private String mandatoryupdate;
        private String herosure;
        private String herosure_date;
        private String avg_km_run;
        private String digitalcustomer;
        private String customerrating;
        private String cusotmer_first_name;
        private String customer_last_name;
        private String cusotmercontact;
        private String address;
        private String name_and_mobileno_multiple;
        private String last_service_date;
        private String last_service_type;
        private String last_jc_km;
        private String organization;
        private String last_invoice_value;
        private String open_complaint_number;
        private String open_sr_number;
        private String fsc1_date;
        private String fsc2_date;
        private String fsc3_date;
        private String fsc4_date;
        private String fsc5_date;
        private String paid1_date;
        private String next_service_due_date;
        private String reminder_followup_date;
        private String insurance_expiry_date;
        private String gl_expiry;
        private String gl_points;
        private String ls_expiry;
        private String ls_balance;
        private String joyride_expiry;
        private String joyride_balance;
        private String contact_status;
        private String calledstatus;
        private String customerreply;
        private String servicebooking;
        private String followup_date;
        private String not_coming_reason;
        private String comments;

        public String getDataCount() {
            return DataCount;
        }

        public void setDataCount(String dataCount) {
            DataCount = dataCount;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        private String DataCount;
        private String Title;
        private String ServiceType;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getCreatedat() {
            return createdat;
        }

        public void setCreatedat(Object createdat) {
            this.createdat = createdat;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getRegistration_number() {
            return registration_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getHsrp_stage() {
            return hsrp_stage;
        }

        public void setHsrp_stage(String hsrp_stage) {
            this.hsrp_stage = hsrp_stage;
        }

        public String getWarranty_start_date() {
            return warranty_start_date;
        }

        public void setWarranty_start_date(String warranty_start_date) {
            this.warranty_start_date = warranty_start_date;
        }

        public String getServiceupdate() {
            return serviceupdate;
        }

        public void setServiceupdate(String serviceupdate) {
            this.serviceupdate = serviceupdate;
        }

        public String getMandatoryupdate() {
            return mandatoryupdate;
        }

        public void setMandatoryupdate(String mandatoryupdate) {
            this.mandatoryupdate = mandatoryupdate;
        }

        public String getHerosure() {
            return herosure;
        }

        public void setHerosure(String herosure) {
            this.herosure = herosure;
        }

        public String getHerosure_date() {
            return herosure_date;
        }

        public void setHerosure_date(String herosure_date) {
            this.herosure_date = herosure_date;
        }

        public String getAvg_km_run() {
            return avg_km_run;
        }

        public void setAvg_km_run(String avg_km_run) {
            this.avg_km_run = avg_km_run;
        }

        public String getDigitalcustomer() {
            return digitalcustomer;
        }

        public void setDigitalcustomer(String digitalcustomer) {
            this.digitalcustomer = digitalcustomer;
        }

        public String getCustomerrating() {
            return customerrating;
        }

        public void setCustomerrating(String customerrating) {
            this.customerrating = customerrating;
        }

        public String getCusotmer_first_name() {
            return cusotmer_first_name;
        }

        public void setCusotmer_first_name(String cusotmer_first_name) {
            this.cusotmer_first_name = cusotmer_first_name;
        }

        public String getCustomer_last_name() {
            return customer_last_name;
        }

        public void setCustomer_last_name(String customer_last_name) {
            this.customer_last_name = customer_last_name;
        }

        public String getCusotmercontact() {
            return cusotmercontact;
        }

        public void setCusotmercontact(String cusotmercontact) {
            this.cusotmercontact = cusotmercontact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName_and_mobileno_multiple() {
            return name_and_mobileno_multiple;
        }

        public void setName_and_mobileno_multiple(String name_and_mobileno_multiple) {
            this.name_and_mobileno_multiple = name_and_mobileno_multiple;
        }

        public String getLast_service_date() {
            return last_service_date;
        }

        public void setLast_service_date(String last_service_date) {
            this.last_service_date = last_service_date;
        }

        public String getLast_service_type() {
            return last_service_type;
        }

        public void setLast_service_type(String last_service_type) {
            this.last_service_type = last_service_type;
        }

        public String getLast_jc_km() {
            return last_jc_km;
        }

        public void setLast_jc_km(String last_jc_km) {
            this.last_jc_km = last_jc_km;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getLast_invoice_value() {
            return last_invoice_value;
        }

        public void setLast_invoice_value(String last_invoice_value) {
            this.last_invoice_value = last_invoice_value;
        }

        public String getOpen_complaint_number() {
            return open_complaint_number;
        }

        public void setOpen_complaint_number(String open_complaint_number) {
            this.open_complaint_number = open_complaint_number;
        }

        public String getOpen_sr_number() {
            return open_sr_number;
        }

        public void setOpen_sr_number(String open_sr_number) {
            this.open_sr_number = open_sr_number;
        }

        public String getFsc1_date() {
            return fsc1_date;
        }

        public void setFsc1_date(String fsc1_date) {
            this.fsc1_date = fsc1_date;
        }

        public String getFsc2_date() {
            return fsc2_date;
        }

        public void setFsc2_date(String fsc2_date) {
            this.fsc2_date = fsc2_date;
        }

        public String getFsc3_date() {
            return fsc3_date;
        }

        public void setFsc3_date(String fsc3_date) {
            this.fsc3_date = fsc3_date;
        }

        public String getFsc4_date() {
            return fsc4_date;
        }

        public void setFsc4_date(String fsc4_date) {
            this.fsc4_date = fsc4_date;
        }

        public String getFsc5_date() {
            return fsc5_date;
        }

        public void setFsc5_date(String fsc5_date) {
            this.fsc5_date = fsc5_date;
        }

        public String getPaid1_date() {
            return paid1_date;
        }

        public void setPaid1_date(String paid1_date) {
            this.paid1_date = paid1_date;
        }

        public String getNext_service_due_date() {
            return next_service_due_date;
        }

        public void setNext_service_due_date(String next_service_due_date) {
            this.next_service_due_date = next_service_due_date;
        }

        public String getReminder_followup_date() {
            return reminder_followup_date;
        }

        public void setReminder_followup_date(String reminder_followup_date) {
            this.reminder_followup_date = reminder_followup_date;
        }

        public String getInsurance_expiry_date() {
            return insurance_expiry_date;
        }

        public void setInsurance_expiry_date(String insurance_expiry_date) {
            this.insurance_expiry_date = insurance_expiry_date;
        }

        public String getGl_expiry() {
            return gl_expiry;
        }

        public void setGl_expiry(String gl_expiry) {
            this.gl_expiry = gl_expiry;
        }

        public String getGl_points() {
            return gl_points;
        }

        public void setGl_points(String gl_points) {
            this.gl_points = gl_points;
        }

        public String getLs_expiry() {
            return ls_expiry;
        }

        public void setLs_expiry(String ls_expiry) {
            this.ls_expiry = ls_expiry;
        }

        public String getLs_balance() {
            return ls_balance;
        }

        public void setLs_balance(String ls_balance) {
            this.ls_balance = ls_balance;
        }

        public String getJoyride_expiry() {
            return joyride_expiry;
        }

        public void setJoyride_expiry(String joyride_expiry) {
            this.joyride_expiry = joyride_expiry;
        }

        public String getJoyride_balance() {
            return joyride_balance;
        }

        public void setJoyride_balance(String joyride_balance) {
            this.joyride_balance = joyride_balance;
        }

        public String getContact_status() {
            return contact_status;
        }

        public void setContact_status(String contact_status) {
            this.contact_status = contact_status;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getCustomerreply() {
            return customerreply;
        }

        public void setCustomerreply(String customerreply) {
            this.customerreply = customerreply;
        }

        public String getServicebooking() {
            return servicebooking;
        }

        public void setServicebooking(String servicebooking) {
            this.servicebooking = servicebooking;
        }

        public String getFollowup_date() {
            return followup_date;
        }

        public void setFollowup_date(String followup_date) {
            this.followup_date = followup_date;
        }

        public String getNot_coming_reason() {
            return not_coming_reason;
        }

        public void setNot_coming_reason(String not_coming_reason) {
            this.not_coming_reason = not_coming_reason;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getServiceType() {
            return ServiceType;
        }

        public void setServiceType(String ServiceType) {
            this.ServiceType = ServiceType;
        }
    }
}
