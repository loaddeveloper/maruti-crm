package com.hero.weconnect.mysmartcrm.activity;



import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.adapter.AdapterEnqSource;
import com.hero.weconnect.mysmartcrm.adapter.AdapterModel;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.lang.Thread.sleep;

public class FilterActivity extends AppCompatActivity implements ApiResponseListener, AdapterModel.ModelClickListener , AdapterEnqSource.EnqSourceClickListener {

    RecyclerView rc_model,rc_enqsource;
    AdapterEnqSource adapterEnqSource;
    AdapterModel adapterModel;
    public ArrayList<String> modellist = new ArrayList<>();
    public ArrayList<String> enquirysourceList = new ArrayList<>();
    public ApiController apiController;
    Spinner sp_username;
    public ArrayList<String>[] userNameArrayList = new ArrayList[]{new ArrayList<String>()};
    public ArrayAdapter<String> userNameArrayAdapter;
    LinearLayout ll_model,ll_enqsource;
    int modelcount=1,enqsourcecount=1;
    ImageView iv_modelarrow,iv_enqsourcearrow;
    Button btn_submit,btn_reset;
    HashMap<String,String> params;
    RadioGroup rg_financereq,rg_exchagereq;
    CopyOnWriteArrayList<String> modelList= new CopyOnWriteArrayList<>();
    CopyOnWriteArrayList<String> modelList1= new CopyOnWriteArrayList<>();
    CopyOnWriteArrayList<String> enquirysourceList1= new CopyOnWriteArrayList<>();
    String exchangerequired,financereq;
    String followuptype="";
    CustomDialog customDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        enquirysourceList.add("Digital Leads");
        enquirysourceList.add("Telephonic");
        enquirysourceList.add("Walk-In");
        rc_enqsource=findViewById(R.id.rc_enq_source);
        rc_model=findViewById(R.id.rc_models);
        ll_model=findViewById(R.id.ll_model);
        iv_modelarrow = findViewById(R.id.model_arrow);
        btn_reset = findViewById(R.id.reset_btn);
        btn_submit = findViewById(R.id.submint_btn);
        rg_exchagereq = findViewById(R.id.rg_exc_req);
        rg_financereq = findViewById(R.id.rg_fin_req);
        iv_enqsourcearrow = findViewById(R.id.enqsource_arrow);
        ll_enqsource=findViewById(R.id.ll_enqsource);
        sp_username = findViewById(R.id.sp_user);
        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        toolbar.setTitle("Filter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        rc_enqsource.setHasFixedSize(true);
        rc_model.setHasFixedSize(true);
        customDialog= new CustomDialog(this);

        if(getIntent() != null)followuptype=getIntent().getStringExtra("followup");
        apiController = new ApiController(this,this);
        rc_model.setLayoutManager(new GridLayoutManager(this,2));
        rc_enqsource.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        adapterEnqSource= new AdapterEnqSource(this,enquirysourceList,this);
        adapterModel= new AdapterModel(this,modellist,this);
        rc_model.setAdapter(adapterModel);
        rc_enqsource.setAdapter(adapterEnqSource);

        getUserNameList(CommonVariables.dealercode);

        userNameArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, userNameArrayList[0]);
        sp_username.setAdapter(userNameArrayAdapter);

        if(!CommonVariables.userName.isEmpty() && userNameArrayAdapter.getCount()>0)
        {
            sp_username.setSelection(userNameArrayAdapter.getPosition(CommonVariables.userName));
        }

        sp_username.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                params = new HashMap<>();
                params.put("Type", ApiConstant.ENQUIRY);
                params.put("UserName", CommonVariables.userName);
                apiController.deAllocation( CommonVariables.token, params);
                String spinner_value = sp_username.getSelectedItem().toString();
                if (spinner_value.equals("ALL FOLLOWUP"))
                {
                    spinner_value = "All";
                }
                else
                {
                    spinner_value = sp_username.getSelectedItem().toString();
                }

                CommonVariables.userName= spinner_value;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                CommonVariables.userName= CommonVariables.UserId;
            }
        });


        if(CommonVariables.filetfinancerequired.equals("N"))
        {
            rg_financereq.check(R.id.rb_finno);

        }
        else if(CommonVariables.filetfinancerequired.equals("Y"))
        {
            rg_financereq.check(R.id.rb_finyes);

        }


        if(CommonVariables.filterexchnage.equals("N"))
        {
            rg_exchagereq.check(R.id.rb_excno);

        }else if(CommonVariables.filterexchnage.equals("Y"))
        {
            rg_exchagereq.check(R.id.rb_excyes);

        }


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   Log.e("ModelList22",""+new Gson().toJson(CommonVariables.filermodellist));
                Log.e("enquiryList22",""+new Gson().toJson(CommonVariables.filerenqsourcelist));
                Log.e("finList22",""+CommonVariables.filetfinancerequired);
                Log.e("exchangeList22",""+CommonVariables.filterexchnage);*/


                if(CommonVariables.APP.equals(CommonVariables.APP_Role_2))
                {


                    if(getIntent() != null && getIntent().getStringExtra("expected") != null && getIntent().getStringExtra("expected").equals("expected") )
                    {
                        CommonVariables.filtertype="Filter";
                        Intent intent=new Intent(FilterActivity.this,ExpectedDateOfPurchase_Activity.class);
                        intent.putExtra("class","sales");
                        intent.putExtra("followup",followuptype);
                        startActivity(intent);
                        finish();
                    }else
                    {
                        CommonVariables.filtertype="Filter";
                        Intent intent=new Intent(FilterActivity.this,Sales_Followup_Activity.class);
                        intent.putExtra("class","sales");
                        intent.putExtra("followup",followuptype);
                        startActivity(intent);
                        finish();
                    }

                }




            }
        });
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonVariables.filermodellist.clear();
                CommonVariables.filerenqsourcelist.clear();
                CommonVariables.filterexchnage="";
                CommonVariables.filetfinancerequired="";

                if(CommonVariables.APP.equals(CommonVariables.APP_Role_2))
                {
                    if(getIntent() != null && getIntent().getStringExtra("expected") != null && getIntent().getStringExtra("expected").equals("expected") )
                    {
                        CommonVariables.filtertype="Filter";
                        Intent intent=new Intent(FilterActivity.this,ExpectedDateOfPurchase_Activity.class);
                        intent.putExtra("class","sales");
                        intent.putExtra("followup",followuptype);
                        startActivity(intent);
                        finish();
                    }else
                    {
                        CommonVariables.filtertype="Filter";
                        Intent intent=new Intent(FilterActivity.this,Sales_Followup_Activity.class);
                        intent.putExtra("class","sales");
                        intent.putExtra("followup",followuptype);
                        startActivity(intent);
                        finish();
                    }

                }

            }
        });
        rg_financereq.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.rb_finyes:
                        CommonVariables.filetfinancerequired="Y";
                        break;

                    case R.id.rb_finno:
                        CommonVariables.filetfinancerequired="N";
                }

            }
        });
        rg_exchagereq.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                switch (checkedId)
                {
                    case R.id.rb_excyes:
                        CommonVariables.filterexchnage="Y";
                        break;

                    case R.id.rb_excno:
                        CommonVariables.filterexchnage="N";
                }

            }
        });


        ll_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelcount++;

                        if(modelcount % 2==0)
                        {
                            iv_modelarrow.setRotation(90);
                            rc_model.setVisibility(View.VISIBLE);


                        }
                        else
                        {
                            iv_modelarrow.setRotation(0);
                            rc_model.setVisibility(View.GONE);
                        }
            }
        });

        ll_enqsource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enqsourcecount++;

                if(enqsourcecount % 2==0)
                {
                    iv_enqsourcearrow.setRotation(90);
                    rc_enqsource.setVisibility(View.VISIBLE);


                }
                else
                {
                    iv_enqsourcearrow.setRotation(0);
                    rc_enqsource.setVisibility(View.GONE);
                }
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                CommonVariables.filermodellist.clear();
//                CommonVariables.filerenqsourcelist.clear();
//                CommonVariables.filterexchnage="N";
//                CommonVariables.filetfinancerequired="N";

                if(CommonVariables.APP.equals(CommonVariables.APP_Role_2))
                {
                     if(getIntent() != null && getIntent().getStringExtra("expected") != null && getIntent().getStringExtra("expected").equals("expected") )
                {
                    CommonVariables.filtertype="Filter";
                    Intent intent=new Intent(FilterActivity.this,ExpectedDateOfPurchase_Activity.class);
                    intent.putExtra("class","sales");
                    intent.putExtra("followup",followuptype);
                    startActivity(intent);
                    finish();
                }else
                {
                    CommonVariables.filtertype="Filter";
                    Intent intent=new Intent(FilterActivity.this,Sales_Followup_Activity.class);
                    intent.putExtra("class","sales");
                    intent.putExtra("followup",followuptype);
                    startActivity(intent);
                    finish();
                }

                }
            }
        });
       getModelData(CommonVariables.token);



       if(CommonVariables.filermodellist.size()>0)
       {
           ll_model.performClick();
       }


        if(CommonVariables.filerenqsourcelist.size()>0)
        {
            ll_enqsource.performClick();
        }
    }

    public void getUserNameList( String dealercode )
    {

        customDialog.show();
        params = new HashMap<>();
        params.put("DealerCode", dealercode);
        params.put("AppType", "HSA");
        apiController.getUserNameList(CommonVariables.token, params);
    }
    void getModelData(String token)
    {
        modellist.clear();
        customDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("Make", "HERO");
        apiController.getModel(token, params);
        //getModelData2(CommonVariables.token);
    }


/*    void getModelData2(String token)
    {
        customDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("Make", "HERO MOTORS LTD.");
        apiController.getModel(token, params);
       // getModelData3(CommonVariables.token);

    }*/

/*    void getModelData3(String token)
    {
        customDialog.show();

        HashMap<String, String> params = new HashMap<>();
        params.put("Make", "HERO ELECTRIC");
        apiController.getModel(token, params);
    }*/
    // OnSuccess API Response Function
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {
        if (beanTag.matches(ApiConstant.MODEL)) {
            ModelModel modelModel = (ModelModel) superClassCastBean;
            if (modelModel.getData() != null && modelModel.getData().size() > 0) {
                for (ModelModel.DataBean dataBean : modelModel.getData()) {
                    modellist.add(dataBean.getModel());

                }

                AdapterModel modelArrayAdapter = new AdapterModel(this, modellist,FilterActivity.this);
                rc_model.setAdapter(modelArrayAdapter);
                customDialog.dismiss();


            }

            customDialog.dismiss();

        }
        else if (beanTag.matches(ApiConstant.USERNAMELIST)) {
            com.hero.weconnect.mysmartcrm.models.UserNameListModel userNameListModel = (com.hero.weconnect.mysmartcrm.models.UserNameListModel) superClassCastBean;
            if (userNameListModel.getData() != null && userNameListModel.getData().size() > 0) {
                userNameArrayList[0].clear();
                userNameArrayList[0].add("ALL FOLLOWUP");
                for (com.hero.weconnect.mysmartcrm.models.UserNameListModel.DataBean dataBean : userNameListModel.getData()) {
                    userNameArrayList[0].add(dataBean.getUsername());

                }

                userNameArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, userNameArrayList[0]);
                sp_username.setAdapter(userNameArrayAdapter);

                for (int i=0;i<userNameArrayList[0].size();i++)
                {
                    if(CommonVariables.userName.equals(userNameArrayList[0].get(i)))
                    {
                        sp_username.setSelection(i);
                    }
                }
                customDialog.dismiss();
            }
        }
    }

    @Override
    public void onFailure(String msg) {
        customDialog.dismiss();

        if (msg.matches(ApiConstant.SALESENQUIRYDATA)) {
           // nodata_found_layout.setVisibility(View.VISIBLE);

        } else {
        }

    }

    @Override
    public void onError(String msg) {
        customDialog.dismiss();

    }

    @Override
    public void addmodel(int position, String model) {

        if(!CommonVariables.filermodellist.contains(model))CommonVariables.filermodellist.add(model);

    }

    @Override
    public void removeModel(int position, String model) {
        CommonVariables.filermodellist.remove(model);

    }

    @Override
    public void addEnqSource(int position, String enqSource) {
        if(!CommonVariables.filerenqsourcelist.contains(enqSource))CommonVariables.filerenqsourcelist.add(enqSource);

    }

    @Override
    public void removeEnqSource(int position, String enqSource) {
        CommonVariables.filerenqsourcelist.remove(enqSource);

    }


    @Override
    public void onBackPressed() {

        if(CommonVariables.APP.equals(CommonVariables.APP_Role_2))
        {
            if(getIntent() != null && getIntent().getStringExtra("expected") != null && getIntent().getStringExtra("expected").equals("expected") )
        {
            CommonVariables.filtertype="Filter";
            Intent intent=new Intent(FilterActivity.this,ExpectedDateOfPurchase_Activity.class);
            intent.putExtra("class","sales");
            intent.putExtra("followup",followuptype);
            startActivity(intent);
            finish();
        }else
        {
            CommonVariables.filtertype="Filter";
            Intent intent=new Intent(FilterActivity.this,Sales_Followup_Activity.class);
            intent.putExtra("class","sales");
            intent.putExtra("followup",followuptype);
            startActivity(intent);
            finish();
        }
        }else
        {
            finish();
        }


        super.onBackPressed();
    }

}