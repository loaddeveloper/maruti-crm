package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.io.Serializable;
import java.util.List;

public class UserNameListModel extends SuperClassCastBean
{


    /**
     * Message : Success
     * Data : [{"username":"DHIR10800"},{"username":"DPAK"},{"username":"GAG10800"},{"username":"TEST10800"},{"username":"TM10800"},{"username":"TMR10800"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean implements Serializable {
        /**
         * username : DHIR10800
         */

        private String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
