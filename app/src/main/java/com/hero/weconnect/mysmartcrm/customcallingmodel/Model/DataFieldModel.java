package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class DataFieldModel extends SuperClassCastBean
{

    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"FieldName":"Customer Name","Front_Choice":"NO"},{"FieldName":"Contact Number","Front_Choice":"YES"},{"FieldName":"Model Name","Front_Choice":"YES"},{"FieldName":"Frame #","Front_Choice":"NO"},{"FieldName":"Registration No.","Front_Choice":"YES"},{"FieldName":"Next Call Plan Date","Front_Choice":"NO"},{"FieldName":"Last Service Date","Front_Choice":"NO"},{"FieldName":"Last Service Type","Front_Choice":"NO"},{"FieldName":"Dlr Invoice Date","Front_Choice":"YES"},{"FieldName":"Account Name","Front_Choice":"NO"},{"FieldName":"AMC Dealer","Front_Choice":"NO"},{"FieldName":"Last Service Network Code","Front_Choice":"NO"},{"FieldName":"Last Service Dealer Code","Front_Choice":"NO"},{"FieldName":"DateOfSale","Front_Choice":"NO"},{"FieldName":"Dealer Code","Front_Choice":"NO"},{"FieldName":"Comments","Front_Choice":"NO"},{"FieldName":"Appointment Status","Front_Choice":"YES"},{"FieldName":"Appointment Status","Front_Choice":"YES"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * FieldName : Customer Name
         * Front_Choice : NO
         */

        @SerializedName("FieldName")
        private String FieldName;
        @SerializedName("Front_Choice")
        private String frontChoice;

        public String getFieldName() {
            return FieldName;
        }

        public void setFieldName(String FieldName) {
            this.FieldName = FieldName;
        }

        public String getFrontChoice() {
            return frontChoice;
        }

        public void setFrontChoice(String frontChoice) {
            this.frontChoice = frontChoice;
        }
    }
}
