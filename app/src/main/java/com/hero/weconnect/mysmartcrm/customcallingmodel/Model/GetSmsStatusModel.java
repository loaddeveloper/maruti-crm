package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class GetSmsStatusModel extends SuperClassCastBean {

    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Account_Id":"5fa71cb4-43bf-4f47-9fc3-0156888e039c","SMSService":"ON"}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * Account_Id : 5fa71cb4-43bf-4f47-9fc3-0156888e039c
         * SMSService : ON
         */

        @SerializedName("Account_Id")
        private String AccountId;
        @SerializedName("SMSService")
        private String SMSService;

        public String getAccountId() {
            return AccountId;
        }

        public void setAccountId(String AccountId) {
            this.AccountId = AccountId;
        }

        public String getSMSService() {
            return SMSService;
        }

        public void setSMSService(String SMSService) {
            this.SMSService = SMSService;
        }
    }
}
