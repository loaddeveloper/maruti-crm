package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.models.TokenModel;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ApiResponseListener {

    private EditText username;
    private EditText password;
    Button login;
    private Button register;
    private Button contatcus;
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    CustomDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ////Log.e("Model", "Model" + Build.MODEL);
        //checking login credential data in common sharedpreferances class
        commonSharedPref = new CommonSharedPref(this);
        if (commonSharedPref.getLoginData() != null) {
//            Intent loginIntent = new Intent()
//
//
//            startActivity(new Intent(this, DashboardActivity.class));

        }



        customDialog = new CustomDialog(this);
        initView();
    }

    // initialization of all views in login  Activity
    private void initView() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.Password);
        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(this);

        apiController = new ApiController(this,this);
    }

    //All clickListeners in this onlick functions
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                if (username.getText().length() > 1)
                {
                    if (password.getText().length() > 1)
                    {
                       /* Intent loginIntent = new Intent(this, DashboardActivity.class);
                        loginIntent.putExtra("dealercode", "11310");
                        loginIntent.putExtra("token", "MDI1NjExMDItMWU3Ny0xMWViLWI1YWUtMDA1MDU2OTIzMDk4OjpIRUVOQTEwMzc1MDE6OndlY29ubmVjdA==");
                        loginIntent.putExtra("role", "aspcnv33");
                        loginIntent.putExtra("pagename", "followup_all");
                        loginIntent.putExtra("userid", "NISHISINGH11310");
                        loginIntent.putExtra("username", "NISHISINGH11310");
                        loginIntent.putExtra("password", "test123");
                        loginIntent.putExtra("app", CommonVariables.APP_Role_2);
                        loginIntent.putExtra("Division", "HSA");
                        startActivity(loginIntent);*/

            /*              Intent loginIntent = new Intent(this, DashboardActivity.class);
                        loginIntent.putExtra("dealercode", "10375");
                        loginIntent.putExtra("token", "MDI1NjExMDItMWU3Ny0xMWViLWI1YWUtMDA1MDU2OTIzMDk4OjpIRUVOQTEwMzc1MDE6OndlY29ubmVjdA==");
                        loginIntent.putExtra("role", "aspcnv33");
                        loginIntent.putExtra("pagename", "followup_all");
                        loginIntent.putExtra("userid", "10375G01");
                        loginIntent.putExtra("username", "10375G01");
                        loginIntent.putExtra("password", "test123");
                        loginIntent.putExtra("app", CommonVariables.APP_Role_2);
                        loginIntent.putExtra("Division", "HSAPT");
                        startActivity(loginIntent);*/


                      //  10035RKGAUR

                  /*      Intent loginIntent = new Intent(this, DashboardActivity.class);
                        loginIntent.putExtra("dealercode", "10375");
                        loginIntent.putExtra("token", "test123");
                        loginIntent.putExtra("role", "aspcnv33");
                        loginIntent.putExtra("pagename" , "dashboard");
                        loginIntent.putExtra("userid", "SCRM");
                        loginIntent.putExtra("username", "SCRM");
                        loginIntent.putExtra("password", "test123");
                        loginIntent.putExtra("app", CommonVariables.APP_Role_1);
                        loginIntent.putExtra("Division", "HJCPT");
                        startActivity(loginIntent);*/
//

//                        //10226V54
                        Intent loginIntent = new Intent(this, DashboardActivity.class);
                        loginIntent.putExtra("dealercode", "10375");
                        loginIntent.putExtra("token", "test123");
                        loginIntent.putExtra("role", "aspcnv33");
                        loginIntent.putExtra("pagename", "dashboard");
                        loginIntent.putExtra("userid", "ANJU10375");
                        loginIntent.putExtra("username", "ANJU10375");
                        loginIntent.putExtra("password", "");
                        loginIntent.putExtra("app", CommonVariables.APP_Role_1);
                        loginIntent.putExtra("Division", "HJCPT");
                        startActivity(loginIntent);

                        EnquiryDataSendAPI();

                        //   apiController.getToken(username.getText().toString()+"+"+imei,password.getText().toString());

                    } else {
                        customDialog.dismiss();
                        password.setError("Please enter password");
                        password.requestFocus();

                    }

                } else {
                    customDialog.dismiss();

                    username.setError("Please enter username");
                    username.requestFocus();
                }


                break;


        }
    }

    // API call success method
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setLoginData(tokenModel);
            Toast.makeText(this, "Successfully", Toast.LENGTH_SHORT).show();


            customDialog.dismiss();

        }
    }

    // API call Failure method
    @Override
    public void onFailure(String msg)
    {
      //  Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();

        customDialog.dismiss();

    }

    // API call error method
    @Override
    public void onError(String msg) {
        Toast.makeText(this, "Please check username and password", Toast.LENGTH_SHORT).show();
        customDialog.dismiss();


    }

    @Override
    protected void onDestroy() {

        if (commonSharedPref.getLoginData() != null) {
            super.onDestroy();

           // apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token());
        }
    }




    public void EnquiryDataSendAPI()
    {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,
                "{\r\n    \"Id\": \"\",\r\n    \"ReceiverMobile\": \"8209388849\",\r\n    \"CallerMobile\": \"8209388849\",\r\n    \"CallType\": \"Outgoing\",\r\n    \"Name\": \"KD\",\r\n    \"Email\": \"kd@gmail.com\",\r\n    \"CategoryId\": \"25\",\r\n    \"SubCategoryId\": \"36\"\r\n}");
        Request request = new Request.Builder()
                .url("https://study91.co.in/api/contentapi/UserEnquiries")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            Log.e("Response","Respone"+response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}