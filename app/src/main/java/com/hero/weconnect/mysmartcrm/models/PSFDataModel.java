package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class PSFDataModel extends SuperClassCastBean {

    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("sno")
        private Integer sno;
        @SerializedName("id")
        private String id;
        @SerializedName("date")
        private String date;
        @SerializedName("type")
        private String type;
        @SerializedName("invoiceamount")
        private String invoiceamount;
        @SerializedName("source")
        private String source;
        @SerializedName("dirf")
        private String dirf;
        @SerializedName("recovery")
        private String recovery;
        @SerializedName("kib")
        private String kib;
        @SerializedName("repeatvisit")
        private String repeatvisit;
        @SerializedName("km")
        private String km;
        @SerializedName("vin")
        private String vin;
        @SerializedName("regno")
        private String regno;
        @SerializedName("model")
        private String model;
        @SerializedName("Column1")
        private String column1;
        @SerializedName("warrantystartdate")
        private String warrantystartdate;
        @SerializedName("serviceupdate")
        private String serviceupdate;
        @SerializedName("mandatoryupdate")
        private String mandatoryupdate;
        @SerializedName("avgkmrun")
        private String avgkmrun;
        @SerializedName("customerrating")
        private String customerrating;
        @SerializedName("customerfirstname")
        private String customerfirstname;
        @SerializedName("customerlastname")
        private String customerlastname;
        @SerializedName("customercontact")
        private String customercontact;
        @SerializedName("address")
        private String address;
        @SerializedName("dealercode")
        private String dealercode;
        @SerializedName("calledstatus")
        private String calledstatus;

        public Integer getSno() {
            return sno;
        }

        public void setSno(Integer sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getInvoiceamount() {
            return invoiceamount;
        }

        public void setInvoiceamount(String invoiceamount) {
            this.invoiceamount = invoiceamount;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getDirf() {
            return dirf;
        }

        public void setDirf(String dirf) {
            this.dirf = dirf;
        }

        public String getRecovery() {
            return recovery;
        }

        public void setRecovery(String recovery) {
            this.recovery = recovery;
        }

        public String getKib() {
            return kib;
        }

        public void setKib(String kib) {
            this.kib = kib;
        }

        public String getRepeatvisit() {
            return repeatvisit;
        }

        public void setRepeatvisit(String repeatvisit) {
            this.repeatvisit = repeatvisit;
        }

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getRegno() {
            return regno;
        }

        public void setRegno(String regno) {
            this.regno = regno;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getColumn1() {
            return column1;
        }

        public void setColumn1(String column1) {
            this.column1 = column1;
        }

        public String getWarrantystartdate() {
            return warrantystartdate;
        }

        public void setWarrantystartdate(String warrantystartdate) {
            this.warrantystartdate = warrantystartdate;
        }

        public String getServiceupdate() {
            return serviceupdate;
        }

        public void setServiceupdate(String serviceupdate) {
            this.serviceupdate = serviceupdate;
        }

        public String getMandatoryupdate() {
            return mandatoryupdate;
        }

        public void setMandatoryupdate(String mandatoryupdate) {
            this.mandatoryupdate = mandatoryupdate;
        }

        public String getAvgkmrun() {
            return avgkmrun;
        }

        public void setAvgkmrun(String avgkmrun) {
            this.avgkmrun = avgkmrun;
        }

        public String getCustomerrating() {
            return customerrating;
        }

        public void setCustomerrating(String customerrating) {
            this.customerrating = customerrating;
        }

        public String getCustomerfirstname() {
            return customerfirstname;
        }

        public void setCustomerfirstname(String customerfirstname) {
            this.customerfirstname = customerfirstname;
        }

        public String getCustomerlastname() {
            return customerlastname;
        }

        public void setCustomerlastname(String customerlastname) {
            this.customerlastname = customerlastname;
        }

        public String getCustomercontact() {
            return customercontact;
        }

        public void setCustomercontact(String customercontact) {
            this.customercontact = customercontact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDealercode() {
            return dealercode;
        }

        public void setDealercode(String dealercode) {
            this.dealercode = dealercode;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
