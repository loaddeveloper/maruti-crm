package com.hero.weconnect.mysmartcrm.Retrofit;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.TokenModel;


import java.lang.reflect.Type;

import static android.content.Context.MODE_PRIVATE;

public class CommonSharedPrefCustom {
    private Context mContext;
    private SharedPreferences.Editor editor, editorpackages;
    private SharedPreferences sharedPreferences, sharedPreferencespackages;

    public CommonSharedPrefCustom(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("logindata", MODE_PRIVATE);
        sharedPreferencespackages = mContext.getSharedPreferences("PackagesList", MODE_PRIVATE);

    }

    public TokenModel getLoginData() {

        String json = sharedPreferences.getString(ApiConstant.TOKEN_TAG, null);
        Gson gson = new Gson();
        Type type = new TypeToken<TokenModel>() {
        }.getType();

        TokenModel beans = null;
        beans = gson.fromJson(json, type);

        return beans;

    }

    public void setLoginData(TokenModel beans) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString(ApiConstant.TOKEN_TAG, json);
        editor.commit();
    }

    public void storeUser(String user, String pass) {
        SharedPreferences preferences = mContext.getSharedPreferences("log", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user", user);
        editor.putString("password", pass);
        editor.apply();
    }

    public boolean isAlreadyLoggedIn() {
        boolean isLoggedIn = false;
        SharedPreferences preferences =mContext.getSharedPreferences("log", MODE_PRIVATE);
        String nullCheck = preferences.getString("user", null);
        if (nullCheck == null) {
            return isLoggedIn;
        } else {
            return true;
        }
    }
    public void removeLoginData() {
        editor = sharedPreferences.edit();
        editor.remove(ApiConstant.TOKEN_TAG);
        editor.apply();

        SharedPreferences preferences = mContext.getSharedPreferences("log", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
    }

    private void removeLoggedInUser() {

    }

    public String getPackagesList() {

        String packagename = sharedPreferencespackages.getString(ApiConstant.PACKAGEADD, null);


        return packagename;

    }

    public void setPackages(String packages) {
        editorpackages = sharedPreferencespackages.edit();
        editorpackages.putString(ApiConstant.PACKAGEADD, packages);
        editorpackages.commit();
    }

    public void clearpackages() {
        editorpackages = sharedPreferencespackages.edit();
        editorpackages.clear();
        editorpackages.commit();
    }


    public void setAllClear() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }


}
