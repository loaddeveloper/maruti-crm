package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class VideoDataModel extends SuperClassCastBean {


    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("Link")
        private String link;
        @SerializedName("Description")
        private String description;
        @SerializedName("Type")
        private String type;

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
