package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.Drawer_Activity;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_Custom extends RecyclerView.Adapter<Adapter_Custom.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    CustomButtonListener customListner;
    int size = 0;
    String holdtag = "";
    ArrayList<Custom_Model.DataBean> servicerreminderdata;
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    private Context context;
    CountDownTimer countDownTimer;

    public Adapter_Custom(Context context, CustomButtonListener customButtonListener, ArrayList<Custom_Model.DataBean> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.servicerreminderdata = list;
        setHasStableIds(true);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_main_data, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(true);
      //  Log.e("SRADAPTER", "" + position);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);

        if (servicerreminderdata.size() > 0) {

            if (position == servicerreminderdata.size()) {
                holder.morebtn1.setVisibility(View.VISIBLE);
                holder.main_ll.setVisibility(View.GONE);

            } else {
                Custom_Model.DataBean dataBean = servicerreminderdata.get(position);

                if (holder.morebtn1 != null) holder.morebtn1.setVisibility(View.GONE);
                if (holder.main_ll != null) holder.main_ll.setVisibility(View.VISIBLE);
                final String idd = String.valueOf(dataBean.getId());
                holder.srno.setText("" + dataBean.getSerialNo() + ".");
                holder.customer_Name.setText("" + dataBean.getData1()+" " + dataBean.getData6());
                holder.data1.setText("" + dataBean.getData3());
                holder.data2.setText("" + dataBean.getData2());
                holder.data3.setText("" + dataBean.getData4());
                holder.data4.setText("" + dataBean.getData5());
                holder.data5.setText("");
                
                if(dataBean.getData7() != null && !dataBean.getData7().isEmpty() && dataBean.getData7().length()>=11)holder.data6.setText("" +dateparse3(dataBean.getData7()).substring(0,11));

                holder.data7.setText("");
                holder.calledstatus.setText(""+dataBean.getCalledstatus());
                //  holder.data5.setText("" +dateparse3( dataBean.getNext_service_due_date()).substring(0,11));
                //  holder.calledstatus.setText("" + dataBean.getCalledstatus().toUpperCase());


                countDownTimer = new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        final String idssss = common_calling_color_sharedPreferances.getmatserid();
                        final String call = common_calling_color_sharedPreferances.getcallcurrentstatus();
                        SharedPreferences chkfsss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String status = chkfsss.getString("Status", "");
                        final String status1 = chkfsss.getString("Status1", "");
                        SharedPreferences chkfss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                        final String icca = chkfss.getString("call", "");
                      //  Log.e("Adapter no ", "------------------------------------- " + position);
                        if (dataBean.getId().equals(idssss) && call.equals("s")) {
                           // Log.e("call color ", "color 11 ");
                            holder.calledstatus.setText("Ringing");
                            pos = position;
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            holder.tv_hold.setVisibility(View.VISIBLE);
                        } else if (dataBean.getId().equals(idssss) && dataBean.getCalledstatus().equals("CALL DONE") && call.equals("s")) {
                          //  Log.e("call color ", "color 12 ");
                            holder.calledstatus.setText("Ringing");
                            pos = position;
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                            holder.tv_hold.setVisibility(View.VISIBLE);

                        } else if ((icca.equals("CALL DONE")) && (!status.equals("booking"))
                                && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                          //  Log.e("call color ", "color 13 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);
                        } else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                           // Log.e("call color ", "color 14 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);
                            if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                               // Log.e("call color ", "color 15 ");
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                holder.tv_hold.setVisibility(View.GONE);
                            } else {
                                //Log.e("call color ", "color 16 ");
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                holder.calledstatus.setText("CALL DONE");
                                holder.tv_hold.setVisibility(View.GONE);
                            }
                        } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {
                            //Log.e("call color ", "color 17 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        } else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {
                            //Log.e("call color ", "color 18 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        } else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                           // Log.e("call color ", "color 19 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);

                        } else {
                           // Log.e("call color ", "color 20 ");
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                            holder.tv_hold.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onFinish() {


                        countDownTimer.cancel();

                    }
                };

                countDownTimer.start();


                holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        customListner.sendWhatsappMessage(position, "" + dataBean.getData2());
                        // TODO: 10-12-2020
                        customListner.sendWhatsappMessage(position, "" + dataBean.getData2());
                    }
                });
                holder.textmessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.sendtextmessage(position, "" + dataBean.getData2());
                    }
                });
                holder.main_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*int position,String mastertableid,String customername,String model,String serviceduefrom, String pickdropbalance, String contactnumber);*/
                        String customername = "" + dataBean.getData1().toUpperCase();
                        customListner.getCustomerDetails(position, dataBean.getId(), dataBean.getData1(), dataBean.getData2());
                    }
                });
                holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public boolean onLongClick(View v) {
                        CommonVariables.Is_LongPress = true;
                        common_calling_color_sharedPreferances.setPosition(String.valueOf(position));

                        //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                        String uid = UUID.randomUUID().toString();

                        SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                        SharedPreferences.Editor edits = prefs.edit();
                        edits.putString(dataBean.getId(), uid);
                        edits.commit();

                        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                            Toasty.warning(context, "Call is Currently Onging", Toast.LENGTH_SHORT).show();
                        } else {
                            if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                // Create the Uri from phoneNumberInput
                                 Uri uri = Uri.parse("tel:" + "+91"+ dataBean.getData2());
                                //Uri uri = Uri.parse("tel:" + CommonVariables.SR_Testingnumber);

                                // Start call to the number in input
                                context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                holder.calledstatus.setText("Ringing");
                                holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                ((Drawer_Activity)context).setIndex(position+1,true);


                                common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                            } else {
                                // Request permission to call
                                ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                            }
                        }
                        return false;
                    }
                });
                holder.historyybtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getHistoryDetails(position);
                    }
                });
                holder.moredetailsbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getMoreDetails(position);
                    }
                });


                holder.tv_hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.VISIBLE);
                        customListner.holdCall(position, dataBean.getData2());
                    }

                });

                holder.tv_unhold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.VISIBLE);
                        holder.tv_unhold.setVisibility(View.GONE);
                        customListner.unHoldCall(position, dataBean.getData2());
                    }

                });


            }
        }


        holder.morebtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (context instanceof Drawer_Activity) {

                    ((Drawer_Activity) context).moreData();

                }

            }
        });


    }

    public CustomButtonListener getCustomListner() {
        return customListner;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        if (servicerreminderdata.size() >= 10) {
            if ((servicerreminderdata.size() % 10) == 0) {
                size = servicerreminderdata.size() + 1;

            } else {
                size = servicerreminderdata.size();

            }
        } else {
            size = servicerreminderdata.size();
        }


        return size;
    }

    public void setCustomButtonListner(Adapter_Custom.CustomButtonListener listener) {
        this.customListner = listener;
    }

    public void updateList(ArrayList<Custom_Model.DataBean> list11) {
        servicerreminderdata = list11;
        notifyDataSetChanged();
    }

    public interface CustomButtonListener {
        void getCustomerDetails(int position, String msterId, String Name, String MobileNo);

        void getHistoryDetails(int position);

        void getMoreDetails(int position);

        void sendWhatsappMessage(int position, String contactnumber);

        void sendtextmessage(int position, String contatcnumber);

        void holdCall(int position, String contatcnumber);

        void unHoldCall(int position, String contatcnumber);


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        // Variable Declare
        public TextView srno, idddata, tv_hold, tv_unhold, customer_Name, data1, data2, data3, data4, data5, data6, data7, calledstatus, morebtn1;
        public ImageView moredetailsbtn, historyybtn, whatsapp, textmessage;
        CardView main_ll;

        public ViewHolder(View rootView) {
            super(rootView);
            this.srno = rootView.findViewById(R.id.srno);
            //   this.idddata = rootView.findViewById(R.id.idddata);
            this.customer_Name = rootView.findViewById(R.id.customername);
            this.data1 = rootView.findViewById(R.id.data1);
            this.data2 = rootView.findViewById(R.id.data2);
            this.data3 = rootView.findViewById(R.id.data3);
            this.data4 = rootView.findViewById(R.id.data4);
            this.data5 = rootView.findViewById(R.id.data5);
            this.data6 = rootView.findViewById(R.id.data6);
            this.data7 = rootView.findViewById(R.id.data7);
            this.calledstatus = rootView.findViewById(R.id.cstatus);
            this.morebtn1 = rootView.findViewById(R.id.morebtn1);
            this.tv_hold = rootView.findViewById(R.id.tv_hold);
            this.tv_unhold = rootView.findViewById(R.id.tv_unhold);
            this.moredetailsbtn = rootView.findViewById(R.id.moredetailsbtn);
            this.historyybtn = rootView.findViewById(R.id.historyybtn);
            this.whatsapp = rootView.findViewById(R.id.whatsapp);
            this.textmessage = rootView.findViewById(R.id.textmessage);
            this.main_ll = rootView.findViewById(R.id.main_ll);

        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String NewDateFormat = null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate = null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }


}




