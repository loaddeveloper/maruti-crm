package com.hero.weconnect.mysmartcrm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.models.ReportingModel;
import com.hero.weconnect.mysmartcrm.models.SR_Report_Model;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.analytics.Analytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReportActivitySR extends AppCompatActivity implements ApiResponseListener,View.OnClickListener {

    CardView todate,fromdate;
    EditText et_todate,et_fromdate;
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    TextView totalavilablecalls,totalcalls,calldone,callnotdone,folowup,bookings,notcomingreson,noresponse;
    String dateto,datefrom,token,callType;
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    Date date1;
    public ArrayAdapter<String> userArrayAdapter;
    ArrayList<String>[] userArrayList;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    Spinner sp_user_report;
    CustomDialog customDialog;
    Calendar calendar;
    String myFormat = "MM-dd-yyyy"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    Calendar myCalendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_sr);

        Toolbar toolbar = findViewById(R.id.dashtoolbar);
        toolbar.setTitle("Service Reminder Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", ""+ CommonVariables.UserId);
        properties.put("DealerCode", ""+CommonVariables.dealercode);
        properties.put("ActivityName", "ReportActivitySR");
        properties.put("Role", ""+CommonVariables.role);

        Analytics.trackEvent("VideoActivitySR", properties);
        init();

    }

    void init()
    {

        apiController = new ApiController(this,this);
        fromdate = (CardView) findViewById(R.id.fromdate);
        todate = (CardView) findViewById(R.id.todate);
        sp_user_report = (Spinner) findViewById(R.id.dealercode_report);
        et_fromdate = (EditText) findViewById(R.id.fromtxtdate);
        et_todate = (EditText) findViewById(R.id.totxtdate);
        totalavilablecalls = (TextView) findViewById(R.id.totalavilablecalls);
        totalcalls = (TextView) findViewById(R.id.totalcalls);
        calldone = (TextView) findViewById(R.id.calldone);
        callnotdone = (TextView) findViewById(R.id.callnotdone);
        folowup = (TextView) findViewById(R.id.folowup);
        bookings = (TextView) findViewById(R.id.bookings);
        notcomingreson = (TextView) findViewById(R.id.ncr);
        noresponse = (TextView) findViewById(R.id.noresponse);
        commonSharedPref = new CommonSharedPref(this);
        customDialog = new CustomDialog(this);
        userArrayList= new ArrayList[]{new ArrayList<String>()};
        calendar = Calendar.getInstance();
        et_fromdate.setText(sdf.format(myCalendar.getTime()));
        et_todate.setText(sdf.format(myCalendar.getTime()));
        datefrom=sdf.format(myCalendar.getTime());
        dateto=sdf.format(myCalendar.getTime());
        userArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,userArrayList[0]);
        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Unauthorized access !!", Toast.LENGTH_SHORT).show();            return;
        }


        Toolbar toolbar = findViewById(R.id.dashtoolbar);
        toolbar.setTitle("Service Reminder Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

      customDialog.show();
      apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);
        fromdate.setOnClickListener(ReportActivitySR.this);
        todate.setOnClickListener(ReportActivitySR.this);
        et_fromdate.setOnClickListener(ReportActivitySR.this);
        et_todate.setOnClickListener(ReportActivitySR.this);



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        arrayAdapterSetUp();


    }

    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {
        if(beanTag.matches(ApiConstant.REPORTINGDATA_SR))
        {
            SR_Report_Model reportingModel=(SR_Report_Model)superClassCastBean;
            if(reportingModel.getData().size()>0)
            {

                for(SR_Report_Model.DataDTO dataBean:reportingModel.getData()) {


                    totalavilablecalls.setText(""+dataBean.getTotalCustomer());
                    totalcalls.setText(""+dataBean.getTotalCallsMade());
                    folowup.setText(""+dataBean.getFollowups());
                    bookings.setText(""+dataBean.getBookings());
                    calldone.setText(""+dataBean.getCallDone());
                    callnotdone.setText(""+dataBean.getCallNotDone());
                    notcomingreson.setText(""+dataBean.getNotComingReason());
                    noresponse.setText(""+dataBean.getNoResponse());



                }
            }

           customDialog.dismiss();

        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customDialog.dismiss();
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
            CommonVariables.token=token;

            getReportingdata(token,datefrom,dateto);



        }

    }

    @Override
    public void onFailure(String msg) {
        customDialog.dismiss();

    }

    @Override
    public void onError(String msg) {
        customDialog.dismiss();

    }


    public  void getReportingdata(String token,String startdate,String enddate)
    {
        customDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("StartDate",startdate);
        params.put("EndDate",enddate);

        apiController.getReportingData_SR(token,params);


    }

    public void arrayAdapterSetUp()
    {

        userArrayList= new ArrayList[]{new ArrayList<String>()};
        userArrayList[0].add(""+CommonVariables.role);
        userArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,userArrayList[0]);
        sp_user_report.setAdapter(userArrayAdapter);
        sp_user_report.setEnabled(false);


    }
    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

       if (id==R.id.fromtxtdate || id==R.id.fromdate||id==R.id.fromicon||id==R.id.fromtext)
       {
           final Calendar c = Calendar.getInstance();
           final int mYear = c.get(Calendar.YEAR);
           final int mMonth = c.get(Calendar.MONTH);
           final int mDay = c.get(Calendar.DAY_OF_MONTH);

           // Launch Date Picker Dialog
           DatePickerDialog dpd = new DatePickerDialog(ReportActivitySR.this,
                   new DatePickerDialog.OnDateSetListener() {

                       @Override
                       public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                           int  ye = year;
                           int  day = dayOfMonth;
                           int ss = month + 1;
                           int mon = ss;


                           et_fromdate.setText(ss + "-" + day + "-" + ye);
                           datefrom=mon+"-"+day+"-"+ye;;

                           getReportingdata(token,datefrom,dateto);

                          /* try {
                               Date  date1 = new SimpleDateFormat("MM-DD-yyyy").parse(et_fromdate.getText().toString());
                               Date  date2 = new SimpleDateFormat("MM-DD-yyyy").parse(et_todate.getText().toString());
                              // nishant.gaur@dianapps.com
                               if(date1.compareTo(date2) > 0) {

                                   et_todate.setText(sdf.format(myCalendar.getTime()));
                                   dateto=sdf.format(myCalendar.getTime());
                                   getReportingdata(token,datefrom,dateto);




                               }
                               else if(date1.compareTo(date2) < 0) {



                                   Calendar calNew = Calendar.getInstance();
                                   calNew.setTime(date2);
                                   calNew.add(Calendar.DATE, -1);
                                   et_todate.setText(sdf.format(calNew.getTime()));
                                   dateto=sdf.format(calNew.getTime());
                                   getReportingdata(token,datefrom,dateto);



                               } else if(date1.compareTo(date2) == 0) {
                                   System.out.println("Both dates are equal");

                               }

                           } catch (ParseException e) {
                               e.printStackTrace();
                           }*/
                           // totalCustomerData();

                       }


                   }, mYear, mMonth, mDay);
           dpd.getDatePicker().setMaxDate(new Date().getTime());
           dpd.show();
       }
       else if (id==R.id.totxtdate||id==R.id.todate)
       {
           final Calendar c = Calendar.getInstance();
           final int mYear = c.get(Calendar.YEAR);
           final int mMonth = c.get(Calendar.MONTH);
           final int mDay = c.get(Calendar.DAY_OF_MONTH);

           // Launch Date Picker Dialog
           DatePickerDialog dpd = new DatePickerDialog(ReportActivitySR.this,
                   new DatePickerDialog.OnDateSetListener() {

                       @Override
                       public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                           int  ye = year;
                           int  day = dayOfMonth;
                           int ss = month + 1;
                           int mon = ss;

                           et_todate.setText(ss + "-" + day + "-" + ye);
                           dateto=mon+"-"+day+"-"+ye;
                           getReportingdata(token,datefrom,dateto);




                           //  getReportingdata(token,datefrom,dateto);




                       }


                   }, mYear, mMonth, mDay);



           dpd.getDatePicker().setMaxDate(new Date().getTime());
           dpd.show();

       }
    }


}