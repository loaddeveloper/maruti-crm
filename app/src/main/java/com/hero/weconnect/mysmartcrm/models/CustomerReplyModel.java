package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class CustomerReplyModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"Reply":"Not\r\nInterested"},{"Reply":"Call Back"},{"Reply":"Not available"},{"Reply":"Interested (booked)"},{"Reply":"NCR"},{"Reply":"sold,theft"}]
     * ErrorMessage : null
     */

    private String Message;
    private Object ErrorMessage;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Object getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Reply : Not
         * Interested
         */

        private String Reply;

        public String getReply() {
            return Reply;
        }

        public void setReply(String Reply) {
            this.Reply = Reply;
        }
    }
}
