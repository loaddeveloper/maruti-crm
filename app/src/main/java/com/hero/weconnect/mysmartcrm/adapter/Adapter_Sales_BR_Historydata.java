package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Adapter_Sales_BR_Historydata extends RecyclerView.Adapter<Adapter_Sales_BR_Historydata.ViewHolder> {
    ArrayList<Sales_HistoryModel.DataBean> dataBeanArrayList = new ArrayList<>();
    Adapter_Sales_Historydata adapter;
    private Context context;


    public Adapter_Sales_BR_Historydata(Context context, ArrayList<Sales_HistoryModel.DataBean> datalist) {

        this.context = context;
        this.dataBeanArrayList = datalist;

    }

    @NonNull
    @Override
    public Adapter_Sales_BR_Historydata.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.history_sales_popup_cardview, parent, false);

        return new Adapter_Sales_BR_Historydata.ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull Adapter_Sales_BR_Historydata.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        Sales_HistoryModel.DataBean dataBean = dataBeanArrayList.get(position);

        holder.edit_followupstatus.setText("" + dataBean.getFollowup_status());
        holder.edit_followupdate.setText("" + dataBean.getNext_followup_date());
        holder.edit_remark.setText("" + dataBean.getRemark());
        holder.edit_calledstatus.setText("" + dataBean.getCalledstatus());
        holder.edit_user.setText(""+dataBean.getUser());

        holder.mainsview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog shippingDialog1 = new Dialog(context);
                shippingDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog1.setContentView(R.layout.history_moredetails_cardview_sales);
                shippingDialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog1.setCancelable(true);
                shippingDialog1.setCanceledOnTouchOutside(false);
                TextView closebtn;
                EditText tv_srnumber,edit_followupstatus,call_start_datetime, edit_followupdate, edit_dateofpurchase, edit_closurereason, edit_closuresubreason, edit_make, edit_model, edit_remark, edit_starttime, edit_endtime, edit_user, edit_calledstatus;
                closebtn=shippingDialog1.findViewById(R.id.txtclose);
                edit_followupstatus = shippingDialog1.findViewById(R.id.history_followup_status);
                edit_followupdate = shippingDialog1.findViewById(R.id.history_followupdate);
                edit_dateofpurchase = shippingDialog1.findViewById(R.id.history_date_purchase);
                edit_closurereason = shippingDialog1.findViewById(R.id.history_closure_reason);
                edit_closuresubreason = shippingDialog1.findViewById(R.id.history_closure_sub_reason);
                edit_make = shippingDialog1.findViewById(R.id.history_make);
                edit_model = shippingDialog1.findViewById(R.id.history_model);
                edit_remark = shippingDialog1.findViewById(R.id.history_remark);
                edit_starttime = shippingDialog1.findViewById(R.id.history_start_time);
                call_start_datetime = shippingDialog1.findViewById(R.id.call_start_datetime);
                edit_endtime = shippingDialog1.findViewById(R.id.history_endtime);
                edit_user = shippingDialog1.findViewById(R.id.history_user);
                edit_calledstatus = shippingDialog1.findViewById(R.id.history_called_status);

                edit_followupstatus.setText("" + dataBean.getFollowup_status());
                edit_followupdate.setText("" + dataBean.getNext_followup_date());
                edit_dateofpurchase.setText("" + dataBean.getExpected_date_of_purchase());
                edit_closurereason.setText("" + dataBean.getClosurereason());
                edit_closuresubreason.setText("" + dataBean.getClosure_sub_reason());
                edit_make.setText("" + dataBean.getMake());
                edit_model.setText("" + dataBean.getModel());

//                if(dataBean.getCall_start_datetime().isEmpty())
//                {
                if(dataBean.getCall_start_datetime() != null && !dataBean.getCall_start_datetime().isEmpty()) edit_starttime.setText("" + dateparse3(dataBean.getCall_start_datetime().replace("T"," ").substring(0,10)));
                if(dataBean.getCall_start_datetime() != null && !dataBean.getCall_start_datetime().isEmpty()) call_start_datetime.setText("" + dateparse3(dataBean.getCall_start_datetime().replace("T"," ").substring(0,10)));

//                }else
//                {
//                    edit_starttime.setText("" + dataBean.getCall_start_datetime());
//
//                }
                if(dataBean.getCall_end_datetime() != null && !dataBean.getCall_end_datetime().isEmpty())edit_endtime.setText("" + dateparse3(dataBean.getCall_end_datetime().replace("T"," ").substring(0,10)));

//                }else
//                {
//                    edit_starttime.setText("" + dataBean.getCall_start_datetime());
//
//                }
                edit_remark.setText("" + dataBean.getRemark());
                edit_calledstatus.setText("" + dataBean.getCalledstatus());
                edit_user.setText(""+dataBean.getUser());

                closebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog1.dismiss();
                    }
                });


                shippingDialog1.show();
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return dataBeanArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView tv_srnumber,edit_followupstatus, edit_followupdate, edit_dateofpurchase, edit_closurereason, edit_closuresubreason, edit_make, edit_model, edit_remark, edit_starttime, edit_endtime, edit_user, edit_calledstatus;
        public LinearLayout mainsview;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            edit_followupstatus = rootView.findViewById(R.id.history_followup_status);
            edit_followupdate = rootView.findViewById(R.id.history_followupdate);
            edit_remark = rootView.findViewById(R.id.history_remark);
            edit_user = rootView.findViewById(R.id.history_user);
            edit_calledstatus = rootView.findViewById(R.id.history_called_status);
            mainsview  = rootView.findViewById(R.id.mainsview);

        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String NewDateFormat=null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }
}