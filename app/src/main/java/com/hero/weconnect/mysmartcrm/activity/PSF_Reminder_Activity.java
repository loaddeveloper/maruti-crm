package com.hero.weconnect.mysmartcrm.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.role.RoleManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.CallUtils.CallRecorderService;
import com.hero.weconnect.mysmartcrm.CallUtils.CallService;
import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.APIInterface;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.ConnectionDetector;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.Utils.NetworkCheckerService;
import com.hero.weconnect.mysmartcrm.adapter.AdapterTemplateShow;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_PSF;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_PSFQuestionRating;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR;
import com.hero.weconnect.mysmartcrm.adapter.Adapter_SR_Historydata;
import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.CustomerReplyModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.NotComingReasonModel;
import com.hero.weconnect.mysmartcrm.models.PSFAnswerModel;
import com.hero.weconnect.mysmartcrm.models.PSFDataModel;
import com.hero.weconnect.mysmartcrm.models.PSFDissatisfactionReasonModel;
import com.hero.weconnect.mysmartcrm.models.PSFSOPRatingQuestionsModel;
import com.hero.weconnect.mysmartcrm.models.PSFSingleDataModel;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.models.UserDetailsModel;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;
import es.dmoral.toasty.Toasty;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;
import static java.lang.Thread.sleep;

public class PSF_Reminder_Activity extends AppCompatActivity implements Adapter_PSF.CustomButtonListener,Adapter_PSFQuestionRating.CustomClickListener, AdapterTemplateShow.CustomButtonListener, ApiResponseListener {
    public static final int REQUEST_PERMISSION = 0;
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    public static final int CHATHEAD_OVERLAY_PERMISSION_REQUEST_CODE = 100;
    public static final int CUSTOM_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    public static int calling_list_serial_no;
    public int dataposition;
    CountDownTimer countDownTimer;
    public boolean calldone = false;
    public static boolean whatsupclick = false, callstuck = false, textmessageclick = false;
    public static RecyclerView recyclerView,rv_questionrating;
    Adapter_PSFQuestionRating adapter_psfQuestionRating;
    public ArrayList<PSFDataModel.DataDTO> serviceReminderDataList = new ArrayList<>();
    public ArrayList<SR_HistoryModel.DataBean> serviceReminderHistoryDataList = new ArrayList<>();
    public Adapter_PSF adapter;
    public int call_current_position = 0, position = 0, state, lastposition;
    public String string_called_status = "", token = "", string_customername, string_masterId, string_mobileno, selectedPath = "", oldhid = "";
    public String Callingstatus = "BUSY", offset = "0";
    public ImageButton startcall, endcall, pausecall;
    public boolean stop = false;
    public Date currentdail, discom;
    public CustomDialog customWaitingDialog;
    public HashMap<String, String> params = new HashMap<>();
    public ArrayList<String> arrayList_matserId = new ArrayList<String>();
    public ArrayList<String> arrayList_customername = new ArrayList<String>();
    public ArrayList<String> arrayList_callstatus = new ArrayList<String>();
    public ArrayList<String> arrayList_mobileno = new ArrayList<String>();
    CopyOnWriteArrayList<PSFAnswerModel> answerlist= new CopyOnWriteArrayList<>();
    public ApiController apiController;
    public CommonSharedPref commonSharedPref;
    public Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    public SharedPreferences sharedPreferencescallUI;
    public Spinner sp_notcomingreason, contactstatus, customerreply, sp_pickanddrop,sp_reason;
    public ImageView iv_booking, iv_nextfollowupdate;
    public String historyID, callstatus, notcomingreason, pickanddrop, contacted_status, customer_reply, whatsapp_number;
    public ArrayAdapter<String> contactArrayAdapter, customerArrayAdapter, notcomingreasonArrayAdapter,reasonArrayAdapter;
    public ArrayList<String>[] contactStatuslist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] customerreplylist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] notcomingreasonlist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String>[] reasonlist = new ArrayList[]{new ArrayList<String>()};
    public Adapter_SR_Historydata adapter_sr_historydata;
    public boolean morebtncliked = false;
    public File audiofile = null;
    public AudioManager am = null;
    public MediaRecorder recorder;
    public AdapterTemplateShow adapterSettingTemplet;
    public ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    public ArrayList<PSFSOPRatingQuestionsModel.DataDTO> questionlist = new ArrayList<>();
    public ArrayList<PSFDissatisfactionReasonModel.DataDTO> dissatisfactionlist = new ArrayList<>();
    public Dialog Whatsapp_shippingDialog;
    public boolean whatsappclicked = false, textmesasageclicked = false, isLongPress = false;
    public ArrayList<PSFSingleDataModel.DataDTO> singledatalist = new ArrayList<>();
    public String bookingdatenew = "New";
    public String bookingdate_send, nextfollowupdate_send, NewDateFormat = null;
    public LinearLayout nodata_found_layout;
    public Button Retry_Button;
    public NetworkCheckerService networkCheckerService;
    public LinearLayoutManager linearLayoutManager;
    public CompositeDisposable disposables = new CompositeDisposable();
    public RecyclerView history_data_recyclerview, recyclerView_template;
    public ConnectionDetector detector;
    public DividerItemDecoration dividerItemDecoration;
    public LinearLayout servicelinear, reminderlinear, complaintcategorysubll, complaintstatusll, ll_update, ll_index;
    public EditText indexs, indexs2, et_bookingdate, et_nextfollowupdate, remarks, edit_Bookkingno;
    public SwitchDateTimeDialogFragment dateTimeFragment;
    public Intent intentFilter;
    public APIInterface apiInterface;
    public int index = 0;
    Button submit_btn;
    boolean PressStop_Button;
    public boolean morebtnclicked = false,start_call_button = false;
    CheckBox cb_calldone;
    public boolean isCalldoneChecked = false;
    int Current_Index_Value;
    AlertDialog dialog_network_only;
    NetworkInfo currentNetworkInfo;
    Intent recordService;



    @SuppressLint("NewApi")
    public static void start(Context context, Call call) {
        context.startActivity(new Intent(context, PSF_Reminder_Activity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle()));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psf_);


        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        AppCenter.start(getApplication(), "f01eac54-241c-474f-9632-317509be0c80",
                Analytics.class, Crashes.class);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        networkCheckerService = new NetworkCheckerService(this, this);


        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);


        //dragableValueshow();

      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final String channelId = "default_floatingview_channel";
            final String channelName = "Default Channel";
            final NotificationChannel defaultChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_MIN);
            final NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.createNotificationChannel(defaultChannel);
            }
        }

            if (savedInstanceState == null) {
            *//*final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, FloatingViewControlFragment.newInstance());

            ft.commit();*//*

            showFloatingView(getApplicationContext(), true, true);


        }*/
        DisableCallWaiting();
        setToolBar();


    }

    public void setToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbarsetup);
        toolbar.setTitle("PSF ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //  toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));


        customWaitingDialog = new CustomDialog(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        apiController = new ApiController(this, this);

        customWaitingDialog.show();
        apiController.getToken(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);

        detector = new ConnectionDetector(PSF_Reminder_Activity.this);

        String folder_main = "Smart CRM";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        recyclerView = findViewById(R.id.service_reminder_recyclerview);
        startcall = findViewById(R.id.startcall);
        pausecall = findViewById(R.id.pasusecall);
        endcall = findViewById(R.id.endcall);
        indexs = findViewById(R.id.indexs);
        indexs2 = findViewById(R.id.indexs2);
        nodata_found_layout = findViewById(R.id.nodatfound);
        Retry_Button = findViewById(R.id.refrshbutton);
        cb_calldone = findViewById(R.id.checkbox_calldone);
        ll_index = findViewById(R.id.index_layout);

        Map<String, String> properties = new HashMap<>();
        properties.put("UserName", "" + CommonVariables.UserId);
        properties.put("DealerCode", "" + CommonVariables.dealercode);
        properties.put("ActivityName", "PSF_Activity");
        properties.put("Role", "" + CommonVariables.role);

        Analytics.trackEvent("PSF_Activity", properties);


        if (common_calling_color_sharedPreferances.getPosition() == null || common_calling_color_sharedPreferances.getPosition().equals("")) {
            position = 0;

        } else {
            position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
        }

        if (position != Current_Index_Value) {
            position = Current_Index_Value;
        }

        adapter = new Adapter_PSF(this, PSF_Reminder_Activity.this, serviceReminderDataList);
        adapter.setHasStableIds(true);
        linearLayoutManager = new LinearLayoutManager(PSF_Reminder_Activity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.hasFixedSize();

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.setAdapter(adapter);
        adapter.setCustomButtonListner(PSF_Reminder_Activity.this);


        //  Toast.makeText(this, "jayant llll", Toast.LENGTH_SHORT).show();
        Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
        disposables.add(disposable);
        Disposable disposable2 = OngoingCall.state
                .filter(state -> state == Call.STATE_DISCONNECTED)
                .delay(2, TimeUnit.SECONDS)
                .firstElement()
                .subscribe(this::finish);
        disposables.add(disposable2);





        commonSharedPref = new CommonSharedPref(this);


        if (commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null) {
            Toast.makeText(this, "Unauthorized access !!", Toast.LENGTH_SHORT).show();
            return;
        }
     /*   token = "bearer " + commonSharedPref.getLoginData().getAccess_token();


        ////Log.e("LLLLLLL  ",token);

        apiController.getNotComingReason(token);
        apiController.getContactStatus(token);
        apiController.getCustomerReply(token);
        params = new HashMap<>();
        params.put("UserName", CommonVariables.UserId);
        apiController.getUserData(token, params);

        if (serviceReminderDataList.size() == 0) getservicereminderdata(token, "0");*/

        isLastVisible();





        /*-----------------------------------Calling Button Click Listeners----------------------------------------------------*/
        indexs.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {

                    int postionoflist = adapter.getItemCount() - 1;


                    int asg = Integer.parseInt(indexs.getText().toString()) - 1;
                    index = asg;


                    if (asg <= postionoflist) {


                    } else {
                        Toasty.warning(PSF_Reminder_Activity.this, " Index Not Avialabe in list Load More Data", Toast.LENGTH_LONG).show();
                    }

                    common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));
                    recyclerView.scrollToPosition(asg);


                } catch (Exception e) {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        startcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if(indexs != null)indexs.setText("1");
                if (detector.isInternetAvailable()) {

                    start_call_button =true;


                    TelecomManager tm = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);

                    if (tm == null) {
                        // whether you want to handle this is up to you really
                        throw new NullPointerException("tm == null");
                    }

                    try {
                        tm.endCall();

                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }

                    if (position >= serviceReminderDataList.size()) {
                        Toasty.warning(PSF_Reminder_Activity.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                        return;

                    }

                    indexs.setEnabled(false);
                    try {
                        TelecomManager systemService = getSystemService(TelecomManager.class);
                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(getPackageName())) {
                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.hero.weconnect"));
                            Toasty.warning(PSF_Reminder_Activity.this, "FIRST CHANGE THE DAILER", Toast.LENGTH_SHORT).show();
                        } else {

                            if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

                                Toasty.warning(PSF_Reminder_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
                            } else {

                                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {


                                    position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                                } else {

                                    position = Integer.parseInt(indexs.getText().toString()) - 1;

                                }
                                common_calling_color_sharedPreferances.setCall_start_stop_status("start");
                                looping();

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }
                } else {

                    PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();


                }


            }
        });
        endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detector.isInternetAvailable()) {

                    PressStop_Button = true;

                    if (CommonVariables.Is_LongPress) {

                        pausecall.performClick();
                        CommonVariables.Is_LongPress = false;
                        return;
                    }


                    if (indexs != null) indexs.setVisibility(View.VISIBLE);
                    if (indexs2 != null) indexs2.setVisibility(View.GONE);

                    indexs.setEnabled(true);
                    CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long l) {

                            if (calldone) {

                                if (call_current_position < arrayList_matserId.size()) {
                                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editss = chkfss.edit();
                                    editss.putString("call", "CALL DONE");
                                    editss.commit();
                                }
                            }


                        }

                        @Override
                        public void onFinish() {


                        }
                    };
                    countDownTimer.start();
                    recyclerView.scrollToPosition(call_current_position);


                    if (isLongPress) {
                        if (indexs != null) indexs.setText("1");
                        if (indexs2 != null) indexs2.setText("1");
                        isLongPress = false;
                    }

                    try {
                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        common_calling_color_sharedPreferances.setcallcurrentstatus("d");


                        OngoingCall.hangup();
                        if (DashboardActivity.callsk) {
                            DashboardActivity.callsk = false;
                        }


                        if (CommonVariables.endcallclick || CommonVariables.receivecall || CommonVariables.incoming) {

                            OngoingCall.hangup();
                            CommonVariables.endcallclick = false;
                            CommonVariables.receivecall = false;
                            CommonVariables.incoming = false;
                            callstuck = true;
                       /* Intent intent= new Intent(S_R_Activity.this,DashboardActivity.class);
                        finish();
                        startActivity(intent);*/
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    try {
                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            CallService.discon1();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                } else {


                    PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();
                }


            }
        });
        pausecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detector.isInternetAvailable()) {

                    if (position >= serviceReminderDataList.size()) {
                        Toasty.warning(PSF_Reminder_Activity.this, "No more data for calling", Toast.LENGTH_SHORT).show();
                        return;

                    }

                    if (indexs != null) indexs.setVisibility(View.GONE);
                    if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
                    if (indexs2 != null) indexs2.setText("" + position);
                    Current_Index_Value = position;


                    CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long l) {

                            if (calldone) {

                                if (call_current_position < arrayList_matserId.size()) {
                                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(position - 1), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editss = chkfss.edit();
                                    editss.putString("call", "CALL DONE");
                                    editss.commit();
                                }
                            }


                        }

                        @Override
                        public void onFinish() {


                        }
                    };
                    countDownTimer.start();
                    recyclerView.scrollToPosition(call_current_position);

                    if (DashboardActivity.callsk) {
                        DashboardActivity.callsk = false;
                    }
                    try {

                        OngoingCall.hangup();


                    } catch (Exception e) {

                    }

                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {
                            clearCallUISharePreferances();
                            scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                            try {

                                int asg = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());

                                common_calling_color_sharedPreferances.setPosition(String.valueOf(asg));


                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            OngoingCall.call.disconnect();
                            if (OngoingCall.call4.size() > 0) OngoingCall.call4.get(0).disconnect();


                            looping();

   /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
    startActivity(i);
    finish();*/


                        }


                    } catch (Exception e) {

                    }

                } else {


                    PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    endcall.performClick();
                }

                //  Toast.makeText(S_R_Activity.this, ""+call_current_position, Toast.LENGTH_SHORT).show();


            }
        });
        Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Intent refresh = new Intent(PSF_Reminder_Activity.this, PSF_Reminder_Activity.class);
                startActivity(refresh);
                finish();
            }
        });

        cb_calldone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    startActivity(new Intent(PSF_Reminder_Activity.this, PSF_CallDoneActiivty.class));
                    finish();


                } else {

                    if (commonSharedPref.getLoginData() != null) {

                        params = new HashMap<>();
                        params.put("Type", ApiConstant.SR);
                        params.put("UserName", CommonVariables.UserId);
                        apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(), params);
                        //  networkCheckerService.UnregisterChecker();
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    isCalldoneChecked = false;
                    CommonVariables.isCalldoneChecked = false;

                    endcall.setVisibility(View.VISIBLE);
                    pausecall.setVisibility(View.VISIBLE);
                    startcall.setVisibility(View.VISIBLE);
                    ll_index.setVisibility(View.VISIBLE);
                    serviceReminderDataList.clear();
                   /* if(menuItem != null)menuItem.setVisible(true);
                    toolbar.setTitle("");*/

                    adapter.notifyDataSetChanged();
                    getservicereminderdata(token, "0");
                    apiController.getPSFRatingQuestionsList(token);
                    apiController.getPSFDissatisfactionReason(token);
                }
            }
        });


        /*---------------------------------------------------------------------------------------*/


    }

    /*--------------------------------------------------------Calling Logic----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void looping() {

        if (detector.isInternetAvailable()) {
            Callingstatus = "BUSY";

            if (common_calling_color_sharedPreferances != null && common_calling_color_sharedPreferances.getCall_start_stop_status() != null && common_calling_color_sharedPreferances.getCall_start_stop_status().equals("start")) {
                try {
                    scrollmovepostion(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()));
                    call_current_position = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());


                    SharedPreferences chkfss = getSharedPreferences(arrayList_matserId.get(call_current_position), Context.MODE_PRIVATE);
                    final String icca = chkfss.getString("call", "");
                    string_called_status = arrayList_callstatus.get(call_current_position);


                    if (string_called_status.equals("CALL DONE")) {

                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                        looping();
                    } else if (icca.equals("CALL DONE")) {

                        common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position + 1));
                        looping();
                    } else {
                        if (detector.isInternetAvailable()) {

                            common_calling_color_sharedPreferances.setcurrentcallmobileno(arrayList_mobileno.get(call_current_position));
                            common_calling_color_sharedPreferances.setcallcurrentstatus("s");
                            common_calling_color_sharedPreferances.setmasterid(arrayList_matserId.get(call_current_position));
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(call_current_position));
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                            if (checkSelfPermission(CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {


                                if (sharedPreferencescallUI.contains("CALLUI")) {

                                    clearCallUISharePreferances();
                                }


                                String uid = "";

                                if (CommonVariables.calldelaytime > 0) {

                                    CountDownTimer countDownTimer = new CountDownTimer(CommonVariables.calldelaytime, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            return;

                                        }

                                        @Override
                                        public void onFinish() {


                                            //Log.e("Mobilenumber"," "+arrayList_mobileno.get(position-1));

                                            // Uri uri = Uri.parse("tel:" +"+91"+ "" + CommonVariables.SR_Testingnumber);
                                            CommonVariables.mobilenumber=arrayList_mobileno.get(position);

                                            Uri uri = Uri.parse("tel:" + "+91"+arrayList_mobileno.get(position-1));
                                            startActivity(new Intent(Intent.ACTION_CALL, uri));


                                        }
                                    };
                                    countDownTimer.start();


                                    position = position + 1;

                                    uid = UUID.randomUUID().toString();
                                } else {
                                    CountDownTimer countDownTimer = new CountDownTimer(CommonVariables.calldelaytime, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            return;

                                        }

                                        @Override
                                        public void onFinish() {


                                            // Log.e("Mobilenumber"," "+arrayList_mobileno.get(position));

                                            //Uri uri = Uri.parse("tel:" +"+91"+ "" + CommonVariables.SR_Testingnumber);
                                            CommonVariables.mobilenumber=arrayList_mobileno.get(position);

                                            Uri uri = Uri.parse("tel:" + "+91"+arrayList_mobileno.get(position));
                                            startActivity(new Intent(Intent.ACTION_CALL, uri));


                                        }
                                    };
                                    countDownTimer.start();


                                    position = position + 1;

                                    uid = UUID.randomUUID().toString();

                                }


                                try {
                                    string_masterId = arrayList_matserId.get(call_current_position);
                                    string_mobileno = arrayList_mobileno.get(call_current_position);
                                    string_customername = arrayList_customername.get(call_current_position);

                                } catch (IndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                }
                                SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editsas = prefsss.edit();
                                editsas.putString(string_masterId, uid);
                                editsas.commit();

                            } else {
                                // Request permission to call
                                ActivityCompat.requestPermissions(PSF_Reminder_Activity.this, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                            }
                            String uid = UUID.randomUUID().toString();


                            try {
                                string_masterId = arrayList_matserId.get(call_current_position);
                                string_mobileno = arrayList_mobileno.get(call_current_position);
                                string_customername = arrayList_customername.get(call_current_position);
                            } catch (IndexOutOfBoundsException e) {

                            }
                            SharedPreferences prefsss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editsas = prefsss.edit();
                            editsas.putString(string_masterId, uid);
                            editsas.commit();
                        } else {
                            Toasty.error(this, "Please connect Internet Connection", Toast.LENGTH_SHORT).show();

                        }
                    }
                } catch (IndexOutOfBoundsException e) {

                    e.printStackTrace();
                }
            } else {

            }

        } else {
            endcall.performClick();
            PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint({"SetTextI18n", "CheckResult"})
    public void updateUi(Integer state) {

        ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);

        if (CommonVariables.incoming) {
            //  adapter.notifyDataSetChanged();

            try {
                Thread.sleep(3000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ////Log.e("income333", "disconnected1111 "+CommonVariables.incoming);

            CommonVariables.incoming = false;
            return;
        }


        // Set callInfo text by the state
        // a.setText(CallStateString.asString(state).toLowerCase() + "\n" + number);
        this.state = state;
        if (state == Call.STATE_DIALING) {
            if (detector.isInternetAvailable()) {

                stop = false;
                final Handler handler = new Handler();
                currentdail = Calendar.getInstance().getTime();
                final int delay = 4000;
                calldone = false;
                if (indexs != null) indexs.setVisibility(View.GONE);
                if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
                if (indexs2 != null) indexs2.setText("" + position);
                Current_Index_Value = position;

               /* handler.postDelayed(new Runnable() {
                    public void run() {


                    }
                }, delay);*/
                if (!stop) {

                    if (sharedPreferencescallUI.contains("CALLUI")) {
                    } else {
                        OngoingCall.call2.add(OngoingCall.call);
                        try {

                            recordService = new Intent(this, CallRecorderService.class);
                            startService(recordService);
                          //  startRecording(historyID);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                            int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                            try {
                                string_masterId = arrayList_matserId.get(ass);
                                string_mobileno = arrayList_mobileno.get(ass);
                                string_customername = arrayList_customername.get(ass);
                            } catch (IndexOutOfBoundsException e) {

                                e.printStackTrace();
                            }

                        }


                        if (contactstatus != null && position == dataposition) {
                               /* contactstatus.setSelection(1);
                                contactstatus.setEnabled(true);*/

                            if (contactstatus != null) contactstatus.setSelection(1);
                            if (contactstatus != null) contactstatus.setEnabled(false);
                            // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

                        }


                        Callingstatus = "BUSY";
                        SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        historyID = chkfss2.getString(string_masterId, "");


                        AddHistoryId(token, string_masterId, historyID, Callingstatus);

                        // Toast.makeText(S_R_Activity.this, ""+arrayList_customername.get(position), Toast.LENGTH_SHORT).show();

                        updatecontatctstatus(token, historyID, "Not Contacted", getString(R.string.SRTAG));


                    }


                }
            } else {
                endcall.performClick();
                PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


            }


        } else if (state == Call.STATE_ACTIVE) {
            if (detector.isInternetAvailable()) {

                calldone = true;
                if (indexs != null) indexs.setVisibility(View.GONE);
                if (indexs2 != null) indexs2.setVisibility(View.VISIBLE);
                if (indexs2 != null) indexs2.setText("" + position);
                Current_Index_Value = position;
                if (contactstatus != null && position == dataposition) {
                    contactstatus.setSelection(0);
                    contactstatus.setEnabled(false);
                    // //Log.e("FollowUpContatctStatus1","Follow"+position+"  "+dataposition+"  "+string_masterId);

                }

                // Toast.makeText(this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                if (sharedPreferencescallUI.contains("CALLUI")) {

                } else {

                    Callingstatus = "CALL DONE";
                    SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editsfs = prefss.edit();
                    editsfs.putString("call", Callingstatus);
                    editsfs.commit();

                    SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                    historyID = chkfss2.getString(string_masterId, "");
                    updatecalledstatus(token, historyID, "" + Callingstatus, getString(R.string.SRTAG));
                    updatecontatctstatus(token, historyID, "Contacted", getString(R.string.SRTAG));


                }
            } else {
                endcall.performClick();
                PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


            }


        } else if (state == Call.STATE_DISCONNECTED && !CommonVariables.incoming) {

            if (detector.isInternetAvailable()) {

                if (indexs != null) indexs.setVisibility(View.VISIBLE);
                if (indexs2 != null) indexs2.setVisibility(View.GONE);

                ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());


                ////Log.e("income3", "disconnected1111 "+Calendar.getInstance().getTimeInMillis());

                //     Toast.makeText(this, "disconnected1111 "+Calendar.getInstance().getTimeInMillis(), Toast.LENGTH_SHORT).show();

                if (CommonVariables.receivecall) adapter.notifyDataSetChanged();


                if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {
                    int ass = Integer.parseInt(common_calling_color_sharedPreferances.getPosition());
                    try {
                        if(arrayList_matserId.size()>0)string_masterId = arrayList_matserId.get(ass);
                        if(arrayList_mobileno.size()>0) string_mobileno = arrayList_mobileno.get(ass);
                        if(arrayList_customername.size()>0) string_customername = arrayList_customername.get(ass);

                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }

                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");

                updatecallendtime(token, historyID, getString(R.string.SRTAG));

                stopService(recordService);
                stopRecording(historyID);

                discom = Calendar.getInstance().getTime();
                try {
                    long diffInMs = discom.getTime() - currentdail.getTime();
                    long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                    if (diffInSec > 3) {
                        stop = false;
                    } else {
                        stop = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (sharedPreferencescallUI.contains("CALLUI")) {

                } else {
                    OngoingCall.call2.clear();


                    if (common_calling_color_sharedPreferances.getSharePreferancesContains().contains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES)) {

                        common_calling_color_sharedPreferances.clearposition();

                    }


                    common_calling_color_sharedPreferances.setcallcurrentstatus("d");

                    try {

                        if (DashboardActivity.callsk) {
                            DashboardActivity.callsk = false;
                            /*asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) ;*/
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition())));
                        } else {
                            /* asgkkk = Integer.parseInt(common_calling_color_sharedPreferances.getPosition())+1 ;*/
                            common_calling_color_sharedPreferances.setPosition(String.valueOf(Integer.parseInt(common_calling_color_sharedPreferances.getPosition()) + 1));
                        }
                        /* common_calling_color_sharedPreferances.setPosition(String.valueOf(asgkkk))*/

                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }


                    if (PressStop_Button) {

                        PressStop_Button = false;

                    } else {
                        SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editsfs = prefss.edit();
                        editsfs.putString("call", Callingstatus);
                        editsfs.commit();


                    }


                    looping();


                }


                adapter.notifyDataSetChanged();

            } else {

                endcall.performClick();
                PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }


         /*   try {
                sleep(100000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/


        } else if (state == Call.STATE_DISCONNECTING) {
            if (detector.isInternetAvailable()) {
                SharedPreferences chkfss2 = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
                historyID = chkfss2.getString(string_masterId, "");

                adapter.notifyDataSetChanged();
                updatecallendtime(token, historyID, getString(R.string.SRTAG));
            } else {

                endcall.performClick();
                PSF_Reminder_Activity.this.registerReceiver(PSF_Reminder_Activity.this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }


        } else if (!(state == Call.STATE_ACTIVE)) {
            if (Callingstatus.equals("CALL DONE")) {

            } else {
                Callingstatus = "BUSY";

            }
        } else if (!(state == Call.STATE_RINGING)) {
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void offerReplacingDefaultDialer() {


       /* TelecomManager systemService = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            systemService = this.getSystemService(TelecomManager.class);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
            }
        }*/
        if (Build.VERSION.SDK_INT <= 28) {
            TelecomManager systemService = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                systemService = this.getSystemService(TelecomManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(this.getPackageName())) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, this.getPackageName()));
                }
            }
        } else {
            @SuppressLint("WrongConstant")
            RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
            Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
            startActivityForResult(intent, 101);


        }
    }
    /*------------------------------------------------------------------------------------------------------------------------*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void finish(Integer state) {

        ////Log.e("newStatekkkk",""+state);


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStop() {
        super.onStop();
        customWaitingDialog.dismiss();
        disposables.clear();


    }

    /*-------------------------------------------------Activity Deafult Function-----------------------------------------------------------------*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {

        super.onStart();
        offerReplacingDefaultDialer();

        notcomingreasonlist[0].clear();



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onPause() {

        customWaitingDialog.dismiss();

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (PSF_Reminder_Activity.whatsupclick == false && state == Call.STATE_ACTIVE && !CommonVariables.incoming
                && PSF_Reminder_Activity.textmessageclick == false) {

            /*OngoingCall.hangup();

            startActivity(new Intent(S_R_Activity.this,DashboardActivity.class));
            // pausestate=false;
            finish();*/

            // OngoingCall.hangup();
            //  adapter.notifyDataSetChanged();


        }


        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Toast.makeText(this, serviceReminderDataList.size()+"  onResume "+adapter.getItemCount(), Toast.LENGTH_SHORT).show();

        if (Sales_Followup_Activity.whatsupclick || Sales_Followup_Activity.textmessageclick) {

            Sales_Followup_Activity.whatsupclick = false;
            Sales_Followup_Activity.textmessageclick = false;
            new OngoingCall();
            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
            disposables.add(disposable);
            //adapter.notifyDataSetChanged();
        } else {
//            new OngoingCall();
//            Disposable disposable = OngoingCall.state.subscribe(this::updateUi);
//            //Disposable disposabless = OngoingCall.state.subscribe(this::retrofitUiUpdate);
//            disposables.add(disposable);
        }


    }
    /*------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------Other Function -----------------------------------------------------------------*/

    @Override
    public void onBackPressed() {

        if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null
                && !common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {

            final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit \n Service Reminder?");
            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {


                    if (state == Call.STATE_ACTIVE) {

                        OngoingCall.hangup();

                    }


                    //  adapter.notifyDataSetChanged();
                    String packagename = commonSharedPref.getPackagesList();
                    if (Build.VERSION.SDK_INT <= 28) {
                        //  Toast.makeText(S_R_Activity.this, "back ", Toast.LENGTH_SHORT).show();

                        TelecomManager systemService = PSF_Reminder_Activity.this.getSystemService(TelecomManager.class);
                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                        }
                        shippingDialog.dismiss();

                    } else {
                        //Toast.makeText(S_R_Activity.this, "back 2", Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                        startActivity(i);
                        shippingDialog.dismiss();

                    }

                    try {

                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        recyclerView.setAdapter(adapter);


//                    else {
////                  /*      TelecomManager systemService = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
////
////                      //  Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();
////                       Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER);
////                       intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
////                               packagename);
////                                        startActivity(intent);*/
////
////                  /*      @SuppressLint("WrongConstant")
////                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
////                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
////                        startActivityForResult(intent, 101);*/
////
////
////
////                /*        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
////                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.android.contacts"));
////                        }
////*/
////
////
////                   /*     Intent showSettings = new Intent();
////                        showSettings.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
////                        Uri uriAppSettings = Uri.fromParts("package", "com.android.phone", null);
////                        showSettings.setData(uriAppSettings);
////                        startActivity(showSettings);*/
//                       /* TelecomManager systemService = S_R_Activity.this.getSystemService(TelecomManager.class);
//                        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
//                        }*/
//
//                        Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
//                         startActivity(i);
//                        shippingDialog.dismiss();
//
//
//                 /*       try{
//                            Runtime.getRuntime().exec("adb shell pm reset-permissions");
//                            finish();
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//*/
//                      //  clearApplicationData(S_R_Activity.this);
//
//
////
////                        @SuppressLint("WrongConstant")
////                          String ACTION_REQUEST_ROLE = "android.app.role.action.REQUEST_ROLE";
////                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
////                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
////                        startActivityForResult(intent, 10);
//                        shippingDialog.dismiss();
//                    }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });
            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();

        } else if (common_calling_color_sharedPreferances.getcallcurrentstatus() == null) {
            final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.back_popup);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView title = shippingDialog.findViewById(R.id.title);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            TextView canncelbutton = shippingDialog.findViewById(R.id.cancelbutton);
            title.setText("Are you sure you want to exit \n Service Reminder?");
            okbutton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {


                    if (state == Call.STATE_ACTIVE) {

                        OngoingCall.hangup();

                    }

                    try {

                        common_calling_color_sharedPreferances.setCall_start_stop_status("stop");
                        OngoingCall.hangup();
                        recyclerView.setAdapter(adapter);

                        //  adapter.notifyDataSetChanged();
                        String packagename = commonSharedPref.getPackagesList();
                        if (Build.VERSION.SDK_INT <= 28) {
                            //Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();

                            TelecomManager systemService = PSF_Reminder_Activity.this.getSystemService(TelecomManager.class);
                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                            }
                            shippingDialog.dismiss();

                        } else if (Build.MODEL.equals("Redmi 7A")) {
                            Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                            startActivity(i);

                        } else {
//                  /*      TelecomManager systemService = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
//
//                      //  Toast.makeText(S_R_Activity.this, "back "+systemService.getDefaultDialerPackage(), Toast.LENGTH_SHORT).show();
//                       Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER);
//                       intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
//                               packagename);
//                                        startActivity(intent);*/
//
//                  /*      @SuppressLint("WrongConstant")
//                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
//                        startActivityForResult(intent, 101);*/
//
//
//
//                /*        if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                            startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, "com.android.contacts"));
//                        }
//*/
//
//
//                   /*     Intent showSettings = new Intent();
//                        showSettings.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        Uri uriAppSettings = Uri.fromParts("package", "com.android.phone", null);
//                        showSettings.setData(uriAppSettings);
//                        startActivity(showSettings);*/
//                            TelecomManager systemService = S_R_Activity.this.getSystemService(TelecomManager.class);
//                            if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
//                                startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
//                            }
                            shippingDialog.dismiss();
                            Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                            startActivity(i);

                 /*       try{
                            Runtime.getRuntime().exec("adb shell pm reset-permissions");
                            finish();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
*/
                            //  clearApplicationData(S_R_Activity.this);


//
//                        @SuppressLint("WrongConstant")
//                          String ACTION_REQUEST_ROLE = "android.app.role.action.REQUEST_ROLE";
//                        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
//                        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
//                        startActivityForResult(intent, 10);
                            shippingDialog.dismiss();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        if (sharedPreferencescallUI.contains("CALLUI")) {

                            clearCallUISharePreferances();
                            CallService.discon1();
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();

                    shippingDialog.dismiss();
                    clearCallUISharePreferances();
                    common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
                    common_calling_color_sharedPreferances.clear__start_stop_status();
                    common_calling_color_sharedPreferances.clearposition();
                    common_calling_color_sharedPreferances.clearSheer();
                    SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
                    SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
                    SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
                    SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
                    SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
                    SharedPreferences.Editor editpostion = prefposition.edit();
                    SharedPreferences.Editor editceer = prefceer.edit();
                    SharedPreferences.Editor editcall = prefcall.edit();
                    editpostion.clear();
                    editcall.clear();
                    editceer.clear();
                    CallingColoreditor.clear();
                    CallingColoreditor.commit();
                    editcall.commit();
                    editceer.commit();
                    editpostion.commit();
                    clearcolorsharedeprefances();
                }
            });
            canncelbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shippingDialog.dismiss();
                }
            });
            shippingDialog.show();
        } else {
            Toasty.warning(PSF_Reminder_Activity.this, "Call is Currently Working", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onDestroy() {
        customWaitingDialog.dismiss();


        if (commonSharedPref.getLoginData() != null) {

            params = new HashMap<>();
            params.put("Type", ApiConstant.SR);
            params.put("UserName", CommonVariables.UserId);
            apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(), params);
            super.onDestroy();
            //  networkCheckerService.UnregisterChecker();
            disposables.dispose();

            OngoingCall.hangup();


        }
        try {
            Runtime.getRuntime().gc();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void scrollmovepostion(Integer ab) {


        recyclerView.scrollToPosition(ab);

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    boolean isLastVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
        lastposition = layoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = adapter.getItemCount();
        int sizes = serviceReminderDataList.size();
        return (lastposition >= numItems - 1);
    }

    /*------------------------------------------------------------------------------------------------------------------*/

    // Clear Call Shared Preferances
    public void clearCallUISharePreferances() {
        sharedPreferencescallUI = getSharedPreferences("CALLUI", Context.MODE_PRIVATE);
        SharedPreferences.Editor editss = sharedPreferencescallUI.edit();
        editss.clear();
        editss.commit();
        editss.apply();

    }

    /*---------------------------------------------------API Call Methods------------------------------------------------------------*/
    // Get Service Reminder Data
    public void getservicereminderdata(String token, String offset) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.offset, offset);
        apiController.getPSFData(token, params);
    }


    public void setApi(APIInterface githubApi) {
        apiInterface = githubApi;
    }

    // Add History Id Function
    public void AddHistoryId(String token, String mastersrid, String historyId, String callingstatus) {
        // customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MasterPSFId", mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BoundType, getString(R.string.OUTBOUND));
        apiController.addPSFHistory(token, params);
        SharedPreferences prefss = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor editsfs = prefss.edit();
        editsfs.putString("call", callingstatus);
        editsfs.commit();

    }

    // Update Contatct Number Function
    public void UpdateContatctNumber(String token, String mastersrid, String historyId, String newcontactno, String priorotyStatus) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MasterPSFId", mastersrid);
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.NewContactNo, newcontactno);
        params.put(ApiConstant.PriorityStatus, priorotyStatus);
        apiController.UpdateContactNumber(token, params);

    }

    // Update Customer Reply Function
    public void updatecustomerreply(String token, String historyId, String Reply) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reply, Reply);
        apiController.UpdateCustomerReply(token, params);
    }

    // Update Tagged SR Status
    public void updatetaggedsrstatus(String token, String historyId, String Status) {
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, Status);
        apiController.UpdateTaggedSRStatus(token, params);
    }

    // Update Booking Date & Time
    public void updatebookingdate(String token, String historyId, String bookingdate, String string_masterId) {

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.BookingDate, bookingdate);
        apiController.UpdateBookingate(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "booking");
        edits.commit();
    }

    // Update Call End Time
    public void updatecallendtime(String token, String historyId, String calltype) {
        // customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallType, "PSF");

        apiController.UpdateCallEndTime(token, params);
    }

    // Update Remark
    public void updateremark(String token, String historyId, String remark, String calltype, String string_masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Comment, remark);
        params.put(ApiConstant.CallType, "PSF");
        apiController.UpdateRemark(token, params);
        SharedPreferences prefs = getSharedPreferences(string_masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status1", "rmkcolor");
        edits.commit();

    }

    // Update Follow-Up Date
    public void updatefollowupdate(String token, String historyId, String followdatetime, String calltype, String masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.FollowupDateTime, followdatetime);
        params.put(ApiConstant.CallType, "PSF");
        apiController.UpdateNextFollowUpdate(token, params);
        SharedPreferences prefs = getSharedPreferences(masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "NextFollowUp");
        edits.commit();
    }

    // Update Contatct Status
    public void updatecontatctstatus(String token, String historyId, String contactstatys, String calltype) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, contactstatys);
        params.put(ApiConstant.CallType, "PSF");

        apiController.UpdateContactStatus(token, params);
    }

    // Update Contatct Status
    public void updatepickanddrop(String token, String historyId, String status) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Status, status);

        apiController.UpdatePickdrop(token, params);
    }

    // Update CalledStatus
    public void updatecalledstatus(String token, String historyId, String callstatus, String calltype) {
        //customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.CallStatus, callstatus);
        params.put(ApiConstant.CallType, "PSF");

        apiController.UpdateCalledStatus(token, params);
    }

    // Update NotComingReason
    public void updatenotcomingReason(String token, String historyId, String reason, String masterId) {


        params = new HashMap<>();
        params.put(ApiConstant.HistoryId, historyId);
        params.put(ApiConstant.Reason, reason);
        apiController.UpdateNotComingReason(token, params);
        SharedPreferences prefs = getSharedPreferences(masterId, Context.MODE_PRIVATE);
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString("Status", "NCR");
        edits.commit();
    }

    // Update get Service Reminder History Data
    public void getSRHistoryData(String token, String mobile) {

        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("MobileNo", mobile);
        params.put("Tag", "PSF");
        apiController.getPSFHistoryData(token, params);
    }

    //Get Single Customer Service Reminder History Data
    public void getSingleCustomerSRHistoryData(String token, String tag, String masterId) {

        customWaitingDialog.show();

        params = new HashMap<>();
        params.put(ApiConstant.MasterId, masterId);
        params.put(ApiConstant.Tag, "PSF");

        apiController.getSingleCustomerHistoryDataPSF(token, params);
    }

    // API Respose of Contact Status List(In this Function Get API Responses of Spinner Lists)
    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (beanTag.matches(ApiConstant.SERVICEREMINDERDATA)) {

            if (serviceReminderDataList.size() == 0) offset = "0";
            if (morebtnclicked) {
                morebtnclicked = false;
            } else {
                serviceReminderDataList.clear();
                arrayList_mobileno.clear();
                arrayList_callstatus.clear();
                arrayList_customername.clear();
                arrayList_matserId.clear();

            }
            PSFDataModel serviceReminderDataModel = (PSFDataModel) superClassCastBean;


            if (serviceReminderDataModel.getData().toString().contains("No data to allocate.")) {
                customWaitingDialog.dismiss();
                nodata_found_layout.setVisibility(View.VISIBLE);

            } else {
                if (serviceReminderDataModel.getData() != null) {
                    // serviceReminderDataList.clear();

                    for (PSFDataModel.DataDTO dataBean : serviceReminderDataModel.getData()) {

                        offset = "" + dataBean.getSno();
                        serviceReminderDataList.add(dataBean);
                        calling_list_serial_no = dataBean.getSno();
                        arrayList_mobileno.add(dataBean.getCustomercontact());
                        arrayList_matserId.add(dataBean.getId());
                        arrayList_customername.add(dataBean.getCustomerfirstname());
                        arrayList_callstatus.add(dataBean.getCalledstatus());
                        nodata_found_layout.setVisibility(View.GONE);


                    }

                    ////Log.e("SRdatalistsize ","  "+serviceReminderDataList.size());

                    recyclerView.setAdapter(adapter);
                    customWaitingDialog.dismiss();
                    adapter.notifyDataSetChanged();


                } else {
                    nodata_found_layout.setVisibility(View.VISIBLE);
                    customWaitingDialog.dismiss();

                }

            }


        }
        else if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setAllClear();
            commonSharedPref.setLoginData(tokenModel);
            customWaitingDialog.dismiss();
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();


            if (start_call_button)
            {
                start_call_button=false;

            }
            else
            {
                apiController.getNotComingReason(token);
                apiController.getContactStatus(token);
                apiController.getCustomerReply(token);
                params = new HashMap<>();
                params.put("UserName", CommonVariables.UserId);
                apiController.getUserData(token, params);

                if (serviceReminderDataList.size() == 0) getservicereminderdata(token, "0");

            }
            ////Log.e("LLLLLLL  ",token);



        }
        else if (beanTag.matches(ApiConstant.CONTACT_STATUS)) {
            ContactStatusModel contactStatusModel = (ContactStatusModel) superClassCastBean;
            if (contactStatusModel.getData() != null && contactStatusModel.getData().size() > 0) {
                //  contactStatuslist[0].add("Contact Status");
                for (ContactStatusModel.DataBean dataBean : contactStatusModel.getData()) {
                    contactStatuslist[0].add(dataBean.getStatus());

                }
            }
        }   else if (beanTag.matches(ApiConstant.GETPSFQUESTIONLIST)) {
            PSFSOPRatingQuestionsModel contactStatusModel = (PSFSOPRatingQuestionsModel) superClassCastBean;
            if (contactStatusModel.getData() != null && contactStatusModel.getData().size() > 0) {
                //  contactStatuslist[0].add("Contact Status");
                for (PSFSOPRatingQuestionsModel.DataDTO dataBean : contactStatusModel.getData()) {
                    questionlist.add(dataBean);

                }



                    adapter_psfQuestionRating = new Adapter_PSFQuestionRating(this,this,questionlist);
                    rv_questionrating.setAdapter(adapter_psfQuestionRating);
                    adapter_psfQuestionRating.notifyDataSetChanged();


            }
        } else if (beanTag.matches(ApiConstant.GETPSFDISATISFACTIONREASONLIST)) {
            PSFDissatisfactionReasonModel contactStatusModel = (PSFDissatisfactionReasonModel) superClassCastBean;
            if (contactStatusModel.getData() != null && contactStatusModel.getData().size() > 0) {
                //  contactStatuslist[0].add("Contact Status");
                reasonlist[0].clear();

                reasonlist[0].add("");

                for (PSFDissatisfactionReasonModel.DataDTO dataBean : contactStatusModel.getData()) {
                    dissatisfactionlist.add(dataBean);

                    reasonlist[0].add(dataBean.getReason());


                }

                reasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, reasonlist[0]);
                sp_reason.setAdapter(reasonArrayAdapter);

            }
        } else if (beanTag.matches(ApiConstant.CUSTOMER_REPLY)) {
            CustomerReplyModel customerReplyModel = (CustomerReplyModel) superClassCastBean;
            if (customerReplyModel.getData() != null && customerReplyModel.getData().size() > 0) {
                customerreplylist[0].add("Customer Reply");
                for (CustomerReplyModel.DataBean dataBean : customerReplyModel.getData()) {
                    customerreplylist[0].add(dataBean.getReply());

                }
            }
        } else if (beanTag.matches(ApiConstant.NOT_COMING_REASON)) {
            NotComingReasonModel notComingReasonModel = (NotComingReasonModel) superClassCastBean;
            if (notComingReasonModel.getData() != null && notComingReasonModel.getData().size() > 0) {
                notcomingreasonlist[0].add("  ");
                for (NotComingReasonModel.DataBean dataBean : notComingReasonModel.getData()) {

                    notcomingreasonlist[0].add(dataBean.getReason());

                }
            }
        } else if (beanTag.matches(ApiConstant.ADDCALLHISTORYUPDATESR)) {
            AddCallHistoryUpdateModel addCallHistoryUpdateModel = (AddCallHistoryUpdateModel) superClassCastBean;
            if (addCallHistoryUpdateModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

            }
        } else if (beanTag.matches(ApiConstant.UPDATECALLENDTIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

            }
        }
        else if (beanTag.matches(ApiConstant.SINGLEDATAPSF)) {

            customWaitingDialog.dismiss();
            singledatalist.clear();
            PSFSingleDataModel singleCustomerHistoryData = (PSFSingleDataModel) superClassCastBean;
            if (singleCustomerHistoryData.getData() != null && singleCustomerHistoryData.getData().size() > 0) {

                singledatalist.addAll(singleCustomerHistoryData.getData());
                oldhid = singleCustomerHistoryData.getData().get(0).getMasterpsfid();

                String ContatctStatus = singleCustomerHistoryData.getData().get(0).getCalledstatus();

                if ((singledatalist.get(0).getBookingdate().length() == 0)
                        && (singledatalist.get(0).getFollowupdate().length() == 0)
                        && (singledatalist.get(0).getRemark().length() == 0))
                        {
                    et_bookingdate.setText("");
                    et_nextfollowupdate.setText("");
                    remarks.setText("");


                } else {
                    submit_btn.setText("Resubmit");
                }
                if (ContatctStatus != null && ContatctStatus.equals("Contacted")) {
                    calldone = true;
                    contactstatus.setSelection(0);
                    contactstatus.setEnabled(false);

                } else if (ContatctStatus != null && ContatctStatus.equals("Not Contacted")) {
                    calldone = false;
                    contactstatus.setSelection(1);
                    contactstatus.setEnabled(false);

                } else if (ContatctStatus != null && ContatctStatus.toString().trim().equals("Wrong Number")) {
                    contactstatus.setSelection(2);
                    contactstatus.setEnabled(false);


                } else if (ContatctStatus == null || ContatctStatus.length() == 0) {
                    //followuptype_spinner.setEnabled(false);
                    contactstatus.setSelection(1);
                }


                try {
                    remarks.setText("" + singleCustomerHistoryData.getData().get(0).getRemark());
                    if (singleCustomerHistoryData.getData().get(0).getFollowupdate().contains("1900-01-01T00:00:00") || singleCustomerHistoryData.getData().get(0).getFollowupdate().isEmpty()) {

                        et_nextfollowupdate.setText("");
                    } else {

                        et_nextfollowupdate.setText(dateparse2(singleCustomerHistoryData.getData().get(0).getFollowupdate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "").replaceAll(".0000000", "")));
                    }
                    if (singleCustomerHistoryData.getData().get(0).getBookingdate().contains("1900-01-01T00:00:00")) {

                        edit_Bookkingno.setText("");

                    } else if (singleCustomerHistoryData.getData().get(0).getBookingdate().length() > 0) {


                        et_bookingdate.setText(dateparse1(singleCustomerHistoryData.getData().get(0).getBookingdate().replaceAll("T", " ").replaceAll("1900-01-01T00:00:00", "")));
                        //  et_bookingdate.setEnabled(false);


                    } else {

                        et_bookingdate.setText("");
                        //  et_bookingdate.setEnabled(true);

                    }


//                    sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getNot_coming_reason().trim()));
//                    //contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getContact_status()));
//                    customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getCustomerreply()));
//                    sp_pickanddrop.setSelection(((ArrayAdapter<String>) sp_pickanddrop.getAdapter()).getPosition(singleCustomerHistoryData.getData().get(0).getPickup_drop()));

                } catch (Exception e) {

                    e.printStackTrace();

                }


            }
        }
        else if (beanTag.matches(ApiConstant.UPDATECALLEDSTATUS)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

            }
        } else if (beanTag.matches(ApiConstant.PSFHISTORYDATA)) {
            serviceReminderHistoryDataList.clear();
            SR_HistoryModel sr_historyModel = (SR_HistoryModel) superClassCastBean;


            if (sr_historyModel.getData().toString().equals("[]")) {
                Toast.makeText(this, "No History Created Yet..!!", Toast.LENGTH_SHORT).show();
                adapter_sr_historydata.notifyDataSetChanged();

                customWaitingDialog.dismiss();
            } else {
                if (sr_historyModel.getData() != null) {
                    for (SR_HistoryModel.DataBean dataBean : sr_historyModel.getData()) {
                        serviceReminderHistoryDataList.add(dataBean);

                    }
                    customWaitingDialog.dismiss();
                    adapter_sr_historydata.notifyDataSetChanged();

                }
            }
            customWaitingDialog.dismiss();

        } else if (beanTag.matches(ApiConstant.UPLOADRECORDING)) {

            UploadRecordingModel uploadRecordingModel = (UploadRecordingModel) superClassCastBean;
            if (uploadRecordingModel.getData().matches(getString(R.string.CommonAPIResponse))) {
                customWaitingDialog.dismiss();
                //CommonVariables.audiofile.delete();
                CommonVariables.audiofile = null;
            }else
            {
                customWaitingDialog.dismiss();
            }


        } else if (beanTag.matches(ApiConstant.UPDATEBOOKINGDATETIME)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEFOLLOWUPDATE)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATEREMARK)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.UPDATECONTACTNUMBERSR)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().matches(getString(R.string.CommonAPIResponse))) {

                customWaitingDialog.dismiss();

            } else {
                customWaitingDialog.dismiss();

            }
        } else if (beanTag.matches(ApiConstant.GETSMSTEMPLATE)) {
            GetTamplateListModel commonModel = (GetTamplateListModel) superClassCastBean;
            if (commonModel.getData() != null) {
                for (GetTamplateListModel.DataBean dataBean : commonModel.getData()) {

                    templatelist.add(dataBean);
                }
                customWaitingDialog.dismiss();
                adapterSettingTemplet.notifyDataSetChanged();
            } else {
                customWaitingDialog.dismiss();
            }


            //      srtextviewtemplate.setText(commonModel.getData());
        } else if (beanTag.matches(ApiConstant.USERDETAILSDATA)) {
            UserDetailsModel userDetailsModel = (UserDetailsModel) superClassCastBean;
            if (userDetailsModel.getData() != null) {
                for (UserDetailsModel.DataBean dataBean : userDetailsModel.getData()) {
                    if (dataBean.getCall_delay_time() != null && !dataBean.getCall_delay_time().isEmpty()) {
                        CommonVariables.callDelay = dataBean.getCall_delay_time().substring(dataBean.getCall_delay_time().length() - 2, dataBean.getCall_delay_time().length());
                        CommonVariables.calldelaytime = 1000 * Integer.parseInt(CommonVariables.callDelay);

                    } else {
                        CommonVariables.callDelay = "0";
                    }

                }
            } else {
                customWaitingDialog.dismiss();
            }


            //      srtextviewtemplate.setText(commonModel.getData());
        }


    }

    @Override
    public void onFailure(String msg) {
        if (msg.matches(ApiConstant.SERVICEREMINDERDATA)) {
            customWaitingDialog.dismiss();
            nodata_found_layout.setVisibility(View.VISIBLE);

        } else {
        }


    }

    @Override
    public void onError(String msg) {
        if (msg.matches(ApiConstant.SERVICEREMINDERDATA)) {
            customWaitingDialog.dismiss();
            nodata_found_layout.setVisibility(View.VISIBLE);

        } else {
        }

    }


    public void setIndex(int number, boolean longpress) {

        position = number;
        isLongPress = longpress;

        if (indexs != null) indexs.setText("" + number);
        if (indexs2 != null) indexs2.setText("" + number);


    }

    //load more Service reminder data
    public void moreData() {

        ////Log.e("MoreClick",offset);
        customWaitingDialog.show();
        if (serviceReminderDataList.size() >= 10) getservicereminderdata(token, offset);
        morebtnclicked = true;
        CommonVariables.morecount = serviceReminderDataList.size();
        adapter.updateList(serviceReminderDataList);
        scrollmovepostion(Integer.parseInt(offset) + 1);

        morebtncliked = true;
        adapter.updateList(serviceReminderDataList);

        new CountDownTimer(2000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (CommonVariables.morecount < serviceReminderDataList.size()) {
                    scrollmovepostion(CommonVariables.morecount);

                }
            }
        }.start();
    }

    /*---------------------------------------------------------------------------------------------------------------*/

    //set Remarks
    public void setRemarks(EditText remark, TextView suggetions) {
        remark.setText(suggetions.getText().toString());
    }

    /*---------------------------------------------------Adapter Click Listeners----------------------------------------------------------------*/
    // Customer Details Show Fuction( This Function is Show Input Popup and Get Feedback from Customer in this Popup)
    public void getCustomerDetails(int position, String master_ID, String customername,
                                   String servicetype, String model, String serviceduefrom,
                                   String pickdropbalance, String contactnumber, String calledStatus) {
        dataposition = position + 1;
        int alertCounter = 0;
        apiController.getPSFRatingQuestionsList(token);
        apiController.getPSFDissatisfactionReason(token);

        final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);

        notcomingreason = null;
        getSingleCustomerSRHistoryData(token, "PSF", master_ID);


        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.psfratingquestion_layout);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Customer Details Popup Id Genrate Code
        /*-------------------------------------------------*/
        ll_update = shippingDialog.findViewById(R.id.ll_update);
        customerreply = shippingDialog.findViewById(R.id.customerreplay);
        contactstatus = shippingDialog.findViewById(R.id.contactstatus);
        sp_notcomingreason = shippingDialog.findViewById(R.id.notcomingreason);
        sp_reason = shippingDialog.findViewById(R.id.dissatisfactionreason);
        sp_pickanddrop = shippingDialog.findViewById(R.id.pickanddrop);
        et_bookingdate = shippingDialog.findViewById(R.id.bboking);
        ImageButton ib_history = shippingDialog.findViewById(R.id.ib_history);
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        iv_booking = shippingDialog.findViewById(R.id.iv_clear_bookingdate);
        iv_nextfollowupdate = shippingDialog.findViewById(R.id.iv_clear_followupdate);
        et_nextfollowupdate = shippingDialog.findViewById(R.id.nextfollowupedit);
        remarks = shippingDialog.findViewById(R.id.remarks);
        rv_questionrating = shippingDialog.findViewById(R.id.rv_rating);
        rv_questionrating.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        rv_questionrating.hasFixedSize();


        EditText edit_srname = shippingDialog.findViewById(R.id.srcuname);
        EditText edit_srservicetype = shippingDialog.findViewById(R.id.serivcetype);
        EditText edit_srmodel = shippingDialog.findViewById(R.id.srmodel);
        EditText edit_serviceduedate = shippingDialog.findViewById(R.id.srserduefrom);
        edit_Bookkingno = shippingDialog.findViewById(R.id.bookingno);
        EditText edit_pickdrop = shippingDialog.findViewById(R.id.pickdropbalance);

        TextView sugesstion1 = shippingDialog.findViewById(R.id.sugesstion1);
        TextView sugesstion2 = shippingDialog.findViewById(R.id.sugesstion2);
        TextView sugesstion3 = shippingDialog.findViewById(R.id.sugesstion3);
        TextView sugesstion4 = shippingDialog.findViewById(R.id.sugesstion4);
        TextView sugesstion5 = shippingDialog.findViewById(R.id.sugesstion5);
        TextView sugesstion6 = shippingDialog.findViewById(R.id.sugesstion6);
        TextView sugesstion7 = shippingDialog.findViewById(R.id.sugesstion22);


        // getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);
        submit_btn = shippingDialog.findViewById(R.id.submitstatus);

        sp_pickanddrop.setEnabled(false);


        if (singledatalist != null && singledatalist.size() > 0) {
//            if(singledatalist.get(0).getServicebooking() != null && !singledatalist.get(0).getServicebooking().equals(""))
//            {
//
//                et_bookingdate.setText(""+singledatalist.get(0).getServicebooking().toString().replaceAll("T00:00:00",""));
//            }
//
//            if(singledatalist.get(0).getReminder_followup_date() != null && !singledatalist.get(0).getReminder_followup_date().equals(""))
//            {
//                et_nextfollowupdate.setText(""+singledatalist.get(0).getReminder_followup_date().toString().replaceAll("T00:00:00",""));
//            }
//
//            if(singledatalist.get(0).getComments() != null && !singledatalist.get(0).getComments().equals(""))
//            {
//                remarks.setText(""+singledatalist.get(0).getComments());
//            }
        } else {


            if (serviceReminderDataList.get(position).getDate() != null && !serviceReminderDataList.get(position).getDate().equals("")) {
                if (serviceReminderDataList.get(position).getDate().contains("1900-01-01")) {
                    et_bookingdate.setText("");
                } else {
                    et_bookingdate.setText("" + serviceReminderDataList.get(position).getDate().replaceAll("T00:00:00", ""));
                }

            }

         /*   if (serviceReminderDataList.get(position).getReminder_followup_date() != null && !serviceReminderDataList.get(position).getReminder_followup_date().equals("")) {

                if (serviceReminderDataList.get(position).getReminder_followup_date().contains("1900-01-01")) {
                    et_nextfollowupdate.setText("");
                } else {
                    et_nextfollowupdate.setText("" + serviceReminderDataList.get(position).getReminder_followup_date().replaceAll("T00:00:00", ""));
                }

            }

            if (serviceReminderDataList.get(position).getComments() != null && !serviceReminderDataList.get(position).getComments().equals("")) {
                remarks.setText("" + serviceReminderDataList.get(position).getComments());
            }

            if (serviceReminderDataList.get(position).getContact_status() != null && !serviceReminderDataList.get(position).getContact_status().equals("")) {
                try {
                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition(serviceReminderDataList.get(position).getContact_status().trim()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (serviceReminderDataList.get(position).getNot_coming_reason() != null && !serviceReminderDataList.get(position).getNot_coming_reason().equals("")) {
                sp_notcomingreason.setSelection(((ArrayAdapter<String>) sp_notcomingreason.getAdapter()).getPosition(serviceReminderDataList.get(position).getNot_coming_reason()));
            }

            if (serviceReminderDataList.get(position).getCustomerreply() != null && !serviceReminderDataList.get(position).getCustomerreply().equals("")) {
                //  customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition(serviceReminderDataList.get(position).getCustomerreply()));
            }*/


        }
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (PSF_Reminder_Activity.this.position == dataposition) {


                    SharedPreferences chkfss = getSharedPreferences(master_ID, Context.MODE_PRIVATE);
                    historyID = chkfss.getString(master_ID, "");


                    if (et_bookingdate.getText().length() > 0 || et_nextfollowupdate.getText().length() > 0 || notcomingreason != null) {

                        if (et_bookingdate.getText().length() > 0) {
                            updatebookingdate(token, historyID, bookingdate_send, string_masterId);
                        }
                        if (et_nextfollowupdate.getText().length() > 0) {
                            updatefollowupdate(token, historyID, nextfollowupdate_send, getString(R.string.SRTAG), string_masterId);
                        }
                        if (notcomingreason != null) {

                            updatenotcomingReason(token, historyID, notcomingreason, string_masterId);
                        }
                        if (et_nextfollowupdate.getText().length() > 0) {
                            updatecustomerreply(token, historyID, "Call Back");
                        }

                        if (contacted_status != null) {
                            if (notcomingreason != null && notcomingreason.equals("Wrong Number")) {
                                updatecontatctstatus(token, historyID, "Wrong Number", getString(R.string.SRTAG));
                            } else {
                                updatecontatctstatus(token, historyID, contacted_status, getString(R.string.SRTAG));
                            }

                        }

                        if (pickanddrop != null) {
                            updatepickanddrop(token, historyID, pickanddrop);
                        }

                        if (remarks.getText().length() > 0) {
                            updateremark(token, historyID, remarks.getText().toString(), getString(R.string.SRTAG), string_masterId);
                        }

                        if(answerlist.size()>0)
                        {
                            for (PSFAnswerModel psfAnswerModel:answerlist)
                            {

                                HashMap<String,String> params = new HashMap<>();
                                params.put("HistoryId",historyID);
                                params.put("Question",psfAnswerModel.getQuestion());
                                params.put("QuestionId",psfAnswerModel.getQuestionId());
                                params.put("Type",psfAnswerModel.getQuestionType());
                                params.put("Answer",psfAnswerModel.getAnswer());

                                apiController.updatePSFRatingQuestionsAnswer(token,params);
                            }
                        }

                        params = new HashMap<>();
                        params.put("HistoryId", historyID);
                        apiController.createServiceRequest(token, params);


                        try {
                            sleep(1000);
                            adapter.notifyDataSetChanged();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        customWaitingDialog.dismiss();
                        shippingDialog.dismiss();
                        Toast.makeText(PSF_Reminder_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                    } else if (remarks.getText().length() > 0) {
                        updateremark(token, historyID, remarks.getText().toString(), getString(R.string.SRTAG), string_masterId);
                        Toast.makeText(PSF_Reminder_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                        customWaitingDialog.dismiss();
                        shippingDialog.dismiss();

                    } else {
                        Toast.makeText(PSF_Reminder_Activity.this, "Please Fill Valid Details..!!", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    if (oldhid != null && !oldhid.isEmpty()) {

                        if (et_bookingdate.getText().length() > 0 || et_nextfollowupdate.getText().length() > 0 || notcomingreason != null) {


                            if (et_bookingdate.getText().length() > 0) {
                                updatebookingdate(token, oldhid, bookingdate_send, master_ID);
                            }
                            if (et_nextfollowupdate.getText().length() > 0) {
                                updatefollowupdate(token, oldhid, nextfollowupdate_send, getString(R.string.SRTAG), master_ID);
                            }
                            if (notcomingreason != null) {
                                updatenotcomingReason(token, oldhid, notcomingreason, master_ID);
                            }
                            if (et_nextfollowupdate.getText().length() > 0) {
                                updatecustomerreply(token, oldhid, "Call Back");
                            }

                            if (contacted_status != null) {
                                updatecontatctstatus(token, oldhid, contacted_status, getString(R.string.SRTAG));
                            }

                            if (pickanddrop != null) {
                                updatepickanddrop(token, oldhid, pickanddrop);
                            }

                            if (remarks.getText().length() > 0) {
                                updateremark(token, oldhid, remarks.getText().toString(), getString(R.string.SRTAG), master_ID);
                            }

                            params = new HashMap<>();
                            params.put("HistoryId", oldhid);
                            apiController.createServiceRequest(token, params);

                            try {
                                sleep(1000);
                                adapter.notifyDataSetChanged();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if(answerlist.size()>0)
                            {
                                for (PSFAnswerModel psfAnswerModel:answerlist)
                                {

                                    HashMap<String,String> params = new HashMap<>();
                                    params.put("HistoryId",historyID);
                                    params.put("Question",psfAnswerModel.getQuestion());
                                    params.put("QuestionId",psfAnswerModel.getQuestionId());
                                    params.put("Type",psfAnswerModel.getQuestionType());
                                    params.put("Answer",psfAnswerModel.getAnswer());

                                    apiController.updatePSFRatingQuestionsAnswer(token,params);
                                }
                            }
                            customWaitingDialog.dismiss();
                            shippingDialog.dismiss();
                            Toast.makeText(PSF_Reminder_Activity.this, "Record Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                        } else if (remarks.getText().length() > 0) {
                            updateremark(token, historyID, remarks.getText().toString(), getString(R.string.SRTAG), string_masterId);
                            Toast.makeText(PSF_Reminder_Activity.this, "Remark Submitted Successfully..!!", Toast.LENGTH_SHORT).show();
                            customWaitingDialog.dismiss();
                            shippingDialog.dismiss();

                        } else {
                            Toast.makeText(PSF_Reminder_Activity.this, "Please Fill Valid Details..!!", Toast.LENGTH_SHORT).show();

                        }


                    }


                }


            }
        });

        sugesstion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion1);
            }
        });

        sugesstion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion2);
            }
        });


        sugesstion3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion3);
            }
        });


        sugesstion4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion4);
            }
        });


        sugesstion5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion5);
            }
        });


        sugesstion6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion6);
            }
        });


        sugesstion7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRemarks(remarks, sugesstion7);
            }
        });


        // Popup to Set Text the Values
        edit_srname.setText(customername);
        edit_srservicetype.setText(servicetype);
        edit_srmodel.setText(model);
        edit_serviceduedate.setText(serviceduefrom);
        edit_pickdrop.setText(pickdropbalance);

        contactArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, contactStatuslist[0]);
        contactstatus.setAdapter(contactArrayAdapter);

        if (calledStatus != null && calledStatus.equals("CALL DONE")) {
            contactstatus.setSelection(0);
        } else {
            contactstatus.setSelection(1);

        }
        contactstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                contacted_status = contactstatus.getSelectedItem().toString();


               /* if(sp_notcomingreason.getSelectedItemPosition()>0 ||et_bookingdate.getText().length()>0|| et_nextfollowupdate.getText().length()>0)
                {
                    sp_notcomingreason.setSelection(2,true);
                }*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                //  contacted_status = "Not Contacted";


            }
        });
        customerArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, customerreplylist[0]);
        customerreply.setAdapter(customerArrayAdapter);
        customerreply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    customer_reply = customerreply.getSelectedItem().toString();

                } else {
                    customer_reply = null;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        notcomingreasonArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, notcomingreasonlist[0]);
        sp_notcomingreason.setAdapter(notcomingreasonArrayAdapter);

        sp_notcomingreason.setOnLongClickListener(
                new View.OnLongClickListener() {
                    public boolean onLongClick(View view) {

                        sp_notcomingreason.setSelection(0);

                        return true;
                    }
                }
        );

        sp_notcomingreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {

                    if (et_nextfollowupdate.getText().length() == 0 && et_bookingdate.getText().length() == 0 && calldone == true) {

                        if (position > 0) {
                            notcomingreason = sp_notcomingreason.getSelectedItem().toString();
                            if (notcomingreason.equals("Wrong Number")) {
                                if (contactstatus != null) contactstatus.setSelection(2);
                            }
                        } else {
                            notcomingreason = null;
                        }


                        // contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("Contacted"));

                    } else {


                        if (sp_notcomingreason.getSelectedItemPosition() > 0 && et_nextfollowupdate.getText().length() > 0) {


                            sp_notcomingreason.setSelection(0, true);
                            Toasty.warning(PSF_Reminder_Activity.this, "First Clear Next Followup Date", Toast.LENGTH_SHORT).show();


                        }

                        if (sp_notcomingreason.getSelectedItemPosition() > 0 && et_bookingdate.getText().length() > 0) {

                            sp_notcomingreason.setSelection(0, true);
                            Toasty.warning(PSF_Reminder_Activity.this, "First clear Booking Date", Toast.LENGTH_SHORT).show();


                        }

                        if (!calldone) {
                            sp_notcomingreason.setSelection(0, true);
                            Toasty.warning(PSF_Reminder_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                            return;


                        }


                    }


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                notcomingreason = null;
            }
        });

        sp_pickanddrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (et_bookingdate.getText().length() > 0) {
                    sp_pickanddrop.setEnabled(true);
                    if (position != 0) {
                        pickanddrop = sp_pickanddrop.getSelectedItem().toString();

                    } else {
                        pickanddrop = null;
                    }
                }
//                else
//                {
//                    Toasty.warning(S_R_Activity.this, "First Select  Booking  Date", Toast.LENGTH_SHORT).show();
//                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*-------------------------------------------------*/


        iv_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (singledatalist.size() > 0) {
                    if (singledatalist.get(0).getBookingdate().length() > 0) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(PSF_Reminder_Activity.this);
                        builder.setMessage("Do you want cancel booking ?");
                        builder.setNeutralButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                params = new HashMap<>();
                                params.put("HistoryId", singledatalist.get(0).getMasterpsfid());
                                apiController.CancelServiceRequest(token, params);
                                updatebookingdate(token, singledatalist.get(0).getMasterpsfid(), "", master_ID);
                                sp_pickanddrop.setEnabled(false);
                                et_bookingdate.setText("");
                                edit_Bookkingno.setText("");
                                et_bookingdate.setEnabled(true);
                                shippingDialog.dismiss();


                            }
                        });
                        builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                            }
                        });

                        builder.show();


                    } else {
                        et_bookingdate.setText("");
                        sp_pickanddrop.setEnabled(false);

                    }
                }


            }
        });
        iv_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nextfollowupdate.setText("");
            }
        });
        ib_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < serviceReminderDataList.size())
                    getSRHistoryData(token, serviceReminderDataList.get(position).getCustomercontact());


                final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.historydata_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);

                TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
                popupclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog.dismiss();
                    }
                });
                history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
                history_data_recyclerview.setHasFixedSize(true);
                history_data_recyclerview.setLayoutManager(new LinearLayoutManager(PSF_Reminder_Activity.this, RecyclerView.VERTICAL, false));
                adapter_sr_historydata = new Adapter_SR_Historydata(PSF_Reminder_Activity.this, serviceReminderHistoryDataList);
                history_data_recyclerview.setAdapter(adapter_sr_historydata);
                adapter_sr_historydata.notifyDataSetChanged();


                shippingDialog.show();

            }
        });
        et_bookingdate.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  if (!calldone) {
                                                      Toasty.warning(PSF_Reminder_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                                                      return;


                                                  }


                                                  if (singledatalist.size() == 0)
                                                      getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);


                                                  if (singledatalist.size() > 0 && !singledatalist.get(0).getBookingdate().replaceAll("1900-01-01T00:00:00", "").equals("")) {
                                                      Toasty.warning(PSF_Reminder_Activity.this, "Booking Already Done Cannot Be Changed..!!", Toast.LENGTH_SHORT).show();
                                                  } else {

                                                      if (sp_notcomingreason.getSelectedItemPosition() == 0 && et_nextfollowupdate.getText().toString().length() == 0) {
                                                          if (singledatalist.size() > 0 && singledatalist.get(0).getBookingdate().isEmpty()) {

                                                              dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                                                              if (dateTimeFragment == null) {
                                                                  dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                                                          getString(R.string.label_datetime_dialog),
                                                                          getString(android.R.string.ok),
                                                                          getString(android.R.string.cancel),
                                                                          getString(R.string.clean) // Optional
                                                                  );
                                                              }

                                                              // Optionally define a timezone
                                                              dateTimeFragment.setTimeZone(TimeZone.getDefault());

                                                              // Init format


                                                              final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault());
                                                              final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());


                                                              // Assign unmodifiable values
                                                              dateTimeFragment.set24HoursMode(true);
                                                              dateTimeFragment.setHighlightAMPMSelection(false);


                                                              // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                                                              // Define new day and month format
                                                              try {
                                                                  dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                                                              } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                                                                  e.printStackTrace();
                                                              }

                                                              // Set listener for date
                                                              // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                                                              dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                                                                  @Override
                                                                  public void onPositiveButtonClick(Date date) {
                                                                      et_bookingdate.setText(myDateFormat.format(date));
                                                                      sp_pickanddrop.setEnabled(true);
                                                                      bookingdate_send = myDateFormat1.format(date);


                                                                      if (et_bookingdate.getText().length() != 0) {
                                                                          contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("Contacted"));
                                                                      }

                                                                  }

                                                                  @Override
                                                                  public void onNegativeButtonClick(Date date) {
                                                                      // Do nothing
                                                                  }

                                                                  @Override
                                                                  public void onNeutralButtonClick(Date date) {
                                                                      // Optional if neutral button does'nt exists
                                                                      et_bookingdate.setText("");
                                                                  }
                                                              });
                                                              dateTimeFragment.startAtCalendarView();
                                                              Calendar cal = Calendar.getInstance();
                                                              cal.setTime(new Date());
                                                              cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                                                              dateTimeFragment.setMinimumDateTime(cal.getTime());
                                                              cal.setTime(new Date());
                                                              cal.add(Calendar.MONTH, 1);
                                                              dateTimeFragment.setMaximumDateTime(cal.getTime());
                                                              dateTimeFragment.setDefaultDateTime(new Date());
                                                              // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());

                                                              if (dateTimeFragment.isAdded()) {
                                                                  return;
                                                              } else {
                                                                  dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                                                              }
                                                          } else if (!et_bookingdate.getText().toString().isEmpty()) {
                                                              Toasty.warning(PSF_Reminder_Activity.this, "Booking already done", Toast.LENGTH_SHORT).show();

                                                          } else {

                                                              Toasty.warning(PSF_Reminder_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();

                                                          }

                                                      } else if (et_nextfollowupdate.getText().toString().length() > 0) {

                                                          Toasty.warning(PSF_Reminder_Activity.this, "First Clear NextFollowup Date", Toast.LENGTH_SHORT).show();
                                                      } else {
                                                          Toasty.warning(PSF_Reminder_Activity.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                                                      }
                                                  }


                                              }


                                          }
        );
        et_nextfollowupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!calldone) {
                    Toasty.warning(PSF_Reminder_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                    return;


                }


                if (singledatalist.size() == 0)
                    getSingleCustomerSRHistoryData(token, getString(R.string.SRTAG), master_ID);


                if (singledatalist.size() == 0) {
                    Toasty.warning(PSF_Reminder_Activity.this, "Wait for call to be connected", Toast.LENGTH_SHORT).show();
                } else {
                    if (et_bookingdate.getText().length() == 0 && sp_notcomingreason.getSelectedItemPosition() == 0) {
                        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
                        if (dateTimeFragment == null) {
                            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                                    getString(R.string.label_datetime_dialog),
                                    getString(android.R.string.ok),
                                    getString(android.R.string.cancel),
                                    getString(R.string.clean) // Optional
                            );
                        }

                        // Optionally define a timezone
                        dateTimeFragment.setTimeZone(TimeZone.getDefault());

                        // Init format


                        final SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault());
                        final SimpleDateFormat myDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

                        // Assign unmodifiable values
                        dateTimeFragment.set24HoursMode(true);
                        dateTimeFragment.setHighlightAMPMSelection(false);


                        // dateTimeFragment.setMinimumDateTime(new GregorianCalendar(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH).getTime());


                        // Define new day and month format
                        try {
                            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
                        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
                            e.printStackTrace();
                        }

                        // Set listener for date
                        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
                            @Override
                            public void onPositiveButtonClick(Date date) {
                                et_nextfollowupdate.setText(myDateFormat.format(date));
                                nextfollowupdate_send = myDateFormat1.format(date);
                                if (et_nextfollowupdate.getText().length() != 0) {
                                    customerreply.setSelection(((ArrayAdapter<String>) customerreply.getAdapter()).getPosition("Call Back"));
                                }
                                if (et_nextfollowupdate.getText().length() != 0) {
                                    contactstatus.setSelection(((ArrayAdapter<String>) contactstatus.getAdapter()).getPosition("Contacted"));
                                }
                            }

                            @Override
                            public void onNegativeButtonClick(Date date) {
                                // Do nothing
                            }

                            @Override
                            public void onNeutralButtonClick(Date date) {
                                // Optional if neutral button does'nt exists
                                et_nextfollowupdate.setText("");
                            }
                        });
                        dateTimeFragment.startAtCalendarView();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                        dateTimeFragment.setMinimumDateTime(cal.getTime());
                        cal.setTime(new Date());
                        cal.add(Calendar.MONTH, 1);
                        dateTimeFragment.setMaximumDateTime(cal.getTime());
                        dateTimeFragment.setDefaultDateTime(new Date());
                        // dateTimeFragment.setDefaultDateTime(new GregorianCalendar(2017, Calendar.MARCH, 4, 15, 20).getTime());
                        if (dateTimeFragment.isAdded()) {
                            return;
                        } else {
                            dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
                        }

                    } else if (et_bookingdate.getText().toString().length() > 0) {

                        Toasty.warning(PSF_Reminder_Activity.this, "First Clear Booking Date", Toast.LENGTH_SHORT).show();
                    } else {
                        Toasty.warning(PSF_Reminder_Activity.this, "First Clear Not Coming Reason", Toast.LENGTH_SHORT).show();

                    }
                }


            }


        });
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
                customWaitingDialog.dismiss();
            }
        });

        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);

        //shippingDialog.show();

        if (shippingDialog != null && (shippingDialog.isShowing())) {
            shippingDialog.dismiss();

        }


        shippingDialog.show();


    }

    // History  Details Show Fuction( This Function is Used on click listener of history icon)
    public void getHistoryDetails(int position) {


        getSRHistoryData(token, serviceReminderDataList.get(position).getCustomercontact());
        final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.historydata_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        history_data_recyclerview = shippingDialog.findViewById(R.id.hisrecycler);
        history_data_recyclerview.setHasFixedSize(true);
        history_data_recyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter_sr_historydata = new Adapter_SR_Historydata(this, serviceReminderHistoryDataList);
        history_data_recyclerview.setAdapter(adapter_sr_historydata);
        adapter_sr_historydata.notifyDataSetChanged();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });


        shippingDialog.show();

    }

    // More Details Fuction( This Function is Used on click listener of more details icon)
    public void getMoreDetails(int position) {
        final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.psf_more_datapopup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(false);
        shippingDialog.show();
        TextView popupclose = shippingDialog.findViewById(R.id.txtclose);
        popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shippingDialog.dismiss();
            }
        });
        EditText edit_number = shippingDialog.findViewById(R.id.mobileno);
        EditText last_service_date = shippingDialog.findViewById(R.id.last_service_date);
        EditText last_service_type = shippingDialog.findViewById(R.id.last_service_type);
        EditText last_jc_km = shippingDialog.findViewById(R.id.last_jc_km);
        EditText last_invoice_value = shippingDialog.findViewById(R.id.last_invoice_value);
        EditText open_complaint_number = shippingDialog.findViewById(R.id.open_complaint_number);
        EditText open_sr_number = shippingDialog.findViewById(R.id.open_sr_number);
        EditText fsc1_date = shippingDialog.findViewById(R.id.fsc1_date);
        EditText fsc2_date = shippingDialog.findViewById(R.id.fsc2_date);
        EditText fsc3_date = shippingDialog.findViewById(R.id.fsc3_date);
        EditText fsc4_date = shippingDialog.findViewById(R.id.fsc4_date);
        EditText fsc5_date = shippingDialog.findViewById(R.id.fsc5_date);
        EditText paid1_date = shippingDialog.findViewById(R.id.paid1_date);
        EditText creatat = shippingDialog.findViewById(R.id.createdat);
        EditText vin = shippingDialog.findViewById(R.id.vin);
        EditText color = shippingDialog.findViewById(R.id.color);
        EditText hsrp_stage = shippingDialog.findViewById(R.id.hsrp_stage);
        EditText warranty_start_date = shippingDialog.findViewById(R.id.warranty_start_date);
        EditText service_update = shippingDialog.findViewById(R.id.service_update);
        EditText mandatory_update = shippingDialog.findViewById(R.id.mandatory_update);
        EditText herosure = shippingDialog.findViewById(R.id.herosure);
        EditText avg_km_run = shippingDialog.findViewById(R.id.avg_km_run);
        EditText digital_customer = shippingDialog.findViewById(R.id.digitalCustomer);
        EditText organization = shippingDialog.findViewById(R.id.organization);
        EditText customer_rating = shippingDialog.findViewById(R.id.customer_rating);
        EditText address = shippingDialog.findViewById(R.id.address);
        EditText multiple_nameandmobile = shippingDialog.findViewById(R.id.name_and_mobileno_multiple);
        EditText next_service_due_date = shippingDialog.findViewById(R.id.next_service_due_date);
        EditText reminder_followup_date = shippingDialog.findViewById(R.id.reminder_follow_update);
        EditText insurance_expiry_date = shippingDialog.findViewById(R.id.insurance_expiry_date);
        EditText gl_expiry = shippingDialog.findViewById(R.id.gl_expiry);
        EditText gl_points = shippingDialog.findViewById(R.id.gl_points);
        EditText ls_expiry = shippingDialog.findViewById(R.id.ls_expiry);
        EditText ls_balance = shippingDialog.findViewById(R.id.ls_balance);
        EditText joyride_expiry = shippingDialog.findViewById(R.id.joyride_expiry);
        EditText joyride_balance = shippingDialog.findViewById(R.id.joyride_balance);

//        if (serviceReminderDataList.get(position).getCreatedat() != null && !serviceReminderDataList.get(position).getCreatedat().toString().isEmpty())
//            creatat.setText("" + dateparse3(serviceReminderDataList.get(position).getCreatedat().toString().replace("T", " ").substring(0, 10)));
        if (serviceReminderDataList.get(position).getVin() != null)
            vin.setText("" + serviceReminderDataList.get(position).getVin());
//        if (serviceReminderDataList.get(position).getC() != null)
//            color.setText("" + serviceReminderDataList.get(position).getColor().toUpperCase());
//        if (serviceReminderDataList.get(position).getHsrp_stage() != null)
//            hsrp_stage.setText("" + serviceReminderDataList.get(position).getHsrp_stage().toUpperCase());
        if (serviceReminderDataList.get(position).getWarrantystartdate() != null && !serviceReminderDataList.get(position).getWarrantystartdate().toString().isEmpty())
            warranty_start_date.setText("" + dateparse3(serviceReminderDataList.get(position).getWarrantystartdate().replace("T", " ").substring(0, 10)));
        if (serviceReminderDataList.get(position).getServiceupdate() != null)
            service_update.setText("" + serviceReminderDataList.get(position).getServiceupdate().toUpperCase());
        if (serviceReminderDataList.get(position).getMandatoryupdate() != null)
            mandatory_update.setText("" + serviceReminderDataList.get(position).getMandatoryupdate().toUpperCase());
//        if (serviceReminderDataList.get(position).getHerosure() != null)
//            herosure.setText("" + serviceReminderDataList.get(position).getHerosure().toUpperCase());
        if (serviceReminderDataList.get(position).getAvgkmrun() != null)
            avg_km_run.setText("" + serviceReminderDataList.get(position).getAvgkmrun());
//        if (serviceReminderDataList.get(position).getDigitalcustomer() != null)
//            digital_customer.setText("" + serviceReminderDataList.get(position).getDigitalcustomer().toUpperCase());
        if (serviceReminderDataList.get(position).getCustomerrating() != null)
            customer_rating.setText("" + serviceReminderDataList.get(position).getCustomerrating());
        if (serviceReminderDataList.get(position).getAddress() != null)
            address.setText("" + serviceReminderDataList.get(position).getAddress().toUpperCase());
//        if (serviceReminderDataList.get(position).getName_and_mobileno_multiple() != null)
//            multiple_nameandmobile.setText("" + serviceReminderDataList.get(position).getName_and_mobileno_multiple().toUpperCase());
//        if (serviceReminderDataList.get(position).getNext_service_due_date() != null && !serviceReminderDataList.get(position).getNext_service_due_date().toString().isEmpty())
//            next_service_due_date.setText("" + dateparse3(serviceReminderDataList.get(position).getNext_service_due_date().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getReminder_followup_date() != null && !serviceReminderDataList.get(position).getReminder_followup_date().toString().isEmpty())
//            reminder_followup_date.setText("" + dateparse3(serviceReminderDataList.get(position).getReminder_followup_date().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getInsurance_expiry_date() != null && !serviceReminderDataList.get(position).getInsurance_expiry_date().toString().isEmpty())
//            insurance_expiry_date.setText("" + dateparse3(serviceReminderDataList.get(position).getInsurance_expiry_date().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getGl_expiry() != null && !serviceReminderDataList.get(position).getGl_expiry().toString().isEmpty())
//            gl_expiry.setText("" + dateparse3(serviceReminderDataList.get(position).getGl_expiry().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getGl_points() != null)
//            gl_points.setText("" + serviceReminderDataList.get(position).getGl_points().toUpperCase());
//        if (serviceReminderDataList.get(position).getLs_expiry() != null && !serviceReminderDataList.get(position).getLs_expiry().toString().isEmpty())
//            ls_expiry.setText("" + dateparse3(serviceReminderDataList.get(position).getLs_expiry().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getLs_balance() != null)
//            ls_balance.setText("" + serviceReminderDataList.get(position).getLs_balance().toUpperCase());
//        if (serviceReminderDataList.get(position).getJoyride_expiry() != null && !serviceReminderDataList.get(position).getJoyride_expiry().toString().isEmpty())
//            joyride_expiry.setText("" + dateparse3(serviceReminderDataList.get(position).getJoyride_expiry().toUpperCase().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getJoyride_balance() != null)
//            joyride_balance.setText("" + dateparse3(serviceReminderDataList.get(position).getJoyride_balance().toUpperCase()));
        if (serviceReminderDataList.get(position).getDate() != null && !serviceReminderDataList.get(position).getDate().toString().isEmpty()) {
          //  last_service_date.setText(dateparse3(serviceReminderDataList.get(position).getDate()));
        }
//        if (serviceReminderDataList.get(position).getType() != null && !serviceReminderDataList.get(position).getType().toString().isEmpty())
//            last_service_type.setText(serviceReminderDataList.get(position).getType());
//        last_jc_km.setText(serviceReminderDataList.get(position).getType().toUpperCase());
//        last_invoice_value.setText(serviceReminderDataList.get(position).getLast_invoice_value().toUpperCase());
//        open_complaint_number.setText(serviceReminderDataList.get(position).getOpen_complaint_number().toUpperCase());
//        open_sr_number.setText(serviceReminderDataList.get(position).getOpen_sr_number().toUpperCase());
//        if (serviceReminderDataList.get(position).getFsc1_date() != null && !serviceReminderDataList.get(position).getFsc1_date().toString().isEmpty())
//
//            fsc1_date.setText(dateparse3(serviceReminderDataList.get(position).getFsc1_date().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getFsc2_date() != null && !serviceReminderDataList.get(position).getFsc2_date().toString().isEmpty())
//
//            fsc2_date.setText(dateparse3(serviceReminderDataList.get(position).getFsc2_date().replace("T", " ").substring(0, 10)));
//        if (serviceReminderDataList.get(position).getFsc3_date() != null && !serviceReminderDataList.get(position).getFsc3_date().toString().isEmpty())
//
//            fsc3_date.setText(dateparse3(serviceReminderDataList.get(position).getFsc3_date().replace("T", " ").substring(0, 10)));
//
//        if (serviceReminderDataList.get(position).getFsc4_date() != null && !serviceReminderDataList.get(position).getFsc4_date().toString().isEmpty())
//            fsc4_date.setText(dateparse3(serviceReminderDataList.get(position).getFsc4_date().replace("T", " ").substring(0, 10)));
//
//        if (serviceReminderDataList.get(position).getFsc5_date() != null && !serviceReminderDataList.get(position).getFsc5_date().toString().isEmpty())
//            fsc5_date.setText(dateparse3(serviceReminderDataList.get(position).getFsc5_date().replace("T", " ").substring(0, 10)));
//
//        if (serviceReminderDataList.get(position).getPaid1_date() != null && !serviceReminderDataList.get(position).getPaid1_date().toString().isEmpty())
//            paid1_date.setText(dateparse3(serviceReminderDataList.get(position).getPaid1_date().replace("T", " ").substring(0, 10)));
//        organization.setText(serviceReminderDataList.get(position).getOrganization());

        edit_number.setText(serviceReminderDataList.get(position).getCustomercontact().toUpperCase());
        edit_number.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.mobile_number_change_popup);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(true);
                shippingDialog.setCanceledOnTouchOutside(false);
                EditText et_newnumber =(EditText) shippingDialog.findViewById(R.id.setupnewEdit);
                et_newnumber.setText(""+serviceReminderDataList.get(position).getCustomercontact());
                Button done_btn =(Button)shippingDialog.findViewById(R.id.setupnewDone);
                Button cancel_btn =(Button)shippingDialog.findViewById(R.id.setupnewCancle);
                done_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(et_newnumber.getText().length()==10)
                        {
                            if(!serviceReminderDataList.get(position).getCustomercontact().equals(et_newnumber.getText().toString()))
                            {

                                AlertDialog.Builder builder = new AlertDialog.Builder(PSF_Reminder_Activity.this);
                                builder.setMessage("Are You Sure Set this Number is Primary ?");
                                builder.setCancelable(false);
                                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        shippingDialog.dismiss();
                                        dialog.cancel();
                                        UpdateContatctNumber(token,serviceReminderDataList.get(position).getId(),historyID,et_newnumber.getText().toString(),
                                                "Primary");
                                    }
                                });
                                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        shippingDialog.dismiss();
                                        UpdateContatctNumber(token,serviceReminderDataList.get(position).getId(),historyID,et_newnumber.getText().toString(),
                                                "Alternate");

                                    }
                                });


                                AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                            }else
                            {
                                Toast.makeText(PSF_Reminder_Activity.this, " number already exist", Toast.LENGTH_SHORT).show();
                            }

                        }else
                        {
                            Toast.makeText(PSF_Reminder_Activity.this, "Please check number", Toast.LENGTH_SHORT).show();

                        }
                    }
                });



                shippingDialog.show();
                TextView title = (TextView) shippingDialog.findViewById(R.id.setupnewtitle);
                title.setText("Do You Want to Change Customer Mobile Number ?");




                return false;
            }
        });


    }

    @Override
    public void holdCall(int position, String contatcnumber) {


        OngoingCall.call.hold();

    }

    @Override
    public void unHoldCall(int position, String contatcnumber) {

        OngoingCall.call.unhold();


    }

    // Send WhatsAPP message to this function
    public void sendWhatsappMessage(int position, String number) {

        whatsappclicked = true;
        whatsapp_number = number;
        //  Toast.makeText(this, number, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(PSF_Reminder_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, PSF_Reminder_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(PSF_Reminder_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, getString(R.string.SRTAG));


    }

    // Send Text Message to this Function
    public void sendtextmessage(int position, String contactnumber) {


        textmesasageclicked = true;
        whatsapp_number = contactnumber;
        //   Toast.makeText(this, contactnumber, Toast.LENGTH_SHORT).show();
        templatelist.clear();
        Whatsapp_shippingDialog = new Dialog(PSF_Reminder_Activity.this);
        Whatsapp_shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Whatsapp_shippingDialog.setContentView(R.layout.template_list_popup);
        Whatsapp_shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        Whatsapp_shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Whatsapp_shippingDialog.setCancelable(true);
        Whatsapp_shippingDialog.setCanceledOnTouchOutside(false);
        ImageView close_btn = Whatsapp_shippingDialog.findViewById(R.id.iv_btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Whatsapp_shippingDialog.dismiss();
            }
        });
        Whatsapp_shippingDialog.show();
        recyclerView_template = Whatsapp_shippingDialog.findViewById(R.id.templatelistshow);
        adapterSettingTemplet = new AdapterTemplateShow(this, PSF_Reminder_Activity.this, templatelist);
        adapterSettingTemplet.setCustomButtonListner(PSF_Reminder_Activity.this);
        recyclerView_template.setHasFixedSize(true);
        recyclerView_template.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView_template.setAdapter(adapterSettingTemplet);


        getsmstemplate(token, getString(R.string.SRTAG));


    }

    //Get SMS Template
    public void getsmstemplate(String token, String templatetype) {
        templatelist.clear();
        customWaitingDialog.show();
        params = new HashMap<>();
        params.put("TemplateType", templatetype);
        apiController.GetSMSTemplate(token, params);

    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    @Override
    public void sendWhatsappMessagetemplate(int position, String templatebody) {

        if (whatsappclicked == true) {

            try {
                Intent sendMsg = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + "+91 " + whatsapp_number + "&text=" + URLEncoder.encode("" + templatebody, "UTF-8");
                sendMsg.setPackage("com.whatsapp");
                sendMsg.setData(Uri.parse(url));
                if (sendMsg.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                    startActivity(sendMsg);
                    PSF_Reminder_Activity.whatsupclick = true;
                    whatsappclicked = false;
                    Whatsapp_shippingDialog.dismiss();

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), " Whatsapp not Installed", Toast.LENGTH_SHORT).show();

            }
        } else if (textmesasageclicked == true) {

            Uri uri = Uri.parse("smsto:" + whatsapp_number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", templatebody);
            startActivity(intent);
            PSF_Reminder_Activity.textmessageclick = true;
            textmesasageclicked = false;
            Whatsapp_shippingDialog.dismiss();

        }


    }

    /*------------------------------------------------Call Recording and Upload Code---------------------------------------------------------*/
    //Recording Start Code
    public void startRecording(String number) throws IOException {
        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM/"));
        if (!dir.exists()) dir.mkdirs();
        try {
            audiofile = File.createTempFile(number, ".mp3", dir);
            recorder = new MediaRecorder();
            am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_IN_CALL);
            am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setAudioEncodingBitRate(1411200);
            recorder.setAudioChannels(1);
            recorder.setAudioSamplingRate(88200);

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            sleep(1000);
                            am.setMode(AudioManager.MODE_IN_CALL);
//                            if (!am.isSpeakerphoneOn())
//                                am.setSpeakerphoneOn(true);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!am.isWiredHeadsetOn()) {
                thread.start();
            }
            recorder.setOutputFile(audiofile.getAbsolutePath());
            try {
                recorder.prepare();
                recorder.start();
            } catch (IOException e) {
                e.printStackTrace();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Stop  Recording Function
    public void stopRecording(String id) {


//        try {
//            if (am != null && recorder != null) {
//                am.setMode(AudioManager.MODE_NORMAL);
//                am.setStreamVolume(AudioManager.MODE_IN_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        convertRecording(CommonVariables.audiofile);
        //after stopping the recorder, create the sound file and add it to media library.

        CommonVariables.wavRecorder.stopRecording();

//        try {
//            if (am != null && recorder != null) {
//                am.setMode(AudioManager.MODE_NORMAL);
//                am.setStreamVolume(AudioManager.MODE_IN_CALL, am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRMs/"));
//        if (!dir.exists()) dir.mkdirs();
//        try {
//           CommonVariables.audiofilewave = File.createTempFile(CommonVariables.mobilenumber, ".wav", dir);
//
        // rawToWave(CommonVariables.audiofile,CommonVariables.audiofilewave);
//
//
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }



        // convertRecording(CommonVariables.audiofile);
        //after stopping the recorder, create the sound file and add it to media library.


        if (CommonVariables.audiofile != null) UploadCallRecording(token, CommonVariables.dealercode, historyID, "PSF");



    }

    //Updaload Call Recording
    public void UploadCallRecording(String token, String dealercode, String historyid, String calltype) {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();

        values.put(MediaStore.Audio.Media.TITLE, "" + CommonVariables.audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/wav");
        values.put(MediaStore.Audio.Media.DATA, CommonVariables.audiofile.getAbsolutePath());
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        if (newUri != null) {
            selectedPath = getPath(newUri);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
            File files = new File(selectedPath);

            Log.e("jayantfilesize", " "+CommonVariables.audiofile.length());
            RequestBody Dealer_Code = RequestBody.create(MediaType.parse("multipart/from-data"), dealercode);
            RequestBody History_id = RequestBody.create(MediaType.parse("multipart/from-data"), historyid);
            RequestBody Content_type = RequestBody.create(MediaType.parse("multipart/from-data"), "audio/wav");
            RequestBody Recording = RequestBody.create(MediaType.parse("audio/wav"), files);
            RequestBody Call_type = RequestBody.create(MediaType.parse("multipart/from-data"), calltype);
            MultipartBody.Part Recording_file = MultipartBody.Part.createFormData("Recording", files.getName(), Recording);

            Log.e("jayantfilesize", " "+CommonVariables.audiofile.length());
            Log.e("jayantfilesize", "2 "+files.length());

            apiController.UploadCallRecording(token, Dealer_Code, History_id, Content_type, Recording_file, Call_type);
            CommonVariables.audiofile= null;
        }


    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    /*------------------------------------------------------------------------------------------------------------------------*/
    // Get Recording File Path

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void clearcolorsharedeprefances() {
        if (arrayList_matserId.size() > 0) {
            for (int k = 0; k < arrayList_matserId.size(); k++) {
                SharedPreferences sharedPreferencesk = getSharedPreferences(arrayList_matserId.get(k), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferencesk.edit();
                editor.clear();
                editor.commit();


            }
        }
    }

    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        NewDateFormat = null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }


    /*------------------------------------------------------------------------------------------------------------------------*/

    public void disconnectCall() {
        try {

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void changedailer2() {
        RoleManager roleManager = (RoleManager) getSystemService(ROLE_SERVICE);
        Intent intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER);
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            //  Toast.makeText(this, ""+new Gson().toJson(data), Toast.LENGTH_SHORT).show();
        }

    }

    public static void clearApplicationData(Context context) {
        File cache = context.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                File f = new File(appDir, s);
                if (deleteDir(f)) {
                    // Log.i("filedelete", String.format("**************** DELETED -> (%s) *******************", f.getAbsolutePath()));
                }
            }
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public String dateparse1(String inputdate) {

        if (!inputdate.isEmpty()) {

            // Oct 31 2020  2:16PM


            //2020-12-14 12:51:00.0000000
            String inputPattern = "MMM dd yyyy HH:mm:ss";
            String outputPattern = "dd-MMM-yyyy HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            try {
                date = inputFormat.parse(inputdate);
                NewDateFormat = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return inputdate;
        } else {
            return inputdate;
        }
    }

    public String dateparse3(String inputdate) {

        if (!inputdate.isEmpty()) {


            // Oct 31 2020  2:16PM
            String inputPattern = "yyyy-MM-dd";
            String outputPattern = "dd-MMM-yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            String newdate = "";
            try {
                date = inputFormat.parse(inputdate);
                newdate = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return newdate;
        } else {
            return inputdate;
        }
    }

    public String dateparse2(String inputdate) {

        if (!inputdate.isEmpty()) {

            // Oct 31 2020  2:16PM


            //2020-12-14 12:51:00.0000000
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd-MMM-yyyy HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date date = null;
            NewDateFormat = null;
            try {
                date = inputFormat.parse(inputdate);
                NewDateFormat = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return NewDateFormat;
        } else {
            return NewDateFormat;
        }
    }

    public void DisableCallWaiting() {

        String ussd = Uri.encode("#") + "43" + Uri.encode("#");
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));

    }

    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {


            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);
            currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            if (currentNetworkInfo.isConnected()) {

                // finish();
                // startActivity(getIntent());
                Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();

            } else {
                final Dialog shippingDialog = new Dialog(PSF_Reminder_Activity.this);
                shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog.setContentView(R.layout.layout_no_internet);
                shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog.setCancelable(false);
                ImageView iv_btn_close = shippingDialog.findViewById(R.id.iv_btn_close);
                iv_btn_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shippingDialog.dismiss();
                    }
                });

                if (shippingDialog != null) {
                    shippingDialog.show();

                }


            }
        }
    };



    void convertRecording(File recording)
    {
        IConvertCallback callback = new IConvertCallback() {
            @Override
            public void onSuccess(File convertedFile) {
                // So fast? Love it!


                CommonVariables.audiofile = convertedFile ;

                if (CommonVariables.audiofile != null) UploadCallRecording(token, "", historyID, getString(R.string.SRTAG));

            }
            @Override
            public void onFailure(Exception error) {
                // Oops! Something went wrong
            }
        };

        AndroidAudioConverter.with(this)
                // Your current audio file
                .setFile(recording)

                // Your desired audio format
                .setFormat(AudioFormat.MP3)

                // An callback to know when conversion is finished
                .setCallback(callback)

                // Start conversion
                .convert();
    }


    @Override
    public void getQuestionAnswerClick(String Question, String QuestionId, String Type, String Answer) {


        answerlist.add(new PSFAnswerModel(Question,QuestionId,Type,Answer));


    }

    @Override
    public void getQuestionAnswerClickRemove(String Question, String QuestionId, String Type, String Answer) {

        if(answerlist.size()==0)
        {
        }else
        {
            for (PSFAnswerModel psfAnswerModel:answerlist)
            {
                if(psfAnswerModel.getQuestionId().equals(QuestionId))
                {
                    answerlist.remove(psfAnswerModel);
                }
            }
        }

    }


}



