package com.hero.weconnect.mysmartcrm.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.CampaignModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.ReportModel;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.TokenModel;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class CustomCallingReportActivity extends AppCompatActivity implements ApiResponseListener {
    CardView todate, fromdate;
    EditText et_todate, et_fromdate;
    public static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    TextView totalavilablecalls, totalcalls, calldone, callnotdone, folowup, bookings, statyus;
    String dateto, datefrom, token, callType;
    ApiController apiController;
    CommonSharedPref commonSharedPref;
    Date date1;
    public ArrayAdapter<String> userArrayAdapter;
    ArrayList<String>[] userArrayList;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    Spinner spinner_reminders, spinner_campaingName;

    //**************************<Campaign Name List> ****************************************
    private HashMap<String, CampaignModel.DataDTO> campaignNameHashMap;
    public ArrayList<String>[] campaign_arraylist = new ArrayList[]{new ArrayList<String>()};
    public ArrayList<String> campaign_arraylist_ID = new ArrayList();
    //**************************</Campaign Name List> ****************************************

    public ArrayList<Custom_Model.DataBean> serviceReminderDataList = new ArrayList<>();

    CustomDialog customDialog;
    Calendar calendar;
    String myFormat = "yyyy-MM-dd"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    Calendar myCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_activity);
        init();

    }

    void init() {
        apiController = new ApiController(this);
        fromdate = (CardView) findViewById(R.id.fromdate);
        todate = (CardView) findViewById(R.id.todate);
        spinner_reminders = (Spinner) findViewById(R.id.spinner_reminders);
        spinner_campaingName = (Spinner) findViewById(R.id.spinner_campaingn);
        et_fromdate = (EditText) findViewById(R.id.fromtxtdate);
        et_todate = (EditText) findViewById(R.id.totxtdate);
        totalavilablecalls = (TextView) findViewById(R.id.totalavilablecalls);
        totalcalls = (TextView) findViewById(R.id.totalcalls);
        calldone = (TextView) findViewById(R.id.calldone);
        callnotdone = (TextView) findViewById(R.id.callnotdone);
        folowup = (TextView) findViewById(R.id.folowup);
        bookings = (TextView) findViewById(R.id.bookings);
        statyus = (TextView) findViewById(R.id.callprogress);
        commonSharedPref = new CommonSharedPref(this);
        customDialog = new CustomDialog(this);
        userArrayList = new ArrayList[]{new ArrayList<String>()};
        calendar = Calendar.getInstance();
        //Log.e("Date1111", "  " + calendar.getTime());

        myCalendar.add(Calendar.DATE, -1);
        et_fromdate.setText(sdf.format(myCalendar.getTime()));
        et_todate.setText(sdf.format(myCalendar.getTime()));
        datefrom = sdf.format(myCalendar.getTime());
        dateto = sdf.format(myCalendar.getTime());
        userArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, userArrayList[0]);
        apiController.getToken(""+CommonVariables.userName,""+CommonVariables.userName,"password");

        Toolbar toolbar = findViewById(R.id.dashtoolbar);
        toolbar.setTitle("Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        et_fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);



                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(CustomCallingReportActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                int ye = year;
                                int day = dayOfMonth;
                                int ss = month + 1;
                                int mon = ss;


                                et_fromdate.setText(ss + "-" + day + "-" + ye);
                                datefrom = ye + "-" + mon + "-" + day;
                                if (spinner_campaingName.getSelectedItem() != null && !dateto.isEmpty()) {
                                    getReportingdata(CommonVariables.customtokenbearer, datefrom, dateto, spinner_campaingName.getSelectedItem().toString());
                                }
                                // totalCustomerData();

                            }


                        }, mYear, mMonth, mDay);
                dpd.getDatePicker().setMaxDate(System.currentTimeMillis()-(1000*60*60*24));
                dpd.show();
            }
        });

        et_todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(CustomCallingReportActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                int ye = year;
                                int day = dayOfMonth;
                                int ss = month + 1;
                                int mon = ss;
                                et_todate.setText(ss + "-" + day + "-" + ye);
                                dateto = ye + "-" + mon + "-" + day;
                                if (spinner_campaingName.getSelectedItem() != null && !datefrom.isEmpty()) {
                                    getReportingdata(token, datefrom, dateto, spinner_campaingName.getSelectedItem().toString());
                                }
                            }
                        }, mYear, mMonth, mDay);

                dpd.getDatePicker();
                dpd.getDatePicker().setMaxDate(System.currentTimeMillis()-(1000*60*60*24));

                dpd.show();
            }
        });

        setToolBar();
    }

    public void setToolBar() {
//        commonSharedPref = new com.hero.load_calling.Retrofit.CommonSharedPref(this);
//        if (commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getData() == null) {
//            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        token = "Basic " + commonSharedPref.getLoginData().getData();


        spinner_reminders.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customWaitingDialog = new CustomDialog(CustomCallingReportActivity.this);
                customWaitingDialog.show();
                campaign_arraylist[0].clear();
                campaign_arraylist_ID.clear();
                serviceReminderDataList.clear();

                spinner_campaingName.setAdapter(new ArrayAdapter<String>(CustomCallingReportActivity.this,
                        android.R.layout.simple_dropdown_item_1line,
                        campaign_arraylist[0]));
//                adapter.notifyDataSetChanged();
                getcampaignlist(CommonVariables.customtokenbearer);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_campaingName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(Drawer_Activity.this, "hello", Toast.LENGTH_SHORT).show();
                spinner_selected_item = spinner_campaingName.getSelectedItem().toString();
                spinner_name_position = position;
                //Log.e("Position", "Postion" + spinner_name_position);
                if (!datefrom.isEmpty() && !dateto.isEmpty() && !CommonVariables.customtokenbearer.isEmpty()) {
                    getReportingdata(token, datefrom, dateto, spinner_selected_item);
//                    getReportingdata(token, datefrom, dateto, spinner_campaingName.getSelectedItem().toString());
                }


                // getcustomcallingdata(token, "0");
//                    if (CallArrayAdapter != null) {
//                        CallArrayAdapter.clear();
//                    }
//                    if (serviceReminderDataList.size() == 0) {
//                        getcustomcallingdata(token, "0");
//                    }
//                    if (campaign_arraylist[0].size() > 0) {
//                        String campaingname = campaign_arraylist_ID.get(spinner_name_position);
//                        Log.e("CampaignName", "Campaing" + campaingname);
//                        getCallProgressstatus(token, spinner_selected_item);
//                    }


                //spinner_campain_list.setSelection(0);

                //Toast.makeText(Drawer_Activity.this, position, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*---------------------------------------------------------------------------------------------------*/


    }

    CustomDialog customWaitingDialog;
    public ArrayAdapter<String> CampaignArrayAdapter, CallArrayAdapter;
    Integer spinner_name_position;
    String spinner_selected_item;

    // Get Campaign List Data
    public void getcampaignlist(String token) {
        //customWaitingDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiConstant.filter, spinner_reminders.getSelectedItem().toString());
        apiController.getCampaignData(token, params);
    }


    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {
        if (beanTag.matches(ApiConstant.REPORTINGDATA)) {
            ReportModel reportingModel = (ReportModel) superClassCastBean;
            if (reportingModel.getData().getTable().size() > 0) {

                for (ReportModel.DataDTO.TableDTO dataBean : reportingModel.getData().getTable()) {


                    totalavilablecalls.setText("" + dataBean.getTotalData());
                    totalcalls.setText("" + dataBean.getTotalCalls());
                    folowup.setText("" + dataBean.getTotalFollowUp());
                    bookings.setText("" + dataBean.getTotalBooking());
                    calldone.setText("" + dataBean.getTotalCallDone());
                    callnotdone.setText("" + dataBean.getTotalCallNotDone());
                    statyus.setText("" + dataBean.getTotalCallProgressStatus());


                }
            }

            if (customDialog.isShowing()) {
                customDialog.dismiss();
            }

        } else if (beanTag.matches(ApiConstant.CAMPAIGNLIST)) {
            CampaignModel campaignModel = (CampaignModel) superClassCastBean;
            if (campaignModel.getData() != null && campaignModel.getData().size() > 0) {
                //campaign_arraylist[0].add("  ");
                campaign_arraylist[0].clear();
                campaign_arraylist_ID.clear();
                campaignNameHashMap = new HashMap<>();
                for (CampaignModel.DataDTO dataBean : campaignModel.getData()) {
                    campaignNameHashMap.put(dataBean.getCampaignName(), dataBean);
                    campaign_arraylist[0].add(dataBean.getCampaignName());
                    campaign_arraylist_ID.add(dataBean.getId());

                }
                CampaignArrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, campaign_arraylist[0]);
                spinner_campaingName.setAdapter(CampaignArrayAdapter);

            }
            customWaitingDialog.dismiss();
        }
        else if(beanTag.matches(ApiConstant.TOKEN_TAG))
        {
            TokenModel tokenModel=(TokenModel)superClassCastBean;


            CommonVariables.customtokenbearer="bearer "+tokenModel.getAccessToken();


            getcampaignlist(CommonVariables.customtokenbearer);

            Toast.makeText(this,"Successfully",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(String msg) {
        customDialog.dismiss();

    }

    @Override
    public void onError(String msg) {
        customDialog.dismiss();

    }


    public void getReportingdata(String token, String startdate, String enddate, String campaignName) {
        if (!campaignNameHashMap.containsKey(campaignName)) {
            spinner_reminders.setSelection(0);// mismatch rehit all the apis
            return;
        }
        customDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("StartDate", startdate);
        params.put("EndDate", enddate);
        params.put("CampaignId", Objects.requireNonNull(campaignNameHashMap.get(campaignName)).getId());
        //Log.e("ooo", "Prarm " + new Gson().toJson(params));
        apiController.getReportingData(CommonVariables.customtokenbearer, params);

// TODO: 05-12-2020  campaign name
    }


    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate = null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }

}