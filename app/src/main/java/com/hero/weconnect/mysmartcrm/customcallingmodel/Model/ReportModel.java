package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class ReportModel extends SuperClassCastBean {

    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : {"Table":[{"TotalData":"12651","TotalCalls":"0","TotalCallDone":"0","TotalCallNotDone":"0","TotalBooking":"0","TotalFollowUp":"0","TotalRemark":"0","TotalCallProgressStatus":"0"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private DataDTO Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataDTO getData() {
        return Data;
    }

    public void setData(DataDTO Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        @SerializedName("Table")
        private List<TableDTO> Table;

        public List<TableDTO> getTable() {
            return Table;
        }

        public void setTable(List<TableDTO> Table) {
            this.Table = Table;
        }

        public static class TableDTO {
            /**
             * TotalData : 12651
             * TotalCalls : 0
             * TotalCallDone : 0
             * TotalCallNotDone : 0
             * TotalBooking : 0
             * TotalFollowUp : 0
             * TotalRemark : 0
             * TotalCallProgressStatus : 0
             */

            @SerializedName("TotalData")
            private String TotalData;
            @SerializedName("TotalCalls")
            private String TotalCalls;
            @SerializedName("TotalCallDone")
            private String TotalCallDone;
            @SerializedName("TotalCallNotDone")
            private String TotalCallNotDone;
            @SerializedName("TotalBooking")
            private String TotalBooking;
            @SerializedName("TotalFollowUp")
            private String TotalFollowUp;
            @SerializedName("TotalRemark")
            private String TotalRemark;
            @SerializedName("TotalCallProgressStatus")
            private String TotalCallProgressStatus;

            public String getTotalData() {
                return TotalData;
            }

            public void setTotalData(String TotalData) {
                this.TotalData = TotalData;
            }

            public String getTotalCalls() {
                return TotalCalls;
            }

            public void setTotalCalls(String TotalCalls) {
                this.TotalCalls = TotalCalls;
            }

            public String getTotalCallDone() {
                return TotalCallDone;
            }

            public void setTotalCallDone(String TotalCallDone) {
                this.TotalCallDone = TotalCallDone;
            }

            public String getTotalCallNotDone() {
                return TotalCallNotDone;
            }

            public void setTotalCallNotDone(String TotalCallNotDone) {
                this.TotalCallNotDone = TotalCallNotDone;
            }

            public String getTotalBooking() {
                return TotalBooking;
            }

            public void setTotalBooking(String TotalBooking) {
                this.TotalBooking = TotalBooking;
            }

            public String getTotalFollowUp() {
                return TotalFollowUp;
            }

            public void setTotalFollowUp(String TotalFollowUp) {
                this.TotalFollowUp = TotalFollowUp;
            }

            public String getTotalRemark() {
                return TotalRemark;
            }

            public void setTotalRemark(String TotalRemark) {
                this.TotalRemark = TotalRemark;
            }

            public String getTotalCallProgressStatus() {
                return TotalCallProgressStatus;
            }

            public void setTotalCallProgressStatus(String TotalCallProgressStatus) {
                this.TotalCallProgressStatus = TotalCallProgressStatus;
            }
        }
    }
}
