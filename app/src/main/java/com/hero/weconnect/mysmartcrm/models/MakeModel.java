package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class MakeModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"Make":"bajaj"},{"Make":"honda"},{"Make":"yamaha"}]
     * ErrorMessage : null
     */

    private String Message;
    private Object ErrorMessage;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Object getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(Object ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Make : bajaj
         */

        private String Make;

        public String getMake() {
            return Make;
        }

        public void setMake(String Make) {
            this.Make = Make;
        }
    }
}
