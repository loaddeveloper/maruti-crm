package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.io.Serializable;
import java.util.List;

public class UserDetailsModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"id":"d87e885c-28c4-4035-bc37-be50bfe1980f","username":"ASHU10375","password":"ashu123","user_designation_id":"466869c8-f260-4c15-a16a-4f6c76bf068e","dealercode":"10375","otp_mobileno":null,"login_active_status":null,"login_entrytime":null,"block_data_allocation":null,"user_call_start_time_permit":null,"user_call_end_time_permit":null,"call_delay_time":"00:00:10","user_position_of_executive":"Front Line Supervisor","createdat":"2020-10-29T17:52:58.143","updatedat":"2020-10-29T17:52:58.143","password_updatedat":"2020-10-29T17:52:58.143","deviceid":null,"usertype":"HJC"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : d87e885c-28c4-4035-bc37-be50bfe1980f
         * username : ASHU10375
         * password : ashu123
         * user_designation_id : 466869c8-f260-4c15-a16a-4f6c76bf068e
         * dealercode : 10375
         * otp_mobileno : null
         * login_active_status : null
         * login_entrytime : null
         * block_data_allocation : null
         * user_call_start_time_permit : null
         * user_call_end_time_permit : null
         * call_delay_time : 00:00:10
         * user_position_of_executive : Front Line Supervisor
         * createdat : 2020-10-29T17:52:58.143
         * updatedat : 2020-10-29T17:52:58.143
         * password_updatedat : 2020-10-29T17:52:58.143
         * deviceid : null
         * usertype : HJC
         */

        private String id;
        private String username;
        private String password;
        private String user_designation_id;
        private String dealercode;
        private Object otp_mobileno;
        private Object login_active_status;
        private Object login_entrytime;
        private Object block_data_allocation;
        private Object user_call_start_time_permit;
        private Object user_call_end_time_permit;
        private String call_delay_time;
        private String user_position_of_executive;
        private String createdat;
        private String updatedat;
        private String password_updatedat;
        private Object deviceid;
        private String usertype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUser_designation_id() {
            return user_designation_id;
        }

        public void setUser_designation_id(String user_designation_id) {
            this.user_designation_id = user_designation_id;
        }

        public String getDealercode() {
            return dealercode;
        }

        public void setDealercode(String dealercode) {
            this.dealercode = dealercode;
        }

        public Object getOtp_mobileno() {
            return otp_mobileno;
        }

        public void setOtp_mobileno(Object otp_mobileno) {
            this.otp_mobileno = otp_mobileno;
        }

        public Object getLogin_active_status() {
            return login_active_status;
        }

        public void setLogin_active_status(Object login_active_status) {
            this.login_active_status = login_active_status;
        }

        public Object getLogin_entrytime() {
            return login_entrytime;
        }

        public void setLogin_entrytime(Object login_entrytime) {
            this.login_entrytime = login_entrytime;
        }

        public Object getBlock_data_allocation() {
            return block_data_allocation;
        }

        public void setBlock_data_allocation(Object block_data_allocation) {
            this.block_data_allocation = block_data_allocation;
        }

        public Object getUser_call_start_time_permit() {
            return user_call_start_time_permit;
        }

        public void setUser_call_start_time_permit(Object user_call_start_time_permit) {
            this.user_call_start_time_permit = user_call_start_time_permit;
        }

        public Object getUser_call_end_time_permit() {
            return user_call_end_time_permit;
        }

        public void setUser_call_end_time_permit(Object user_call_end_time_permit) {
            this.user_call_end_time_permit = user_call_end_time_permit;
        }

        public String getCall_delay_time() {
            return call_delay_time;
        }

        public void setCall_delay_time(String call_delay_time) {
            this.call_delay_time = call_delay_time;
        }

        public String getUser_position_of_executive() {
            return user_position_of_executive;
        }

        public void setUser_position_of_executive(String user_position_of_executive) {
            this.user_position_of_executive = user_position_of_executive;
        }

        public String getCreatedat() {
            return createdat;
        }

        public void setCreatedat(String createdat) {
            this.createdat = createdat;
        }

        public String getUpdatedat() {
            return updatedat;
        }

        public void setUpdatedat(String updatedat) {
            this.updatedat = updatedat;
        }

        public String getPassword_updatedat() {
            return password_updatedat;
        }

        public void setPassword_updatedat(String password_updatedat) {
            this.password_updatedat = password_updatedat;
        }

        public Object getDeviceid() {
            return deviceid;
        }

        public void setDeviceid(Object deviceid) {
            this.deviceid = deviceid;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }
    }
}
