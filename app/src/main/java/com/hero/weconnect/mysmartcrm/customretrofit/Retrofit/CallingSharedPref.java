package com.hero.weconnect.mysmartcrm.customretrofit.Retrofit;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.Custom_Model;

import java.lang.reflect.Type;

public class CallingSharedPref {

    private Context mContext;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;

    public CallingSharedPref(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("SRCallingdata", Context.MODE_PRIVATE);

    }

    public Custom_Model getSRCallingData() {

        String json = sharedPreferences.getString(ApiConstant.CUSTOMCALLING, null);
        Gson gson = new Gson();
        Type type = new TypeToken<Custom_Model>() {
        }.getType();

        Custom_Model beans = null;
        beans = gson.fromJson(json, type);

        return beans;

    }

    public void setSRCallingData(Custom_Model beans) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString(ApiConstant.CUSTOMCALLING, json);
        editor.commit();
    }

    public String getSRCallingStatus() {
        String status = sharedPreferences.getString(ApiConstant.CUSTOMCALLING + "status", null);

        return status;

    }

    public void setSRCallingStatus(String status) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.CUSTOMCALLING + "status", status);
        editor.commit();
    }

    public String getSRCallingPosition() {
        String position = sharedPreferences.getString(ApiConstant.CUSTOMCALLING + "position", null);

        return position;

    }

    public void setSRCallingPosition(String position) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.CUSTOMCALLING + "position", position);
        editor.commit();
    }

    public String getSRCallStatus() {
        String callstatus = sharedPreferences.getString(ApiConstant.CUSTOMCALLING + "callstatus", null);

        return callstatus;

    }

    public void setSRCallStatus(String callstatus) {
        editor = sharedPreferences.edit();

        editor.putString(ApiConstant.CUSTOMCALLING + "callstatus", callstatus);
        editor.commit();
    }

    public void setAllClearSRCallingData() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }
}
