package com.hero.weconnect.mysmartcrm.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.customcallingmodel.Model.GetTemplateListModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;

import java.util.ArrayList;


public class AdapterTemplateShowCustom extends RecyclerView.Adapter<AdapterTemplateShowCustom.ViewHolder> {
    ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();
    AdapterTemplateShow.CustomButtonListener customListner;
    private Context context;


    public AdapterTemplateShowCustom(Context context, AdapterTemplateShow.CustomButtonListener customButtonListener, ArrayList<GetTamplateListModel.DataBean> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.templatelist = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.template_list_cardview, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.setIsRecyclable(false);
        GetTamplateListModel.DataBean dataBean = templatelist.get(position);
        holder.templatename.setText("" + dataBean.getTemplate_name().toUpperCase());


        holder.templatelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customListner.sendWhatsappMessagetemplate(position, "" + dataBean.getTemplate_body());
            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return templatelist.size();
    }

    public void setCustomButtonListner(AdapterTemplateShow.CustomButtonListener listener) {
       this.customListner = listener;
    }


    public interface CustomButtonListener {
        void sendWhatsappMessagetemplate(int position, String templatebody);

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView templatename;
        public LinearLayout templatelayout;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.templatename = rootView.findViewById(R.id.srtext);
            this.templatelayout = rootView.findViewById(R.id.srlayout);
        }
    }

}