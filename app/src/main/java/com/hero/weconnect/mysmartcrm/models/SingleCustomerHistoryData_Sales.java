package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SingleCustomerHistoryData_Sales extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"master_sales_enquiry_id":"14c5903d-c84b-4457-901a-70139b47e26c","record_creation_date":"1900-01-01T00:00:00","followup_status":"","next_followup_date":"","followup_done":"contacted","expected_date_of_purchase":"","closurereason":"","closure_sub_reason":"","make":"","model":"","remark":"","id":"58dcc337-cba3-41ca-9079-da7c4d230842"}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * master_sales_enquiry_id : 14c5903d-c84b-4457-901a-70139b47e26c
         * record_creation_date : 1900-01-01T00:00:00
         * followup_status :
         * next_followup_date :
         * followup_done : contacted
         * expected_date_of_purchase :
         * closurereason :
         * closure_sub_reason :
         * make :
         * model :
         * remark :
         * id : 58dcc337-cba3-41ca-9079-da7c4d230842
         */

        @SerializedName("master_sales_enquiry_id")
        private String masterSalesEnquiryId;
        @SerializedName("record_creation_date")
        private String recordCreationDate;
        @SerializedName("followup_status")
        private String followupStatus;
        @SerializedName("next_followup_date")
        private String nextFollowupDate;
        @SerializedName("followup_done")
        private String followupDone;
        @SerializedName("expected_date_of_purchase")
        private String expectedDateOfPurchase;
        @SerializedName("closurereason")
        private String closurereason;
        @SerializedName("closure_sub_reason")
        private String closureSubReason;
        @SerializedName("make")
        private String make;
        @SerializedName("model")
        private String model;
        @SerializedName("remark")
        private String remark;
        @SerializedName("id")
        private String id;

        public String getMasterSalesEnquiryId() {
            return masterSalesEnquiryId;
        }

        public void setMasterSalesEnquiryId(String masterSalesEnquiryId) {
            this.masterSalesEnquiryId = masterSalesEnquiryId;
        }

        public String getRecordCreationDate() {
            return recordCreationDate;
        }

        public void setRecordCreationDate(String recordCreationDate) {
            this.recordCreationDate = recordCreationDate;
        }

        public String getFollowupStatus() {
            return followupStatus;
        }

        public void setFollowupStatus(String followupStatus) {
            this.followupStatus = followupStatus;
        }

        public String getNextFollowupDate() {
            return nextFollowupDate;
        }

        public void setNextFollowupDate(String nextFollowupDate) {
            this.nextFollowupDate = nextFollowupDate;
        }

        public String getFollowupDone() {
            return followupDone;
        }

        public void setFollowupDone(String followupDone) {
            this.followupDone = followupDone;
        }

        public String getExpectedDateOfPurchase() {
            return expectedDateOfPurchase;
        }

        public void setExpectedDateOfPurchase(String expectedDateOfPurchase) {
            this.expectedDateOfPurchase = expectedDateOfPurchase;
        }

        public String getClosurereason() {
            return closurereason;
        }

        public void setClosurereason(String closurereason) {
            this.closurereason = closurereason;
        }

        public String getClosureSubReason() {
            return closureSubReason;
        }

        public void setClosureSubReason(String closureSubReason) {
            this.closureSubReason = closureSubReason;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
