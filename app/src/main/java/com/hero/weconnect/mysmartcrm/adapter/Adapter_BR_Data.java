package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.models.SR_Alloted_data_model;

import java.util.ArrayList;


public class Adapter_BR_Data extends RecyclerView.Adapter<Adapter_BR_Data.ViewHolder> {
    ArrayList<SR_Alloted_data_model.DataDTO> templatelist = new ArrayList<>();

    private Context context;
    Adapter_BR_Data.CustomButtonListener customListner;

    public Adapter_BR_Data(Context context, Adapter_BR_Data.CustomButtonListener customButtonListener, ArrayList<SR_Alloted_data_model.DataDTO> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.templatelist = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.sr_data_allocation_card, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        SR_Alloted_data_model.DataDTO dataBean = templatelist.get(position);
        holder.username.setText("" + dataBean.getUsername());
        holder.alloteddatal.setText("" + dataBean.getTotalCount());







    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return templatelist.size();
    }
    public void setCustomButtonListner(Adapter_BR_Data.CustomButtonListener listener) {
        this.customListner = listener;
    }

    public interface CustomButtonListener {

        void getAllotedCustomer(int position, String DSEID);


    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView username,alloteddatal;
        public LinearLayout AllotedData;


        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.username = rootView.findViewById(R.id.username);
            this.alloteddatal = rootView.findViewById(R.id.alloteddata);
            this.AllotedData = rootView.findViewById(R.id.alloteddatalayout);
        }
    }


}