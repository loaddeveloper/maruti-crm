package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class PSFDissatisfactionReasonModel extends SuperClassCastBean {

    @com.google.gson.annotations.SerializedName("Message")
    private String message;
    @com.google.gson.annotations.SerializedName("Data")
    private java.util.List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @com.google.gson.annotations.SerializedName("id")
        private String id;
        @com.google.gson.annotations.SerializedName("reason")
        private String reason;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}
