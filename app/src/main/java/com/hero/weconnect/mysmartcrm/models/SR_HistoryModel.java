package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SR_HistoryModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"contact_status":"Contacted","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T17:57:51.647","call_end_datetime":"2020-09-29T17:57:51.803","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T17:57:51.647"},{"sno":2,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T16:39:57.903","call_end_datetime":"2020-09-29T17:06:54.947","User":"test","calledstatus":"BUSY","createdat":"2020-09-29T16:39:57.903"},{"sno":3,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T15:52:18.98","call_end_datetime":"2020-09-29T15:52:23.52","User":"test","calledstatus":"BUSY","createdat":"2020-09-29T15:52:18.98"},{"sno":4,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T14:59:30.193","call_end_datetime":"2020-09-29T15:00:40.527","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T14:59:30.193"},{"sno":5,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"2020-09-30T14:49:09","not_coming_reason":"","comments":"KM not covered","pickup_drop":"","call_start_datetime":"2020-09-29T14:49:04.863","call_end_datetime":"2020-09-29T14:49:31.877","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T14:49:04.863"},{"sno":6,"contact_status":"Contacted","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T14:30:09.547","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T14:30:09.547"},{"sno":7,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T14:03:09.877","call_end_datetime":"2020-09-29T14:03:57.053","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T14:03:09.877"},{"sno":8,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T13:52:26.34","call_end_datetime":"2020-09-29T14:23:33.353","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T13:52:26.34"},{"sno":9,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T13:05:09.763","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T13:05:09.763"},{"sno":10,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T12:45:13.367","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T12:45:13.367"},{"sno":11,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T12:37:28.7","call_end_datetime":"2020-09-29T12:37:46.263","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T12:37:28.7"},{"sno":12,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T12:35:36.5","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T12:35:36.5"},{"sno":13,"contact_status":"","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-29T12:23:13.307","call_end_datetime":"1900-01-01T00:00:00","User":"test","calledstatus":"CALL DONE","createdat":"2020-09-29T12:23:13.307"},{"sno":14,"contact_status":"Not Contacted","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","tagged_sr_status":"","contact_changed":"","reminder_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","comments":"","pickup_drop":"","call_start_datetime":"2020-09-23T13:01:09.12","call_end_datetime":"2020-09-23T13:01:17.39","User":"TM10375","calledstatus":"BUSY","createdat":"2020-09-23T13:01:09.12"}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * sno : 1
         * contact_status : Contacted
         * customerreply :
         * servicebooking : 1900-01-01T00:00:00
         * servicerequest :
         * tagged_sr_status :
         * contact_changed :
         * reminder_followup_date : 1900-01-01T00:00:00
         * not_coming_reason :
         * comments :
         * pickup_drop :
         * call_start_datetime : 2020-09-29T17:57:51.647
         * call_end_datetime : 2020-09-29T17:57:51.803
         * User : test
         * calledstatus : CALL DONE
         * createdat : 2020-09-29T17:57:51.647
         */

        private int sno;
        private String contact_status;
        private String customerreply;
        private String servicebooking;
        private String servicerequest;
        private String tagged_sr_status;
        private String contact_changed;
        private String reminder_followup_date;
        private String not_coming_reason;
        private String comments;
        private String pickup_drop;
        private String call_start_datetime;
        private String call_end_datetime;
        private String User;
        private String calledstatus;
        private String createdat;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getContact_status() {
            return contact_status;
        }

        public void setContact_status(String contact_status) {
            this.contact_status = contact_status;
        }

        public String getCustomerreply() {
            return customerreply;
        }

        public void setCustomerreply(String customerreply) {
            this.customerreply = customerreply;
        }

        public String getServicebooking() {
            return servicebooking;
        }

        public void setServicebooking(String servicebooking) {
            this.servicebooking = servicebooking;
        }

        public String getServicerequest() {
            return servicerequest;
        }

        public void setServicerequest(String servicerequest) {
            this.servicerequest = servicerequest;
        }

        public String getTagged_sr_status() {
            return tagged_sr_status;
        }

        public void setTagged_sr_status(String tagged_sr_status) {
            this.tagged_sr_status = tagged_sr_status;
        }

        public String getContact_changed() {
            return contact_changed;
        }

        public void setContact_changed(String contact_changed) {
            this.contact_changed = contact_changed;
        }

        public String getReminder_followup_date() {
            return reminder_followup_date;
        }

        public void setReminder_followup_date(String reminder_followup_date) {
            this.reminder_followup_date = reminder_followup_date;
        }

        public String getNot_coming_reason() {
            return not_coming_reason;
        }

        public void setNot_coming_reason(String not_coming_reason) {
            this.not_coming_reason = not_coming_reason;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getPickup_drop() {
            return pickup_drop;
        }

        public void setPickup_drop(String pickup_drop) {
            this.pickup_drop = pickup_drop;
        }

        public String getCall_start_datetime() {
            return call_start_datetime;
        }

        public void setCall_start_datetime(String call_start_datetime) {
            this.call_start_datetime = call_start_datetime;
        }

        public String getCall_end_datetime() {
            return call_end_datetime;
        }

        public void setCall_end_datetime(String call_end_datetime) {
            this.call_end_datetime = call_end_datetime;
        }

        public String getUser() {
            return User;
        }

        public void setUser(String User) {
            this.User = User;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getCreatedat() {
            return createdat;
        }

        public void setCreatedat(String createdat) {
            this.createdat = createdat;
        }
    }
}
