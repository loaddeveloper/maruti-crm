package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Calling_Sales_EnquirySharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.MissedCallActivity_ENQUIRY;
import com.hero.weconnect.mysmartcrm.activity.MissedCallActivity_SR;
import com.hero.weconnect.mysmartcrm.activity.Sales_Followup_Activity;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_Sales;

import java.util.ArrayList;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_MissedCall_Enquiry extends RecyclerView.Adapter<Adapter_MissedCall_Enquiry.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    CustomButtonListener customListner;
    Adapter_MissedCall_Enquiry adapter;
    ArrayList<MissedDataModel_Sales.DataBean> misseddatalist = new ArrayList<>();
    int size = 0;
    Calling_Sales_EnquirySharedPref callingSharedPref;
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    private Context context;
    CountDownTimer countDownTimer;



    public Adapter_MissedCall_Enquiry(Context context, CustomButtonListener customButtonListener, ArrayList<MissedDataModel_Sales.DataBean> list) {

        this.context = context;
        this.customListner = customButtonListener;
        this.misseddatalist = list;
        setHasStableIds(true);

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.sales_data_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //holder.setIsRecyclable(true);



        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);

        if (misseddatalist.size() > 0) {
            if (position == misseddatalist.size())
            {
                holder.morebtn1.setVisibility(View.VISIBLE);
                holder.main_ll.setVisibility(View.GONE);

            }
            else {

                MissedDataModel_Sales.DataBean dataBean = misseddatalist.get(position);
                holder.morebtn1.setVisibility(View.GONE);
                holder.main_ll.setVisibility(View.VISIBLE);
                int sno = position + 1;
                final String idd = String.valueOf(dataBean.getId());
                holder.srno.setText("" + sno + ".");
                holder.customername.setText("" + dataBean.getCusotmer_first_and_last_name().toUpperCase());
                holder.enquiry_no.setText("" + dataBean.getEnquirynumber().toUpperCase());
                holder.customer_contact.setText("" + dataBean.getMobileno_missed().toUpperCase());
                holder.enquiry_opendate.setText("" + dataBean.getEnquiry_open_date().toUpperCase());
                holder.enquiry_status.setText("" + dataBean.getEnquiry_status().toUpperCase());
                holder.enquiry_model.setText("" + dataBean.getModel_interested_in().toUpperCase());
                holder.calledstatus.setText("");


                countDownTimer =new CountDownTimer(2000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {



                        final String idssss = common_calling_color_sharedPreferances.getmatserid();
                final String call = common_calling_color_sharedPreferances.getcallcurrentstatus();
                SharedPreferences chkfsss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                final String status = chkfsss.getString("Status", "");
                final String status1 = chkfsss.getString("Status1", "");
                SharedPreferences chkfss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                final String icca = chkfss.getString("call", "");

                if (dataBean.getCusotmer_first_and_last_name() != null && !dataBean.getCusotmer_first_and_last_name().equals("")) {

                    if (dataBean.getId().equals(idssss) && call.equals("s")) {
                        holder.calledstatus.setText("Ringing");
                        pos = position;
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                        holder.tv_hold.setVisibility(View.VISIBLE);

                    } else if (dataBean.getId().equals(idssss) && /*dataBean.getCalledstatus().equals("CALL DONE")*/call.equals("s")) {
                        holder.calledstatus.setText("Ringing");
                        pos = position;
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                        holder.tv_hold.setVisibility(View.VISIBLE);

                    } else if ((icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                        if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.tv_hold.setVisibility(View.GONE);
                            holder.tv_unhold.setVisibility(View.GONE);


                        } else {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);
                            holder.tv_unhold.setVisibility(View.GONE);


                        }
                    } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {

                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    }
                    holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customListner.sendWhatsappMessage(position, "" + dataBean.getMobilenumber());
                        }
                    });
                    holder.textmessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customListner.sendtextmessage(position, "" + dataBean.getMobilenumber());
                        }
                    });
                    holder.main_ll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            customListner.getCustomerDetails(position, dataBean.getId(), dataBean.getCusotmer_first_and_last_name(),
                                    dataBean.getEnquirynumber().toUpperCase(),
                                    dataBean.getModel_interested_in().toUpperCase(), dataBean.getEnquiry_open_date().toUpperCase()
                                    , dataBean.getEnquiry_status().toUpperCase(),
                                    dataBean.getMobilenumber());
                        }
                    });
                    holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public boolean onLongClick(View v) {
                            if (CommonVariables.isCalldoneChecked) {

                                return CommonVariables.isCalldoneChecked;
                            }

                            common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                            String uid = UUID.randomUUID().toString();
                            // Log.e("Longpresshid"," "+uid +" \n"+dataBean.getId());
                            SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                            SharedPreferences.Editor edits = prefs.edit();
                            edits.putString(dataBean.getId(), uid);
                            edits.commit();
                            if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                                Toasty.warning(context, "Call is Currently Onging", Toast.LENGTH_SHORT).show();
                            } else {
                                if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                    // Create the Uri from phoneNumberInput
                                  // Uri uri = Uri.parse("tel:" + CommonVariables.SALES_Testingnumber);
                                    Uri uri = Uri.parse("tel:" + dataBean.getMobilenumber());

                                    // Start call to the number in input
                                    context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                    holder.calledstatus.setText("Ringing");
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                    common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                    ((Sales_Followup_Activity) context).setIndex(position, true, uid);


                                    common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                                } else {
                                    // Request permission to call
                                    ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                                }
                            }
                            return false;
                        }
                    });
                } else {
                    if (dataBean.getId().equals(idssss) && call.equals("s")) {
                        holder.calledstatus.setText("Ringing");
                        pos = position;
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                        holder.tv_hold.setVisibility(View.VISIBLE);

                    }
                    else if (dataBean.getId().equals(idssss) && /*dataBean.getCalledstatus().equals("CALL DONE")*/call.equals("s")) {
                        holder.calledstatus.setText("Ringing");
                        pos = position;
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                        holder.tv_hold.setVisibility(View.VISIBLE);

                    }
                    else if ((icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status.equals("NextFollowUp")) && (!status.equals("NCR")) && (!status1.equals("rmkcolor"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    }
                    else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                        if (status.equals("booking") || status.equals("NextFollowUp") || status.equals("NCR")) {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                            holder.tv_hold.setVisibility(View.GONE);
                            holder.tv_unhold.setVisibility(View.GONE);


                        } else {
                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                            holder.calledstatus.setText("CALL DONE");
                            holder.tv_hold.setVisibility(View.GONE);
                            holder.tv_unhold.setVisibility(View.GONE);


                        }
                    } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else if ((icca.equals("CALL DONE")) && (status.equals("NextFollowUp"))) {

                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else if ((icca.equals("CALL DONE")) && (status.equals("NCR"))) {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                        holder.calledstatus.setText("CALL DONE");
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    } else {
                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.GONE);


                    }





                    holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customListner.sendWhatsappMessage(position, "" + dataBean.getMobilenumber());
                        }
                    });
                    holder.textmessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customListner.sendtextmessage(position, "" + dataBean.getMobilenumber());
                        }
                    });
                    holder.main_ll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            customListner.getMissed(position);
                        }
                    });
                    holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public boolean onLongClick(View v) {
                            if (CommonVariables.isCalldoneChecked) {

                                return CommonVariables.isCalldoneChecked;
                            }

                            common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                            String uid = UUID.randomUUID().toString();
                            // Log.e("Longpresshid"," "+uid +" \n"+dataBean.getId());
                            SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                            SharedPreferences.Editor edits = prefs.edit();
                            edits.putString(dataBean.getId(), uid);
                            edits.commit();
                            if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                                Toasty.warning(context, "Call is Currently Onging", Toast.LENGTH_SHORT).show();
                            } else {
                                if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                    // Create the Uri from phoneNumberInput
                                   // Uri uri = Uri.parse("tel:" + CommonVariables.SALES_Testingnumber);
                                    Uri uri = Uri.parse("tel:" + dataBean.getMobilenumber());

                                    // Start call to the number in input
                                    context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                    holder.calledstatus.setText("Ringing");
                                    holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                    common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                   // ((MissedCallActivity_ENQUIRY) context).setIndex(position, true, uid);


                                    common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                                } else {
                                    // Request permission to call
                                    ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                                }
                            }
                            return false;
                        }
                    });
                }


                    }

                    @Override
                    public void onFinish() {

                        countDownTimer.cancel();

                    }
                };

                countDownTimer.start();

                holder.historyybtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getHistoryDetails(position);
                    }
                });
                holder.moredetailsbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customListner.getMoreDetails(position);
                    }
                });

                holder.tv_hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.GONE);
                        holder.tv_unhold.setVisibility(View.VISIBLE);
                        customListner.holdCall(position, dataBean.getMobilenumber());
                    }

                });

                holder.tv_unhold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.tv_hold.setVisibility(View.VISIBLE);
                        holder.tv_unhold.setVisibility(View.GONE);
                        customListner.unHoldCall(position, dataBean.getMobilenumber());
                    }

                });
            }


        }
        // load more data
        holder.morebtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context instanceof MissedCallActivity_ENQUIRY) {

                    ((MissedCallActivity_ENQUIRY) context).moreData();

                }
            }
        });




    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    public int getItemCount() {


        if (misseddatalist.size() >= 10) {
            if ((misseddatalist.size() % 10) == 0) {
                size = misseddatalist.size() + 1;

            } else {
                size = misseddatalist.size();

            }
        } else {
            size = misseddatalist.size();
        }


        return size;
    }

    public void setCustomButtonListner(Adapter_MissedCall_Enquiry.CustomButtonListener listener) {
        this.customListner = listener;
    }

    public void updateList(ArrayList<MissedDataModel_Sales.DataBean> list11) {
        misseddatalist = list11;
        notifyDataSetChanged();
    }
    public interface CustomButtonListener {


        void getCustomerDetails(int position, String matserid, String customername, String enuiryno, String model, String enquiryopendate, String enquirystatus, String mobileno);

        void getHistoryDetails(int position);

        void getMoreDetails(int position);

        void sendWhatsappMessage(int position, String mobileno);

        void sendtextmessage(int position, String mobileno);

        void getMissed(int position);

        void holdCall(int position, String contatcnumber);

        void unHoldCall(int position, String contatcnumber);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView srno, idddata, tv_hold, tv_unhold, customername, customer_contact, enquiry_no, enquiry_opendate, enquiry_status, enquiry_model, calledstatus, morebtn1;
        public ImageView moredetailsbtn, historyybtn, whatsapp, textmessage;
        public LinearLayout main_ll;

        public ViewHolder(View rootView) {
            super(rootView);
            this.srno = rootView.findViewById(R.id.srno);
            this.idddata = rootView.findViewById(R.id.idddata);
            this.customername = rootView.findViewById(R.id.customerName);
            this.customer_contact = rootView.findViewById(R.id.mobileno);
            this.enquiry_no = rootView.findViewById(R.id.enquiryno);
            this.enquiry_opendate = rootView.findViewById(R.id.enquiryopendate);
            this.enquiry_status = rootView.findViewById(R.id.enquirystatus);
            this.enquiry_model = rootView.findViewById(R.id.model);
            this.calledstatus = rootView.findViewById(R.id.cstatus);
            this.morebtn1 = rootView.findViewById(R.id.morebtn1);
            this.moredetailsbtn = rootView.findViewById(R.id.moredetailsbtn);
            this.historyybtn = rootView.findViewById(R.id.historyybtn);
            this.tv_hold = rootView.findViewById(R.id.tv_hold);
            this.tv_unhold = rootView.findViewById(R.id.tv_unhold);
            this.whatsapp = rootView.findViewById(R.id.whatsapp);
            textmessage = rootView.findViewById(R.id.textmessage);
            this.main_ll = rootView.findViewById(R.id.main_ll);

        }
    }
}