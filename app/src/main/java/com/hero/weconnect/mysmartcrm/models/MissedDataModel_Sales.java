package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class MissedDataModel_Sales extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"id":"41b0fce2-83d3-4251-9d6e-392d99d6de07","mobileno_missed":"7877268058","missed_tag":"ENQUIRY","dealercode":"trial","createdat":"2020-09-17T16:20:39.15","updatedat":"2020-09-17T16:20:39.15","id1":null,"enquirynumber":"","enquiry_open_date":"","enquiry_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_purchase":"","cusotmer_first_and_last_name":"","mobilenumber":"","emailid":"","address":"","age":"","gender":"","model_interested_in":"","exchange_required":"","finance_required":"","dse_name":"","position_of_executive":"","enquiry_id":"","dealer_name":"","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":""},{"id":"459a5989-605b-46b6-9f3b-da9b5e96fe28","mobileno_missed":"7877268059","missed_tag":"ENQUIRY","dealercode":"trial","createdat":"2020-09-17T16:20:11.977","updatedat":"2020-09-17T16:20:11.977","id1":null,"enquirynumber":"","enquiry_open_date":"","enquiry_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_purchase":"","cusotmer_first_and_last_name":"","mobilenumber":"","emailid":"","address":"","age":"","gender":"","model_interested_in":"","exchange_required":"","finance_required":"","dse_name":"","position_of_executive":"","enquiry_id":"","dealer_name":"","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":""},{"id":"df283938-b85a-4ccb-938e-fb7097765fe1","mobileno_missed":"7014499376","missed_tag":"ENQUIRY","dealercode":"trial","createdat":"2020-09-15T14:20:19.683","updatedat":"2020-09-15T14:20:19.683","id1":null,"enquirynumber":"","enquiry_open_date":"","enquiry_status":"","next_followup_date":"1900-01-01T00:00:00","expected_date_purchase":"","cusotmer_first_and_last_name":"","mobilenumber":"","emailid":"","address":"","age":"","gender":"","model_interested_in":"","exchange_required":"","finance_required":"","dse_name":"","position_of_executive":"","enquiry_id":"","dealer_name":"","last_follow_up_date_if_any":"","enquiry_comments":"","dse_employee_id":"","existing_vehicle":"","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":""}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * id : 41b0fce2-83d3-4251-9d6e-392d99d6de07
         * mobileno_missed : 7877268058
         * missed_tag : ENQUIRY
         * dealercode : trial
         * createdat : 2020-09-17T16:20:39.15
         * updatedat : 2020-09-17T16:20:39.15
         * id1 : null
         * enquirynumber :
         * enquiry_open_date :
         * enquiry_status :
         * next_followup_date : 1900-01-01T00:00:00
         * expected_date_purchase :
         * cusotmer_first_and_last_name :
         * mobilenumber :
         * emailid :
         * address :
         * age :
         * gender :
         * model_interested_in :
         * exchange_required :
         * finance_required :
         * dse_name :
         * position_of_executive :
         * enquiry_id :
         * dealer_name :
         * last_follow_up_date_if_any :
         * enquiry_comments :
         * dse_employee_id :
         * existing_vehicle :
         * test_ride_required :
         * test_ride_required_time :
         * test_ride_taken :
         * test_ride_taken_time :
         * enquiry_source :
         * awareness_source :
         * opinion_leader :
         * financier :
         */

        private String id;
        private String mobileno_missed;
        private String missed_tag;
        private String dealercode;
        private String createdat;
        private String updatedat;
        private String id1;
        private String enquirynumber;
        private String enquiry_open_date;
        private String enquiry_status;
        private String next_followup_date;
        private String expected_date_purchase;
        private String cusotmer_first_and_last_name;
        private String mobilenumber;
        private String emailid;
        private String address;
        private String age;
        private String gender;
        private String model_interested_in;
        private String exchange_required;
        private String finance_required;
        private String dse_name;
        private String position_of_executive;
        private String enquiry_id;
        private String dealer_name;
        private String last_follow_up_date_if_any;
        private String enquiry_comments;
        private String dse_employee_id;
        private String existing_vehicle;
        private String test_ride_required;
        private String test_ride_required_time;
        private String test_ride_taken;
        private String test_ride_taken_time;
        private String enquiry_source;
        private String awareness_source;
        private String opinion_leader;
        private String financier;
        private String sno;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMobileno_missed() {
            return mobileno_missed;
        }

        public void setMobileno_missed(String mobileno_missed) {
            this.mobileno_missed = mobileno_missed;
        }

        public String getMissed_tag() {
            return missed_tag;
        }

        public void setMissed_tag(String missed_tag) {
            this.missed_tag = missed_tag;
        }

        public String getDealercode() {
            return dealercode;
        }

        public void setDealercode(String dealercode) {
            this.dealercode = dealercode;
        }

        public String getCreatedat() {
            return createdat;
        }

        public void setCreatedat(String createdat) {
            this.createdat = createdat;
        }

        public String getUpdatedat() {
            return updatedat;
        }

        public void setUpdatedat(String updatedat) {
            this.updatedat = updatedat;
        }

        public String getId1() {
            return id1;
        }

        public void setId1(String id1) {
            this.id1 = id1;
        }

        public String getEnquirynumber() {
            return enquirynumber;
        }

        public void setEnquirynumber(String enquirynumber) {
            this.enquirynumber = enquirynumber;
        }

        public String getEnquiry_open_date() {
            return enquiry_open_date;
        }

        public void setEnquiry_open_date(String enquiry_open_date) {
            this.enquiry_open_date = enquiry_open_date;
        }

        public String getEnquiry_status() {
            return enquiry_status;
        }

        public void setEnquiry_status(String enquiry_status) {
            this.enquiry_status = enquiry_status;
        }

        public String getNext_followup_date() {
            return next_followup_date;
        }

        public void setNext_followup_date(String next_followup_date) {
            this.next_followup_date = next_followup_date;
        }

        public String getExpected_date_purchase() {
            return expected_date_purchase;
        }

        public void setExpected_date_purchase(String expected_date_purchase) {
            this.expected_date_purchase = expected_date_purchase;
        }

        public String getCusotmer_first_and_last_name() {
            return cusotmer_first_and_last_name;
        }

        public void setCusotmer_first_and_last_name(String cusotmer_first_and_last_name) {
            this.cusotmer_first_and_last_name = cusotmer_first_and_last_name;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getModel_interested_in() {
            return model_interested_in;
        }

        public void setModel_interested_in(String model_interested_in) {
            this.model_interested_in = model_interested_in;
        }

        public String getExchange_required() {
            return exchange_required;
        }

        public void setExchange_required(String exchange_required) {
            this.exchange_required = exchange_required;
        }

        public String getFinance_required() {
            return finance_required;
        }

        public void setFinance_required(String finance_required) {
            this.finance_required = finance_required;
        }

        public String getDse_name() {
            return dse_name;
        }

        public void setDse_name(String dse_name) {
            this.dse_name = dse_name;
        }

        public String getPosition_of_executive() {
            return position_of_executive;
        }

        public void setPosition_of_executive(String position_of_executive) {
            this.position_of_executive = position_of_executive;
        }

        public String getEnquiry_id() {
            return enquiry_id;
        }

        public void setEnquiry_id(String enquiry_id) {
            this.enquiry_id = enquiry_id;
        }

        public String getDealer_name() {
            return dealer_name;
        }

        public void setDealer_name(String dealer_name) {
            this.dealer_name = dealer_name;
        }

        public String getLast_follow_up_date_if_any() {
            return last_follow_up_date_if_any;
        }

        public void setLast_follow_up_date_if_any(String last_follow_up_date_if_any) {
            this.last_follow_up_date_if_any = last_follow_up_date_if_any;
        }

        public String getEnquiry_comments() {
            return enquiry_comments;
        }

        public void setEnquiry_comments(String enquiry_comments) {
            this.enquiry_comments = enquiry_comments;
        }

        public String getDse_employee_id() {
            return dse_employee_id;
        }

        public void setDse_employee_id(String dse_employee_id) {
            this.dse_employee_id = dse_employee_id;
        }

        public String getExisting_vehicle() {
            return existing_vehicle;
        }

        public void setExisting_vehicle(String existing_vehicle) {
            this.existing_vehicle = existing_vehicle;
        }

        public String getTest_ride_required() {
            return test_ride_required;
        }

        public void setTest_ride_required(String test_ride_required) {
            this.test_ride_required = test_ride_required;
        }

        public String getTest_ride_required_time() {
            return test_ride_required_time;
        }

        public void setTest_ride_required_time(String test_ride_required_time) {
            this.test_ride_required_time = test_ride_required_time;
        }

        public String getTest_ride_taken() {
            return test_ride_taken;
        }

        public void setTest_ride_taken(String test_ride_taken) {
            this.test_ride_taken = test_ride_taken;
        }

        public String getTest_ride_taken_time() {
            return test_ride_taken_time;
        }

        public void setTest_ride_taken_time(String test_ride_taken_time) {
            this.test_ride_taken_time = test_ride_taken_time;
        }

        public String getEnquiry_source() {
            return enquiry_source;
        }

        public void setEnquiry_source(String enquiry_source) {
            this.enquiry_source = enquiry_source;
        }

        public String getAwareness_source() {
            return awareness_source;
        }

        public void setAwareness_source(String awareness_source) {
            this.awareness_source = awareness_source;
        }

        public String getOpinion_leader() {
            return opinion_leader;
        }

        public void setOpinion_leader(String opinion_leader) {
            this.opinion_leader = opinion_leader;
        }

        public String getFinancier() {
            return financier;
        }

        public void setFinancier(String financier) {
            this.financier = financier;
        }

        public String getSno() {
            return sno;
        }

        public void setSno(String sno) {
            this.sno = sno;
        }
    }
}
