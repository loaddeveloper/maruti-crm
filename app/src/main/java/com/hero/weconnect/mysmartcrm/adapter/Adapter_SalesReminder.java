package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.Calling_Sales_EnquirySharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.Sales_Followup_Activity;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CALL_PHONE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class Adapter_SalesReminder extends RecyclerView.Adapter<Adapter_SalesReminder.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    ArrayList<SalesEnquiryDataModel.DataBean> salesnquirydata;
    CustomButtonListener customListner;
    int size = 0;

    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    private Context context;
    CountDownTimer countDownTimer;

    final long defaultClickTimeGap=2000;
    long lastClickTime= SystemClock.elapsedRealtime();

    public Adapter_SalesReminder(Context context, CustomButtonListener customButtonListener, ArrayList<SalesEnquiryDataModel.DataBean> salesnquirydata) {

        this.context = context;
        this.customListner = customButtonListener;
        this.salesnquirydata = salesnquirydata;
        setHasStableIds(true);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {

        View v = LayoutInflater.from(context).inflate(R.layout.sales_data_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      //  holder.setIsRecyclable(true);



                common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(context);

                if (salesnquirydata.size() > 0) {
                    if (position == salesnquirydata.size()) {
                        holder.morebtn1.setVisibility(View.VISIBLE);
                        holder.main_ll.setVisibility(View.GONE);

                    } else {

                            SalesEnquiryDataModel.DataBean dataBean = salesnquirydata.get(position);
                            holder.morebtn1.setVisibility(View.GONE);
                            holder.main_ll.setVisibility(View.VISIBLE);

                            final String idd = String.valueOf(dataBean.getId());
                            holder.srno.setText("" + dataBean.getSno() + ".");
                            holder.customername.setText("" + dataBean.getCusotmer_first_and_last_name().toUpperCase());
                            holder.enquiry_no.setText("" + dataBean.getEnquiry_source().toUpperCase());
                            holder.customer_contact.setText("" + dataBean.getMobilenumber().toUpperCase());
                            holder.enquiry_opendate.setText("" + dateparse3(dataBean.getEnquiry_open_date().replace("T", " ")).substring(0, 11));
                            holder.enquiry_status.setText("EXCHANGE FLAG - " + dataBean.getExchange_required().toUpperCase());
                            holder.enquiry_model.setText("" + dataBean.getModel_interested_in().toUpperCase());
                            holder.calledstatus.setText("" + dataBean.getCalledstatus().toUpperCase());
                            holder.enqurysource.setText("FINANCE FLAG - " + dataBean.getFinance_required().toUpperCase());

                            countDownTimer = new CountDownTimer(2000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    final String idssss = common_calling_color_sharedPreferances.getmatserid();
                                    final String call = common_calling_color_sharedPreferances.getcallcurrentstatus();
                                    SharedPreferences chkfsss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                                    final String status = chkfsss.getString("Status", "");
                                    final String status1 = chkfsss.getString("Status1", "");
                                    final String status2 = chkfsss.getString("Status2", "");
                                    final String status3 = chkfsss.getString("Status3", "");
                                    SharedPreferences chkfss = context.getSharedPreferences(idd, Context.MODE_PRIVATE);
                                    final String icca = chkfss.getString("call", "");

                                    if (dataBean.getId().equals(idssss) && call.equals("s")) {
                                        holder.calledstatus.setText("Ringing");
                                        pos = position;
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                        holder.tv_hold.setVisibility(View.VISIBLE);

                                    } else if (dataBean.getId().equals(idssss) && dataBean.getCalledstatus().equals("CALL DONE") && call.equals("s")) {
                                        holder.calledstatus.setText("Ringing");
                                        pos = position;
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                        holder.tv_hold.setVisibility(View.VISIBLE);

                                    } else if ((icca.equals("CALL DONE")) && (!status.equals("booking")) && (!status2.equals("NextFollowUp")) && (!status3.equals("ClosureReason")) && (!status1.equals("rmkcolor"))) {
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                        holder.calledstatus.setText("CALL DONE");
                                        holder.tv_hold.setVisibility(View.GONE);


                                    } else if ((icca.equals("CALL DONE")) && (status1.equals("rmkcolor"))) {
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                        holder.calledstatus.setText("CALL DONE");
                                        holder.tv_hold.setVisibility(View.GONE);


                                        if (status.equals("booking") || status2.equals("NextFollowUp") || status3.equals("ClosureReason")) {
                                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                            holder.tv_hold.setVisibility(View.GONE);


                                        } else {
                                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.REDCALL));
                                            holder.calledstatus.setText("CALL DONE");
                                            holder.tv_hold.setVisibility(View.GONE);


                                        }
                                    } else if ((icca.equals("CALL DONE")) && (status.equals("booking"))) {

                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                        holder.calledstatus.setText("CALL DONE");
                                        holder.tv_hold.setVisibility(View.GONE);


                                    } else if ((icca.equals("CALL DONE")) && (status2.equals("NextFollowUp"))) {

                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                        holder.calledstatus.setText("CALL DONE");
                                        holder.tv_hold.setVisibility(View.GONE);


                                    } else if ((icca.equals("CALL DONE")) && (status3.equals("ClosureReason"))) {
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.YELLOWCALL));
                                        holder.calledstatus.setText("CALL DONE");
                                        holder.tv_hold.setVisibility(View.GONE);


                                    } else {
                                        holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.WHITECALL));
                                        holder.tv_hold.setVisibility(View.GONE);


                                    }


                                }

                                @Override
                                public void onFinish() {

                                    countDownTimer.cancel();

                                }
                            };

                            countDownTimer.start();

                            holder.whatsapp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendWhatsappMessage(position, "" + dataBean.getMobilenumber());
                                }
                            });
                            holder.textmessage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.sendtextmessage(position, "" + dataBean.getMobilenumber());
                                }
                            });
                            holder.main_ll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (SystemClock.elapsedRealtime() - lastClickTime < defaultClickTimeGap) {

                                        return;
                                    }

                                    lastClickTime = SystemClock.elapsedRealtime();

                                    customListner.getCustomerDetails(position, dataBean.getId(),
                                            dataBean.getCusotmer_first_and_last_name(),
                                            dataBean.getEnquirynumber().toUpperCase(),
                                            dataBean.getModel().toUpperCase(),
                                            dataBean.getEnquiry_open_date().toUpperCase()
                                            , dataBean.getEnquiry_status().toUpperCase(), dataBean.getMobilenumber(), holder.calledstatus.getText().toString());
                                }
                            });
                            holder.main_ll.setOnLongClickListener(new View.OnLongClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public boolean onLongClick(View v) {
                                    CommonVariables.Is_LongPress = true;


                                    common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                    String uid = UUID.randomUUID().toString();
                                    CommonVariables.mobilenumber=dataBean.getMobilenumber();
                                    // Log.e("Longpresshid"," "+uid +" \n"+dataBean.getId());
                                    SharedPreferences prefs = context.getSharedPreferences(dataBean.getId(), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edits = prefs.edit();
                                    edits.putString(dataBean.getId(), uid);
                                    edits.commit();
                                    if (common_calling_color_sharedPreferances.getcallcurrentstatus() != null && common_calling_color_sharedPreferances.getcallcurrentstatus().equals("s")) {
                                        Toasty.warning(context, "Call is Currently Onging", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (context.checkSelfPermission(CALL_PHONE) == PERMISSION_GRANTED) {

                                            // Create the Uri from phoneNumberInput
                                            //Uri uri = Uri.parse("tel:" + CommonVariables.SALES_Testingnumber);
                                           Uri uri = Uri.parse("tel:" + "+91"+ dataBean.getMobilenumber());

                                            // Start call to the number in input
                                            context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                                            holder.calledstatus.setText("Ringing");
                                            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.GREENCALL));
                                            common_calling_color_sharedPreferances.setPosition(String.valueOf(position));
                                            ((Sales_Followup_Activity) context).setIndex(position + 1, true, uid);
                                            ((Sales_Followup_Activity) context).uiSetup(false,false,false,false,false);


                                            common_calling_color_sharedPreferances.setcallcurrentstatus("s");


                                        } else {
                                            // Request permission to call
                                            ActivityCompat.requestPermissions((Activity) context, new String[]{CALL_PHONE}, REQUEST_PERMISSION);
                                        }
                                    }
                                    return false;
                                }
                            });
                            holder.historyybtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.getHistoryDetails(position);
                                }
                            });
                            holder.moredetailsbtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    customListner.getMoreDetails(position);
                                }
                            });


                            holder.tv_hold.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    holder.tv_hold.setVisibility(View.GONE);
                                    holder.tv_unhold.setVisibility(View.VISIBLE);
                                    customListner.holdCall(position, dataBean.getMobilenumber());
                                }

                            });

                            holder.tv_unhold.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    holder.tv_hold.setVisibility(View.VISIBLE);
                                    holder.tv_unhold.setVisibility(View.GONE);
                                    customListner.unHoldCall(position, dataBean.getMobilenumber());
                                }

                            });

                        }


                    }


                    if (((Sales_Followup_Activity) context).isCalldoneChecked) {
                        holder.morebtn1.setVisibility(View.GONE);
                    }


                    // load more data
                    holder.morebtn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (context instanceof Sales_Followup_Activity) {
                                ((Sales_Followup_Activity) context).moreData();
                            }
                        }
                    });

    }
             public TextView getMoreButton(TextView textView)
             {
                 return textView;
             }
    public void updateList(ArrayList<SalesEnquiryDataModel.DataBean> list11) {
        salesnquirydata = list11;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        if (salesnquirydata.size() >= 10) {
            if ((salesnquirydata.size() % 10) == 0) {
                size = salesnquirydata.size() + 1;

            } else {
                size = salesnquirydata.size();

            }
        } else {
            size = salesnquirydata.size();
        }


        return size;
    }

    public void setCustomButtonListner(CustomButtonListener listener) {
        this.customListner = listener;
    }

    public interface CustomButtonListener {


        void getCustomerDetails(int position, String matserid, String customername, String enuiryno, String model, String enquiryopendate, String enquirystatus, String mobileno,String calledStatus);

        void getHistoryDetails(int position);

        void getMoreDetails(int position);

        void sendWhatsappMessage(int position, String mobileno);

        void sendtextmessage(int position, String mobileno);

        void holdCall(int position, String contatcnumber);

        void unHoldCall(int position, String contatcnumber);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView srno, idddata,enqurysource, tv_hold, tv_unhold, customername, customer_contact, enquiry_no, enquiry_opendate, enquiry_status, enquiry_model, calledstatus, morebtn1;
        public ImageView moredetailsbtn, historyybtn, whatsapp, textmessage;
        public LinearLayout main_ll;

        public ViewHolder(View rootView) {
            super(rootView);
            this.srno = rootView.findViewById(R.id.srno);
            this.idddata = rootView.findViewById(R.id.idddata);
            this.customername = rootView.findViewById(R.id.customerName);
            this.customer_contact = rootView.findViewById(R.id.mobileno);
            this.enquiry_no = rootView.findViewById(R.id.enquiryno);
            this.enquiry_opendate = rootView.findViewById(R.id.enquiryopendate);
            this.enquiry_status = rootView.findViewById(R.id.enquirystatus);
            this.enquiry_model = rootView.findViewById(R.id.model);
            this.calledstatus = rootView.findViewById(R.id.cstatus);
            this.morebtn1 = rootView.findViewById(R.id.morebtn1);
            this.enqurysource = rootView.findViewById(R.id.enquirysource);
            this.moredetailsbtn = rootView.findViewById(R.id.moredetailsbtn);
            this.historyybtn = rootView.findViewById(R.id.historyybtn);
            this.tv_hold = rootView.findViewById(R.id.tv_hold);
            this.tv_unhold = rootView.findViewById(R.id.tv_unhold);
            this.whatsapp = rootView.findViewById(R.id.whatsapp);
            textmessage = rootView.findViewById(R.id.textmessage);

            this.main_ll = rootView.findViewById(R.id.main_ll);




        }
    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }


}