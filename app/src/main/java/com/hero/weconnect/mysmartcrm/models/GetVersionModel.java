package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class GetVersionModel extends SuperClassCastBean {
    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("currentversion")
        private String currentversion;
        @SerializedName("minimumversion")
        private String minimumversion;
        @SerializedName("ramstorage")
        private String ramstorage;
        @SerializedName("requiredstorage")
        private String requiredstorage;
        @SerializedName("url")
        private String url;

        public String getCurrentversion() {
            return currentversion;
        }

        public void setCurrentversion(String currentversion) {
            this.currentversion = currentversion;
        }

        public String getMinimumversion() {
            return minimumversion;
        }

        public void setMinimumversion(String minimumversion) {
            this.minimumversion = minimumversion;
        }

        public String getRamstorage() {
            return ramstorage;
        }

        public void setRamstorage(String ramstorage) {
            this.ramstorage = ramstorage;
        }

        public String getRequiredstorage() {
            return requiredstorage;
        }

        public void setRequiredstorage(String requiredstorage) {
            this.requiredstorage = requiredstorage;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }




}
