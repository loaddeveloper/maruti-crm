package com.hero.weconnect.mysmartcrm.receiver;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.NetworkState;

public class NetworkReceiver extends BroadcastReceiver {
    static String  status =null;
    AlertDialog dialog_network_only;
    @Override
    public void onReceive(Context context, Intent intent) {
        status = NetworkState.getConnectivityStatusString(context);
        Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        if(status == "Wifi enabled"){
            //your code when wifi enable
            if (dialog_network_only != null)
                dialog_network_only.dismiss();
        }
        else if(status=="Mobile data enabled"){
            if (dialog_network_only != null)
                dialog_network_only.dismiss();
            //your code when TYPE_MOBILE network enable
        }
        else if(status=="Not connected to Internet"){
            //your code when no network connected

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View v = LayoutInflater.from(context).inflate(R.layout.layout_no_internet, null, false);
            ImageView iv_btn_close  = v.findViewById(R.id.iv_btn_close);
            iv_btn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog_network_only.dismiss();
                }
            });

            builder.setView(v);
            dialog_network_only = builder.create();
            dialog_network_only.show();
            dialog_network_only.setCancelable(false);
        }
    }


}
