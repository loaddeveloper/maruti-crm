package com.hero.weconnect.mysmartcrm.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.an.deviceinfo.device.DeviceInfo;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.hero.weconnect.mysmartcrm.CallUtils.MyAccessibilityService;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiConstant;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiController;
import com.hero.weconnect.mysmartcrm.Retrofit.ApiResponseListener;
import com.hero.weconnect.mysmartcrm.Retrofit.CallingSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Calling_Sales_EnquirySharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonCallingSharedPrefrences;
import com.hero.weconnect.mysmartcrm.Retrofit.CommonSharedPref;
import com.hero.weconnect.mysmartcrm.Retrofit.Common_Calling_Color_SharedPreferances;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.Utils.CustomDialog;
import com.hero.weconnect.mysmartcrm.Utils.ExceptionHandler;
import com.hero.weconnect.mysmartcrm.Utils.NetworkCheckerService;
import com.hero.weconnect.mysmartcrm.Utils.RequestPermissionHandler;
import com.hero.weconnect.mysmartcrm.adapter.Custom_Swip_Adapter;
import com.hero.weconnect.mysmartcrm.models.CallingSharedprefrencesModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.GetVersionModel;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;

import org.apache.commons.io.FileUtils;

public class DashboardActivity extends AppCompatActivity implements ApiResponseListener, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    // Class Variable Declartion

    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
    public static boolean callsk = false;
    private final String PACKAGE_NAME = "com.hero.weconnect.mysmartcrm";
    RequestPermissionHandler mRequestPermissionHandler;
    DrawerLayout drawerLayout;
    NavigationView nav_view;
    AppBarLayout Appbar;
    Custom_Swip_Adapter mAdapter;
    ViewPager viewPager;
    CardView custom_card, sr_card, sales_card, booking_card, thankucard, missed_card, nonturn_card, thankucard1, card_psf_reminder;
    Handler handler = new Handler();
    boolean permission = false;
    TextView br_textview;
    CallingSharedPref callingSharedPref;
    CommonSharedPref commonSharedPref;
    Calling_Sales_EnquirySharedPref calling_sales_enquirySharedPref;
    CommonCallingSharedPrefrences commonCallingSharedPrefrences;
    NetworkCheckerService networkCheckerService;
    String token = "";
    Common_Calling_Color_SharedPreferances common_calling_color_sharedPreferances;
    ApiController apiController;
    CustomDialog customDialog;
    private PermissionRequestErrorListener errorListener;
    private MultiplePermissionsListener allPermissionsListener;
    private TextView nav_header_user_name, nav_header_versioncode, psf_commingsoon, tv_br_commingsoon, non_turnup_commingsoon;
    NavigationView arcNavigationView;
    String App_Identify;
    Dialog dialogset, dialogsetnew;
    public HashMap<String, String> params = new HashMap<>();
    SharedPreferences appname;
    SharedPreferences.Editor appnameeditor;
    Menu menu;
    MenuItem nav_dashboard, customcall, nav_expected_date_report,brreport;
    boolean token_sales = false;
    DeviceInfo deviceInfo;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_dashboard);


        viewPager = findViewById(R.id.viewpager);
        mAdapter = new Custom_Swip_Adapter(this);
        viewPager.setAdapter(mAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 10-1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);


        AppCenter.start(getApplication(), "f01eac54-241c-474f-9632-317509be0c80", Analytics.class, Crashes.class);
        //    Toast.makeText(this, new Common().getTest(), Toast.LENGTH_LONG).show();
        apiController = new ApiController(this, this);
        customDialog = new CustomDialog(this);
        commonSharedPref = new CommonSharedPref(this);
        commonSharedPref.setAllClear();
        appname = getSharedPreferences("appname", MODE_PRIVATE);
        appnameeditor = appname.edit();
        br_textview = findViewById(R.id.br_textview);
        non_turnup_commingsoon = findViewById(R.id.non_turnup_commingsoon);

        deviceInfo = new DeviceInfo(this);


        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion < Build.VERSION_CODES.O){
            Android_version_check(Build.VERSION.RELEASE);
            //return;

        } else{

        }


        File dir = new File((Environment.getExternalStorageDirectory().getAbsolutePath() + "/Smart CRM"));
//        if (dir.exists()) dir.delete();
        try {
            FileUtils.deleteDirectory(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        networkCheckerService = new NetworkCheckerService(DashboardActivity.this, this);
        initView();



        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        ////Log.e("IntentValue","value"+getIntent().getStringExtra("role"));

        if(getIntent() != null && getIntent().getStringExtra("token_new") != null) {

            CommonVariables.token=getIntent().getStringExtra("token_new");
            Log.d("token_new ",CommonVariables.token);
        }
        else {
            try {

                if (getIntent() != null && getIntent().getStringExtra("role").equals("null")) {
                    showAlertDialog();
                    //   Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();

                } else {
                    //Toast.makeText(this, "2", Toast.LENGTH_SHORT).show();

                    if (dialogset != null) dialogset.dismiss();
                }
            } catch (Exception e) {
                // Toast.makeText(this, "3", Toast.LENGTH_SHOR
                //
                //    T).show();

                //  showAlertDialog();
                e.printStackTrace();
            }
        }





        if (getIntent() != null && getIntent().getStringExtra("role") != null) {

            CommonVariables.dealercode = getIntent().getStringExtra("dealercode");
            CommonVariables.token = getIntent().getStringExtra("token");
            CommonVariables.role = getIntent().getStringExtra("role");
            CommonVariables.pagename = getIntent().getStringExtra("pagename");
            CommonVariables.UserId = getIntent().getStringExtra("userid");
            CommonVariables.userName = getIntent().getStringExtra("userid");
            CommonVariables.APP = getIntent().getStringExtra("app");
            CommonVariables.LOC = getIntent().getStringExtra("Division");
            customDialog.show();
            //Log.e("TokenDashBoard","APP "+CommonVariables.token);



            nav_header_user_name.setText("Test");





//            dealercode-  10800
//            userid - TEST10800
//            token - base64 token
//            role - DSE / DSM
//            pagename - followup_today( Today's followup ) / followup_pending ( Pending followup ) / followup_all(Side menu and We connect icon)
//            app - sales
//            Division - blank ("")



            if (CommonVariables.LOC.equals(""))
            {
                CommonVariables.LOC= "BLANK";
            }

            if (CommonVariables.APP.toLowerCase().trim().equals("sales"))
            {

                App_Identify = "HSA";
                //  App_Identify = "HSAPT";
                appnameeditor.putString("appname",App_Identify);
                appnameeditor.commit();
                tv_br_commingsoon.setVisibility(View.VISIBLE);
            }
            else if(CommonVariables.APP.toLowerCase().trim().equals("service"))
            {
                App_Identify = CommonVariables.App_IdentifyService;
                appnameeditor.putString("appname",App_Identify);
                appnameeditor.commit();
            }


            //Token Send Values in Android to DB API
            String WeConnect_User_Name = "D_" + CommonVariables.dealercode + "|" + "U_" + CommonVariables.UserId + "|" + "R_" + CommonVariables.role+"|"+"L_"+CommonVariables.LOC+"|"+"A_"+App_Identify;
            String WeConnect_Password = "T_" + CommonVariables.token + "|" + "P_" + CommonVariables.pagename;

          //  Log.e("WeConnectUserName","UserName "+WeConnect_User_Name);
           // Log.e("WeConnectUserPassword","Password "+WeConnect_Password);
            CommonVariables.WeConnect_User_Name = WeConnect_User_Name;
            CommonVariables.WeConnect_Password = WeConnect_Password;
            get_Load_Infotech_Token("" + WeConnect_User_Name, "" + WeConnect_Password);


            Map<String,String> properties = new HashMap<>();
            properties.put("UserName", ""+CommonVariables.UserId);
            properties.put("DealerCode", ""+CommonVariables.dealercode);
            properties.put("ActivityName", "Dashboard");
            properties.put("App", ""+App_Identify);
            properties.put("Role", ""+CommonVariables.role);
            Analytics.trackEvent("Dashboard",properties);




            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {
                nav_dashboard.setTitle("Alloted Data Report");
                nav_dashboard.setVisible(false);

                nav_expected_date_report.setTitle("Sales Expected Report");
                nav_expected_date_report.setVisible(false);
                sr_card.setVisibility(View.VISIBLE);
                card_psf_reminder.setVisibility(View.VISIBLE);
                sales_card.setVisibility(View.GONE);
                custom_card.setVisibility(View.VISIBLE);
                nonturn_card.setVisibility(View.VISIBLE);
                thankucard1.setVisibility(View.GONE);
                thankucard.setVisibility(View.GONE);
                CommonVariables.WECONNECT_APP_ROLE = ApiConstant.SR;
                br_textview.setText("Booking Reminder");




            } else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {

                sr_card.setVisibility(View.GONE);
                card_psf_reminder.setVisibility(View.GONE);
                sales_card.setVisibility(View.VISIBLE);
                custom_card.setVisibility(View.GONE);
                thankucard1.setVisibility(View.VISIBLE);
                thankucard.setVisibility(View.GONE);
                nonturn_card.setVisibility(View.GONE);
                CommonVariables.WECONNECT_APP_ROLE = ApiConstant.ENQUIRY;
                br_textview.setText("Sales Booking Reminder");

            }


        }

        Bundle bundle = getIntent().getExtras();
        if ( getIntent().getStringExtra("error") != null && bundle != null && commonSharedPref.getLoginData() != null) {
            String value = bundle.getString("error");
            String tokenexception = bundle.getString("token");
            HashMap<String, String> exceptionparams = new HashMap<>();
            exceptionparams.put("Brand", Build.BRAND);
            exceptionparams.put("Model", Build.MODEL);
            exceptionparams.put("ExceptionLog", value);
            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
            apiController.addException(token, exceptionparams);
        }

        // Init View Function Call
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {




                if (handleButtonClicked()) {


                } else {
                    handleButtonClicked();
                }
                checkPermission();
//                if (!isAccessibilitySettingsOn(getApplicationContext())) {
//                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                }

            }
        }, 1000);


    }


    // View Intilization Function of Activity Class
    private void initView() {

        drawerLayout = findViewById(R.id.drawer_layout);
        nav_view = findViewById(R.id.nav_view);
        nav_view.setNavigationItemSelectedListener(this);
        View headerview = nav_view.getHeaderView(0);
        Appbar = findViewById(R.id.appbar);
        sr_card = (findViewById(R.id.sr_cardview));
        card_psf_reminder=(findViewById(R.id.card_psf_reminder));
        custom_card = (findViewById(R.id.custom_cardview));
        sales_card = (findViewById(R.id.card_salesfollowup));
        booking_card = (findViewById(R.id.booking_cardview));
        thankucard = (findViewById(R.id.card_thanku));
        thankucard1 = (findViewById(R.id.card_thanku1));
        missed_card = (findViewById(R.id.missed_cardview));
        nonturn_card = (findViewById(R.id.nonturn_cardview));
        tv_br_commingsoon = (findViewById(R.id.br_comming_soon));
        psf_commingsoon = (findViewById(R.id.psf_commingsoon));


        arcNavigationView = findViewById(R.id.nav_view);
        arcNavigationView.setNavigationItemSelectedListener(this);
        View headerView = arcNavigationView.getHeaderView(0);
        nav_header_user_name = headerView.findViewById(R.id.tv_username);
        nav_header_versioncode = headerView.findViewById(R.id.versioncode);
        sr_card.setOnClickListener(this);
        card_psf_reminder.setOnClickListener(this);
        custom_card.setOnClickListener(this);
        sales_card.setOnClickListener(this);
        booking_card.setOnClickListener(this);
        thankucard.setOnClickListener(this);
        thankucard1.setOnClickListener(this);
        missed_card.setOnClickListener(this);
        nonturn_card.setOnClickListener(this);

        menu =nav_view.getMenu();

        nav_dashboard = menu.findItem(R.id.nav_dse_report);
        nav_expected_date_report = menu.findItem(R.id.nav_expected_report);

        customcall = menu.findItem(R.id.customcalling_report);
        brreport = menu.findItem(R.id.br_report);



        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);

        callingSharedPref = new CallingSharedPref(this);
        commonSharedPref = new CommonSharedPref(this);
        calling_sales_enquirySharedPref = new Calling_Sales_EnquirySharedPref(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        commonCallingSharedPrefrences = new CommonCallingSharedPrefrences(this);
        common_calling_color_sharedPreferances = new Common_Calling_Color_SharedPreferances(this);
        callingSharedPref.setAllClearSRCallingData();
        calling_sales_enquirySharedPref.setAllClearSRCallingData();
        mRequestPermissionHandler = new RequestPermissionHandler();


        // Call Dialer Packages List Function Call
        getPackagesList();

        PackageManager manager = DashboardActivity.this.getPackageManager();

        try {
            PackageInfo info = manager.getPackageInfo(DashboardActivity.this.getPackageName(), PackageManager.GET_ACTIVITIES);
            String versioncode = info.versionName;
            //Log.e("VresionCode","Version"+versioncode);
            nav_header_versioncode.setText("v"+versioncode);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    //Navigation Handler Function
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {




        int itemId = menuItem.getItemId();




        if (itemId == R.id.nav_setting) {
            if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
            {
                Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
            }else
            {
                startActivity(new Intent(this, SettingActivity.class));

            }
        }
        else  if (itemId == R.id.nav_report) {
            // Toast.makeText(this, ""+CommonVariables.APP, Toast.LENGTH_SHORT).show();
            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

                startActivity(new Intent(this, ReportActivitySR.class));
                //showComingSoonPopUp();


            }
            else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {

                startActivity(new Intent(this, ReportActivitySales.class));


            }


        }


        else if (itemId == R.id.nav_dse_report)
        {



            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {


                nav_dashboard.setVisible(true);

                startActivity(new Intent(this, SR_Data_Alloted_Activity.class));
                //showComingSoonPopUp();



            }
            else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {

                startActivity(new Intent(this, DsetodayReport.class));
            }
        }

        else if (itemId == R.id.nav_expected_report)
        {



            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {




                //startActivity(new Intent(this, SR_Data_Alloted_Activity.class));
                //showComingSoonPopUp();



            }
            else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {

                startActivity(new Intent(this, Sales_Booking_Report.class));

            }
        }


        else if (itemId == R.id.customcalling_report)
        {

            showComingSoonPopUp();


            /*if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {




                startActivity(new Intent(this, CustomCallingReportActivity.class));


            }else
            {


            }*/

        }

        else if (itemId == R.id.br_report)
        {

            showComingSoonPopUp();

            /*if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {




                startActivity(new Intent(this, ReportActivityBR.class));



            }else
            {


            }
*/
        }

        else if (itemId == R.id.nav_logout) {

        }
        else if (itemId == R.id.nav_video) {
            showComingSoonPopUp();

           // startActivity(new Intent(this,VideoActivity.class));

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    void getmsg()
    {
        if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
        {
            Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();            return;
        }
    }
    //Coming Soon Popup Function
    public void showComingSoonPopUp() {
        Dialog shippingDialog = new Dialog(DashboardActivity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.coming_soon_popup);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(true);
        shippingDialog.setCanceledOnTouchOutside(true);
        shippingDialog.show();
    }


    // Click Listener of ALL Cards in DashBoard
    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == R.id.booking_cardview) {


            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }

                 showComingSoonPopUp();
                tv_br_commingsoon.setVisibility(View.VISIBLE);

               /* if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }

                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
                callingSharedprefrencesModel.setPosition("0");
                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
                callsk = true;
                common_calling_color_sharedPreferances.setPosition("0");
                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
                //Intent to open Actctivity
                startActivity(new Intent(this, B_R_Activity.class));*/
            }else {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }

                    showComingSoonPopUp();

//                if (!isAccessibilitySettingsOn(getApplicationContext())) {
//                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                    return;
//                }
//                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
//                callingSharedprefrencesModel.setPosition("0");
//                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
//                callsk = true;
//
//                tv_br_commingsoon = (findViewById(R.id.br_comming_soon));
//
//                tv_br_commingsoon.setVisibility(View.GONE);
//
//                common_calling_color_sharedPreferances.setPosition("0");
//                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
//                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
//                //Intent to open Actctivity
//                startActivity(new Intent(this, ExpectedDateOfPurchase_Activity.class));
            }
        }
        if (id == R.id.custom_cardview) {


            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

               /* if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }*/

                showComingSoonPopUp();
                /*tv_br_commingsoon.setVisibility(View.GONE);

                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
                callingSharedprefrencesModel.setPosition("0");
                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
                callsk = true;
                common_calling_color_sharedPreferances.setPosition("0");
                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
                //Intent to open Actctivity
                startActivity(new Intent(this, Drawer_Activity.class));*/
            }
            else {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }

                showComingSoonPopUp();

                if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }
//                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
//                callingSharedprefrencesModel.setPosition("0");
//                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
//                callsk = true;
//
//                tv_br_commingsoon = (findViewById(R.id.br_comming_soon));
//
//                tv_br_commingsoon.setVisibility(View.GONE);
//
//                common_calling_color_sharedPreferances.setPosition("0");
//                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
//                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
//                //Intent to open Actctivity
//                startActivity(new Intent(this, ExpectedDateOfPurchase_Activity.class));
            }
        }
        else if (id == R.id.card_thanku) {

            showComingSoonPopUp();

        }
        else if (id == R.id.card_thanku1) {
            if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();                    return;
                }

                showComingSoonPopUp();


//                if (!isAccessibilitySettingsOn(getApplicationContext())) {
//                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                    return;
//                }
//                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
//                callingSharedprefrencesModel.setPosition("0");
//                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
//                callsk = true;
//                common_calling_color_sharedPreferances.setPosition("0");
//                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
//                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
//                //Intent to open Actctivity
//                startActivity(new Intent(this, Sales_FinancialCalls_Activity.class));
            }else {
                showComingSoonPopUp();

            }
        }
        else if (id == R.id.missed_cardview) {
            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

                showComingSoonPopUp();
                /*if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();                    return;
                }
                if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }
                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
                callingSharedprefrencesModel.setPosition("0");
                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
                callsk = true;
                common_calling_color_sharedPreferances.setPosition("0");
                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
                //Intent to open Actctivity
                startActivity(new Intent(this, MissedCallActivity_SR.class));*/
            }
            else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2))
            {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }
                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
                callingSharedprefrencesModel.setPosition("0");
                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
                callsk = true;
                common_calling_color_sharedPreferances.setPosition("0");
                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
                //Intent to open Actctivity
                startActivity(new Intent(this, MissedCallActivity_ENQUIRY.class));
            }


        }
        else if (id == R.id.nonturn_cardview) {
            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }
                non_turnup_commingsoon.setVisibility(View.VISIBLE);
                showComingSoonPopUp();


//                if (!isAccessibilitySettingsOn(getApplicationContext())) {
//                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                    return;
//                }
//                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
//                callingSharedprefrencesModel.setPosition("0");
//                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
//                callsk = true;
//                common_calling_color_sharedPreferances.setPosition("0");
//                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
//                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
//                //Intent to open Actctivity
//                startActivity(new Intent(this, NonTurnUpCallsActivity.class));
            }else {
                showComingSoonPopUp();

            }

        }
        else if (id == R.id.sr_cardview || id == R.id.sr_icon || id == R.id.sr_textview) {

            if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
            {
                Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!isAccessibilitySettingsOn(getApplicationContext())) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                return;
            }
            CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
            callingSharedprefrencesModel.setPosition("0");
            commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
            callsk = true;
            common_calling_color_sharedPreferances.setPosition("0");
            common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
            common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
            startActivity(new Intent(this, S_R_Activity.class));
        }
        else if (id == R.id.card_psf_reminder || id == R.id.psf_icon || id == R.id.psf_text) {

            showComingSoonPopUp();

//            if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
//            {
//                Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
//                return;
//            }
//
//            if (!isAccessibilitySettingsOn(getApplicationContext())) {
//                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//                return;
//           }
//            CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
//            callingSharedprefrencesModel.setPosition("0");
//            commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
//            callsk = true;
//            common_calling_color_sharedPreferances.setPosition("0");
//            common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
//            common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
//            startActivity(new Intent(this, PSF_Reminder_Activity.class));
        }





        else if (id == R.id.card_salesfollowup) {
           /* get_Load_Infotech_Token_Sales(CommonVariables.WeConnect_User_Name,CommonVariables.WeConnect_Password);
            token_sales = true;
            customDialog.show();*/

            if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
            {
                Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!isAccessibilitySettingsOn(getApplicationContext())) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                return;
            }
            CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
            callingSharedprefrencesModel.setPosition("0");
            commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
            callsk = true;
            common_calling_color_sharedPreferances.setPosition("0");
            common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
            common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
            startActivity(new Intent(this, Sales_Followup_Activity.class));


        }

    }

    // RunTime Permission Handle Function

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRequestPermissionHandler.onRequestPermissionsResult(requestCode, permissions,
                grantResults);
    }

    private boolean handleButtonClicked() {


//        Intent goToSettings = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//        goToSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
//        startActivity(goToSettings);





        mRequestPermissionHandler.requestPermission(this, new String[]{


                Manifest.permission.SYSTEM_ALERT_WINDOW,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.BIND_ACCESSIBILITY_SERVICE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.PROCESS_OUTGOING_CALLS,
                Manifest.permission.ANSWER_PHONE_CALLS,
                Manifest.permission.FOREGROUND_SERVICE
        }, 123, new RequestPermissionHandler.RequestPermissionListener() {
            @Override
            public void onSuccess() {

                permission = true;

            }

            @Override
            public void onFailed() {
                permission = false;
                // Toast.makeText(SplashController.this, "request permission failed", Toast.LENGTH_SHORT).show();
            }
        });

        return permission;

    }

    // Application OverLay Permission

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(DashboardActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            } else {

            }
        }
    }

    // Get Pacakges List
    public void getPackagesList() {

        commonSharedPref.clearpackages();
        List<String> packageNames = new ArrayList<>();
        // Declare action which target application listen to initiate phone call
        final Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        // Query for all those applications
        List<ResolveInfo> resolveInfos = getApplicationContext().getPackageManager().queryIntentActivities(intent, 0);
        // Read package name of all those applications
        for (ResolveInfo resolveInfo : resolveInfos) {
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            packageNames.add(activityInfo.applicationInfo.packageName);
        }

        String lastpackagename = packageNames.get(0);
        commonSharedPref.setPackages(lastpackagename);


    }

    @Override
    public void onBackPressed() {

        String packagename = commonSharedPref.getPackagesList();

        TelecomManager systemService1 = DashboardActivity.this.getSystemService(TelecomManager.class);
        if (systemService1 != null && !systemService1.getDefaultDialerPackage().equals(packagename))
        {

            if (Build.VERSION.SDK_INT <= 28) {
                TelecomManager systemService = DashboardActivity.this.getSystemService(TelecomManager.class);
                if (systemService != null && !systemService.getDefaultDialerPackage().equals(packagename)) {
                    startActivity((new Intent(ACTION_CHANGE_DEFAULT_DIALER)).putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packagename));
                }


            }
            else if (Build.MODEL.equals("Redmi 7A")) {
                Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                startActivity(i);

            }
            else {
                Intent i = new Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                startActivity(i);

            }

            Toast.makeText(this, "First Change your Default Dialer", Toast.LENGTH_LONG).show();

        }
        else
        {

            super.onBackPressed();

        }




    }



    @Override
    public void onSuccess(String beanTag, SuperClassCastBean superClassCastBean) {

        if (ApiConstant.TOKEN_TAG.matches(beanTag)) {
            TokenModel tokenModel = (TokenModel) superClassCastBean;
            commonSharedPref.setLoginData(tokenModel);
            //Log.e("Token","token"+tokenModel.getAccess_token());
            Toast.makeText(this, "Successfully", Toast.LENGTH_SHORT).show();

            token = "bearer " + commonSharedPref.getLoginData().getAccess_token();
            CommonVariables.New_token_for_template = token;

            apiController.getVersionData(token);
            customDialog.dismiss();

            if (token_sales)
            {


                if(commonSharedPref.getLoginData() == null || commonSharedPref.getLoginData().getAccess_token() == null)
                {
                    Toast.makeText(this, "Retry After Some Time !!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!isAccessibilitySettingsOn(getApplicationContext())) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                    return;
                }
                CallingSharedprefrencesModel callingSharedprefrencesModel = new CallingSharedprefrencesModel();
                callingSharedprefrencesModel.setPosition("0");
                commonCallingSharedPrefrences.setCallingData(callingSharedprefrencesModel);
                callsk = true;
                common_calling_color_sharedPreferances.setPosition("0");
                common_calling_color_sharedPreferances.setContains(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCES);
                common_calling_color_sharedPreferances.setContainsSheer(ApiConstant.COMMONCALLINGCOLORSHAREDPREANCESSHREE);
                startActivity(new Intent(this, Sales_Followup_Activity.class));
            }


        } else if (beanTag.matches(ApiConstant.ADDEXCEPTION)) {
            CommonModel commonModel = (CommonModel) superClassCastBean;
            if (commonModel.getMessage().toLowerCase().equals("sucess")) {
                //  Toast.makeText(this, "Sucess", Toast.LENGTH_SHORT).show();
            }
        }
        else if (beanTag.matches(ApiConstant.GETVERSION)) {
            GetVersionModel getVersionModel = (GetVersionModel) superClassCastBean;
            PackageManager manager = DashboardActivity.this.getPackageManager();

            try {
                PackageInfo info = manager.getPackageInfo(DashboardActivity.this.getPackageName(), PackageManager.GET_ACTIVITIES);
                String versioncode = info.versionName;
                //Log.e("VresionCode","Version"+versioncode);
                double abcd;
                abcd = Double.valueOf(getVersionModel.getData().get(0).getCurrentversion());

                if(getVersionModel.getData().get(0).getRamstorage() != null)
                {

                    CommonVariables.RAM = Float.valueOf(getVersionModel.getData().get(0).getRamstorage());
                    CommonVariables.STORAGE = Float.valueOf(getVersionModel.getData().get(0).getRequiredstorage());
                    CommonVariables.AppCenter_URL = getVersionModel.getData().get(0).getUrl();


                }


//                if(convertToGb(deviceInfo.getAvailableInternalMemorySize())>=CommonVariables.STORAGE && convertToGb(deviceInfo.getTotalRAM())>=CommonVariables.RAM)
//                {
//                  //  Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//                    showStorageAlertDialog();
//                }

                //Log.e("CuurentVersion","Version "+CommonVariables.RAM+"    "+  CommonVariables.STORAGE);

                if (abcd<=Double.valueOf(versioncode)) {


                }
                else {

                    Dialog shippingDialog = new Dialog(DashboardActivity.this);
                    shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    shippingDialog.setContentView(R.layout.forceupdatepopup);
                    shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    shippingDialog.setCancelable(false);
                    shippingDialog.setCanceledOnTouchOutside(false);
                    Button button_install = (Button)shippingDialog.findViewById(R.id.buttoninstall);
                    shippingDialog.show();
                    button_install.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            shippingDialog.dismiss();
                            //String url = "https://install.appcenter.ms/users/deepanshu.sharma/apps/WeConnect/distribution_groups/publictesting";
                           // String url = "https://install.appcenter.ms/orgs/heromobility/apps/weconnect/distribution_groups/publictesting";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(CommonVariables.AppCenter_URL));
                            finishAffinity();
                            startActivity(i);
                        }
                    });




                }


            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        }
        token = "bearer " + commonSharedPref.getLoginData().getAccess_token();

    }

    @Override
    public void onFailure(String msg) {
        customDialog.dismiss();
    }

    @Override
    public void onError(String msg) {
        try {
            customDialog.dismiss();
        } catch (final IllegalArgumentException e) {
            // Do nothing.
        } catch (final Exception e) {
            // Do nothing.
        } finally {
            customDialog = null;
        }

    }

    @Override
    protected void onDestroy() {

        if (commonSharedPref.getLoginData() != null) {


            if (CommonVariables.APP.equals(CommonVariables.APP_Role_1))
            {
                params = new HashMap<>();
                params.put("Type", ApiConstant.SR);
                params.put("UserName", CommonVariables.UserId);
                apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                networkCheckerService.UnregisterChecker();
            }
            else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2))
            {
                params = new HashMap<>();
                params.put("UserName", CommonVariables.UserId);
                apiController.deAllocation("bearer " + commonSharedPref.getLoginData().getAccess_token(),params);
                networkCheckerService.UnregisterChecker();

            }


        }

        super.onDestroy();
        finishAndRemoveTask();
        finish();


    }




    // Get Token  for APIS Aunthentication
    public String get_Load_Infotech_Token(String username, String password)
    {
        apiController.getToken(username, password);
        return username;
    }


    // Get Token  for APIS Aunthentication
    public void get_Load_Infotech_Token_Sales(String username, String password)
    {
        token_sales = false;
        apiController.getToken(username, password);

    }



    // On Start to Clear Shared Preferances Values
    @Override
    protected void onStart() {


        super.onStart();


        common_calling_color_sharedPreferances.clear__start_stop_status();
        common_calling_color_sharedPreferances.clearcommoncallingsharedpreferances();
        common_calling_color_sharedPreferances.clearposition();
        common_calling_color_sharedPreferances.clearSheer();
        SharedPreferences prefposition = getSharedPreferences("position", Context.MODE_PRIVATE);
        SharedPreferences prefceer = getSharedPreferences("ceer", Context.MODE_PRIVATE);
        SharedPreferences prefcall = getSharedPreferences("call", Context.MODE_PRIVATE);
        SharedPreferences CallingColor = getSharedPreferences("CallingColor", Context.MODE_PRIVATE);
        SharedPreferences.Editor CallingColoreditor = CallingColor.edit();
        SharedPreferences.Editor editpostion = prefposition.edit();
        SharedPreferences.Editor editceer = prefceer.edit();
        SharedPreferences.Editor editcall = prefcall.edit();
        editpostion.clear();
        editcall.clear();
        editceer.clear();
        CallingColoreditor.clear();
        CallingColoreditor.commit();
        editcall.commit();
        editceer.commit();
        editpostion.commit();

        SharedPreferences historysave = getSharedPreferences("HISTORYIDSAVE", Context.MODE_PRIVATE);
        SharedPreferences.Editor historysaveeditor = historysave.edit();
        historysaveeditor.clear();
        historysaveeditor.commit();




        CommonVariables.dashboard=true;
        CommonVariables.SALES_FOLLOW_UPTYPE="Today";


        CommonVariables.filermodellist.clear();
        CommonVariables.filerenqsourcelist.clear();
        CommonVariables.filterexchnage="";
        CommonVariables.filetfinancerequired="";
    }

    @Override
    protected void onStop() {
        super.onStop();
        CommonVariables.dashboard=false;

    }


    // User Not Avaiable Alert Dialog
    private void showAlertDialog() {
        dialogset = new Dialog(DashboardActivity.this);
        dialogset.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialogset.setContentView(R.layout.alert_popup);
        dialogset.setCancelable(false);
        dialogset.show();

        AppCompatButton appCompatButtonExit = dialogset.findViewById(R.id.bt_Exit);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogset.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        appCompatButtonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();

                finishAffinity();
                dialogset.dismiss();


            }
        });


        try
        {

            dialogset.getWindow().setAttributes(lp);

        }catch (WindowManager.BadTokenException e)
        {
            e.printStackTrace();
        }
        catch (final Exception e) {
            // Handle or //Log or ignore
        }




    }

    @Override
    protected void onPause() {
        super.onPause();

        CommonVariables.dashboard=false;

    }

    @Override
    protected void onResume() {
        super.onResume();

//        if (!isAccessibilitySettingsOn(getApplicationContext())) {
//            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
//        }

        CommonVariables.dashboard=true;

        if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {


            customcall.setVisible(false);
            brreport.setVisible(false);
        }
        else
        {
            customcall.setVisible(false);
            brreport.setVisible(false);

        }



        if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {
            nav_expected_date_report.setVisible(false);

        }
        else {

            nav_expected_date_report.setVisible(false);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    // To check if service is enabled
    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = getPackageName() + "/" + MyAccessibilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            // Log.v("TAG", "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            //Log.e("TAG", "Error finding setting, default accessibility to not found: " + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            //Log.v("TAG", "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    // Log.v("TAG", "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        //Log.v("TAG", "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            //Log.v("TAG", "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    public double getRam()
    {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        double availableMegs = mi.availMem / 0x100000L;

//Percentage can be calculated for API 16+
        double percentAvail = mi.availMem / (double)mi.totalMem * 100.0;

        return availableMegs/(1024*1024*1024);
    }

    private float convertToGb(long valInBytes) {
        return Float.valueOf(String.format("%.2f", (float) valInBytes / (1024 * 1024 * 1024)));
    }

    // User Not Avaiable Alert Dialog
    private void showStorageAlertDialog()
    {
        dialogsetnew = new Dialog(DashboardActivity.this);
        dialogsetnew.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialogsetnew.setContentView(R.layout.storagealert_popup);
        dialogsetnew.setCancelable(false);
        dialogsetnew.show();


        TextView ram = dialogsetnew.findViewById(R.id.ram);
        TextView storage = dialogsetnew.findViewById(R.id.stoarge);
        AppCompatButton appCompatButtonExit = dialogsetnew.findViewById(R.id.bt_Exit);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogsetnew.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        ram.setText(""+CommonVariables.RAM);
        storage.setText(""+CommonVariables.STORAGE);

        appCompatButtonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();

                finishAffinity();
                dialogsetnew.dismiss();


            }
        });


        try
        {

            dialogsetnew.getWindow().setAttributes(lp);


        }catch (WindowManager.BadTokenException e)
        {
            e.printStackTrace();
        }
        catch (final Exception e) {
            // Handle or //Log or ignore
        }

    }


    public void Android_version_check(String AndroidVersion)
    {
        Dialog shippingDialog = new Dialog(DashboardActivity.this);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.android_version_checker);
        shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        shippingDialog.setCancelable(false);
        shippingDialog.setCanceledOnTouchOutside(false);
        Button button_install = (Button)shippingDialog.findViewById(R.id.buttoninstall);
        TextView textView = (TextView) shippingDialog.findViewById(R.id.setupnewtitle);
        textView.setText("Minimum Android version for weconnect - 8.0 Your mobile android version - "+AndroidVersion+" Kindly update your android version/change device..!");
        shippingDialog.show();
        button_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shippingDialog.dismiss();
                finishAffinity();

            }
        });
    }




}