package com.hero.weconnect.mysmartcrm.models;

import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class Incoming_Service_ReminderModel extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"id":"12dc60fa-4721-4e8a-a147-126c3251ce45","vin":"123","registration_number":"seh654564","model":"deluxe","color":"black","hsrp_stage":"dsf","warranty_start_date":"","serviceupdate":"","mandatoryupdate":"","herosure":"yes","avg_km_run":"1200","digitalcustomer":"","customerrating":"1","cusotmer_first_name":"KUMAR","customer_last_name":"SENA","cusotmercontact":"9024936518","address":"","name_and_mobileno_multiple":"","last_service_date":"","last_service_type":"","last_jc_km":"","organization":"","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"1900-01-01T00:00:00","reminder_followup_date":"1900-01-01T00:00:00","insurance_expiry_date":"","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":""}]
     */

    private String Message;
    private List<DataBean> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * id : 12dc60fa-4721-4e8a-a147-126c3251ce45
         * vin : 123
         * registration_number : seh654564
         * model : deluxe
         * color : black
         * hsrp_stage : dsf
         * warranty_start_date :
         * serviceupdate :
         * mandatoryupdate :
         * herosure : yes
         * avg_km_run : 1200
         * digitalcustomer :
         * customerrating : 1
         * cusotmer_first_name : KUMAR
         * customer_last_name : SENA
         * cusotmercontact : 9024936518
         * address :
         * name_and_mobileno_multiple :
         * last_service_date :
         * last_service_type :
         * last_jc_km :
         * organization :
         * last_invoice_value :
         * open_complaint_number :
         * open_sr_number :
         * fsc1_date :
         * fsc2_date :
         * fsc3_date :
         * fsc4_date :
         * fsc5_date :
         * paid1_date :
         * next_service_due_date : 1900-01-01T00:00:00
         * reminder_followup_date : 1900-01-01T00:00:00
         * insurance_expiry_date :
         * gl_expiry :
         * gl_points :
         * ls_expiry :
         * ls_balance :
         * joyride_expiry :
         * joyride_balance :
         */

        private String id;
        private String vin;
        private String registration_number;
        private String model;
        private String color;
        private String hsrp_stage;
        private String warranty_start_date;
        private String serviceupdate;
        private String mandatoryupdate;
        private String herosure;
        private String avg_km_run;
        private String digitalcustomer;
        private String customerrating;
        private String cusotmer_first_name;
        private String customer_last_name;
        private String cusotmercontact;
        private String address;
        private String name_and_mobileno_multiple;
        private String last_service_date;
        private String last_service_type;
        private String last_jc_km;
        private String organization;
        private String last_invoice_value;
        private String open_complaint_number;
        private String open_sr_number;
        private String fsc1_date;
        private String fsc2_date;
        private String fsc3_date;
        private String fsc4_date;
        private String fsc5_date;
        private String paid1_date;
        private String next_service_due_date;
        private String reminder_followup_date;
        private String insurance_expiry_date;
        private String gl_expiry;
        private String gl_points;
        private String ls_expiry;
        private String ls_balance;
        private String joyride_expiry;
        private String joyride_balance;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getRegistration_number() {
            return registration_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getHsrp_stage() {
            return hsrp_stage;
        }

        public void setHsrp_stage(String hsrp_stage) {
            this.hsrp_stage = hsrp_stage;
        }

        public String getWarranty_start_date() {
            return warranty_start_date;
        }

        public void setWarranty_start_date(String warranty_start_date) {
            this.warranty_start_date = warranty_start_date;
        }

        public String getServiceupdate() {
            return serviceupdate;
        }

        public void setServiceupdate(String serviceupdate) {
            this.serviceupdate = serviceupdate;
        }

        public String getMandatoryupdate() {
            return mandatoryupdate;
        }

        public void setMandatoryupdate(String mandatoryupdate) {
            this.mandatoryupdate = mandatoryupdate;
        }

        public String getHerosure() {
            return herosure;
        }

        public void setHerosure(String herosure) {
            this.herosure = herosure;
        }

        public String getAvg_km_run() {
            return avg_km_run;
        }

        public void setAvg_km_run(String avg_km_run) {
            this.avg_km_run = avg_km_run;
        }

        public String getDigitalcustomer() {
            return digitalcustomer;
        }

        public void setDigitalcustomer(String digitalcustomer) {
            this.digitalcustomer = digitalcustomer;
        }

        public String getCustomerrating() {
            return customerrating;
        }

        public void setCustomerrating(String customerrating) {
            this.customerrating = customerrating;
        }

        public String getCusotmer_first_name() {
            return cusotmer_first_name;
        }

        public void setCusotmer_first_name(String cusotmer_first_name) {
            this.cusotmer_first_name = cusotmer_first_name;
        }

        public String getCustomer_last_name() {
            return customer_last_name;
        }

        public void setCustomer_last_name(String customer_last_name) {
            this.customer_last_name = customer_last_name;
        }

        public String getCusotmercontact() {
            return cusotmercontact;
        }

        public void setCusotmercontact(String cusotmercontact) {
            this.cusotmercontact = cusotmercontact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName_and_mobileno_multiple() {
            return name_and_mobileno_multiple;
        }

        public void setName_and_mobileno_multiple(String name_and_mobileno_multiple) {
            this.name_and_mobileno_multiple = name_and_mobileno_multiple;
        }

        public String getLast_service_date() {
            return last_service_date;
        }

        public void setLast_service_date(String last_service_date) {
            this.last_service_date = last_service_date;
        }

        public String getLast_service_type() {
            return last_service_type;
        }

        public void setLast_service_type(String last_service_type) {
            this.last_service_type = last_service_type;
        }

        public String getLast_jc_km() {
            return last_jc_km;
        }

        public void setLast_jc_km(String last_jc_km) {
            this.last_jc_km = last_jc_km;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getLast_invoice_value() {
            return last_invoice_value;
        }

        public void setLast_invoice_value(String last_invoice_value) {
            this.last_invoice_value = last_invoice_value;
        }

        public String getOpen_complaint_number() {
            return open_complaint_number;
        }

        public void setOpen_complaint_number(String open_complaint_number) {
            this.open_complaint_number = open_complaint_number;
        }

        public String getOpen_sr_number() {
            return open_sr_number;
        }

        public void setOpen_sr_number(String open_sr_number) {
            this.open_sr_number = open_sr_number;
        }

        public String getFsc1_date() {
            return fsc1_date;
        }

        public void setFsc1_date(String fsc1_date) {
            this.fsc1_date = fsc1_date;
        }

        public String getFsc2_date() {
            return fsc2_date;
        }

        public void setFsc2_date(String fsc2_date) {
            this.fsc2_date = fsc2_date;
        }

        public String getFsc3_date() {
            return fsc3_date;
        }

        public void setFsc3_date(String fsc3_date) {
            this.fsc3_date = fsc3_date;
        }

        public String getFsc4_date() {
            return fsc4_date;
        }

        public void setFsc4_date(String fsc4_date) {
            this.fsc4_date = fsc4_date;
        }

        public String getFsc5_date() {
            return fsc5_date;
        }

        public void setFsc5_date(String fsc5_date) {
            this.fsc5_date = fsc5_date;
        }

        public String getPaid1_date() {
            return paid1_date;
        }

        public void setPaid1_date(String paid1_date) {
            this.paid1_date = paid1_date;
        }

        public String getNext_service_due_date() {
            return next_service_due_date;
        }

        public void setNext_service_due_date(String next_service_due_date) {
            this.next_service_due_date = next_service_due_date;
        }

        public String getReminder_followup_date() {
            return reminder_followup_date;
        }

        public void setReminder_followup_date(String reminder_followup_date) {
            this.reminder_followup_date = reminder_followup_date;
        }

        public String getInsurance_expiry_date() {
            return insurance_expiry_date;
        }

        public void setInsurance_expiry_date(String insurance_expiry_date) {
            this.insurance_expiry_date = insurance_expiry_date;
        }

        public String getGl_expiry() {
            return gl_expiry;
        }

        public void setGl_expiry(String gl_expiry) {
            this.gl_expiry = gl_expiry;
        }

        public String getGl_points() {
            return gl_points;
        }

        public void setGl_points(String gl_points) {
            this.gl_points = gl_points;
        }

        public String getLs_expiry() {
            return ls_expiry;
        }

        public void setLs_expiry(String ls_expiry) {
            this.ls_expiry = ls_expiry;
        }

        public String getLs_balance() {
            return ls_balance;
        }

        public void setLs_balance(String ls_balance) {
            this.ls_balance = ls_balance;
        }

        public String getJoyride_expiry() {
            return joyride_expiry;
        }

        public void setJoyride_expiry(String joyride_expiry) {
            this.joyride_expiry = joyride_expiry;
        }

        public String getJoyride_balance() {
            return joyride_balance;
        }

        public void setJoyride_balance(String joyride_balance) {
            this.joyride_balance = joyride_balance;
        }
    }
}
