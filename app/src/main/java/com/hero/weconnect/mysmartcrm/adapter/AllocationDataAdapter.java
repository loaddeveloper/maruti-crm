package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;

import java.util.ArrayList;

public class AllocationDataAdapter extends RecyclerView.Adapter<AllocationDataAdapter.ViewHolder>
{
       Context context;
       ArrayList<ServiceReminderDataModel.DataBean> list;


    public AllocationDataAdapter(@NonNull Context context,ArrayList<ServiceReminderDataModel.DataBean> list) {
        this.context=context;
        this.list=list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.data_allocation_layout,parent,false)  ;

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        ServiceReminderDataModel.DataBean dataBean = list.get(position);


         holder.mTvTotalCount.setText(""+dataBean.getDataCount());
         holder.mTvTitle.setText(""+dataBean.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvTitle;
        private TextView mTvTotalCount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvTitle = itemView.findViewById(R.id.tv_title);
            mTvTotalCount = itemView.findViewById(R.id.tv_total_count);



        }
    }
}
