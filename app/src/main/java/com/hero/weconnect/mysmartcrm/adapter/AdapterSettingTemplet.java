package com.hero.weconnect.mysmartcrm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.activity.SettingActivity;
import com.hero.weconnect.mysmartcrm.customfonts.TextView_Roboto_Medium;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;

import java.util.ArrayList;


public class AdapterSettingTemplet extends RecyclerView.Adapter<AdapterSettingTemplet.ViewHolder> {
    ArrayList<GetTamplateListModel.DataBean> templatelist = new ArrayList<>();

    private Context context;

    public AdapterSettingTemplet(Context context, ArrayList<GetTamplateListModel.DataBean> list) {

        this.context = context;
        this.templatelist = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.temp_recyclerview_layout, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        GetTamplateListModel.DataBean dataBean = templatelist.get(position);
        holder.headertemplate.setText("" + dataBean.getTemplate_name());
        holder.srtext.setText("" + dataBean.getTemplate_body());


        holder.srlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SettingActivity) context).srlayouttemplate(holder.headertemplate.getText().toString(), holder.srtext.getText().toString(), dataBean.getId());
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return templatelist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView_Roboto_Medium headertemplate;
        public TextView srtext;
        public LinearLayout srlayout;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.headertemplate = rootView.findViewById(R.id.headertemplate);
            this.srtext = rootView.findViewById(R.id.srtext);
            this.srlayout = rootView.findViewById(R.id.srlayout);
        }
    }


}