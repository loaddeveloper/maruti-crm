package com.hero.weconnect.mysmartcrm.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Adapter_Sales_FinancialCalls_HistoryIncomingdata extends RecyclerView.Adapter<Adapter_Sales_FinancialCalls_HistoryIncomingdata.ViewHolder> {
    private static final int REQUEST_PERMISSION = 0;
    public static int pos = 0;
    public static String srtemplate;
    static Typeface tf;
    ArrayList<SR_HistoryModel.DataBean> srhistorydatalist = new ArrayList<>();
    Adapter_Sales_FinancialCalls_HistoryIncomingdata adapter;
    int ye, day, mon;
    int hh = 0;
    int size = 0;
    private Context context;


    public Adapter_Sales_FinancialCalls_HistoryIncomingdata(Context context, ArrayList<SR_HistoryModel.DataBean> datalist) {

        this.context = context;
        this.srhistorydatalist = datalist;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.history_sr_popup_cardview, parent, false);

        return new ViewHolder(v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);

        SR_HistoryModel.DataBean dataBean = srhistorydatalist.get(position);
        holder.tv_srnumber.setText(""+dataBean.getSno()+". ".toUpperCase());
        holder.customer_name.setText("" + dataBean.getUser().toUpperCase());
        holder.calledstatus.setText("" + dataBean.getCalledstatus().toUpperCase());
        if(dataBean.getReminder_followup_date() != null && !dataBean.getReminder_followup_date().isEmpty()) {
            holder.reminder_followup_date.setText("" +dataBean.getReminder_followup_date());
        }else
        {
            holder.reminder_followup_date.setText("");
        }
        holder.service_booking.setText("" +dataBean.getServicebooking() );
        holder.remarks.setText("" + dataBean.getComments().toUpperCase());
        holder.contact_status.setText("" + dataBean.getContact_status().toUpperCase());
        holder.service_request.setText("" + dataBean.getCall_end_datetime());

        holder.mainsview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog shippingDialog1 = new Dialog(context);
                shippingDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                shippingDialog1.setContentView(R.layout.history_moredetails_cardview_incomingsr);
                shippingDialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                shippingDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                shippingDialog1.setCancelable(true);
                shippingDialog1.setCanceledOnTouchOutside(false);

                TextView closebtn;
                EditText customer_name,tv_srnumber,contact_status,created_at,service_booking,service_request,tageged_sr_status,contact_changed,reminder_followup_date,
                        remarks,not_comming_reason,pickup_drop,call_start_datetime,call_end_datetime,calledstatus;

                customer_name = shippingDialog1.findViewById(R.id.customer_name);
                contact_status = shippingDialog1.findViewById(R.id.contact_status);
                created_at = shippingDialog1.findViewById(R.id.created_at);
                service_booking = shippingDialog1.findViewById(R.id.service_booking);
                service_request = shippingDialog1.findViewById(R.id.service_request);
                tageged_sr_status = shippingDialog1.findViewById(R.id.tageged_sr_status);
                contact_changed = shippingDialog1.findViewById(R.id.contact_changed);
                reminder_followup_date = shippingDialog1.findViewById(R.id.reminder_followup_date);
                remarks = shippingDialog1.findViewById(R.id.remarks);
                not_comming_reason = shippingDialog1.findViewById(R.id.not_comming_reason);
                pickup_drop = shippingDialog1.findViewById(R.id.pickup_drop);
                call_start_datetime = shippingDialog1.findViewById(R.id.call_start_datetime);
                call_end_datetime = shippingDialog1.findViewById(R.id.call_end_datetime);
                calledstatus = shippingDialog1.findViewById(R.id.calledstatus);
                closebtn=shippingDialog1.findViewById(R.id.txtclose);
                closebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shippingDialog1.dismiss();
                    }
                });



                customer_name.setText("" + dataBean.getUser());
                calledstatus.setText("" + dataBean.getCalledstatus());

                if(dataBean.getCall_start_datetime().isEmpty())
                {
                    call_start_datetime.setText("" + dataBean.getCall_start_datetime());

                }else
                {
                    call_start_datetime.setText("" + dataBean.getCall_start_datetime().substring(0,11));

                }

                if(dataBean.getCall_end_datetime().isEmpty())
                {
                    call_end_datetime.setText("" + dataBean.getCall_end_datetime());

                }else
                {
                    call_end_datetime.setText("" + dataBean.getCall_end_datetime().substring(0,11));

                }


                    reminder_followup_date.setText("" + dataBean.getReminder_followup_date());


                service_booking.setText("" +dataBean.getServicebooking());
                remarks.setText("" + dataBean.getComments());
                contact_status.setText("" + dataBean.getContact_status());
                created_at.setText("" + dateparse3(dataBean.getCreatedat().replace("T"," ")).substring(0,11));
                not_comming_reason.setText("" + dataBean.getNot_coming_reason());
                tageged_sr_status.setText("" + dataBean.getTagged_sr_status());
                contact_changed.setText("" + dataBean.getContact_changed());
                pickup_drop.setText("" + dataBean.getPickup_drop());
                service_request.setText("" + dataBean.getServicerequest());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    shippingDialog1.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    shippingDialog1.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {
                    shippingDialog1.getWindow().setType(WindowManager.LayoutParams.TYPE_PHONE);
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    shippingDialog1.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }

                shippingDialog1.show();

            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {


        return srhistorydatalist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView customer_name;
        public TextView tv_srnumber;
        public TextView contact_status;
        public TextView created_at;
        public TextView service_booking;
        public TextView service_request;
        public TextView tageged_sr_status;
        public TextView contact_changed;
        public TextView reminder_followup_date;
        public TextView remarks;
        public TextView not_comming_reason;
        public TextView pickup_drop;
        public TextView call_start_datetime;
        public TextView call_end_datetime;
        public TextView calledstatus;
        public LinearLayout mainsview;

        public ViewHolder(View rootView) {
            super(rootView);
            this.rootView = rootView;
            this.customer_name = rootView.findViewById(R.id.customer_name);
            this.tv_srnumber = rootView.findViewById(R.id.tv_srnumber);
            this.contact_status = rootView.findViewById(R.id.contact_status);
            this.service_booking = rootView.findViewById(R.id.service_booking);
            this.service_request = rootView.findViewById(R.id.service_request);
            this.remarks = rootView.findViewById(R.id.remarks);
            this.reminder_followup_date = rootView.findViewById(R.id.reminder_followup_date);
            this.calledstatus = rootView.findViewById(R.id.calledstatus);
            this.mainsview = rootView.findViewById(R.id.mainsview);
        }
    }
    public String dateparse(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String NewDateFormat=null;


        try {
            date = inputFormat.parse(inputdate);
            NewDateFormat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return NewDateFormat;

    }

    public String dateparse3(String inputdate) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;

        String newdate=null;
        try {
            date = inputFormat.parse(inputdate);
            newdate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newdate;
    }
}