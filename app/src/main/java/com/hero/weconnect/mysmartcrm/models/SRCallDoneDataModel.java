package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SRCallDoneDataModel extends SuperClassCastBean {


    /**
     * Message : Success
     * Data : [{"sno":1,"id":"432efcd0-cd99-4af3-bbe0-a8bb4aae2255","vin":"MBLHAW080K4M13895","registration_number":"TN96C7464","model":"SPLENDOR +","color":"GBK","hsrp_stage":"HSRP Received","warranty_start_date":"2020-01-28 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"17","digitalcustomer":"N","customerrating":"BAC006SPND","cusotmer_first_name":"KAMALA KANNAN","customer_last_name":"P","cusotmercontact":"1111111111","address":"1/159, EAST STREET MELMANDAI","name_and_mobileno_multiple":"KAMALA KANNAN P 1111111111; ","last_service_date":"2020-06-02 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"2196","organization":"Thirupathi Auto Agency","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2020-02-24 19:32:08.0000000","fsc2_date":"2020-06-02 17:08:58.0000000","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2020-09-10 00:00:00.0000000","reminder_followup_date":"","insurance_expiry_date":"2025-01-28 00:00:00.0000000","gl_expiry":"","gl_points":"","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","calledstatus":"CALL DONE","ServiceType":"SRVCE_SKPD_FSC3_CALL_FSC4","contact_status":"Contacted","customerreply":"","servicebooking":"2021-01-11T10:32:14","servicerequest":"10155-03-RSRB-0121-4576","contact_changed":"","next_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","pickup_drop":"","comments":"Booked"},{"sno":2,"id":"ef82a414-93b3-4c9d-8ade-bbd9539d6a27","vin":"MBLJFN05XKGM01464","registration_number":"TN69BK4374","model":"PLEASURE+","color":"PBU","hsrp_stage":"HSRP Received","warranty_start_date":"2020-01-28 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"11","digitalcustomer":"N","customerrating":"BBA006NONE","cusotmer_first_name":"AZHAGU SHANMUGALAKSHMI","customer_last_name":"C","cusotmercontact":"1111111111","address":"4/137-9, KRISHNA NAGAR MUTTAYYAPURAM","name_and_mobileno_multiple":"AZHAGU SHANMUGALAKSHMI C 1111111111; ","last_service_date":"2020-03-23 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"625","organization":"ARAVINTH AUTO AGENCY","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2020-03-23 10:32:36.0000000","fsc2_date":"","fsc3_date":"","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2020-07-01 00:00:00.0000000","reminder_followup_date":"2020-08-27 00:00:00.0000000","insurance_expiry_date":"2025-01-24 00:00:00.0000000","gl_expiry":"29/01/2023","gl_points":"679","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","calledstatus":"CALL DONE","ServiceType":"SRVCE_SKPD_FSC3_CALL_FSC4","contact_status":"Contacted","customerreply":"","servicebooking":"1900-01-01T00:00:00","servicerequest":"","contact_changed":"","next_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","pickup_drop":"","comments":""},{"sno":3,"id":"cb9ea8b1-7c79-48d9-95df-792b4b6c518b","vin":"MBLJFW028KGE03292","registration_number":"TN69BJ1879","model":"PLEASURE","color":"MAG","hsrp_stage":"Sent for HSRP","warranty_start_date":"2019-06-12 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"4","digitalcustomer":"Y","customerrating":"ABA014NONE","cusotmer_first_name":"MANMATHARAJAN","customer_last_name":"A","cusotmercontact":"1111111111","address":"269A/1 PALAYAMKOTTAI ROAD THOOTHUKKUDI","name_and_mobileno_multiple":"MANMATHARAJAN A 1111111111","last_service_date":"2020-03-11 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"1208","organization":"ARAVINTH AUTO AGENCY","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-08-19 11:10:36.0000000","fsc2_date":"2019-12-10 10:28:00.0000000","fsc3_date":"2020-03-11 11:11:07.0000000","fsc4_date":"","fsc5_date":"","paid1_date":"","next_service_due_date":"2020-06-19 00:00:00.0000000","reminder_followup_date":"2020-08-24 00:00:00.0000000","insurance_expiry_date":"2024-06-11 00:00:00.0000000","gl_expiry":"13/06/2022","gl_points":"1135","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","calledstatus":"CALL DONE","ServiceType":"SRVCE_SKPD_FSC5_CALL_PAID1","contact_status":"Contacted","customerreply":"","servicebooking":"2021-01-14T10:29:58","servicerequest":"10155-03-RSRB-0121-4575","contact_changed":"","next_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","pickup_drop":"","comments":"Booked"},{"sno":4,"id":"6bc06ff5-bea4-4a99-8535-b6691c265816","vin":"MBLJFW027KGE03669","registration_number":"TN69BJ1904","model":"PLEASURE","color":"CRD","hsrp_stage":"Sent for HSRP","warranty_start_date":"2019-06-12 00:00:00.0000000","serviceupdate":"N","mandatoryupdate":"N","herosure":"N","herosure_date":"","avg_km_run":"6","digitalcustomer":"N","customerrating":"ABA014NONE","cusotmer_first_name":"PON ESAKKI","customer_last_name":"D","cusotmercontact":"1111111111","address":"3/430-19,PONRAJ NAGAR KUMARAGIRI","name_and_mobileno_multiple":"PON ESAKKI D 1111111111","last_service_date":"2020-06-18 00:00:00.0000000","last_service_type":"FSC","last_jc_km":"2404","organization":"ARAVINTH AUTO AGENCY","last_invoice_value":"","open_complaint_number":"","open_sr_number":"","fsc1_date":"2019-08-22 12:42:10.0000000","fsc2_date":"","fsc3_date":"2020-01-04 10:43:28.0000000","fsc4_date":"2020-06-18 11:16:11.0000000","fsc5_date":"","paid1_date":"","next_service_due_date":"2020-09-26 00:00:00.0000000","reminder_followup_date":"2020-07-03 00:00:00.0000000","insurance_expiry_date":"2024-06-11 00:00:00.0000000","gl_expiry":"18/06/2022","gl_points":"1130","ls_expiry":"","ls_balance":"","joyride_expiry":"","joyride_balance":"","calledstatus":"CALL DONE","ServiceType":"SRVCE_SKPD_FSC5_CALL_PAID1","contact_status":"Contacted","customerreply":"","servicebooking":"2021-01-08T10:28:07","servicerequest":"10155-03-RSRB-0121-4574","contact_changed":"","next_followup_date":"1900-01-01T00:00:00","not_coming_reason":"","pickup_drop":"","comments":"Booked"}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * sno : 1
         * id : 432efcd0-cd99-4af3-bbe0-a8bb4aae2255
         * vin : MBLHAW080K4M13895
         * registration_number : TN96C7464
         * model : SPLENDOR +
         * color : GBK
         * hsrp_stage : HSRP Received
         * warranty_start_date : 2020-01-28 00:00:00.0000000
         * serviceupdate : N
         * mandatoryupdate : N
         * herosure : N
         * herosure_date :
         * avg_km_run : 17
         * digitalcustomer : N
         * customerrating : BAC006SPND
         * cusotmer_first_name : KAMALA KANNAN
         * customer_last_name : P
         * cusotmercontact : 1111111111
         * address : 1/159, EAST STREET MELMANDAI
         * name_and_mobileno_multiple : KAMALA KANNAN P 1111111111;
         * last_service_date : 2020-06-02 00:00:00.0000000
         * last_service_type : FSC
         * last_jc_km : 2196
         * organization : Thirupathi Auto Agency
         * last_invoice_value :
         * open_complaint_number :
         * open_sr_number :
         * fsc1_date : 2020-02-24 19:32:08.0000000
         * fsc2_date : 2020-06-02 17:08:58.0000000
         * fsc3_date :
         * fsc4_date :
         * fsc5_date :
         * paid1_date :
         * next_service_due_date : 2020-09-10 00:00:00.0000000
         * reminder_followup_date :
         * insurance_expiry_date : 2025-01-28 00:00:00.0000000
         * gl_expiry :
         * gl_points :
         * ls_expiry :
         * ls_balance :
         * joyride_expiry :
         * joyride_balance :
         * calledstatus : CALL DONE
         * ServiceType : SRVCE_SKPD_FSC3_CALL_FSC4
         * contact_status : Contacted
         * customerreply :
         * servicebooking : 2021-01-11T10:32:14
         * servicerequest : 10155-03-RSRB-0121-4576
         * contact_changed :
         * next_followup_date : 1900-01-01T00:00:00
         * not_coming_reason :
         * pickup_drop :
         * comments : Booked
         */

        @SerializedName("sno")
        private int sno;
        @SerializedName("id")
        private String id;
        @SerializedName("vin")
        private String vin;
        @SerializedName("registration_number")
        private String registrationNumber;
        @SerializedName("model")
        private String model;
        @SerializedName("color")
        private String color;
        @SerializedName("hsrp_stage")
        private String hsrpStage;
        @SerializedName("warranty_start_date")
        private String warrantyStartDate;
        @SerializedName("serviceupdate")
        private String serviceupdate;
        @SerializedName("mandatoryupdate")
        private String mandatoryupdate;
        @SerializedName("herosure")
        private String herosure;
        @SerializedName("herosure_date")
        private String herosureDate;
        @SerializedName("avg_km_run")
        private String avgKmRun;
        @SerializedName("digitalcustomer")
        private String digitalcustomer;
        @SerializedName("customerrating")
        private String customerrating;
        @SerializedName("cusotmer_first_name")
        private String cusotmerFirstName;
        @SerializedName("customer_last_name")
        private String customerLastName;
        @SerializedName("cusotmercontact")
        private String cusotmercontact;
        @SerializedName("address")
        private String address;
        @SerializedName("name_and_mobileno_multiple")
        private String nameAndMobilenoMultiple;
        @SerializedName("last_service_date")
        private String lastServiceDate;
        @SerializedName("last_service_type")
        private String lastServiceType;
        @SerializedName("last_jc_km")
        private String lastJcKm;
        @SerializedName("organization")
        private String organization;
        @SerializedName("last_invoice_value")
        private String lastInvoiceValue;
        @SerializedName("open_complaint_number")
        private String openComplaintNumber;
        @SerializedName("open_sr_number")
        private String openSrNumber;
        @SerializedName("fsc1_date")
        private String fsc1Date;
        @SerializedName("fsc2_date")
        private String fsc2Date;
        @SerializedName("fsc3_date")
        private String fsc3Date;
        @SerializedName("fsc4_date")
        private String fsc4Date;
        @SerializedName("fsc5_date")
        private String fsc5Date;
        @SerializedName("paid1_date")
        private String paid1Date;
        @SerializedName("next_service_due_date")
        private String nextServiceDueDate;
        @SerializedName("reminder_followup_date")
        private String reminderFollowupDate;
        @SerializedName("insurance_expiry_date")
        private String insuranceExpiryDate;
        @SerializedName("gl_expiry")
        private String glExpiry;
        @SerializedName("gl_points")
        private String glPoints;
        @SerializedName("ls_expiry")
        private String lsExpiry;
        @SerializedName("ls_balance")
        private String lsBalance;
        @SerializedName("joyride_expiry")
        private String joyrideExpiry;
        @SerializedName("joyride_balance")
        private String joyrideBalance;
        @SerializedName("calledstatus")
        private String calledstatus;
        @SerializedName("ServiceType")
        private String ServiceType;
        @SerializedName("contact_status")
        private String contactStatus;
        @SerializedName("customerreply")
        private String customerreply;
        @SerializedName("servicebooking")
        private String servicebooking;
        @SerializedName("servicerequest")
        private String servicerequest;
        @SerializedName("contact_changed")
        private String contactChanged;
        @SerializedName("next_followup_date")
        private String nextFollowupDate;
        @SerializedName("not_coming_reason")
        private String notComingReason;
        @SerializedName("pickup_drop")
        private String pickupDrop;
        @SerializedName("comments")
        private String comments;

        public int getSno() {
            return sno;
        }

        public void setSno(int sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getRegistrationNumber() {
            return registrationNumber;
        }

        public void setRegistrationNumber(String registrationNumber) {
            this.registrationNumber = registrationNumber;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getHsrpStage() {
            return hsrpStage;
        }

        public void setHsrpStage(String hsrpStage) {
            this.hsrpStage = hsrpStage;
        }

        public String getWarrantyStartDate() {
            return warrantyStartDate;
        }

        public void setWarrantyStartDate(String warrantyStartDate) {
            this.warrantyStartDate = warrantyStartDate;
        }

        public String getServiceupdate() {
            return serviceupdate;
        }

        public void setServiceupdate(String serviceupdate) {
            this.serviceupdate = serviceupdate;
        }

        public String getMandatoryupdate() {
            return mandatoryupdate;
        }

        public void setMandatoryupdate(String mandatoryupdate) {
            this.mandatoryupdate = mandatoryupdate;
        }

        public String getHerosure() {
            return herosure;
        }

        public void setHerosure(String herosure) {
            this.herosure = herosure;
        }

        public String getHerosureDate() {
            return herosureDate;
        }

        public void setHerosureDate(String herosureDate) {
            this.herosureDate = herosureDate;
        }

        public String getAvgKmRun() {
            return avgKmRun;
        }

        public void setAvgKmRun(String avgKmRun) {
            this.avgKmRun = avgKmRun;
        }

        public String getDigitalcustomer() {
            return digitalcustomer;
        }

        public void setDigitalcustomer(String digitalcustomer) {
            this.digitalcustomer = digitalcustomer;
        }

        public String getCustomerrating() {
            return customerrating;
        }

        public void setCustomerrating(String customerrating) {
            this.customerrating = customerrating;
        }

        public String getCusotmerFirstName() {
            return cusotmerFirstName;
        }

        public void setCusotmerFirstName(String cusotmerFirstName) {
            this.cusotmerFirstName = cusotmerFirstName;
        }

        public String getCustomerLastName() {
            return customerLastName;
        }

        public void setCustomerLastName(String customerLastName) {
            this.customerLastName = customerLastName;
        }

        public String getCusotmercontact() {
            return cusotmercontact;
        }

        public void setCusotmercontact(String cusotmercontact) {
            this.cusotmercontact = cusotmercontact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getNameAndMobilenoMultiple() {
            return nameAndMobilenoMultiple;
        }

        public void setNameAndMobilenoMultiple(String nameAndMobilenoMultiple) {
            this.nameAndMobilenoMultiple = nameAndMobilenoMultiple;
        }

        public String getLastServiceDate() {
            return lastServiceDate;
        }

        public void setLastServiceDate(String lastServiceDate) {
            this.lastServiceDate = lastServiceDate;
        }

        public String getLastServiceType() {
            return lastServiceType;
        }

        public void setLastServiceType(String lastServiceType) {
            this.lastServiceType = lastServiceType;
        }

        public String getLastJcKm() {
            return lastJcKm;
        }

        public void setLastJcKm(String lastJcKm) {
            this.lastJcKm = lastJcKm;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getLastInvoiceValue() {
            return lastInvoiceValue;
        }

        public void setLastInvoiceValue(String lastInvoiceValue) {
            this.lastInvoiceValue = lastInvoiceValue;
        }

        public String getOpenComplaintNumber() {
            return openComplaintNumber;
        }

        public void setOpenComplaintNumber(String openComplaintNumber) {
            this.openComplaintNumber = openComplaintNumber;
        }

        public String getOpenSrNumber() {
            return openSrNumber;
        }

        public void setOpenSrNumber(String openSrNumber) {
            this.openSrNumber = openSrNumber;
        }

        public String getFsc1Date() {
            return fsc1Date;
        }

        public void setFsc1Date(String fsc1Date) {
            this.fsc1Date = fsc1Date;
        }

        public String getFsc2Date() {
            return fsc2Date;
        }

        public void setFsc2Date(String fsc2Date) {
            this.fsc2Date = fsc2Date;
        }

        public String getFsc3Date() {
            return fsc3Date;
        }

        public void setFsc3Date(String fsc3Date) {
            this.fsc3Date = fsc3Date;
        }

        public String getFsc4Date() {
            return fsc4Date;
        }

        public void setFsc4Date(String fsc4Date) {
            this.fsc4Date = fsc4Date;
        }

        public String getFsc5Date() {
            return fsc5Date;
        }

        public void setFsc5Date(String fsc5Date) {
            this.fsc5Date = fsc5Date;
        }

        public String getPaid1Date() {
            return paid1Date;
        }

        public void setPaid1Date(String paid1Date) {
            this.paid1Date = paid1Date;
        }

        public String getNextServiceDueDate() {
            return nextServiceDueDate;
        }

        public void setNextServiceDueDate(String nextServiceDueDate) {
            this.nextServiceDueDate = nextServiceDueDate;
        }

        public String getReminderFollowupDate() {
            return reminderFollowupDate;
        }

        public void setReminderFollowupDate(String reminderFollowupDate) {
            this.reminderFollowupDate = reminderFollowupDate;
        }

        public String getInsuranceExpiryDate() {
            return insuranceExpiryDate;
        }

        public void setInsuranceExpiryDate(String insuranceExpiryDate) {
            this.insuranceExpiryDate = insuranceExpiryDate;
        }

        public String getGlExpiry() {
            return glExpiry;
        }

        public void setGlExpiry(String glExpiry) {
            this.glExpiry = glExpiry;
        }

        public String getGlPoints() {
            return glPoints;
        }

        public void setGlPoints(String glPoints) {
            this.glPoints = glPoints;
        }

        public String getLsExpiry() {
            return lsExpiry;
        }

        public void setLsExpiry(String lsExpiry) {
            this.lsExpiry = lsExpiry;
        }

        public String getLsBalance() {
            return lsBalance;
        }

        public void setLsBalance(String lsBalance) {
            this.lsBalance = lsBalance;
        }

        public String getJoyrideExpiry() {
            return joyrideExpiry;
        }

        public void setJoyrideExpiry(String joyrideExpiry) {
            this.joyrideExpiry = joyrideExpiry;
        }

        public String getJoyrideBalance() {
            return joyrideBalance;
        }

        public void setJoyrideBalance(String joyrideBalance) {
            this.joyrideBalance = joyrideBalance;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }

        public String getServiceType() {
            return ServiceType;
        }

        public void setServiceType(String ServiceType) {
            this.ServiceType = ServiceType;
        }

        public String getContactStatus() {
            return contactStatus;
        }

        public void setContactStatus(String contactStatus) {
            this.contactStatus = contactStatus;
        }

        public String getCustomerreply() {
            return customerreply;
        }

        public void setCustomerreply(String customerreply) {
            this.customerreply = customerreply;
        }

        public String getServicebooking() {
            return servicebooking;
        }

        public void setServicebooking(String servicebooking) {
            this.servicebooking = servicebooking;
        }

        public String getServicerequest() {
            return servicerequest;
        }

        public void setServicerequest(String servicerequest) {
            this.servicerequest = servicerequest;
        }

        public String getContactChanged() {
            return contactChanged;
        }

        public void setContactChanged(String contactChanged) {
            this.contactChanged = contactChanged;
        }

        public String getNextFollowupDate() {
            return nextFollowupDate;
        }

        public void setNextFollowupDate(String nextFollowupDate) {
            this.nextFollowupDate = nextFollowupDate;
        }

        public String getNotComingReason() {
            return notComingReason;
        }

        public void setNotComingReason(String notComingReason) {
            this.notComingReason = notComingReason;
        }

        public String getPickupDrop() {
            return pickupDrop;
        }

        public void setPickupDrop(String pickupDrop) {
            this.pickupDrop = pickupDrop;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }
    }
}
