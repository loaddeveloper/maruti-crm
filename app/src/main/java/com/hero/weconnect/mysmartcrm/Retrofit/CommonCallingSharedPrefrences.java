package com.hero.weconnect.mysmartcrm.Retrofit;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hero.weconnect.mysmartcrm.models.CallingSharedprefrencesModel;

import java.lang.reflect.Type;

public class CommonCallingSharedPrefrences {
    private Context mContext;
    private SharedPreferences.Editor editor, editorpackages;
    private SharedPreferences sharedPreferences, sharedPreferencespackages;

    public CommonCallingSharedPrefrences(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences("callingdata", Context.MODE_PRIVATE);

    }

    public CallingSharedprefrencesModel getCallingData() {

        String json = sharedPreferences.getString(ApiConstant.CALLING_SHARED, null);
        Gson gson = new Gson();
        Type type = new TypeToken<CallingSharedprefrencesModel>() {
        }.getType();

        CallingSharedprefrencesModel beans = null;
        beans = gson.fromJson(json, type);

        return beans;

    }

    public void setCallingData(CallingSharedprefrencesModel beans) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString(ApiConstant.CALLING_SHARED, json);
        editor.commit();
    }


    public void setAllClearCallingData() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }


}
