package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class NonTurnUpDataModel extends SuperClassCastBean {


    @SerializedName("Message")
    private String message;
    @SerializedName("Data")
    private List<DataDTO> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        @SerializedName("sno")
        private Integer sno;
        @SerializedName("id")
        private String id;
        @SerializedName("vin")
        private String vin;
        @SerializedName("regno")
        private String regno;
        @SerializedName("model")
        private String model;
        @SerializedName("color")
        private String color;
        @SerializedName("warrantystartdate")
        private String warrantystartdate;
        @SerializedName("serviceupdate")
        private String serviceupdate;
        @SerializedName("mandatoryupdate")
        private String mandatoryupdate;
        @SerializedName("avgkmrun")
        private String avgkmrun;
        @SerializedName("customerrating")
        private String customerrating;
        @SerializedName("customerfirstname")
        private String customerfirstname;
        @SerializedName("customerlastname")
        private String customerlastname;
        @SerializedName("customercontact")
        private String customercontact;
        @SerializedName("address")
        private String address;
        @SerializedName("insuranceexpirydate")
        private String insuranceexpirydate;
        @SerializedName("glexpiry")
        private String glexpiry;
        @SerializedName("glpoints")
        private String glpoints;
        @SerializedName("lsexpiry")
        private String lsexpiry;
        @SerializedName("lsbalance")
        private String lsbalance;
        @SerializedName("joyrideexpiry")
        private String joyrideexpiry;
        @SerializedName("joyridebalance")
        private String joyridebalance;
        @SerializedName("bookingtime")
        private String bookingtime;
        @SerializedName("bookingdate")
        private String bookingdate;
        @SerializedName("natureofjob")
        private String natureofjob;
        @SerializedName("source")
        private String source;
        @SerializedName("dealercode")
        private String dealercode;
        @SerializedName("calledstatus")
        private String calledstatus;

        public Integer getSno() {
            return sno;
        }

        public void setSno(Integer sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVin() {
            return vin;
        }

        public void setVin(String vin) {
            this.vin = vin;
        }

        public String getRegno() {
            return regno;
        }

        public void setRegno(String regno) {
            this.regno = regno;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getWarrantystartdate() {
            return warrantystartdate;
        }

        public void setWarrantystartdate(String warrantystartdate) {
            this.warrantystartdate = warrantystartdate;
        }

        public String getServiceupdate() {
            return serviceupdate;
        }

        public void setServiceupdate(String serviceupdate) {
            this.serviceupdate = serviceupdate;
        }

        public String getMandatoryupdate() {
            return mandatoryupdate;
        }

        public void setMandatoryupdate(String mandatoryupdate) {
            this.mandatoryupdate = mandatoryupdate;
        }

        public String getAvgkmrun() {
            return avgkmrun;
        }

        public void setAvgkmrun(String avgkmrun) {
            this.avgkmrun = avgkmrun;
        }

        public String getCustomerrating() {
            return customerrating;
        }

        public void setCustomerrating(String customerrating) {
            this.customerrating = customerrating;
        }

        public String getCustomerfirstname() {
            return customerfirstname;
        }

        public void setCustomerfirstname(String customerfirstname) {
            this.customerfirstname = customerfirstname;
        }

        public String getCustomerlastname() {
            return customerlastname;
        }

        public void setCustomerlastname(String customerlastname) {
            this.customerlastname = customerlastname;
        }

        public String getCustomercontact() {
            return customercontact;
        }

        public void setCustomercontact(String customercontact) {
            this.customercontact = customercontact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getInsuranceexpirydate() {
            return insuranceexpirydate;
        }

        public void setInsuranceexpirydate(String insuranceexpirydate) {
            this.insuranceexpirydate = insuranceexpirydate;
        }

        public String getGlexpiry() {
            return glexpiry;
        }

        public void setGlexpiry(String glexpiry) {
            this.glexpiry = glexpiry;
        }

        public String getGlpoints() {
            return glpoints;
        }

        public void setGlpoints(String glpoints) {
            this.glpoints = glpoints;
        }

        public String getLsexpiry() {
            return lsexpiry;
        }

        public void setLsexpiry(String lsexpiry) {
            this.lsexpiry = lsexpiry;
        }

        public String getLsbalance() {
            return lsbalance;
        }

        public void setLsbalance(String lsbalance) {
            this.lsbalance = lsbalance;
        }

        public String getJoyrideexpiry() {
            return joyrideexpiry;
        }

        public void setJoyrideexpiry(String joyrideexpiry) {
            this.joyrideexpiry = joyrideexpiry;
        }

        public String getJoyridebalance() {
            return joyridebalance;
        }

        public void setJoyridebalance(String joyridebalance) {
            this.joyridebalance = joyridebalance;
        }

        public String getBookingtime() {
            return bookingtime;
        }

        public void setBookingtime(String bookingtime) {
            this.bookingtime = bookingtime;
        }

        public String getBookingdate() {
            return bookingdate;
        }

        public void setBookingdate(String bookingdate) {
            this.bookingdate = bookingdate;
        }

        public String getNatureofjob() {
            return natureofjob;
        }

        public void setNatureofjob(String natureofjob) {
            this.natureofjob = natureofjob;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getDealercode() {
            return dealercode;
        }

        public void setDealercode(String dealercode) {
            this.dealercode = dealercode;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
