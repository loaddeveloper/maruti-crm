package com.hero.weconnect.mysmartcrm.Utils;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.hero.weconnect.mysmartcrm.CallUtils.OngoingCall;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.microsoft.appcenter.distribute.DistributeConstants.LOG_TAG;


public class NetworkCheckerService {

    Context context, applicationContext;
    AlertDialog dialog_network_only;
    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

           // NetworkInfo currentNetworkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo currentNetworkInfo = cm.getActiveNetworkInfo();



            if (currentNetworkInfo != null && currentNetworkInfo.isConnected()) {
                if (dialog_network_only != null)
                    dialog_network_only.dismiss();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View v = LayoutInflater.from(context).inflate(R.layout.layout_no_internet, null, false);
                ImageView iv_btn_close  = v.findViewById(R.id.iv_btn_close);
                iv_btn_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog_network_only.dismiss();
                    }
                });
                builder.setView(v);
                dialog_network_only = builder.create();
                try {
                    if (dialog_network_only != null)
                    {
                        dialog_network_only.show();

                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

                dialog_network_only.setCancelable(false);
                OngoingCall.hangup();
            }



        }
    };

    public NetworkCheckerService(Context context, Context applicationContext) {
        this.context = context;
        this.applicationContext = applicationContext;
        initChecker();

    }

    private void initChecker() {
        context.registerReceiver(mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void UnregisterChecker() {
        context.unregisterReceiver(mConnReceiver);
    }

    /***
     * To be called in activity's on destroy or (onPause under Test)
     */
    public void unRegisterReceiver() {
        context.unregisterReceiver(mConnReceiver);
    }

    private static boolean isWifiAvailable(Context context) {
        ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return wifi.isConnected();
    }





}
