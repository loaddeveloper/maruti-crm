package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SalesBookingDataModel extends SuperClassCastBean
{

    /**
     * Message : Success
     * Data : [{"sno":1,"id":"d218911c-8340-4bb5-9517-0b1a3a057d5b","enquirynumber":"10375-01-SENQ-0121-18912","enquiry_open_date":"2021-01-11 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-22T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"TAJ SINGH","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"40","gender":"M","model_interested_in":"GLAMOUR","model":"GLAMOUR","exchange_required":"N","finance_required":"N","dse_name":"SHARMA SANJAY","position_of_executive":"DSE","enquiry_id":"2-B4CJ1PC","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-15 17:49:17.0000000","enquiry_comments":"SANJAYS~","dse_employee_id":"10375-01-EMP-5","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":2,"id":"7d9acc9b-8cb8-4e24-ba8d-0d6be40fd778","enquirynumber":"10375-01-SENQ-0121-18901","enquiry_open_date":"2021-01-11 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-17T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"AJAY .","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"58","gender":"M","model_interested_in":"SPLENDOR +","model":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":". SAPNA","position_of_executive":"DSE","enquiry_id":"2-B454H44","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-15 19:20:38.0000000","enquiry_comments":"SAPNA10375~","dse_employee_id":"10375-04-EMP-48","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":3,"id":"9819e13a-479a-4252-a9e7-10b1c8d8c824","enquirynumber":"10375-01-SENQ-1220-17683","enquiry_open_date":"2020-12-28 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-20T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"RAJESH KR","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"46","gender":"M","model_interested_in":"DESTINI 125","model":"DESTINI 125","exchange_required":"N","finance_required":"Y","dse_name":"SHARMA SANJAY","position_of_executive":"DSE","enquiry_id":"2-ASKZU56","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-08 22:47:28.0000000","enquiry_comments":"SANJAYS~","dse_employee_id":"10375-01-EMP-5","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":4,"id":"343f2e0e-4efb-43ac-ab67-281363a4c474","enquirynumber":"10375-01-SENQ-0121-18375","enquiry_open_date":"2021-01-03 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-24T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"SAURAV .","mobilenumber":"1111111111","emailid":"","address":"EAST DELHI,EAST DELHI,EAST DELHI,(DELHI)","age":"29","gender":"M","model_interested_in":"SPLENDOR +","model":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":"PANCHAL RAVI","position_of_executive":"DSE","enquiry_id":"2-AY3PQW4","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-08 16:53:46.0000000","enquiry_comments":"RAVIPAN10375~","dse_employee_id":"10375-02-EMP-166","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"Walk-In","awareness_source":"News Paper","opinion_leader":"","financier":"","calledstatus":""},{"sno":5,"id":"a57e1f84-8b0e-4ed9-ad35-2ae14f908c14","enquirynumber":"10375-01-SENQ-0321-19320","enquiry_open_date":"2021-03-01 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-03-18T00:00:00","expected_date_purchase":"2021-03-20 00:00:00.0000000","cusotmer_first_and_last_name":"MINTU VARMA","mobilenumber":"7566541481","emailid":"","address":"GUNNOR,BARHA KALAN,PANNA,(MP)","age":"24","gender":"M","model_interested_in":"PASSION PRO 110","model":"PASSION PRO 110","exchange_required":"N","finance_required":"Y","dse_name":"GOEL ANIL","position_of_executive":"DSE","enquiry_id":"2-BA5URGZ","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-03-01 18:39:13.0000000","enquiry_comments":"","dse_employee_id":"1-65430585","existing_vehicle":"Two Wheeler","test_ride_required":"","test_ride_required_time":"","test_ride_taken":"","test_ride_taken_time":"","enquiry_source":"Showroom Referral","awareness_source":"TV","opinion_leader":"","financier":"HERO","calledstatus":""},{"sno":6,"id":"7e826f4e-8cf0-4c82-abed-3e487e21547b","enquirynumber":"10375-01-SENQ-0121-19060","enquiry_open_date":"2021-01-14 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-25T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"ANWAR .","mobilenumber":"1111111111","emailid":"","address":"NORTH EAST DELHI,NORTH EAST DELHI,NORTH EAST DELHI,(DELHI)","age":"28","gender":"M","model_interested_in":"PLEASURE+","model":"PLEASURE+","exchange_required":"N","finance_required":"Y","dse_name":"CHAUDHARY ASHOK","position_of_executive":"DSE","enquiry_id":"2-B6RXH8C","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-15 17:55:19.0000000","enquiry_comments":"ASHOK10375~","dse_employee_id":"10375-04-EMP-31","existing_vehicle":"First Time Buyer","test_ride_required":"Y","test_ride_required_time":"","test_ride_taken":"Y","test_ride_taken_time":"","enquiry_source":"","awareness_source":"News Paper","opinion_leader":"","financier":"","calledstatus":""},{"sno":7,"id":"34cccd74-170b-452b-8101-ac8a8d973347","enquirynumber":"10375-01-SENQ-0121-18471","enquiry_open_date":"2021-01-06 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-22T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"MOHD YUSAF","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"45","gender":"M","model_interested_in":"SPLENDOR +","model":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":"SHARMA SANJAY","position_of_executive":"DSE","enquiry_id":"2-B05LY8A","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-08 22:54:35.0000000","enquiry_comments":"SANJAYS~","dse_employee_id":"10375-01-EMP-5","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":8,"id":"ece28969-07bf-4c29-8613-af28dd81a741","enquirynumber":"10375-01-SENQ-0121-18707","enquiry_open_date":"2021-01-08 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-02-02T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"VIPIN .","mobilenumber":"1111111111","emailid":"","address":"EAST DELHI,EAST DELHI,EAST DELHI,(DELHI)","age":"29","gender":"M","model_interested_in":"SPLENDOR +","model":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":"PANCHAL RAVI","position_of_executive":"DSE","enquiry_id":"2-B2JRAO6","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-15 21:17:15.0000000","enquiry_comments":"RAVIPAN10375~","dse_employee_id":"10375-02-EMP-166","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"Cloud Telephony","awareness_source":"Hoarding","opinion_leader":"","financier":"","calledstatus":""},{"sno":9,"id":"9f871a87-709a-4cf7-90b6-b7374ea51038","enquirynumber":"10375-01-SENQ-1220-17893","enquiry_open_date":"2020-12-30 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-18T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"SUNIL .","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"45","gender":"M","model_interested_in":"GLAMOUR","model":"GLAMOUR","exchange_required":"N","finance_required":"N","dse_name":"BHALLA SONIA","position_of_executive":"DSE","enquiry_id":"2-AUEM00S","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-08 22:59:33.0000000","enquiry_comments":"BHALLA10375~","dse_employee_id":"10375-04-EMP-63","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""},{"sno":10,"id":"266dac64-08da-471d-9ee3-dd715e03a3c9","enquirynumber":"10375-01-SENQ-1220-16436","enquiry_open_date":"2020-12-10 00:00:00.0000000","enquiry_status":"Open","next_followup_date":"2021-01-28T00:00:00","expected_date_purchase":"2021-03-19 00:00:00.0000000","cusotmer_first_and_last_name":"DEVINDER .","mobilenumber":"1111111111","emailid":"","address":"DELHI,DELHI,DELHI,(DELHI)","age":"25","gender":"M","model_interested_in":"SPLENDOR +","model":"SPLENDOR +","exchange_required":"N","finance_required":"N","dse_name":". SAPNA","position_of_executive":"DSE","enquiry_id":"2-ADGJ6Z0","dealer_name":"HIMGIRI AUTOMOBILES (P) LTD.","last_follow_up_date_if_any":"2021-01-15 21:51:15.0000000","enquiry_comments":"SAPNA10375~","dse_employee_id":"10375-04-EMP-48","existing_vehicle":"First Time Buyer","test_ride_required":"N","test_ride_required_time":"","test_ride_taken":"N","test_ride_taken_time":"","enquiry_source":"Cloud Telephony","awareness_source":"","opinion_leader":"","financier":"","calledstatus":""}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * sno : 1
         * id : d218911c-8340-4bb5-9517-0b1a3a057d5b
         * enquirynumber : 10375-01-SENQ-0121-18912
         * enquiry_open_date : 2021-01-11 00:00:00.0000000
         * enquiry_status : Open
         * next_followup_date : 2021-01-22T00:00:00
         * expected_date_purchase : 2021-03-19 00:00:00.0000000
         * cusotmer_first_and_last_name : TAJ SINGH
         * mobilenumber : 1111111111
         * emailid :
         * address : DELHI,DELHI,DELHI,(DELHI)
         * age : 40
         * gender : M
         * model_interested_in : GLAMOUR
         * model : GLAMOUR
         * exchange_required : N
         * finance_required : N
         * dse_name : SHARMA SANJAY
         * position_of_executive : DSE
         * enquiry_id : 2-B4CJ1PC
         * dealer_name : HIMGIRI AUTOMOBILES (P) LTD.
         * last_follow_up_date_if_any : 2021-01-15 17:49:17.0000000
         * enquiry_comments : SANJAYS~
         * dse_employee_id : 10375-01-EMP-5
         * existing_vehicle : First Time Buyer
         * test_ride_required : N
         * test_ride_required_time :
         * test_ride_taken : N
         * test_ride_taken_time :
         * enquiry_source :
         * awareness_source :
         * opinion_leader :
         * financier :
         * calledstatus :
         */

        @SerializedName("sno")
        private Integer sno;
        @SerializedName("id")
        private String id;
        @SerializedName("enquirynumber")
        private String enquirynumber;
        @SerializedName("enquiry_open_date")
        private String enquiryOpenDate;
        @SerializedName("enquiry_status")
        private String enquiryStatus;
        @SerializedName("next_followup_date")
        private String nextFollowupDate;
        @SerializedName("expected_date_purchase")
        private String expectedDatePurchase;
        @SerializedName("cusotmer_first_and_last_name")
        private String cusotmerFirstAndLastName;
        @SerializedName("mobilenumber")
        private String mobilenumber;
        @SerializedName("emailid")
        private String emailid;
        @SerializedName("address")
        private String address;
        @SerializedName("age")
        private String age;
        @SerializedName("gender")
        private String gender;
        @SerializedName("model_interested_in")
        private String modelInterestedIn;
        @SerializedName("model")
        private String model;
        @SerializedName("exchange_required")
        private String exchangeRequired;
        @SerializedName("finance_required")
        private String financeRequired;
        @SerializedName("dse_name")
        private String dseName;
        @SerializedName("position_of_executive")
        private String positionOfExecutive;
        @SerializedName("enquiry_id")
        private String enquiryId;
        @SerializedName("dealer_name")
        private String dealerName;
        @SerializedName("last_follow_up_date_if_any")
        private String lastFollowUpDateIfAny;
        @SerializedName("enquiry_comments")
        private String enquiryComments;
        @SerializedName("dse_employee_id")
        private String dseEmployeeId;
        @SerializedName("existing_vehicle")
        private String existingVehicle;
        @SerializedName("test_ride_required")
        private String testRideRequired;
        @SerializedName("test_ride_required_time")
        private String testRideRequiredTime;
        @SerializedName("test_ride_taken")
        private String testRideTaken;
        @SerializedName("test_ride_taken_time")
        private String testRideTakenTime;
        @SerializedName("enquiry_source")
        private String enquirySource;
        @SerializedName("awareness_source")
        private String awarenessSource;
        @SerializedName("opinion_leader")
        private String opinionLeader;
        @SerializedName("financier")
        private String financier;
        @SerializedName("calledstatus")
        private String calledstatus;

        public Integer getSno() {
            return sno;
        }

        public void setSno(Integer sno) {
            this.sno = sno;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEnquirynumber() {
            return enquirynumber;
        }

        public void setEnquirynumber(String enquirynumber) {
            this.enquirynumber = enquirynumber;
        }

        public String getEnquiryOpenDate() {
            return enquiryOpenDate;
        }

        public void setEnquiryOpenDate(String enquiryOpenDate) {
            this.enquiryOpenDate = enquiryOpenDate;
        }

        public String getEnquiryStatus() {
            return enquiryStatus;
        }

        public void setEnquiryStatus(String enquiryStatus) {
            this.enquiryStatus = enquiryStatus;
        }

        public String getNextFollowupDate() {
            return nextFollowupDate;
        }

        public void setNextFollowupDate(String nextFollowupDate) {
            this.nextFollowupDate = nextFollowupDate;
        }

        public String getExpectedDatePurchase() {
            return expectedDatePurchase;
        }

        public void setExpectedDatePurchase(String expectedDatePurchase) {
            this.expectedDatePurchase = expectedDatePurchase;
        }

        public String getCusotmerFirstAndLastName() {
            return cusotmerFirstAndLastName;
        }

        public void setCusotmerFirstAndLastName(String cusotmerFirstAndLastName) {
            this.cusotmerFirstAndLastName = cusotmerFirstAndLastName;
        }

        public String getMobilenumber() {
            return mobilenumber;
        }

        public void setMobilenumber(String mobilenumber) {
            this.mobilenumber = mobilenumber;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getModelInterestedIn() {
            return modelInterestedIn;
        }

        public void setModelInterestedIn(String modelInterestedIn) {
            this.modelInterestedIn = modelInterestedIn;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getExchangeRequired() {
            return exchangeRequired;
        }

        public void setExchangeRequired(String exchangeRequired) {
            this.exchangeRequired = exchangeRequired;
        }

        public String getFinanceRequired() {
            return financeRequired;
        }

        public void setFinanceRequired(String financeRequired) {
            this.financeRequired = financeRequired;
        }

        public String getDseName() {
            return dseName;
        }

        public void setDseName(String dseName) {
            this.dseName = dseName;
        }

        public String getPositionOfExecutive() {
            return positionOfExecutive;
        }

        public void setPositionOfExecutive(String positionOfExecutive) {
            this.positionOfExecutive = positionOfExecutive;
        }

        public String getEnquiryId() {
            return enquiryId;
        }

        public void setEnquiryId(String enquiryId) {
            this.enquiryId = enquiryId;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public String getLastFollowUpDateIfAny() {
            return lastFollowUpDateIfAny;
        }

        public void setLastFollowUpDateIfAny(String lastFollowUpDateIfAny) {
            this.lastFollowUpDateIfAny = lastFollowUpDateIfAny;
        }

        public String getEnquiryComments() {
            return enquiryComments;
        }

        public void setEnquiryComments(String enquiryComments) {
            this.enquiryComments = enquiryComments;
        }

        public String getDseEmployeeId() {
            return dseEmployeeId;
        }

        public void setDseEmployeeId(String dseEmployeeId) {
            this.dseEmployeeId = dseEmployeeId;
        }

        public String getExistingVehicle() {
            return existingVehicle;
        }

        public void setExistingVehicle(String existingVehicle) {
            this.existingVehicle = existingVehicle;
        }

        public String getTestRideRequired() {
            return testRideRequired;
        }

        public void setTestRideRequired(String testRideRequired) {
            this.testRideRequired = testRideRequired;
        }

        public String getTestRideRequiredTime() {
            return testRideRequiredTime;
        }

        public void setTestRideRequiredTime(String testRideRequiredTime) {
            this.testRideRequiredTime = testRideRequiredTime;
        }

        public String getTestRideTaken() {
            return testRideTaken;
        }

        public void setTestRideTaken(String testRideTaken) {
            this.testRideTaken = testRideTaken;
        }

        public String getTestRideTakenTime() {
            return testRideTakenTime;
        }

        public void setTestRideTakenTime(String testRideTakenTime) {
            this.testRideTakenTime = testRideTakenTime;
        }

        public String getEnquirySource() {
            return enquirySource;
        }

        public void setEnquirySource(String enquirySource) {
            this.enquirySource = enquirySource;
        }

        public String getAwarenessSource() {
            return awarenessSource;
        }

        public void setAwarenessSource(String awarenessSource) {
            this.awarenessSource = awarenessSource;
        }

        public String getOpinionLeader() {
            return opinionLeader;
        }

        public void setOpinionLeader(String opinionLeader) {
            this.opinionLeader = opinionLeader;
        }

        public String getFinancier() {
            return financier;
        }

        public void setFinancier(String financier) {
            this.financier = financier;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
