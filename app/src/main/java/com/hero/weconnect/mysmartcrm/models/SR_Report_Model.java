package com.hero.weconnect.mysmartcrm.models;

import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.Retrofit.SuperClassCastBean;

import java.util.List;

public class SR_Report_Model extends SuperClassCastBean {

    /**
     * Message : Success
     * Data : [{"TotalCustomer":287,"TotalCallsMade":0,"CallDone":0,"CallNotDone":0,"Followups":0,"Bookings":0,"NotComingReason":0,"NoResponse":0}]
     */

    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataDTO> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataDTO> getData() {
        return Data;
    }

    public void setData(List<DataDTO> Data) {
        this.Data = Data;
    }

    public static class DataDTO {
        /**
         * TotalCustomer : 287
         * TotalCallsMade : 0
         * CallDone : 0
         * CallNotDone : 0
         * Followups : 0
         * Bookings : 0
         * NotComingReason : 0
         * NoResponse : 0
         */

        @SerializedName("TotalCustomer")
        private int TotalCustomer;
        @SerializedName("TotalCallsMade")
        private int TotalCallsMade;
        @SerializedName("CallDone")
        private int CallDone;
        @SerializedName("CallNotDone")
        private int CallNotDone;
        @SerializedName("Followups")
        private int Followups;
        @SerializedName("Bookings")
        private int Bookings;
        @SerializedName("NotComingReason")
        private int NotComingReason;
        @SerializedName("NoResponse")
        private int NoResponse;

        public int getTotalCustomer() {
            return TotalCustomer;
        }

        public void setTotalCustomer(int TotalCustomer) {
            this.TotalCustomer = TotalCustomer;
        }

        public int getTotalCallsMade() {
            return TotalCallsMade;
        }

        public void setTotalCallsMade(int TotalCallsMade) {
            this.TotalCallsMade = TotalCallsMade;
        }

        public int getCallDone() {
            return CallDone;
        }

        public void setCallDone(int CallDone) {
            this.CallDone = CallDone;
        }

        public int getCallNotDone() {
            return CallNotDone;
        }

        public void setCallNotDone(int CallNotDone) {
            this.CallNotDone = CallNotDone;
        }

        public int getFollowups() {
            return Followups;
        }

        public void setFollowups(int Followups) {
            this.Followups = Followups;
        }

        public int getBookings() {
            return Bookings;
        }

        public void setBookings(int Bookings) {
            this.Bookings = Bookings;
        }

        public int getNotComingReason() {
            return NotComingReason;
        }

        public void setNotComingReason(int NotComingReason) {
            this.NotComingReason = NotComingReason;
        }

        public int getNoResponse() {
            return NoResponse;
        }

        public void setNoResponse(int NoResponse) {
            this.NoResponse = NoResponse;
        }
    }
}
