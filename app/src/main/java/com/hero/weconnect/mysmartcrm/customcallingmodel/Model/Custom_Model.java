package com.hero.weconnect.mysmartcrm.customcallingmodel.Model;



import com.google.gson.annotations.SerializedName;
import com.hero.weconnect.mysmartcrm.customretrofit.Retrofit.SuperClassCastBean;

import java.util.List;

public class Custom_Model extends SuperClassCastBean {


    /**
     * status : true
     * message : Data Fetch Successfully
     * Data : [{"Id":"f8535723-374d-48f0-aed9-04cf1854fa4e","Serial_No":11,"Data1":"LAKSHMANAN M","Data2":"9487951640","Data3":"CB UNICORN 160","Data4":"ME4KC20AEH8001766","Data5":"TN32AL0176","Data6":"","Data7":"28-11-2020 12:00:00 AM","Data8":"MINOR REPAIRS","Data9":"24-06-2017 12:00:00 AM","Data10":"","Data11":"","Data12":"TN220002","Data13":"TN220002","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"087fa039-47da-4835-b96e-06b793754d2e","Serial_No":12,"Data1":"RAMARAJ K","Data2":"6383134478","Data3":"ACTIVA 6G","Data4":"ME4JF913AMG333991","Data5":"","Data6":"","Data7":"","Data8":"","Data9":"26-01-2021 12:00:00 AM","Data10":"","Data11":"","Data12":"","Data13":"","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"fded27f0-c2d6-4fe2-a9f7-07db9fbde9cf","Serial_No":13,"Data1":"ELUMALAI R","Data2":"9791399017","Data3":"X BLADE","Data4":"ME4KC356LLA015300","Data5":"","Data6":"","Data7":"","Data8":"","Data9":"21-01-2021 12:00:00 AM","Data10":"","Data11":"","Data12":"","Data13":"","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"3e1c76a7-52e3-4b84-9e51-07e7b6ed9819","Serial_No":14,"Data1":"RAMESH K","Data2":"7540000384","Data3":"GRAZIA","Data4":"ME4JF49HLKW029339","Data5":"TN45BR4219","Data6":"","Data7":"15-10-2020 12:00:00 AM","Data8":"GENERAL REPAIRS","Data9":"12-12-2019 12:00:00 AM","Data10":"","Data11":"","Data12":"TN220002","Data13":"TN220002","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"3f5af61a-e80b-4b33-820f-07e83d1132b9","Serial_No":15,"Data1":"VELMARAN A","Data2":"9626805959","Data3":"X BLADE","Data4":"ME4KC351EJ8022552","Data5":"TN32AQ0308","Data6":"","Data7":"31-10-2020 12:00:00 AM","Data8":"PAID","Data9":"26-11-2018 12:00:00 AM","Data10":"","Data11":"","Data12":"TN010002","Data13":"TN010002","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"13ea0501-a353-4ac2-bf4c-09f2091faf5a","Serial_No":16,"Data1":"MOORTHY J","Data2":"9486432623","Data3":"DREAM NEO","Data4":"ME4JC623HD8046272","Data5":"TN32AD0169","Data6":"","Data7":"15-10-2019 12:00:00 AM","Data8":"PAID","Data9":"04-09-2013 12:00:00 AM","Data10":"","Data11":"","Data12":"TN220002","Data13":"TN220002","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"8bfb813e-d442-4bdf-bde9-0a061c488fea","Serial_No":17,"Data1":"SABARI RAJ S","Data2":"6380523338","Data3":"CLIQ","Data4":"ME4JF762JH7011224","Data5":"TN91E9450","Data6":"","Data7":"23-01-2021 12:00:00 AM","Data8":"MINOR REPAIRS","Data9":"29-08-2018 12:00:00 AM","Data10":"","Data11":"","Data12":"TN38BA01","Data13":"TN380001","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"2d98e97d-0806-4012-bfe8-0a4d93a72b07","Serial_No":18,"Data1":"DHANAJAYAN P","Data2":"9791557253","Data3":"CB UNICORN PREMIUM","Data4":"ME4KC315EKA026927","Data5":"TN32AR0209","Data6":"","Data7":"17-06-2020 12:00:00 AM","Data8":"FREE 04","Data9":"31-05-2019 12:00:00 AM","Data10":"","Data11":"","Data12":"TN010004","Data13":"TN010004","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"63967c6a-f31f-48dc-bd12-0bd645f39ed2","Serial_No":19,"Data1":"KUMARAKRISHNAN B","Data2":"9659493952","Data3":"CB UNICORN","Data4":"ME4KC311KH8138893","Data5":"TN32AL7646","Data6":"","Data7":"31-12-2018 12:00:00 AM","Data8":"GENERAL REPAIRS","Data9":"07-11-2017 12:00:00 AM","Data10":"","Data11":"","Data12":"TN220002","Data13":"TN220002","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""},{"Id":"c1eda4d2-29ce-4006-b3ea-0c03283efd02","Serial_No":20,"Data1":"ELUMALAI T","Data2":"9840099503","Data3":"ACTIVA 5G","Data4":"ME4JF50BEJT069801","Data5":"TN32AP0973","Data6":"","Data7":"25-10-2019 12:00:00 AM","Data8":"PAID","Data9":"04-07-2018 12:00:00 AM","Data10":"","Data11":"","Data12":"TN22SB02","Data13":"TN22SB02","Data14":"","Data15":"TN220002","Data16":"","Data17":"","Data18":"","Is_Closed":"NO","calledstatus":""}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Id : f8535723-374d-48f0-aed9-04cf1854fa4e
         * Serial_No : 11
         * Data1 : LAKSHMANAN M
         * Data2 : 9487951640
         * Data3 : CB UNICORN 160
         * Data4 : ME4KC20AEH8001766
         * Data5 : TN32AL0176
         * Data6 :
         * Data7 : 28-11-2020 12:00:00 AM
         * Data8 : MINOR REPAIRS
         * Data9 : 24-06-2017 12:00:00 AM
         * Data10 :
         * Data11 :
         * Data12 : TN220002
         * Data13 : TN220002
         * Data14 :
         * Data15 : TN220002
         * Data16 :
         * Data17 :
         * Data18 :
         * Is_Closed : NO
         * calledstatus :
         */

        @SerializedName("Id")
        private String Id;
        @SerializedName("Serial_No")
        private int serialNo;
        @SerializedName("Data1")
        private String Data1;
        @SerializedName("Data2")
        private String Data2;
        @SerializedName("Data3")
        private String Data3;
        @SerializedName("Data4")
        private String Data4;
        @SerializedName("Data5")
        private String Data5;
        @SerializedName("Data6")
        private String Data6;
        @SerializedName("Data7")
        private String Data7;
        @SerializedName("Data8")
        private String Data8;
        @SerializedName("Data9")
        private String Data9;
        @SerializedName("Data10")
        private String Data10;
        @SerializedName("Data11")
        private String Data11;
        @SerializedName("Data12")
        private String Data12;
        @SerializedName("Data13")
        private String Data13;
        @SerializedName("Data14")
        private String Data14;
        @SerializedName("Data15")
        private String Data15;
        @SerializedName("Data16")
        private String Data16;
        @SerializedName("Data17")
        private String Data17;
        @SerializedName("Data18")
        private String Data18;
        @SerializedName("Is_Closed")
        private String isClosed;
        @SerializedName("calledstatus")
        private String calledstatus;

        @SerializedName("DateValue")
        private int DateValue;

        public int getDateValue() {
            return DateValue;
        }

        public void setDateValue(int dateValue) {
            DateValue = dateValue;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }

        public int getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(int serialNo) {
            this.serialNo = serialNo;
        }

        public String getData1() {
            return Data1;
        }

        public void setData1(String Data1) {
            this.Data1 = Data1;
        }

        public String getData2() {
            return Data2;
        }

        public void setData2(String Data2) {
            this.Data2 = Data2;
        }

        public String getData3() {
            return Data3;
        }

        public void setData3(String Data3) {
            this.Data3 = Data3;
        }

        public String getData4() {
            return Data4;
        }

        public void setData4(String Data4) {
            this.Data4 = Data4;
        }

        public String getData5() {
            return Data5;
        }

        public void setData5(String Data5) {
            this.Data5 = Data5;
        }

        public String getData6() {
            return Data6;
        }

        public void setData6(String Data6) {
            this.Data6 = Data6;
        }

        public String getData7() {
            return Data7;
        }

        public void setData7(String Data7) {
            this.Data7 = Data7;
        }

        public String getData8() {
            return Data8;
        }

        public void setData8(String Data8) {
            this.Data8 = Data8;
        }

        public String getData9() {
            return Data9;
        }

        public void setData9(String Data9) {
            this.Data9 = Data9;
        }

        public String getData10() {
            return Data10;
        }

        public void setData10(String Data10) {
            this.Data10 = Data10;
        }

        public String getData11() {
            return Data11;
        }

        public void setData11(String Data11) {
            this.Data11 = Data11;
        }

        public String getData12() {
            return Data12;
        }

        public void setData12(String Data12) {
            this.Data12 = Data12;
        }

        public String getData13() {
            return Data13;
        }

        public void setData13(String Data13) {
            this.Data13 = Data13;
        }

        public String getData14() {
            return Data14;
        }

        public void setData14(String Data14) {
            this.Data14 = Data14;
        }

        public String getData15() {
            return Data15;
        }

        public void setData15(String Data15) {
            this.Data15 = Data15;
        }

        public String getData16() {
            return Data16;
        }

        public void setData16(String Data16) {
            this.Data16 = Data16;
        }

        public String getData17() {
            return Data17;
        }

        public void setData17(String Data17) {
            this.Data17 = Data17;
        }

        public String getData18() {
            return Data18;
        }

        public void setData18(String Data18) {
            this.Data18 = Data18;
        }

        public String getIsClosed() {
            return isClosed;
        }

        public void setIsClosed(String isClosed) {
            this.isClosed = isClosed;
        }

        public String getCalledstatus() {
            return calledstatus;
        }

        public void setCalledstatus(String calledstatus) {
            this.calledstatus = calledstatus;
        }
    }
}
