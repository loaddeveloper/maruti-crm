package com.hero.weconnect.mysmartcrm.Retrofit;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.IntentCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hero.weconnect.mysmartcrm.R;
import com.hero.weconnect.mysmartcrm.Utils.CommonVariables;
import com.hero.weconnect.mysmartcrm.activity.S_R_Activity;
import com.hero.weconnect.mysmartcrm.activity.Sales_Followup_Activity;
import com.hero.weconnect.mysmartcrm.models.AddCallHistoryUpdateModel;
import com.hero.weconnect.mysmartcrm.models.AllocationDataModel;
import com.hero.weconnect.mysmartcrm.models.AllotedDSEModel;
import com.hero.weconnect.mysmartcrm.models.ApiController_Interface;
import com.hero.weconnect.mysmartcrm.models.BRCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.BR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.BR_Report_Model;
import com.hero.weconnect.mysmartcrm.models.ClouserReasonModel;
import com.hero.weconnect.mysmartcrm.models.ClouserSubReasonModel;
import com.hero.weconnect.mysmartcrm.models.CommonModel;
import com.hero.weconnect.mysmartcrm.models.DataAllotedModel;
import com.hero.weconnect.mysmartcrm.models.GetNonTurnUpHistoryModel;
import com.hero.weconnect.mysmartcrm.models.NonTurnUpDataModel;
import com.hero.weconnect.mysmartcrm.models.PSFDataModel;
import com.hero.weconnect.mysmartcrm.models.PSFDissatisfactionReasonModel;
import com.hero.weconnect.mysmartcrm.models.PSFSOPRatingQuestionsModel;
import com.hero.weconnect.mysmartcrm.models.PSFSingleDataModel;
import com.hero.weconnect.mysmartcrm.models.SRCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.SR_Alloted_data_model;
import com.hero.weconnect.mysmartcrm.models.SR_Report_Model;
import com.hero.weconnect.mysmartcrm.models.SalesBookingDataModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryCallDoneDataModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryDataBR;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_NonTurnUp;
import com.hero.weconnect.mysmartcrm.models.UserNameListModel;
import com.hero.weconnect.mysmartcrm.models.ContactStatusModel;
import com.hero.weconnect.mysmartcrm.models.CreateFolloupModel;
import com.hero.weconnect.mysmartcrm.models.CustomerReplyModel;
import com.hero.weconnect.mysmartcrm.models.FilterDataSendModel;
import com.hero.weconnect.mysmartcrm.models.FollowUpListModel;
import com.hero.weconnect.mysmartcrm.models.GetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.GetTamplateListModel;
import com.hero.weconnect.mysmartcrm.models.GetVersionModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_SalesModel;
import com.hero.weconnect.mysmartcrm.models.Incoming_Service_ReminderModel;
import com.hero.weconnect.mysmartcrm.models.MakeModel;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_SR;
import com.hero.weconnect.mysmartcrm.models.MissedDataModel_Sales;
import com.hero.weconnect.mysmartcrm.models.ModelModel;
import com.hero.weconnect.mysmartcrm.models.NotComingReasonModel;
import com.hero.weconnect.mysmartcrm.models.ReportingModel;
import com.hero.weconnect.mysmartcrm.models.SR_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.SalesEnquiryDataModel;
import com.hero.weconnect.mysmartcrm.models.Sales_HistoryModel;
import com.hero.weconnect.mysmartcrm.models.ServiceReminderDataModel;
import com.hero.weconnect.mysmartcrm.models.SetSMSTemplateCommonModel;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData;
import com.hero.weconnect.mysmartcrm.models.SingleCustomerHistoryData_Sales;
import com.hero.weconnect.mysmartcrm.models.TokenModel;
import com.hero.weconnect.mysmartcrm.models.UploadRecordingModel;
import com.hero.weconnect.mysmartcrm.models.UserDetailsModel;
import com.hero.weconnect.mysmartcrm.models.booking_reminder_model;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Thread.sleep;


public class ApiController{
    private APIInterface apiInterface;
    private ApiResponseListener apiResponseListener;
    Context context;
    CommonSharedPref commonSharedPref;


    public ApiController(ApiResponseListener apiResponseListener,Context context) {
        this.context=context;
        this.apiResponseListener = apiResponseListener;
        commonSharedPref = new CommonSharedPref(context);

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }

    }


    public void getToken(String username, String password) {
        APIInterface apiInterface = APIClientMain.getClienttoken().create(APIInterface.class);
        Call<TokenModel> call = apiInterface.getToken(username, password, "password");
        call.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }



                if (response.body() != null) {
                    TokenModel tokenModel = response.body();
                    SuperClassCastBean superClassCastBean = tokenModel;

                    apiResponseListener.onSuccess(ApiConstant.TOKEN_TAG, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    public void getContactStatus(String token) {
        Call<ContactStatusModel> call = apiInterface.getContactStatus(token);
        call.enqueue(new Callback<ContactStatusModel>() {
            @Override
            public void onResponse(Call<ContactStatusModel> call, Response<ContactStatusModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    ContactStatusModel contactStatusModel = response.body();
                    SuperClassCastBean superClassCastBean = contactStatusModel;

                    apiResponseListener.onSuccess(ApiConstant.CONTACT_STATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ContactStatusModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getMake(String token) {
        Call<MakeModel> call = apiInterface.getMake(token);
        call.enqueue(new Callback<MakeModel>() {
            @Override
            public void onResponse(Call<MakeModel> call, Response<MakeModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    MakeModel makeModel = response.body();
                    SuperClassCastBean superClassCastBean = makeModel;

                    apiResponseListener.onSuccess(ApiConstant.MAKE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MakeModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getModel(String token, HashMap<String, String> params) {
        Call<ModelModel> call = apiInterface.getModel(token, params);
        call.enqueue(new Callback<ModelModel>() {
            @Override
            public void onResponse(Call<ModelModel> call, Response<ModelModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    ModelModel modelModel = response.body();
                    SuperClassCastBean superClassCastBean = modelModel;

                    apiResponseListener.onSuccess(ApiConstant.MODEL, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ModelModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getCustomerReply(String token) {
        Call<CustomerReplyModel> call = apiInterface.getCustomerReply(token);
        call.enqueue(new Callback<CustomerReplyModel>() {
            @Override
            public void onResponse(Call<CustomerReplyModel> call, Response<CustomerReplyModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CustomerReplyModel customerReplyModel = response.body();
                    SuperClassCastBean superClassCastBean = customerReplyModel;

                    apiResponseListener.onSuccess(ApiConstant.CUSTOMER_REPLY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CustomerReplyModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getNotComingReason(String token) {
        Call<NotComingReasonModel> call = apiInterface.getNotComingReason(token);
        call.enqueue(new Callback<NotComingReasonModel>() {
            @Override
            public void onResponse(Call<NotComingReasonModel> call, Response<NotComingReasonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    NotComingReasonModel notComingReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = notComingReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.NOT_COMING_REASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<NotComingReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getClosureReason(String token) {
        Call<ClouserReasonModel> call = apiInterface.getClosureReason(token);
        call.enqueue(new Callback<ClouserReasonModel>() {
            @Override
            public void onResponse(Call<ClouserReasonModel> call, Response<ClouserReasonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    ClouserReasonModel clouserReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = clouserReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.CLOUSERREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ClouserReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void getClosureSubReason(String token, HashMap<String, String> params) {
        Call<ClouserSubReasonModel> call = apiInterface.getClosureSubReason(token, params);
        call.enqueue(new Callback<ClouserSubReasonModel>() {
            @Override
            public void onResponse(Call<ClouserSubReasonModel> call, Response<ClouserSubReasonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    ClouserSubReasonModel clouserSubReasonModel = response.body();
                    SuperClassCastBean superClassCastBean = clouserSubReasonModel;

                    apiResponseListener.onSuccess(ApiConstant.CLOUSERSUBREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ClouserSubReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Add History in Database API Code
    public void addCallHistoryData(String token, HashMap<String, String> params) {
        Call<AddCallHistoryUpdateModel> call = apiInterface.addCallHistoryUpdate(token, params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    AddCallHistoryUpdateModel addCallHistoryUpdateModel = response.body();
                    SuperClassCastBean superClassCastBean = addCallHistoryUpdateModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Contact Number  in Database for this function API Code
    public void UpdateContactNumber(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecontacts(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECONTACTNUMBERSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Customer Reply in Database for this function API Code
    public void UpdateCustomerReply(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecustomerreply(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECUSTOMERREPLYSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Taggged SR Status in Database for this function API Code
    public void UpdateTaggedSRStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatetaggedsrstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATETAGGERSRSTATUSSR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Booking Date and Time in Database for this function API Code
    public void UpdateBookingate(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatebookingdate(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEBOOKINGDATETIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Call End Time  in Database for this function API Code
    public void UpdateCallEndTime(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecallendtime(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLENDTIME, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Remark in Database for this function API Code
    public void UpdateRemark(String token, HashMap<String, String> params) {


        Call<CommonModel> call = apiInterface.updateremark(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEREMARK, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Next Follow-up date in Database for this function API Code
    public void UpdateNextFollowUpdate(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatefollowupdate(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEFOLLOWUPDATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Contatct  Status in Database for this function API Code
    public void UpdateContactStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecontactstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECONTACTSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Called  Status in Database for this function API Code
    public void UpdateCalledStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatecalledstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECALLEDSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    } // Update Not Coming Reasson in Database for this function API Code

    public void UpdateNotComingReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatenotconingreason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATENOTCOMINGREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get Service Reminder Data in this Function  API Code
    public void getServiceReminderData(String token, HashMap<String, String> params) {
        Call<ServiceReminderDataModel> call = apiInterface.getServiceReminderData(token, params);
        call.enqueue(new Callback<ServiceReminderDataModel>() {
            @Override
            public void onResponse(Call<ServiceReminderDataModel> call, Response<ServiceReminderDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.code()==500)
                {

                    Toast.makeText(context, "Something Went Wrong.Please try again..!!", Toast.LENGTH_SHORT).show();
                    S_R_Activity.srwaitingDialog.dismiss();

                }

                if (response.body() != null) {
                    ServiceReminderDataModel serviceReminderDataModel = response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SERVICEREMINDERDATA, superClassCastBean);
                } else {

                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ServiceReminderDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SERVICEREMINDERDATA);

            }
        });


    }

    // Get Sales Enquiry Data in this Function  API Code
    public void getSalesEnquiryData(String token, HashMap<String, String> params) {
        Call<SalesEnquiryDataModel> call = apiInterface.getSalesEnquiryData(token, params);
        call.enqueue(new Callback<SalesEnquiryDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryDataModel> call, Response<SalesEnquiryDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.code()==500)
                {

                    Toast.makeText(context, "Something Went Wrong.Please try again..!!", Toast.LENGTH_SHORT).show();
                    Sales_Followup_Activity.SALEScustomWaitingDialog.dismiss();

                }

                if (response.body() != null) {

                    if(response.body().getMessage() != null && response.body().getMessage().equals("Failed"))
                    {
                        apiResponseListener.onError(ApiConstant.SALESENQUIRYDATA);
                    }
                    SalesEnquiryDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESENQUIRYDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESENQUIRYDATA);
            }
        });


    }



    // Get Sales DateOfPurchase Data in this Function  API Code
    public void getSalesDateOfPurchaseData(String token, HashMap<String, String> params) {
        Call<SalesBookingDataModel> call = apiInterface.getSalesExpectedDateOfPurchase(token, params);
        call.enqueue(new Callback<SalesBookingDataModel>() {
            @Override
            public void onResponse(Call<SalesBookingDataModel> call, Response<SalesBookingDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {

                    if(response.body().getMessage() != null && response.body().getMessage().equals("Failed"))
                    {
                        apiResponseListener.onError(ApiConstant.BOOKINGSALESENQUIRYDATA);
                    }
                    SalesBookingDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.BOOKINGSALESENQUIRYDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesBookingDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.BOOKINGSALESENQUIRYDATA);
            }
        });


    }


    // Get Sales Enquiry Call Done Data in this Function  API Code
    public void getSalesEnquiryCallDoneData(String token, HashMap<String, String> params) {
        Call<SalesEnquiryCallDoneDataModel> call = apiInterface.getCalldoneData(token, params);
        call.enqueue(new Callback<SalesEnquiryCallDoneDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryCallDoneDataModel> call, Response<SalesEnquiryCallDoneDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SalesEnquiryCallDoneDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESENQUIRYCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryCallDoneDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESENQUIRYCALLDONEDATA);
            }
        });


    }


    // Get Sales Enquiry Call Done Data in this Function  API Code
    public void getExpectedSalesEnquiryCallDoneData(String token, HashMap<String, String> params) {
        Call<SalesEnquiryCallDoneDataModel> call = apiInterface.getExpectedCalldoneData(token, params);
        call.enqueue(new Callback<SalesEnquiryCallDoneDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryCallDoneDataModel> call, Response<SalesEnquiryCallDoneDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SalesEnquiryCallDoneDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESEXPECTEDENQUIRYCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryCallDoneDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESEXPECTEDENQUIRYCALLDONEDATA);
            }
        });


    }


    // Get Service Reminder History Data in this Function  API Code
    public void getSRHistory(String token, HashMap<String, String> params) {
        Call<SR_HistoryModel> call = apiInterface.getSRHistory(token, params);
        call.enqueue(new Callback<SR_HistoryModel>() {
            @Override
            public void onResponse(Call<SR_HistoryModel> call, Response<SR_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SR_HistoryModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SR_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Sales History Data in this Function  API Code
    public void getSalesHistory(String token, HashMap<String, String> params) {
        Call<Sales_HistoryModel> call = apiInterface.getSalesHistory(token, params);
        call.enqueue(new Callback<Sales_HistoryModel>() {
            @Override
            public void onResponse(Call<Sales_HistoryModel> call, Response<Sales_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    Sales_HistoryModel sales_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sales_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSALESCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Sales_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Get Expected Sales History Data in this Function  API Code
    public void getExpectedSalesHistory(String token, HashMap<String, String> params) {
        Call<Sales_HistoryModel> call = apiInterface.getExpectedSalesHistory(token, params);
        call.enqueue(new Callback<Sales_HistoryModel>() {
            @Override
            public void onResponse(Call<Sales_HistoryModel> call, Response<Sales_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    Sales_HistoryModel sales_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sales_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETEXPECTEDSALESCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Sales_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Get Expected SR History Data in this Function  API Code
    public void getExpectedSRHistory(String token, HashMap<String, String> params) {
        Call<SR_HistoryModel> call = apiInterface.getExpectedSRHistory(token, params);
        call.enqueue(new Callback<SR_HistoryModel>() {
            @Override
            public void onResponse(Call<SR_HistoryModel> call, Response<SR_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SR_HistoryModel sales_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sales_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETEXPECTEDSALESCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SR_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Update History Id in  Database for this function API Code
    public void UpdateHistorySales(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesaleshistoryId(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESHISTORYID, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Date of Purchase in Database for this function API Code
    public void Updatedateofpurchase(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatedateofpurchase(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEDATEOFPURCHASE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Reason in Database for this function API Code
    public void UpdateClosureReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateclosurereason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECLOSUREREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Sub Reason in Database for this function API Code
    public void UpdateClosureSubReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateclosuresubreason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATECLOSURESUBREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update Model in Database for this function API Code
    public void UpdateMake(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatemake(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEMAKE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Model in Database for this function API Code
    public void UpdateModel(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatemodel(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEMODEL, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Pick&Drop in Database for this function API Code
    public void UpdatePickdrop(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatepickdrop(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEPICKDROP, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Update SMS Status in Database for this function API Code
    public void UpdateSMSStatus(String token, HashMap<String, String> params) {
        Call<SetSMSTemplateCommonModel> call = apiInterface.updatesmsstatus(token, params);
        call.enqueue(new Callback<SetSMSTemplateCommonModel>() {
            @Override
            public void onResponse(Call<SetSMSTemplateCommonModel> call, Response<SetSMSTemplateCommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SetSMSTemplateCommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.SETSMSSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SetSMSTemplateCommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update SMS Template  in Database for this function API Code
    public void UpdateSMSTemplate(String token, HashMap<String, String> params) {
        Call<SetSMSTemplateCommonModel> call = apiInterface.updatesmstemplate(token, params);
        call.enqueue(new Callback<SetSMSTemplateCommonModel>() {
            @Override
            public void onResponse(Call<SetSMSTemplateCommonModel> call, Response<SetSMSTemplateCommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SetSMSTemplateCommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.SETSMSTEMPLATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SetSMSTemplateCommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get SMS Status in Database for this function API Code
    public void GetSMSStatus(String token) {
        Call<GetSMSTemplateCommonModel> call = apiInterface.getsmsstatus(token);
        call.enqueue(new Callback<GetSMSTemplateCommonModel>() {
            @Override
            public void onResponse(Call<GetSMSTemplateCommonModel> call, Response<GetSMSTemplateCommonModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    GetSMSTemplateCommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSMSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetSMSTemplateCommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get SMS Template  in Database for this function API Code
    public void GetSMSTemplate(String token, HashMap<String, String> params) {
        Call<GetTamplateListModel> call = apiInterface.getsmstemplate(token, params);
        call.enqueue(new Callback<GetTamplateListModel>() {
            @Override
            public void onResponse(Call<GetTamplateListModel> call, Response<GetTamplateListModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    GetTamplateListModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.GETSMSTEMPLATE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetTamplateListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Upload Call Recording Code
    public void UploadCallRecording(String token, RequestBody dealercode, RequestBody historyid,
                                    RequestBody contenttype, MultipartBody.Part recording, RequestBody calltype) {

        Call<UploadRecordingModel> call = apiInterface.uploadRecording(token, dealercode, historyid, contenttype, recording, calltype);
        call.enqueue(new Callback<UploadRecordingModel>() {
            @Override
            public void onResponse(Call<UploadRecordingModel> call, Response<UploadRecordingModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    UploadRecordingModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;




                    apiResponseListener.onSuccess(ApiConstant.UPLOADRECORDING, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UploadRecordingModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get Service Reminder Incoming  Data Fetch in this Function  API Code
    public void getSRIncomingData(String token, HashMap<String, String> params) {
        Call<Incoming_Service_ReminderModel> call = apiInterface.getSRIncomingdata(token, params);
        call.enqueue(new Callback<Incoming_Service_ReminderModel>() {
            @Override
            public void onResponse(Call<Incoming_Service_ReminderModel> call, Response<Incoming_Service_ReminderModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    Incoming_Service_ReminderModel incoming_service_reminderModel = response.body();
                    SuperClassCastBean superClassCastBean = incoming_service_reminderModel;

                    apiResponseListener.onSuccess(ApiConstant.SRINCOMINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Incoming_Service_ReminderModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Enquiry Incoming  Data Fetch in this Function  API Code
    public void getEnquiryIncomingData(String token, HashMap<String, String> params)
    {
        Call<Incoming_SalesModel> call = apiInterface.getEnquiryIncomingdata(token, params);
        call.enqueue(new Callback<Incoming_SalesModel>() {
            @Override
            public void onResponse(Call<Incoming_SalesModel> call, Response<Incoming_SalesModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    Incoming_SalesModel incoming_salesModel = response.body();
                    SuperClassCastBean superClassCastBean = incoming_salesModel;

                    apiResponseListener.onSuccess(ApiConstant.ENQUIRYINCOMINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<Incoming_SalesModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    public void deAllocation(String token,HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.setDeAllocation(token,params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.DEALLOCATION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Get Missed Data in this Function  API Code
    public void getMissedData(String token, HashMap<String, String> params) {
        Call<MissedDataModel_SR> call = apiInterface.getMissedData(token, params);
        call.enqueue(new Callback<MissedDataModel_SR>() {
            @Override
            public void onResponse(Call<MissedDataModel_SR> call, Response<MissedDataModel_SR> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    MissedDataModel_SR missedDataModelSR = response.body();
                    SuperClassCastBean superClassCastBean = missedDataModelSR;

                    apiResponseListener.onSuccess(ApiConstant.MISSEDDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MissedDataModel_SR> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.MISSEDDATA);
            }
        });


    }

    // Get Missed Data Sales in this Function  API Code
    public void getMissedDataSales(String token, HashMap<String, String> params) {
        Call<MissedDataModel_Sales> call = apiInterface.getMissedDataSales(token, params);
        call.enqueue(new Callback<MissedDataModel_Sales>() {
            @Override
            public void onResponse(Call<MissedDataModel_Sales> call, Response<MissedDataModel_Sales> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    MissedDataModel_Sales missedDataModel_sales = response.body();
                    SuperClassCastBean superClassCastBean = missedDataModel_sales;

                    apiResponseListener.onSuccess(ApiConstant.MISSEDDATASALES, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<MissedDataModel_Sales> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.MISSEDDATASALES);
            }
        });


    }

    // Get Single Customer history Data in this Function  API Code
    public void getSingleCustomerHistoryData(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData> call = apiInterface.getsinglecustomerhistory(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData> call, Response<SingleCustomerHistoryData> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SingleCustomerHistoryData singleCustomerHistoryData = response.body();
                    SuperClassCastBean superClassCastBean = singleCustomerHistoryData;

                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });




    }



    // Get Single Customer history Data in this Function  API Code
    public void getSingleCustomerHistoryDataBR(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryDataBR> call = apiInterface.getsinglecustomerhistoryBR(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryDataBR>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryDataBR> call, Response<SingleCustomerHistoryDataBR> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SingleCustomerHistoryDataBR singleCustomerHistoryDataBR = response.body();
                    SuperClassCastBean superClassCastBeannew = singleCustomerHistoryDataBR;


                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATABR, superClassCastBeannew);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryDataBR> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }




    // Get Single Customer PSF history Data in this Function  API Code
    public void getSingleCustomerHistoryDataPSF(String token, HashMap<String, String> params) {
        Call<PSFSingleDataModel> call = apiInterface.getsinglecustomerhistoryPSF(token, params);
        call.enqueue(new Callback<PSFSingleDataModel>() {
            @Override
            public void onResponse(Call<PSFSingleDataModel> call, Response<PSFSingleDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    PSFSingleDataModel singleCustomerHistoryDataBR = response.body();
                    SuperClassCastBean superClassCastBeannew = singleCustomerHistoryDataBR;


                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATAPSF, superClassCastBeannew);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<PSFSingleDataModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Get Single Customer history Data in this Function  API Code
    public void getSingleCustomerHistoryDataNonTurnUp(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData_NonTurnUp> call = apiInterface.getsinglecustomernonturnuphistory(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData_NonTurnUp>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData_NonTurnUp> call, Response<SingleCustomerHistoryData_NonTurnUp> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SingleCustomerHistoryData_NonTurnUp singleCustomerHistoryDataBR = response.body();
                    SuperClassCastBean superClassCastBeannew = singleCustomerHistoryDataBR;


                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATANONTURNUP, superClassCastBeannew);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData_NonTurnUp> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }




    // Get Single Customer BR history Data in this Function  API Code
    public void getSingleCustomerHistoryData_Sales(String token, HashMap<String, String> params) {
        Call<SingleCustomerHistoryData_Sales> call = apiInterface.getsinglecustomerhistorysales(token, params);
        call.enqueue(new Callback<SingleCustomerHistoryData_Sales>() {
            @Override
            public void onResponse(Call<SingleCustomerHistoryData_Sales> call, Response<SingleCustomerHistoryData_Sales> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //  Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SingleCustomerHistoryData_Sales singleCustomerHistoryData = response.body();
                    SuperClassCastBean superClassCastBean = singleCustomerHistoryData;

                    apiResponseListener.onSuccess(ApiConstant.SINGLEDATASALES, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SingleCustomerHistoryData_Sales> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    public void addMissedNumber(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.addtMissedNumber(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.CommonModel, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Add Exception  in Database for this function API Code
    public void addException(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.AddException(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDEXCEPTION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get Follow Up Done List
    public void getFollowUpDoneList(String token) {
        Call<FollowUpListModel> call = apiInterface.getSalesFollowupDoneList(token);
        call.enqueue(new Callback<FollowUpListModel>() {
            @Override
            public void onResponse(Call<FollowUpListModel> call, Response<FollowUpListModel> response) {

                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    FollowUpListModel followUpListModel = response.body();
                    SuperClassCastBean superClassCastBean = followUpListModel;

                    apiResponseListener.onSuccess(ApiConstant.GETFOLLOWUPDONELIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FollowUpListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get Follow Up Conatct Status List
    public void getFollowContatctStatusList(String token) {
        Call<FollowUpListModel> call = apiInterface.getSalesFollowupConatctList(token);
        call.enqueue(new Callback<FollowUpListModel>() {
            @Override
            public void onResponse(Call<FollowUpListModel> call, Response<FollowUpListModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //  Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    FollowUpListModel followUpListModel = response.body();
                    SuperClassCastBean superClassCastBean = followUpListModel;

                    apiResponseListener.onSuccess(ApiConstant.GETFOLLOWUPSTATUSLIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<FollowUpListModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleFollowUpStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesalescontactstatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleFollowUpDoneStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesfollowupdonestatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPDONESTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    public void createFolloup(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.setCreateFollowup(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.CREATEFOLLOWUP, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    public void createServiceRequest(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.getServiceRequest(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.SERVICEREQUEST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    public void CancelServiceRequest(String token, HashMap<String, String> params) {
        Call<CreateFolloupModel> call = apiInterface.CancelServiceRequest(token, params);
        call.enqueue(new Callback<CreateFolloupModel>() {
            @Override
            public void onResponse(Call<CreateFolloupModel> call, Response<CreateFolloupModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CreateFolloupModel createFolloupModel = response.body();
                    SuperClassCastBean superClassCastBean = createFolloupModel;

                    apiResponseListener.onSuccess(ApiConstant.CANCELBOOKINGREQUEST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }


            @Override
            public void onFailure(Call<CreateFolloupModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }
    // Get Sales Enquiry Data in this Function  API Code
    public void getSearchSalesEnquiryData(String token, HashMap<String, String> params) {



        Call<SalesEnquiryDataModel> call = apiInterface.getSearchingData(token, params);
        call.enqueue(new Callback<SalesEnquiryDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryDataModel> call, Response<SalesEnquiryDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SalesEnquiryDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESENQUIRYDATASEARCH, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESENQUIRYDATASEARCH);
            }
        });


    }


    // Get Sales Enquiry Data in this Function  API Code
    public void getSearchExpectedSalesEnquiryData(String token, HashMap<String, String> params) {



        Call<SalesBookingDataModel> call = apiInterface.getExpectedSearchingData(token, params);
        call.enqueue(new Callback<SalesBookingDataModel>() {
            @Override
            public void onResponse(Call<SalesBookingDataModel> call, Response<SalesBookingDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SalesBookingDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESEXPECTEDENQUIRYDATASEARCH, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesBookingDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESEXPECTEDENQUIRYDATASEARCH);
            }
        });


    }

    public void getFilteredData(String token, FilterDataSendModel filterDataSendModel) {



        Call<SalesEnquiryDataModel> call = apiInterface.getFilteredData(token, filterDataSendModel);
        call.enqueue(new Callback<SalesEnquiryDataModel>() {
            @Override
            public void onResponse(Call<SalesEnquiryDataModel> call, Response<SalesEnquiryDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.code()==500)
                {

                    Toast.makeText(context, "Something Went Wrong.Please try again..!!", Toast.LENGTH_SHORT).show();
                    Sales_Followup_Activity.SALEScustomWaitingDialog.dismiss();

                }
                if (response.code()==500)
                {

                    Toast.makeText(context, "Something Went Wrong.Please try again..!!", Toast.LENGTH_SHORT).show();
                    Sales_Followup_Activity.SALEScustomWaitingDialog.dismiss();

                }
                if (response.body() != null) {
                    SalesEnquiryDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESENQUIRYDATAFILTER, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesEnquiryDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESENQUIRYDATAFILTER);
            }
        });


    }

    public void getExpectedFilteredData(String token, FilterDataSendModel filterDataSendModel) {



        Call<SalesBookingDataModel> call = apiInterface.getExptectedFilteredData(token, filterDataSendModel);
        call.enqueue(new Callback<SalesBookingDataModel>() {
            @Override
            public void onResponse(Call<SalesBookingDataModel> call, Response<SalesBookingDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SalesBookingDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SALESEXPECTEDENQUIRYDATAFILTER, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SalesBookingDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SALESEXPECTEDENQUIRYDATAFILTER);
            }
        });


    }

    public void getReportingData(String token, HashMap<String, String> params) {



        Call<ReportingModel> call = apiInterface.getReportingData(token, params);
        call.enqueue(new Callback<ReportingModel>() {
            @Override
            public void onResponse(Call<ReportingModel> call, Response<ReportingModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    ReportingModel reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.REPORTINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ReportingModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.REPORTINGDATA);
            }
        });


    }

    public void getExpectedReportingData(String token, HashMap<String, String> params) {



        Call<ReportingModel> call = apiInterface.getExpectedReportingData(token, params);
        call.enqueue(new Callback<ReportingModel>() {
            @Override
            public void onResponse(Call<ReportingModel> call, Response<ReportingModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    ReportingModel reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.EXPECTEDREPORTINGDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<ReportingModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.EXPECTEDREPORTINGDATA);
            }
        });


    }



    public void getReportingData_SR(String token, HashMap<String, String> params) {

        Call<SR_Report_Model> call = apiInterface.getReportingData_SR(token, params);
        call.enqueue(new Callback<SR_Report_Model>() {
            @Override
            public void onResponse(Call<SR_Report_Model> call, Response<SR_Report_Model> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SR_Report_Model reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.REPORTINGDATA_SR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SR_Report_Model> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.REPORTINGDATA_SR);
            }
        });


    }



    public void getReportingData_BR(String token, HashMap<String, String> params) {

        Call<BR_Report_Model> call = apiInterface.getReportingData_BR(token, params);
        call.enqueue(new Callback<BR_Report_Model>() {
            @Override
            public void onResponse(Call<BR_Report_Model> call, Response<BR_Report_Model> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    BR_Report_Model reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.REPORTINGDATA_BR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<BR_Report_Model> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.REPORTINGDATA_BR);
            }
        });


    }


    public void getVersionData(String token) {

        Call<GetVersionModel> call = apiInterface.getVersion(token);
        call.enqueue(new Callback<GetVersionModel>() {
            @Override
            public void onResponse(Call<GetVersionModel> call, Response<GetVersionModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    GetVersionModel versionModel = response.body();
                    SuperClassCastBean superClassCastBean = versionModel;

                    apiResponseListener.onSuccess(ApiConstant.GETVERSION, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetVersionModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.GETVERSION);
            }
        });


    }



    public void getUserData(String token, HashMap<String, String> params) {



        Call<UserDetailsModel> call = apiInterface.getUserDetails(token, params);
        call.enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //  Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    UserDetailsModel userDetailsModel = response.body();
                    SuperClassCastBean superClassCastBean = userDetailsModel;

                    apiResponseListener.onSuccess(ApiConstant.USERDETAILSDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.USERDETAILSDATA);
            }
        });


    }


    public void getUserNameList(String token, HashMap<String, String> params) {



        Call<UserNameListModel> call = apiInterface.getUserNameListData(token, params);
        call.enqueue(new Callback<UserNameListModel>() {
            @Override
            public void onResponse(Call<UserNameListModel> call, Response<UserNameListModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    UserNameListModel userNameListModel = response.body();
                    SuperClassCastBean superClassCastBean = userNameListModel;

                    apiResponseListener.onSuccess(ApiConstant.USERNAMELIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<UserNameListModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.USERNAMELIST);
            }
        });


    }



    public void getAllotedDataList(String token, HashMap<String, String> params) {

        Call<DataAllotedModel> call = apiInterface.getAllotedDatalist(token, params);
        call.enqueue(new Callback<DataAllotedModel>() {
            @Override
            public void onResponse(Call<DataAllotedModel> call, Response<DataAllotedModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    DataAllotedModel userNameListModel = response.body();
                    SuperClassCastBean superClassCastBean = userNameListModel;

                    apiResponseListener.onSuccess(ApiConstant.ALLOTEDDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<DataAllotedModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.ALLOTEDDATA);
            }
        });


    }

    public void getAllotedDSENew(String token, HashMap<String, String> params) {

        Call<AllotedDSEModel> call = apiInterface.getAllotedDSE(token, params);
        call.enqueue(new Callback<AllotedDSEModel>() {
            @Override
            public void onResponse(Call<AllotedDSEModel> call, Response<AllotedDSEModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    AllotedDSEModel reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.ALLOTEDDSE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AllotedDSEModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.ALLOTEDDSE);
            }
        });


    }

    public void getAllotedDataSR(String token, HashMap<String, String> params) {

        Call<SR_Alloted_data_model> call = apiInterface.getAllotedDATA_SR(token, params);
        call.enqueue(new Callback<SR_Alloted_data_model>() {
            @Override
            public void onResponse(Call<SR_Alloted_data_model> call, Response<SR_Alloted_data_model> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SR_Alloted_data_model reportingModel = response.body();
                    SuperClassCastBean superClassCastBean = reportingModel;

                    apiResponseListener.onSuccess(ApiConstant.ALLOTED_SR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SR_Alloted_data_model> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.ALLOTED_SR);
            }
        });


    }


    // Session Expired Popup
    public void sessionExpired_Popup()
    {


        if (CommonVariables.APP.equals(CommonVariables.APP_Role_1)) {

            final Dialog shippingDialog = new Dialog(context);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.session_expired);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();

            shippingDialog.setCanceledOnTouchOutside(false);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            okbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.hero.JCapp");
                    if (intent != null) {
                        shippingDialog.dismiss();
                        ComponentName componentName = intent.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        context.startActivity(mainIntent);
                        ((Activity)context).finish();
                        ((Activity)context).finishAndRemoveTask();
                        ((Activity)context).finishAffinity();
                    }
                }
            });



        } else if (CommonVariables.APP.equals(CommonVariables.APP_Role_2)) {


            final Dialog shippingDialog = new Dialog(context);
            shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            shippingDialog.setContentView(R.layout.session_expired);
            shippingDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            shippingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            shippingDialog.setCancelable(true);
            shippingDialog.setCanceledOnTouchOutside(false);
            shippingDialog.show();
            shippingDialog.setCanceledOnTouchOutside(false);
            TextView okbutton = shippingDialog.findViewById(R.id.okbutton);
            okbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.herocorp");
                    if (intent != null) {
                        shippingDialog.dismiss();
                        ComponentName componentName = intent.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        context.startActivity(mainIntent);
                        ((Activity)context).finish();
                        ((Activity)context).finishAndRemoveTask();
                        ((Activity)context).finishAffinity();
                    }
                }
            });

        }

    }


    // Token Get


    // Get SR Call Done Data in this Function  API Code
    public void getSRCallDoneData(String token, HashMap<String, String> params) {
        Call<SRCallDoneDataModel> call = apiInterface.getSRCalldoneData(token, params);
        call.enqueue(new Callback<SRCallDoneDataModel>() {
            @Override
            public void onResponse(Call<SRCallDoneDataModel> call, Response<SRCallDoneDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    SRCallDoneDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SRCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SRCallDoneDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.SRCALLDONEDATA);
            }
        });


    }




    // Get PSF Call Done Data in this Function  API Code
    public void getPSFCallDoneData(String token, HashMap<String, String> params) {
        Call<PSFDataModel> call = apiInterface.getPSFCalldoneData(token, params);
        call.enqueue(new Callback<PSFDataModel>() {
            @Override
            public void onResponse(Call<PSFDataModel> call, Response<PSFDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    PSFDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.PSFCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<PSFDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.PSFCALLDONEDATA);
            }
        });


    }


    // Get BR Call Done Data in this Function  API Code
    public void getBRCallDoneData(String token, HashMap<String, String> params) {
        Call<BRCallDoneDataModel> call = apiInterface.getBRCalldoneData(token, params);
        call.enqueue(new Callback<BRCallDoneDataModel>() {
            @Override
            public void onResponse(Call<BRCallDoneDataModel> call, Response<BRCallDoneDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    BRCallDoneDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.BRCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<BRCallDoneDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.BRCALLDONEDATA);
            }
        });


    }


    // Get NonTurnUP Call Done Data in this Function  API Code
    public void getNonTurnUPCallDoneData(String token, HashMap<String, String> params) {
        Call<NonTurnUpDataModel> call = apiInterface.getNONTURNUPBOOKINGCalldoneData(token, params);
        call.enqueue(new Callback<NonTurnUpDataModel>() {
            @Override
            public void onResponse(Call<NonTurnUpDataModel> call, Response<NonTurnUpDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    NonTurnUpDataModel salesEnquiryDataModel = response.body();
                    SuperClassCastBean superClassCastBean = salesEnquiryDataModel;

                    apiResponseListener.onSuccess(ApiConstant.NONTURNUPCALLDONEDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<NonTurnUpDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.NONTURNUPCALLDONEDATA);
            }
        });


    }


    // Update History Id in  Database for this function API Code
    public void UpdateHistorySalesExpected(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesalesExpectedhistoryId(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESEXPECTEDHISTORYID, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Date of Purchase in Database for this function API Code
    public void UpdateExpecteddateofpurchase(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpecteddateofpurchase(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }

                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEEXPECTEDDATEOFPURCHASE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Reason in Database for this function API Code
    public void UpdateExpectedClosureReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpectedclosurereason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEEXPECTEDCLOSUREREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Closure Sub Reason in Database for this function API Code
    public void UpdateExpectedClosureSubReason(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpectedclosuresubreason(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEEXPECTEDCLOSURESUBREASON, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Model in Database for this function API Code
    public void UpdateExpectedMake(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpectedmake(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEEXPECTEDMAKE, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Model in Database for this function API Code
    public void UpdateExpectedModel(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpectedmodel(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATEEXPECTEDMODEL, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleExpectedFollowUpStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updateExpectedDateOfPurchaseFollowUpDone(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPSTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Update Sales Follow Up  Contatct  Status in Database for this function API Code
    public void UpdateSaleExpectedFollowUpDoneStatus(String token, HashMap<String, String> params) {
        Call<CommonModel> call = apiInterface.updatesExpectedfollowupdonestatus(token, params);
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    CommonModel commonModel = response.body();
                    SuperClassCastBean superClassCastBean = commonModel;

                    apiResponseListener.onSuccess(ApiConstant.UPDATESALESFOLLOWUPDONESTATUS, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get Booking Reminder Data in this Function  API Code
    public void getBookingReminderData(String token, HashMap<String, String> params) {
        Call<booking_reminder_model> call = apiInterface.getBookingRemindersData(token, params);
        call.enqueue(new Callback<booking_reminder_model>() {
            @Override
            public void onResponse(Call<booking_reminder_model> call, Response<booking_reminder_model> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.body() != null) {
                    booking_reminder_model serviceReminderDataModel = (booking_reminder_model) response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.BOOKINGREMINDERDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<booking_reminder_model> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.BOOKINGREMINDERDATA);
            }
        });


    }


    // Get Booking Reminder History Data in this Function  API Code
    public void getBookingReminderHistoryData(String token, HashMap<String, String> params) {
        Call<BR_HistoryModel> call = apiInterface.getBookingReminderHistory(token, params);
        call.enqueue(new Callback<BR_HistoryModel>() {
            @Override
            public void onResponse(Call<BR_HistoryModel> call, Response<BR_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.body() != null) {
                    BR_HistoryModel serviceReminderDataModel = (BR_HistoryModel) response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.BOOKINGREMINDERHISTORYDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<BR_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.BOOKINGREMINDERHISTORYDATA);
            }
        });


    }





    // Get Booking Reminder History Data in this Function  API Code
    public void getPSFHistoryData(String token, HashMap<String, String> params) {
        Call<SR_HistoryModel> call = apiInterface.getPSFHistory(token, params);
        call.enqueue(new Callback<SR_HistoryModel>() {
            @Override
            public void onResponse(Call<SR_HistoryModel> call, Response<SR_HistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.body() != null) {
                    SR_HistoryModel serviceReminderDataModel = (SR_HistoryModel) response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.PSFHISTORYDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<SR_HistoryModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.PSFHISTORYDATA);
            }
        });


    }
    // Add History in Database API Code
    public void addBookingReminderHistory(String token, HashMap<String, String> params)
    {
        Call<AddCallHistoryUpdateModel> call = apiInterface.addBookingReminderHistory(token, params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    AddCallHistoryUpdateModel addCallHistoryUpdateModel = response.body();
                    SuperClassCastBean superClassCastBean = addCallHistoryUpdateModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }



    // Get Booking Reminder Data in this Function  API Code
    public void getNonTurnUpData(String token, HashMap<String, String> params) {
        Call<NonTurnUpDataModel> call = apiInterface.getNonTurnUpData(token, params);
        call.enqueue(new Callback<NonTurnUpDataModel>() {
            @Override
            public void onResponse(Call<NonTurnUpDataModel> call, Response<NonTurnUpDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.body() != null) {
                    NonTurnUpDataModel serviceReminderDataModel = (NonTurnUpDataModel) response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.NONTURNUPREMINDERDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<NonTurnUpDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.NONTURNUPREMINDERDATA);
            }
        });


    }




    // Add History in Database API Code
    public void addNonTurnUpHistory(String token, HashMap<String, String> params)
    {
        Call<AddCallHistoryUpdateModel> call = apiInterface.addNonTurnUpHistory(token, params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    AddCallHistoryUpdateModel addCallHistoryUpdateModel = response.body();
                    SuperClassCastBean superClassCastBean = addCallHistoryUpdateModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }

    // Get PSF Booking Reminder Data in this Function  API Code
    public void getPSFData(String token, HashMap<String, String> params) {
        Call<PSFDataModel> call = apiInterface.getPSFData(token, params);
        call.enqueue(new Callback<PSFDataModel>() {
            @Override
            public void onResponse(Call<PSFDataModel> call, Response<PSFDataModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();

                }
                if (response.body() != null) {
                    PSFDataModel serviceReminderDataModel = (PSFDataModel) response.body();
                    SuperClassCastBean superClassCastBean = serviceReminderDataModel;

                    apiResponseListener.onSuccess(ApiConstant.SERVICEREMINDERDATA, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<PSFDataModel> call, Throwable t) {
                apiResponseListener.onFailure(ApiConstant.BOOKINGREMINDERDATA);
            }
        });


    }




    // Add PSF History in Database API Code
    public void addPSFHistory(String token, HashMap<String, String> params)
    {
        Call<AddCallHistoryUpdateModel> call = apiInterface.addPSFHistory(token, params);
        call.enqueue(new Callback<AddCallHistoryUpdateModel>() {
            @Override
            public void onResponse(Call<AddCallHistoryUpdateModel> call, Response<AddCallHistoryUpdateModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    //Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    AddCallHistoryUpdateModel addCallHistoryUpdateModel = response.body();
                    SuperClassCastBean superClassCastBean = addCallHistoryUpdateModel;

                    apiResponseListener.onSuccess(ApiConstant.ADDCALLHISTORYUPDATESR, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<AddCallHistoryUpdateModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });

    }


    // Get Service Reminder History Data in this Function  API Code
    public void getNonTurnUpBookingHistory(String token, HashMap<String, String> params) {
        Call<GetNonTurnUpHistoryModel> call = apiInterface.getNonTurnUpBookingReminder(token, params);
        call.enqueue(new Callback<GetNonTurnUpHistoryModel>() {
            @Override
            public void onResponse(Call<GetNonTurnUpHistoryModel> call, Response<GetNonTurnUpHistoryModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    GetNonTurnUpHistoryModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<GetNonTurnUpHistoryModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }


    //  getPSFRatingQuestionsList Data in this Function  API Code
    public void getPSFRatingQuestionsList(String token) {
        Call<PSFSOPRatingQuestionsModel> call = apiInterface.getPSFSOPRatingQuestions(token);
        call.enqueue(new Callback<PSFSOPRatingQuestionsModel>() {
            @Override
            public void onResponse(Call<PSFSOPRatingQuestionsModel> call, Response<PSFSOPRatingQuestionsModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    PSFSOPRatingQuestionsModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETPSFQUESTIONLIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<PSFSOPRatingQuestionsModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // Get PSFDissatisfactionReason Data in this Function  API Code
    public void getPSFDissatisfactionReason(String token) {
        Call<PSFDissatisfactionReasonModel> call = apiInterface.getPSFDissatisfactionreason(token);
        call.enqueue(new Callback<PSFDissatisfactionReasonModel>() {
            @Override
            public void onResponse(Call<PSFDissatisfactionReasonModel> call, Response<PSFDissatisfactionReasonModel> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    PSFDissatisfactionReasonModel sr_historyModel = response.body();
                    SuperClassCastBean superClassCastBean = sr_historyModel;

                    apiResponseListener.onSuccess(ApiConstant.GETPSFDISATISFACTIONREASONLIST, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<PSFDissatisfactionReasonModel> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    // updatePSFRatingQuestionsAnswer in this Function  API Code
    public void updatePSFRatingQuestionsAnswer(String token, HashMap<String, String> params) {
        Call<JSONObject> call = apiInterface.insertOrUpdatePSFAnswer(token, params);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    JSONObject sr_historyModel = response.body();
//                    SuperClassCastBean superClassCastBean = sr_historyModel;
//
//                    apiResponseListener.onSuccess(ApiConstant.GETCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }

    //  updatePSFdissatisfactionreason in this Function  API Code
    public void updatePSFdissatisfactionreason(String token, HashMap<String, String> params) {
        Call<JSONObject> call = apiInterface.updatePSFdissatisfactionreason(token, params);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if(response.code()==401)
                {
                    sessionExpired_Popup();
                    // Toast.makeText(context, "token exprired , please login again", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
//                    GetNonTurnUpHistoryModel sr_historyModel = response.body();
//                    SuperClassCastBean superClassCastBean = sr_historyModel;
//
//                    apiResponseListener.onSuccess(ApiConstant.GETCALLINGHISTORY, superClassCastBean);
                } else {
                    apiResponseListener.onError("No Data");
                }

            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                apiResponseListener.onFailure("Failure");
            }
        });


    }
}
